#+TITLE: Standardized Keybindings Across All Tiling Window Managers
#+DESCRIPTION: DT Articles - Standardized Keybindings Across All Tiling Window Managers
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* Standardized Keybindings Across All Tiling Window Managers
/By Derek Taylor at March 12, 2020/

One of the things that I have been working on recently is cleaning up all of my tiling window manager configuration files. I know many people grab my configs from my GitLab, and I want to make sure that those people have a nice, consistent experience when trying out various window managers.

One of the things I want to do is provide consistent keybindings among the various window manager configs on my GitLab. I think that this is important because it will make transitioning from one window manager to another easier, since many of the keybindings will be the same. Until today, this has not been the case. The command to open dmenu was not the same in all of my configs. The command to switch monitor focus was not the same in all of my configs. And so on.

So I'm working hard to rectify this problem and standardize on keybindings. Here are the some of the basic functions that almost every tiling window manager has builtin, and here are my standardized keybindings:

| Keybinding              | Action                                                               |
|-------------------------+----------------------------------------------------------------------|
| MODKEY + RETURN         | opens terminal                                                       |
| MODKEY + SHIFT + RETURN | opens run launcher (such as dmenu)                                   |
| MODKEY + TAB            | rotates through the available layouts                                |
| MODKEY + SHIFT + c      | closes window with focus                                             |
| MODKEY + SHIFT + r      | restarts the window manager                                          |
| MODKEY + SHIFT + q      | quits the window manager                                             |
| MODKEY + 1-9            | switch focus to workspace (1-9)                                      |
| MODKEY + SHIFT + 1-9    | send focused window to workspace (1-9)                               |
| MODKEY + j              | switches focus between windows in stack, the opposite direction of k |
| MODKEY + k              | switches focus between windows in stack, the opposite direction of j |
| MODKEY + SHIFT + j      | rotates the windows in the stack, the opposite direction of k        |
| MODKEY + SHIFT + k      | rotates the windows in the stack, the opposite direction of j        |
| MODKEY + h              | expand size of window                                                |
| MODKEY + l              | shrink size of window                                                |
| MODKEY + w              | switch focus to monitor 1                                            |
| MODKEY + e              | switch focus to monitor 2                                            |
| MODKEY + r              | switch focus to monitor 3                                            |
| MODKEY + period         | switch focus to next monitor                                         |
| MODKEY + comma          | switch focus to prev monitor                                         |

This is just an initial attempt at standardizing keybindings. I may add more to this list, because there may be other basic functions that most, if not all, tiling window managers have; and if so, then I want to be sure the keybindings for those functions are consistent. For the most part I'm sticking with commonly used bindings; for example, SUPER + RETURN to open a terminal seems reasonable. It's the default keybinding for that function in many tiling window managers, so why reinvent the wheel.

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
