#+TITLE: STDIN, STDOUT & STDERR

* STDIN, STDOUT & STDERR
STDIN, STDOUT and STDERR are three data streams that used when you run Linux commands.  Like everything on a Unix-like system, these three data streams are files that exist on your system.  They are located at:

+ /dev/stdin
+ /dev/stdout
+ /dev/stderr

** STDIN (Standard Input)
STDIN is symbolized by the number '0'.  You can also think of STDIN as '0<' but usually we drop the '0' and just use '<'.  For example, the following three commands are exactly the same thing.

#+begin_example
cat 0< filename
cat < filename
cat filename
#+end_example

=NOTE:= The 'cat' command assumes the filename right after it to be STDIN as an argument.

** STDOUT (Standard Output)
STDOUT is symbolized by the number '1'.  You can also think of STDOUT as '1>' but usually we drop the '1' and just use '>'.  For example, the following two commands are exactly the same thing.

#+begin_example
cat filename 1> /dev/stdout
cat filename
#+end_example

In the above examples, the 'cat' command by default prints to STDOUT, so there is no need to specifically tell it to do so like we did in the first command.  Now, if you want 'cat' to write to another location that is not STDOUT, then you need to tell it to do so.  The following two examples do the same thing, which is write the contents of "filename" into "newfile".

#+begin_example
cat filename 1> newfile.txt
cat filename > newfile.txt
#+end_example

But remember, in the example above, we really don't have use '1>' since just '>' assumes we are wanting to send to STDOUT.

So to sum up STDIN and STDOUT using  'cat' as an example, the basic 'cat filename' is actually:

#+begin_example
cat 0< filename 1> /dev/stdout
#+end_example

+ 0, 1 ,2
+ 0<, 1>, 2>
+ /dev/stdin , /dev/stdout , /dev/stderr

** STDERR (Standard Error)
STDERR is symbolized by the number '2'.  You can also think of STDERR as '2>'.  Like STDOUT, STDERR is output but it is a different kind of output.  STDERR is the output you should get when a command encounters an error.

Most people just cat to print a file to STDOUT.
#+begin_example
cat filename
cat 0< filename
cat < filename
cat 0< filename 1> /dev/stdout
#+end_example

#+begin_example
cat filename > newfile.txt
# is actually #
cat 0< filename 1> newfile.txt
cat filename 1> newfile.txt
cat filename 2> newfile.txt # for STDERR instead
#+end_example

#+begin_example
ls -z
Error: unknown argument -z
ls -z 2> /dev/null
#+end_example

** Overwriting versus appending files (> vs >>)
