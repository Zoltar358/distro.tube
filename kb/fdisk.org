#+TITLE: Fdisk
#+DESCRIPTION: Knowledge Base - fdisk
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* What is fdisk?
fdisk is a program for managing partition tables and partitions on a hard disk.  It understands GPT, MBR, Sun, SGI and BSD partition tables.

* Basic commands
** List partitions:
sudo fdisk -l

** Start the partition manipulator:
sudo fdisk /dev/sdX

** Create a new empty partition table:
*** For a new GPT table:
g
*** For a new DOS table:
o

** Partitioning the disk:
*** create a partition:
n
*** change partition type:
t
*** delete a partition:
d
*** print the partition table:
p
*** write the changes made:
w
*** quit without saving:
q
*** open a help menu:
m

* After fdisk
** Make your file system:
+ sudo mkswap /dev/vda1
+ sudo swapon /dev/vda1
+ sudo mkfs.ext4 /dev/vda2
+ sudo mkfs.ext4 /dev/vdb1

** sudo blkid
This command gets the UUID of your block devices if you need to add to the fstab.  This is useful when wanting to auto-mount partitions that are on their own disk (such as /home or /music).

** Editing the fstab:
sudo vim /etc/fstab (or nano, if you prefer)

** Add this line at the end of the fstab:
UUID=<noted number from above> /home ext4 defaults 0 2

** Now reboot and see if it works!
sudo reboot

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
