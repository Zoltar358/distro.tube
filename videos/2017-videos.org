#+TITLE: DT Videos
#+DESCRIPTION: DistroTube Videos From 2021
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

#+begin_example
     __ __       _______ _______ _____ _______
 .--|  |  |_    |       |   _   | _   |   _   |
 |  _  |   _|   |___|   |.  |   |.|   |___|   |
 |_____|____|    /  ___/|.  |   `-|.  |  /   / 
                |:  1  \|:  1   | |:  | |   |  
                |::.. . |::.. . | |::.| |   |  
                `-------`-------' `---' `---'
#+end_example

* DistroTube Videos from 2017
DT's videos are hosted on YouTube and on Odysee.  The links on this page are Odysee links.  Videos are sorted in reverse order.
+ [[https://odysee.com/@DistroTube:2/calculate-linux-17-12-xfce-great-way-to:0][Calculate Linux 17.12 XFCE - Great Way To End 2017]]
+ [[https://odysee.com/@DistroTube:2/measuring-distro-popularity-and-why:9][Measuring Distro Popularity And Why Distrowatch's Page Hit Rankings Suck.]]
+ [[https://odysee.com/@DistroTube:2/my-manjaro-kde-journey-day-one:1][My Manjaro KDE Journey, Day One]]
+ [[https://odysee.com/@DistroTube:2/made-my-decision-the-distro-i-m:8][Made my decision. The distro I'm installing for 2018 is....]]
+ [[https://odysee.com/@DistroTube:2/how-to-install-debian-unstable:3][How To Install Debian Unstable]]
+ [[https://odysee.com/@DistroTube:2/top-five-linux-distros-of-2017:7][Top Five Linux Distros of 2017]]
+ [[https://odysee.com/@DistroTube:2/archbang-systemd-install-review:5][Archbang (systemd) Install & Review]]
+ [[https://odysee.com/@DistroTube:2/ulauncher-fast-application-launcher:2][Ulauncher Fast Application Launcher]]
+ [[https://odysee.com/@DistroTube:2/endless-os-3-3-5-first-impression:e][Endless OS 3.3.5 First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/a-look-at-how-i-setup-virtualbox-for:7][A Look at How I Setup Virtualbox For Testing Linux Distros]]
+ [[https://odysee.com/@DistroTube:2/salentos-neriton-first-impression:3][SalentOS Neriton First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/rofi-application-launcher-window:e][Rofi - Application Launcher, Window Switcher and Run Command Utility]]
+ [[https://odysee.com/@DistroTube:2/puzzle-moppet-3d-puzzle-game-for-linux:5][Puzzle Moppet - 3D Puzzle Game for Linux (and Mac and Windows)]]
+ [[https://odysee.com/@DistroTube:2/a-quick-look-at-the-official-release-of:a][A Quick Look at the Official Release of MX Linux 17]]
+ [[https://odysee.com/@DistroTube:2/sabayon-lxqt-daily-build-install-review:d][Sabayon LXQt Daily Build Install & Review]]
+ [[https://odysee.com/@DistroTube:2/swagarch-gnu-linux-first-impression:c][SwagArch GNU/Linux First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/revisiting-solus-why-it-isn-t-right-for:d][Revisiting Solus. Why It Isn't Right For Me, Yet.]]
+ [[https://odysee.com/@DistroTube:2/crunchbangplusplus-install-review:][] CrunchBangPlusPlus (#!++) Install & Review]]
+ [[https://odysee.com/@DistroTube:2/a-little-supertuxkart-with-a-bit-of-talk:][] A Little SuperTuxKart With a Bit of Talk About Linux Gaming of the Past]]
+ [[https://odysee.com/@DistroTube:2/i-install-unity7sl-and-discuss-the-pros:][] I Install unity7sl and Discuss the Pros of the Unity Desktop and Its Future.]]
+ [[https://odysee.com/@DistroTube:2/bodhi-linux-4-4-0-apppack-release:][] Bodhi Linux 4.4.0 AppPack Release Install & Review]]
+ [[https://odysee.com/@DistroTube:2/the-openbox-window-manager-install-setup:][] The Openbox Window Manager - Install & Setup]]
+ [[https://odysee.com/@DistroTube:2/peppermint-os-8-install-review:][] Peppermint OS 8 Install & Review]]
+ [[https://odysee.com/@DistroTube:2/archmerge-first-impression-install:][] ArchMerge First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/rosa-r10-fresh-kde-first-impression:][] ROSA R10 Fresh KDE First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/gradio-searching-and-listening-to:][] Gradio - Searching and Listening to  Internet Radio Stations]]
+ [[https://odysee.com/@DistroTube:2/distro-hopping-which-distro-should-i-go:][] Distro-Hopping.  Which Distro Should I Go With?  Help Me Decide!]]
+ [[https://odysee.com/@DistroTube:2/solus-first-impression-install-review:][] Solus First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/stacer-system-monitor-optimizer-and:][] Stacer - System Monitor, Optimizer and Cleaner]]
+ [[https://odysee.com/@DistroTube:2/antix-17-first-impression-install-review:][] AntiX 17 First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/deepin-15-5-first-impression-install:][] Deepin 15.5 First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/stress-terminal-ui-s-tui-stress-testing:][] Stress Terminal UI (s-tui) - Stress-Testing System Monitor]]
+ [[https://odysee.com/@DistroTube:2/nmon-terminal-based-system-performance:][] Nmon - Terminal-Based System Performance Monitor]]
+ [[https://odysee.com/@DistroTube:2/xebian-first-impression-install-review:][] Xebian First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/linux-mint-18-3-sylvia-install-review:][] Linux Mint 18.3 Sylvia Install & Review]]
+ [[https://odysee.com/@DistroTube:2/pclinuxos-first-impression-install:][] PCLinuxOS First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/bunsenlabs-linux-install-review:][] BunsenLabs Linux Install & Review]]
+ [[https://odysee.com/@DistroTube:2/lxle-16-04-3-install-review:][] LXLE 16.04.3 Install & Review]]
+ [[https://odysee.com/@DistroTube:2/glances-terminal-based-system-monitoring:][] Glances - Terminal-Based System Monitoring Tool]]
+ [[https://odysee.com/@DistroTube:2/chakra-linux-first-impression-install:][] Chakra Linux First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/htop-terminal-based-interactive-process:][] Htop - Terminal-Based Interactive Process Viewing Program]]
+ [[https://odysee.com/@DistroTube:2/calculate-linux-first-impression-install:][] Calculate Linux First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/korora-26-first-impression-install:][] Korora 26 First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/ranger-a-minimal-terminal-based-file:][] Ranger - A Minimal Terminal-Based File Manager]]
+ [[https://odysee.com/@DistroTube:2/fedora-27-workstation-installation-and:][] Fedora 27 Workstation Installation and Review]]
+ [[https://odysee.com/@DistroTube:2/mx-linux-17-beta-1-first-impression:][] MX Linux 17 Beta 1 First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/redcore-linux-first-impression-install:][] Redcore Linux First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/canto-terminal-based-rss-feed-reader:][] Canto - Terminal-Based RSS Feed Reader]]
+ [[https://odysee.com/@DistroTube:2/truesos-formerly-pc-bsd-first-impression:][] TruesOS (formerly PC-BSD) First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/kaos-first-impression-install-review:][] KaOS First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/cmus-terminal-based-music-player:][] Cmus - Terminal-Based Music Player - Lightweight and Super-Fast]]
+ [[https://odysee.com/@DistroTube:2/terminal-commands-lesson-04-editing-text:][] Terminal Commands Lesson 04 - Editing Text Files - echo, cat, nano, vi]]
+ [[https://odysee.com/@DistroTube:2/terminal-commands-lesson-03-which:][] Terminal Commands Lesson 03 - which, whereis, locate, find]]
+ [[https://odysee.com/@DistroTube:2/lynx-text-based-web-browser-surf-the-web:][] Lynx - Text-based Web Browser - Surf the Web From a Terminal]]
+ [[https://odysee.com/@DistroTube:2/tails-live-operating-system-privacy-for:][] Tails Live Operating System - Privacy For Anyone Anywhere]]
+ [[https://odysee.com/@DistroTube:2/irssi-terminal-irc-client-who-needs-a:][] Irssi - Terminal IRC Client - Who Needs a GUI to Chat!]]
+ [[https://odysee.com/@DistroTube:2/clamav-anti-virus-for-linux-is-it:][] ClamAV - Anti-Virus for Linux - Is It Necessary?]]
+ [[https://odysee.com/@DistroTube:2/calcurse-organizer-and-scheduling-app:][] Calcurse - Organizer and Scheduling App]]
+ [[https://odysee.com/@DistroTube:2/ubuntu-studio-17-10-artful-aardvark:][] Ubuntu Studio 17.10 Artful Aardvark - Install and Review]]
+ [[https://odysee.com/@DistroTube:2/ubuntu-budgie-17-10-artful-aarvark:][] Ubuntu Budgie 17.10 Artful Aarvark - Install and Review]]
+ [[https://odysee.com/@DistroTube:2/ubuntu-mate-17-10-artful-aarvark-install:][] Ubuntu MATE 17.10 Artful Aarvark - Install and Review]]
+ [[https://odysee.com/@DistroTube:2/lubuntu-17-10-artful-aarvark-install-and:][] Lubuntu 17.10 Artful Aarvark - Install and Review]]
+ [[https://odysee.com/@DistroTube:2/xubuntu-17-10-artful-aarvark-install-and:][] Xubuntu 17.10 Artful Aarvark - Install and Review]]
+ [[https://odysee.com/@DistroTube:2/kubuntu-17-10-artful-aarvark-install-and:][] Kubuntu 17.10 Artful Aarvark - Install and Review]]
+ [[https://odysee.com/@DistroTube:2/feren-os-first-impression-install-review:][] Feren OS First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/opensuse-tumbleweed-install-and-review:][] OpenSUSE Tumbleweed Install and Review KDE edition]]
+ [[https://odysee.com/@DistroTube:2/qtile-tiling-window-manager-written-in:][] Qtile - Tiling Window Manager Written in Python]]
+ [[https://odysee.com/@DistroTube:2/archlabs-minimo-first-impression-install:][] ArchLabs Minimo First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/sabayon-minimal-edition-most-painful:][] Sabayon Minimal Edition - Most Painful Linux Install Ever?]]
+ [[https://odysee.com/@DistroTube:2/siduction-lxqt-first-impression-install:][] Siduction LXQt First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/categorizing-linux-distros-which-one-is:][] Categorizing Linux Distros - Which One Is Best For Beginners?]]
+ [[https://odysee.com/@DistroTube:2/antergos-cinnamon-first-impression:][] Antergos Cinnamon First Impression Install & Review]]
+ [[https://odysee.com/@DistroTube:2/welcome-to-distrotube:][] Welcome to DistroTube]]
+ [[https://odysee.com/@DistroTube:2/terminal-commands-lesson-02-touch-mkdir:][] Terminal Commands Lesson 02 - touch, mkdir, mv, cp, rm, rmdir]]
+ [[https://odysee.com/@DistroTube:2/terminal-commands-lesson-01-pwd-cd-ls:][] Terminal Commands Lesson 01 - pwd, cd, ls, man, --help]]
+ [[https://odysee.com/@DistroTube:2/manjaro-linux-kde-first-impression:][] Manjaro Linux KDE First Impression Install & Review]]

* More Videos From DistroTube
+ [[file:2022-videos.org][Videos from 2022]]
+ [[file:2021-videos.org][Videos from 2021]]
+ [[file:2020-videos.org][Videos from 2020]]
+ [[file:2019-videos.org][Videos from 2019]]
+ [[file:2018-videos.org][Videos from 2018]]
+ [[file:2017-videos.org][Videos from 2017]]

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
