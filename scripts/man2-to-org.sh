#!/usr/bin/env bash

### man2 ###
# An array of all files in the 'mandb' directory
readarray -t manfiles < <(find ../mandb/man2 -iname "*.*")

# For each file, convert to from 'man' to 'org'.
for file in "${manfiles[@]}"; do
   name=$(echo "$file" | awk -F / '{print $NF}')
   pandoc -o ../man-org/man2/"$name".org -f man -t org "$file" && \
      echo "$file.org created." || echo "Error converting $file."
done

cd ../man-org/man2/ || exit
yes | rm ./*.gz
