#!/usr/bin/env bash

readarray -t orgbefore < <(find ../man-org/ -type f -iname "*.org")

for x in "${orgbefore[@]}"; do
    filename=$(basename -- "$x")
    extension="${filename##*.}"
    filename="${filename%.*}"
    
    sed -i '1s/^/\#+INCLUDE\: "\~\/nc\/gitlab-repos\/distro.tube\/header.org"\n/' "$x"
    sed -i '1s/^/\#+OPTIONS\: num:nil ^:{}\n/' "$x"
    sed -i '1s/^/\#+SETUPFILE\: https:\/\/distro.tube\/org-html-themes\/org\/theme-readtheorg.setup\n/' "$x"
    sed -i "1s/^/\#+DESCRIPTION\: Linux manpage for ${filename}\n/" "$x"
    sed -i "1s/^/\#+TITLE\: Manpages - ${filename}\n/" "$x"

    echo "Adding header to ${filename}"
done
