#!/usr/bin/env bash

# An array of all files in the 'mandb/man0p' directory
readarray -t manfiles < <(find ../mandb/man0p -iname "*.*")

# For each file, convert to from 'man' to 'org'.
for file in "${manfiles[@]}"; do
    name=$(echo "$file" | awk -F / '{print $NF}')
    sed -i "s/\.so .*/Links to manpage for: ${name}/g"
    pandoc -o ../man-org/man0p/"$name".org -f man -t org "$file" && \
      echo "$file.org created." || echo "Error converting $file."
done
