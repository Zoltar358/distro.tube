#!/usr/bin/env bash

readarray -t manpages < <(find ../mandb -iname "*.gz")

for x in "${manpages[@]}"; do
   gzip -d $x
done
