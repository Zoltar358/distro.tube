#!/usr/bin/env bash

### mann ###
# An array of all files in the 'mandb' directory
readarray -t manfiles < <(find ../mandb/mann -iname "*.*")

# For each file, convert to from 'man' to 'org'.
for file in "${manfiles[@]}"; do
   name=$(echo "$file" | awk -F / '{print $NF}')
   pandoc -o ../man-org/mann/"$name".org -f man -t org "$file" && \
      echo "$file.org created." || echo "Error converting $file."
done

cd ../man-org/mann/ || exit
yes | rm ./*.gz
