#!/usr/bin/env bash

# Run tree in gemini directory and get all gmi files in a list.
# We remove the .gmi extension in the names.
tree -fi | sed -n '/\.gmi$/{s///;s+^\./++;p}' > gmi_list

# An array of the files from above.
readarray -t options < gmi_list

# For each member of the array, we convert $i.gmi to $i.html
for i in "${options[@]}"; do
    getTitle=$(grep '^# ' "$i".gmi)
	outputFile=html/"$i".html
    printf "<!DOCTYPE html>\n<head>\n<meta charset=\"utf-8\"/>\n<link rel=\"stylesheet\" href=\"https:\/\/distrotube.com/css/style.css\"/>\n<title>${getTitle}</title>\n</head>\n<body>\n" > $outputFile
    cat "$i".gmi | sed 's/.gmi/.html/g;s/=> gemini/\n=> gemini/g' | gmi2html >> $outputFile
	echo "</body>" >> $outputFile
done
