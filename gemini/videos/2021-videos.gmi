```
     __ __       _______ _______ _______ _____ 
 .--|  |  |_    |       |   _   |       | _   |
 |  _  |   _|   |___|   |.  |   |___|   |.|   |
 |_____|____|    /  ___/|.  |   |/  ___/`-|.  |
                |:  1  \|:  1   |:  1  \  |:  |
                |::.. . |::.. . |::.. . | |::.|
                `-------`-------`-------' `---'
                                               
```

# DistroTube Videos from 2021 (sorted in reverse order)

TITLE: The Killer Feature Of Tiling Window Managers Isn't Tiling
=> https://odysee.com/@DistroTube:2/the-killer-feature-of-tiling-window:a

TITLE: To Protect Yourself, Never Give Your DNA Or Biometric Data
=> https://odysee.com/@DistroTube:2/to-protect-yourself,-never-give-your-dna:c

TITLE: Xargs Should Be In Your Command Line Toolbag
=> https://odysee.com/@DistroTube:2/xargs-should-be-in-your-command-line:7

TITLE: The Best Software Center For Debian and Ubuntu
=> https://odysee.com/@DistroTube:2/the-best-software-center-for-debian-and:e

TITLE: Is The End Of Linux Near?
=> https://odysee.com/@DistroTube:2/is-the-end-of-linux-near:0

TITLE: "Hey, DT! Don't Show The Terminal To New Users." (And Other Comments)
=> https://odysee.com/@DistroTube:2/hey,-dt!-don't-show-the-terminal-to-new:b

TITLE: Channel Introduction Video 2021
=> https://odysee.com/@DistroTube:2/channel-introduction-video-2021:b

TITLE: Linux Mint Has Three Flavors. Which Is Right For You?
=> https://odysee.com/@DistroTube:2/linux-mint-has-three-flavors.-which-is:4

TITLE: DON'T Upgrade To Windows 11! Upgrade To Linux Instead.
=> https://odysee.com/@DistroTube:2/don't-upgrade-to-windows-11!-upgrade-to:d

TITLE: Installing Xorg And DWM On Gentoo
=> https://odysee.com/@DistroTube:2/installing-xorg-and-dwm-on-gentoo:0

TITLE: A Rambling YouTube Studio Tour (#fluff-content)
=> https://odysee.com/@DistroTube:2/a-rambling-youtube-studio-tour-(-fluff:c

TITLE: BEWARE! Audacity Is Now Spyware. Time To Fork?
=> https://odysee.com/@DistroTube:2/beware!-audacity-is-now-spyware.-time-to:0

TITLE: Two Powerful Command Line Utilities 'cut' And 'tr'
=> https://odysee.com/@DistroTube:2/two-powerful-command-line-utilities:2

TITLE: Pop!_OS And The COSMIC Desktop. The Best Desktop Linux?
=> https://odysee.com/@DistroTube:2/pop!_os-and-the-cosmic-desktop.-the-best:b

TITLE: Htop Is Great But It Uses The Arrow Keys. Yuck! (#shorts)
=> https://odysee.com/@DistroTube:2/htop-is-great-but-it-uses-the-arrow:7

TITLE: A Base Gentoo Installation
=> https://odysee.com/@DistroTube:2/a-base-gentoo-installation:e

TITLE: The Right Mindset For Growing As A Linux User
=> https://odysee.com/@DistroTube:2/the-right-mindset-for-growing-as-a-linux:3

TITLE: Linux Boomer Reacts To Windows 11 Video Trailer
=> https://odysee.com/@DistroTube:2/linux-boomer-reacts-to-windows-11-video:e

TITLE: Learning Sed Is Beneficial For Linux Users
=> https://odysee.com/@DistroTube:2/learning-sed-is-beneficial-for-linux:a

TITLE: The Brave Search Engine. Will This Be The Google Killer?
=> https://odysee.com/@DistroTube:2/the-brave-search-engine.-will-this-be:9

TITLE: Taking Ubuntu To The Next Level
=> https://odysee.com/@DistroTube:2/taking-ubuntu-to-the-next-level:5

TITLE: You Are NOT The Software You Run! (#shorts)
=> https://odysee.com/@DistroTube:2/you-are-not-the-software-you-run!-(:1

TITLE: Transform Words Into Pretty Symbols In Emacs
=> https://odysee.com/@DistroTube:2/transform-words-into-pretty-symbols-in:4

TITLE: Redcore Linux Makes Gentoo User Friendly
=> https://odysee.com/@DistroTube:2/redcore-linux-makes-gentoo-user-friendly:4

TITLE: Word Processors Are Evil And Should Not Exist!
=> https://odysee.com/@DistroTube:2/word-processors-are-evil-and-should-not:c

TITLE: Learning Awk Is Essential For Linux Users
=> https://odysee.com/@DistroTube:2/learning-awk-is-essential-for-linux:4

TITLE: The Beginner's Guide To SSH
=> https://odysee.com/@DistroTube:2/the-beginner's-guide-to-ssh:c

TITLE: Why So Many People Lack Critical Thinking Skills
=> https://odysee.com/@DistroTube:2/why-so-many-people-lack-critical:7

TITLE: What Are The Benefits Of Emacs Over Vim?
=> https://odysee.com/@DistroTube:2/what-are-the-benefits-of-emacs-over-vim:3

TITLE: Switch To A Trackball Mouse And Save Your Wrists
=> https://odysee.com/@DistroTube:2/switch-to-a-trackball-mouse-and-save:5

TITLE: "Hey, DT! I Want A Distro In Between Ubuntu and Arch." (And Other Questions)
=> https://odysee.com/@DistroTube:2/hey,-dt!-i-want-a-distro-in-between:c

TITLE: A First Look At Two Editions Of Peux OS (Xfce, XMonad) 
=> https://odysee.com/@DistroTube:2/a-first-look-at-two-editions-of-peux-os:3

TITLE: Life Without The Internet? It's Actually BETTER! (Boomer Rant) 
=> https://odysee.com/@DistroTube:2/life-without-the-internet-it's-actually:0

TITLE: Nyxt Is The Most Customizable Web Browser EVER 
=> https://odysee.com/@DistroTube:2/nyxt-is-the-most-customizable-web:4

TITLE: Discover New Wallpapers And Set Them With Styli.sh 
=> https://odysee.com/@DistroTube:2/discover-new-wallpapers-and-set-them:9

TITLE: The Only Password Manager I Can Trust 
=> https://odysee.com/@DistroTube:2/the-only-password-manager-i-can-trust:c

TITLE: Hey, DT. That's Too Much Information! (clip taken from DT LIVE) 
=> https://odysee.com/@DistroTube:2/too-much-info-going-in-public:b

TITLE: Testing Audio And Internet At New Office (#shorts) 
=> https://odysee.com/@DistroTube:2/testing-audio-and-internet-at-new-office:e

TITLE: Special Live Event From The New Office - DT LIVE 
=> https://odysee.com/@DistroTube:2/special-live-event-from-the-new-office:4

TITLE: Using Htop To Monitor Your System 
=> https://odysee.com/@DistroTube:2/using-htop-to-monitor-your-system:f

TITLE: New Release of Archcraft OS. Will It Change My Mind? 
=> https://odysee.com/@DistroTube:2/new-release-of-archcraft-os.-will-it:d

TITLE: A Few Of My Favorite Things (Gemini, Mastodon, Odysee) 
=> https://odysee.com/@DistroTube:2/a-few-of-my-favorite-things-(gemini,:5

TITLE: Improve Efficiency In Chrome or Firefox With Vimium
=> https://odysee.com/@DistroTube:2/improve-efficiency-in-chrome-or-firefox:4

TITLE: Are "Mainstream" Linux Distros Better?
=> https://odysee.com/@DistroTube:2/are-mainstream-linux-distros-better:f

TITLE: Working At Home Sucks. Moving Into An Office.
=> https://odysee.com/@DistroTube:2/working-at-home-sucks.-moving-into-an:d

TITLE: My Thoughts On The Recent Crypto Crash
=> https://odysee.com/@DistroTube:2/my-thoughts-on-the-recent-crypto-crash:6

TITLE: If Someone Asks You About Linux, Tell Them...
=> https://odysee.com/@DistroTube:2/if-someone-asks-you-about-linux,-tell:4

TITLE: Nextcloud Installation Is A Snap
=> https://odysee.com/@DistroTube:2/nextcloud-installation-is-a-snap:0

TITLE: Nitrux Demands Public Apology From DistroWatch
=> https://odysee.com/@DistroTube:2/nitrux-demands-public-apology-from:f

TITLE: The AUR Removed My Packages, So I Created My Own Repo
=> https://odysee.com/@DistroTube:2/the-aur-removed-my-packages,-so-i:3

TITLE: "Hey, DT. FreeBSD Is Actually Unix!" (And Other Comments I Get)
=> https://odysee.com/@DistroTube:2/hey,-dt.-freebsd-is-actually-unix!-(and:d

TITLE: Nitrux Is An Impressive Linux Distribution
=> https://odysee.com/@DistroTube:2/nitrux-is-an-impressive-linux:4

TITLE: Termite Is Dead. Dev Lashes Out At GNOME.
=> https://odysee.com/@DistroTube:2/termite-is-dead.-dev-lashes-out-at:b

TITLE: Create Portable Packages With AppImage
=> https://odysee.com/@DistroTube:2/create-portable-packages-with-appimage:c

TITLE: Pacstall Is An "AUR" For Ubuntu
=> https://odysee.com/@DistroTube:2/pacstall-is-an-aur-for-ubuntu:8

TITLE: Easily Customize DWM With Flexipatch
=> https://odysee.com/@DistroTube:2/easily-customize-dwm-with-flexipatch:1

TITLE: Rice Your Terminal With Fetch Master 6000
=> https://odysee.com/@DistroTube:2/rice-your-terminal-with-fetch-master:d

TITLE: My First Look At Fedora 34 (and GNOME 40)
=> https://odysee.com/@DistroTube:2/my-first-look-at-fedora-34-(and-gnome:7

TITLE: Working On (Breaking Some) Stuff - DT LIVE
=> https://odysee.com/@DistroTube:2/working-on-(breaking-some)-stuff-dt-live:b

TITLE: I Need Ambient Noise At My Workstation
=> https://odysee.com/@DistroTube:2/i-need-ambient-noise-at-my-workstation:9

TITLE: Being "Sick" Doesn't Mean The "C" Word #shorts
=> https://odysee.com/@DistroTube:2/being-sick-doesn't-mean-the-c-word:0

TITLE: Fix Your Shell Scripts With Shellcheck
=> https://odysee.com/@DistroTube:2/fix-your-shell-scripts-with-shellcheck:1

TITLE: Chat With Patrons (April 25, 2021)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-(april-25,-2021):b

TITLE: Keep Politics Out Of Free And Open Source Software 
=> https://odysee.com/@DistroTube:2/keep-politics-out-of-free-and-open:e 

TITLE: Looking At Six Flavors Of Ubuntu 21.04 "Hirsute Hippo" 
=> https://odysee.com/@DistroTube:2/looking-at-six-flavors-of-ubuntu-21.04:6 

TITLE: XMonad, You're Simply The Best! 
=> https://odysee.com/@DistroTube:2/xmonad,-you're-simply-the-best!:a 

TITLE: Want To Be Like DT? Install Shell-Color-Scripts And Dmscripts! 
=> https://odysee.com/@DistroTube:2/want-to-be-like-dt-install-shell-color:8 

TITLE: A Quick Installation Of FreeBSD 13.0
=> https://odysee.com/@DistroTube:2/a-quick-installation-of-freebsd-13.0:3

TITLE: Leaving Doom Emacs For GNU Emacs? - DT Live!
=> https://odysee.com/@DistroTube:2/leaving-doom-emacs-for-gnu-emacs-dt:7

TITLE: The Linux Community Is Real. Linux UNITY Is A Myth.
=> https://odysee.com/@DistroTube:2/the-linux-community-is-real.-linux-unity:c

TITLE: Create Beautiful Websites Using Emacs Org Mode
=> https://odysee.com/@DistroTube:2/create-beautiful-websites-using-emacs:7

TITLE: Neovide Is A Graphical Neovim Client Written In Rust
=> https://odysee.com/@DistroTube:2/neovide-is-a-graphical-neovim-client:c

TITLE: Friday Night Q&A - Ask DT Live!
=> https://odysee.com/@DistroTube:2/friday-night-q-a-ask-dt-live!:5

TITLE: How To Change Your Themes In Linux
=> https://odysee.com/@DistroTube:2/how-to-change-your-themes-in-linux:1

TITLE: "Hey, DT! Free Linux Distros Are Not Really Free." Plus More Comments.
=> https://odysee.com/@DistroTube:2/hey,-dt!-free-linux-distros-are-not:2

TITLE: My Personal Website Will Now Be A Gemini Capsule
=> https://odysee.com/@DistroTube:2/my-personal-website-will-now-be-a-gemini:3

TITLE: Unboxing The ZSA Moonlander Programmable Keyboard
=> https://odysee.com/@DistroTube:2/unboxing-the-zsa-moonlander-programmable:0

TITLE: Is The Best RSS Reader An Emacs Package?
=> https://odysee.com/@DistroTube:2/is-the-best-rss-reader-an-emacs-package:6

TITLE: The SEC Sues LBRY. Also, Follow Me On Odysee!
=> https://odysee.com/@DistroTube:2/the-sec-sues-lbry.-also,-follow-me-on:9

TITLE: A Quick Look At Manjaro 21.0 "Ornara" Xfce
=> https://odysee.com/@DistroTube:2/a-quick-look-at-manjaro-21.0-ornara-xfce:c

TITLE: GNOME Board Members Must Resign In Disgrace
=> https://odysee.com/@DistroTube:2/gnome-board-members-must-resign-in:c

TITLE: Mob Mentality Threatens The Free Software Movement
=> https://odysee.com/@DistroTube:2/mob-mentality-threatens-the-free:b

TITLE: Creating A Dmenu Script For Web Bookmarks And History
=> https://odysee.com/@DistroTube:2/creating-a-dmenu-script-for-web:b

TITLE: Want DT's Desktop? Deploy My Dotfiles! - DT LIVE
=> https://odysee.com/@DistroTube:2/want-dt's-desktop-deploy-my-dotfiles!-dt:1

TITLE: Stop Pretending "/bin/sh" Is A Real Shell #shorts
=> https://odysee.com/@DistroTube:2/stop-pretending-bin-sh-is-a-real-shell:5

TITLE: Fix Your Mouse Wheel Scrolling Speed With Imwheel
=> https://odysee.com/@DistroTube:2/fix-qutebrowser-mouse-scrolling-with:5

TITLE: Manage Your Virtual Machines With Cockpit
=> https://odysee.com/@DistroTube:2/manage-your-virtual-machines-with:0

TITLE: The MEME Live Stream - DT LIVE
=> https://odysee.com/@DistroTube:2/the-meme-live-stream-dt-live:3

TITLE: Resistance Is Futile, So I'm Back In Qtile
=> https://odysee.com/@DistroTube:2/resistance-is-futile,-so-i'm-back-in:5

TITLE: The Yakuake Drop-Down Terminal For KDE Plasma
=> https://odysee.com/@DistroTube:2/the-yakuale-drop-down-terminal-for-kde:d

TITLE: I See So Much Negativity In The World
=> https://odysee.com/@DistroTube:2/i-see-so-much-negativity-in-the-world:5

TITLE: Solve Problems With Shell Scripting And Dmenu
=> https://odysee.com/@DistroTube:2/solve-problems-with-shell-scripting-and:d

TITLE: "Hey, DT! How Do I Learn The Terminal?" (And Other Questions Answered)
=> https://odysee.com/@DistroTube:2/hey,-dt!-how-do-i-learn-the-terminal:d

TITLE: YouTube Channels I've Enjoyed Watching In 2021
=> https://odysee.com/@DistroTube:2/youtube-channels-i-ve-enjoyed-watching:a

TITLE: Installation And First Look At Mageia 8
=> https://odysee.com/@DistroTube:2/installation-and-first-look-at-mageia-8:2

TITLE: Five Tips For The Openbox Window Manager
=> https://odysee.com/@DistroTube:2/five-tips-for-the-openbox-window-manager:7

TITLE: The Right Wallpaper Is The Key To A Beautiful Desktop
=> https://odysee.com/@DistroTube:2/the-right-wallpaper-is-the-key-to-a:e

TITLE: The Latest Release Of 0 A.D. Looks Amazing!
=> https://odysee.com/@DistroTube:2/the-latest-release-of-0-a-d-looks-2:0

TITLE: Explaining Everything In My XMonad Config
=> https://odysee.com/@DistroTube:2/explaining-everything-in-my-xmonad:f

TITLE: Siduction Is The "Unstable" Debian-Based Linux Distro
=> https://odysee.com/@DistroTube:2/siduction-is-the-unstable-debian-based:4

TITLE: Read Manpages With Less, Bat, Vim or Neovim
=> https://odysee.com/@DistroTube:2/read-manpages-with-less-bat-vim-or:b

TITLE: Many Computer Users Never Run Updates
=> https://odysee.com/@DistroTube:2/many-computer-users-never-run-updates:8

TITLE: Ubuntu, Then Arch. It's The Road So Many Of Us Travel.
=> https://odysee.com/@DistroTube:2/ubuntu-then-arch-it-s-the-road-so-many:9

TITLE: Mailspring Is An Email Client For Windows, Mac And Linux
=> https://odysee.com/@DistroTube:2/mailspring-is-an-email-client-for:b

TITLE: Crypto Currency Rates From The Command Line
=> https://odysee.com/@DistroTube:2/crypto-currency-rates-from-the-command:e

TITLE: JingOS Is An Ipad Inspired Linux Distro
=> https://odysee.com/@DistroTube:2/jingos-is-an-ipad-inspired-linux-distro:1

TITLE: "Ethical" Software Threatens Free And Open Source Software
=> https://odysee.com/@DistroTube:2/ethical-software-threatens-free-and-open:b

TITLE: Some Additional Thoughts About Gemini And Amfora 
=> https://odysee.com/@DistroTube:2/some-additional-thoughts-about-gemini:4 

TITLE: LeftWM Is A Tiling Window Manager Written In Rust
=> https://odysee.com/@DistroTube:2/leftwm-is-a-tiling-window-manager:9

TITLE: Solus Continues To Impress Me With Each Release 
=> https://odysee.com/@DistroTube:2/solus-continues-to-impress-me-with-each:6

TITLE: The 100K Live Stream
=> https://odysee.com/@DistroTube:2/the-100k-live-stream:f

TITLE: Gemini Is What The Web Should Have Been
=> https://odysee.com/@DistroTube:2/gemini-is-what-the-web-should-have-been:9

TITLE: Read Your RSS Feeds With NewsFlash
=> https://odysee.com/@DistroTube:2/read-your-rss-feeds-with-newsflash:4

TITLE: How To Enable DRM Restricted Content In LibreWolf
=> https://odysee.com/@DistroTube:2/how-to-enable-drm-restricted-content-in:d

TITLE: Rust Programs Every Linux User Should Know About
=> https://odysee.com/@DistroTube:2/rust-programs-every-linux-user-should:d

TITLE: Tabliss Is A "New Tab" Plugin For Firefox and Chrome
=> https://odysee.com/@DistroTube:2/tabliss-is-a-new-tab-plugin-for-firefox:4

TITLE: Attention Arch Users! Replace 'Yay' With 'Paru'.
=> https://odysee.com/@DistroTube:2/attention-arch-users-replace-yay-with:b

TITLE: "Hey, DT. Why LibreOffice Instead Of OpenOffice?" Plus Other Questions.
=> https://odysee.com/@DistroTube:2/hey-dt-why-libreoffice-instead-of:3

TITLE: Configuring The Xmobar Panel In Xmonad
=> https://odysee.com/@DistroTube:2/configuring-the-xmobar-panel-in-xmonad:8

TITLE: A First Look At Manjaro Sway Edition
=> https://odysee.com/@DistroTube:2/a-first-look-at-manjaro-sway-edition:8

TITLE: LibreWolf Is A Web Browser For Privacy and Freedom
=> https://odysee.com/@DistroTube:2/librewolf-is-a-web-browser-for-privacy:9

TITLE: DistroToot Is Now Accepting New Members
=> https://odysee.com/@DistroTube:2/distrotoot-is-now-accepting-new-members:f

TITLE: Beginner's Guide To The Linux Terminal
=> https://odysee.com/@DistroTube:2/beginner-s-guide-to-the-linux-terminal:8

TITLE: Banned From Mastodon? #MeToo
=> https://odysee.com/@DistroTube:2/banned-from-mastodon-metoo:f

TITLE: Mozilla No Longer Supports A Free Internet
=> https://odysee.com/@DistroTube:2/mozilla-no-longer-supports-a-free:5

TITLE: Trading Stocks On Linux
=> https://odysee.com/@DistroTube:2/trading-stocks-on-linux:1

TITLE: The Kimafun Dual Wireless Microphone Set
=> https://odysee.com/@DistroTube:2/the-kimafun-dual-wireless-microphone-set:c

TITLE: Kitty Is A Fast And Feature Rich Terminal Emulator
=> https://odysee.com/@DistroTube:2/kitty-is-a-fast-and-feature-rich:e

TITLE: Installation and First Look of Deepin 20.1
=> https://odysee.com/@DistroTube:2/installation-and-first-look-of-deepin-20:6

TITLE: Good Riddance, 2020.  How You Doin', 2021?!  (DT LIVE)
=> https://odysee.com/@DistroTube:2/good-riddance-2020-how-you-doin-2021-dt:e

## Video Categories

=> ../videos/2017-videos.gmi Videos from 2017
=> ../videos/2018-videos.gmi Videos from 2018
=> ../videos/2019-videos.gmi Videos from 2019
=> ../videos/2020-videos.gmi Videos from 2020
=> ../videos/2021-videos.gmi Videos from 2021
