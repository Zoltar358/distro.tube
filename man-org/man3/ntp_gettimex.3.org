#+TITLE: Manpages - ntp_gettimex.3
#+DESCRIPTION: Linux manpage for ntp_gettimex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ntp_gettimex.3 is found in manpage for: [[../man3/ntp_gettime.3][man3/ntp_gettime.3]]