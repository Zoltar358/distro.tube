#+TITLE: Manpages - gnutls_sec_param_to_symmetric_bits.3
#+DESCRIPTION: Linux manpage for gnutls_sec_param_to_symmetric_bits.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_sec_param_to_symmetric_bits - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*unsigned int gnutls_sec_param_to_symmetric_bits(gnutls_sec_param_t
*/param/*);*

* ARGUMENTS
- gnutls_sec_param_t param :: is a security parameter

* DESCRIPTION
This function will return the number of bits that correspond to
symmetric cipher strength for the given security parameter.

* RETURNS
The number of bits, or (0).

* SINCE
3.3.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
