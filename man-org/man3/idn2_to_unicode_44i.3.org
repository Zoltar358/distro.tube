#+TITLE: Manpages - idn2_to_unicode_44i.3
#+DESCRIPTION: Linux manpage for idn2_to_unicode_44i.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idn2_to_unicode_44i - API function

* SYNOPSIS
*#include <idn2.h>*

*int idn2_to_unicode_44i(const uint32_t * */in/*, size_t */inlen/*,
uint32_t * */out/*, size_t * */outlen/*, int */flags/*);*

* ARGUMENTS
- const uint32_t * in :: Input array with UTF-32 code points.

- size_t inlen :: number of code points of input array

- uint32_t * out :: output array with UTF-32 code points.

- size_t * outlen :: on input, maximum size of output array with UTF-32
  code points, on exit, actual size of output array with UTF-32 code
  points.

- int flags :: Currently unused.

* DESCRIPTION
The ToUnicode operation takes a sequence of UTF-32 code points that make
up one domain label and returns a sequence of UTF-32 code points. If the
input sequence is a label in ACE form, then the result is an equivalent
internationalized label that is not in ACE form, otherwise the original
sequence is returned unaltered.

/output/ may be NULL to test lookup of /input/ without allocating
memory.

* SINCE
2.0.0

* SEE ALSO
The full documentation for *libidn2* is maintained as a Texinfo manual.
If the *info* and *libidn2* programs are properly installed at your
site, the command

#+begin_quote
  *info libidn2*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libidn/libidn2/manual/*
#+end_quote
