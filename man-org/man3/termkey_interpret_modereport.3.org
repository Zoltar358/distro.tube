#+TITLE: Manpages - termkey_interpret_modereport.3
#+DESCRIPTION: Linux manpage for termkey_interpret_modereport.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_interpret_modereport - interpret opaque mode report data

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  TermKeyResult termkey_interpret_modereport(TermKey *tk, const TermKeyKey *key, 
   int *initial, int *mode, int *value);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_interpret_modereport*() fills in variables in the passed
pointers according to the mode report event found in /key/. It should be
called if *termkey_getkey*(3) or similar have returned a key event with
the type of *TERMKEY_TYPE_MODEREPORT*.

Any pointer may instead be given as *NULL* to not return that value.

The /initial/ variable will be filled with 0 for an ANSI mode report, or
='?'= for a DEC mode report. The /mode/ variable will be filled with the
number of the mode, and /value/ will be filled with the value from the
report.

* RETURN VALUE
If passed a /key/ event of the type *TERMKEY_TYPE_MODEREPORT*, this
function will return *TERMKEY_RES_KEY* and will affect the variables
whose pointers were passed in, as described above.

For other event types it will return *TERMKEY_RES_NONE*, and its effects
on any variables whose pointers were passed in, are undefined.

* SEE ALSO
*termkey_waitkey*(3), *termkey_getkey*(3), *termkey*(7)
