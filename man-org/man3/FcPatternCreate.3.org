#+TITLE: Manpages - FcPatternCreate.3
#+DESCRIPTION: Linux manpage for FcPatternCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternCreate - Create a pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcPattern * FcPatternCreate (void*);*

* DESCRIPTION
Creates a pattern with no properties; used to build patterns from
scratch.
