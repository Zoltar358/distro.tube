#+TITLE: Manpages - zip_source_rollback_write.3
#+DESCRIPTION: Linux manpage for zip_source_rollback_write.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

reverts changes written to

restoring the data before

was called. Usually this removes temporary files or frees buffers.

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

was added in libzip 1.0.

and
