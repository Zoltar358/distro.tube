#+TITLE: Manpages - FcPatternGet.3
#+DESCRIPTION: Linux manpage for FcPatternGet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternGet - Return a value from a pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcResult FcPatternGet (FcPattern */p/*, const char **/object/*, int
*/id/*, FcValue **/v/*);*

* DESCRIPTION
Returns in /v/ the /id/'th value associated with the property /object/.
The value returned is not a copy, but rather refers to the data stored
within the pattern directly. Applications must not free this value.
