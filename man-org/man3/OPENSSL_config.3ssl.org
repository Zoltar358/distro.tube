#+TITLE: Manpages - OPENSSL_config.3ssl
#+DESCRIPTION: Linux manpage for OPENSSL_config.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
OPENSSL_config, OPENSSL_no_config - simple OpenSSL configuration
functions

* SYNOPSIS
#include <openssl/conf.h> #if OPENSSL_API_COMPAT < 0x10100000L void
OPENSSL_config(const char *appname); void OPENSSL_no_config(void);
#endif

* DESCRIPTION
*OPENSSL_config()* configures OpenSSL using the standard *openssl.cnf*
and reads from the application section *appname*. If *appname* is NULL
then the default section, *openssl_conf*, will be used. Errors are
silently ignored. Multiple calls have no effect.

*OPENSSL_no_config()* disables configuration. If called before
*OPENSSL_config()* no configuration takes place.

If the application is built with *OPENSSL_LOAD_CONF* defined, then a
call to *OpenSSL_add_all_algorithms()* will implicitly call
*OPENSSL_config()* first.

* NOTES
The *OPENSSL_config()* function is designed to be a very simple call it
and forget it function. It is however *much* better than nothing.
Applications which need finer control over their configuration
functionality should use the configuration functions such as
*CONF_modules_load()* directly. This function is deprecated and its use
should be avoided. Applications should instead call
*CONF_modules_load()* during initialization (that is before starting any
threads).

There are several reasons why calling the OpenSSL configuration routines
is advisable. For example, to load dynamic ENGINEs from shared libraries
(DSOs). However, very few applications currently support the control
interface and so very few can load and use dynamic ENGINEs. Equally in
future more sophisticated ENGINEs will require certain control
operations to customize them. If an application calls *OPENSSL_config()*
it doesn't need to know or care about ENGINE control operations because
they can be performed by editing a configuration file.

* ENVIRONMENT
- OPENSSL_CONF :: The path to the config file. Ignored in set-user-ID
  and set-group-ID programs.

* RETURN VALUES
Neither *OPENSSL_config()* nor *OPENSSL_no_config()* return a value.

* SEE ALSO
*config* (5), *CONF_modules_load_file* (3)

* HISTORY
The *OPENSSL_no_config()* and *OPENSSL_config()* functions were
deprecated in OpenSSL 1.1.0 by *OPENSSL_init_crypto()*.

* COPYRIGHT
Copyright 2004-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
