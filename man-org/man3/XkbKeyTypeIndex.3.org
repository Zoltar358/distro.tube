#+TITLE: Manpages - XkbKeyTypeIndex.3
#+DESCRIPTION: Linux manpage for XkbKeyTypeIndex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbKeyTypeIndex - Obtain the index of a key type or the pointer to a key
type

* SYNOPSIS
*int XkbKeyTypeIndex* *( XkbDescPtr */xkb/* ,* *KeyCode */keycode/* ,*
*int */group/* );*

* ARGUMENTS
- /- xkb/ :: Xkb description of interest

- /- keycode/ :: keycode of interest

- /- group/ :: group index

* DESCRIPTION
/XkbKeyTypeIndex/ computes an index into the /types/ vector of the
client map in /xkb/ from the given /keycode/ and /group/ index.
