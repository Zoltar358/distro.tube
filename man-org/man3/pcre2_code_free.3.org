#+TITLE: Manpages - pcre2_code_free.3
#+DESCRIPTION: Linux manpage for pcre2_code_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  void pcre2_code_free(pcre2_code *code);
#+end_example

* DESCRIPTION
If /code/ is NULL, this function does nothing. Otherwise, /code/ must
point to a compiled pattern. This function frees its memory, including
any memory used by the JIT compiler. If the compiled pattern was created
by a call to *pcre2_code_copy_with_tables()*, the memory for the
character tables is also freed.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
