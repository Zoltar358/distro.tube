#+TITLE: Manpages - XkbGetState.3
#+DESCRIPTION: Linux manpage for XkbGetState.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbGetState - Obtains the keyboard state

* SYNOPSIS
*Status XkbGetState* *( Display **/display/* ,* *unsigned int
*/device_spec/* ,* *XkbStatePtr */state_return/* );*

* ARGUMENTS
- /display/ :: connection to the X server

- /device_spec/ :: device ID, or XkbUseCoreKbd

- /state_return/ :: backfilled with Xkb state

* DESCRIPTION
The /XkbGetState/ function queries the server for the current keyboard
state, waits for a reply, and then backfills /state_return/ with the
results.

All group values are expressed as group indices in the range [0..3].
Modifiers and the compatibility modifier state values are expressed as
the bitwise union of the core X11 modifier masks. The pointer button
state is reported as in the core X11 protocol.
