#+TITLE: Manpages - netsnmp_config_api.3
#+DESCRIPTION: Linux manpage for netsnmp_config_api.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
register_config_handler, register_const_config_handler,
register_prenetsnmp_mib_handler, unregister_config_handler,
register_mib_handlers, unregister_all_config_handlers,
register_app_config_handler, register_app_prenetsnmp_mib_handler,
unregister_app_config_handler, read_configs, read_premib_configs,
read_config_print_usage, config_perror, config_pwarn -
netsnmp_config_api functions

* SYNOPSIS
*#include <net-snmp/config_api.h>*\\

** Config Handlers
*struct config_line **\\
* register_config_handler(const char **/filePrefix/*",*\\
* const char **/token/*,*\\
* void (**/parser/*)(const char *, char *),*\\
* void (**/releaser/*)(void),*\\
* const char **/usageLine/*);*

*struct config_line **\\
* register_const_config_handler(const char **/filePrefix/*",*\\
* const char **/token/*,*\\
* void (**/parser/*)(const char *, const char *),*\\
* void (**/releaser/*)(void),*\\
* const char **/usageLine/*);*

*struct config_line **\\
* register_prenetsnmp_mib_handler(const char **/filePrefix/*",*\\
* const char **/token/*,*\\
* void (**/parser/*)(const char *, char *),*\\
* void (**/releaser/*)(void),*\\
* const char **/usageLine/*);*

*void unregister_config_handler(const char **/filePrefix/*,*\\
* const char **/token/*);*

*void register_mib_handlers(void);*\\
*void unregister_all_config_handlers(void);*\\

** Application Handlers
*struct config_line **\\
* register_app_config_handler(const char **/token/*,*\\
* void (**/parser/*)(const char *, char *),*\\
* void (**/releaser/*)(void),*\\
* const char **/usageLine/*);*

*struct config_line **\\
* register_app_prenetsnmp_mib_handler(const char **/token/*,*\\
* void (**/parser/*)(const char *, char *),*\\
* void (**/releaser/*)(void),*\\
* const char **/usageLine/*);*

*void unregister_app_config_handler(const char **/token/*);*\\

** Reading Configuration Files
*void read_premib_configs(void);*\\
*void read_configs(void);*\\

** Help Strings and Errors
*void read_config_print_usage(char **/lead/*);*\\
*void config_pwarn(const char **/string/*);*\\
*void config_perror(const char **/string/*);*

* DESCRIPTION
The functions are a fairly extensible system of parsing various
configuration files at the run time of an application. The configuration
file flow is broken into the following phases:

#+begin_quote
  - 1. :: Registration of handlers.

  - 2. :: Reading of the configuration files for pre-MIB parsing
    requirements.

  - 3. :: Reading and parsing of the textual MIB files.

  - 4. :: Reading of the configuration files for configuration
    directives.

  - 5. :: Optionally re-reading the configuration files at a future
    date.
#+end_quote

The idea is that the calling application is able to register /handlers/
for certain /tokens/ specified in certain named /configuration files./
The *read_configs()* function can then be called to look for all
relevant configuration files, match the first word on each line against
the list of registered tokens and pass the remainder of the line to the
appropriate registered handler.

* REGISTERING A HANDLER
- *register_config_handler()* :: Registers a configuration handler
  routine, which should be called to process configuration directives
  starting with the specified token. For example:

#+begin_quote

  #+begin_quote
    register_config_handler("snmp", "exampleToken", example_handler,
    NULL, "ARG1 [ARG2]");
  #+end_quote
#+end_quote

#+begin_quote
  would register the *example_handler()* function so that it will get
  called every time the first word of a line in the /snmp.conf/
  configuration file(s) matches "exampleToken".\\
  Calling the appropriate handlers to process the configuration file
  directives is the responsibility of *read_configs()* (see below).
#+end_quote

- *register_const_config_handler()* :: Similar to the
  *register_config_handler()* function, but the parser routine is
  explicitly constrained to not modify the string being parsed.

- *register_prenetsnmp_mib_handler()* :: Similar to the
  *register_config_handler()* function, but the registered handler
  routine will be called /before/ the textual MIBs are read in. This is
  typically used for tokens that will affect the configuration of the
  MIB parser, and will normally only be used within the SNMP library
  itself.

- *register_mib_handlers()* :: Initialisation routine to register the
  internal SNMP library configuration handlers.

- *unregister_config_handler()* :: Removes the registered configuration
  handler for the specified /filePrefix/ and /token/.

- *unregister_all_config_handlers()* :: Removes all registered
  configuration handlers.

** Token Handlers
Handler functions should have the following signature:

#+begin_quote
  *void handler(const char **/token/*, char **/line/*);*\\
  or\\
  *void handler(const char **/token/*, const char **/line/*);* br (if
  registered using /register_const_config_handler/)
#+end_quote

The function will be called with two arguments, the first being the
token that triggered the call to this function (i.e. the token used when
registering the handler), and the second being the remainder of the
configuration file line (i.e. everything following the white space
following the matched token).

** Freeing Handlers
If the token handler function dynamically allocates resources when
processing a configuration entry, then these may need to be released
before re-reading the configuration files. If the fourth parameter (
/releaser/ ) passed to *register_config_handler* is non-NULL, then this
specifies a function to be called before re-reading the configuration
files. This function should free any resources allocated by the token
handler function and reset its notion of the configuration to its
default. The token handler function can then safely be called again. No
arguments are passed to the resource freeing handler.\\
Note that this function is not called when the handler is unregistered
individually (but /is/ called as part of
*unregister_all_config_handlers()* ).

** Application Handlers
- *register_app_config_handler()* :: 

- *register_app_prenetsnmp_mib_handler()* :: 

- *unregister_app_config_handler()* :: These functions are analagous to
  *register_config_handler()*, *register_prenetsnmp_mib_handler()* and
  *unregister_config_handler()* but do not require the file type
  argument (which is filled in by the application). It is intended that
  MIB modules written for the agent use these functions to allow the
  agent to have more control over which configuration files are read
  (typically the /snmpd.conf/ files).

* READING CONFIGURATION FILES
- *read_premib_configs()* :: 

- *read_configs()* :: These routines process the configuration files
  found in the configuration search path (see below). For each entry,
  the handler registered for that configuration token is called.

*read_premib_configs()* is run before the MIB files are read in, and
processes those configuration tokens registered using
*register_prenetsnmp_mib_handler()* (or
*register_app_prenetsnmp_mib_handler()* ). All other entries are
ignored.

*read_configs()* is run after the MIB files have been read in, and
processes those configuration tokens registered using
*register_config_handler()* (or *register_app_config_handler()* ). If it
encounters a configuration token for which no handler has been
registered (either pre- or post-mib), then it will display a warning
message, and continue processing with the next line of the configuration
file.

** Configuration Search Path
The configuration files to be read are found by searching a list of
configuration directories for appropriately named files. In each such
directory, the library will look for files named = snmp.conf=,
= snmp.local.conf=, / app/=.conf=, / app/=.local.conf=,\\
(where /app/ is the appication-specific filePrefix used to register
configuration handlers). It is not necessary for any or all of these
files to be present in each directory. Missing files will be silently
skipped.\\
The idea behind the two different suffixes is that the first file can be
shared (via rdist or an NFS mount) across a large number of machines and
the second file can be used to configure local settings for one
particular machine.

The default list of directories to search is = /etc/snmp=, followed by
= /usr/share/snmp=, followed by = /usr/lib/snmp=, followed by
= $HOME/.snmp=. This list can be changed by setting the environmental
variable /SNMPCONFPATH/ to be a (colon separated) list of directories to
search.\\

** init_snmp()
The normal mode of operation would be to register the
application-specific configuration handlers, and then invoke
*init_snmp()*. This would call the routines listed above to register the
internal library configuration handlers, process any configuration
tokens registered with *register_prenetsnmp_mib_handler(),* read in the
textual MIB files using *init_mib(),* and finally parse the
configuration file tokens registered with *register_config_handler()*.

If the *init_snmp()* function is used, none of these functions need to
be explicitly called by the application.

* HELP STRINGS AND ERRORS
The /usageLine/ parameter passed to *register_config_handler()* and
similar calls, is used to display help information when the
*read_config_print_usage()* function is called. This function is used by
all of the applications when the *-H* flag is passed on the command
line. It prints a summary of all of the configuration file lines, and
the associated files, that the configuration system understands. The
/usageLine/ parameter should be a list of arguments expected after the
token, and not a lengthy description (which should go into a manual page
instead). The /lead/ prefix will be prepended to each line that the
function prints to stderr, where it displays its output.

The *init_snmp()* function should be called before the
*read_config_print_usage()* function is called, so that the library can
register its configuration file directives as well for the
*read_config_print_usage()* function to display.

** Error Handling Functions
The two functions *config_pwarn()* and *config_perror()* both take an
error string as an argument and print it to stderr along with the file
and line number that caused the error. A call to the second function
will also force *read_configs()* to eventually return with an error code
indicating to it's calling function that it should abort the operation
of the application.

* ENVIRONMENT VARIABLES
- SNMPCONFPATH :: A colon separated list of directories to search for
  configuration files in. Default:\\
  /etc/snmp:/usr/share/snmp:/usr/lib/snmp:$HOME/.snmp

* SEE ALSO
netsnmp_mib_api(3), snmp_api(3)
