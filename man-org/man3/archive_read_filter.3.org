#+TITLE: Manpages - archive_read_filter.3
#+DESCRIPTION: Linux manpage for archive_read_filter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

Enables auto-detection code and decompression support for the specified
compression. These functions may fall back on external programs if an
appropriate library was not available at build time. Decompression using
an external program is usually slower than decompression through
built-in libraries. Note that

is always enabled by default.

Enables all available decompression filters.

Enables a single filter specified by the filter code. This function does
not work with

Note: In statically-linked executables, this will cause your program to
include support for every filter. If executable size is a concern, you
may wish to avoid using this function.

Data is fed through the specified external program before being
dearchived. Note that this disables automatic detection of the
compression format, so it makes no sense to specify this in conjunction
with any other decompression option.

This feeds data through the specified external program but only if the
initial bytes of the data match the specified signature value.

These functions return

if the compression is fully supported,

if the compression is supported only through an external program.

always succeeds.

Detailed error codes and textual descriptions are available from the

and

functions.
