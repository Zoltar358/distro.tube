#+TITLE: Manpages - acl_free.3
#+DESCRIPTION: Linux manpage for acl_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function frees any releasable memory currently allocated by to the ACL
data object identified by

The argument

may identify an ACL, an ACL entry qualifier, or a pointer to a string
allocated by the

function.

If any of the following conditions occur, the

function returns the value

and and sets

to the corresponding value:

The value of the argument

is invalid.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
