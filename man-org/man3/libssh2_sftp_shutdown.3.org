#+TITLE: Manpages - libssh2_sftp_shutdown.3
#+DESCRIPTION: Linux manpage for libssh2_sftp_shutdown.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_sftp_shutdown - shut down an SFTP session

* SYNOPSIS
#include <libssh2.h> #include <libssh2_sftp.h>

int libssh2_sftp_shutdown(LIBSSH2_SFTP *sftp);

* DESCRIPTION
/sftp/ - SFTP instance as returned by *libssh2_sftp_init(3)*

Destroys a previously initialized SFTP session and frees all resources
associated with it.

* RETURN VALUE
Return 0 on success or negative on failure. It returns
LIBSSH2_ERROR_EAGAIN when it would otherwise block. While
LIBSSH2_ERROR_EAGAIN is a negative number, it isn't really a failure per
se.

* SEE ALSO
*libssh2_sftp_init(3)*
