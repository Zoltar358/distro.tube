#+TITLE: Manpages - std_priority_queue.3
#+DESCRIPTION: Linux manpage for std_priority_queue.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::priority_queue< _Tp, _Sequence, _Compare > - A standard container
automatically sorting its contents.

* SYNOPSIS
\\

=#include <stl_queue.h>=

** Public Types
typedef _Sequence::const_reference *const_reference*\\

typedef _Sequence *container_type*\\

typedef _Sequence::reference *reference*\\

typedef _Sequence::size_type *size_type*\\

typedef _Compare *value_compare*\\

typedef _Sequence::value_type *value_type*\\

** Public Member Functions
template<typename _Seq = _Sequence, typename _Requires = typename
enable_if<__and_<is_default_constructible<_Compare>,
is_default_constructible<_Seq>>::value>::type> *priority_queue* ()\\
Default constructor creates no elements.

template<typename _InputIterator > *priority_queue* (_InputIterator
__first, _InputIterator __last, const _Compare &__x, const _Sequence
&__s)\\
Builds a queue from a range.

template<typename _InputIterator > *priority_queue* (_InputIterator
__first, _InputIterator __last, const _Compare &__x=_Compare(),
_Sequence &&__s=_Sequence())\\

template<typename _Alloc , typename _Requires = _Uses<_Alloc>>
*priority_queue* (const _Alloc &__a)\\

template<typename _Alloc , typename _Requires = _Uses<_Alloc>>
*priority_queue* (const _Compare &__x, _Sequence &&__c, const _Alloc
&__a)\\

*priority_queue* (const _Compare &__x, _Sequence &&__s=_Sequence())\\

template<typename _Alloc , typename _Requires = _Uses<_Alloc>>
*priority_queue* (const _Compare &__x, const _Alloc &__a)\\

template<typename _Alloc , typename _Requires = _Uses<_Alloc>>
*priority_queue* (const _Compare &__x, const _Sequence &__c, const
_Alloc &__a)\\

*priority_queue* (const _Compare &__x, const _Sequence &__s)\\

template<typename _Alloc , typename _Requires = _Uses<_Alloc>>
*priority_queue* (const *priority_queue* &__q, const _Alloc &__a)\\

template<typename _Alloc , typename _Requires = _Uses<_Alloc>>
*priority_queue* (*priority_queue* &&__q, const _Alloc &__a)\\

template<typename... _Args> void *emplace* (_Args &&... __args)\\

bool *empty* () const\\

void *pop* ()\\
Removes first element.

void *push* (const value_type &__x)\\
Add data to the queue.

void *push* (value_type &&__x)\\

size_type *size* () const\\

void *swap* (*priority_queue* &__pq) noexcept(__and_<//c++1z or gnu++11
__is_nothrow_swappable< _Sequence >, __is_nothrow_swappable< _Compare >
>::value)\\

const_reference *top* () const\\

** Protected Attributes
_Sequence *c*\\

_Compare *comp*\\

* Detailed Description
** "template<typename _Tp, typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>>
\\
class std::priority_queue< _Tp, _Sequence, _Compare >"A standard
container automatically sorting its contents.

*Template Parameters*

#+begin_quote
  /_Tp/ Type of element.\\
  /_Sequence/ Type of underlying sequence, defaults to vector<_Tp>.\\
  /_Compare/ Comparison function object type, defaults to
  less<_Sequence::value_type>.
#+end_quote

This is not a true container, but an /adaptor/. It holds another
container, and provides a wrapper interface to that container. The
wrapper is what enforces priority-based sorting and queue behavior. Very
few of the standard container/sequence interface requirements are met
(e.g., iterators).

The second template parameter defines the type of the underlying
sequence/container. It defaults to std::vector, but it can be any type
that supports =front()=, =push_back=, =pop_back=, and random-access
iterators, such as std::deque or an appropriate user-defined type.

The third template parameter supplies the means of making priority
comparisons. It defaults to =less<value_type>= but can be anything
defining a strict weak ordering.

Members not found in /normal/ containers are =container_type=, which is
a typedef for the second Sequence parameter, and =push=, =pop=, and
=top=, which are standard queue operations.

*Note*

#+begin_quote
  No equality/comparison operators are provided for priority_queue.

  Sorting of the elements takes place as they are added to, and removed
  from, the priority_queue using the priority_queue's member functions.
  If you access the elements by other means, and change their data such
  that the sorting order would be different, the priority_queue will not
  re-sort the elements for you. (How could it know to do so?)
#+end_quote

Definition at line *456* of file *stl_queue.h*.

* Member Typedef Documentation
** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> typedef
_Sequence::const_reference *std::priority_queue*< _Tp, _Sequence,
_Compare >::const_reference
Definition at line *488* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> typedef _Sequence
*std::priority_queue*< _Tp, _Sequence, _Compare >::container_type
Definition at line *490* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> typedef
_Sequence::reference *std::priority_queue*< _Tp, _Sequence, _Compare
>::reference
Definition at line *487* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> typedef
_Sequence::size_type *std::priority_queue*< _Tp, _Sequence, _Compare
>::size_type
Definition at line *489* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> typedef _Compare
*std::priority_queue*< _Tp, _Sequence, _Compare >::value_compare
Definition at line *493* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> typedef
_Sequence::value_type *std::priority_queue*< _Tp, _Sequence, _Compare
>::value_type
Definition at line *486* of file *stl_queue.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename _Seq
= _Sequence, typename _Requires = typename
enable_if<__and_<is_default_constructible<_Compare>,
is_default_constructible<_Seq>>::value>::type> *std::priority_queue*<
_Tp, _Sequence, _Compare >::*priority_queue* ()= [inline]=
Default constructor creates no elements.

Definition at line *514* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> *std::priority_queue*<
_Tp, _Sequence, _Compare >::*priority_queue* (const _Compare & __x,
const _Sequence & __s)= [inline]=, = [explicit]=
Definition at line *518* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> *std::priority_queue*<
_Tp, _Sequence, _Compare >::*priority_queue* (const _Compare & __x,
_Sequence && __s = =_Sequence()=)= [inline]=, = [explicit]=
Definition at line *523* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_Alloc , typename _Requires = _Uses<_Alloc>> *std::priority_queue*< _Tp,
_Sequence, _Compare >::*priority_queue* (const _Alloc & __a)= [inline]=,
= [explicit]=
Definition at line *529* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_Alloc , typename _Requires = _Uses<_Alloc>> *std::priority_queue*< _Tp,
_Sequence, _Compare >::*priority_queue* (const _Compare & __x, const
_Alloc & __a)= [inline]=
Definition at line *533* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_Alloc , typename _Requires = _Uses<_Alloc>> *std::priority_queue*< _Tp,
_Sequence, _Compare >::*priority_queue* (const _Compare & __x, const
_Sequence & __c, const _Alloc & __a)= [inline]=
Definition at line *539* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_Alloc , typename _Requires = _Uses<_Alloc>> *std::priority_queue*< _Tp,
_Sequence, _Compare >::*priority_queue* (const _Compare & __x, _Sequence
&& __c, const _Alloc & __a)= [inline]=
Definition at line *545* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_Alloc , typename _Requires = _Uses<_Alloc>> *std::priority_queue*< _Tp,
_Sequence, _Compare >::*priority_queue* (const *priority_queue*< _Tp,
_Sequence, _Compare > & __q, const _Alloc & __a)= [inline]=
Definition at line *550* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_Alloc , typename _Requires = _Uses<_Alloc>> *std::priority_queue*< _Tp,
_Sequence, _Compare >::*priority_queue* (*priority_queue*< _Tp,
_Sequence, _Compare > && __q, const _Alloc & __a)= [inline]=
Definition at line *554* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_InputIterator > *std::priority_queue*< _Tp, _Sequence, _Compare
>::*priority_queue* (_InputIterator __first, _InputIterator __last,
const _Compare & __x, const _Sequence & __s)= [inline]=
Builds a queue from a range.

*Parameters*

#+begin_quote
  /__first/ An input iterator.\\
  /__last/ An input iterator.\\
  /__x/ A comparison functor describing a strict weak ordering.\\
  /__s/ An initial sequence with which to start.
#+end_quote

Begins by copying /__s/, inserting a copy of the elements from
[first,last) into the copy of /__s/, then ordering the copy according to
/__x/.

For more information on function objects, see the documentation on
*functor base classes*.

Definition at line *586* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename
_InputIterator > *std::priority_queue*< _Tp, _Sequence, _Compare
>::*priority_queue* (_InputIterator __first, _InputIterator __last,
const _Compare & __x = =_Compare()=, _Sequence && __s =
=_Sequence()=)= [inline]=
Definition at line *597* of file *stl_queue.h*.

* Member Function Documentation
** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> template<typename...
_Args> void *std::priority_queue*< _Tp, _Sequence, _Compare >::emplace
(_Args &&... __args)= [inline]=
Definition at line *656* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> bool
*std::priority_queue*< _Tp, _Sequence, _Compare >::empty ()
const= [inline]=
Returns true if the queue is empty.

Definition at line *612* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> void
*std::priority_queue*< _Tp, _Sequence, _Compare >::pop ()= [inline]=
Removes first element. This is a typical queue operation. It shrinks the
queue by one. The time complexity of the operation depends on the
underlying sequence.

Note that no data is returned, and if the first element's data is
needed, it should be retrieved before pop() is called.

Definition at line *675* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> void
*std::priority_queue*< _Tp, _Sequence, _Compare >::push (const
value_type & __x)= [inline]=
Add data to the queue.

*Parameters*

#+begin_quote
  /__x/ Data to be added.
#+end_quote

This is a typical queue operation. The time complexity of the operation
depends on the underlying sequence.

Definition at line *640* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> void
*std::priority_queue*< _Tp, _Sequence, _Compare >::push (value_type &&
__x)= [inline]=
Definition at line *648* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> size_type
*std::priority_queue*< _Tp, _Sequence, _Compare >::size ()
const= [inline]=
Returns the number of elements in the queue.\\

Definition at line *617* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> void
*std::priority_queue*< _Tp, _Sequence, _Compare >::swap
(*priority_queue*< _Tp, _Sequence, _Compare > & __pq)= [inline]=,
= [noexcept]=
Definition at line *684* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> const_reference
*std::priority_queue*< _Tp, _Sequence, _Compare >::top ()
const= [inline]=
Returns a read-only (constant) reference to the data at the first
element of the queue.

Definition at line *625* of file *stl_queue.h*.

* Member Data Documentation
** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> _Sequence
*std::priority_queue*< _Tp, _Sequence, _Compare >::c= [protected]=
Definition at line *497* of file *stl_queue.h*.

** template<typename _Tp , typename _Sequence = vector<_Tp>, typename
_Compare = less<typename _Sequence::value_type>> _Compare
*std::priority_queue*< _Tp, _Sequence, _Compare >::comp= [protected]=
Definition at line *498* of file *stl_queue.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
