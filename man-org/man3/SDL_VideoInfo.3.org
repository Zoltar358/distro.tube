#+TITLE: Manpages - SDL_VideoInfo.3
#+DESCRIPTION: Linux manpage for SDL_VideoInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_VideoInfo - Video Target information

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    Uint32 hw_available:1;
    Uint32 wm_available:1;
    Uint32 blit_hw:1;
    Uint32 blit_hw_CC:1;
    Uint32 blit_hw_A:1;
    Uint32 blit_sw:1;
    Uint32 blit_sw_CC:1;
    Uint32 blit_sw_A:1;
    Uint32 blit_fill;
    Uint32 video_mem;
    SDL_PixelFormat *vfmt;
  } SDL_VideoInfo;
#+end_example

* STRUCTURE DATA
- *hw_available* :: Is it possible to create hardware surfaces?

- *wm_available* :: Is there a window manager available

- *blit_hw* :: Are hardware to hardware blits accelerated?

- *blit_hw_CC* :: Are hardware to hardware colorkey blits accelerated?

- *blit_hw_A* :: Are hardware to hardware alpha blits accelerated?

- *blit_sw* :: Are software to hardware blits accelerated?

- *blit_sw_CC* :: Are software to hardware colorkey blits accelerated?

- *blit_sw_A* :: Are software to hardware alpha blits accelerated?

- *blit_fill* :: Are color fills accelerated?

- *video_mem* :: Total amount of video memory in Kilobytes

- *vfmt* :: /Pixel format/ of the video device

* DESCRIPTION
This (read-only) structure is returned by *SDL_GetVideoInfo*. It
contains information on either the 'best' available mode (if called
before *SDL_SetVideoMode*) or the current video mode.

* SEE ALSO
*SDL_PixelFormat*, *SDL_GetVideoInfo*
