#+TITLE: Manpages - SDL_GetGamma.3
#+DESCRIPTION: Linux manpage for SDL_GetGamma.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetGamma - Gets the gamma of the display

* SYNOPSIS
*#include "SDL.h"*

*int SDL_GetGamma*(*float *red, float *green, float *blue*);

* DESCRIPTION
Gets the color gamma of the display. The gamma value for each color
component will be place in the parameters *red*, *green* and *blue*. The
values can range from 0.1 to 10.

#+begin_quote
  *Note: *

  This function currently only works on XFreee 4.0 and up.
#+end_quote

* SEE ALSO
*SDL_SetGamma*, *SDL_SetVideoMode*
