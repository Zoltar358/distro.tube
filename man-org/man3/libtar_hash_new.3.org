#+TITLE: Manpages - libtar_hash_new.3
#+DESCRIPTION: Linux manpage for libtar_hash_new.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libtar_hash_new, libtar_hash_free, libtar_hash_next, libtar_hash_prev,
libtar_hash_getkey, libtar_hash_search, libtar_hash_add,
libtar_hash_del - hash table routines

* SYNOPSIS
*#include <libtar.h>*

*libtar_hash_t *libtar_hash_new(int */num/*, int (**/hashfunc/*)());*\\
*void libtar_hash_free(libtar_hash_t **/h/*, void (**/freefunc/*)());*\\
*int libtar_hash_next(libtar_hash_t **/h/*, libtar_hashptr_t
**/hp/*);*\\
*int libtar_hash_prev(libtar_hash_t **/h/*, libtar_hashptr_t
**/hp/*);*\\
*int libtar_hash_search(libtar_hash_t **/h/*, libtar_hashptr_t **/hp/*,*
*void **/data/*, int (**/matchfunc/*)());*\\
*int libtar_hash_getkey(libtar_hash_t **/h/*, libtar_hashptr_t **/hp/*,*
*void **/data/*, int (**/matchfunc/*)());*\\
*int libtar_hash_add(libtar_hash_t **/h/*, void **/data/*);*\\
*int libtar_hash_del(libtar_hash_t **/h/*, libtar_hashptr_t **/hp/*);*

* DESCRIPTION
The *libtar_hash_new*() function creates a new hash with /num/ buckets
and using hash function pointed to by /hashfunc/. If /hashfunc/ is
/NULL/, a default hash function designed for 7-bit ASCII strings is
used.

The *libtar_hash_free*() function deallocates all memory associated with
the hash structure /h/. If /freefunc/ is not /NULL/, it is called to
free memory associated with each node in the hash.

The *libtar_hash_next*() and *libtar_hash_prev*() functions are used to
iterate through the hash. The /libtar_hashptr_t/ structure has two
fields: /bucket/, which indicates the current bucket in the hash, and
/node/, which is a pointer to the current node in the current bucket. To
start at the beginning or end of the hash, the caller should initialize
/hp.bucket/ to -1 and /hp.node/ to /NULL/.

The *libtar_hash_search*() function searches iteratively through the
hash /h/ until it finds a node whose contents match /data/ using the
matching function /matchfunc/. Searching begins at the location pointed
to by /hp/.

The *libtar_hash_getkey*() function uses the hash function associated
with /h/ to determine which bucket /data/ should be in, and searches
only that bucket for a matching node using /matchfunc/. Searching begins
at the location pointed to by /hp/.

The *libtar_hash_add*() function adds /data/ into hash /h/.

The *libtar_hash_del*() function removes the node referenced by /hp/.

* RETURN VALUE
The *libtar_hash_new*() function returns a pointer to the new hash
structure, or /NULL/ on error.

The *libtar_hash_next*() and *libtar_hash_prev*() functions return 1
when valid data is returned, and 0 at the end of the hash.

The *libtar_hash_getkey*() and *libtar_hash_search*() functions return 1
when a match is found, or 0 otherwise.

The *libtar_hash_add*() function returns 0 on success, or -1 on error
(and sets /errno/).

The *libtar_hash_del*() function returns 0 on success, or -1 on error
(and sets /errno/).

* SEE ALSO
*libtar_list_new*(3)
