#+TITLE: Manpages - std__Node_insert_return.3
#+DESCRIPTION: Linux manpage for std__Node_insert_return.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Node_insert_return< _Iterator, _NodeHandle > - Return type of
insert(node_handle&&) on unique maps/sets.

* SYNOPSIS
\\

=#include <node_handle.h>=

** Public Attributes
bool *inserted*\\

_NodeHandle *node*\\

_Iterator *position*\\

* Detailed Description
** "template<typename _Iterator, typename _NodeHandle>
\\
struct std::_Node_insert_return< _Iterator, _NodeHandle >"Return type of
insert(node_handle&&) on unique maps/sets.

Definition at line *363* of file *node_handle.h*.

* Member Data Documentation
** template<typename _Iterator , typename _NodeHandle > bool
*std::_Node_insert_return*< _Iterator, _NodeHandle >::inserted
Definition at line *366* of file *node_handle.h*.

** template<typename _Iterator , typename _NodeHandle > _NodeHandle
*std::_Node_insert_return*< _Iterator, _NodeHandle >::node
Definition at line *367* of file *node_handle.h*.

** template<typename _Iterator , typename _NodeHandle > _Iterator
*std::_Node_insert_return*< _Iterator, _NodeHandle >::position
Definition at line *365* of file *node_handle.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
