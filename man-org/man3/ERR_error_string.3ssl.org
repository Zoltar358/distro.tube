#+TITLE: Manpages - ERR_error_string.3ssl
#+DESCRIPTION: Linux manpage for ERR_error_string.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ERR_error_string, ERR_error_string_n, ERR_lib_error_string,
ERR_func_error_string, ERR_reason_error_string - obtain human-readable
error message

* SYNOPSIS
#include <openssl/err.h> char *ERR_error_string(unsigned long e, char
*buf); void ERR_error_string_n(unsigned long e, char *buf, size_t len);
const char *ERR_lib_error_string(unsigned long e); const char
*ERR_func_error_string(unsigned long e); const char
*ERR_reason_error_string(unsigned long e);

* DESCRIPTION
*ERR_error_string()* generates a human-readable string representing the
error code /e/, and places it at /buf/. /buf/ must be at least 256 bytes
long. If /buf/ is *NULL*, the error string is placed in a static buffer.
Note that this function is not thread-safe and does no checks on the
size of the buffer; use *ERR_error_string_n()* instead.

*ERR_error_string_n()* is a variant of *ERR_error_string()* that writes
at most /len/ characters (including the terminating 0) and truncates the
string if necessary. For *ERR_error_string_n()*, /buf/ may not be
*NULL*.

The string will have the following format:

error:[error code]:[library name]:[function name]:[reason string]

/error code/ is an 8 digit hexadecimal number, /library name/, /function
name/ and /reason string/ are ASCII text.

*ERR_lib_error_string()*, *ERR_func_error_string()* and
*ERR_reason_error_string()* return the library name, function name and
reason string respectively.

If there is no text string registered for the given error code, the
error string will contain the numeric code.

*ERR_print_errors* (3) can be used to print all error codes currently in
the queue.

* RETURN VALUES
*ERR_error_string()* returns a pointer to a static buffer containing the
string if /buf/ *== NULL*, /buf/ otherwise.

*ERR_lib_error_string()*, *ERR_func_error_string()* and
*ERR_reason_error_string()* return the strings, and *NULL* if none is
registered for the error code.

* SEE ALSO
*ERR_get_error* (3), *ERR_print_errors* (3)

* COPYRIGHT
Copyright 2000-2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
