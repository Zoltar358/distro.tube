#+TITLE: Manpages - Test2_EventFacet_Assert.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet_Assert.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet::Assert - Facet representing an assertion.

* DESCRIPTION
The assertion facet is provided by any event representing an assertion
that was made.

* FIELDS
- $string = $assert->{details} :: 

- $string = $assert->details() :: 

Human readable description of the assertion.

- $bool = $assert->{pass} :: 

- $bool = $assert->pass() :: 

True if the assertion passed.

- $bool = $assert->{no_debug} :: 

- $bool = $assert->no_debug() :: 

Set this to true if you have provided custom diagnostics and do not want
the defaults to be displayed.

- $int = $assert->{number} :: 

- $int = $assert->number() :: 

(Optional) assertion number. This may be omitted or ignored. This is
usually only useful when parsing/processing TAP. *Note*: This is not set
by the Test2 system, assertion number is not known until AFTER the
assertion has been processed. This attribute is part of the spec only
for harnesses.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
