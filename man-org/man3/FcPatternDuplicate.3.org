#+TITLE: Manpages - FcPatternDuplicate.3
#+DESCRIPTION: Linux manpage for FcPatternDuplicate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternDuplicate - Copy a pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcPattern * FcPatternDuplicate (const FcPattern */p/*);*

* DESCRIPTION
Copy a pattern, returning a new pattern that matches /p/. Each pattern
may be modified without affecting the other.
