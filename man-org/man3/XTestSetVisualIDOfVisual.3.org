#+TITLE: Manpages - XTestSetVisualIDOfVisual.3
#+DESCRIPTION: Linux manpage for XTestSetVisualIDOfVisual.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XTestSetVisualIDOfVisual.3 is found in manpage for: [[../XTestQueryExtension.3][XTestQueryExtension.3]]