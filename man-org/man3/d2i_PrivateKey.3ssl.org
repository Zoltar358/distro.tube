#+TITLE: Manpages - d2i_PrivateKey.3ssl
#+DESCRIPTION: Linux manpage for d2i_PrivateKey.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
d2i_PrivateKey, d2i_PublicKey, d2i_AutoPrivateKey, i2d_PrivateKey,
i2d_PublicKey, d2i_PrivateKey_bio, d2i_PrivateKey_fp - decode and encode
functions for reading and saving EVP_PKEY structures

* SYNOPSIS
#include <openssl/evp.h> EVP_PKEY *d2i_PrivateKey(int type, EVP_PKEY
**a, const unsigned char **pp, long length); EVP_PKEY *d2i_PublicKey(int
type, EVP_PKEY **a, const unsigned char **pp, long length); EVP_PKEY
*d2i_AutoPrivateKey(EVP_PKEY **a, const unsigned char **pp, long
length); int i2d_PrivateKey(EVP_PKEY *a, unsigned char **pp); int
i2d_PublicKey(EVP_PKEY *a, unsigned char **pp); EVP_PKEY
*d2i_PrivateKey_bio(BIO *bp, EVP_PKEY **a); EVP_PKEY
*d2i_PrivateKey_fp(FILE *fp, EVP_PKEY **a)

* DESCRIPTION
*d2i_PrivateKey()* decodes a private key using algorithm *type*. It
attempts to use any key specific format or PKCS#8 unencrypted
PrivateKeyInfo format. The *type* parameter should be a public key
algorithm constant such as *EVP_PKEY_RSA*. An error occurs if the
decoded key does not match *type*. *d2i_PublicKey()* does the same for
public keys.

*d2i_AutoPrivateKey()* is similar to *d2i_PrivateKey()* except it
attempts to automatically detect the private key format.

*i2d_PrivateKey()* encodes *key*. It uses a key specific format or, if
none is defined for that key type, PKCS#8 unencrypted PrivateKeyInfo
format. *i2d_PublicKey()* does the same for public keys.

These functions are similar to the *d2i_X509()* functions; see
*d2i_X509* (3).

* NOTES
All the functions that operate on data in memory update the data pointer
/*pp/ after a successful operation, just like the other d2i and i2d
functions; see *d2i_X509* (3).

All these functions use DER format and unencrypted keys. Applications
wishing to encrypt or decrypt private keys should use other functions
such as *d2i_PKCS8PrivateKey()* instead.

If the **a* is not NULL when calling *d2i_PrivateKey()* or
*d2i_AutoPrivateKey()* (i.e. an existing structure is being reused) and
the key format is PKCS#8 then **a* will be freed and replaced on a
successful call.

To decode a key with type *EVP_PKEY_EC*, *d2i_PublicKey()* requires **a*
to be a non-NULL EVP_PKEY structure assigned an EC_KEY structure
referencing the proper EC_GROUP.

* RETURN VALUES
The *d2i_PrivateKey()*, *d2i_AutoPrivateKey()*, *d2i_PrivateKey_bio()*,
*d2i_PrivateKey_fp()*, and *d2i_PublicKey()* functions return a valid
*EVP_KEY* structure or *NULL* if an error occurs. The error code can be
obtained by calling *ERR_get_error* (3).

*i2d_PrivateKey()* and *i2d_PublicKey()* return the number of bytes
successfully encoded or a negative value if an error occurs. The error
code can be obtained by calling *ERR_get_error* (3).

* SEE ALSO
*crypto* (7), *d2i_PKCS8PrivateKey_bio* (3)

* COPYRIGHT
Copyright 2017-2021 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
