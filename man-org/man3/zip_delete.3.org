#+TITLE: Manpages - zip_delete.3
#+DESCRIPTION: Linux manpage for zip_delete.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The file at position

in the zip archive

is marked as deleted.

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error code in

is set to indicate the error.

fails if:

is not a valid file index in

was added in libzip 0.6. In libzip 0.10 the type of

was changed from

to

and
