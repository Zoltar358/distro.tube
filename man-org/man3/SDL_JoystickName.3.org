#+TITLE: Manpages - SDL_JoystickName.3
#+DESCRIPTION: Linux manpage for SDL_JoystickName.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickName - Get joystick name.

* SYNOPSIS
*#include "SDL.h"*

*const char *SDL_JoystickName*(*int index*);

* DESCRIPTION
Get the implementation dependent name of joystick. The *index* parameter
refers to the N'th joystick on the system.

* RETURN VALUE
Returns a char pointer to the joystick name.

* EXAMPLES
#+begin_example
  /* Print the names of all attached joysticks */
  int num_joy, i;
  num_joy=SDL_NumJoysticks();
  printf("%d joysticks found
  ", num_joy);
  for(i=0;i<num_joy;i++)
    printf("%s
  ", SDL_JoystickName(i));
#+end_example

* SEE ALSO
*SDL_JoystickOpen*
