#+TITLE: Manpages - errc.3bsd
#+DESCRIPTION: Linux manpage for errc.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

and

family of functions display a formatted error message on the standard
error output. In all cases, the last component of the program name,
followed by a colon

character and a space, are output. The text that follows depends on the
function being called. The

specification (and associated arguments) may be any format allowed by

or

If the

argument is not

the formatted error message is output.

The functions all output an error message string affiliated with an
error value (see

preceded by a colon character and a space if

is not

That is, the output is as follows:

progname: fmt: error message string

if

is not

or:

progname: error message string

if it is.

The argument

is used as the error value instead of the current value of the global
variable

In all cases, the output is followed by a newline character.

The

and

functions do not return, but exit with the value of the argument

Display the current

information string and exit:

if ((p = malloc(size)) == NULL) err(1, NULL); if ((fd = open(file_name,
O_RDONLY, 0)) == -1) err(1, "%s", file_name);

Display an error message and exit:

if (tm.tm_hour < START_TIME) errx(1, "too early, wait until %s",
start_time_string);

Warn of an error:

if ((fd = open(raw_device, O_RDONLY, 0)) == -1) warnx("%s: %s: trying
the block device", raw_device, strerror(errno)); if ((fd =
open(block_device, O_RDONLY, 0)) == -1) err(1, "%s", block_device);

The functions

and

first appeared in

and

It is important never to pass a string with user-supplied data as a
format without using

An attacker can put format specifiers in the string to mangle the stack,
leading to a possible security hole. This holds true even if the string
has been built

using a function like

as the resulting string may still contain user-supplied conversion
specifiers for later interpolation by the

and

family of functions.

Always be sure to use the proper secure idiom:

errc(1, 0, "%s", string);
