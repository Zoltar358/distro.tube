#+TITLE: Manpages - DefaultColormap.3
#+DESCRIPTION: Linux manpage for DefaultColormap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about DefaultColormap.3 is found in manpage for: [[../man3/AllPlanes.3][man3/AllPlanes.3]]