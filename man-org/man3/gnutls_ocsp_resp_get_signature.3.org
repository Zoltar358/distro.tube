#+TITLE: Manpages - gnutls_ocsp_resp_get_signature.3
#+DESCRIPTION: Linux manpage for gnutls_ocsp_resp_get_signature.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_ocsp_resp_get_signature - API function

* SYNOPSIS
*#include <gnutls/ocsp.h>*

*int gnutls_ocsp_resp_get_signature(gnutls_ocsp_resp_const_t */resp/*,
gnutls_datum_t * */sig/*);*

* ARGUMENTS
- gnutls_ocsp_resp_const_t resp :: should contain a *gnutls_ocsp_resp_t*
  type

- gnutls_datum_t * sig :: newly allocated output buffer with signature
  data

* DESCRIPTION
This function will extract the signature field of a OCSP response.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
