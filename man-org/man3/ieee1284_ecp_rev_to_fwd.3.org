#+TITLE: Manpages - ieee1284_ecp_rev_to_fwd.3
#+DESCRIPTION: Linux manpage for ieee1284_ecp_rev_to_fwd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ieee1284_ecp_rev_to_fwd.3 is found in manpage for: [[../ieee1284_ecp_fwd_to_rev.3][ieee1284_ecp_fwd_to_rev.3]]