#+TITLE: Manpages - XdbeFreeVisualInfo.3
#+DESCRIPTION: Linux manpage for XdbeFreeVisualInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XdbeFreeVisualInfo - frees information returned by
*XdbeGetVisualInfo().*

* SYNOPSIS
#include <X11/extensions/Xdbe.h>

void XdbeFreeVisualInfo( XdbeScreenVisualInfo *visual_info)

* DESCRIPTION
This function frees the list of XdbeScreenVisualInfo returned by the
function *XdbeGetVisualInfo().*

* SEE ALSO
DBE, /XdbeAllocateBackBufferName(),/ /XdbeBeginIdiom(),/
/XdbeDeallocateBackBufferName(),/ /XdbeEndIdiom(),/
/XdbeGetBackBufferAttributes(),/ /XdbeGetVisualInfo(),/
/XdbeQueryExtension(),/ /XdbeSwapBuffers()./
