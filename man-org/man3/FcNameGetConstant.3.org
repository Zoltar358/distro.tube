#+TITLE: Manpages - FcNameGetConstant.3
#+DESCRIPTION: Linux manpage for FcNameGetConstant.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcNameGetConstant - Lookup symbolic constant

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

const FcConstant * FcNameGetConstant (FcChar8 */string/*);*

* DESCRIPTION
Return the FcConstant structure related to symbolic constant /string/.
