#+TITLE: Manpages - Net_HTTP_Methods.3pm
#+DESCRIPTION: Linux manpage for Net_HTTP_Methods.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Net::HTTP::Methods - Methods shared by Net::HTTP and Net::HTTPS

* VERSION
version 6.21

* AUTHOR
Gisle Aas <gisle@activestate.com>

* COPYRIGHT AND LICENSE
This software is copyright (c) 2001-2017 by Gisle Aas.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
