#+TITLE: Manpages - curl_free.3
#+DESCRIPTION: Linux manpage for curl_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_free - reclaim memory that has been obtained through a libcurl call

* SYNOPSIS
*#include <curl/curl.h>*

*void curl_free( char **/ptr/* );*

* DESCRIPTION
curl_free reclaims memory that has been obtained through a libcurl call.
Use /curl_free(3)/ instead of free() to avoid anomalies that can result
from differences in memory management between your application and
libcurl.

Passing in a NULL pointer in /ptr/ will make this function return
immediately with no action.

* EXAMPLE
#+begin_example
    char *width = curl_getenv("COLUMNS");
    if(width) {
      /* it was set! */
      curl_free(width);
    }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
None

* SEE ALSO
*curl_easy_unescape*(3), *curl_easy_escape*(3)
