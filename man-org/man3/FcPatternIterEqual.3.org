#+TITLE: Manpages - FcPatternIterEqual.3
#+DESCRIPTION: Linux manpage for FcPatternIterEqual.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternIterEqual - Compare iterators

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcPatternIterEqual (const FcPattern */p1/*, FcPatternIter
**/i1/*, const FcPattern **/p2/*, FcPatternIter **/i2/*);*

* DESCRIPTION
Return FcTrue if both /i1/ and /i2/ point to same object and contains
same values. return FcFalse otherwise.

* SINCE
version 2.13.1
