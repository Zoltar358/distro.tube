#+TITLE: Manpages - SSL_CTX_sessions.3ssl
#+DESCRIPTION: Linux manpage for SSL_CTX_sessions.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CTX_sessions - access internal session cache

* SYNOPSIS
#include <openssl/ssl.h> struct lhash_st *SSL_CTX_sessions(SSL_CTX
*ctx);

* DESCRIPTION
*SSL_CTX_sessions()* returns a pointer to the lhash databases containing
the internal session cache for *ctx*.

* NOTES
The sessions in the internal session cache are kept in an *LHASH* (3)
type database. It is possible to directly access this database e.g. for
searching. In parallel, the sessions form a linked list which is
maintained separately from the *LHASH* (3) operations, so that the
database must not be modified directly but by using the
*SSL_CTX_add_session* (3) family of functions.

* RETURN VALUES
*SSL_CTX_sessions()* returns a pointer to the lhash of *SSL_SESSION*.

* SEE ALSO
*ssl* (7), *LHASH* (3), *SSL_CTX_add_session* (3),
*SSL_CTX_set_session_cache_mode* (3)

* COPYRIGHT
Copyright 2001-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
