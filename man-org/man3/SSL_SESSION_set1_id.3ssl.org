#+TITLE: Manpages - SSL_SESSION_set1_id.3ssl
#+DESCRIPTION: Linux manpage for SSL_SESSION_set1_id.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_SESSION_get_id, SSL_SESSION_set1_id - get and set the SSL session ID

* SYNOPSIS
#include <openssl/ssl.h> const unsigned char *SSL_SESSION_get_id(const
SSL_SESSION *s, unsigned int *len) int SSL_SESSION_set1_id(SSL_SESSION
*s, const unsigned char *sid, unsigned int sid_len);

* DESCRIPTION
*SSL_SESSION_get_id()* returns a pointer to the internal session id
value for the session *s*. The length of the id in bytes is stored in
**len*. The length may be 0. The caller should not free the returned
pointer directly.

*SSL_SESSION_set1_id()* sets the session ID for the *ssl* SSL/TLS
session to *sid* of length *sid_len*.

* RETURN VALUES
*SSL_SESSION_get_id()* returns a pointer to the session id value.
*SSL_SESSION_set1_id()* returns 1 for success and 0 for failure, for
example if the supplied session ID length exceeds
*SSL_MAX_SSL_SESSION_ID_LENGTH*.

* SEE ALSO
*ssl* (7)

* HISTORY
The *SSL_SESSION_set1_id()* function was added in OpenSSL 1.1.0.

* COPYRIGHT
Copyright 2015-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
