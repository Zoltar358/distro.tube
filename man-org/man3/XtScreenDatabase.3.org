#+TITLE: Manpages - XtScreenDatabase.3
#+DESCRIPTION: Linux manpage for XtScreenDatabase.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtScreenDatabase.3 is found in manpage for: [[../man3/XtDisplayInitialize.3][man3/XtDisplayInitialize.3]]