#+TITLE: Manpages - __gnu_pbds_associative_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_associative_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::associative_tag - Basic associative-container.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::container_tag*.

Inherited by *__gnu_pbds::basic_branch_tag*,
*__gnu_pbds::basic_hash_tag*, and *__gnu_pbds::list_update_tag*.

* Detailed Description
Basic associative-container.

Definition at line *135* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
