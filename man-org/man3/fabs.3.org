#+TITLE: Manpages - fabs.3
#+DESCRIPTION: Linux manpage for fabs.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fabs, fabsf, fabsl - absolute value of floating-point number

* SYNOPSIS
#+begin_example
  #include <math.h>

  double fabs(double x);
  float fabsf(float x);
  long double fabsl(long double x);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*fabsf*(), *fabsl*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
These functions return the absolute value of the floating-point number
/x/.

* RETURN VALUE
These functions return the absolute value of /x/.

If /x/ is a NaN, a NaN is returned.

If /x/ is -0, +0 is returned.

If /x/ is negative infinity or positive infinity, positive infinity is
returned.

* ERRORS
No errors occur.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *fabs*(), *fabsf*(), *fabsl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

The variant returning /double/ also conforms to SVr4, 4.3BSD, C89.

* SEE ALSO
*abs*(3), *cabs*(3), *ceil*(3), *floor*(3), *labs*(3), *rint*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
