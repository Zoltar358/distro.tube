#+TITLE: Manpages - acl_delete_perm.3
#+DESCRIPTION: Linux manpage for acl_delete_perm.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function deletes the permission contained in the argument

(one of ACL_READ, ACL_WRITE, ACL_EXECUTE) from the permission set
referred to by the argument

An attempt to delete a permission that is not contained in the
permission set is not considered an error.

Any existing descriptors that refer to

continue to refer to that permission set.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid descriptor for a permission set within an ACL entry.

The argument

does not contain a valid

value.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
