#+TITLE: Manpages - WWW_AUR_Login.3pm
#+DESCRIPTION: Linux manpage for WWW_AUR_Login.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
WWW::AUR::Login - Login to the AUR and manage packages, vote, etc.

* SYNOPSIS
my $login = $aurobj->login( whoami, password ) or die failed to login as
whoami; # or ... my $login = eval { WWW::AUR::Login->( whoami,
password ) }; die failed to login as whoami if $@; # You can use package
names for actions... $login->adopt( my-new-package); $login->disown(
not-my-problem ); # Or package IDs for actions... $login->vote( 12353 );
$login->unvote( 12353 ); # Or package objects for actions...
$login->notify( WWW::AUR::Package->new( interesting-package ));
$login->unnotify( WWW::AUR::Package->new( boring-package )); # Flagging
packages out of date... $login->flag( out-of-date-package );
$login->unflag( up-to-date-package ); # Upload a new package file...
$login->upload( /path/to/package-file.src.tar.gz, devel ); # Use the
inherited WWW::AUR::Maintainer accessors. my $name = $login->name; my
@packages = $login->packages;

* DESCRIPTION
This module provides an interface for the AUR package maintainer to
perform /almost/ any action they could perform on the website.
Commenting has not yet been implemented.

*WWW::AUR::Login* is a sub-class of WWW::AUR::Maintainer.

* CONSTRUCTOR
$OBJ = WWW::AUR::Login->new( $USERNAME, $PASSWORD )

- $USERNAME :: The AUR user name to login as.

- $PASSWORD :: The password needed to login as the specified user.

- Errors :: The following errors messages are thrown with =Carp::croak=
  if we were unable to login to the AUR.

  - Failed to login to AUR: bad username or password :: We could not
    login because either the username doesn't exist or the password for
    the username is incorrect.

  - Failed to login to AUR: <LWP error> :: If LWP failed to retrieve the
    page because of some problem with the HTTP request an HTTP status
    code and short message is given at /<LWP error>/.

* METHODS
** adopt disown [un]vote [un]notify [un]flag delete
$MSG = $OBJ->adopt( $PKGID | $PKGNAME | $PKGOBJ ); $MSG = $OBJ->disown(
$PKGID | $PKGNAME | $PKGOBJ ); $MSG = $OBJ->unvote( $PKGID | $PKGNAME |
$PKGOBJ ); $MSG = $OBJ->vote( $PKGID | $PKGNAME | $PKGOBJ ); $MSG =
$OBJ->unnotify( $PKGID | $PKGNAME | $PKGOBJ ); $MSG = $OBJ->notify(
$PKGID | $PKGNAME | $PKGOBJ ); $MSG = $OBJ->unflag( $PKGID | $PKGNAME |
$PKGOBJ ); $MSG = $OBJ->flag( $PKGID | $PKGNAME | $PKGOBJ ); $MSG =
$OBJ->delete( $PKGID | $PKGNAME | $PKGOBJ );

If you are an AUR maintainer you already know what these do. These
actions are the same actions you can perform to a package (with buttons
on the webpage) when you are logged in.

In order to use the delete method you must be logged in as a Trusted
User.

- $MSG :: This is the message displayed at the top of the page by the
  AUR after an action is completed. Not really useful but there it is.

- $PKGID :: The ID of the package. You can see this on a package's
  webpage after the =?ID==. You can also use a WWW::AUR::Package
  object's =id= method. Interally, the ID is what is passed as a
  parameter to the AUR action script.

- $PKGNAME :: The name of the package you want to perform the action on.
  The package is looked up in order to find the ID number.

- $PKGOBJ :: A WWW::AUR::Package object.

- Errors :: 

  - Failed to find package: <Package Name> :: 

  This happens when you pass a package name as argument to an action but
  no package with that name exists.

  - Failed to send package action: <LWP Error> :: Some low level error
    with the HTTP request occurred. The HTTP status code and a short
    message describing the problem is inserted at /<LWP Error>/.

  - Failed to parse package action response :: The code examines the
    HTML that is sent to it in response to an attempted action. When we
    scraped the HTML it did not match the output we expected. Maybe the
    AUR website was changed and this module wasn't updated?

  - <action> action on <package> failed:\n<< <AUR message> >> :: This
    hardly happens. If this does it is probably an internal error. A
    stack-trace is printed out. /<action>/ is the name of the action
    method, /<package>/ is the argument passed to the method (converted
    to a name if possible), /<AUR message>/ is the message the AUR
    prints at the top of the webpage.

** upload
$OBJ->upload( $SRCPKG_PATH, $CATEGORY );

This method submits a new package to the AUR by uploading a source
package file. Just point it to a source package file and specify which
category to assign the package to.

- $SRCPKG_PATH :: The path to a source package file to upload/submit to
  the AUR.

- $CATEGORY :: Every package in the AUR belongs to a category. This may
  be useful someday. You will have to choose which category your package
  should belong to. The category is given as a string and must be one of
  the following:

  - daemons

  - devel

  - editors

  - emulators

  - games

  - gnome

  - i18n

  - kde

  - kernels

  - lib

  - modules

  - multimedia

  - network

  - office

  - science

  - system

  - x11

  - xfce

* SEE ALSO
WWW::AUR

* AUTHOR
Justin Davis, =<juster at cpan dot org>=

* BUGS
Please email me any bugs you find. I will try to fix them as quick as I
can.

* SUPPORT
Send me an email if you have any questions or need help.

* LICENSE AND COPYRIGHT
Copyright 2014 Justin Davis.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
