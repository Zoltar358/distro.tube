#+TITLE: Manpages - CURLOPT_POSTFIELDS.3
#+DESCRIPTION: Linux manpage for CURLOPT_POSTFIELDS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_POSTFIELDS - data to POST to server

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POSTFIELDS, char
*postdata);

* DESCRIPTION
Pass a char * as parameter, pointing to the full data to send in an HTTP
POST operation. You must make sure that the data is formatted the way
you want the server to receive it. libcurl will not convert or encode it
for you in any way. For example, the web server may assume that this
data is url-encoded.

The data pointed to is NOT copied by the library: as a consequence, it
must be preserved by the calling application until the associated
transfer finishes. This behavior can be changed (so libcurl does copy
the data) by setting the /CURLOPT_COPYPOSTFIELDS(3)/ option.

This POST is a normal application/x-www-form-urlencoded kind (and
libcurl will set that Content-Type by default when this option is used),
which is commonly used by HTML forms. Change Content-Type with
/CURLOPT_HTTPHEADER(3)/.

You can use /curl_easy_escape(3)/ to url-encode your data, if necessary.
It returns a pointer to an encoded string that can be passed as
/postdata/.

Using /CURLOPT_POSTFIELDS(3)/ implies setting /CURLOPT_POST(3)/ to 1.

If /CURLOPT_POSTFIELDS(3)/ is explicitly set to NULL then libcurl will
get the POST data from the read callback. If you want to send a
zero-byte POST set /CURLOPT_POSTFIELDS(3)/ to an empty string, or set
/CURLOPT_POST(3)/ to 1 and /CURLOPT_POSTFIELDSIZE(3)/ to 0.

libcurl will use assume this option points to a nul-terminated string
unless you also set /CURLOPT_POSTFIELDSIZE(3)/ to specify the length of
the provided data, which then is strictly required if you want to send
off nul bytes included in the data.

Using POST with HTTP 1.1 implies the use of a "Expect: 100-continue"
header, and libcurl will add that header automatically if the POST is
either known to be larger than 1MB or if the expected size is unknown.
You can disable this header with /CURLOPT_HTTPHEADER(3)/ as usual.

To make multipart/formdata posts (aka RFC2388-posts), check out the
/CURLOPT_HTTPPOST(3)/ option combined with /curl_formadd(3)/.

* DEFAULT
NULL

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    const char *data = "data to send";

    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* size of the POST data */
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, 12L);

    /* pass in a pointer to the data - libcurl will not copy */
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_POSTFIELDSIZE*(3), *CURLOPT_READFUNCTION*(3),
