#+TITLE: Manpages - FcPatternEqual.3
#+DESCRIPTION: Linux manpage for FcPatternEqual.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternEqual - Compare patterns

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcPatternEqual (const FcPattern */pa/*, const FcPattern
**/pb/*);*

* DESCRIPTION
Returns whether /pa/ and /pb/ are exactly alike.
