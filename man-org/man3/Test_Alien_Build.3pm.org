#+TITLE: Manpages - Test_Alien_Build.3pm
#+DESCRIPTION: Linux manpage for Test_Alien_Build.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test::Alien::Build - Tools for testing Alien::Build + alienfile

* VERSION
version 2.44

* SYNOPSIS
use Test2::V0; use Test::Alien::Build; # returns an instance of
Alien::Build. my $build = alienfile_ok q{ use alienfile; plugin
My::Plugin => ( foo => 1, bar => string, ... ); }; alien_build_ok builds
okay.; done_testing;

* DESCRIPTION
This module provides some tools for testing Alien::Build and alienfile.
Outside of Alien::Build core development, It is probably most useful for
Alien::Build::Plugin developers.

This module also unsets a number of Alien::Build specific environment
variables, in order to make tests reproducible even when overrides are
set in different environments. So if you want to test those variables in
various states you should explicitly set them in your test script. These
variables are unset if they defined: =ALIEN_BUILD_PRELOAD=
=ALIEN_BUILD_POSTLOAD= =ALIEN_INSTALL_TYPE=.

* FUNCTIONS
** alienfile
my $build = alienfile; my $build = alienfile q{ use alienfile ... }; my
$build = alienfile filename => alienfile;

Create a Alien::Build instance from the given alienfile. The first two
forms are abbreviations.

my $build = alienfile; # is the same as my $build = alienfile filename
=> alienfile;

and

my $build = alienfile q{ use alienfile ... }; # is the same as my $build
= alienfile source => q{ use alienfile ... };

Except for the second abbreviated form sets the line number before
feeding the source into Alien::Build so that you will get diagnostics
with the correct line numbers.

- source :: The source for the alienfile as a string. You must specify
  one of =source= or =filename=.

- filename :: The filename for the alienfile. You must specify one of
  =source= or =filename=.

- root :: The build root.

- stage :: The staging area for the build.

- prefix :: The install prefix for the build.

** alienfile_ok
my $build = alienfile_ok; my $build = alienfile_ok q{ use alienfile ...
}; my $build = alienfile_ok filename => alienfile; my $build =
alienfile_ok $build;

Same as =alienfile= above, except that it runs as a test, and will not
throw an exception on failure (it will return undef instead).

[version 1.49]

As of version 1.49 you can also pass in an already formed instance of
Alien::Build. This allows you to do something like this:

subtest a subtest => sub { my $build = alienfile q{ use alienfile; ...
}; alienfile_skip_if_missing_prereqs; # skip if alienfile prereqs are
missing alienfile_ok $build; # delayed pass/fail for the compile of
alienfile };

** alienfile_skip_if_missing_prereqs
alienfile_skip_if_missing_prereqs; alienfile_skip_if_missing_prereqs
$phase;

Skips the test or subtest if the prereqs for the alienfile are missing.
If =$phase= is not given, then either =share= or =system= will be
detected.

** alien_install_type_is
alien_install_type_is $type; alien_install_type_is $type, $name;

Simple test to see if the install type is what you expect. =$type=
should be one of =system= or =share=.

** alien_download_ok
my $file = alien_download_ok; my $file = alien_download_ok $name;

Makes a download attempt and test that a file or directory results.
Returns the file or directory if successful. Returns =undef= otherwise.

** alien_extract_ok
my $dir = alien_extract_ok; my $dir = alien_extract_ok $archive; my $dir
= alien_extract_ok $archive, $name; my $dir = alien_extract_ok undef,
$name;

Makes an extraction attempt and test that a directory results. Returns
the directory if successful. Returns =undef= otherwise.

** alien_build_ok
my $alien = alien_build_ok; my $alien = alien_build_ok $name; my $alien
= alien_build_ok { class => $class }; my $alien = alien_build_ok { class
=> $class }, $name;

Runs the download and build stages. Passes if the build succeeds.
Returns an instance of Alien::Base which can be passed into =alien_ok=
from Test::Alien. Returns =undef= if the test fails.

Options

- class :: The base class to use for your alien. This is Alien::Base by
  default. Should be a subclass of Alien::Base, or at least adhere to
  its API.

** alien_build_clean
alien_build_clean;

Removes all files with the current build, except for the runtime prefix.
This helps test that the final install won't depend on the build files.

** alien_clean_install
alien_clean_install;

Runs =$build->clean_install=, and verifies it did not crash.

** alien_checkpoint_ok
alien_checkpoint_ok; alien_checkpoint_ok $test_name;

Test the checkpoint of a build.

** alien_resume_ok
alien_resume_ok; alien_resume_ok $test_name;

Test a resume a checkpointed build.

** alien_rc
alien_rc $code;

Creates =rc.pl= file in a temp directory and sets ALIEN_BUILD_RC. Useful
for testing plugins that should be called from =~/.alienbuild/rc.pl=.
Note that because of the nature of how the =~/.alienbuild/rc.pl= file
works, you can only use this once!

** alien_subtest
alien_subtest $test_name => sub { ... };

Clear the build object and clear the build object before and after the
subtest.

* SEE ALSO
- Alien :: 

- alienfile :: 

- Alien::Build :: 

- Test::Alien :: 

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
