#+TITLE: Manpages - XSetFunction.3
#+DESCRIPTION: Linux manpage for XSetFunction.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetFunction.3 is found in manpage for: [[../man3/XSetState.3][man3/XSetState.3]]