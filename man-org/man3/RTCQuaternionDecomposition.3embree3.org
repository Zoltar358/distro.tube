#+TITLE: Manpages - RTCQuaternionDecomposition.3embree3
#+DESCRIPTION: Linux manpage for RTCQuaternionDecomposition.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  RTCQuaternionDecomposition - structure that represents a quaternion
    decomposition of an affine transformation
#+end_example

** SYNOPSIS
#+begin_example
  struct RTCQuaternionDecomposition
  {
    float scale_x, scale_y, scale_z;
    float skew_xy, skew_xz, skew_yz;
    float shift_x, shift_y, shift_z;
    float quaternion_r, quaternion_i, quaternion_j, quaternion_k;
    float translation_x, translation_y, translation_z;
  };
#+end_example

** DESCRIPTION
The struct =RTCQuaternionDecomposition= represents an affine
transformation decomposed into three parts. An upper triangular
scaling/skew/shift matrix

#+begin_quote
  $$ S = \left( \begin{array}{cccc} scale_x & skew_{xy} & skew_{xz} &
  shift_x \\ 0 & scale_y & skew_{yz} & shift_y \\ 0 & 0 & scale_z &
  shift_z \\ 0 & 0 & 0 & 1 \\ \end{array} \right), $$
#+end_quote

a translation matrix

#+begin_quote
  $$ T = \left( \begin{array}{cccc} 1 & 0 & 0 & translation_x \\ 0 & 1 &
  0 & translation_y \\ 0 & 0 & 1 & translation_z \\ 0 & 0 & 0 & 1 \\
  \end{array} \right), $$
#+end_quote

and a rotation matrix /R/, represented as a quaternion

/quaternion/~/r/~ + /quaternion/~/i/~ *i* + /quaternion/~/j/~
*i* + /quaternion/~/k/~ *k*

where *i*, *j* *k* are the imaginary quaternion units. The passed
quaternion will be normalized internally.

The affine transformation matrix corresponding to a
=RTCQuaternionDecomposition= is /TRS/ and a point
/p/ = (/p/~/x/~, /p/~/y/~, /p/~/z/~, 1)^/T/^ will be transformed as

#+begin_quote
  /p/′ = /T/ /R/ /S/ /p/.
#+end_quote

The functions =rtcInitQuaternionDecomposition=,
=rtcQuaternionDecompositionSetQuaternion=,
=rtcQuaternionDecompositionSetScale=,
=rtcQuaternionDecompositionSetSkew=,
=rtcQuaternionDecompositionSetShift=, and
=rtcQuaternionDecompositionSetTranslation= allow to set the fields of
the structure more conveniently.

** EXIT STATUS
No error code is set by this function.

** SEE ALSO
[rtcSetGeometryTransformQuaternion], [rtcInitQuaternionDecomposition]
