#+TITLE: Manpages - SDL_JoystickNumButtons.3
#+DESCRIPTION: Linux manpage for SDL_JoystickNumButtons.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickNumButtons - Get the number of joysitck buttons

* SYNOPSIS
*#include "SDL.h"*

*int SDL_JoystickNumButtons*(*SDL_Joystick *joystick*);

* DESCRIPTION
Return the number of buttons available from a previously opened
*SDL_Joystick*.

* RETURN VALUE
Number of buttons.

* SEE ALSO
*SDL_JoystickGetButton*, *SDL_JoystickOpen*
