#+TITLE: Manpages - rewinddir.3
#+DESCRIPTION: Linux manpage for rewinddir.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rewinddir - reset directory stream

* SYNOPSIS
#+begin_example
  #include <sys/types.h>
  #include <dirent.h>

  void rewinddir(DIR *dirp);
#+end_example

* DESCRIPTION
The *rewinddir*() function resets the position of the directory stream
/dirp/ to the beginning of the directory.

* RETURN VALUE
The *rewinddir*() function returns no value.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface     | Attribute     | Value   |
| *rewinddir*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.

* SEE ALSO
*closedir*(3), *opendir*(3), *readdir*(3), *scandir*(3), *seekdir*(3),
*telldir*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
