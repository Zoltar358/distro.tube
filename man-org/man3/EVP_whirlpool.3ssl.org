#+TITLE: Manpages - EVP_whirlpool.3ssl
#+DESCRIPTION: Linux manpage for EVP_whirlpool.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_whirlpool - WHIRLPOOL For EVP

* SYNOPSIS
#include <openssl/evp.h> const EVP_MD *EVP_whirlpool(void);

* DESCRIPTION
WHIRLPOOL is a cryptographic hash function standardized in ISO/IEC
10118-3:2004 designed by Vincent Rijmen and Paulo S. L. M. Barreto.

- EVP_whirlpool() :: The WHIRLPOOL algorithm that produces a message
  digest of 512-bits from a given input.

* RETURN VALUES
These functions return a *EVP_MD* structure that contains the
implementation of the symmetric cipher. See *EVP_MD_meth_new* (3) for
details of the *EVP_MD* structure.

* CONFORMING TO
ISO/IEC 10118-3:2004.

* SEE ALSO
*evp* (7), *EVP_DigestInit* (3)

* COPYRIGHT
Copyright 2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
