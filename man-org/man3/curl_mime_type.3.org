#+TITLE: Manpages - curl_mime_type.3
#+DESCRIPTION: Linux manpage for curl_mime_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_mime_type - set a mime part's content type

* SYNOPSIS
*#include <curl/curl.h>*

*CURLcode curl_mime_type(curl_mimepart * */part/*,* *const char *
*/mimetype/*);*

* DESCRIPTION
/curl_mime_type(3)/ sets a mime part's content type.

/part/ is the part's handle to assign the content type to.

/mimetype/ points to the null-terminated file mime type string; it may
be set to NULL to remove a previously attached mime type.

The mime type string is copied into the part, thus the associated
storage may safely be released or reused after call. Setting a part's
type twice is valid: only the value set by the last call is retained.

In the absence of a mime type and if needed by the protocol
specifications, a default mime type is determined by the context:\\
- If set as a custom header, use this value.\\
- application/form-data for an HTTP form post.\\
- If a remote file name is set, the mime type is taken from the file
name extension, or application/octet-stream by default.\\
- For a multipart part, multipart/mixed.\\
- text/plain in other cases.

* EXAMPLE
#+begin_example
   curl_mime *mime;
   curl_mimepart *part;

   /* create a mime handle */
   mime = curl_mime_init(easy);

   /* add a part */
   part = curl_mime_addpart(mime);

   /* get data from this file */
   curl_mime_filedata(part, "image.png");

   /* content-type for this part */
   curl_mime_type(part, "image/png");

   /* set name */
   curl_mime_name(part, "image");
#+end_example

* AVAILABILITY
As long as at least one of HTTP, SMTP or IMAP is enabled. Added in
7.56.0.

* RETURN VALUE
CURLE_OK or a CURL error code upon failure.

* SEE ALSO
*curl_mime_addpart*(3), *curl_mime_name*(3), *curl_mime_data*(3)
