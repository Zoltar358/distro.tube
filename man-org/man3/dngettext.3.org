#+TITLE: Manpages - dngettext.3
#+DESCRIPTION: Linux manpage for dngettext.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about dngettext.3 is found in manpage for: [[../man3/ngettext.3][man3/ngettext.3]]