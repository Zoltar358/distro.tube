#+TITLE: Manpages - cpow.3
#+DESCRIPTION: Linux manpage for cpow.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cpow, cpowf, cpowl - complex power function

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double complex cpow(double complex x, double complex z);
  float complex cpowf(float complex x, float complex z);
  long double complex cpowl(long double complex x,
   long double complex z);

  Link with -lm.
#+end_example

* DESCRIPTION
These functions calculate /x/ raised to the power /z/ (with a branch cut
for /x/ along the negative real axis.)

* VERSIONS
These functions first appeared in glibc in version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *cpow*(), *cpowf*(), *cpowl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*cabs*(3), *pow*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
