#+TITLE: Manpages - std_geometric_distribution.3
#+DESCRIPTION: Linux manpage for std_geometric_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::geometric_distribution< _IntType > - A discrete geometric random
number distribution.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef _IntType *result_type*\\

** Public Member Functions
*geometric_distribution* (const *param_type* &__p)\\

*geometric_distribution* (double __p)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

*result_type* *max* () const\\
Returns the least upper bound value of the distribution.

*result_type* *min* () const\\
Returns the greatest lower bound value of the distribution.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

double *p* () const\\
Returns the distribution parameter =p=.

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\
Resets the distribution state.

** Friends
bool *operator==* (const *geometric_distribution* &__d1, const
*geometric_distribution* &__d2)\\
Return true if two geometric distributions have the same parameters.

* Detailed Description
** "template<typename _IntType = int>
\\
class std::geometric_distribution< _IntType >"A discrete geometric
random number distribution.

The formula for the geometric probability density function is $p(i|p) =
p(1 - p)^{i}$ where $p$ is the parameter of the distribution.

Definition at line *3980* of file *random.h*.

* Member Typedef Documentation
** template<typename _IntType = int> typedef _IntType
*std::geometric_distribution*< _IntType >::*result_type*
The type of the range of the distribution.

Definition at line *3987* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _IntType = int> *std::geometric_distribution*<
_IntType >::*geometric_distribution* ()= [inline]=
Definition at line *4029* of file *random.h*.

** template<typename _IntType = int> *std::geometric_distribution*<
_IntType >::*geometric_distribution* (double __p)= [inline]=,
= [explicit]=
Definition at line *4032* of file *random.h*.

** template<typename _IntType = int> *std::geometric_distribution*<
_IntType >::*geometric_distribution* (const *param_type* &
__p)= [inline]=, = [explicit]=
Definition at line *4037* of file *random.h*.

* Member Function Documentation
** template<typename _IntType = int> template<typename _ForwardIterator
, typename _UniformRandomNumberGenerator > void
*std::geometric_distribution*< _IntType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator &
__urng)= [inline]=
Definition at line *4101* of file *random.h*.

** template<typename _IntType = int> template<typename _ForwardIterator
, typename _UniformRandomNumberGenerator > void
*std::geometric_distribution*< _IntType >::__generate (_ForwardIterator
__f, _ForwardIterator __t, _UniformRandomNumberGenerator & __urng, const
*param_type* & __p)= [inline]=
Definition at line *4108* of file *random.h*.

** template<typename _IntType = int> template<typename
_UniformRandomNumberGenerator > void *std::geometric_distribution*<
_IntType >::__generate (*result_type* * __f, *result_type* * __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *4115* of file *random.h*.

** template<typename _IntType = int> *result_type*
*std::geometric_distribution*< _IntType >::max () const= [inline]=
Returns the least upper bound value of the distribution.

Definition at line *4082* of file *random.h*.

References *std::numeric_limits< _Tp >::max()*.

** template<typename _IntType = int> *result_type*
*std::geometric_distribution*< _IntType >::min () const= [inline]=
Returns the greatest lower bound value of the distribution.

Definition at line *4075* of file *random.h*.

** template<typename _IntType = int> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::geometric_distribution*< _IntType >::operator()
(_UniformRandomNumberGenerator & __urng)= [inline]=
Generating functions.

Definition at line *4090* of file *random.h*.

References *std::geometric_distribution< _IntType >::operator()()*.

Referenced by *std::geometric_distribution< _IntType >::operator()()*.

** template<typename _IntType > template<typename
_UniformRandomNumberGenerator > *geometric_distribution*< _IntType
>::*result_type* *std::geometric_distribution*< _IntType >::operator()
(_UniformRandomNumberGenerator & __urng, const *param_type* & __p)
Definition at line *1044* of file *bits/random.tcc*.

** template<typename _IntType = int> double
*std::geometric_distribution*< _IntType >::p () const= [inline]=
Returns the distribution parameter =p=.

Definition at line *4053* of file *random.h*.

** template<typename _IntType = int> *param_type*
*std::geometric_distribution*< _IntType >::param () const= [inline]=
Returns the parameter set of the distribution.

Definition at line *4060* of file *random.h*.

Referenced by *std::operator>>()*.

** template<typename _IntType = int> void *std::geometric_distribution*<
_IntType >::param (const *param_type* & __param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *4068* of file *random.h*.

** template<typename _IntType = int> void *std::geometric_distribution*<
_IntType >::reset ()= [inline]=
Resets the distribution state. Does nothing for the geometric
distribution.

Definition at line *4047* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _IntType = int> bool operator== (const
*geometric_distribution*< _IntType > & __d1, const
*geometric_distribution*< _IntType > & __d2)= [friend]=
Return true if two geometric distributions have the same parameters.

Definition at line *4125* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
