#+TITLE: Manpages - XwcLookupString.3
#+DESCRIPTION: Linux manpage for XwcLookupString.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XwcLookupString.3 is found in manpage for: [[../man3/XmbLookupString.3][man3/XmbLookupString.3]]