#+TITLE: Manpages - std_forward_iterator_tag.3
#+DESCRIPTION: Linux manpage for std_forward_iterator_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::forward_iterator_tag - Forward iterators support a superset of
input iterator operations.

* SYNOPSIS
\\

=#include <stl_iterator_base_types.h>=

Inherits *std::input_iterator_tag*.

Inherited by *std::bidirectional_iterator_tag*.

* Detailed Description
Forward iterators support a superset of input iterator operations.

Definition at line *99* of file *stl_iterator_base_types.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
