#+TITLE: Manpages - gnutls_pkcs12_bag_set_key_id.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs12_bag_set_key_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs12_bag_set_key_id - API function

* SYNOPSIS
*#include <gnutls/pkcs12.h>*

*int gnutls_pkcs12_bag_set_key_id(gnutls_pkcs12_bag_t */bag/*, unsigned
*/indx/*, const gnutls_datum_t * */id/*);*

* ARGUMENTS
- gnutls_pkcs12_bag_t bag :: The bag

- unsigned indx :: The bag's element to add the id

- const gnutls_datum_t * id :: the ID

* DESCRIPTION
This function will add the given key ID, to the specified, by the index,
bag element. The key ID will be encoded as a 'Local key identifier' bag
attribute, which is usually used to distinguish the local private key
and the certificate pair.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value. or a negative error code on error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
