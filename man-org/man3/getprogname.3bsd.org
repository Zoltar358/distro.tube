#+TITLE: Manpages - getprogname.3bsd
#+DESCRIPTION: Linux manpage for getprogname.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

and

functions manipulate the name of the current program. They are used by
error-reporting routines to produce consistent output.

The

function returns the name of the program. If the name has not been set
yet, it will return

The

function sets the name of the program to be the last component of the

argument. Since a pointer to the given string is kept as the program
name, it should not be modified for the rest of the program's lifetime.

In

the name of the program is set by the start-up code that is run before

thus, running

is not necessary. Programs that desire maximum portability should still
call it; on another operating system, these functions may be implemented
in a portability library. Calling

allows the aforementioned library to learn the program name without
modifications to the start-up code.

These functions first appeared in

and made their way into
