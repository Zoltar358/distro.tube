#+TITLE: Manpages - siglongjmp.3
#+DESCRIPTION: Linux manpage for siglongjmp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about siglongjmp.3 is found in manpage for: [[../man3/setjmp.3][man3/setjmp.3]]