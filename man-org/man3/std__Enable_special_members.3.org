#+TITLE: Manpages - std__Enable_special_members.3
#+DESCRIPTION: Linux manpage for std__Enable_special_members.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Enable_special_members< _Default, _Destructor, _Copy,
_CopyAssignment, _Move, _MoveAssignment, _Tag > - A mixin helper to
conditionally enable or disable the special members.

* SYNOPSIS
\\

=#include <enable_special_members.h>=

Inherits *std::_Enable_default_constructor< _Default, void >*,
*std::_Enable_destructor< _Destructor, void >*, and
*std::_Enable_copy_move< _Copy, _CopyAssignment, _Move, _MoveAssignment,
void >*.

* Detailed Description
** "template<bool _Default, bool _Destructor, bool _Copy, bool
_CopyAssignment, bool _Move, bool _MoveAssignment, typename _Tag = void>
\\
struct std::_Enable_special_members< _Default, _Destructor, _Copy,
_CopyAssignment, _Move, _MoveAssignment, _Tag >"A mixin helper to
conditionally enable or disable the special members.

The =_Tag= type parameter is to make mixin bases unique and thus avoid
ambiguities.

Definition at line *99* of file *enable_special_members.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
