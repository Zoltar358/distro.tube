#+TITLE: Manpages - ldns_rr_owner.3
#+DESCRIPTION: Linux manpage for ldns_rr_owner.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_rdf, ldns_rr_owner, ldns_rr_rd_count, ldns_rr_ttl,
ldns_rr_get_class - access rdata fields on ldns_rr

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rdf* ldns_rr_rdf(const ldns_rr *rr, size_t nr);

ldns_rdf* ldns_rr_owner(const ldns_rr *rr);

size_t ldns_rr_rd_count(const ldns_rr *rr);

uint32_t ldns_rr_ttl(const ldns_rr *rr);

ldns_rr_class ldns_rr_get_class(const ldns_rr *rr);

* DESCRIPTION
/ldns_rr_rdf/() returns the rdata field member counter. .br **rr*: rr to
operate on .br *nr*: the number of the rdf to return .br Returns
ldns_rdf *

/ldns_rr_owner/() returns the owner name of an rr structure. .br **rr*:
rr to operate on .br Returns ldns_rdf *

/ldns_rr_rd_count/() returns the rd_count of an rr structure. .br **rr*:
the rr to read from .br Returns the rd count of the rr

/ldns_rr_ttl/() returns the ttl of an rr structure. .br **rr*: the rr to
read from .br Returns the ttl of the rr

/ldns_rr_get_class/() returns the class of the rr. .br **rr*: the rr to
read from .br Returns the class of the rr

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr/, /ldns_rr_list/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
