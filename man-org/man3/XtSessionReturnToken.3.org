#+TITLE: Manpages - XtSessionReturnToken.3
#+DESCRIPTION: Linux manpage for XtSessionReturnToken.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtSessionReturnToken.3 is found in manpage for: [[../man3/XtSessionGetToken.3][man3/XtSessionGetToken.3]]