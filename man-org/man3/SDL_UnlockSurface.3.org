#+TITLE: Manpages - SDL_UnlockSurface.3
#+DESCRIPTION: Linux manpage for SDL_UnlockSurface.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_UnlockSurface - Unlocks a previously locked surface.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_UnlockSurface*(*SDL_Surface *surface*);

* DESCRIPTION
Surfaces that were previously locked using *SDL_LockSurface* must be
unlocked with *SDL_UnlockSurface*. Surfaces should be unlocked as soon
as possible.

It should be noted that since 1.1.8, surface locks are recursive. See
*SDL_LockSurface*.

* SEE ALSO
*SDL_LockSurface*
