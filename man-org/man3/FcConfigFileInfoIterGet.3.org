#+TITLE: Manpages - FcConfigFileInfoIterGet.3
#+DESCRIPTION: Linux manpage for FcConfigFileInfoIterGet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigFileInfoIterGet - Obtain the configuration file information

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigFileInfoIterGet (FcConfig */config/*,
FcConfigFileInfoIter **/iter/*, FcChar8 ***/name/*, FcChar8
***/description/*, FcBool **/enabled/*);*

* DESCRIPTION
Obtain the filename, the description and the flag whether it is enabled
or not for 'iter' where points to current configuration file
information. If the iterator is invalid, FcFalse is returned.

This function isn't MT-safe. *FcConfigReference* must be called before
using *FcConfigFileInfoIterInit* and then *FcConfigDestroy* when the
relevant values are no longer referenced.

* SINCE
version 2.12.91
