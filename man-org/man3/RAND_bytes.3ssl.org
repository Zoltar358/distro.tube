#+TITLE: Manpages - RAND_bytes.3ssl
#+DESCRIPTION: Linux manpage for RAND_bytes.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
RAND_bytes, RAND_priv_bytes, RAND_pseudo_bytes - generate random data

* SYNOPSIS
#include <openssl/rand.h> int RAND_bytes(unsigned char *buf, int num);
int RAND_priv_bytes(unsigned char *buf, int num);

Deprecated:

#if OPENSSL_API_COMPAT < 0x10100000L int RAND_pseudo_bytes(unsigned char
*buf, int num); #endif

* DESCRIPTION
*RAND_bytes()* generates *num* random bytes using a cryptographically
secure pseudo random generator (CSPRNG) and stores them in *buf*.

*RAND_priv_bytes()* has the same semantics as *RAND_bytes()*. It is
intended to be used for generating values that should remain private. If
using the default RAND_METHOD, this function uses a separate private
PRNG instance so that a compromise of the public PRNG instance will not
affect the secrecy of these private values, as described in *RAND* (7)
and *RAND_DRBG* (7).

* NOTES
By default, the OpenSSL CSPRNG supports a security level of 256 bits,
provided it was able to seed itself from a trusted entropy source. On
all major platforms supported by OpenSSL (including the Unix-like
platforms and Windows), OpenSSL is configured to automatically seed the
CSPRNG on first use using the operating systems's random generator.

If the entropy source fails or is not available, the CSPRNG will enter
an error state and refuse to generate random bytes. For that reason, it
is important to always check the error return value of *RAND_bytes()*
and *RAND_priv_bytes()* and not take randomness for granted.

On other platforms, there might not be a trusted entropy source
available or OpenSSL might have been explicitly configured to use
different entropy sources. If you are in doubt about the quality of the
entropy source, don't hesitate to ask your operating system vendor or
post a question on GitHub or the openssl-users mailing list.

* RETURN VALUES
*RAND_bytes()* and *RAND_priv_bytes()* return 1 on success, -1 if not
supported by the current RAND method, or 0 on other failure. The error
code can be obtained by *ERR_get_error* (3).

* SEE ALSO
*RAND_add* (3), *RAND_bytes* (3), *RAND_priv_bytes* (3),
*ERR_get_error* (3), *RAND* (7), *RAND_DRBG* (7)

* HISTORY

- *RAND_pseudo_bytes()* was deprecated in OpenSSL 1.1.0; use
  *RAND_bytes()* instead.

- The *RAND_priv_bytes()* function was added in OpenSSL 1.1.1.

* COPYRIGHT
Copyright 2000-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
