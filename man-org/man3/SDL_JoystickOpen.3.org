#+TITLE: Manpages - SDL_JoystickOpen.3
#+DESCRIPTION: Linux manpage for SDL_JoystickOpen.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_JoystickOpen - Opens a joystick for use.

* SYNOPSIS
*#include "SDL.h"*

*SDL_Joystick *SDL_JoystickOpen*(*int index*);

* DESCRIPTION
Opens a joystick for use within SDL. The *index* refers to the N'th
joystick in the system. A joystick must be opened before it game be
used.

* RETURN VALUE
Returns a *SDL_Joystick* structure on success. *NULL* on failure.

* EXAMPLES
#+begin_example
  SDL_Joystick *joy;
  // Check for joystick
  if(SDL_NumJoysticks()>0){
    // Open joystick
    joy=SDL_JoystickOpen(0);
    
    if(joy)
    {
      printf("Opened Joystick 0
  ");
      printf("Name: %s
  ", SDL_JoystickName(0));
      printf("Number of Axes: %d
  ", SDL_JoystickNumAxes(joy));
      printf("Number of Buttons: %d
  ", SDL_JoystickNumButtons(joy));
      printf("Number of Balls: %d
  ", SDL_JoystickNumBalls(joy));
    }
    else
      printf("Couldn't open Joystick 0
  ");
    
    // Close if opened
    if(SDL_JoystickOpened(0))
      SDL_JoystickClose(joy);
  }
#+end_example

* SEE ALSO
*SDL_JoystickClose*
