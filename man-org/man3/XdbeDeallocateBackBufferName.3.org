#+TITLE: Manpages - XdbeDeallocateBackBufferName.3
#+DESCRIPTION: Linux manpage for XdbeDeallocateBackBufferName.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XdbeDeallocateBackBufferName - frees a DBE buffer.

* SYNOPSIS
#include <X11/extensions/Xdbe.h>

Status XdbeDeallocateBackBufferName( Display *dpy, XdbeBackBuffer
buffer)

* DESCRIPTION
This function frees a drawable ID, buffer, that was obtained via
*XdbeAllocateBackBufferName().* The buffer must be a valid name for the
back buffer of a window, or a protocol error results.

* ERRORS
- BadBuffer :: The specified buffer is not associated with a window.

* SEE ALSO
DBE, /XdbeAllocateBackBufferName(),/ /XdbeBeginIdiom(),/
/XdbeEndIdiom(),/ /XdbeFreeVisualInfo(),/
/XdbeGetBackBufferAttributes(),/ /XdbeGetVisualInfo(),/
/XdbeQueryExtension(),/ /XdbeSwapBuffers()./
