#+TITLE: Manpages - FcFreeTypeQueryAll.3
#+DESCRIPTION: Linux manpage for FcFreeTypeQueryAll.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFreeTypeQueryAll - compute all patterns from font file (and index)

* SYNOPSIS
*#include <fontconfig.h>* #include <fcfreetype.h>

unsigned int FcFreeTypeQueryAll (const FcChar8 */file/*, int */id/*,
FcBlanks **/blanks/*, int **/count/*, FcFontSet **/set/*);*

* DESCRIPTION
Constructs patterns found in 'file'. If id is -1, then all patterns
found in 'file' are added to 'set'. Otherwise, this function works
exactly like FcFreeTypeQuery(). The number of faces in 'file' is
returned in 'count'. The number of patterns added to 'set' is returned.
FcBlanks is deprecated, /blanks/ is ignored and accepted only for
compatibility with older code.

* SINCE
version 2.12.91
