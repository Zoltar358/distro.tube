#+TITLE: Manpages - rtcInitQuaternionDecomposition.3embree3
#+DESCRIPTION: Linux manpage for rtcInitQuaternionDecomposition.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcInitQuaternionDecomposition - initializes quaternion decomposition
#+end_example

** SYNOPSIS
#+begin_example
  void rtcInitQuaternionDecomposition(
    struct RTCQuaternionDecomposition* qd
  );
#+end_example

** DESCRIPTION
The =rtcInitQuaternionDecomposition= function initializes a
=RTCQuaternionDecomposition= structure to represent an identity
transformation.

** EXIT STATUS
No error code is set by this function.

** SEE ALSO
[rtcSetGeometryTransformQuaternion], [RTCQuaternionDecomposition]
