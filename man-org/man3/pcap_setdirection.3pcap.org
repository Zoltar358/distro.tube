#+TITLE: Manpages - pcap_setdirection.3pcap
#+DESCRIPTION: Linux manpage for pcap_setdirection.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_setdirection - set the direction for which packets will be captured

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_setdirection(pcap_t *p, pcap_direction_t d);
#+end_example

* DESCRIPTION
*pcap_setdirection*() is used to specify a direction that packets will
be captured. /d/ is one of the constants *PCAP_D_IN*, *PCAP_D_OUT* or
*PCAP_D_INOUT*. *PCAP_D_IN* will only capture packets received by the
device, *PCAP_D_OUT* will only capture packets sent by the device and
*PCAP_D_INOUT* will capture packets received by or sent by the device.
*PCAP_D_INOUT* is the default setting if this function is not called.

*pcap_setdirection*() isn't necessarily fully supported on all
platforms; some platforms might return an error for all values, and some
other platforms might not support *PCAP_D_OUT*.

This operation is not supported if a ``savefile'' is being read.

* RETURN VALUE
*pcap_setdirection*() returns *0* on success and *PCAP_ERROR* on
failure. If *PCAP_ERROR* is returned, *pcap_geterr*(3PCAP) or
*pcap_perror*(3PCAP) may be called with /p/ as an argument to fetch or
display the error text.

* SEE ALSO
*pcap*(3PCAP)
