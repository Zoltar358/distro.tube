#+TITLE: Manpages - XLookupString.3
#+DESCRIPTION: Linux manpage for XLookupString.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XLookupString.3 is found in manpage for: [[../man3/XLookupKeysym.3][man3/XLookupKeysym.3]]