#+TITLE: Manpages - gnutls_hmac_set_nonce.3
#+DESCRIPTION: Linux manpage for gnutls_hmac_set_nonce.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_hmac_set_nonce - API function

* SYNOPSIS
*#include <gnutls/crypto.h>*

*void gnutls_hmac_set_nonce(gnutls_hmac_hd_t */handle/*, const void *
*/nonce/*, size_t */nonce_len/*);*

* ARGUMENTS
- gnutls_hmac_hd_t handle :: is a *gnutls_hmac_hd_t* type

- const void * nonce :: the data to set as nonce

- size_t nonce_len :: the length of data

* DESCRIPTION
This function will set the nonce in the MAC algorithm.

* SINCE
3.2.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
