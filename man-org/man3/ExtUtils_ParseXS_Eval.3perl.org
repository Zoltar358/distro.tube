#+TITLE: Manpages - ExtUtils_ParseXS_Eval.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_ParseXS_Eval.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::ParseXS::Eval - Clean package to evaluate code in

* SYNOPSIS
use ExtUtils::ParseXS::Eval; my $rv =
ExtUtils::ParseXS::Eval::eval_typemap_code( $parsexs_obj, "some Perl
code" );

* SUBROUTINES
** $pxs->eval_output_typemap_code($typemapcode, $other_hashref)
Sets up various bits of previously global state (formerly
ExtUtils::ParseXS package variables) for eval'ing output typemap code
that may refer to these variables.

Warns the contents of =$@= if any.

Not all these variables are necessarily considered public wrt. use in
typemaps, so beware. Variables set up from the ExtUtils::ParseXS object:

$Package $ALIAS $func_name $Full_func_name $pname

Variables set up from =$other_hashref=:

$var $type $ntype $subtype $arg

** $pxs->eval_input_typemap_code($typemapcode, $other_hashref)
Sets up various bits of previously global state (formerly
ExtUtils::ParseXS package variables) for eval'ing output typemap code
that may refer to these variables.

Warns the contents of =$@= if any.

Not all these variables are necessarily considered public wrt. use in
typemaps, so beware. Variables set up from the ExtUtils::ParseXS object:

$Package $ALIAS $func_name $Full_func_name $pname

Variables set up from =$other_hashref=:

$var $type $ntype $subtype $num $init $printed_name $arg $argoff

* TODO
Eventually, with better documentation and possible some cleanup, this
could be part of =ExtUtils::Typemaps=.
