#+TITLE: Manpages - SDL_LoadWAV.3
#+DESCRIPTION: Linux manpage for SDL_LoadWAV.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_LoadWAV - Load a WAVE file

* SYNOPSIS
*#include "SDL.h"*

*SDL_AudioSpec *SDL_LoadWAV*(*const char *file, SDL_AudioSpec *spec,
Uint8 **audio_buf, Uint32 *audio_len*);

* DESCRIPTION
*SDL_LoadWAV* This function loads a WAVE *file* into memory.

If this function succeeds, it returns the given *SDL_AudioSpec*, filled
with the audio data format of the wave data, and sets *audio_buf* to a
*malloc*'d buffer containing the audio data, and sets *audio_len* to the
length of that audio buffer, in bytes. You need to free the audio buffer
with *SDL_FreeWAV* when you are done with it.

This function returns *NULL* and sets the SDL error message if the wave
file cannot be opened, uses an unknown data format, or is corrupt.
Currently raw, MS-ADPCM and IMA-ADPCM WAVE files are supported.

* EXAMPLE
#+begin_example
  SDL_AudioSpec wav_spec;
  Uint32 wav_length;
  Uint8 *wav_buffer;

  /* Load the WAV */
  if( SDL_LoadWAV("test.wav", &wav_spec, &wav_buffer, &wav_length) == NULL ){
    fprintf(stderr, "Could not open test.wav: %s
  ", SDL_GetError());
    exit(-1);
  }
  .
  .
  .
  /* Do stuff with the WAV */
  .
  .
  /* Free It */
  SDL_FreeWAV(wav_buffer);
#+end_example

* SEE ALSO
*SDL_AudioSpec*, *SDL_OpenAudio*, *SDL_FreeWAV*
