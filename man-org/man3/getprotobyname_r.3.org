#+TITLE: Manpages - getprotobyname_r.3
#+DESCRIPTION: Linux manpage for getprotobyname_r.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getprotobyname_r.3 is found in manpage for: [[../man3/getprotoent_r.3][man3/getprotoent_r.3]]