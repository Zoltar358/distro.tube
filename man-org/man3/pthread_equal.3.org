#+TITLE: Manpages - pthread_equal.3
#+DESCRIPTION: Linux manpage for pthread_equal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pthread_equal - compare thread IDs

* SYNOPSIS
#+begin_example
  #include <pthread.h>

  int pthread_equal(pthread_t t1, pthread_t t2);

  Compile and link with -pthread.
#+end_example

* DESCRIPTION
The *pthread_equal*() function compares two thread identifiers.

* RETURN VALUE
If the two thread IDs are equal, *pthread_equal*() returns a nonzero
value; otherwise, it returns 0.

* ERRORS
This function always succeeds.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface         | Attribute     | Value   |
| *pthread_equal*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008.

* NOTES
The *pthread_equal*() function is necessary because thread IDs should be
considered opaque: there is no portable way for applications to directly
compare two /pthread_t/ values.

* SEE ALSO
*pthread_create*(3), *pthread_self*(3), *pthreads*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
