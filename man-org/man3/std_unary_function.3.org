#+TITLE: Manpages - std_unary_function.3
#+DESCRIPTION: Linux manpage for std_unary_function.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::unary_function< _Arg, _Result >

* SYNOPSIS
\\

=#include <stl_function.h>=

Inherited by *std::pointer_to_unary_function< _Arg, _Result >*.

** Public Types
typedef _Arg *argument_type*\\
=argument_type= is the type of the argument

typedef _Result *result_type*\\
=result_type= is the return type

* Detailed Description
** "template<typename _Arg, typename _Result>
\\
struct std::unary_function< _Arg, _Result >"This is one of the *functor
base classes*.

Definition at line *105* of file *stl_function.h*.

* Member Typedef Documentation
** template<typename _Arg , typename _Result > typedef _Arg
*std::unary_function*< _Arg, _Result >::*argument_type*
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** template<typename _Arg , typename _Result > typedef _Result
*std::unary_function*< _Arg, _Result >::*result_type*
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
