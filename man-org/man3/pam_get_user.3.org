#+TITLE: Manpages - pam_get_user.3
#+DESCRIPTION: Linux manpage for pam_get_user.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_get_user - get user name

* SYNOPSIS
#+begin_example
  #include <security/pam_modules.h>
#+end_example

*int pam_get_user(const pam_handle_t **/pamh/*, const char ***/user/*,
const char **/prompt/*);*

* DESCRIPTION
The *pam_get_user* function returns the name of the user specified by
*pam_start*(3). If no user was specified it returns what *pam_get_item
(pamh, PAM_USER, ... );* would have returned. If this is NULL it obtains
the username via the *pam_conv*(3) mechanism, it prompts the user with
the first non-NULL string in the following list:

#+begin_quote
  ·

  The /prompt/ argument passed to the function.
#+end_quote

#+begin_quote
  ·

  What is returned by pam_get_item (pamh, PAM_USER_PROMPT, ... );
#+end_quote

#+begin_quote
  ·

  The default prompt: "login: "
#+end_quote

By whatever means the username is obtained, a pointer to it is returned
as the contents of /*user/. Note, this memory should *not* be /free()/d
or /modified/ by the module.

This function sets the /PAM_USER/ item associated with the
*pam_set_item*(3) and *pam_get_item*(3) functions.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  User name was successful retrieved.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  A NULL pointer was submitted.
#+end_quote

PAM_CONV_ERR

#+begin_quote
  The conversation method supplied by the application failed to obtain
  the username.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_ABORT

#+begin_quote
  Error resuming an old conversation.
#+end_quote

PAM_CONV_AGAIN

#+begin_quote
  The conversation method supplied by the application is waiting for an
  event.
#+end_quote

* SEE ALSO
*pam_end*(3), *pam_get_item*(3), *pam_set_item*(3), *pam_strerror*(3)
