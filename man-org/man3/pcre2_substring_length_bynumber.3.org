#+TITLE: Manpages - pcre2_substring_length_bynumber.3
#+DESCRIPTION: Linux manpage for pcre2_substring_length_bynumber.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_substring_length_bynumber(pcre2_match_data *match_data,
   uint32_t number, PCRE2_SIZE *length);
#+end_example

* DESCRIPTION
This function returns the length of a matched substring, identified by
number. The arguments are:

/match_data/ The match data block for the match /number/ The substring
number /length/ Where to return the length, or NULL

The third argument may be NULL if all you want to know is whether or not
a substring is set. The yield is zero on success, or a negative error
code otherwise. After a partial match, only substring 0 is available.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
