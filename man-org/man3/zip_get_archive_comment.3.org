#+TITLE: Manpages - zip_get_archive_comment.3
#+DESCRIPTION: Linux manpage for zip_get_archive_comment.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function returns the comment for the entire zip archive. The return
value is in UTF-8 encoding unless

was specified (see below). This pointer should not be modified or

and becomes invalid when

is closed. If

is not

the integer to which it points will be set to the length of the comment.
If

is set to

the original unchanged comment is returned.

Additionally, the following

are supported:

Return the unmodified archive comment as it is in the ZIP archive.

(Default.) Guess the encoding of the archive comment in the ZIP archive
and convert it to UTF-8, if necessary. (Only CP-437 and UTF-8 are
recognized.)

Follow the ZIP specification for file names and extend it to the archive
comment, thus also expecting it in CP-437 encoding. Convert it to UTF-8.

ASCII is a subset of both CP-437 and UTF-8.

Upon successful completion, a pointer to the comment is returned, or

if there is no comment.

was added in libzip 0.7. In libzip 0.11 the type of

was changed from

to

and
