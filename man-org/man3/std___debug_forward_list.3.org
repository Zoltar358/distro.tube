#+TITLE: Manpages - std___debug_forward_list.3
#+DESCRIPTION: Linux manpage for std___debug_forward_list.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__debug::forward_list< _Tp, _Alloc > - Class std::forward_list with
safety/checking/debug instrumentation.

* SYNOPSIS
\\

Inherits *__gnu_debug::_Safe_container< forward_list< _Tp,
std::allocator< _Tp > >, std::allocator< _Tp >,
__gnu_debug::_Safe_forward_list >*, and forward_list< _Tp,
std::allocator< _Tp > >.

** Public Types
typedef _Base::allocator_type *allocator_type*\\

typedef *__gnu_debug::_Safe_iterator*< *_Base_const_iterator*,
*forward_list* > *const_iterator*\\

typedef _Base::const_pointer *const_pointer*\\

typedef _Base::const_reference *const_reference*\\

typedef _Base::difference_type *difference_type*\\

typedef *__gnu_debug::_Safe_iterator*< *_Base_iterator*, *forward_list*
> *iterator*\\

typedef _Base::pointer *pointer*\\

typedef _Base::reference *reference*\\

typedef _Base::size_type *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
*forward_list* (_Base_ref __x)\\

template<typename _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *forward_list* (_InputIterator
__first, _InputIterator __last, const allocator_type
&__al=allocator_type())\\

*forward_list* (const allocator_type &__al) noexcept\\

*forward_list* (const *forward_list* &)=default\\

*forward_list* (const *forward_list* &__list, const allocator_type
&__al)\\

*forward_list* (*forward_list* &&)=default\\

*forward_list* (*forward_list* &&__list, const allocator_type &__al)
noexcept(*std::is_nothrow_constructible*< *_Base*, *_Base*, const
allocator_type & >::value)\\

*forward_list* (size_type __n, const _Tp &__value, const allocator_type
&__al=allocator_type())\\

*forward_list* (size_type __n, const allocator_type
&__al=allocator_type())\\

*forward_list* (*std::initializer_list*< _Tp > __il, const
allocator_type &__al=allocator_type())\\

const *_Base* & *_M_base* () const noexcept\\

*_Base* & *_M_base* () noexcept\\

void *_M_invalidate_if* (_Predicate __pred)\\

void *_M_swap* (_Safe_container &__x) noexcept\\

void *_M_transfer_from_if* (_Safe_sequence &__from, _Predicate __pred)\\

template<typename _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> void *assign* (_InputIterator
__first, _InputIterator __last)\\

void *assign* (size_type __n, const _Tp &__val)\\

void *assign* (*std::initializer_list*< _Tp > __il)\\

*const_iterator* *before_begin* () const noexcept\\

*iterator* *before_begin* () noexcept\\

*const_iterator* *begin* () const noexcept\\

*iterator* *begin* () noexcept\\

*const_iterator* *cbefore_begin* () const noexcept\\

*const_iterator* *cbegin* () const noexcept\\

*const_iterator* *cend* () const noexcept\\

void *clear* () noexcept\\

template<typename... _Args> *iterator* *emplace_after* (*const_iterator*
__pos, _Args &&... __args)\\

*const_iterator* *end* () const noexcept\\

*iterator* *end* () noexcept\\

*iterator* *erase_after* (*const_iterator* __pos)\\

*iterator* *erase_after* (*const_iterator* __pos, *const_iterator*
__last)\\

reference *front* ()\\

const_reference *front* () const\\

template<typename _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *iterator* *insert_after*
(*const_iterator* __pos, _InputIterator __first, _InputIterator
__last)\\

*iterator* *insert_after* (*const_iterator* __pos, _Tp &&__val)\\

*iterator* *insert_after* (*const_iterator* __pos, const _Tp &__val)\\

*iterator* *insert_after* (*const_iterator* __pos, size_type __n, const
_Tp &__val)\\

*iterator* *insert_after* (*const_iterator* __pos,
*std::initializer_list*< _Tp > __il)\\

void *merge* (*forward_list* &&__list)\\

template<typename _Comp > void *merge* (*forward_list* &&__list, _Comp
__comp)\\

void *merge* (*forward_list* &__list)\\

template<typename _Comp > void *merge* (*forward_list* &__list, _Comp
__comp)\\

*forward_list* & *operator=* (const *forward_list* &)=default\\

*forward_list* & *operator=* (*forward_list* &&)=default\\

*forward_list* & *operator=* (*std::initializer_list*< _Tp > __il)\\

void *pop_front* ()\\

__remove_return_type *remove* (const _Tp &__val)\\

template<typename _Pred > __remove_return_type *remove_if* (_Pred
__pred)\\

void *resize* (size_type __sz)\\

void *resize* (size_type __sz, const value_type &__val)\\

void *splice_after* (*const_iterator* __pos, *forward_list* &&__list)\\

void *splice_after* (*const_iterator* __pos, *forward_list* &&__list,
*const_iterator* __before, *const_iterator* __last)\\

void *splice_after* (*const_iterator* __pos, *forward_list* &&__list,
*const_iterator* __i)\\

void *splice_after* (*const_iterator* __pos, *forward_list* &__list)\\

void *splice_after* (*const_iterator* __pos, *forward_list* &__list,
*const_iterator* __before, *const_iterator* __last)\\

void *splice_after* (*const_iterator* __pos, *forward_list* &__list,
*const_iterator* __i)\\

void *swap* (*forward_list* &__list) noexcept(noexcept(declval< *_Base*
& >().swap(__list)))\\

__remove_return_type *unique* ()\\

template<typename _BinPred > __remove_return_type *unique* (_BinPred
__binary_pred)\\

** Public Attributes
_Safe_iterator_base * *_M_const_iterators*\\
The list of constant iterators that reference this container.

_Safe_iterator_base * *_M_iterators*\\
The list of mutable iterators that reference this container.

unsigned int *_M_version*\\
The container version number. This number may never be 0.

** Protected Member Functions
void *_M_detach_all* ()\\

void *_M_detach_singular* ()\\

__gnu_cxx::__mutex & *_M_get_mutex* () throw ()\\

void *_M_invalidate_all* ()\\

void *_M_invalidate_all* () const\\

void *_M_revalidate_singular* ()\\

_Safe_container & *_M_safe* () noexcept\\

void *_M_swap* (_Safe_sequence_base &) noexcept\\

** Friends
template<typename _ItT , typename _SeqT , typename _CatT > class
*::__gnu_debug::_Safe_iterator*\\

* Detailed Description
** "template<typename _Tp, typename _Alloc = std::allocator<_Tp>>
\\
class std::__debug::forward_list< _Tp, _Alloc >"Class std::forward_list
with safety/checking/debug instrumentation.

Definition at line *189* of file *debug/forward_list*.

* Member Typedef Documentation
** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Base::allocator_type *std::__debug::forward_list*< _Tp, _Alloc
>::allocator_type
Definition at line *225* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef *__gnu_debug::_Safe_iterator*< *_Base_const_iterator*,
*forward_list*> *std::__debug::forward_list*< _Tp, _Alloc
>::*const_iterator*
Definition at line *219* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Base::const_pointer *std::__debug::forward_list*< _Tp, _Alloc
>::const_pointer
Definition at line *227* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Base::const_reference *std::__debug::forward_list*< _Tp, _Alloc
>::const_reference
Definition at line *214* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Base::difference_type *std::__debug::forward_list*< _Tp, _Alloc
>::difference_type
Definition at line *222* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef *__gnu_debug::_Safe_iterator*< *_Base_iterator*, *forward_list*>
*std::__debug::forward_list*< _Tp, _Alloc >::*iterator*
Definition at line *217* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Base::pointer *std::__debug::forward_list*< _Tp, _Alloc
>::pointer
Definition at line *226* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Base::reference *std::__debug::forward_list*< _Tp, _Alloc
>::reference
Definition at line *213* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Base::size_type *std::__debug::forward_list*< _Tp, _Alloc
>::size_type
Definition at line *221* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
typedef _Tp *std::__debug::forward_list*< _Tp, _Alloc >::value_type
Definition at line *224* of file *debug/forward_list*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*std::__debug::forward_list*< _Tp, _Alloc >::*forward_list* (const
allocator_type & __al)= [inline]=, = [explicit]=, = [noexcept]=
Definition at line *234* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*std::__debug::forward_list*< _Tp, _Alloc >::*forward_list* (const
*forward_list*< _Tp, _Alloc > & __list, const allocator_type &
__al)= [inline]=
Definition at line *237* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*std::__debug::forward_list*< _Tp, _Alloc >::*forward_list*
(*forward_list*< _Tp, _Alloc > && __list, const allocator_type &
__al)= [inline]=, = [noexcept]=
Definition at line *241* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*std::__debug::forward_list*< _Tp, _Alloc >::*forward_list* (size_type
__n, const allocator_type & __al = =allocator_type()=)= [inline]=,
= [explicit]=
Definition at line *250* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*std::__debug::forward_list*< _Tp, _Alloc >::*forward_list* (size_type
__n, const _Tp & __value, const allocator_type & __al =
=allocator_type()=)= [inline]=
Definition at line *254* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *std::__debug::forward_list*<
_Tp, _Alloc >::*forward_list* (_InputIterator __first, _InputIterator
__last, const allocator_type & __al = =allocator_type()=)= [inline]=
Definition at line *261* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*std::__debug::forward_list*< _Tp, _Alloc >::*forward_list*
(*std::initializer_list*< _Tp > __il, const allocator_type & __al =
=allocator_type()=)= [inline]=
Definition at line *272* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*std::__debug::forward_list*< _Tp, _Alloc >::*forward_list* (_Base_ref
__x)= [inline]=
Definition at line *279* of file *debug/forward_list*.

* Member Function Documentation
** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> const
*_Base* & *std::__debug::forward_list*< _Tp, _Alloc >::_M_base ()
const= [inline]=, = [noexcept]=
Definition at line *835* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*_Base* & *std::__debug::forward_list*< _Tp, _Alloc >::_M_base
()= [inline]=, = [noexcept]=
Definition at line *832* of file *debug/forward_list*.

** void __gnu_debug::_Safe_sequence_base::_M_detach_all
()= [protected]=, = [inherited]=
Detach all iterators, leaving them singular.

Referenced by
*__gnu_debug::_Safe_sequence_base::~_Safe_sequence_base()*.

** void __gnu_debug::_Safe_sequence_base::_M_detach_singular
()= [protected]=, = [inherited]=
Detach all singular iterators.

*Postcondition*

#+begin_quote
  for all iterators i attached to this sequence, i->_M_version ==
  _M_version.
#+end_quote

** __gnu_cxx::__mutex & __gnu_debug::_Safe_sequence_base::_M_get_mutex
()= [protected]=, = [inherited]=
For use in _Safe_sequence.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** template<typename _SafeSequence > void
*__gnu_debug::_Safe_forward_list*< _SafeSequence >::_M_invalidate_all
()= [inline]=, = [protected]=, = [inherited]=
Definition at line *73* of file *debug/forward_list*.

** void __gnu_debug::_Safe_sequence_base::_M_invalidate_all ()
const= [inline]=, = [protected]=, = [inherited]=
Invalidates all iterators.

Definition at line *256* of file *safe_base.h*.

References *__gnu_debug::_Safe_sequence_base::_M_version*.

** void *__gnu_debug::_Safe_sequence*< _SafeSequence >::_M_invalidate_if
(_Predicate __pred)= [inherited]=
Invalidates all iterators =x= that reference this sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *117* of file *safe_sequence.tcc*.

** void __gnu_debug::_Safe_sequence_base::_M_revalidate_singular
()= [protected]=, = [inherited]=
Revalidates all attached singular iterators. This method may be used to
validate iterators that were invalidated before (but for some reason,
such as an exception, need to become valid again).

** _Safe_container & *__gnu_debug::_Safe_container*< *forward_list*<
_Tp, *std::allocator*< _Tp > > , *std::allocator*< _Tp > ,
*__gnu_debug::_Safe_forward_list* , true >::_M_safe ()= [inline]=,
= [protected]=, = [noexcept]=, = [inherited]=
Definition at line *52* of file *safe_container.h*.

** void *__gnu_debug::_Safe_container*< *forward_list*< _Tp,
*std::allocator*< _Tp > > , *std::allocator*< _Tp > ,
*__gnu_debug::_Safe_forward_list* , true >::_M_swap (*_Safe_container*<
*forward_list*< _Tp, *std::allocator*< _Tp > >, *std::allocator*< _Tp >,
*__gnu_debug::_Safe_forward_list* > & __x)= [inline]=, = [noexcept]=,
= [inherited]=
Definition at line *111* of file *safe_container.h*.

** template<typename _SafeSequence > void
*__gnu_debug::_Safe_forward_list*< _SafeSequence >::_M_swap
(*_Safe_sequence_base* & __other)= [protected]=, = [noexcept]=,
= [inherited]=
Definition at line *159* of file *debug/forward_list*.

** void *__gnu_debug::_Safe_sequence*< _SafeSequence
>::_M_transfer_from_if (*_Safe_sequence*< _SafeSequence > & __from,
_Predicate __pred)= [inherited]=
Transfers all iterators =x= that reference =from= sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *125* of file *safe_sequence.tcc*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> void
*std::__debug::forward_list*< _Tp, _Alloc >::assign (_InputIterator
__first, _InputIterator __last)= [inline]=
Definition at line *298* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::assign (size_type __n,
const _Tp & __val)= [inline]=
Definition at line *313* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::assign
(*std::initializer_list*< _Tp > __il)= [inline]=
Definition at line *320* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*const_iterator* *std::__debug::forward_list*< _Tp, _Alloc
>::before_begin () const= [inline]=, = [noexcept]=
Definition at line *335* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::before_begin
()= [inline]=, = [noexcept]=
Definition at line *331* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*const_iterator* *std::__debug::forward_list*< _Tp, _Alloc >::begin ()
const= [inline]=, = [noexcept]=
Definition at line *343* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::begin
()= [inline]=, = [noexcept]=
Definition at line *339* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*const_iterator* *std::__debug::forward_list*< _Tp, _Alloc
>::cbefore_begin () const= [inline]=, = [noexcept]=
Definition at line *359* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*const_iterator* *std::__debug::forward_list*< _Tp, _Alloc >::cbegin ()
const= [inline]=, = [noexcept]=
Definition at line *355* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*const_iterator* *std::__debug::forward_list*< _Tp, _Alloc >::cend ()
const= [inline]=, = [noexcept]=
Definition at line *363* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::clear ()= [inline]=,
= [noexcept]=
Definition at line *553* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename... _Args> *iterator* *std::__debug::forward_list*<
_Tp, _Alloc >::emplace_after (*const_iterator* __pos, _Args &&...
__args)= [inline]=
Definition at line *401* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*const_iterator* *std::__debug::forward_list*< _Tp, _Alloc >::end ()
const= [inline]=, = [noexcept]=
Definition at line *351* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::end
()= [inline]=, = [noexcept]=
Definition at line *347* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::erase_after
(*const_iterator* __pos)= [inline]=
Definition at line *459* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::erase_after
(*const_iterator* __pos, *const_iterator* __last)= [inline]=
Definition at line *470* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
reference *std::__debug::forward_list*< _Tp, _Alloc >::front
()= [inline]=
Definition at line *372* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
const_reference *std::__debug::forward_list*< _Tp, _Alloc >::front ()
const= [inline]=
Definition at line *379* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _InputIterator , typename =
std::_RequireInputIter<_InputIterator>> *iterator*
*std::__debug::forward_list*< _Tp, _Alloc >::insert_after
(*const_iterator* __pos, _InputIterator __first, _InputIterator
__last)= [inline]=
Definition at line *433* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::insert_after
(*const_iterator* __pos, _Tp && __val)= [inline]=
Definition at line *417* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::insert_after
(*const_iterator* __pos, const _Tp & __val)= [inline]=
Definition at line *410* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::insert_after
(*const_iterator* __pos, size_type __n, const _Tp & __val)= [inline]=
Definition at line *424* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*iterator* *std::__debug::forward_list*< _Tp, _Alloc >::insert_after
(*const_iterator* __pos, *std::initializer_list*< _Tp > __il)= [inline]=
Definition at line *452* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::merge (*forward_list*< _Tp,
_Alloc > && __list)= [inline]=
Definition at line *784* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _Comp > void *std::__debug::forward_list*< _Tp, _Alloc
>::merge (*forward_list*< _Tp, _Alloc > && __list, _Comp
__comp)= [inline]=
Definition at line *806* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::merge (*forward_list*< _Tp,
_Alloc > & __list)= [inline]=
Definition at line *801* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _Comp > void *std::__debug::forward_list*< _Tp, _Alloc
>::merge (*forward_list*< _Tp, _Alloc > & __list, _Comp
__comp)= [inline]=
Definition at line *825* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
*forward_list* & *std::__debug::forward_list*< _Tp, _Alloc >::operator=
(*std::initializer_list*< _Tp > __il)= [inline]=
Definition at line *288* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::pop_front ()= [inline]=
Definition at line *391* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
__remove_return_type *std::__debug::forward_list*< _Tp, _Alloc >::remove
(const _Tp & __val)= [inline]=
Definition at line *685* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _Pred > __remove_return_type
*std::__debug::forward_list*< _Tp, _Alloc >::remove_if (_Pred
__pred)= [inline]=
Definition at line *715* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::resize (size_type
__sz)= [inline]=
Definition at line *497* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::resize (size_type __sz,
const value_type & __val)= [inline]=
Definition at line *525* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::splice_after
(*const_iterator* __pos, *forward_list*< _Tp, _Alloc > &&
__list)= [inline]=
Definition at line *561* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::splice_after
(*const_iterator* __pos, *forward_list*< _Tp, _Alloc > && __list,
*const_iterator* __before, *const_iterator* __last)= [inline]=
Definition at line *615* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::splice_after
(*const_iterator* __pos, *forward_list*< _Tp, _Alloc > && __list,
*const_iterator* __i)= [inline]=
Definition at line *584* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::splice_after
(*const_iterator* __pos, *forward_list*< _Tp, _Alloc > &
__list)= [inline]=
Definition at line *580* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::splice_after
(*const_iterator* __pos, *forward_list*< _Tp, _Alloc > & __list,
*const_iterator* __before, *const_iterator* __last)= [inline]=
Definition at line *666* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::splice_after
(*const_iterator* __pos, *forward_list*< _Tp, _Alloc > & __list,
*const_iterator* __i)= [inline]=
Definition at line *610* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>> void
*std::__debug::forward_list*< _Tp, _Alloc >::swap (*forward_list*< _Tp,
_Alloc > & __list)= [inline]=, = [noexcept]=
Definition at line *489* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
__remove_return_type *std::__debug::forward_list*< _Tp, _Alloc >::unique
()= [inline]=
Definition at line *744* of file *debug/forward_list*.

** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _BinPred > __remove_return_type
*std::__debug::forward_list*< _Tp, _Alloc >::unique (_BinPred
__binary_pred)= [inline]=
Definition at line *749* of file *debug/forward_list*.

* Friends And Related Function Documentation
** template<typename _Tp , typename _Alloc = std::allocator<_Tp>>
template<typename _ItT , typename _SeqT , typename _CatT > friend class
::*__gnu_debug::_Safe_iterator*= [friend]=
Definition at line *202* of file *debug/forward_list*.

* Member Data Documentation
** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_const_iterators= [inherited]=
The list of constant iterators that reference this container.

Definition at line *197* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_iterators= [inherited]=
The list of mutable iterators that reference this container.

Definition at line *194* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** unsigned int
__gnu_debug::_Safe_sequence_base::_M_version= [mutable]=, = [inherited]=
The container version number. This number may never be 0.

Definition at line *200* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence_base::_M_invalidate_all()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
