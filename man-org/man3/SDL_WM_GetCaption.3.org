#+TITLE: Manpages - SDL_WM_GetCaption.3
#+DESCRIPTION: Linux manpage for SDL_WM_GetCaption.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WM_GetCaption - Gets the window title and icon name.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_WM_GetCaption*(*char **title, char **icon*);

* DESCRIPTION
Set pointers to the window *title* and *icon* name.

* SEE ALSO
*SDL_WM_SetCaption*, *SDL_WM_SetIcon*
