#+TITLE: Manpages - asn1_get_length_der.3
#+DESCRIPTION: Linux manpage for asn1_get_length_der.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
asn1_get_length_der - API function

* SYNOPSIS
*#include <libtasn1.h>*

*long asn1_get_length_der(const unsigned char * */der/*, int
*/der_len/*, int * */len/*);*

* ARGUMENTS
- const unsigned char * der :: DER data to decode.

- int der_len :: Length of DER data to decode.

- int * len :: Output variable containing the length of the DER length
  field.

* DESCRIPTION
Extract a length field from DER data.

* RETURNS
Return the decoded length value, or -1 on indefinite length, or -2 when
the value was too big to fit in a int, or -4 when the decoded length
value plus /len/ would exceed /der_len/ .

* COPYRIGHT
Copyright © 2006-2021 Free Software Foundation, Inc..\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libtasn1* is maintained as a Texinfo manual.
If the *info* and *libtasn1* programs are properly installed at your
site, the command

#+begin_quote
  *info libtasn1*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libtasn1/manual/*
#+end_quote
