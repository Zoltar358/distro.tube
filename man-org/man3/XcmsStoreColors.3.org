#+TITLE: Manpages - XcmsStoreColors.3
#+DESCRIPTION: Linux manpage for XcmsStoreColors.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XcmsStoreColors.3 is found in manpage for: [[../man3/XcmsStoreColor.3][man3/XcmsStoreColor.3]]