#+TITLE: Manpages - keyctl_invalidate.3
#+DESCRIPTION: Linux manpage for keyctl_invalidate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_invalidate - invalidate a key

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_invalidate(key_serial_t key);
#+end_example

* DESCRIPTION
*keyctl_invalidate*() invalidates a /key/. The key is scheduled for
immediate removal from all the keyrings that point to it, after which it
will be deleted. The key will be ignored by all searches once this
function is called even if it is not yet fully dealt with.

The caller must have *search* permission on a key to be able to
invalidate it.

* RETURN VALUE
On success *keyctl_invalidate*() returns *0*. On error, the value *-1*
will be returned and /errno/ will have been set to an appropriate error.

* ERRORS
- *ENOKEY* :: The key specified is invalid.

- *EKEYEXPIRED* :: The key specified has expired.

- *EKEYREVOKED* :: The key specified had been revoked.

- *EACCES* :: The key exists, but is not *searchable* by the calling
  process.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *add_key*(2), *keyctl*(2), *request_key*(2), *keyctl*(3),
*keyrings*(7), *keyutils*(7)
