#+TITLE: Manpages - CURLOPT_LOCALPORTRANGE.3
#+DESCRIPTION: Linux manpage for CURLOPT_LOCALPORTRANGE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_LOCALPORTRANGE - number of additional local ports to try

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_LOCALPORTRANGE, long
range);

* DESCRIPTION
Pass a long. The /range/ argument is the number of attempts libcurl will
make to find a working local port number. It starts with the given
/CURLOPT_LOCALPORT(3)/ and adds one to the number for each retry.
Setting this option to 1 or below will make libcurl do only one try for
the exact port number. Port numbers by nature are scarce resources that
will be busy at times so setting this value to something too low might
cause unnecessary connection setup failures.

* DEFAULT
1

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
    curl_easy_setopt(curl, CURLOPT_LOCALPORT, 49152L);
    /* and try 20 more ports following that */
    curl_easy_setopt(curl, CURLOPT_LOCALPORTRANGE, 20L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.15.2

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_LOCALPORT*(3), *CURLOPT_INTERFACE*(3),
