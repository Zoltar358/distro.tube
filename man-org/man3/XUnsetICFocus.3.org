#+TITLE: Manpages - XUnsetICFocus.3
#+DESCRIPTION: Linux manpage for XUnsetICFocus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XUnsetICFocus.3 is found in manpage for: [[../man3/XSetICFocus.3][man3/XSetICFocus.3]]