#+TITLE: Manpages - CURLOPT_IGNORE_CONTENT_LENGTH.3
#+DESCRIPTION: Linux manpage for CURLOPT_IGNORE_CONTENT_LENGTH.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_IGNORE_CONTENT_LENGTH - ignore content length

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_IGNORE_CONTENT_LENGTH,
                            long ignore);
#+end_example

* DESCRIPTION
If /ignore/ is set to 1L, ignore the Content-Length header in the HTTP
response and ignore asking for or relying on it for FTP transfers.

This is useful for HTTP with Apache 1.x (and similar servers) which will
report incorrect content length for files over 2 gigabytes. If this
option is used, curl will not be able to accurately report progress, and
will simply stop the download when the server ends the connection.

It is also useful with FTP when for example the file is growing while
the transfer is in progress which otherwise will unconditionally cause
libcurl to report error.

Only use this option if strictly necessary.

* DEFAULT
0

* PROTOCOLS
HTTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* we know the server is silly, ignore content-length */
    curl_easy_setopt(curl, CURLOPT_IGNORE_CONTENT_LENGTH, 1L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.14.1. Support for FTP added in 7.46.0. This option is not
working for HTTP when libcurl is built to use the hyper backend.

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_HTTP_VERSION*(3), *CURLOPT_MAXFILESIZE_LARGE*(3),
