#+TITLE: Manpages - strtoll.3
#+DESCRIPTION: Linux manpage for strtoll.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strtoll.3 is found in manpage for: [[../man3/strtol.3][man3/strtol.3]]