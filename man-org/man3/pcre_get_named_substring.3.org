#+TITLE: Manpages - pcre_get_named_substring.3
#+DESCRIPTION: Linux manpage for pcre_get_named_substring.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

#+begin_example
  int pcre_get_named_substring(const pcre *code,
   const char *subject, int *ovector,
   int stringcount, const char *stringname,
   const char **stringptr);

  int pcre16_get_named_substring(const pcre16 *code,
   PCRE_SPTR16 subject, int *ovector,
   int stringcount, PCRE_SPTR16 stringname,
   PCRE_SPTR16 *stringptr);

  int pcre32_get_named_substring(const pcre32 *code,
   PCRE_SPTR32 subject, int *ovector,
   int stringcount, PCRE_SPTR32 stringname,
   PCRE_SPTR32 *stringptr);
#+end_example

* DESCRIPTION
This is a convenience function for extracting a captured substring by
name. The arguments are:

/code/ Compiled pattern /subject/ Subject that has been successfully
matched /ovector/ Offset vector that *pcre[16|32]_exec()* used
/stringcount/ Value returned by *pcre[16|32]_exec()* /stringname/ Name
of the required substring /stringptr/ Where to put the string pointer

The memory in which the substring is placed is obtained by calling
*pcre[16|32]_malloc()*. The convenience function
*pcre[16|32]_free_substring()* can be used to free it when it is no
longer needed. The yield of the function is the length of the extracted
substring, PCRE_ERROR_NOMEMORY if sufficient memory could not be
obtained, or PCRE_ERROR_NOSUBSTRING if the string name is invalid.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
