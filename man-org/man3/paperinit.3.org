#+TITLE: Manpages - paperinit.3
#+DESCRIPTION: Linux manpage for paperinit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
paperinit, paperdone - begin and end using the paper library

* SYNOPSYS
#+begin_example
  #include <paper.h>

  void paperinit(void)
  void paperdone(void)
#+end_example

* DESCRIPTION
*paperinit()* initializes the paper library, allocating any resources
that are necessary for its use. This function must be called before any
other function of the paper library is used.

*paperdone()* frees any resources that were allocated for the paper
library. No function of the library should be called after a call to
this function.

* SEE ALSO
*defaultpapername*(3), *paperinfo*(3)\\
*papersize*(5)
