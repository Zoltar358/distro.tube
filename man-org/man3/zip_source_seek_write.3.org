#+TITLE: Manpages - zip_source_seek_write.3
#+DESCRIPTION: Linux manpage for zip_source_seek_write.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

sets the current write offset for

Just like in

depending on the

argument, the

is counted relative from:

start of file

current write offset in file

end of file

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

was added in libzip 1.0.

and
