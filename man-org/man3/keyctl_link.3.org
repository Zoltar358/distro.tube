#+TITLE: Manpages - keyctl_link.3
#+DESCRIPTION: Linux manpage for keyctl_link.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_link, keyctl_unlink - link/unlink a key to/from a keyring

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_link(key_serial_t key, key_serial_t keyring);

  long keyctl_unlink(key_serial_t key, key_serial_t keyring);
#+end_example

* DESCRIPTION
*keyctl_link*() creates a link from /keyring/ to /key/, displacing any
link to another key of the same type and description in that keyring if
one exists.

*keyctl_unlink*() removes the link from /keyring/ to /key/ if it exists.

The caller must have *write* permission on a keyring to be able to
create or remove links in it.

The caller must have *link* permission on a key to be able to create a
link to it.

* RETURN VALUE
On success *keyctl_link*() and *keyctl_unlink*() return *0*. On error,
the value *-1* will be returned and /errno/ will have been set to an
appropriate error.

* ERRORS
- *ENOKEY* :: The key or the keyring specified are invalid.

- *EKEYEXPIRED* :: The key or the keyring specified have expired.

- *EKEYREVOKED* :: The key or the keyring specified have been revoked.

- *EACCES* :: The keyring exists, but is not *writable* by the calling
  process.

For *keyctl_link*() only:

- *ENOMEM* :: Insufficient memory to expand the keyring

- *EDQUOT* :: Expanding the keyring would exceed the keyring owner's
  quota.

- *EACCES* :: The key exists, but is not *linkable* by the calling
  process.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *add_key*(2), *keyctl*(2), *request_key*(2), *keyctl*(3),
*keyrings*(7), *keyutils*(7)
