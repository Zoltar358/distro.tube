#+TITLE: Manpages - klogctl.3
#+DESCRIPTION: Linux manpage for klogctl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about klogctl.3 is found in manpage for: [[../man2/syslog.2][man2/syslog.2]]