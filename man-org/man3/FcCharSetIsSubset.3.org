#+TITLE: Manpages - FcCharSetIsSubset.3
#+DESCRIPTION: Linux manpage for FcCharSetIsSubset.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetIsSubset - Test for charset inclusion

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcCharSetIsSubset (const FcCharSet */a/*, const FcCharSet
**/b/*);*

* DESCRIPTION
Returns whether /a/ is a subset of /b/.
