#+TITLE: Manpages - std_chrono__steady_clock.3
#+DESCRIPTION: Linux manpage for std_chrono__steady_clock.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::chrono::steady_clock - Monotonic clock.

* SYNOPSIS
\\

** Public Types
typedef *chrono::nanoseconds* *duration*\\

typedef duration::period *period*\\

typedef duration::rep *rep*\\

typedef *chrono::time_point*< *steady_clock*, *duration* >
*time_point*\\

** Static Public Member Functions
static *time_point* *now* () noexcept\\

** Static Public Attributes
static constexpr bool *is_steady*\\

* Detailed Description
Monotonic clock.

Time returned has the property of only increasing at a uniform rate.

Definition at line *1141* of file *std/chrono*.

* Member Typedef Documentation
** typedef *chrono::nanoseconds* *std::chrono::steady_clock::duration*
Definition at line *1143* of file *std/chrono*.

** typedef duration::period std::chrono::steady_clock::period
Definition at line *1145* of file *std/chrono*.

** typedef duration::rep std::chrono::steady_clock::rep
Definition at line *1144* of file *std/chrono*.

** typedef *chrono::time_point*<*steady_clock*, *duration*>
*std::chrono::steady_clock::time_point*
Definition at line *1146* of file *std/chrono*.

* Member Data Documentation
** constexpr bool std::chrono::steady_clock::is_steady= [static]=,
= [constexpr]=
Definition at line *1148* of file *std/chrono*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
