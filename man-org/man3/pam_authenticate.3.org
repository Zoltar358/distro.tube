#+TITLE: Manpages - pam_authenticate.3
#+DESCRIPTION: Linux manpage for pam_authenticate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_authenticate - account authentication

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

*int pam_authenticate(pam_handle_t **/pamh/*, int */flags/*);*

* DESCRIPTION
The *pam_authenticate* function is used to authenticate the user. The
user is required to provide an authentication token depending upon the
authentication service, usually this is a password, but could also be a
finger print.

The PAM service module may request that the user enter their username
via the conversation mechanism (see *pam_start*(3) and *pam_conv*(3)).
The name of the authenticated user will be present in the PAM item
PAM_USER. This item may be recovered with a call to *pam_get_item*(3).

The /pamh/ argument is an authentication handle obtained by a prior call
to pam_start(). The flags argument is the binary or of zero or more of
the following values:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

PAM_DISALLOW_NULL_AUTHTOK

#+begin_quote
  The PAM module service should return PAM_AUTH_ERR if the user does not
  have a registered authentication token.
#+end_quote

* RETURN VALUES
PAM_ABORT

#+begin_quote
  The application should exit immediately after calling *pam_end*(3)
  first.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  The user was not authenticated.
#+end_quote

PAM_CRED_INSUFFICIENT

#+begin_quote
  For some reason the application does not have sufficient credentials
  to authenticate the user.
#+end_quote

PAM_AUTHINFO_UNAVAIL

#+begin_quote
  The modules were not able to access the authentication information.
  This might be due to a network or hardware failure etc.
#+end_quote

PAM_MAXTRIES

#+begin_quote
  One or more of the authentication modules has reached its limit of
  tries authenticating the user. Do not try again.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The user was successfully authenticated.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User unknown to authentication service.
#+end_quote

* SEE ALSO
*pam_start*(3), *pam_setcred*(3), *pam_chauthtok*(3), *pam_strerror*(3),
*pam*(8)
