#+TITLE: Manpages - FcFreeTypeCharIndex.3
#+DESCRIPTION: Linux manpage for FcFreeTypeCharIndex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFreeTypeCharIndex - map Unicode to glyph id

* SYNOPSIS
*#include <fontconfig.h>* #include <fcfreetype.h>

FT_UInt FcFreeTypeCharIndex (FT_Face /face/*, FcChar32 */ucs4/*);*

* DESCRIPTION
Maps a Unicode char to a glyph index. This function uses information
from several possible underlying encoding tables to work around broken
fonts. As a result, this function isn't designed to be used in
performance sensitive areas; results from this function are intended to
be cached by higher level functions.
