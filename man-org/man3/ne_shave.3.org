#+TITLE: Manpages - ne_shave.3
#+DESCRIPTION: Linux manpage for ne_shave.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_shave - trim whitespace from a string

* SYNOPSIS
#+begin_example
  #include <ne_string.h>
#+end_example

*char *ne_shave(char **/str/*, const char **/whitespace/*);*

* DESCRIPTION
*ne_shave* returns a portion of /str/ with any leading or trailing
characters in the /whitespace/ array removed. /str/ may be modified.
Note that the return value may not be equal to /str/.

* EXAMPLES
The following code segment will output "fish":

#+begin_quote
  #+begin_example
    char s[] = ".!.fish!.!";
    puts(ne_shave(s, ".!"));
  #+end_example
#+end_quote

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
