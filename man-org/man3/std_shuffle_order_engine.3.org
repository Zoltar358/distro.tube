#+TITLE: Manpages - std_shuffle_order_engine.3
#+DESCRIPTION: Linux manpage for std_shuffle_order_engine.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::shuffle_order_engine< _RandomNumberEngine, __k > - Produces random
numbers by reordering random numbers from some base engine.

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
template<typename _Sseq > using *_If_seed_seq* = typename *enable_if*<
__detail::__is_seed_seq< _Sseq, *shuffle_order_engine*, *result_type*
>::value >::type\\

typedef _RandomNumberEngine::result_type *result_type*\\

** Public Member Functions
*shuffle_order_engine* ()\\
Constructs a default shuffle_order_engine engine.

*shuffle_order_engine* (_RandomNumberEngine &&__rng)\\
Move constructs a shuffle_order_engine engine.

template<typename _Sseq , typename = _If_seed_seq<_Sseq>>
*shuffle_order_engine* (_Sseq &__q)\\
Generator construct a shuffle_order_engine engine.

*shuffle_order_engine* (const _RandomNumberEngine &__rng)\\
Copy constructs a shuffle_order_engine engine.

*shuffle_order_engine* (*result_type* __s)\\
Seed constructs a shuffle_order_engine engine.

const _RandomNumberEngine & *base* () const noexcept\\

void *discard* (unsigned long long __z)\\

*result_type* *operator()* ()\\

void *seed* ()\\
Reseeds the shuffle_order_engine object with the default seed for the
underlying base class generator engine.

template<typename _Sseq > _If_seed_seq< _Sseq > *seed* (_Sseq &__q)\\
Reseeds the shuffle_order_engine object with the given seed sequence.

void *seed* (*result_type* __s)\\
Reseeds the shuffle_order_engine object with the default seed for the
underlying base class generator engine.

** Static Public Member Functions
static constexpr *result_type* *max* ()\\

static constexpr *result_type* *min* ()\\

** Static Public Attributes
static constexpr size_t *table_size*\\

** Friends
template<typename _RandomNumberEngine1 , size_t __k1, typename _CharT ,
typename _Traits > *std::basic_ostream*< _CharT, _Traits > &
*operator<<* (*std::basic_ostream*< _CharT, _Traits > &__os, const
*std::shuffle_order_engine*< _RandomNumberEngine1, __k1 > &__x)\\
Inserts the current state of a shuffle_order_engine random number
generator engine =__x= into the output stream =__os=.

bool *operator==* (const *shuffle_order_engine* &__lhs, const
*shuffle_order_engine* &__rhs)\\

template<typename _RandomNumberEngine1 , size_t __k1, typename _CharT ,
typename _Traits > *std::basic_istream*< _CharT, _Traits > &
*operator>>* (*std::basic_istream*< _CharT, _Traits > &__is,
*std::shuffle_order_engine*< _RandomNumberEngine1, __k1 > &__x)\\
Extracts the current state of a % subtract_with_carry_engine random
number generator engine =__x= from the input stream =__is=.

* Detailed Description
** "template<typename _RandomNumberEngine, size_t __k>
\\
class std::shuffle_order_engine< _RandomNumberEngine, __k >"Produces
random numbers by reordering random numbers from some base engine.

The values from the base engine are stored in a sequence of size =__k=
and shuffled by an algorithm that depends on those values.

Definition at line *1326* of file *random.h*.

* Member Typedef Documentation
** template<typename _RandomNumberEngine , size_t __k> template<typename
_Sseq > using *std::shuffle_order_engine*< _RandomNumberEngine, __k
>::_If_seed_seq = typename *enable_if*<__detail::__is_seed_seq< _Sseq,
*shuffle_order_engine*, *result_type*>::value>::type
Definition at line *1336* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k> typedef
_RandomNumberEngine::result_type *std::shuffle_order_engine*<
_RandomNumberEngine, __k >::*result_type*
The type of the generated random value.

Definition at line *1333* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RandomNumberEngine , size_t __k>
*std::shuffle_order_engine*< _RandomNumberEngine, __k
>::*shuffle_order_engine* ()= [inline]=
Constructs a default shuffle_order_engine engine. The underlying engine
is default constructed as well.

Definition at line *1346* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k>
*std::shuffle_order_engine*< _RandomNumberEngine, __k
>::*shuffle_order_engine* (const _RandomNumberEngine &
__rng)= [inline]=, = [explicit]=
Copy constructs a shuffle_order_engine engine. Copies an existing base
class random number generator.

*Parameters*

#+begin_quote
  /__rng/ An existing (base class) engine object.
#+end_quote

Definition at line *1357* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k>
*std::shuffle_order_engine*< _RandomNumberEngine, __k
>::*shuffle_order_engine* (_RandomNumberEngine && __rng)= [inline]=,
= [explicit]=
Move constructs a shuffle_order_engine engine. Copies an existing base
class random number generator.

*Parameters*

#+begin_quote
  /__rng/ An existing (base class) engine object.
#+end_quote

Definition at line *1368* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k>
*std::shuffle_order_engine*< _RandomNumberEngine, __k
>::*shuffle_order_engine* (*result_type* __s)= [inline]=, = [explicit]=
Seed constructs a shuffle_order_engine engine. Constructs the underlying
generator engine seeded with =__s=.

*Parameters*

#+begin_quote
  /__s/ A seed value for the base class engine.
#+end_quote

Definition at line *1379* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k> template<typename
_Sseq , typename = _If_seed_seq<_Sseq>> *std::shuffle_order_engine*<
_RandomNumberEngine, __k >::*shuffle_order_engine* (_Sseq &
__q)= [inline]=, = [explicit]=
Generator construct a shuffle_order_engine engine.

*Parameters*

#+begin_quote
  /__q/ A seed sequence.
#+end_quote

Definition at line *1390* of file *random.h*.

* Member Function Documentation
** template<typename _RandomNumberEngine , size_t __k> const
_RandomNumberEngine & *std::shuffle_order_engine*< _RandomNumberEngine,
__k >::base () const= [inline]=, = [noexcept]=
Gets a const reference to the underlying generator engine object.

Definition at line *1433* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k> void
*std::shuffle_order_engine*< _RandomNumberEngine, __k >::discard
(unsigned long long __z)= [inline]=
Discard a sequence of random numbers.

Definition at line *1454* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k> static constexpr
*result_type* *std::shuffle_order_engine*< _RandomNumberEngine, __k
>::max ()= [inline]=, = [static]=, = [constexpr]=
Gets the maximum value in the generated random number range.

Definition at line *1447* of file *random.h*.

References *std::max()*.

** template<typename _RandomNumberEngine , size_t __k> static constexpr
*result_type* *std::shuffle_order_engine*< _RandomNumberEngine, __k
>::min ()= [inline]=, = [static]=, = [constexpr]=
Gets the minimum value in the generated random number range.

Definition at line *1440* of file *random.h*.

References *std::min()*.

** template<typename _RandomNumberEngine , size_t __k>
*shuffle_order_engine*< _RandomNumberEngine, __k >::*result_type*
*std::shuffle_order_engine*< _RandomNumberEngine, __k >::operator()
Gets the next value in the generated random number sequence.

Definition at line *837* of file *bits/random.tcc*.

References *std::max()*, and *std::min()*.

** template<typename _RandomNumberEngine , size_t __k> void
*std::shuffle_order_engine*< _RandomNumberEngine, __k >::seed
()= [inline]=
Reseeds the shuffle_order_engine object with the default seed for the
underlying base class generator engine.

Definition at line *1399* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k> template<typename
_Sseq > _If_seed_seq< _Sseq > *std::shuffle_order_engine*<
_RandomNumberEngine, __k >::seed (_Sseq & __q)= [inline]=
Reseeds the shuffle_order_engine object with the given seed sequence.

*Parameters*

#+begin_quote
  /__q/ A seed generator function.
#+end_quote

Definition at line *1423* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k> void
*std::shuffle_order_engine*< _RandomNumberEngine, __k >::seed
(*result_type* __s)= [inline]=
Reseeds the shuffle_order_engine object with the default seed for the
underlying base class generator engine.

Definition at line *1410* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RandomNumberEngine , size_t __k> template<typename
_RandomNumberEngine1 , size_t __k1, typename _CharT , typename _Traits >
*std::basic_ostream*< _CharT, _Traits > & operator<<
(*std::basic_ostream*< _CharT, _Traits > & __os, const
*std::shuffle_order_engine*< _RandomNumberEngine1, __k1 > &
__x)= [friend]=
Inserts the current state of a shuffle_order_engine random number
generator engine =__x= into the output stream =__os=.

*Parameters*

#+begin_quote
  /__os/ An output stream.\\
  /__x/ A shuffle_order_engine random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The output stream with the state of =__x= inserted or in an error
  state.
#+end_quote

** template<typename _RandomNumberEngine , size_t __k> bool operator==
(const *shuffle_order_engine*< _RandomNumberEngine, __k > & __lhs, const
*shuffle_order_engine*< _RandomNumberEngine, __k > & __rhs)= [friend]=
Compares two shuffle_order_engine random number generator objects of the
same type for equality.

*Parameters*

#+begin_quote
  /__lhs/ A shuffle_order_engine random number generator object.\\
  /__rhs/ Another shuffle_order_engine random number generator object.
#+end_quote

*Returns*

#+begin_quote
  true if the infinite sequences of generated values would be equal,
  false otherwise.
#+end_quote

Definition at line *1478* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __k> template<typename
_RandomNumberEngine1 , size_t __k1, typename _CharT , typename _Traits >
*std::basic_istream*< _CharT, _Traits > & operator>>
(*std::basic_istream*< _CharT, _Traits > & __is,
*std::shuffle_order_engine*< _RandomNumberEngine1, __k1 > &
__x)= [friend]=
Extracts the current state of a % subtract_with_carry_engine random
number generator engine =__x= from the input stream =__is=.

*Parameters*

#+begin_quote
  /__is/ An input stream.\\
  /__x/ A shuffle_order_engine random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The input stream with the state of =__x= extracted or in an error
  state.
#+end_quote

* Member Data Documentation
** template<typename _RandomNumberEngine , size_t __k> constexpr size_t
*std::shuffle_order_engine*< _RandomNumberEngine, __k
>::table_size= [static]=, = [constexpr]=
Definition at line *1339* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
