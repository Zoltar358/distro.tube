#+TITLE: Manpages - WWW_AUR_PKGBUILD.3pm
#+DESCRIPTION: Linux manpage for WWW_AUR_PKGBUILD.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
WWW::AUR::PKGBUILD - Parse PKGBUILD files created for makepkg

* SYNOPSIS
use WWW::AUR::PKGBUILD; # Read a PKGBUILD from a file handle... open my
$fh, <, PKGBUILD or die "open: $!"; my $pb = WWW::AUR::PKGBUILD->new(
$fh ); close $fh; # Or read from text my $pbtext = do { local (@ARGV,
$/) = PKGBUILD; <> }; my $pbobj = WWW::AUR::PKGBUILD->new( $pbtext ); my
%pb = $pbobj->fields(); # Array fields are converted into arrayrefs...
my $deps = join q{, }, @{ $pb{depends} }; my %pb = $pb->fields(); print
<<"END_PKGBUILD"; pkgname = $pb{pkgname} pkgver = $pb{pkgver} pkgdesc =
$pb{pkgdesc} depends = $deps END_PKGBUILD # There are also method
accessors for all fields

* DESCRIPTION
This class reads the text contents of a PKGBUILD file and does some
primitive parsing. PKGBUILD fields (ie pkgname, pkgver, pkgdesc) are
extracted into a hash. Bash arrays are extracted into an arrayref (ie
depends, makedepends, source).

Remember, bash is more lenient about using arrays than perl is. Bash
treats one-element arrays the same as non-array parameters and
vice-versa. Perl doesn't. I might use a module to copy bash's behavior
later on.

* CONSTRUCTOR
$OBJ = WWW::AUR::PKGBUILD->new( $PBTEXT | $PBFILE );

All this does is create a new *WWW::AUR::PKGBUILD* object and then call
the read method with the provided arguments.

- $PBTEXT :: A scalar containing the text of a PKGBUILD file.

- $PBFILE :: A filehandle of an open PKGBUILD file.

* METHODS
** fields
%PBFIELDS = $OBJ->fields();

- %PBFIELDS :: The fields and values of the PKGBUILD. Bash arrays (those
  values defined with parenthesis around them) are converted to array
  references.

** read
%PBFIELDS = $OBJ->read( $PBTEXT | $PBFILE );

- $PBTEXT :: A scalar containing the text of a PKGBUILD file.

- $PBFILE :: A filehandle of an open PKGBUILD file.

- %PBFIELDS :: The fields and values of the PKGBUILD. Bash arrays (those
  values defined with parenthesis around them) are converted to array
  references.

** PKGBUILD Field Accessors
undef | $TEXT | $AREF = ( $OBJ->pkgname | $OBJ->pkgver | $OBJ->pkgdesc |
$OBJ->url | $OBJ->license | $OBJ->install | $OBJ->changelog |
$OBJ->source | $OBJ->noextract | $OBJ->md5sums | $OBJ->sha1sums |
$OBJ->sha256sums | $OBJ->sha384sums | $OBJ->sha512sums | $OBJ->groups |
$OBJ->arch | $OBJ->backup | $OBJ->depends | $OBJ->makedepends |
$OBJ->optdepends | $OBJ->conflicts | $OBJ->provides | $OBJ->replaces |
$OBJ->options )

Each standard field of a PKGBUILD can be accessed by using one of these
accessors. The fields method returns a hashref containing ALL bash
variables defined globally.

- "undef" :: If the field was not defined in the PKGBUILD undef is
  returned.

- $TEXT :: If a field is defined but is not a bash array it is returned
  as a scalar text value.

- $AREF :: If a field is defined as a bash array (with parenthesis) it
  is returned as an array reference.

* SEE ALSO

- WWW::AUR::Package::File

- <http://www.archlinux.org/pacman/PKGBUILD.5.html>

* AUTHOR
Justin Davis, =<juster at cpan dot org>=

* BUGS
Please email me any bugs you find. I will try to fix them as quick as I
can.

* SUPPORT
Send me an email if you have any questions or need help.

* LICENSE AND COPYRIGHT
Copyright 2014 Justin Davis.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
