#+TITLE: Manpages - std__Temporary_buffer.3
#+DESCRIPTION: Linux manpage for std__Temporary_buffer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Temporary_buffer< _ForwardIterator, _Tp >

* SYNOPSIS
\\

=#include <stl_tempbuf.h>=

Inherited by *__gnu_cxx::temporary_buffer< _ForwardIterator, _Tp >*.

** Public Types
typedef pointer *iterator*\\

typedef value_type * *pointer*\\

typedef ptrdiff_t *size_type*\\

typedef _Tp *value_type*\\

** Public Member Functions
*_Temporary_buffer* (_ForwardIterator __seed, size_type
__original_len)\\

iterator *begin* ()\\
As per Table mumble.

iterator *end* ()\\
As per Table mumble.

size_type *requested_size* () const\\
Returns the size requested by the constructor; may be >size().

size_type *size* () const\\
As per Table mumble.

** Protected Attributes
pointer *_M_buffer*\\

size_type *_M_len*\\

size_type *_M_original_len*\\

* Detailed Description
** "template<typename _ForwardIterator, typename _Tp>
\\
class std::_Temporary_buffer< _ForwardIterator, _Tp >"This class is used
in two places: stl_algo.h and ext/memory, where it is wrapped as the
temporary_buffer class. See temporary_buffer docs for more notes.

Definition at line *136* of file *stl_tempbuf.h*.

* Member Typedef Documentation
** template<typename _ForwardIterator , typename _Tp > typedef pointer
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::iterator
Definition at line *144* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > typedef
value_type* *std::_Temporary_buffer*< _ForwardIterator, _Tp >::pointer
Definition at line *143* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > typedef ptrdiff_t
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::size_type
Definition at line *145* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > typedef _Tp
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::value_type
Definition at line *142* of file *stl_tempbuf.h*.

* Constructor & Destructor Documentation
** template<typename _ForwardIterator , typename _Tp >
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::*_Temporary_buffer*
(_ForwardIterator __seed, size_type __original_len)
Constructs a temporary buffer of a size somewhere between zero and the
given length.

Definition at line *257* of file *stl_tempbuf.h*.

References *std::pair< _T1, _T2 >::first*, and *std::pair< _T1, _T2
>::second*.

** template<typename _ForwardIterator , typename _Tp >
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::~*_Temporary_buffer*
()= [inline]=
Definition at line *179* of file *stl_tempbuf.h*.

* Member Function Documentation
** template<typename _ForwardIterator , typename _Tp > iterator
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::begin ()= [inline]=
As per Table mumble.

Definition at line *165* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > iterator
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::end ()= [inline]=
As per Table mumble.

Definition at line *170* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::requested_size ()
const= [inline]=
Returns the size requested by the constructor; may be >size().

Definition at line *160* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::size ()
const= [inline]=
As per Table mumble.

Definition at line *155* of file *stl_tempbuf.h*.

* Member Data Documentation
** template<typename _ForwardIterator , typename _Tp > pointer
*std::_Temporary_buffer*< _ForwardIterator, _Tp
>::_M_buffer= [protected]=
Definition at line *150* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp >::_M_len= [protected]=
Definition at line *149* of file *stl_tempbuf.h*.

** template<typename _ForwardIterator , typename _Tp > size_type
*std::_Temporary_buffer*< _ForwardIterator, _Tp
>::_M_original_len= [protected]=
Definition at line *148* of file *stl_tempbuf.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
