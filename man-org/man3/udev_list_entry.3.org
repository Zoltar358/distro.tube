#+TITLE: Manpages - udev_list_entry.3
#+DESCRIPTION: Linux manpage for udev_list_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
udev_list_entry, udev_list_entry_get_next, udev_list_entry_get_by_name,
udev_list_entry_get_name, udev_list_entry_get_value - Iterate and access
udev lists

* SYNOPSIS
#+begin_example
  #include <libudev.h>
#+end_example

*struct udev_list_entry *udev_list_entry_get_next(struct udev_list_entry
**/list_entry/*);*

*struct udev_list_entry *udev_list_entry_get_by_name(struct
udev_list_entry **/list_entry/*, const char **/name/*);*

*const char *udev_list_entry_get_name(struct udev_list_entry
**/list_entry/*);*

*const char *udev_list_entry_get_value(struct udev_list_entry
**/list_entry/*);*

* RETURN VALUE
On success, *udev_list_entry_get_next()* and
*udev_list_entry_get_by_name()* return a pointer to the requested list
entry. If no such entry can be found, or on failure, *NULL* is returned.

On success, *udev_list_entry_get_name()* and
*udev_list_entry_get_value()* return a pointer to a constant string
representing the requested value. The string is bound to the lifetime of
the list entry itself. On failure, *NULL* is returned.

* SEE ALSO
*udev_new*(3), *udev_device_new_from_syspath*(3),
*udev_enumerate_new*(3), *udev_monitor_new_from_netlink*(3),
*systemd*(1),
