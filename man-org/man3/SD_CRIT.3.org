#+TITLE: Manpages - SD_CRIT.3
#+DESCRIPTION: Linux manpage for SD_CRIT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about SD_CRIT.3 is found in manpage for: [[../sd-daemon.3][sd-daemon.3]]