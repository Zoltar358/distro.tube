#+TITLE: Manpages - ares_set_local_ip4.3
#+DESCRIPTION: Linux manpage for ares_set_local_ip4.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_set_local_ip4 - Set local IPv4 address outgoing requests.

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_set_local_ip4(ares_channel channel, unsigned int local_ip)
#+end_example

* DESCRIPTION
The *ares_set_local_ip4* function sets the IP address for outbound
requests. The parameter /local_ip/ is specified in host byte order. This
allows users to specify outbound interfaces when used on multi-homed
systems.

* SEE ALSO
*ares_set_local_ip6*(3)

* NOTES
This function was added in c-ares 1.7.4

* AUTHOR
Ben Greear
