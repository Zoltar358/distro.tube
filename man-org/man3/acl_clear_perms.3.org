#+TITLE: Manpages - acl_clear_perms.3
#+DESCRIPTION: Linux manpage for acl_clear_perms.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function clears all permissions from the permission set referred to by
the argument

Any existing descriptors that refer to

shall continue to refer to that permission set.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid descriptor for a permission set within an ACL entry.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
