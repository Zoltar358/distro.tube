#+TITLE: Manpages - std_enable_shared_from_this.3
#+DESCRIPTION: Linux manpage for std_enable_shared_from_this.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::enable_shared_from_this< _Tp > - Base class allowing use of member
function shared_from_this.

* SYNOPSIS
\\

=#include <shared_ptr.h>=

** Public Member Functions
*shared_ptr*< _Tp > *shared_from_this* ()\\

*shared_ptr*< const _Tp > *shared_from_this* () const\\

*weak_ptr*< const _Tp > *weak_from_this* () const noexcept\\

*weak_ptr*< _Tp > *weak_from_this* () noexcept\\

** Protected Member Functions
*enable_shared_from_this* (const *enable_shared_from_this* &) noexcept\\

*enable_shared_from_this* & *operator=* (const *enable_shared_from_this*
&) noexcept\\

** Friends
const *enable_shared_from_this* * *__enable_shared_from_this_base*
(const __shared_count<> &, const *enable_shared_from_this* *__p)\\

template<typename , _Lock_policy > class *__shared_ptr*\\

* Detailed Description
** "template<typename _Tp>
\\
class std::enable_shared_from_this< _Tp >"Base class allowing use of
member function shared_from_this.

Definition at line *792* of file *bits/shared_ptr.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > constexpr *std::enable_shared_from_this*< _Tp
>::*enable_shared_from_this* ()= [inline]=, = [constexpr]=,
= [protected]=, = [noexcept]=
Definition at line *795* of file *bits/shared_ptr.h*.

** template<typename _Tp > *std::enable_shared_from_this*< _Tp
>::*enable_shared_from_this* (const *enable_shared_from_this*< _Tp >
&)= [inline]=, = [protected]=, = [noexcept]=
Definition at line *797* of file *bits/shared_ptr.h*.

** template<typename _Tp > *std::enable_shared_from_this*< _Tp
>::~*enable_shared_from_this* ()= [inline]=, = [protected]=
Definition at line *803* of file *bits/shared_ptr.h*.

* Member Function Documentation
** template<typename _Tp > *enable_shared_from_this* &
*std::enable_shared_from_this*< _Tp >::operator= (const
*enable_shared_from_this*< _Tp > &)= [inline]=, = [protected]=,
= [noexcept]=
Definition at line *800* of file *bits/shared_ptr.h*.

** template<typename _Tp > *shared_ptr*< _Tp >
*std::enable_shared_from_this*< _Tp >::shared_from_this ()= [inline]=
Definition at line *807* of file *bits/shared_ptr.h*.

** template<typename _Tp > *shared_ptr*< const _Tp >
*std::enable_shared_from_this*< _Tp >::shared_from_this ()
const= [inline]=
Definition at line *811* of file *bits/shared_ptr.h*.

** template<typename _Tp > *weak_ptr*< const _Tp >
*std::enable_shared_from_this*< _Tp >::weak_from_this ()
const= [inline]=, = [noexcept]=
Definition at line *821* of file *bits/shared_ptr.h*.

** template<typename _Tp > *weak_ptr*< _Tp >
*std::enable_shared_from_this*< _Tp >::weak_from_this ()= [inline]=,
= [noexcept]=
Definition at line *817* of file *bits/shared_ptr.h*.

* Friends And Related Function Documentation
** template<typename _Tp > const *enable_shared_from_this* *
__enable_shared_from_this_base (const __shared_count<> &, const
*enable_shared_from_this*< _Tp > * __p)= [friend]=
Definition at line *833* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename , _Lock_policy > friend
class __shared_ptr= [friend]=
Definition at line *838* of file *bits/shared_ptr.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
