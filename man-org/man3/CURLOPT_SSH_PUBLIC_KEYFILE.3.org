#+TITLE: Manpages - CURLOPT_SSH_PUBLIC_KEYFILE.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSH_PUBLIC_KEYFILE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSH_PUBLIC_KEYFILE - public key file for SSH auth

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_PUBLIC_KEYFILE,
                            char *filename);
#+end_example

* DESCRIPTION
Pass a char * pointing to a /filename/ for your public key. If not used,
libcurl defaults to *$HOME/.ssh/id_dsa.pub* if the HOME environment
variable is set, and just "id_dsa.pub" in the current directory if HOME
is not set.

If NULL (or an empty string) is passed, libcurl will pass no public key
to libssh2, which then tries to compute it from the private key. This is
known to work with libssh2 1.4.0+ linked against OpenSSL.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
SFTP and SCP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
    curl_easy_setopt(curl, CURLOPT_SSH_PUBLIC_KEYFILE,
                     "/home/clarkkent/.ssh/id_rsa.pub");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
The "" trick was added in 7.26.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_SSH_PRIVATE_KEYFILE*(3), *CURLOPT_SSH_AUTH_TYPES*(3),
