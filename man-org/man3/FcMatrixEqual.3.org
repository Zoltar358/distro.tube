#+TITLE: Manpages - FcMatrixEqual.3
#+DESCRIPTION: Linux manpage for FcMatrixEqual.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcMatrixEqual - Compare two matrices

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcMatrixEqual (const FcMatrix */matrix1/*, const FcMatrix
**/matrix2/*);*

* DESCRIPTION
*FcMatrixEqual* compares /matrix1/ and /matrix2/ returning FcTrue when
they are equal and FcFalse when they are not.
