#+TITLE: Manpages - XFreeExtensionList.3
#+DESCRIPTION: Linux manpage for XFreeExtensionList.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFreeExtensionList.3 is found in manpage for: [[../man3/XQueryExtension.3][man3/XQueryExtension.3]]