#+TITLE: Manpages - SDL_WM_SetIcon.3
#+DESCRIPTION: Linux manpage for SDL_WM_SetIcon.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WM_SetIcon - Sets the icon for the display window.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_WM_SetIcon*(*SDL_Surface *icon, Uint8 *mask*);

* DESCRIPTION
Sets the icon for the display window. Win32 icons must be 32x32.

This function must be called before the first call to
/SDL_SetVideoMode/.

It takes an *icon* surface, and a *mask* in MSB format.

If *mask* is *NULL*, the entire icon surface will be used as the icon.

* EXAMPLE
#+begin_example
  SDL_WM_SetIcon(SDL_LoadBMP("icon.bmp"), NULL);
#+end_example

* SEE ALSO
*SDL_SetVideoMode*, *SDL_WM_SetCaption*
