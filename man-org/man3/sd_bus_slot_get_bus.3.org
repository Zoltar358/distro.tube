#+TITLE: Manpages - sd_bus_slot_get_bus.3
#+DESCRIPTION: Linux manpage for sd_bus_slot_get_bus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_slot_get_bus, sd_bus_slot_get_current_handler,
sd_bus_slot_get_current_message, sd_bus_slot_get_current_userdata -
Query information attached to a bus slot object

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*typedef int (*sd_bus_message_handler_t)(sd_bus_message **/m/*, void
**/userdata/*, sd_bus_error **/ret_error/*);*

*sd_bus *sd_bus_slot_get_bus(sd_bus_slot **/slot/*);*

*sd_bus_message_handler_t sd_bus_slot_get_current_handler(sd_bus_slot
**/slot/*);*

*sd_bus_message *sd_bus_slot_get_current_message(sd_bus_slot
**/slot/*);*

*void *sd_bus_slot_get_current_userdata(sd_bus_slot **/slot/*);*

* DESCRIPTION
*sd_bus_slot_get_bus()* returns the bus object that message /slot/ is
attached to.

*sd_bus_slot_get_current_handler()*, *sd_bus_slot_get_current_message()*
and *sd_bus_slot_get_current_userdata()* return the current handler,
message and userdata respectively of the bus /slot/ is attached to if
were currently executing the callback associated with /slot/.

* RETURN VALUE
*sd_bus_slot_get_bus()* always returns the bus object.

On success, *sd_bus_slot_get_current_handler()*,
*sd_bus_slot_get_current_message()* and
*sd_bus_slot_get_current_userdata()* return the requested object. On
failure, they return *NULL*.

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3),
