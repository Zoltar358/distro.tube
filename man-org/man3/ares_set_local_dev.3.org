#+TITLE: Manpages - ares_set_local_dev.3
#+DESCRIPTION: Linux manpage for ares_set_local_dev.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_set_local_dev - Bind to a specific network device when creating
sockets.

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_set_local_dev(ares_channel channel, const char* local_dev_name)
#+end_example

* DESCRIPTION
The *ares_set_local_dev* function causes all future sockets to be bound
to this device with SO_BINDTODEVICE. This forces communications to go
over a certain interface, which can be useful on multi-homed machines.
This option is only supported on Linux, and root privileges are required
for the option to work. If SO_BINDTODEVICE is not supported or the
setsocktop call fails (probably because of permissions), the error is
silently ignored.

* SEE ALSO
*ares_set_local_ip4*(3) *ares_set_local_ip6*(3)

* NOTES
This function was added in c-ares 1.7.4

* AUTHOR
Ben Greear
