#+TITLE: Manpages - forkpty.3
#+DESCRIPTION: Linux manpage for forkpty.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about forkpty.3 is found in manpage for: [[../man3/openpty.3][man3/openpty.3]]