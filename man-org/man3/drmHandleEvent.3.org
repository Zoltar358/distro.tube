#+TITLE: Manpages - drmHandleEvent.3
#+DESCRIPTION: Linux manpage for drmHandleEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
drmHandleEvent - read and process pending DRM events

* SYNOPSIS
*#include <xf86drm.h>*

*int drmHandleEvent(int fd, drmEventContextPtr evctx);*

* DESCRIPTION
*drmHandleEvent* processes outstanding DRM events on the DRM
file-descriptor passed as *fd*. This function should be called after the
DRM file-descriptor has polled readable; it will read the events and use
the passed-in *evctx* structure to call function pointers with the
parameters noted below:

#+begin_quote

  #+begin_quote
    #+begin_example
      typedef struct _drmEventContext {
          int version;
          void (*vblank_handler) (int fd,
                                  unsigned int sequence,
                                  unsigned int tv_sec,
                                  unsigned int tv_usec,
                                  void *user_data)
          void (*page_flip_handler) (int fd,
                                     unsigned int sequence,
                                     unsigned int tv_sec,
                                     unsigned int tv_usec,
                                     void *user_data)
      } drmEventContext, *drmEventContextPtr;
    #+end_example
  #+end_quote
#+end_quote

* RETURN VALUE
*drmHandleEvent* returns 0 on success, or if there is no data to read
from the file-descriptor. Returns -1 if the read on the file-descriptor
fails or returns less than a full event record.

* REPORTING BUGS
Bugs in this function should be reported to
/https://gitlab.freedesktop.org/mesa/drm/-/issues/

* SEE ALSO
*drm*(7), *drm-kms*(7), *drmModePageFlip*(3), *drmWaitVBlank*(3)
