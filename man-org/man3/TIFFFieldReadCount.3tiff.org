#+TITLE: Manpages - TIFFFieldReadCount.3tiff
#+DESCRIPTION: Linux manpage for TIFFFieldReadCount.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFFieldReadCount - Get number of values to be read from field

* SYNOPSIS
*#include <tiffio.h>*

*int TIFFFieldReadCount(const TIFFField* */fip/*)*

* DESCRIPTION
*TIFFFieldReadCount* returns the number of values available to be read
from the specified TIFF field; that is, the number of arguments that
should be supplied to *TIFFGetField*. For most field types this is a
small positive integer, typically 1 or 2, but there are some special
values:\\
*TIFF_VARIABLE* indicates that a variable number of values is possible;
then, a *uint16_t* /count/ argument and a pointer /data/ argument must
be supplied to *TIFFGetField*.\\
*TIFF_VARIABLE2* is the same as *TIFF_VARIABLE* except that the /count/
argument must have type *uint32_t*.\\
*TIFF_SPP* indicates that the number of arguments is equal to the
image's number of samples per pixel.

/fip/ is a field information pointer previously returned by
*TIFFFindField*, *TIFFFieldWithTag*, or *TIFFFieldWithName*.\\

* RETURN VALUES
\\
*TIFFFieldReadCount* returns an integer.\\

* SEE ALSO
*libtiff*(3TIFF),

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
