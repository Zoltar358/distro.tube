#+TITLE: Manpages - XkbSetAutoResetControls.3
#+DESCRIPTION: Linux manpage for XkbSetAutoResetControls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbSetAutoResetControls - Changes the current values of the AutoReset
control attributes

* SYNOPSIS
*Bool XkbSetAutoResetControls* *( Display **/dpy/* ,* *unsigned int
*/changes/* ,* *unsigned int **/auto_ctrls/* ,* *unsigned int
**/auto_values/* );*

* ARGUMENTS
- /- dpy/ :: connection to X server

- /- changes/ :: controls for which to change auto-reset values

- /- auto_ctrls/ :: controls from changes that should auto reset

- /- auto_values/ :: 1 bit => auto-reset on

* DESCRIPTION
/XkbSetAutoResetControls/ changes the auto-reset status and associated
auto-reset values for the controls selected by /changes./ For any
control selected by /changes,/ if the corresponding bit is set in
/auto_ctrls,/ the control is configured to auto-reset when the client
exits. If the corresponding bit in /auto_values/ is on, the control is
turned on when the client exits; if zero, the control is turned off when
the client exits. For any control selected by /changes,/ if the
corresponding bit is not set in /auto_ctrls,/ the control is configured
to not reset when the client exits.

For example:

#+begin_example
  To leave the auto-reset controls for StickyKeys the way they are:

  	ok = XkbSetAutoResetControls(dpy, 0, 0, 0);
  	
  To change the auto-reset controls so that StickyKeys are unaffected when the 
  client exits:

  	ok = XkbSetAutoResetControls(dpy, XkbStickyKeysMask, 0, 0);
  	
  To change the auto-reset controls so that StickyKeys are turned off when the 
  client exits:

  	ok = XkbSetAutoResetControls(dpy, XkbStickyKeysMask, XkbStickyKeysMask, 0);
  	
  To change the auto-reset controls so that StickyKeys are turned on when the 
  client exits:

  	ok = XkbSetAutoResetControls(dpy, XkbStickyKeysMask, XkbStickyKeysMask, 
  XkbStickyKeysMask);
#+end_example

/XkbSetAutoResetControls/ backfills /auto_ctrls/ and /auto_values/ with
the auto-reset controls for this particular client. Note that all of the
bits are valid in the returned values, not just the ones selected in the
/changes/ mask.
