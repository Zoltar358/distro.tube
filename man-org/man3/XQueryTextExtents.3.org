#+TITLE: Manpages - XQueryTextExtents.3
#+DESCRIPTION: Linux manpage for XQueryTextExtents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XQueryTextExtents.3 is found in manpage for: [[../man3/XTextExtents.3][man3/XTextExtents.3]]