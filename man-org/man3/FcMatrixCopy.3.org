#+TITLE: Manpages - FcMatrixCopy.3
#+DESCRIPTION: Linux manpage for FcMatrixCopy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcMatrixCopy - Copy a matrix

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcMatrixCopy (const FcMatrix */matrix/*);*

* DESCRIPTION
*FcMatrixCopy* allocates a new FcMatrix and copies /mat/ into it.
