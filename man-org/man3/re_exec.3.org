#+TITLE: Manpages - re_exec.3
#+DESCRIPTION: Linux manpage for re_exec.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about re_exec.3 is found in manpage for: [[../man3/re_comp.3][man3/re_comp.3]]