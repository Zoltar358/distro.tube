#+TITLE: Manpages - strnstr.3bsd
#+DESCRIPTION: Linux manpage for strnstr.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function locates the first occurrence of the null-terminated string

in the string

where not more than

characters are searched. Characters that appear after a

character are not searched. Since the

function is a

specific API, it should only be used when portability is not a concern.

If

is an empty string,

is returned; if

occurs nowhere in

is returned; otherwise a pointer to the first character of the first
occurrence of

is returned.

The following sets the pointer

to

because only the first 4 characters of

are searched:

const char *largestring = "Foo Bar Baz"; const char *smallstring =
"Bar"; char *ptr;

ptr = strnstr(largestring, smallstring, 4);
