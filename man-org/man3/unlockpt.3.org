#+TITLE: Manpages - unlockpt.3
#+DESCRIPTION: Linux manpage for unlockpt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
unlockpt - unlock a pseudoterminal master/slave pair

* SYNOPSIS
#+begin_example
  #define _XOPEN_SOURCE
  #include <stdlib.h>

  int unlockpt(int fd);
#+end_example

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*unlockpt*():

#+begin_example
      Since glibc 2.24:
          _XOPEN_SOURCE >= 500
      Glibc 2.23 and earlier:
          _XOPEN_SOURCE
#+end_example

* DESCRIPTION
The *unlockpt*() function unlocks the slave pseudoterminal device
corresponding to the master pseudoterminal referred to by the file
descriptor /fd/.

*unlockpt*() should be called before opening the slave side of a
pseudoterminal.

* RETURN VALUE
When successful, *unlockpt*() returns 0. Otherwise, it returns -1 and
sets /errno/ to indicate the error.

* ERRORS
- *EBADF* :: The /fd/ argument is not a file descriptor open for
  writing.

- *EINVAL* :: The /fd/ argument is not associated with a master
  pseudoterminal.

* VERSIONS
*unlockpt*() is provided in glibc since version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface    | Attribute     | Value   |
| *unlockpt*() | Thread safety | MT-Safe |

* CONFORMING TO
POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*grantpt*(3), *posix_openpt*(3), *ptsname*(3), *pts*(4), *pty*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
