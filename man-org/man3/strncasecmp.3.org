#+TITLE: Manpages - strncasecmp.3
#+DESCRIPTION: Linux manpage for strncasecmp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strncasecmp.3 is found in manpage for: [[../man3/strcasecmp.3][man3/strcasecmp.3]]