#+TITLE: Manpages - FcFontSetCreate.3
#+DESCRIPTION: Linux manpage for FcFontSetCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFontSetCreate - Create a font set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcFontSet * FcFontSetCreate (void*);*

* DESCRIPTION
Creates an empty font set.
