#+TITLE: Manpages - librasqal.3
#+DESCRIPTION: Linux manpage for librasqal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
librasqal - Rasqal RDF query library

* SYNOPSIS
#+begin_example
  #include <rasqal.h>


   


  rasqal_world*world=rasqal_new_world();


  rasqal_query_results *results;


  raptor_uri *base_uri=raptor_new_uri("http://example.org/foo");


  rasqal_query *rq=rasqal_new_query(world,"rdql",NULL);


  const char *query_string="select * from <http://example.org/data.rdf>";


   


  rasqal_query_prepare(rq,query_string,base_uri);


  results=rasqal_query_execute(rq);


  while(!rasqal_query_results_finished(results)) {


   for(i=0;i<rasqal_query_results_get_bindings_count(results);i++) {


   const char *name=rasqal_query_results_get_binding_name(results,i);


   rasqal_literal *value=rasqal_query_results_get_binding_value(results,i);


   /* ... */


   }


   rasqal_query_results_next(results);


  }


  rasqal_free_query_results(results);


  rasqal_free_query(rq);


  raptor_free_uri(base_uri);


  rasqal_free_world(world);



  cc prog.c -o prog `pkg-config rasqal --cflags` `pkg-config rasqal --libs`
#+end_example

* DESCRIPTION
The /Rasqal/ library provides a high-level interface to RDF query
parsing, query construction, query execution over an RDF graph and query
results manipulation and formatting. The library provides APIs to each
of the steps in the process and provides support for handling multiple
query language syntaxes. At present Rasqal supports most of the W3C
SPARQL 1.0 Query language, some of SPARQL 1.1 draft and fully supports
RDQL.

Rasqal uses the libraptor(3) library for providing URI handling, WWW
content retrieval and other support functions.

* API REFERENCE
See the HTML API docs that may be installed system wide at
/usr/share/gtk-doc/html/rasqal/ or on the web at
[[http://librdf.org/rasqal/docs/api/]]

* API CHANGES
See the Raptor API docs changes section at
[[http://librdf.org/rasqal/docs/api/rasqal-changes.html][http://librdf.org/rasqal/docs/api/rasqal-changes.html
]]

* CONFORMING TO
/SPARQL Query Language for RDF/, Eric Prud'hommeaux and Andy Seaborne
(eds), W3C Recommendation, 15 January 2008
[[http://www.w3.org/TR/2008/REC-rdf-sparql-query-20080115/]]

/SPARQL Query Results XML Format/, Jeen Broekstra and Dave Beckett
(eds), W3C Recommendation, 15 January 2008.
[[http://www.w3.org/TR/2008/REC-rdf-sparql-XMLres-20080115/]]

/RDQL - A Query Language for RDF/, Andy Seaborne, W3C Member Submission
9 January 2004 [[http://www.w3.org/Submission/2004/SUBM-RDQL-20040109/]]

* SEE ALSO
*roqet*(1),*libraptor(3)*

* AUTHOR
Dave Beckett - [[http://purl.org/net/dajobe/]]\\
