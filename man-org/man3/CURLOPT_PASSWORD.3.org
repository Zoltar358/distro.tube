#+TITLE: Manpages - CURLOPT_PASSWORD.3
#+DESCRIPTION: Linux manpage for CURLOPT_PASSWORD.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PASSWORD - password to use in authentication

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PASSWORD, char *pwd);

* DESCRIPTION
Pass a char * as parameter, which should be pointing to the
null-terminated password to use for the transfer.

The /CURLOPT_PASSWORD(3)/ option should be used in conjunction with the
/CURLOPT_USERNAME(3)/ option.

The application does not have to keep the string around after setting
this option.

* DEFAULT
blank

* PROTOCOLS
Most

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

    curl_easy_setopt(curl, CURLOPT_PASSWORD, "qwerty");

    ret = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.19.1

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_USERPWD*(3), *CURLOPT_USERNAME*(3), *CURLOPT_HTTPAUTH*(3),
*CURLOPT_PROXYAUTH*(3)
