#+TITLE: Manpages - std___detail__Insert_base.3
#+DESCRIPTION: Linux manpage for std___detail__Insert_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Insert_base< _Key, _Value, _Alloc, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherited by *std::__detail::_Insert< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, false >*,
and *std::__detail::_Insert< _Key, _Value, _Alloc, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, true >*.

** Public Types
using *__ireturn_type* = typename *std::conditional*<
__unique_keys::value, *std::pair*< *iterator*, bool >, *iterator*
>::type\\

using *const_iterator* = *_Node_const_iterator*< _Value,
__constant_iterators::value, __hash_cached::value >\\

using *iterator* = *_Node_iterator*< _Value,
__constant_iterators::value, __hash_cached::value >\\

** Public Member Functions
template<typename _InputIterator > void *insert* (_InputIterator
__first, _InputIterator __last)\\

__ireturn_type *insert* (const value_type &__v)\\

*iterator* *insert* (*const_iterator* __hint, const value_type &__v)\\

void *insert* (*initializer_list*< value_type > __l)\\

template<typename _KType , typename... _Args> *std::pair*< *iterator*,
bool > *try_emplace* (*const_iterator*, _KType &&__k, _Args &&...
__args)\\

** Protected Types
using *__constant_iterators* = typename _Traits::__constant_iterators\\

using *__hash_cached* = typename _Traits::__hash_cached\\

using *__hashtable* = *_Hashtable*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >\\

using *__hashtable_alloc* = *_Hashtable_alloc*< __alloc_rebind< _Alloc,
*_Hash_node*< _Value, __hash_cached::value > > >\\

using *__hashtable_base* = *_Hashtable_base*< _Key, _Value, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _Traits >\\

using *__node_alloc_type* = typename
__hashtable_alloc::__node_alloc_type\\

using *__node_gen_type* = _AllocNode< __node_alloc_type >\\

using *__unique_keys* = typename _Traits::__unique_keys\\

using *size_type* = typename __hashtable_base::size_type\\

using *value_type* = typename __hashtable_base::value_type\\

** Protected Member Functions
*__hashtable* & *_M_conjure_hashtable* ()\\

template<typename _InputIterator , typename _NodeGetter > void
*_M_insert_range* (_InputIterator __first, _InputIterator __last, const
_NodeGetter &, *false_type* __uks)\\

template<typename _InputIterator , typename _NodeGetter > void
*_M_insert_range* (_InputIterator __first, _InputIterator __last, const
_NodeGetter &, *true_type* __uks)\\

* Detailed Description
** "template<typename _Key, typename _Value, typename _Alloc, typename
_ExtractKey, typename _Equal, typename _Hash, typename _RangeHash,
typename _Unused, typename _RehashPolicy, typename _Traits>
\\
struct std::__detail::_Insert_base< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >"Primary
class template _Insert_base.

Defines =insert= member functions appropriate to all _Hashtables.

Definition at line *788* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::__constant_iterators = typename
_Traits::__constant_iterators= [protected]=
Definition at line *800* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::__hash_cached = typename _Traits::__hash_cached= [protected]=
Definition at line *799* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::*__hashtable* = *_Hashtable*<_Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy,
_Traits>= [protected]=
Definition at line *795* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::*__hashtable_alloc* = *_Hashtable_alloc*< __alloc_rebind<_Alloc,
*_Hash_node*<_Value, __hash_cached::value> >>= [protected]=
Definition at line *802* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::*__hashtable_base* = *_Hashtable_base*<_Key, _Value, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _Traits>= [protected]=
Definition at line *791* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::__ireturn_type = typename *std::conditional*<__unique_keys::value,
*std::pair*<*iterator*, bool>, *iterator*>::type
Definition at line *834* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::__node_alloc_type = typename
__hashtable_alloc::__node_alloc_type= [protected]=
Definition at line *810* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::__node_gen_type = _AllocNode<__node_alloc_type>= [protected]=
Definition at line *811* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::__unique_keys = typename _Traits::__unique_keys= [protected]=
Definition at line *809* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::*const_iterator* = *_Node_const_iterator*<_Value,
__constant_iterators::value, __hash_cached::value>
Definition at line *831* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >::*iterator*
= *_Node_iterator*<_Value, __constant_iterators::value,
__hash_cached::value>
Definition at line *828* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >::size_type
= typename __hashtable_base::size_type= [protected]=
Definition at line *807* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > using
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >::value_type
= typename __hashtable_base::value_type= [protected]=
Definition at line *806* of file *hashtable_policy.h*.

* Member Function Documentation
** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits >
*__hashtable* & *std::__detail::_Insert_base*< _Key, _Value, _Alloc,
_ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::_M_conjure_hashtable ()= [inline]=, = [protected]=
Definition at line *814* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits >
template<typename _InputIterator > void *std::__detail::_Insert_base*<
_Key, _Value, _Alloc, _ExtractKey, _Equal, _Hash, _RangeHash, _Unused,
_RehashPolicy, _Traits >::insert (_InputIterator __first, _InputIterator
__last)= [inline]=
Definition at line *882* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits >
__ireturn_type *std::__detail::_Insert_base*< _Key, _Value, _Alloc,
_ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::insert (const value_type & __v)= [inline]=
Definition at line *839* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits >
*iterator* *std::__detail::_Insert_base*< _Key, _Value, _Alloc,
_ExtractKey, _Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::insert (*const_iterator* __hint, const value_type & __v)= [inline]=
Definition at line *847* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits > void
*std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >::insert
(*initializer_list*< value_type > __l)= [inline]=
Definition at line *877* of file *hashtable_policy.h*.

** template<typename _Key , typename _Value , typename _Alloc , typename
_ExtractKey , typename _Equal , typename _Hash , typename _RangeHash ,
typename _Unused , typename _RehashPolicy , typename _Traits >
template<typename _KType , typename... _Args> *std::pair*< *iterator*,
bool > *std::__detail::_Insert_base*< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits
>::try_emplace (*const_iterator*, _KType && __k, _Args &&...
__args)= [inline]=
Definition at line *856* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
