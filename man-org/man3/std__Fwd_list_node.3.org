#+TITLE: Manpages - std__Fwd_list_node.3
#+DESCRIPTION: Linux manpage for std__Fwd_list_node.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Fwd_list_node< _Tp > - A helper node class for forward_list. This
is just a linked list with uninitialized storage for a data value in
each node. There is a sorting utility method.

* SYNOPSIS
\\

=#include <forward_list.h>=

Inherits *std::_Fwd_list_node_base*.

** Public Member Functions
void *_M_reverse_after* () noexcept\\

*_Fwd_list_node_base* * *_M_transfer_after* (*_Fwd_list_node_base*
*__begin, *_Fwd_list_node_base* *__end) noexcept\\

const _Tp * *_M_valptr* () const noexcept\\

_Tp * *_M_valptr* () noexcept\\

** Public Attributes
*_Fwd_list_node_base* * *_M_next*\\

__gnu_cxx::__aligned_buffer< _Tp > *_M_storage*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::_Fwd_list_node< _Tp >"A helper node class for forward_list.
This is just a linked list with uninitialized storage for a data value
in each node. There is a sorting utility method.

Definition at line *113* of file *forward_list.h*.

* Member Function Documentation
** void std::_Fwd_list_node_base::_M_reverse_after ()= [inline]=,
= [noexcept]=, = [inherited]=
Definition at line *91* of file *forward_list.h*.

** *_Fwd_list_node_base* * std::_Fwd_list_node_base::_M_transfer_after
(*_Fwd_list_node_base* * __begin, *_Fwd_list_node_base* *
__end)= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *75* of file *forward_list.h*.

** template<typename _Tp > const _Tp * *std::_Fwd_list_node*< _Tp
>::_M_valptr () const= [inline]=, = [noexcept]=
Definition at line *125* of file *forward_list.h*.

** template<typename _Tp > _Tp * *std::_Fwd_list_node*< _Tp >::_M_valptr
()= [inline]=, = [noexcept]=
Definition at line *121* of file *forward_list.h*.

* Member Data Documentation
** *_Fwd_list_node_base**
std::_Fwd_list_node_base::_M_next= [inherited]=
Definition at line *72* of file *forward_list.h*.

** template<typename _Tp > __gnu_cxx::__aligned_buffer<_Tp>
*std::_Fwd_list_node*< _Tp >::_M_storage
Definition at line *118* of file *forward_list.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
