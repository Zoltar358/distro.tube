#+TITLE: Manpages - std___detail__Hash_node_value_base.3
#+DESCRIPTION: Linux manpage for std___detail__Hash_node_value_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Hash_node_value_base< _Value >

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherited by std::__detail::_Hash_node_value< _Value, _Cache_hash_code
>.

** Public Types
typedef _Value *value_type*\\

** Public Member Functions
const _Value & *_M_v* () const noexcept\\

_Value & *_M_v* () noexcept\\

const _Value * *_M_valptr* () const noexcept\\

_Value * *_M_valptr* () noexcept\\

** Public Attributes
__gnu_cxx::__aligned_buffer< _Value > *_M_storage*\\

* Detailed Description
** "template<typename _Value>
\\
struct std::__detail::_Hash_node_value_base< _Value >"struct
_Hash_node_value_base

Node type with the value to store.

Definition at line *229* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _Value > typedef _Value
*std::__detail::_Hash_node_value_base*< _Value >::value_type
Definition at line *231* of file *hashtable_policy.h*.

* Member Function Documentation
** template<typename _Value > const _Value &
*std::__detail::_Hash_node_value_base*< _Value >::_M_v ()
const= [inline]=, = [noexcept]=
Definition at line *248* of file *hashtable_policy.h*.

** template<typename _Value > _Value &
*std::__detail::_Hash_node_value_base*< _Value >::_M_v ()= [inline]=,
= [noexcept]=
Definition at line *244* of file *hashtable_policy.h*.

** template<typename _Value > const _Value *
*std::__detail::_Hash_node_value_base*< _Value >::_M_valptr ()
const= [inline]=, = [noexcept]=
Definition at line *240* of file *hashtable_policy.h*.

** template<typename _Value > _Value *
*std::__detail::_Hash_node_value_base*< _Value >::_M_valptr
()= [inline]=, = [noexcept]=
Definition at line *236* of file *hashtable_policy.h*.

* Member Data Documentation
** template<typename _Value > __gnu_cxx::__aligned_buffer<_Value>
*std::__detail::_Hash_node_value_base*< _Value >::_M_storage
Definition at line *233* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
