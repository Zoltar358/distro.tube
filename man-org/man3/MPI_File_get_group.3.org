#+TITLE: Manpages - MPI_File_get_group.3
#+DESCRIPTION: Linux manpage for MPI_File_get_group.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_File_get_group* - Returns a duplicate of the process group of a
file.

* SYNTAX
#+begin_example
#+end_example

* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_File_get_group(MPI_File fh, MPI_Group *group)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_FILE_GET_GROUP(FH, GROUP, IERROR)
  	INTEGER	FH, GROUP, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_File_get_group(fh, group, ierror)
  	TYPE(MPI_File), INTENT(IN) :: fh
  	TYPE(MPI_Group), INTENT(OUT) :: group
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  MPI::Group MPI::File::Get_group() const
#+end_example

* INPUT PARAMETER
- fh :: File handle (handle).

* OUTPUT PARAMETERS
- group :: Group that opened the file (handle).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_File_get_group returns a duplicate of the group of the communicator
used to open the file associated with /fh./ The group is returned in
/group./ The user is responsible for freeing /group,/ using
MPI_Group_free.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. For MPI I/O function errors, the default error handler is set to
MPI_ERRORS_RETURN. The error handler may be changed with
MPI_File_set_errhandler; the predefined error handler
MPI_ERRORS_ARE_FATAL may be used to make I/O errors fatal. Note that MPI
does not guarantee that an MPI program can continue past an error.
