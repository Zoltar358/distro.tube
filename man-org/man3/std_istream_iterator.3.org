#+TITLE: Manpages - std_istream_iterator.3
#+DESCRIPTION: Linux manpage for std_istream_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::istream_iterator< _Tp, _CharT, _Traits, _Dist > - Provides input
iterator semantics for streams.

* SYNOPSIS
\\

=#include <stream_iterator.h>=

Inherits *std::iterator< input_iterator_tag, _Tp, ptrdiff_t, const _Tp
*, const _Tp & >*.

** Public Types
typedef _CharT *char_type*\\

typedef ptrdiff_t *difference_type*\\
Distance between iterators is represented as this type.

typedef *basic_istream*< _CharT, _Traits > *istream_type*\\

typedef *input_iterator_tag* *iterator_category*\\
One of the *tag types*.

typedef const _Tp * *pointer*\\
This type represents a pointer-to-value_type.

typedef const _Tp & *reference*\\
This type represents a reference-to-value_type.

typedef _Traits *traits_type*\\

typedef _Tp *value_type*\\
The type 'pointed to' by the iterator.

** Public Member Functions
constexpr *istream_iterator* ()\\
Construct end of input stream iterator.

*istream_iterator* (const *istream_iterator* &__obj)\\

*istream_iterator* (*istream_type* &__s)\\
Construct start of input stream iterator.

const _Tp & *operator** () const\\

*istream_iterator* & *operator++* ()\\

*istream_iterator* *operator++* (int)\\

const _Tp * *operator->* () const\\

*istream_iterator* & *operator=* (const *istream_iterator* &)=default\\

** Friends
bool *operator!=* (const *istream_iterator* &__x, const
*istream_iterator* &__y)\\
Return true if the iterators refer to different streams, or if one is at
end-of-stream and the other is not.

bool *operator==* (const *istream_iterator* &__x, const
*istream_iterator* &__y)\\
Return true if the iterators refer to the same stream, or are both at
end-of-stream.

* Detailed Description
** "template<typename _Tp, typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t>
\\
class std::istream_iterator< _Tp, _CharT, _Traits, _Dist >"Provides
input iterator semantics for streams.

Definition at line *49* of file *stream_iterator.h*.

* Member Typedef Documentation
** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> typedef _CharT
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist >::char_type
Definition at line *53* of file *stream_iterator.h*.

** typedef ptrdiff_t *std::iterator*< *input_iterator_tag* , _Tp,
ptrdiff_t , const _Tp * , const _Tp & >::*difference_type*= [inherited]=
Distance between iterators is represented as this type.

Definition at line *134* of file *stl_iterator_base_types.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> typedef
*basic_istream*<_CharT, _Traits> *std::istream_iterator*< _Tp, _CharT,
_Traits, _Dist >::*istream_type*
Definition at line *55* of file *stream_iterator.h*.

** typedef *input_iterator_tag* *std::iterator*< *input_iterator_tag* ,
_Tp, ptrdiff_t , const _Tp * , const _Tp &
>::*iterator_category*= [inherited]=
One of the *tag types*.

Definition at line *130* of file *stl_iterator_base_types.h*.

** typedef const _Tp * *std::iterator*< *input_iterator_tag* , _Tp,
ptrdiff_t , const _Tp * , const _Tp & >::*pointer*= [inherited]=
This type represents a pointer-to-value_type.

Definition at line *136* of file *stl_iterator_base_types.h*.

** typedef const _Tp & *std::iterator*< *input_iterator_tag* , _Tp,
ptrdiff_t , const _Tp * , const _Tp & >::*reference*= [inherited]=
This type represents a reference-to-value_type.

Definition at line *138* of file *stl_iterator_base_types.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> typedef _Traits
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist >::traits_type
Definition at line *54* of file *stream_iterator.h*.

** typedef _Tp *std::iterator*< *input_iterator_tag* , _Tp, ptrdiff_t ,
const _Tp * , const _Tp & >::*value_type*= [inherited]=
The type 'pointed to' by the iterator.

Definition at line *132* of file *stl_iterator_base_types.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> constexpr
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist
>::*istream_iterator* ()= [inline]=, = [constexpr]=
Construct end of input stream iterator.

Definition at line *67* of file *stream_iterator.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t>
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist
>::*istream_iterator* (*istream_type* & __s)= [inline]=
Construct start of input stream iterator.

Definition at line *71* of file *stream_iterator.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t>
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist
>::*istream_iterator* (const *istream_iterator*< _Tp, _CharT, _Traits,
_Dist > & __obj)= [inline]=
Definition at line *75* of file *stream_iterator.h*.

* Member Function Documentation
** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> const _Tp &
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist >::operator* ()
const= [inline]=
Definition at line *93* of file *stream_iterator.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> *istream_iterator* &
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist >::operator++
()= [inline]=
Definition at line *105* of file *stream_iterator.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> *istream_iterator*
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist >::operator++
(int)= [inline]=
Definition at line *115* of file *stream_iterator.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> const _Tp *
*std::istream_iterator*< _Tp, _CharT, _Traits, _Dist >::operator-> ()
const= [inline]=
Definition at line *102* of file *stream_iterator.h*.

* Friends And Related Function Documentation
** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> bool operator!= (const
*istream_iterator*< _Tp, _CharT, _Traits, _Dist > & __x, const
*istream_iterator*< _Tp, _CharT, _Traits, _Dist > & __y)= [friend]=
Return true if the iterators refer to different streams, or if one is at
end-of-stream and the other is not.

Definition at line *153* of file *stream_iterator.h*.

** template<typename _Tp , typename _CharT = char, typename _Traits =
char_traits<_CharT>, typename _Dist = ptrdiff_t> bool operator== (const
*istream_iterator*< _Tp, _CharT, _Traits, _Dist > & __x, const
*istream_iterator*< _Tp, _CharT, _Traits, _Dist > & __y)= [friend]=
Return true if the iterators refer to the same stream, or are both at
end-of-stream.

Definition at line *147* of file *stream_iterator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
