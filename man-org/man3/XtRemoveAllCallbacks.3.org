#+TITLE: Manpages - XtRemoveAllCallbacks.3
#+DESCRIPTION: Linux manpage for XtRemoveAllCallbacks.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtRemoveAllCallbacks.3 is found in manpage for: [[../man3/XtAddCallback.3][man3/XtAddCallback.3]]