#+TITLE: Manpages - std_nested_exception.3
#+DESCRIPTION: Linux manpage for std_nested_exception.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::nested_exception - Exception class with exception_ptr data member.

* SYNOPSIS
\\

=#include <nested_exception.h>=

** Public Member Functions
*nested_exception* (const *nested_exception* &) noexcept=default\\

*exception_ptr* *nested_ptr* () const noexcept\\

*nested_exception* & *operator=* (const *nested_exception* &)
noexcept=default\\

void *rethrow_nested* () const\\

* Detailed Description
Exception class with exception_ptr data member.

Definition at line *52* of file *nested_exception.h*.

* Constructor & Destructor Documentation
** std::nested_exception::nested_exception ()= [inline]=, = [noexcept]=
Definition at line *57* of file *nested_exception.h*.

* Member Function Documentation
** *exception_ptr* std::nested_exception::nested_ptr ()
const= [inline]=, = [noexcept]=
Definition at line *75* of file *nested_exception.h*.

** void std::nested_exception::rethrow_nested () const= [inline]=
Definition at line *67* of file *nested_exception.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
