#+TITLE: Manpages - zip_file_strerror.3
#+DESCRIPTION: Linux manpage for zip_file_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function returns a string describing the last error for the zip archive

while the

function does the same for a zip file

(one file in an archive). The returned string must not be modified or
freed, and becomes invalid when

or

respectively, is closed or on the next call to

or

respectively, for the same archive.

and

return a pointer to the error string.

and

were added in libzip 0.6.

and
