#+TITLE: Manpages - curl_share_strerror.3
#+DESCRIPTION: Linux manpage for curl_share_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_share_strerror - return string describing error code

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>
  const char *curl_share_strerror(CURLSHcode errornum);
#+end_example

* DESCRIPTION
The curl_share_strerror() function returns a string describing the
CURLSHcode error code passed in the argument /errornum/.

* EXAMPLE
#+begin_example
    CURLSHcode sh
    share = curl_share_init();
    sh = curl_share_setopt(share, CURLSHOPT_SHARE, CURL_LOCK_DATA_CONNECT);
    if(sh)
      printf("Error: %s\n", curl_share_strerror(sh));
#+end_example

* AVAILABILITY
This function was added in libcurl 7.12.0

* RETURN VALUE
A pointer to a null-terminated string.

* SEE ALSO
*libcurl-errors*(3), *curl_multi_strerror*(3), *curl_easy_strerror*(3),
*curl_url_strerror*(3)
