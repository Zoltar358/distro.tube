#+TITLE: Manpages - ldap_first_message.3
#+DESCRIPTION: Linux manpage for ldap_first_message.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldap_first_message, ldap_next_message, ldap_count_messages - Stepping
through messages in a result chain

* LIBRARY
OpenLDAP LDAP (libldap, -lldap)

* SYNOPSIS
#+begin_example
  #include <ldap.h>
  int ldap_count_messages( LDAP *ld, LDAPMessage *result )
  LDAPMessage *ldap_first_message( LDAP *ld, LDAPMessage *result )
  LDAPMessage *ldap_next_message( LDAP *ld, LDAPMessage *message )
#+end_example

* DESCRIPTION
These routines are used to step through the messages in a result chain
received from *ldap_result*(3)*.* For search operations, the result
chain can contain referral, entry and result messages. The
*ldap_msgtype*(3) function can be used to distinguish between the
different message types.

The *ldap_first_message()* routine is used to retrieve the first message
in a result chain. It takes the /result/ as returned by a call to
*ldap_result*(3)*,* *ldap_search_s*(3) or *ldap_search_st*(3) and
returns a pointer to the first message in the result chain.

This pointer should be supplied on a subsequent call to
*ldap_next_message()* to get the next message, the result of which
should be supplied to the next call to *ldap_next_message()*, etc.
*ldap_next_message()* will return NULL when there are no more messages.

These functions are useful when using routines like
*ldap_parse_result*(3) that only operate on the first result in the
chain.

A count of the number of messages in the result chain can be obtained by
calling *ldap_count_messages()*. It can also be used to count the number
of remaining messages in a chain if called with a message, entry or
reference returned by *ldap_first_message() ,* *ldap_next_message() ,*
*ldap_first_entry*(3)*,* *ldap_next_entry*(3)*,*
*ldap_first_reference*(3)*,* *ldap_next_reference*(3)*.*

* ERRORS
If an error occurs in *ldap_first_message()* or *ldap_next_message()*,
NULL is returned. If an error occurs in *ldap_count_messages()*, -1 is
returned.

* SEE ALSO
*ldap*(3), *ldap_search*(3), *ldap_result*(3), *ldap_parse_result*(3),
*ldap_first_entry*(3), *ldap_first_reference*(3)

* ACKNOWLEDGEMENTS
*OpenLDAP Software* is developed and maintained by The OpenLDAP Project
<http://www.openldap.org/>. *OpenLDAP Software* is derived from the
University of Michigan LDAP 3.3 Release.
