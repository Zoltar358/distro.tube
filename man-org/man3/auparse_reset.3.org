#+TITLE: Manpages - auparse_reset.3
#+DESCRIPTION: Linux manpage for auparse_reset.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_reset - reset audit parser instance

* SYNOPSIS
*#include <auparse.h>*

int auparse_reset(auparse_state_t *au);

* DESCRIPTION
auparse_reset resets all internal cursors to the beginning. It closes
files, descriptors, and frees memory buffers.

* RETURN VALUE
Returns -1 if an error occurs; otherwise, 0 for success.

* SEE ALSO
*auparse_init*(3), *auparse_destroy*(3).

* AUTHOR
Steve Grubb
