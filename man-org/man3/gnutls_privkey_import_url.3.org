#+TITLE: Manpages - gnutls_privkey_import_url.3
#+DESCRIPTION: Linux manpage for gnutls_privkey_import_url.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_privkey_import_url - API function

* SYNOPSIS
*#include <gnutls/abstract.h>*

*int gnutls_privkey_import_url(gnutls_privkey_t */key/*, const char *
*/url/*, unsigned int */flags/*);*

* ARGUMENTS
- gnutls_privkey_t key :: A key of type *gnutls_privkey_t*

- const char * url :: A PKCS 11 url

- unsigned int flags :: should be zero

* DESCRIPTION
This function will import a PKCS11 or TPM URL as a private key. The
supported URL types can be checked using *gnutls_url_is_supported()*.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
3.1.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
