#+TITLE: Manpages - hwloc_windows_get_nr_processor_groups.3
#+DESCRIPTION: Linux manpage for hwloc_windows_get_nr_processor_groups.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_windows_get_nr_processor_groups.3 is found in manpage for: [[../man3/hwlocality_windows.3][man3/hwlocality_windows.3]]