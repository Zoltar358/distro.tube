#+TITLE: Manpages - pcap_datalink.3pcap
#+DESCRIPTION: Linux manpage for pcap_datalink.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_datalink - get the link-layer header type

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_datalink(pcap_t *p);
#+end_example

* DESCRIPTION
*pcap_datalink*() returns the link-layer header type for the live
capture or ``savefile'' specified by /p/.

It must not be called on a pcap descriptor created by
*pcap_create*(3PCAP) that has not yet been activated by
*pcap_activate*(3PCAP).

/https://www.tcpdump.org/linktypes.html/ lists the values
*pcap_datalink*() can return and describes the packet formats that
correspond to those values.

Do *NOT* assume that the packets for a given capture or ``savefile``
will have any given link-layer header type, such as *DLT_EN10MB* for
Ethernet. For example, the "any" device on Linux will have a link-layer
header type of *DLT_LINUX_SLL* or *DLT_LINUX_SLL2* even if all devices
on the system at the time the "any" device is opened have some other
data link type, such as *DLT_EN10MB* for Ethernet.

* RETURN VALUE
*pcap_datalink*() returns the link-layer header type on success and
*PCAP_ERROR_NOT_ACTIVATED* if called on a capture handle that has been
created but not activated.

* SEE ALSO
*pcap*(3PCAP), *pcap-linktype*(7)
