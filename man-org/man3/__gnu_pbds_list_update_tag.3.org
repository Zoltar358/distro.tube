#+TITLE: Manpages - __gnu_pbds_list_update_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_list_update_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::list_update_tag - List-update.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::associative_tag*.

* Detailed Description
List-update.

Definition at line *168* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
