#+TITLE: Manpages - expf.3
#+DESCRIPTION: Linux manpage for expf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about expf.3 is found in manpage for: [[../man3/exp.3][man3/exp.3]]