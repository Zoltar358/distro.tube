#+TITLE: Manpages - FcBlanksAdd.3
#+DESCRIPTION: Linux manpage for FcBlanksAdd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcBlanksAdd - Add a character to an FcBlanks

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcBlanksAdd (FcBlanks */b/*, FcChar32 */ucs4/*);*

* DESCRIPTION
FcBlanks is deprecated. This function always returns FALSE.
