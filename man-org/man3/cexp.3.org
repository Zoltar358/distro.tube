#+TITLE: Manpages - cexp.3
#+DESCRIPTION: Linux manpage for cexp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cexp, cexpf, cexpl - complex exponential function

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double complex cexp(double complex z);
  float complex cexpf(float complex z);
  long double complex cexpl(long double complex z);

  Link with -lm.
#+end_example

* DESCRIPTION
These functions calculate e (2.71828..., the base of natural logarithms)
raised to the power of /z/.

One has:

#+begin_example
      cexp(I * z) = ccos(z) + I * csin(z)
#+end_example

* VERSIONS
These functions first appeared in glibc in version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *cexp*(), *cexpf*(), *cexpl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*cabs*(3), *cexp2*(3), *clog*(3), *cpow*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
