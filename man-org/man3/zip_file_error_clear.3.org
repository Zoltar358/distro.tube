#+TITLE: Manpages - zip_file_error_clear.3
#+DESCRIPTION: Linux manpage for zip_file_error_clear.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function clears the error state for the zip archive

The

function does the same for the zip file

and

were added in libzip 0.8.

and
