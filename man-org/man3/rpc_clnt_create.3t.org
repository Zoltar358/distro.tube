#+TITLE: Manpages - rpc_clnt_create.3t
#+DESCRIPTION: Linux manpage for rpc_clnt_create.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
handles

RPC library routines allow C language programs to make procedure calls
on other machines across the network. First a

handle is created and then the client calls a procedure to send a
request to the server. On receipt of the request, the server calls a
dispatch routine to perform the requested service, and then sends a
reply.

A function macro to change or retrieve various information about a
client object. The

argument indicates the type of operation, and

is a pointer to the information. For both connectionless and
connection-oriented transports, the supported values of

and their argument types and what they do are:

Note: if you set the timeout using

the timeout argument passed by

is ignored in all subsequent calls.

Note: If you set the timeout value to 0,

immediately returns an error

Set the timeout argument to 0 for batching calls.

The following operations are valid for connectionless transports only:

The retry timeout is the time that RPC waits for the server to reply
before retransmitting the request. The

function returns

on success and

on failure.

Generic client creation routine for program

and version

The

argument identifies the name of the remote host where the server is
located. The

argument indicates the class of transport protocol to use. The
transports are tried in left to right order in

environment variable or in top to bottom order in the netconfig
database. The

function tries all the transports of the

class available from the

environment variable and the netconfig database, and chooses the first
successful one. A default timeout is set and can be modified using

This routine returns

if it fails. The

routine can be used to print the reason for failure.

Note:

returns a valid client handle even if the particular version number
supplied to

is not registered with the

service. This mismatch will be discovered by a

later (see

Generic client creation routine which is similar to

but which also has the additional argument

that specifies the maximum amount of time allowed for each transport
class tried. In all other respects, the

call behaves exactly like the

call.

Generic client creation routine which is similar to

but which also checks for the version availability. The

argument identifies the name of the remote host where the server is
located. The

argument indicates the class transport protocols to be used. If the
routine is successful it returns a client handle created for the highest
version between

and

that is supported by the server. The

argument is set to this value. That is, after a successful return

<=

<=

If no version between

and

is supported by the server then the routine fails and returns

A default timeout is set and can be modified using

This routine returns

if it fails. The

routine can be used to print the reason for failure. Note:

returns a valid client handle even if the particular version number
supplied to

is not registered with the

service. This mismatch will be discovered by a

later (see

However,

does this for you and returns a valid handle only if a version within
the range supplied is supported by the server.

Generic client creation routine which is similar to

but which also has the additional argument

that specifies the maximum amount of time allowed for each transport
class tried. In all other respects, the

call behaves exactly like the

call.

A function macro that destroys the client's RPC handle. Destruction
usually involves deallocation of private data structures, including

itself. Use of

is undefined after calling

If the RPC library opened the associated file descriptor, or

was set using

the file descriptor will be closed. The caller should call

(before calling

to destroy the associated

structure (see

This routine creates an RPC client for the remote program

and version

the client uses a connectionless transport. The remote program is
located at address

The

argument is an open and bound file descriptor. This routine will resend
the call message in intervals of 15 seconds until a response is received
or until the call times out. The total time for the call to time out is
specified by

(see

in

The retry time out and the total time out periods can be changed using

The user may set the size of the send and receive buffers with the

and

arguments; values of 0 choose suitable defaults. This routine returns

if it fails.

Print a message to standard error indicating why a client RPC handle
could not be created. The message is prepended with the string

and a colon, and appended with a newline.

Like

except that it returns a string instead of printing to the standard
error. A newline is not appended to the message in this case. Warning:
returns a pointer to a buffer that is overwritten on each call.

This routine creates an RPC client handle for the remote program

and version

The transport used to pass messages to the service is a buffer within
the process's address space, so the corresponding RPC server should live
in the same address space; (see

in

This allows simulation of RPC and measurement of RPC overheads, such as
round trip times, without any kernel or networking interference. This
routine returns

if it fails. The

function should be called after

This routine creates an RPC client handle for the remote program

and version

The remote program is located at address

If

is

and it is connection-oriented, it is assumed that the file descriptor is
connected. For connectionless transports, if

is

error is set. The

argument is a file descriptor which may be open, bound and connected. If
it is

it opens a file descriptor on the transport specified by

If

is

and

is

a

error is set. If

is unbound, then it will attempt to bind the descriptor. The user may
specify the size of the buffers with the

and

arguments; values of 0 choose suitable defaults. Depending upon the type
of the transport (connection-oriented or connectionless),

calls appropriate client creation routines. This routine returns

if it fails. The

routine can be used to print the reason for failure. The remote rpcbind
service (see

is not consulted for the address of the remote service.

Like

except

tries only one transport specified through

The

function creates a client handle for the program

the version

and for the transport specified by

Default options are set, which can be changed using

calls. The remote rpcbind service on the host

is consulted for the address of the remote service. This routine returns

if it fails. The

routine can be used to print the reason for failure.

Like

except

has the extra argument

which specifies the maximum time allowed for the creation attempt to
succeed. In all other respects, the

call behaves exactly like the

call.

This routine creates an RPC client for the remote program

and version

the client uses a connection-oriented transport. The remote program is
located at address

The

argument is an open and bound file descriptor. The user may specify the
size of the send and receive buffers with the

and

arguments; values of 0 choose suitable defaults. This routine returns

if it fails. The address

should not be

and should point to the actual address of the remote program. The

function does not consult the remote rpcbind service for this
information.

A global variable whose value is set by any RPC client handle creation
routine that fails. It is used by the routine

to print the reason for the failure.

These functions are part of libtirpc.
