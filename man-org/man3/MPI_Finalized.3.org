#+TITLE: Manpages - MPI_Finalized.3
#+DESCRIPTION: Linux manpage for MPI_Finalized.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Finalized * - Checks whether MPI has been finalized

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Finalized(int *flag)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_FINALIZED(FLAG, IERROR)
  	LOGICAL	FLAG
  	INTEGER	IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Finalized(flag, ierror)
  	LOGICAL, INTENT(OUT) :: flag
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  bool MPI::Is_finalized()
#+end_example

* OUTPUT PARAMETER
- flag :: True if MPI was finalized, and false otherwise (logical).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
This routine may be used to determine whether MPI has been finalized. It
is one of a small number of routines that may be called before MPI is
initialized and after MPI has been finalized (MPI_Initialized is
another).

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
#+begin_example
  MPI_Init
  MPI_Init_thread
  MPI_Initialized
  MPI_Finalize
#+end_example
