#+TITLE: Manpages - SSL_CTX_get0_param.3ssl
#+DESCRIPTION: Linux manpage for SSL_CTX_get0_param.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_CTX_get0_param, SSL_get0_param, SSL_CTX_set1_param, SSL_set1_param -
get and set verification parameters

* SYNOPSIS
#include <openssl/ssl.h> X509_VERIFY_PARAM *SSL_CTX_get0_param(SSL_CTX
*ctx) X509_VERIFY_PARAM *SSL_get0_param(SSL *ssl) int
SSL_CTX_set1_param(SSL_CTX *ctx, X509_VERIFY_PARAM *vpm) int
SSL_set1_param(SSL *ssl, X509_VERIFY_PARAM *vpm)

* DESCRIPTION
*SSL_CTX_get0_param()* and *SSL_get0_param()* retrieve an internal
pointer to the verification parameters for *ctx* or *ssl* respectively.
The returned pointer must not be freed by the calling application.

*SSL_CTX_set1_param()* and *SSL_set1_param()* set the verification
parameters to *vpm* for *ctx* or *ssl*.

* NOTES
Typically parameters are retrieved from an *SSL_CTX* or *SSL* structure
using *SSL_CTX_get0_param()* or *SSL_get0_param()* and an application
modifies them to suit its needs: for example to add a hostname check.

* RETURN VALUES
*SSL_CTX_get0_param()* and *SSL_get0_param()* return a pointer to an
*X509_VERIFY_PARAM* structure.

*SSL_CTX_set1_param()* and *SSL_set1_param()* return 1 for success and 0
for failure.

* EXAMPLES
Check hostname matches www.foo.com in peer certificate:

X509_VERIFY_PARAM *vpm = SSL_get0_param(ssl);
X509_VERIFY_PARAM_set1_host(vpm, "www.foo.com", 0);

* SEE ALSO
*X509_VERIFY_PARAM_set_flags* (3)

* HISTORY
These functions were added in OpenSSL 1.0.2.

* COPYRIGHT
Copyright 2015-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
