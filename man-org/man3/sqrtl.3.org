#+TITLE: Manpages - sqrtl.3
#+DESCRIPTION: Linux manpage for sqrtl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sqrtl.3 is found in manpage for: [[../man3/sqrt.3][man3/sqrt.3]]