#+TITLE: Manpages - XML_LibXML_CDATASection.3pm
#+DESCRIPTION: Linux manpage for XML_LibXML_CDATASection.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
XML::LibXML::CDATASection - XML::LibXML Class for CDATA Sections

* SYNOPSIS
use XML::LibXML; # Only methods specific to CDATA nodes are listed here,
# see the XML::LibXML::Node manpage for other methods $node =
XML::LibXML::CDATASection->new( $content );

* DESCRIPTION
This class provides all functions of XML::LibXML::Text, but for CDATA
nodes.

* METHODS
The class inherits from XML::LibXML::Node. The documentation for
Inherited methods is not listed here.

Many functions listed here are extensively documented in the DOM Level 3
specification (<http://www.w3.org/TR/DOM-Level-3-Core/>). Please refer
to the specification for extensive documentation.

- new ::  $node = XML::LibXML::CDATASection->new( $content ); The
  constructor is the only provided function for this package. It is
  required, because /libxml2/ treats the different text node types
  slightly differently.

* AUTHORS
Matt Sergeant, Christian Glahn, Petr Pajas

* VERSION
2.0207

* COPYRIGHT
2001-2007, AxKit.com Ltd.

2002-2006, Christian Glahn.

2006-2009, Petr Pajas.

* LICENSE
This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
