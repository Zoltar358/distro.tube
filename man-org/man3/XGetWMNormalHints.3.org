#+TITLE: Manpages - XGetWMNormalHints.3
#+DESCRIPTION: Linux manpage for XGetWMNormalHints.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetWMNormalHints.3 is found in manpage for: [[../man3/XAllocSizeHints.3][man3/XAllocSizeHints.3]]