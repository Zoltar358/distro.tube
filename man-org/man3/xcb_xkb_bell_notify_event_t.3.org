#+TITLE: Manpages - xcb_xkb_bell_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_xkb_bell_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xkb_bell_notify_event_t -

* SYNOPSIS
*#include <xcb/xkb.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_xkb_bell_notify_event_t {
      uint8_t         response_type;
      uint8_t         xkbType;
      uint16_t        sequence;
      xcb_timestamp_t time;
      uint8_t         deviceID;
      uint8_t         bellClass;
      uint8_t         bellID;
      uint8_t         percent;
      uint16_t        pitch;
      uint16_t        duration;
      xcb_atom_t      name;
      xcb_window_t    window;
      uint8_t         eventOnly;
      uint8_t         pad0[7];
  } xcb_xkb_bell_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_XKB_BELL_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- xkbType :: NOT YET DOCUMENTED.

- time :: NOT YET DOCUMENTED.

- deviceID :: NOT YET DOCUMENTED.

- bellClass :: NOT YET DOCUMENTED.

- bellID :: NOT YET DOCUMENTED.

- percent :: NOT YET DOCUMENTED.

- pitch :: NOT YET DOCUMENTED.

- duration :: NOT YET DOCUMENTED.

- name :: NOT YET DOCUMENTED.

- window :: NOT YET DOCUMENTED.

- eventOnly :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xkb.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
