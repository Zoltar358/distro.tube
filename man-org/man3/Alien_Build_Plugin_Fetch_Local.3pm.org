#+TITLE: Manpages - Alien_Build_Plugin_Fetch_Local.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Fetch_Local.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Fetch::Local - Plugin for fetching a local file

* VERSION
version 2.44

* SYNOPSIS
use alienfile; share { start_url patch/libfoo-1.00.tar.gz; plugin
Fetch::Local; };

* DESCRIPTION
Note: in most case you will want to use
Alien::Build::Plugin::Download::Negotiate instead. It picks the
appropriate fetch plugin based on your platform and environment. In some
cases you may need to use this plugin directly instead.

This fetch plugin fetches files from the local file system. It is mostly
useful if you intend to bundle packages (as tarballs or zip files) with
your Alien. If you intend to bundle a source tree, use
Alien::Build::Plugin::Fetch::LocalDir.

* PROPERTIES
** url
The initial URL to fetch. This may be a =file://= style URL, or just the
path on the local system.

** root
The directory from which the URL should be relative. The default is
usually reasonable.

** ssl
This property is for compatibility with other fetch plugins, but is not
used.

* SEE ALSO
- Alien::Build::Plugin::Download::Negotiate :: 

- Alien::Build::Plugin::Fetch::LocalDir :: 

- Alien::Build :: 

- alienfile :: 

- Alien::Build::MM :: 

- Alien :: 

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
