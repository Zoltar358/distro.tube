#+TITLE: Manpages - SDL_CDStatus.3
#+DESCRIPTION: Linux manpage for SDL_CDStatus.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDStatus - Returns the current status of the given drive.

* SYNOPSIS
*#include "SDL.h"*

*CDstatus SDL_CDStatus*(*SDL_CD *cdrom*); */* Given a status, returns
true if there's a disk in the drive */* #define CD_INDRIVE(status)
((int)status > 0)

* DESCRIPTION
This function returns the current status of the given drive. Status is
described like so:

#+begin_example
  typedef enum {
    CD_TRAYEMPTY,
    CD_STOPPED,
    CD_PLAYING,
    CD_PAUSED,
    CD_ERROR = -1
  } CDstatus;
#+end_example

If the drive has a CD in it, the table of contents of the CD and current
play position of the CD will be stored in the SDL_CD structure.

The macro *CD_INDRIVE* is provided for convenience, and given a status
returns true if there's a disk in the drive.

#+begin_quote
  *Note: *

  *SDL_CDStatus also updates the SDL_CD* structure passed to it.
#+end_quote

* EXAMPLE
#+begin_example
  int playTrack(int track)
  {
    int playing = 0;

    if ( CD_INDRIVE(SDL_CDStatus(cdrom)) ) {
    /* clamp to the actual number of tracks on the CD */
      if (track >= cdrom->numtracks) {
        track = cdrom->numtracks-1;
      }

      if ( SDL_CDPlayTracks(cdrom, track, 0, 1, 0) == 0 ) {
        playing = 1;
      }
    }
    return playing;
  }
#+end_example

* SEE ALSO
*SDL_CD*
