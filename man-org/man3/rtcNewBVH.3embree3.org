#+TITLE: Manpages - rtcNewBVH.3embree3
#+DESCRIPTION: Linux manpage for rtcNewBVH.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcNewBVH - creates a new BVH object
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  RTCBVH rtcNewBVH(RTCDevice device);
#+end_example

** DESCRIPTION
This function creates a new BVH object and returns a handle to this BVH.
The BVH object is reference counted with an initial reference count
of 1. The handle can be released using the =rtcReleaseBVH= API call.

The BVH object can be used to build a BVH in a user-specified format
over user-specified primitives. See the documentation of the
=rtcBuildBVH= call for more details.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcRetainBVH], [rtcReleaseBVH], [rtcBuildBVH]
