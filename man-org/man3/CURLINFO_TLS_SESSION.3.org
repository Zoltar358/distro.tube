#+TITLE: Manpages - CURLINFO_TLS_SESSION.3
#+DESCRIPTION: Linux manpage for CURLINFO_TLS_SESSION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_TLS_SESSION - get TLS session info

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_TLS_SESSION,
                             struct curl_tlssessioninfo **session);
#+end_example

* DESCRIPTION
*This option has been superseded* by /CURLINFO_TLS_SSL_PTR(3)/ which was
added in 7.48.0. The only reason you would use this option instead is if
you could be using a version of libcurl earlier than 7.48.0.

This option is exactly the same as /CURLINFO_TLS_SSL_PTR(3)/ except in
the case of OpenSSL. If the session /backend/ is CURLSSLBACKEND_OPENSSL
the session /internals/ pointer varies depending on the option:

CURLINFO_TLS_SESSION OpenSSL session /internals/ is SSL_CTX *.

CURLINFO_TLS_SSL_PTR OpenSSL session /internals/ is SSL *.

You can obtain an SSL_CTX pointer from an SSL pointer using OpenSSL
function SSL_get_SSL_CTX. Therefore unless you need compatibility with
older versions of libcurl use /CURLINFO_TLS_SSL_PTR(3)/. Refer to that
document for more information.

* PROTOCOLS
All TLS-based

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    struct curl_tlssessioninfo *tls;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    res = curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_TLS_SESSION, &tls);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.34.0, and supported OpenSSL, GnuTLS, NSS and gskit only up
until 7.48.0 was released.

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_TLS_SSL_PTR*(3),
