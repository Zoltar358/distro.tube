#+TITLE: Manpages - XIGrabTouchBegin.3
#+DESCRIPTION: Linux manpage for XIGrabTouchBegin.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XIGrabTouchBegin.3 is found in manpage for: [[../man3/XIGrabButton.3][man3/XIGrabButton.3]]