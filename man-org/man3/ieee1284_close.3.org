#+TITLE: Manpages - ieee1284_close.3
#+DESCRIPTION: Linux manpage for ieee1284_close.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_close - close an open port

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*int ieee1284_close(struct parport **/port/*);*

* DESCRIPTION
To close an open port and free any resources associated with it, call
*ieee1284_close*.

* RETURN VALUE
*E1284_OK*

#+begin_quote
  The port is now closed.
#+end_quote

*E1284_INVALIDPORT*

#+begin_quote
  The /port/ parameter is invalid (perhaps it is not open, for
  instance).
#+end_quote

*E1284_SYS*

#+begin_quote
  There was a problem at the operating system level. The global variable
  /errno/ has been set appropriately.
#+end_quote

* SEE ALSO
*ieee1284_open*(3)

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
