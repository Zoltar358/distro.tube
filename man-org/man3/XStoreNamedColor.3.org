#+TITLE: Manpages - XStoreNamedColor.3
#+DESCRIPTION: Linux manpage for XStoreNamedColor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XStoreNamedColor.3 is found in manpage for: [[../man3/XStoreColors.3][man3/XStoreColors.3]]