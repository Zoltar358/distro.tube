#+TITLE: Manpages - MPI_Comm_set_errhandler.3
#+DESCRIPTION: Linux manpage for MPI_Comm_set_errhandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Comm_set_errhandler * - Attaches a new error handler to a
communicator.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Comm_set_errhandler(MPI_Comm comm,
  	MPI_Errhandler errhandler)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_COMM_SET_ERRHANDLER(COMM, ERRHANDLER, IERROR)
  	INTEGER	COMM, ERRHANDLER, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Comm_set_errhandler(comm, errhandler, ierror)
  	TYPE(MPI_Comm), INTENT(IN) :: comm
  	TYPE(MPI_Errhandler), INTENT(IN) :: errhandler
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::Comm::Set_errhandler(const MPI::Errhandler& errhandler)
#+end_example

* INPUT/OUTPUT PARAMETER
- comm :: Communicator (handle).

* OUTPUT PARAMETERS
- errhandler :: New error handler for communicator (handle).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_Comm_set_errhandler attaches a new error handler to a communicator.
The error handler must be either a predefined error handler or an error
handler created by a call to MPI_Comm_create_errhandler. This call is
identical to MPI_Errhandler_set, the use of which is deprecated.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
