#+TITLE: Manpages - XkbComputeShapeBounds.3
#+DESCRIPTION: Linux manpage for XkbComputeShapeBounds.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbComputeShapeBounds - Updates the bounding box of a shape

* SYNOPSIS
*Bool XkbComputeShapeBounds* *( XkbShapePtr */shape/* );*

* ARGUMENTS
- /- shape/ :: shape to be examined

* DESCRIPTION
Xkb provides a number of convenience functions to help use a keyboard
geometry. These include functions to return the bounding box of a
shape's top surface and to update the bounding box of a shape row or
section.

A shape is made up of a number of outlines. Each outline is a polygon
made up of a number of points. The bounding box of a shape is a
rectangle that contains all the outlines of that shape.

A ShapeRec contains a BoundsRec that describes the bounds of the shape.
If you add or delete an outline to or from a shape, the bounding box
must be updated.

/XkbComputeShapeBounds/ updates the BoundsRec contained in the /shape/
by examining all the outlines of the shape and setting the BoundsRec to
the minimum x and minimum y, and maximum x and maximum y values found in
those outlines. /XkbComputeShapeBounds/ returns False if /shape/ is NULL
or if there are no outlines for the shape; otherwise, it returns True.

If you add or delete a key to or from a row, or if you update the shape
of one of the keys in that row, you may need to update the bounding box
of that row. To update the bounding box of a row, use
/XkbComputeRowBounds./

* STRUCTURES
#+begin_example

  typedef struct _XkbShape {
      Atom           name;         /* shape's name */
      unsigned short num_outlines; /* number of outlines for the shape */
      unsigned short sz_outlines;  /* size of the outlines array */
      XkbOutlinePtr  outlines;     /* array of outlines for the shape */
      XkbOutlinePtr  approx;       /* pointer into the array to the approximating outline */
      XkbOutlinePtr  primary;      /* pointer into the array to the primary outline */
      XkbBoundsRec   bounds;       /* bounding box for the shape; encompasses all outlines */
  } XkbShapeRec, *XkbShapePtr;

  typedef struct _XkbBounds {
      short	x1,y1;	/* upper left corner of the bounds, in mm/10 */
      short	x2,y2;	/* lower right corner of the bounds, in mm/10 */
  } XkbBoundsRec, *XkbBoundsPtr;
#+end_example

* SEE ALSO
*XkbComputeRowBounds*(3)
