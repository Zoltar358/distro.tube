#+TITLE: Manpages - gnutls_pk_to_sign.3
#+DESCRIPTION: Linux manpage for gnutls_pk_to_sign.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pk_to_sign - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*gnutls_sign_algorithm_t gnutls_pk_to_sign(gnutls_pk_algorithm_t */pk/*,
gnutls_digest_algorithm_t */hash/*);*

* ARGUMENTS
- gnutls_pk_algorithm_t pk :: is a public key algorithm

- gnutls_digest_algorithm_t hash :: a hash algorithm

* DESCRIPTION
This function maps public key and hash algorithms combinations to
signature algorithms.

* RETURNS
return a *gnutls_sign_algorithm_t* value, or *GNUTLS_SIGN_UNKNOWN* on
error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
