#+TITLE: Manpages - archive_entry_perms.3
#+DESCRIPTION: Linux manpage for archive_entry_perms.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

The functions

and

can be used to extract the user id, group id and permission from the
given entry. The corresponding functions

and

store the given user id, group id and permission in the entry. The
permission is also set as a side effect of calling

returns a string representation of the permission as used by the long
mode of

User and group names can be provided in one of three different ways:

Multibyte strings in the current locale.

Wide character strings in the current locale. The accessor functions are
named

Unicode strings encoded as UTF-8. These are convenience functions to
update both the multibyte and wide character strings at the same time.

is an alias for

File flags are transparently converted between a bitmap representation
and a textual format. For example, if you set the bitmap and ask for
text, the library will build a canonical text format. However, if you
set a text format and request a text format, you will get back the same
text, even if it is ill-formed. If you need to canonicalize a textual
flags string, you should first set the text form, then request the
bitmap form, then use that to set the bitmap form. Setting the bitmap
format will clear the internal text representation and force it to be
reconstructed when you next request the text form.

The bitmap format consists of two integers, one containing bits that
should be set, the other specifying bits that should be cleared. Bits
not mentioned in either bitmap will be ignored. Usually, the bitmap of
bits to be cleared will be set to zero. In unusual circumstances, you
can force a fully-specified set of file flags by setting the bitmap of
flags to clear to the complement of the bitmap of flags to set. (This
differs from

which only includes names for set bits.) Converting a bitmap to a
textual string is a platform-specific operation; bits that are not
meaningful on the current platform will be ignored.

The canonical text format is a comma-separated list of flag names. The

and

functions parse the provided text and set the internal bitmap values.
This is a platform-specific operation; names that are not meaningful on
the current platform will be ignored. The function returns a pointer to
the start of the first name that was not recognized, or NULL if every
name was recognized. Note that every name --- including names that
follow an unrecognized name --- will be evaluated, and the bitmaps will
be set to reflect every name that is recognized. (In particular, this
differs from

which stops parsing at the first unrecognized name.)

The platform types

and

are often 16 or 32 bit wide. In this case it is possible that the ids
can not be correctly restored from archives and get truncated.
