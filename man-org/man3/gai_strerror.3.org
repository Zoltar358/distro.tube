#+TITLE: Manpages - gai_strerror.3
#+DESCRIPTION: Linux manpage for gai_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about gai_strerror.3 is found in manpage for: [[../man3/getaddrinfo.3][man3/getaddrinfo.3]]