#+TITLE: Manpages - SDL_FreeYUVOverlay.3
#+DESCRIPTION: Linux manpage for SDL_FreeYUVOverlay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_FreeYUVOverlay - Free a YUV video overlay

* SYNOPSIS
*#include "SDL.h"*

*void SDL_FreeYUVOverlay*(*SDL_Overlay *overlay*);

* DESCRIPTION
Frees and *overlay* created by *SDL_CreateYUVOverlay*.

* SEE ALSO
*SDL_Overlay*, *SDL_DisplayYUVOverlay*, *SDL_FreeYUVOverlay*
