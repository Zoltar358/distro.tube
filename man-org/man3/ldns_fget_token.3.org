#+TITLE: Manpages - ldns_fget_token.3
#+DESCRIPTION: Linux manpage for ldns_fget_token.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_fget_token, ldns_fskipcs - get tokens from files

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ssize_t ldns_fget_token(FILE *f, char *token, const char *delim, size_t
limit);

void ldns_fskipcs(FILE *fp, const char *s);

* DESCRIPTION
/ldns_fget_token/() returns a token/char from the stream F. This
function deals with ( and ) in the stream, and ignores them when
encountered .br **f*: the file to read from .br **token*: the read token
is put here .br **delim*: chars at which the parsing should stop .br
**limit*: how much to read. If 0 the builtin maximum is used .br Returns
0 on error of EOF of the stream F. Otherwise return the length of what
is read

/ldns_fskipcs/() skips all of the characters in the given string in the
fp, moving the position to the first character that is not in *s. .br
**fp*: file to use .br **s*: characters to skip .br Returns void

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_buffer/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
