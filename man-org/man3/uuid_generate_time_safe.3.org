#+TITLE: Manpages - uuid_generate_time_safe.3
#+DESCRIPTION: Linux manpage for uuid_generate_time_safe.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about uuid_generate_time_safe.3 is found in manpage for: [[../uuid_generate.3][uuid_generate.3]]