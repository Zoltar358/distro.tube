#+TITLE: Manpages - zmq_atomic_counter_new.3
#+DESCRIPTION: Linux manpage for zmq_atomic_counter_new.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_atomic_counter_new - create a new atomic counter

* SYNOPSIS
*void *zmq_atomic_counter_new (void);*

* DESCRIPTION
The /zmq_atomic_counter_new/ function creates a new atomic counter. You
can use this in multithreaded applications to do, for example, reference
counting of shared objects. The atomic counter is at least 32 bits
large. This function uses platform specific atomic operations.

* RETURN VALUE
The /zmq_atomic_counter_new()/ function returns the new atomic counter
if successful. Otherwise it returns NULL.

* EXAMPLE
*Test code for atomic counters*.

#+begin_quote
  #+begin_example
    void *counter = zmq_atomic_counter_new ();
    assert (zmq_atomic_counter_value (counter) == 0);
    assert (zmq_atomic_counter_inc (counter) == 0);
    assert (zmq_atomic_counter_inc (counter) == 1);
    assert (zmq_atomic_counter_inc (counter) == 2);
    assert (zmq_atomic_counter_value (counter) == 3);
    assert (zmq_atomic_counter_dec (counter) == 1);
    assert (zmq_atomic_counter_dec (counter) == 1);
    assert (zmq_atomic_counter_dec (counter) == 0);
    zmq_atomic_counter_set (counter, 2);
    assert (zmq_atomic_counter_dec (counter) == 1);
    assert (zmq_atomic_counter_dec (counter) == 0);
    zmq_atomic_counter_destroy (&counter);
    return 0;
  #+end_example
#+end_quote

* SEE ALSO
*zmq_atomic_counter_set*(3) *zmq_atomic_counter_inc*(3)
*zmq_atomic_counter_dec*(3) *zmq_atomic_counter_value*(3)
*zmq_atomic_counter_destroy*(3)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
