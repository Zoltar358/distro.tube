#+TITLE: Manpages - CURLOPT_NEW_FILE_PERMS.3
#+DESCRIPTION: Linux manpage for CURLOPT_NEW_FILE_PERMS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_NEW_FILE_PERMS - permissions for remotely created files

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NEW_FILE_PERMS,
                            long mode);
#+end_example

* DESCRIPTION
Pass a long as a parameter, containing the value of the permissions that
will be assigned to newly created files on the remote server. The
default value is /0644/, but any valid value can be used. The only
protocols that can use this are /sftp:///, /scp:///, and /file:///.

* DEFAULT
0644

* PROTOCOLS
SFTP, SCP and FILE

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "sftp://upload.example.com/file.txt");
    curl_easy_setopt(curl, CURLOPT_NEW_FILE_PERMS, 0664L);
    ret = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.16.4

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_NEW_DIRECTORY_PERMS*(3), *CURLOPT_UPLOAD*(3),
