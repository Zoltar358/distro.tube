#+TITLE: Manpages - TimeDate.3pm
#+DESCRIPTION: Linux manpage for TimeDate.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

This is an empty module which is just there to get ownership on TimeDate
using first-come permissions from PAUSE.

This was required during the release of 1.21 for transferring ownership.
