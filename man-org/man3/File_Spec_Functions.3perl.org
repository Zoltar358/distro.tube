#+TITLE: Manpages - File_Spec_Functions.3perl
#+DESCRIPTION: Linux manpage for File_Spec_Functions.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
File::Spec::Functions - portably perform operations on file names

* SYNOPSIS
use File::Spec::Functions; $x = catfile(a,b);

* DESCRIPTION
This module exports convenience functions for all of the class methods
provided by File::Spec.

For a reference of available functions, please consult File::Spec::Unix,
which contains the entire set, and which is inherited by the modules for
other platforms. For further information, please see File::Spec::Mac,
File::Spec::OS2, File::Spec::Win32, or File::Spec::VMS.

** Exports
The following functions are exported by default.

canonpath catdir catfile curdir rootdir updir no_upwards
file_name_is_absolute path

The following functions are exported only by request.

devnull tmpdir splitpath splitdir catpath abs2rel rel2abs case_tolerant

All the functions may be imported using the =:ALL= tag.

* COPYRIGHT
Copyright (c) 2004 by the Perl 5 Porters. All rights reserved.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

* SEE ALSO
File::Spec, File::Spec::Unix, File::Spec::Mac, File::Spec::OS2,
File::Spec::Win32, File::Spec::VMS, ExtUtils::MakeMaker
