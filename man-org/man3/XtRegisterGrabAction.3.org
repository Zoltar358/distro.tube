#+TITLE: Manpages - XtRegisterGrabAction.3
#+DESCRIPTION: Linux manpage for XtRegisterGrabAction.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtRegisterGrabAction - register button and key grabs

* SYNTAX
#include <X11/Intrinsic.h>

void XtRegisterGrabAction(XtActionProc /action_proc/, Boolean
/owner_events/, unsigned int /event_mask/, int /pointer_mode/, int
/keyboard_mode/);

* ARGUMENTS
- action_proc :: Specifies the action procedure to search for in
  translation tables.

- action :: 

- event :: 

- params :: 

- num_params :: Specify arguments to *XtGrabButton* or *XtGrabKey*

* DESCRIPTION
*XtRegisterGrabAction* adds the specified /action_proc/ to a list known
to the translation manager.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
