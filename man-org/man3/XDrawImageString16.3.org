#+TITLE: Manpages - XDrawImageString16.3
#+DESCRIPTION: Linux manpage for XDrawImageString16.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XDrawImageString16.3 is found in manpage for: [[../man3/XDrawImageString.3][man3/XDrawImageString.3]]