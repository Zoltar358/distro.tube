#+TITLE: Manpages - SDL_CDResume.3
#+DESCRIPTION: Linux manpage for SDL_CDResume.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDResume - Resumes a CDROM

* SYNOPSIS
*#include "SDL.h"*

*int SDL_CDResume*(*SDL_CD *cdrom*);

* DESCRIPTION
Resumes play on the given *cdrom*.

* RETURN VALUE
Returns *0* on success, or *-1* on an error.

* SEE ALSO
*SDL_CDPlay*, *SDL_CDPause*
