#+TITLE: Manpages - xcb_input_device_state_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_input_device_state_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_input_device_state_notify_event_t -

* SYNOPSIS
*#include <xcb/xinput.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_input_device_state_notify_event_t {
      uint8_t         response_type;
      uint8_t         device_id;
      uint16_t        sequence;
      xcb_timestamp_t time;
      uint8_t         num_keys;
      uint8_t         num_buttons;
      uint8_t         num_valuators;
      uint8_t         classes_reported;
      uint8_t         buttons[4];
      uint8_t         keys[4];
      uint32_t        valuators[3];
  } xcb_input_device_state_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_INPUT_DEVICE_STATE_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- device_id :: NOT YET DOCUMENTED.

- time :: NOT YET DOCUMENTED.

- num_keys :: NOT YET DOCUMENTED.

- num_buttons :: NOT YET DOCUMENTED.

- num_valuators :: NOT YET DOCUMENTED.

- classes_reported :: NOT YET DOCUMENTED.

- buttons :: NOT YET DOCUMENTED.

- keys :: NOT YET DOCUMENTED.

- valuators :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xinput.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
