#+TITLE: Manpages - zmq_msg_set_routing_id.3
#+DESCRIPTION: Linux manpage for zmq_msg_set_routing_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_msg_set_routing_id - set routing ID property on message

* SYNOPSIS
*int zmq_msg_set_routing_id (zmq_msg_t */*message/*, uint32_t
*/routing_id/*);*

* DESCRIPTION
The /zmq_msg_set_routing_id()/ function sets the /routing_id/ specified,
on the the message pointed to by the /message/ argument. The
/routing_id/ must be greater than zero. To get a valid routing ID, you
must receive a message from a /ZMQ_SERVER/ socket, and use the
libzmq:zmq_msg_routing_id method. Routing IDs are transient.

* RETURN VALUE
The /zmq_msg_set_routing_id()/ function shall return zero if successful.
Otherwise it shall return -1 and set /errno/ to one of the values
defined below.

* ERRORS
*EINVAL*

#+begin_quote
  The provided /routing_id/ is zero.
#+end_quote

* SEE ALSO
*zmq_msg_routing_id*(3) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
