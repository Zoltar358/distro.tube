#+TITLE: Manpages - FcGetLangs.3
#+DESCRIPTION: Linux manpage for FcGetLangs.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcGetLangs - Get list of languages

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcStrSet * FcGetLangs (void*);*

* DESCRIPTION
Returns a string set of all known languages.
