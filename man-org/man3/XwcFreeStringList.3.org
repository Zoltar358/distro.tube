#+TITLE: Manpages - XwcFreeStringList.3
#+DESCRIPTION: Linux manpage for XwcFreeStringList.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XwcFreeStringList.3 is found in manpage for: [[../man3/XmbTextListToTextProperty.3][man3/XmbTextListToTextProperty.3]]