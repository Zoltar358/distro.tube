#+TITLE: Manpages - ASN1_ITEM_lookup.3ssl
#+DESCRIPTION: Linux manpage for ASN1_ITEM_lookup.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ASN1_ITEM_lookup, ASN1_ITEM_get - lookup ASN.1 structures

* SYNOPSIS
#include <openssl/asn1.h> const ASN1_ITEM *ASN1_ITEM_lookup(const char
*name); const ASN1_ITEM *ASN1_ITEM_get(size_t i);

* DESCRIPTION
*ASN1_ITEM_lookup()* returns the *ASN1_ITEM name*.

*ASN1_ITEM_get()* returns the *ASN1_ITEM* with index *i*. This function
returns *NULL* if the index *i* is out of range.

* RETURN VALUES
*ASN1_ITEM_lookup()* and *ASN1_ITEM_get()* return a valid *ASN1_ITEM*
structure or *NULL* if an error occurred.

* SEE ALSO
*ERR_get_error* (3)

* COPYRIGHT
Copyright 2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
