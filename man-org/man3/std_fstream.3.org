#+TITLE: Manpages - std::fstream.3
#+DESCRIPTION: Linux manpage for std::fstream.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about std::fstream.3 is found in manpage for: [[../man3/std_basic_fstream.3.org][man3/std_basic_fstream.3]]
