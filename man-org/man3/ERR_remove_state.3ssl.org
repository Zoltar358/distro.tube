#+TITLE: Manpages - ERR_remove_state.3ssl
#+DESCRIPTION: Linux manpage for ERR_remove_state.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ERR_remove_thread_state, ERR_remove_state - DEPRECATED

* SYNOPSIS
Deprecated:

#if OPENSSL_API_COMPAT < 0x10000000L void ERR_remove_state(unsigned long
tid); #endif #if OPENSSL_API_COMPAT < 0x10100000L void
ERR_remove_thread_state(void *tid); #endif

* DESCRIPTION
*ERR_remove_state()* frees the error queue associated with the specified
thread, identified by *tid*. *ERR_remove_thread_state()* does the same
thing, except the identifier is an opaque pointer.

* RETURN VALUES
*ERR_remove_state()* and *ERR_remove_thread_state()* return no value.

* SEE ALSO
L*OPENSSL_init_crypto* (3)

* HISTORY
*ERR_remove_state()* was deprecated in OpenSSL 1.0.0 and
*ERR_remove_thread_state()* was deprecated in OpenSSL 1.1.0; these
functions and should not be used.

* COPYRIGHT
Copyright 2000-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
