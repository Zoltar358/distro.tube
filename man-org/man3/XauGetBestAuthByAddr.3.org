#+TITLE: Manpages - XauGetBestAuthByAddr.3
#+DESCRIPTION: Linux manpage for XauGetBestAuthByAddr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XauGetBestAuthByAddr.3 is found in manpage for: [[../man3/Xau.3][man3/Xau.3]]