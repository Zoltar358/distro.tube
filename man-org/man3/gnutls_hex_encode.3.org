#+TITLE: Manpages - gnutls_hex_encode.3
#+DESCRIPTION: Linux manpage for gnutls_hex_encode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_hex_encode - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_hex_encode(const gnutls_datum_t * */data/*, char *
*/result/*, size_t * */result_size/*);*

* ARGUMENTS
- const gnutls_datum_t * data :: contain the raw data

- char * result :: the place where hex data will be copied

- size_t * result_size :: holds the size of the result

* DESCRIPTION
This function will convert the given data to printable data, using the
hex encoding, as used in the PSK password files.

Note that the size of the result includes the null terminator.

* RETURNS
*GNUTLS_E_SHORT_MEMORY_BUFFER* if the buffer given is not long enough,
or 0 on success.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
