#+TITLE: Manpages - XwcDrawString.3
#+DESCRIPTION: Linux manpage for XwcDrawString.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XwcDrawString.3 is found in manpage for: [[../man3/XmbDrawString.3][man3/XmbDrawString.3]]