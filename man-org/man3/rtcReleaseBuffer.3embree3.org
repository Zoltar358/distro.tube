#+TITLE: Manpages - rtcReleaseBuffer.3embree3
#+DESCRIPTION: Linux manpage for rtcReleaseBuffer.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcReleaseBuffer - decrements the buffer reference count
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcReleaseBuffer(RTCBuffer buffer);
#+end_example

** DESCRIPTION
Buffer objects are reference counted. The =rtcReleaseBuffer= function
decrements the reference count of the passed buffer object (=buffer=
argument). When the reference count falls to 0, the buffer gets
destroyed.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewBuffer], [rtcRetainBuffer]
