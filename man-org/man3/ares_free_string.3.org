#+TITLE: Manpages - ares_free_string.3
#+DESCRIPTION: Linux manpage for ares_free_string.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_free_string - Free strings allocated by ares functions

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_free_string(void *str)
#+end_example

* DESCRIPTION
The /ares_free_string(3)/ function frees a string allocated by an ares
function.

* SEE ALSO
*ares_mkquery*(3) *ares_expand_string*(3)

* AUTHOR
Greg Hudson, MIT Information Systems\\
Copyright 2000 by the Massachusetts Institute of Technology.
