#+TITLE: Manpages - CURLOPT_NOPROXY.3
#+DESCRIPTION: Linux manpage for CURLOPT_NOPROXY.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_NOPROXY - disable proxy use for specific hosts

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NOPROXY, char *noproxy);

* DESCRIPTION
Pass a pointer to a null-terminated string. The string consists of a
comma separated list of host names that do not require a proxy to get
reached, even if one is specified. The only wildcard available is a
single * character, which matches all hosts, and effectively disables
the proxy. Each name in this list is matched as either a domain which
contains the hostname, or the hostname itself. For example, example.com
would match example.com, example.com:80, and www.example.com, but not
www.notanexample.com or example.com.othertld.

If the name in the noproxy list has a leading period, it is a domain
match against the provided host name. This way ".example.com" will
switch off proxy use for both "www.example.com" as well as for
"foo.example.com".

Setting the noproxy string to "" (an empty string) will explicitly
enable the proxy for all host names, even if there is an environment
variable set for it.

Enter IPv6 numerical addresses in the list of host names without
enclosing brackets:

"example.com,::1,localhost"

IPv6 numerical addresses are compared as strings, so they will only
match if the representations are the same: "::1" is the same as "::0:1"
but they do not match.

The application does not have to keep the string around after setting
this option.

* Environment variables
If there's an environment variable called *no_proxy* (or *NO_PROXY*), it
will be used if the /CURLOPT_NOPROXY(3)/ option is not set. It works
exactly the same way.

* DEFAULT
NULL

* PROTOCOLS
Most

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    /* accept various URLs */
    curl_easy_setopt(curl, CURLOPT_URL, input);
    /* use this proxy */
    curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy:80");
    /* ... but make sure this host name is not proxied */
    curl_easy_setopt(curl, CURLOPT_NOPROXY, "www.example.com");
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.19.4

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_PROXY*(3), *CURLOPT_PROXYAUTH*(3),
