#+TITLE: Manpages - libssh2_knownhost_free.3
#+DESCRIPTION: Linux manpage for libssh2_knownhost_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_knownhost_free - free a collection of known hosts

* SYNOPSIS
#include <libssh2.h>

void libssh2_knownhost_free(LIBSSH2_KNOWNHOSTS *hosts);

* DESCRIPTION
Free a collection of known hosts.

* RETURN VALUE
None.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_knownhost_init(3)* *libssh2_knownhost_add(3)*
*libssh2_knownhost_check(3)*
