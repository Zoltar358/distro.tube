#+TITLE: Manpages - std_basic_string_view.3
#+DESCRIPTION: Linux manpage for std_basic_string_view.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::basic_string_view< _CharT, _Traits > - A non-owning reference to a
string.

* SYNOPSIS
\\

=#include <>>=

** Public Types
using *const_iterator* = const value_type *\\

using *const_pointer* = const value_type *\\

using *const_reference* = const value_type &\\

using *const_reverse_iterator* = *std::reverse_iterator*< const_iterator
>\\

using *difference_type* = ptrdiff_t\\

using *iterator* = const_iterator\\

using *pointer* = value_type *\\

using *reference* = value_type &\\

using *reverse_iterator* = *const_reverse_iterator*\\

using *size_type* = size_t\\

using *traits_type* = _Traits\\

using *value_type* = _CharT\\

** Public Member Functions
constexpr *basic_string_view* (const _CharT *__str) noexcept\\

constexpr *basic_string_view* (const _CharT *__str, size_type __len)
noexcept\\

constexpr *basic_string_view* (const *basic_string_view* &)
noexcept=default\\

constexpr const_reference *at* (size_type __pos) const\\

constexpr const_reference *back* () const noexcept\\

constexpr const_iterator *begin* () const noexcept\\

constexpr const_iterator *cbegin* () const noexcept\\

constexpr const_iterator *cend* () const noexcept\\

constexpr int *compare* (*basic_string_view* __str) const noexcept\\

constexpr int *compare* (const _CharT *__str) const noexcept\\

constexpr int *compare* (size_type __pos1, size_type __n1,
*basic_string_view* __str) const\\

constexpr int *compare* (size_type __pos1, size_type __n1,
*basic_string_view* __str, size_type __pos2, size_type __n2) const\\

constexpr int *compare* (size_type __pos1, size_type __n1, const _CharT
*__str) const\\

constexpr int *compare* (size_type __pos1, size_type __n1, const _CharT
*__str, size_type __n2) const noexcept(false)\\

constexpr size_type *copy* (_CharT *__str, size_type __n, size_type
__pos=0) const\\

constexpr *const_reverse_iterator* *crbegin* () const noexcept\\

constexpr *const_reverse_iterator* *crend* () const noexcept\\

constexpr const_pointer *data* () const noexcept\\

constexpr bool *empty* () const noexcept\\

constexpr const_iterator *end* () const noexcept\\

constexpr size_type *find* (_CharT __c, size_type __pos=0) const
noexcept\\

constexpr size_type *find* (*basic_string_view* __str, size_type
__pos=0) const noexcept\\

constexpr size_type *find* (const _CharT *__str, size_type __pos,
size_type __n) const noexcept\\

constexpr size_type *find* (const _CharT *__str, size_type __pos=0)
const noexcept\\

constexpr size_type *find_first_not_of* (_CharT __c, size_type __pos=0)
const noexcept\\

constexpr size_type *find_first_not_of* (*basic_string_view* __str,
size_type __pos=0) const noexcept\\

constexpr size_type *find_first_not_of* (const _CharT *__str, size_type
__pos, size_type __n) const noexcept\\

constexpr size_type *find_first_not_of* (const _CharT *__str, size_type
__pos=0) const noexcept\\

constexpr size_type *find_first_of* (_CharT __c, size_type __pos=0)
const noexcept\\

constexpr size_type *find_first_of* (*basic_string_view* __str,
size_type __pos=0) const noexcept\\

constexpr size_type *find_first_of* (const _CharT *__str, size_type
__pos, size_type __n) const noexcept\\

constexpr size_type *find_first_of* (const _CharT *__str, size_type
__pos=0) const noexcept\\

constexpr size_type *find_last_not_of* (_CharT __c, size_type
__pos=npos) const noexcept\\

constexpr size_type *find_last_not_of* (*basic_string_view* __str,
size_type __pos=npos) const noexcept\\

constexpr size_type *find_last_not_of* (const _CharT *__str, size_type
__pos, size_type __n) const noexcept\\

constexpr size_type *find_last_not_of* (const _CharT *__str, size_type
__pos=npos) const noexcept\\

constexpr size_type *find_last_of* (_CharT __c, size_type __pos=npos)
const noexcept\\

constexpr size_type *find_last_of* (*basic_string_view* __str, size_type
__pos=npos) const noexcept\\

constexpr size_type *find_last_of* (const _CharT *__str, size_type
__pos, size_type __n) const noexcept\\

constexpr size_type *find_last_of* (const _CharT *__str, size_type
__pos=npos) const noexcept\\

constexpr const_reference *front* () const noexcept\\

constexpr size_type *length* () const noexcept\\

constexpr size_type *max_size* () const noexcept\\

constexpr *basic_string_view* & *operator=* (const *basic_string_view*
&) noexcept=default\\

constexpr const_reference *operator[]* (size_type __pos) const
noexcept\\

constexpr *const_reverse_iterator* *rbegin* () const noexcept\\

constexpr void *remove_prefix* (size_type __n) noexcept\\

constexpr void *remove_suffix* (size_type __n) noexcept\\

constexpr *const_reverse_iterator* *rend* () const noexcept\\

constexpr size_type *rfind* (_CharT __c, size_type __pos=npos) const
noexcept\\

constexpr size_type *rfind* (*basic_string_view* __str, size_type
__pos=npos) const noexcept\\

constexpr size_type *rfind* (const _CharT *__str, size_type __pos,
size_type __n) const noexcept\\

constexpr size_type *rfind* (const _CharT *__str, size_type __pos=npos)
const noexcept\\

constexpr size_type *size* () const noexcept\\

constexpr *basic_string_view* *substr* (size_type __pos=0, size_type
__n=npos) const noexcept(false)\\

constexpr void *swap* (*basic_string_view* &__sv) noexcept\\

** Static Public Attributes
static constexpr size_type *npos*\\

* Detailed Description
** "template<typename _CharT, typename _Traits =
std::char_traits<_CharT>>
\\
class std::basic_string_view< _CharT, _Traits >"A non-owning reference
to a string.

*Template Parameters*

#+begin_quote
  /_CharT/ Type of character\\
  /_Traits/ Traits for character type, defaults to char_traits<_CharT>.
#+end_quote

A basic_string_view looks like this:

#+begin_example
  _CharT*    _M_str
  size_t     _M_len
#+end_example

Definition at line *99* of file *std/string_view*.

* Member Typedef Documentation
** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::const_iterator = const value_type*
Definition at line *114* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::const_pointer = const value_type*
Definition at line *111* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::const_reference = const value_type&
Definition at line *113* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::*const_reverse_iterator* =
*std::reverse_iterator*<const_iterator>
Definition at line *116* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::difference_type = ptrdiff_t
Definition at line *119* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::iterator = const_iterator
Definition at line *115* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::pointer = value_type*
Definition at line *110* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::reference = value_type&
Definition at line *112* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::*reverse_iterator* = *const_reverse_iterator*
Definition at line *117* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::size_type = size_t
Definition at line *118* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::traits_type = _Traits
Definition at line *108* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> using *std::basic_string_view*< _CharT,
_Traits >::value_type = _CharT
Definition at line *109* of file *std/string_view*.

* Constructor & Destructor Documentation
** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *std::basic_string_view*< _CharT,
_Traits >::*basic_string_view* ()= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *125* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *std::basic_string_view*< _CharT,
_Traits >::*basic_string_view* (const _CharT * __str)= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *132* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *std::basic_string_view*< _CharT,
_Traits >::*basic_string_view* (const _CharT * __str, size_type
__len)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *138* of file *std/string_view*.

* Member Function Documentation
** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_reference
*std::basic_string_view*< _CharT, _Traits >::at (size_type __pos)
const= [inline]=, = [constexpr]=
Definition at line *239* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_reference
*std::basic_string_view*< _CharT, _Traits >::back () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *256* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_iterator
*std::basic_string_view*< _CharT, _Traits >::begin () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *177* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_iterator
*std::basic_string_view*< _CharT, _Traits >::cbegin () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *185* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_iterator
*std::basic_string_view*< _CharT, _Traits >::cend () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *189* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr int *std::basic_string_view*<
_CharT, _Traits >::compare (*basic_string_view*< _CharT, _Traits >
__str) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *312* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr int *std::basic_string_view*<
_CharT, _Traits >::compare (const _CharT * __str) const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *333* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr int *std::basic_string_view*<
_CharT, _Traits >::compare (size_type __pos1, size_type __n1,
*basic_string_view*< _CharT, _Traits > __str) const= [inline]=,
= [constexpr]=
Definition at line *322* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr int *std::basic_string_view*<
_CharT, _Traits >::compare (size_type __pos1, size_type __n1,
*basic_string_view*< _CharT, _Traits > __str, size_type __pos2,
size_type __n2) const= [inline]=, = [constexpr]=
Definition at line *326* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr int *std::basic_string_view*<
_CharT, _Traits >::compare (size_type __pos1, size_type __n1, const
_CharT * __str) const= [inline]=, = [constexpr]=
Definition at line *337* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr int *std::basic_string_view*<
_CharT, _Traits >::compare (size_type __pos1, size_type __n1, const
_CharT * __str, size_type __n2) const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *341* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::copy (_CharT * __str, size_type __n, size_type __pos
= =0=) const= [inline]=, = [constexpr]=
Definition at line *292* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *const_reverse_iterator*
*std::basic_string_view*< _CharT, _Traits >::crbegin ()
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *201* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *const_reverse_iterator*
*std::basic_string_view*< _CharT, _Traits >::crend () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *205* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_pointer
*std::basic_string_view*< _CharT, _Traits >::data () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *263* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr bool *std::basic_string_view*<
_CharT, _Traits >::empty () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *226* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_iterator
*std::basic_string_view*< _CharT, _Traits >::end () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *181* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find (_CharT __c, size_type
__pos = =0=) const= [constexpr]=, = [noexcept]=
Definition at line *80* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find (*basic_string_view*< _CharT, _Traits > __str,
size_type __pos = =0=) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *396* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find (const _CharT * __str,
size_type __pos, size_type __n) const= [constexpr]=, = [noexcept]=
Definition at line *47* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find (const _CharT * __str, size_type __pos = =0=)
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *406* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find_first_not_of (_CharT
__c, size_type __pos = =0=) const= [constexpr]=, = [noexcept]=
Definition at line *185* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_first_not_of (*basic_string_view*< _CharT,
_Traits > __str, size_type __pos = =0=) const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *457* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find_first_not_of (const
_CharT * __str, size_type __pos, size_type __n) const= [constexpr]=,
= [noexcept]=
Definition at line *172* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_first_not_of (const _CharT * __str, size_type
__pos = =0=) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *469* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_first_of (_CharT __c, size_type __pos = =0=)
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *428* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_first_of (*basic_string_view*< _CharT, _Traits >
__str, size_type __pos = =0=) const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *424* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find_first_of (const _CharT
* __str, size_type __pos, size_type __n) const= [constexpr]=,
= [noexcept]=
Definition at line *133* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_first_of (const _CharT * __str, size_type __pos
= =0=) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *436* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find_last_not_of (_CharT
__c, size_type __pos = =npos=) const= [constexpr]=, = [noexcept]=
Definition at line *218* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_last_not_of (*basic_string_view*< _CharT,
_Traits > __str, size_type __pos = =npos=) const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *476* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find_last_not_of (const
_CharT * __str, size_type __pos, size_type __n) const= [constexpr]=,
= [noexcept]=
Definition at line *196* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_last_not_of (const _CharT * __str, size_type
__pos = =npos=) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *488* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_last_of (_CharT __c, size_type __pos = =npos=)
const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *445* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_last_of (*basic_string_view*< _CharT, _Traits >
__str, size_type __pos = =npos=) const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *440* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::find_last_of (const _CharT
* __str, size_type __pos, size_type __n) const= [constexpr]=,
= [noexcept]=
Definition at line *150* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::find_last_of (const _CharT * __str, size_type __pos =
=npos=) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *453* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_reference
*std::basic_string_view*< _CharT, _Traits >::front () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *249* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::length () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *215* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::max_size () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *219* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr const_reference
*std::basic_string_view*< _CharT, _Traits >::operator[] (size_type
__pos) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *232* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *const_reverse_iterator*
*std::basic_string_view*< _CharT, _Traits >::rbegin () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *193* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr void *std::basic_string_view*<
_CharT, _Traits >::remove_prefix (size_type __n)= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *269* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr void *std::basic_string_view*<
_CharT, _Traits >::remove_suffix (size_type __n)= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *277* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *const_reverse_iterator*
*std::basic_string_view*< _CharT, _Traits >::rend () const= [inline]=,
= [constexpr]=, = [noexcept]=
Definition at line *197* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::rfind (_CharT __c,
size_type __pos = =npos=) const= [constexpr]=, = [noexcept]=
Definition at line *116* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::rfind (*basic_string_view*< _CharT, _Traits > __str,
size_type __pos = =npos=) const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *410* of file *std/string_view*.

** template<typename _CharT , typename _Traits > constexpr
*basic_string_view*< _CharT, _Traits >::size_type
*std::basic_string_view*< _CharT, _Traits >::rfind (const _CharT *
__str, size_type __pos, size_type __n) const= [constexpr]=,
= [noexcept]=
Definition at line *96* of file *bits/string_view.tcc*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::rfind (const _CharT * __str, size_type __pos =
=npos=) const= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *420* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::size () const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *211* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr *basic_string_view*
*std::basic_string_view*< _CharT, _Traits >::substr (size_type __pos =
=0=, size_type __n = =npos=) const= [inline]=, = [constexpr]=,
= [noexcept]=
Definition at line *304* of file *std/string_view*.

** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr void *std::basic_string_view*<
_CharT, _Traits >::swap (*basic_string_view*< _CharT, _Traits > &
__sv)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *281* of file *std/string_view*.

* Member Data Documentation
** template<typename _CharT , typename _Traits =
std::char_traits<_CharT>> constexpr size_type *std::basic_string_view*<
_CharT, _Traits >::npos= [static]=, = [constexpr]=
Definition at line *120* of file *std/string_view*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
