#+TITLE: Manpages - gnu_get_libc_release.3
#+DESCRIPTION: Linux manpage for gnu_get_libc_release.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about gnu_get_libc_release.3 is found in manpage for: [[../man3/gnu_get_libc_version.3][man3/gnu_get_libc_version.3]]