#+TITLE: Manpages - acl_get_fd.3
#+DESCRIPTION: Linux manpage for acl_get_fd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function retrieves the access ACL associated with the file referred to
by

The ACL is placed into working storage and

returns a pointer to that storage.

In order to read an ACL from an object, a process must have read access
to the object's attributes.

This function may cause memory to be allocated. The caller should free
any releasable memory, when the new ACL is no longer required, by
calling

with the

returned by

as an argument.

On success, this function shall return a pointer to the working storage.
On error, a value of

shall be returned, and

is set appropriately.

If any of the following conditions occur, the

function returns a value of

and sets

to the corresponding value:

The

argument is not a valid file descriptor.

The ACL working storage requires more memory than is allowed by the
hardware or system-imposed memory management constraints.

The file system on which the file identified by

is located does not support ACLs, or ACLs are disabled.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
