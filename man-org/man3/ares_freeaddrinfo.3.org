#+TITLE: Manpages - ares_freeaddrinfo.3
#+DESCRIPTION: Linux manpage for ares_freeaddrinfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ares_freeaddrinfo - Free addrinfo structure allocated by ares functions

* SYNOPSIS
#+begin_example
  #include <ares.h>

  void ares_freeaddrinfo(struct ares_addrinfo *ai)
#+end_example

* DESCRIPTION
The *ares_freeaddrinfo* function frees a *struct ares_addrinfo* returned
in /result/ of *ares_addrinfo_callback*

* SEE ALSO
*ares_getaddrinfo*(3),

* AUTHOR
Christian Ammer Andrew Selivanov <andrew.selivanov@gmail.com>
