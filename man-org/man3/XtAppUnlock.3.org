#+TITLE: Manpages - XtAppUnlock.3
#+DESCRIPTION: Linux manpage for XtAppUnlock.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppUnlock.3 is found in manpage for: [[../man3/XtAppLock.3][man3/XtAppLock.3]]