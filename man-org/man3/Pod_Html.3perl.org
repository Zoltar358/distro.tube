#+TITLE: Manpages - Pod_Html.3perl
#+DESCRIPTION: Linux manpage for Pod_Html.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Html - module to convert pod files to HTML

* SYNOPSIS
use Pod::Html; pod2html([options]);

* DESCRIPTION
Converts files from pod format (see perlpod) to HTML format. It can
automatically generate indexes and cross-references, and it keeps a
cache of things it knows how to cross-reference.

* FUNCTIONS
** pod2html
pod2html("pod2html", "--podpath=lib:ext:pod:vms",
"--podroot=/usr/src/perl", "--htmlroot=/perl/nmanual", "--recurse",
"--infile=foo.pod", "--outfile=/perl/nmanual/foo.html");

pod2html takes the following arguments:

- backlink ::  --backlink Turns every =head1= heading into a link back
  to the top of the page. By default, no backlinks are generated.

- cachedir ::  --cachedir=name Creates the directory cache in the given
  directory.

- css ::  --css=stylesheet Specify the URL of a cascading style sheet.
  Also disables all HTML/CSS =style= attributes that are output by
  default (to avoid conflicts).

- flush ::  --flush Flushes the directory cache.

- header ::  --header --noheader Creates header and footer blocks
  containing the text of the =NAME= section. By default, no headers are
  generated.

- help ::  --help Displays the usage message.

- htmldir ::  --htmldir=name Sets the directory to which all cross
  references in the resulting html file will be relative. Not passing
  this causes all links to be absolute since this is the value that
  tells Pod::Html the root of the documentation tree. Do not use this
  and --htmlroot in the same call to pod2html; they are mutually
  exclusive.

- htmlroot ::  --htmlroot=name Sets the base URL for the HTML files.
  When cross-references are made, the HTML root is prepended to the URL.
  Do not use this if relative links are desired: use --htmldir instead.
  Do not pass both this and --htmldir to pod2html; they are mutually
  exclusive.

- index ::  --index --noindex Generate an index at the top of the HTML
  file. This is the default behaviour.

- infile ::  --infile=name Specify the pod file to convert. Input is
  taken from STDIN if no infile is specified.

- outfile ::  --outfile=name Specify the HTML file to create. Output
  goes to STDOUT if no outfile is specified.

- poderrors ::  --poderrors --nopoderrors Include a POD ERRORS section
  in the outfile if there were any POD errors in the infile. This
  section is included by default.

- podpath ::  --podpath=name:...:name Specify which subdirectories of
  the podroot contain pod files whose HTML converted forms can be linked
  to in cross references.

- podroot ::  --podroot=name Specify the base directory for finding
  library pods. Default is the current working directory.

- quiet ::  --quiet --noquiet Don't display /mostly harmless/ warning
  messages. These messages will be displayed by default. But this is not
  the same as =verbose= mode.

- recurse ::  --recurse --norecurse Recurse into subdirectories
  specified in podpath (default behaviour).

- title ::  --title=title Specify the title of the resulting HTML file.

- verbose ::  --verbose --noverbose Display progress messages. By
  default, they won't be displayed.

** htmlify
htmlify($heading);

Converts a pod section specification to a suitable section specification
for HTML. Note that we keep spaces and special characters except =", ?=
(Netscape problem) and the hyphen (writer's problem...).

** anchorify
anchorify(@heading);

Similar to =htmlify()=, but turns non-alphanumerics into underscores.
Note that =anchorify()= is not exported by default.

* ENVIRONMENT
Uses =$Config{pod2html}= to setup default options.

* AUTHOR
Marc Green, <marcgreen@cpan.org>.

Original version by Tom Christiansen, <tchrist@perl.com>.

* SEE ALSO
perlpod

* COPYRIGHT
This program is distributed under the Artistic License.
