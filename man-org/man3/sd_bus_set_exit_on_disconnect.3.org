#+TITLE: Manpages - sd_bus_set_exit_on_disconnect.3
#+DESCRIPTION: Linux manpage for sd_bus_set_exit_on_disconnect.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_set_exit_on_disconnect, sd_bus_get_exit_on_disconnect - Control
the exit behavior when the bus object disconnects

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_set_exit_on_disconnect(sd_bus **/bus/*, int */b/*);*

*int sd_bus_get_exit_on_disconnect(sd_bus **/bus/*);*

* DESCRIPTION
*sd_bus_set_exit_on_disconnect()* may be used to configure the exit
behavior when the given bus object disconnects. If /b/ is zero, no
special logic is executed when the bus object disconnects. If /b/ is
non-zero, the behavior on disconnect depends on whether the bus object
is attached to an event loop or not. If the bus object is attached to an
event loop (see *sd_bus_attach_event*(3)), the event loop is closed when
the bus object disconnects (as if calling *sd_event_exit*(3)).
Otherwise, *exit*(3) is called. The exit code passed to
*sd_event_exit()* and *exit()* is *EXIT_FAILURE*. If the bus object has
already disconnected when enabling the exit behavior, the exit behavior
is executed immediately. By default, the exit behavior is disabled.

*sd_bus_get_exit_on_disconnect()* returns whether the exit on disconnect
behavior is enabled for the given bus object.

* RETURN VALUE
On success, *sd_bus_set_exit_on_disconnect()* returns a non-negative
integer. On failure, it returns a negative errno-style error code.

*sd_bus_get_exit_on_disconnect()* returns a positive integer if the exit
on disconnect behavior is enabled. Otherwise, it returns zero.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  A required parameter was *NULL*.
#+end_quote

*-ENOPKG*

#+begin_quote
  The bus object could not be resolved.
#+end_quote

*-ECHILD*

#+begin_quote
  The bus connection was created in a different process.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_attach_event*(3), *sd-event*(3),
*sd_event_exit*(3)
