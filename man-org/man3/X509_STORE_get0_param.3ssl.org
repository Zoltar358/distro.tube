#+TITLE: Manpages - X509_STORE_get0_param.3ssl
#+DESCRIPTION: Linux manpage for X509_STORE_get0_param.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
X509_STORE_get0_param, X509_STORE_set1_param, X509_STORE_get0_objects -
X509_STORE setter and getter functions

* SYNOPSIS
#include <openssl/x509_vfy.h> X509_VERIFY_PARAM
*X509_STORE_get0_param(X509_STORE *ctx); int
X509_STORE_set1_param(X509_STORE *ctx, X509_VERIFY_PARAM *pm);
STACK_OF(X509_OBJECT) *X509_STORE_get0_objects(X509_STORE *ctx);

* DESCRIPTION
*X509_STORE_set1_param()* sets the verification parameters to *pm* for
*ctx*.

*X509_STORE_get0_param()* retrieves an internal pointer to the
verification parameters for *ctx*. The returned pointer must not be
freed by the calling application

*X509_STORE_get0_objects()* retrieve an internal pointer to the store's
X509 object cache. The cache contains *X509* and *X509_CRL* objects. The
returned pointer must not be freed by the calling application.

* RETURN VALUES
*X509_STORE_get0_param()* returns a pointer to an *X509_VERIFY_PARAM*
structure.

*X509_STORE_set1_param()* returns 1 for success and 0 for failure.

*X509_STORE_get0_objects()* returns a pointer to a stack of
*X509_OBJECT*.

* SEE ALSO
*X509_STORE_new* (3)

* HISTORY
*X509_STORE_get0_param* and *X509_STORE_get0_objects* were added in
OpenSSL 1.1.0.

* COPYRIGHT
Copyright 2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
