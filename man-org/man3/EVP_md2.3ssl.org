#+TITLE: Manpages - EVP_md2.3ssl
#+DESCRIPTION: Linux manpage for EVP_md2.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_md2 - MD2 For EVP

* SYNOPSIS
#include <openssl/evp.h> const EVP_MD *EVP_md2(void);

* DESCRIPTION
MD2 is a cryptographic hash function standardized in RFC 1319 and
designed by Ronald Rivest.

- EVP_md2() :: The MD2 algorithm which produces a 128-bit output from a
  given input.

* RETURN VALUES
These functions return a *EVP_MD* structure that contains the
implementation of the symmetric cipher. See *EVP_MD_meth_new* (3) for
details of the *EVP_MD* structure.

* CONFORMING TO
IETF RFC 1319.

* SEE ALSO
*evp* (7), *EVP_DigestInit* (3)

* COPYRIGHT
Copyright 2017 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
