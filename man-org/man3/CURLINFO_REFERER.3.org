#+TITLE: Manpages - CURLINFO_REFERER.3
#+DESCRIPTION: Linux manpage for CURLINFO_REFERER.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_REFERER - get the referrer header

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REFERER, char **hdrp);

* DESCRIPTION
Pass in a pointer to a char pointer and get the referrer header.

The *hdrp* pointer will be NULL or pointing to private memory you MUST
NOT free - it gets freed when you call /curl_easy_cleanup(3)/ on the
corresponding CURL handle.

* PROTOCOLS
HTTP(S)

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_REFERER, "https://example.org/referrer");
    res = curl_easy_perform(curl);
    if(res == CURLE_OK) {
      char *hdr = NULL;
      curl_easy_getinfo(curl, CURLINFO_REFERER, &hdr);
      if(hdr)
        printf("Referrer header: %s\n", hdr);
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.76.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3), *CURLOPT_REFERER*(3),
