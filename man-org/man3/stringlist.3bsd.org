#+TITLE: Manpages - stringlist.3bsd
#+DESCRIPTION: Linux manpage for stringlist.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

functions manipulate stringlists, which are lists of strings that extend
automatically if necessary.

The

structure has the following definition:

typedef struct _stringlist { char **sl_str; size_t sl_max; size_t
sl_cur; } StringList;

where:

is a pointer to the base of the array containing the list,

is the size of

and

is the offset in

of the current element.

The following stringlist manipulation functions are available:

Create a stringlist. Returns a pointer to a

or

in case of failure.

Releases memory occupied by

and the

array. If

is non-zero, then each of the items within

is released as well.

Add

to

at

extending the size of

Returns zero upon success, -1 upon failure.

Find

in

returning

if it's not found.

Remove

from the list. If

is non-zero, the string is freed. Returns

if the name is found and

if the name is not found.

The

functions appeared in

and
