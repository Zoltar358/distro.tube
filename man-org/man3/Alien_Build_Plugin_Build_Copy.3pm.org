#+TITLE: Manpages - Alien_Build_Plugin_Build_Copy.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Build_Copy.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Build::Copy - Copy plugin for Alien::Build

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Build::Copy;

* DESCRIPTION
This plugin copies all of the files from the source to the staging
prefix. This is mainly useful for software packages that are provided as
binary blobs. It works on both Unix and Windows using the appropriate
commands for those platforms without having worry about the platform
details in your alienfile.

If you want to filter add or remove files from what gets installed you
can use a =before= hook.

build { ... before build => sub { # remove or modify files }; plugin
Build::Copy; ... };

Some packages might have binary blobs on some platforms and require
build from source on others. In that situation you can use =if=
statements with the appropriate logic in your alienfile.

configure { # normally the Build::Copy plugin will insert itself # as a
config requires, but since it is only used # on some platforms, you will
want to explicitly # require it in your alienfile in case you build your
# alien dist on a platform that doesnt use it. requires
Alien::Build::Plugin::Build::Copy; }; build { ... if($^O eq linux) {
start_url http://example.com/binary-blob-linux.tar.gz; plugin Download;
plugin Extract => tar.gz; plugin Build::Copy; } else { start_url
http://example.com/source.tar.gz; plugin Download; plugin Extract =>
tar.gz; plugin Build::Autoconf; } };

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
