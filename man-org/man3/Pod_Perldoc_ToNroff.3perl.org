#+TITLE: Manpages - Pod_Perldoc_ToNroff.3perl
#+DESCRIPTION: Linux manpage for Pod_Perldoc_ToNroff.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Perldoc::ToNroff - let Perldoc convert Pod to nroff

* SYNOPSIS
perldoc -o nroff -d something.3 Some::Modulename

* DESCRIPTION
This is a plug-in class that allows Perldoc to use Pod::Man as a
formatter class.

The following options are supported: center, date, fixed, fixedbold,
fixeditalic, fixedbolditalic, quotes, release, section

Those options are explained in Pod::Man.

For example:

perldoc -o nroff -w center:Pod -d something.3 Some::Modulename

* CAVEAT
This module may change to use a different pod-to-nroff formatter class
in the future, and this may change what options are supported.

* SEE ALSO
Pod::Man, Pod::Perldoc, Pod::Perldoc::ToMan

* COPYRIGHT AND DISCLAIMERS
Copyright (c) 2002 Sean M. Burke. All rights reserved.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

* AUTHOR
Current maintainer: Mark Allen =<mallen@cpan.org>=

Past contributions from: brian d foy =<bdfoy@cpan.org>= Adriano R.
Ferreira =<ferreira@cpan.org>=, Sean M. Burke =<sburke@cpan.org>=
