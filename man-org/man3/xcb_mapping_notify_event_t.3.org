#+TITLE: Manpages - xcb_mapping_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_mapping_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_mapping_notify_event_t - keyboard mapping changed

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_mapping_notify_event_t {
      uint8_t       response_type;
      uint8_t       pad0;
      uint16_t      sequence;
      uint8_t       request;
      xcb_keycode_t first_keycode;
      uint8_t       count;
      uint8_t       pad1;
  } xcb_mapping_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_MAPPING_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- request :: 

- first_keycode :: The first number in the range of the altered mapping.

- count :: The number of keycodes altered.

* DESCRIPTION
* SEE ALSO
*xcb_generic_event_t*(3)

* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
