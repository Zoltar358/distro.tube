#+TITLE: Manpages - CURLINFO_CONTENT_LENGTH_DOWNLOAD_T.3
#+DESCRIPTION: Linux manpage for CURLINFO_CONTENT_LENGTH_DOWNLOAD_T.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_CONTENT_LENGTH_DOWNLOAD_T - get content-length of download

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle,
CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, curl_off_t *content_length);

* DESCRIPTION
Pass a pointer to a /curl_off_t/ to receive the content-length of the
download. This is the value read from the Content-Length: field. Stores
-1 if the size is not known.

* PROTOCOLS
HTTP(S)

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* Perform the request */
    res = curl_easy_perform(curl);

    if(!res) {
      /* check the size */
      curl_off_t cl;
      res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &cl);
      if(!res) {
        printf("Download size: %" CURL_FORMAT_CURL_OFF_T "\n", cl);
      }
    }
  }
#+end_example

* AVAILABILITY
Added in 7.55.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
*CURLINFO_CONTENT_LENGTH_UPLOAD_T*(3),
