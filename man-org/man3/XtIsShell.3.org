#+TITLE: Manpages - XtIsShell.3
#+DESCRIPTION: Linux manpage for XtIsShell.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtIsShell.3 is found in manpage for: [[../man3/XtClass.3][man3/XtClass.3]]