#+TITLE: Manpages - std_binder2nd.3
#+DESCRIPTION: Linux manpage for std_binder2nd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::binder2nd< _Operation > - One of the *binder functors*.

* SYNOPSIS
\\

=#include <binders.h>=

Inherits *std::unary_function< _Operation::first_argument_type,
_Operation::result_type >*.

** Public Types
typedef _Operation::first_argument_type *argument_type*\\
=argument_type= is the type of the argument

typedef _Operation::result_type *result_type*\\
=result_type= is the return type

** Public Member Functions
*binder2nd* (const _Operation &__x, const typename
_Operation::second_argument_type &__y)\\

_Operation::result_type *operator()* (const typename
_Operation::first_argument_type &__x) const\\

_Operation::result_type *operator()* (typename
_Operation::first_argument_type &__x) const\\

** Protected Attributes
_Operation *op*\\

_Operation::second_argument_type *value*\\

* Detailed Description
** "template<typename _Operation>
\\
class std::binder2nd< _Operation >"One of the *binder functors*.

Definition at line *143* of file *binders.h*.

* Member Typedef Documentation
** typedef _Operation::first_argument_type *std::unary_function*<
_Operation::first_argument_type , _Operation::result_type
>::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef _Operation::result_type *std::unary_function*<
_Operation::first_argument_type , _Operation::result_type
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Constructor & Destructor Documentation
** template<typename _Operation > *std::binder2nd*< _Operation
>::*binder2nd* (const _Operation & __x, const typename
_Operation::second_argument_type & __y)= [inline]=
Definition at line *152* of file *binders.h*.

* Member Function Documentation
** template<typename _Operation > _Operation::result_type
*std::binder2nd*< _Operation >::operator() (const typename
_Operation::first_argument_type & __x) const= [inline]=
Definition at line *157* of file *binders.h*.

** template<typename _Operation > _Operation::result_type
*std::binder2nd*< _Operation >::operator() (typename
_Operation::first_argument_type & __x) const= [inline]=
Definition at line *163* of file *binders.h*.

* Member Data Documentation
** template<typename _Operation > _Operation *std::binder2nd*<
_Operation >::op= [protected]=
Definition at line *148* of file *binders.h*.

** template<typename _Operation > _Operation::second_argument_type
*std::binder2nd*< _Operation >::value= [protected]=
Definition at line *149* of file *binders.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
