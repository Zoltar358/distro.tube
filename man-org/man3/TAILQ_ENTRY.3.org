#+TITLE: Manpages - TAILQ_ENTRY.3
#+DESCRIPTION: Linux manpage for TAILQ_ENTRY.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about TAILQ_ENTRY.3 is found in manpage for: [[../man3/tailq.3][man3/tailq.3]]