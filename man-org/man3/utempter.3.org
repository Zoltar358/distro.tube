#+TITLE: Manpages - utempter.3
#+DESCRIPTION: Linux manpage for utempter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
function adds a login record to the database for the TTY belonging to
the pseudo-terminal master file descriptor

using the username corresponding with the real user ID of the calling
process and the optional hostname

This function spawns a privileged process to perform the actual logging.

function marks the login session as being closed for the TTY belonging
to the pseudo-terminal master file descriptor

This function spawns a privileged process to perform the actual logging.

function has the same properties as the previously mentioned function,
except that it uses an internally cached value of the file descriptor
passed to the login functions.

function changes default helper path to the specified value. The pointer
passed to this function must remain valid all the time while utempter
interface is in use.

On error, zero is returned. On success, a non-zero value is returned.

During execution of the privileged process spawned by these functions,
SIGCHLD signal handler will be temporarily set to the default action.

These functions appeared in ALT Linux Sisyphus in October 2001.
