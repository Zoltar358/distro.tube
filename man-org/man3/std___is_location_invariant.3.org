#+TITLE: Manpages - std___is_location_invariant.3
#+DESCRIPTION: Linux manpage for std___is_location_invariant.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__is_location_invariant< _Tp >

* SYNOPSIS
\\

=#include <std_function.h>=

Inherits is_trivially_copyable::type.

* Detailed Description
** "template<typename _Tp>
\\
struct std::__is_location_invariant< _Tp >"Trait identifying
'location-invariant' types, meaning that the address of the object (or
any of its members) will not escape. Trivially copyable types are
location-invariant and users can specialize this trait for other types.

Definition at line *69* of file *std_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
