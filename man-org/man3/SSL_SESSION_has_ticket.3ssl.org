#+TITLE: Manpages - SSL_SESSION_has_ticket.3ssl
#+DESCRIPTION: Linux manpage for SSL_SESSION_has_ticket.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_SESSION_get0_ticket, SSL_SESSION_has_ticket,
SSL_SESSION_get_ticket_lifetime_hint - get details about the ticket
associated with a session

* SYNOPSIS
#include <openssl/ssl.h> int SSL_SESSION_has_ticket(const SSL_SESSION
*s); unsigned long SSL_SESSION_get_ticket_lifetime_hint(const
SSL_SESSION *s); void SSL_SESSION_get0_ticket(const SSL_SESSION *s,
const unsigned char **tick, size_t *len);

* DESCRIPTION
*SSL_SESSION_has_ticket()* returns 1 if there is a Session Ticket
associated with this session, and 0 otherwise.

SSL_SESSION_get_ticket_lifetime_hint returns the lifetime hint in
seconds associated with the session ticket.

SSL_SESSION_get0_ticket obtains a pointer to the ticket associated with
a session. The length of the ticket is written to **len*. If *tick* is
non NULL then a pointer to the ticket is written to **tick*. The pointer
is only valid while the connection is in use. The session (and hence the
ticket pointer) may also become invalid as a result of a call to
*SSL_CTX_flush_sessions()*.

* RETURN VALUES
*SSL_SESSION_has_ticket()* returns 1 if session ticket exists or 0
otherwise.

*SSL_SESSION_get_ticket_lifetime_hint()* returns the number of seconds.

* SEE ALSO
*ssl* (7), *d2i_SSL_SESSION* (3), *SSL_SESSION_get_time* (3),
*SSL_SESSION_free* (3)

* HISTORY
The *SSL_SESSION_has_ticket()*, *SSL_SESSION_get_ticket_lifetime_hint()*
and *SSL_SESSION_get0_ticket()* functions were added in OpenSSL 1.1.0.

* COPYRIGHT
Copyright 2015-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
