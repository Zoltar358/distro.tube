#+TITLE: Manpages - expand_number.3bsd
#+DESCRIPTION: Linux manpage for expand_number.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function unformats the

string and stores a unsigned 64-bit quantity at address pointed out by
the

argument.

The

function follows the SI power of two convention.

The prefixes are:

The

function will fail if:

The given string contains no digits.

An unrecognized prefix was given.

Result doesn't fit into 64 bits.

The

function first appeared in
