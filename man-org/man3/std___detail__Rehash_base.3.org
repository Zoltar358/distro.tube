#+TITLE: Manpages - std___detail__Rehash_base.3
#+DESCRIPTION: Linux manpage for std___detail__Rehash_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Rehash_base< _Key, _Value, _Alloc, _ExtractKey, _Equal,
_Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, typename >

* SYNOPSIS
\\

Inherited by *std::_Hashtable< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits >*.

* Detailed Description
** "template<typename _Key, typename _Value, typename _Alloc, typename
_ExtractKey, typename _Equal, typename _Hash, typename _RangeHash,
typename _Unused, typename _RehashPolicy, typename _Traits, typename =
__detected_or_t<false_type, __has_load_factor, _RehashPolicy>>
\\
struct std::__detail::_Rehash_base< _Key, _Value, _Alloc, _ExtractKey,
_Equal, _Hash, _RangeHash, _Unused, _RehashPolicy, _Traits, typename
>"Primary class template _Rehash_base.

Give hashtable the max_load_factor functions and reserve iff the rehash
policy supports it.

Definition at line *1063* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
