#+TITLE: Manpages - std_numpunct_byname.3
#+DESCRIPTION: Linux manpage for std_numpunct_byname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::numpunct_byname< _CharT > - class numpunct_byname [22.2.3.2].

* SYNOPSIS
\\

=#include <locale_facets.h>=

Inherits *std::numpunct< _CharT >*.

** Public Types
typedef __numpunct_cache< _CharT > *__cache_type*\\

typedef _CharT *char_type*\\

typedef *basic_string*< _CharT > *string_type*\\

** Public Member Functions
*numpunct_byname* (const char *__s, size_t __refs=0)\\

*numpunct_byname* (const *string* &__s, size_t __refs=0)\\

*char_type* *decimal_point* () const\\
Return decimal point character.

*string_type* *falsename* () const\\
Return string representation of bool false.

*string* *grouping* () const\\
Return grouping specification.

*char_type* *thousands_sep* () const\\
Return thousands separator character.

*string_type* *truename* () const\\
Return string representation of bool true.

** Static Public Attributes
static *locale::id* *id*\\
Numpunct facet id.

** Protected Member Functions
void *_M_initialize_numpunct* (__c_locale __cloc)\\

void *_M_initialize_numpunct* (__c_locale __cloc)\\

void *_M_initialize_numpunct* (__c_locale __cloc=0)\\

virtual *char_type* *do_decimal_point* () const\\
Return decimal point character.

virtual *string_type* *do_falsename* () const\\
Return string representation of bool false.

virtual *string* *do_grouping* () const\\
Return grouping specification.

virtual *char_type* *do_thousands_sep* () const\\
Return thousands separator character.

virtual *string_type* *do_truename* () const\\
Return string representation of bool true.

** Static Protected Member Functions
static __c_locale *_S_clone_c_locale* (__c_locale &__cloc) throw ()\\

static void *_S_create_c_locale* (__c_locale &__cloc, const char *__s,
__c_locale __old=0)\\

static void *_S_destroy_c_locale* (__c_locale &__cloc)\\

static __c_locale *_S_get_c_locale* ()\\

static const char * *_S_get_c_name* () throw ()\\

static __c_locale *_S_lc_ctype_c_locale* (__c_locale __cloc, const char
*__s)\\

** Protected Attributes
__cache_type * *_M_data*\\

* Detailed Description
** "template<typename _CharT>
\\
class std::numpunct_byname< _CharT >"class numpunct_byname [22.2.3.2].

Definition at line *1906* of file *locale_facets.h*.

* Member Typedef Documentation
** template<typename _CharT > typedef __numpunct_cache<_CharT>
*std::numpunct*< _CharT >::__cache_type= [inherited]=
Definition at line *1682* of file *locale_facets.h*.

** template<typename _CharT > typedef _CharT *std::numpunct_byname*<
_CharT >::*char_type*
Definition at line *1909* of file *locale_facets.h*.

** template<typename _CharT > typedef *basic_string*<_CharT>
*std::numpunct_byname*< _CharT >::*string_type*
Definition at line *1910* of file *locale_facets.h*.

* Constructor & Destructor Documentation
** template<typename _CharT > *std::numpunct_byname*< _CharT
>::*numpunct_byname* (const char * __s, size_t __refs = =0=)= [inline]=,
= [explicit]=
Definition at line *1913* of file *locale_facets.h*.

** template<typename _CharT > *std::numpunct_byname*< _CharT
>::*numpunct_byname* (const *string* & __s, size_t __refs =
=0=)= [inline]=, = [explicit]=
Definition at line *1928* of file *locale_facets.h*.

** template<typename _CharT > virtual *std::numpunct_byname*< _CharT
>::~*numpunct_byname* ()= [inline]=, = [protected]=, = [virtual]=
Definition at line *1934* of file *locale_facets.h*.

* Member Function Documentation
** template<typename _CharT > *char_type* *std::numpunct*< _CharT
>::decimal_point () const= [inline]=, = [inherited]=
Return decimal point character. This function returns a char_type to use
as a decimal point. It does so by returning returning
numpunct<char_type>::do_decimal_point().

*Returns*

#+begin_quote
  /char_type/ representing a decimal point.
#+end_quote

Definition at line *1739* of file *locale_facets.h*.

References *std::numpunct< _CharT >::do_decimal_point()*.

** template<typename _CharT > virtual *char_type* *std::numpunct*<
_CharT >::do_decimal_point () const= [inline]=, = [protected]=,
= [virtual]=, = [inherited]=
Return decimal point character. Returns a char_type to use as a decimal
point. This function is a hook for derived classes to change the value
returned.

*Returns*

#+begin_quote
  /char_type/ representing a decimal point.
#+end_quote

Definition at line *1826* of file *locale_facets.h*.

Referenced by *std::numpunct< _CharT >::decimal_point()*.

** template<typename _CharT > virtual *string_type* *std::numpunct*<
_CharT >::do_falsename () const= [inline]=, = [protected]=,
= [virtual]=, = [inherited]=
Return string representation of bool false. Returns a string_type
containing the text representation for false bool variables. This
function is a hook for derived classes to change the value returned.

*Returns*

#+begin_quote
  string_type representing printed form of false.
#+end_quote

Definition at line *1877* of file *locale_facets.h*.

Referenced by *std::numpunct< _CharT >::falsename()*.

** template<typename _CharT > virtual *string* *std::numpunct*< _CharT
>::do_grouping () const= [inline]=, = [protected]=, = [virtual]=,
= [inherited]=
Return grouping specification. Returns a string representing groupings
for the integer part of a number. This function is a hook for derived
classes to change the value returned.

*See also*

#+begin_quote
  grouping() for details.
#+end_quote

*Returns*

#+begin_quote
  String representing grouping specification.
#+end_quote

Definition at line *1851* of file *locale_facets.h*.

Referenced by *std::numpunct< _CharT >::grouping()*.

** template<typename _CharT > virtual *char_type* *std::numpunct*<
_CharT >::do_thousands_sep () const= [inline]=, = [protected]=,
= [virtual]=, = [inherited]=
Return thousands separator character. Returns a char_type to use as a
thousands separator. This function is a hook for derived classes to
change the value returned.

*Returns*

#+begin_quote
  /char_type/ representing a thousands separator.
#+end_quote

Definition at line *1838* of file *locale_facets.h*.

Referenced by *std::numpunct< _CharT >::thousands_sep()*.

** template<typename _CharT > virtual *string_type* *std::numpunct*<
_CharT >::do_truename () const= [inline]=, = [protected]=, = [virtual]=,
= [inherited]=
Return string representation of bool true. Returns a string_type
containing the text representation for true bool variables. This
function is a hook for derived classes to change the value returned.

*Returns*

#+begin_quote
  string_type representing printed form of true.
#+end_quote

Definition at line *1864* of file *locale_facets.h*.

Referenced by *std::numpunct< _CharT >::truename()*.

** template<typename _CharT > *string_type* *std::numpunct*< _CharT
>::falsename () const= [inline]=, = [inherited]=
Return string representation of bool false. This function returns a
string_type containing the text representation for false bool variables.
It does so by calling numpunct<char_type>::do_falsename().

*Returns*

#+begin_quote
  string_type representing printed form of false.
#+end_quote

Definition at line *1809* of file *locale_facets.h*.

References *std::numpunct< _CharT >::do_falsename()*.

** template<typename _CharT > *string* *std::numpunct*< _CharT
>::grouping () const= [inline]=, = [inherited]=
Return grouping specification. This function returns a string
representing groupings for the integer part of a number. Groupings
indicate where thousands separators should be inserted in the integer
part of a number.

Each char in the return string is interpret as an integer rather than a
character. These numbers represent the number of digits in a group. The
first char in the string represents the number of digits in the least
significant group. If a char is negative, it indicates an unlimited
number of digits for the group. If more chars from the string are
required to group a number, the last char is used repeatedly.

For example, if the grouping() returns '\003\002' and is applied to the
number 123456789, this corresponds to 12,34,56,789. Note that if the
string was '32', this would put more than 50 digits into the least
significant group if the character set is ASCII.

The string is returned by calling numpunct<char_type>::do_grouping().

*Returns*

#+begin_quote
  string representing grouping specification.
#+end_quote

Definition at line *1783* of file *locale_facets.h*.

References *std::numpunct< _CharT >::do_grouping()*.

** template<typename _CharT > *char_type* *std::numpunct*< _CharT
>::thousands_sep () const= [inline]=, = [inherited]=
Return thousands separator character. This function returns a char_type
to use as a thousands separator. It does so by returning returning
numpunct<char_type>::do_thousands_sep().

*Returns*

#+begin_quote
  char_type representing a thousands separator.
#+end_quote

Definition at line *1752* of file *locale_facets.h*.

References *std::numpunct< _CharT >::do_thousands_sep()*.

** template<typename _CharT > *string_type* *std::numpunct*< _CharT
>::truename () const= [inline]=, = [inherited]=
Return string representation of bool true. This function returns a
string_type containing the text representation for true bool variables.
It does so by calling numpunct<char_type>::do_truename().

*Returns*

#+begin_quote
  string_type representing printed form of true.
#+end_quote

Definition at line *1796* of file *locale_facets.h*.

References *std::numpunct< _CharT >::do_truename()*.

* Member Data Documentation
** template<typename _CharT > __cache_type* *std::numpunct*< _CharT
>::_M_data= [protected]=, = [inherited]=
Definition at line *1685* of file *locale_facets.h*.

** template<typename _CharT > *locale::id* *std::numpunct*< _CharT
>::id= [static]=, = [inherited]=
Numpunct facet id.

Definition at line *1689* of file *locale_facets.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
