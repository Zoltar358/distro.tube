#+TITLE: Manpages - PEM_write_bio_PKCS7_stream.3ssl
#+DESCRIPTION: Linux manpage for PEM_write_bio_PKCS7_stream.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
PEM_write_bio_PKCS7_stream - output PKCS7 structure in PEM format

* SYNOPSIS
#include <openssl/pkcs7.h> int PEM_write_bio_PKCS7_stream(BIO *out,
PKCS7 *p7, BIO *data, int flags);

* DESCRIPTION
*PEM_write_bio_PKCS7_stream()* outputs a PKCS7 structure in PEM format.

It is otherwise identical to the function *SMIME_write_PKCS7()*.

* NOTES
This function is effectively a version of the *PEM_write_bio_PKCS7()*
supporting streaming.

* RETURN VALUES
*PEM_write_bio_PKCS7_stream()* returns 1 for success or 0 for failure.

* SEE ALSO
*ERR_get_error* (3), *PKCS7_sign* (3), *PKCS7_verify* (3),
*PKCS7_encrypt* (3) *PKCS7_decrypt* (3), *SMIME_write_PKCS7* (3),
*i2d_PKCS7_bio_stream* (3)

* HISTORY
The *PEM_write_bio_PKCS7_stream()* function was added in OpenSSL 1.0.0.

* COPYRIGHT
Copyright 2007-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
