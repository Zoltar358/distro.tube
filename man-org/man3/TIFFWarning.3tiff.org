#+TITLE: Manpages - TIFFWarning.3tiff
#+DESCRIPTION: Linux manpage for TIFFWarning.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFWarning, TIFFSetWarningHandler - library warning interface

* SYNOPSIS
*#include <tiffio.h>*

*void TIFFWarning(const char **/module/*, const char **/fmt/*, */.../*)*

*#include <stdarg.h>*

*typedef void (*TIFFWarningHandler)(const char **/module/*, const char
**/fmt/*, va_list */ap/*);*

*TIFFWarningHandler TIFFSetWarningHandler(TIFFWarningHandler
*/handler/*);*

* DESCRIPTION
/TIFFWarning/ invokes the library-wide warning handler function to
(normally) write a warning message to the *stderr*. The /fmt/ parameter
is a /printf/(3S) format string, and any number arguments can be
supplied. The /module/ parameter is interpreted as a string that, if
non-zero, should be printed before the message; it typically is used to
identify the software module in which a warning is detected.

Applications that desire to capture control in the event of a warning
should use /TIFFSetWarningHandler/ to override the default warning
handler. A

(0) warning handler function may be installed to suppress error
messages.

* RETURN VALUES
/TIFFSetWarningHandler/ returns a reference to the previous error
handling function.

* SEE ALSO
*TIFFError*(3TIFF), *libtiff*(3TIFF), *printf*(3)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
