#+TITLE: Manpages - XtGetSelectionRequest.3
#+DESCRIPTION: Linux manpage for XtGetSelectionRequest.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtGetSelectionRequest - retrieve the event that triggered the
XtConvertSelectionProc

* SYNTAX
#include <X11/Intrinsic.h>

XSelectionRequestEvent* XtGetSelectionRequest(Widget /w/, Atom
/selection/, XtRequestId /request_id/);

* ARGUMENTS

23. Specifies the widget.

- selection :: Specifies the selection being processed.

- request_id :: Specifies the requestor id in the case of incremental
  selections, or NULL in the case of atomic transfers.

* DESCRIPTION
*XtGetSelectionRequest* may only be called from within an
*XtConvertSelectionProc* procedure and returns a pointer to the
*SelectionRequest* event that caused the conversion procedure to be
invoked. /Request_id/ specifies a unique id for the individual request
in the case that multiple incremental transfers are outstanding. For
atomic transfers, /request_id/ must be specified as NULL. If no
*SelectionRequest* event is being processed for the specified /widget/,
/selection/, and /request_id/, *XtGetSelectionRequest* returns NULL.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
