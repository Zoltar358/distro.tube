#+TITLE: Manpages - zip_source_zip.3
#+DESCRIPTION: Linux manpage for zip_source_zip.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The functions

and

create a zip source from a file in a zip archive. The

argument is the (open) zip archive containing the source zip file at
index

bytes from offset

will be used in the zip_source. If

is 0 or -1, the rest of the file, starting from

is used. If

is zero and

is -1, the whole file will be copied without decompressing it.

Supported flags are:

Try to get the original data without any changes that may have been made
to

after opening it.

When adding the data from

re-compress it using the current settings instead of copying the
compressed data.

Upon successful completion, the created source is returned. Otherwise,

is returned and the error code in

or

is set to indicate the error.

and

fail if:

Unchanged data was requested, but it is not available.

or

are invalid.

Required memory could not be allocated.

Additionally, it can return all error codes from

and

was added in libzip 1.0.

was added in libzip 1.8.0.

and
