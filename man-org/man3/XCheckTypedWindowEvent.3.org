#+TITLE: Manpages - XCheckTypedWindowEvent.3
#+DESCRIPTION: Linux manpage for XCheckTypedWindowEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XCheckTypedWindowEvent.3 is found in manpage for: [[../man3/XNextEvent.3][man3/XNextEvent.3]]