#+TITLE: Manpages - MPI_Comm_group.3
#+DESCRIPTION: Linux manpage for MPI_Comm_group.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Comm_group * - Returns the group associated with a communicator.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Comm_group(MPI_Comm comm, MPI_Group *group)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_COMM_GROUP(COMM, GROUP, IERROR)
    	INTEGER	COMM, GROUP, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Comm_group(comm, group, ierror)
  	TYPE(MPI_Comm), INTENT(IN) :: comm
  	TYPE(MPI_Group), INTENT(OUT) :: group
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  Group Comm::Get_group() const
#+end_example

* INPUT PARAMETER
- comm :: Communicator.

* OUTPUT PARAMETERS
- group :: Group in communicator (handle).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
If the communicator is an intercommunicator (enables communication
between two groups of processes), this function returns the local group.
To return the remote group, use the MPI_Comm_remote_group function.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.
