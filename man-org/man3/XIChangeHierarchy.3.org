#+TITLE: Manpages - XIChangeHierarchy.3
#+DESCRIPTION: Linux manpage for XIChangeHierarchy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XIChangeHierarchy - change the device hierarchy.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput2.h>
#+end_example

#+begin_example
  Status XIChangeHierarchy( Display *display,
                            XIAnyHierarchyChangeInfo *changes,
                            int num_changes);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  num_changes
         Specifies the number of elements in changes.
#+end_example

#+begin_example
  changes
         Specifies the changes to be made.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    XIChangeHierarchy modifies the device hierarchy by creating or
    removing master devices or changing the attachment of slave
    devices. If num_changes is non-zero, changes is an array of
    XIAnyHierarchyChangeInfo structures. If num_changes is equal or less than
    zero, XIChangeHierarchy does nothing.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIChangeHierarchy processes changes in order, effective
    immediately. If an error occurs, processing is aborted and the
    error is reported to the client. Changes already made remain
    effective.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The list of changes is any combination of
    XIAnyHierarchyChangeInfo. The type of a hierarchy change can be
    XIAddMaster, XIRemoveMaster, XIAttachSlave or XIDetachSlave.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef union {
        int type;
        XIAddMasterInfo add;
        XIRemoveMasterInfo remove;
        XIAttachSlave attach;
        XIDetachSlave detach;
    } XIAnyHierarchyChangeInfo;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        int type; /* XIAddMaster */
        char* name;
        Bool send_core;
        Bool enable;
    } XIAddMasterInfo;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        int type; /* XIRemoveMaster */
        int deviceid;
        int return_mode;
        int return_pointer;
        int return_keyboard;
    } XIRemoveMasterInfo;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        int type; /* XIAttachSlave */
        int deviceid;
        int new_master;
    } XIAttachSlaveInfo;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        int type; /* XIDetachSlave */
        int deviceid;
    } XIDetachSlaveInfo;
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIAddMasterInfo creates a new master pointer and a new
    master keyboard labeled "name pointer" and "name keyboard"
    respectively. If sendCore is True, the devices will send core
    events. If enable is True, the device is enabled immediately.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIAddMasterInfo can generate a BadValue error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIRemoveMasterInfo removes device and its paired master device.
    If returnMode is XIAttachToMaster, all pointers attached to
    device or its paired master device are attached to
    returnPointer. Likewise, all keyboards are attached to
    returnKeyboard. If returnMode is XIFloating, all attached
    devices are set to floating.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIRemoveMasterInfo can generate a BadValue and a BadDevice
    error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIAttachSlaveInfo attaches device to new_master. If the device
    is currently attached to a master device, it is detached from
    the master device and attached to the new master device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIAttachSlaveInfo can generate a BadDevice error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIDetachSlaveInfo detaches device from the current master
    device and sets it floating. If the device is already floating,
    no changes are made.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIDetachSlaveInfo can generate a BadDevice error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIChangeHierarchy generates an XIHierarchyEvent if any
    modifications were successful.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The device does not
           exist or is not a appropriate for the type of change.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls out of the allowed range.
  #+end_example
#+end_quote
