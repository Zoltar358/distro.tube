#+TITLE: Manpages - sd_session_get_remote_user.3
#+DESCRIPTION: Linux manpage for sd_session_get_remote_user.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd_session_get_remote_user.3 is found in manpage for: [[../sd_session_is_active.3][sd_session_is_active.3]]