#+TITLE: Manpages - isdigit.3
#+DESCRIPTION: Linux manpage for isdigit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about isdigit.3 is found in manpage for: [[../man3/isalpha.3][man3/isalpha.3]]