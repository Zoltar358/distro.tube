#+TITLE: Manpages - std__Node_handle_common.3
#+DESCRIPTION: Linux manpage for std__Node_handle_common.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Node_handle_common< _Val, _NodeAlloc > - Base class for node
handle types of maps and sets.

* SYNOPSIS
\\

=#include <node_handle.h>=

** Public Types
using *allocator_type* = __alloc_rebind< _NodeAlloc, _Val >\\

** Public Member Functions
bool *empty* () const noexcept\\

allocator_type *get_allocator* () const noexcept\\

*operator bool* () const noexcept\\

** Protected Member Functions
*_Node_handle_common* (*_Node_handle_common* &&__nh) noexcept\\

*_Node_handle_common* (typename *_AllocTraits::pointer* __ptr, const
_NodeAlloc &__alloc)\\

void *_M_swap* (*_Node_handle_common* &__nh) noexcept\\

*_Node_handle_common* & *operator=* (*_Node_handle_common* &&__nh)
noexcept\\

** Protected Attributes
*_AllocTraits::pointer* *_M_ptr*\\

** Friends
template<typename _Key2 , typename _Value2 , typename _KeyOfValue ,
typename _Compare , typename _ValueAlloc > class *_Rb_tree*\\

* Detailed Description
** "template<typename _Val, typename _NodeAlloc>
\\
class std::_Node_handle_common< _Val, _NodeAlloc >"Base class for node
handle types of maps and sets.

Definition at line *49* of file *node_handle.h*.

* Member Typedef Documentation
** template<typename _Val , typename _NodeAlloc > using
*std::_Node_handle_common*< _Val, _NodeAlloc >::allocator_type =
__alloc_rebind<_NodeAlloc, _Val>
Definition at line *54* of file *node_handle.h*.

* Constructor & Destructor Documentation
** template<typename _Val , typename _NodeAlloc > constexpr
*std::_Node_handle_common*< _Val, _NodeAlloc >::*_Node_handle_common*
()= [inline]=, = [constexpr]=, = [protected]=, = [noexcept]=
Definition at line *68* of file *node_handle.h*.

** template<typename _Val , typename _NodeAlloc >
*std::_Node_handle_common*< _Val, _NodeAlloc >::~*_Node_handle_common*
()= [inline]=, = [protected]=
Definition at line *70* of file *node_handle.h*.

** template<typename _Val , typename _NodeAlloc >
*std::_Node_handle_common*< _Val, _NodeAlloc >::*_Node_handle_common*
(*_Node_handle_common*< _Val, _NodeAlloc > && __nh)= [inline]=,
= [protected]=, = [noexcept]=
Definition at line *76* of file *node_handle.h*.

** template<typename _Val , typename _NodeAlloc >
*std::_Node_handle_common*< _Val, _NodeAlloc >::*_Node_handle_common*
(typename *_AllocTraits::pointer* __ptr, const _NodeAlloc &
__alloc)= [inline]=, = [protected]=
Definition at line *106* of file *node_handle.h*.

* Member Function Documentation
** template<typename _Val , typename _NodeAlloc > void
*std::_Node_handle_common*< _Val, _NodeAlloc >::_M_swap
(*_Node_handle_common*< _Val, _NodeAlloc > & __nh)= [inline]=,
= [protected]=, = [noexcept]=
Definition at line *114* of file *node_handle.h*.

** template<typename _Val , typename _NodeAlloc > bool
*std::_Node_handle_common*< _Val, _NodeAlloc >::empty ()
const= [inline]=, = [noexcept]=
Definition at line *65* of file *node_handle.h*.

** template<typename _Val , typename _NodeAlloc > allocator_type
*std::_Node_handle_common*< _Val, _NodeAlloc >::get_allocator ()
const= [inline]=, = [noexcept]=
Definition at line *57* of file *node_handle.h*.

** template<typename _Val , typename _NodeAlloc >
*std::_Node_handle_common*< _Val, _NodeAlloc >::operator bool ()
const= [inline]=, = [explicit]=, = [noexcept]=
Definition at line *63* of file *node_handle.h*.

** template<typename _Val , typename _NodeAlloc > *_Node_handle_common*
& *std::_Node_handle_common*< _Val, _NodeAlloc >::operator=
(*_Node_handle_common*< _Val, _NodeAlloc > && __nh)= [inline]=,
= [protected]=, = [noexcept]=
Definition at line *84* of file *node_handle.h*.

* Friends And Related Function Documentation
** template<typename _Val , typename _NodeAlloc > template<typename
_Key2 , typename _Value2 , typename _KeyOfValue , typename _Compare ,
typename _ValueAlloc > friend class _Rb_tree= [friend]=
Definition at line *216* of file *node_handle.h*.

* Member Data Documentation
** template<typename _Val , typename _NodeAlloc >
*_AllocTraits::pointer* *std::_Node_handle_common*< _Val, _NodeAlloc
>::_M_ptr= [protected]=
Definition at line *156* of file *node_handle.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
