#+TITLE: Manpages - SSL_get_verify_result.3ssl
#+DESCRIPTION: Linux manpage for SSL_get_verify_result.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_get_verify_result - get result of peer certificate verification

* SYNOPSIS
#include <openssl/ssl.h> long SSL_get_verify_result(const SSL *ssl);

* DESCRIPTION
*SSL_get_verify_result()* returns the result of the verification of the
X509 certificate presented by the peer, if any.

* NOTES
*SSL_get_verify_result()* can only return one error code while the
verification of a certificate can fail because of many reasons at the
same time. Only the last verification error that occurred during the
processing is available from *SSL_get_verify_result()*.

The verification result is part of the established session and is
restored when a session is reused.

* BUGS
If no peer certificate was presented, the returned result code is
X509_V_OK. This is because no verification error occurred, it does
however not indicate success. *SSL_get_verify_result()* is only useful
in connection with *SSL_get_peer_certificate* (3).

* RETURN VALUES
The following return values can currently occur:

- X509_V_OK :: The verification succeeded or no peer certificate was
  presented.

- Any other value :: Documented in *verify* (1).

* SEE ALSO
*ssl* (7), *SSL_set_verify_result* (3), *SSL_get_peer_certificate* (3),
*verify* (1)

* COPYRIGHT
Copyright 2000-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
