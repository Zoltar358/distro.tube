#+TITLE: Manpages - libssh2_sftp_get_channel.3
#+DESCRIPTION: Linux manpage for libssh2_sftp_get_channel.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_sftp_get_channel - return the channel of sftp

* SYNOPSIS
#+begin_example
  #include <libssh2.h>
  #include <libssh2_sftp.h>
#+end_example

LIBSSH2_CHANNEL *libssh2_sftp_get_channel(LIBSSH2_SFTP *sftp);

* DESCRIPTION
/sftp/ - SFTP instance as returned by *libssh2_sftp_init(3)*

Return the channel of the given sftp handle.

* RETURN VALUE
The channel of the SFTP instance or NULL if something was wrong.

* AVAILABILITY
Added in 1.4.0

* SEE ALSO
*libssh2_sftp_init(3)*
