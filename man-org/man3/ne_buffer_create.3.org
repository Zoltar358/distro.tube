#+TITLE: Manpages - ne_buffer_create.3
#+DESCRIPTION: Linux manpage for ne_buffer_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_buffer_create, ne_buffer_ncreate - create a string buffer

* SYNOPSIS
#+begin_example
  #include <ne_alloc.h>
#+end_example

*ne_buffer *ne_buffer_create(void);*

*ne_buffer *ne_buffer_ncreate(size_t */size/*);*

* DESCRIPTION
*ne_buffer_create* creates a new buffer object, with an
implementation-defined initial size. *ne_buffer_ncreate* creates an
*ne_buffer* where the minimum initial size is given in the /size/
parameter. The buffer created will contain the empty string ("").

* RETURN VALUE
Both functions return a pointer to a new buffer object, and never NULL.

* SEE ALSO
ne_buffer

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
