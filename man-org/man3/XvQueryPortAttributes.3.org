#+TITLE: Manpages - XvQueryPortAttributes.3
#+DESCRIPTION: Linux manpage for XvQueryPortAttributes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XvQueryPortAttributes - return list of attributes of a video port

* SYNOPSIS
*#include <X11/extensions/Xvlib.h>*

#+begin_example
  XvAttribute* XvQueryPortAttributes(Display *dpy,
   XvPortID port, int *p_num_attributes);
#+end_example

* ARGUMENTS
- dpy :: Specifies the connection to the X server.

- port :: Specifies the port whose adaptor is to be queried for its list
  of attributes.

- p_num_attributes :: A pointer to where the number of attributes
  returned in the array is written.

* DESCRIPTION
*XvQueryPortAttributes*(3)*returns*the*number*of*attributes* and an
array of XvAttributes valid for the given port. The array may be freed
with *XFree*(3).

* RETURN VALUES
XvAttribute has the following structure:

#+begin_example

      typedef struct {
        int flags;
        int min_value;
        int max_value;
        char *name;
      } XvAttribute;
#+end_example

- flags :: May be XvGettable or XvSettable or both OR'd together
  indicating the particular attribute is readable, writeable or readable
  and writeable.

- min_value :: The minimum attribute values which are valid for the
  driver.

-  :: The maximum attribute values which are valid for the driver.

- name :: A string describing the name of the attribute that may be used
  to retrieve the Atom for the particular attribute.

* DIAGNOSTICS
- [XvBadPort] :: Generated if the requested port does not exist.

* SEE ALSO
*XvGetPortAttribute*(3), *XvSetPortAttribute*(3)
