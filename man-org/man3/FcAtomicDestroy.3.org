#+TITLE: Manpages - FcAtomicDestroy.3
#+DESCRIPTION: Linux manpage for FcAtomicDestroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcAtomicDestroy - destroy an FcAtomic object

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcAtomicDestroy (FcAtomic */atomic/*);*

* DESCRIPTION
Destroys /atomic/.
