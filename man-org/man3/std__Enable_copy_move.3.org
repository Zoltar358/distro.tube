#+TITLE: Manpages - std__Enable_copy_move.3
#+DESCRIPTION: Linux manpage for std__Enable_copy_move.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Enable_copy_move< _Copy, _CopyAssignment, _Move, _MoveAssignment,
_Tag > - A mixin helper to conditionally enable or disable the copy/move
special members.

* SYNOPSIS
\\

=#include <enable_special_members.h>=

* Detailed Description
** "template<bool _Copy, bool _CopyAssignment, bool _Move, bool
_MoveAssignment, typename _Tag = void>
\\
struct std::_Enable_copy_move< _Copy, _CopyAssignment, _Move,
_MoveAssignment, _Tag >"A mixin helper to conditionally enable or
disable the copy/move special members.

*See also*

#+begin_quote
  _Enable_special_members
#+end_quote

Definition at line *86* of file *enable_special_members.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
