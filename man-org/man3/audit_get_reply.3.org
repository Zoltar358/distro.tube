#+TITLE: Manpages - audit_get_reply.3
#+DESCRIPTION: Linux manpage for audit_get_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_get_reply - Get the audit system's reply

* SYNOPSIS
*#include <libaudit.h>*

int audit_get_reply(int fd, struct audit_reply *rep, reply_t block, int
peek);

* DESCRIPTION
This function gets the next data packet sent on the audit netlink
socket. This function is usually called after sending a command to the
audit system. fd should be an open file descriptor returned by
audit_open. rep should be a data structure to put the reply in. block is
of type reply_t which is either: GET_REPLY_BLOCKING and
GET_REPLY_NONBLOCKING. peek, if non-zero, gets the data without
dequeueing it from the netlink socket.

* RETURN VALUE
This function returns -errno on error, 0 if error response received, and
positive value on success.

* SEE ALSO
*audit_open*(3).

* AUTHOR
Steve Grubb
