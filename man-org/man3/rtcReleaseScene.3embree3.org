#+TITLE: Manpages - rtcReleaseScene.3embree3
#+DESCRIPTION: Linux manpage for rtcReleaseScene.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcReleaseScene - decrements the scene reference count
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcReleaseScene(RTCScene scene);
#+end_example

** DESCRIPTION
Scene objects are reference counted. The =rtcReleaseScene= function
decrements the reference count of the passed scene object (=scene=
argument). When the reference count falls to 0, the scene gets
destroyed.

The scene holds a reference to all attached geometries, thus if the
scene gets destroyed, all geometries get detached and their reference
count decremented.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewScene], [rtcRetainScene]
