#+TITLE: Manpages - SDL_LockSurface.3
#+DESCRIPTION: Linux manpage for SDL_LockSurface.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_LockSurface - Lock a surface for directly access.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_LockSurface*(*SDL_Surface *surface*);

* DESCRIPTION
*SDL_LockSurface* sets up a surface for directly accessing the pixels.
Between calls to *SDL_LockSurface* and *SDL_UnlockSurface*, you can
write to and read from *surface->pixels*, using the pixel format stored
in *surface->format*. Once you are done accessing the surface, you
should use *SDL_UnlockSurface* to release it.

Not all surfaces require locking. If *SDL_MUSTLOCK*(*surface*) evaluates
to *0*, then you can read and write to the surface at any time, and the
pixel format of the surface will not change.

No operating system or library calls should be made between lock/unlock
pairs, as critical system locks may be held during this time.

It should be noted, that since SDL 1.1.8 surface locks are recursive.
This means that you can lock a surface multiple times, but each lock
must have a match unlock.

#+begin_example
      .
      .
      SDL_LockSurface( surface );
      .
      /* Surface is locked */
      /* Direct pixel access on surface here */
      .
      SDL_LockSurface( surface );
      .
      /* More direct pixel access on surface */
      .
      SDL_UnlockSurface( surface );
      /* Surface is still locked */
      /* Note: Is versions < 1.1.8, the surface would have been */
      /* no longer locked at this stage                         */
      .
      SDL_UnlockSurface( surface );
      /* Surface is now unlocked */
      .
      .
#+end_example

* RETURN VALUE
*SDL_LockSurface* returns *0*, or *-1* if the surface couldn't be
locked.

* SEE ALSO
*SDL_UnlockSurface*
