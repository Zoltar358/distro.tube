#+TITLE: Manpages - XtVaGetValues.3
#+DESCRIPTION: Linux manpage for XtVaGetValues.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtVaGetValues.3 is found in manpage for: [[../man3/XtSetValues.3][man3/XtSetValues.3]]