#+TITLE: Manpages - sio_open.3
#+DESCRIPTION: Linux manpage for sio_open.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

library allows user processes to access

hardware and the

audio server in a uniform way.

First the application must call the

function to obtain a handle to the device; later it will be passed as
the

argument of most other functions. The

parameter gives the device string discussed in

In most cases it should be set to

to allow the user to select it using the

environment variable.

The following values of the

parameter are supported:

Play-only mode: data written will be played by the device.

Record-only mode: samples are recorded by the device and must be read.

The device plays and records synchronously; this means that the n-th
recorded sample was physically sampled exactly when the n-th played
sample was actually played.

If the

argument is true (i.e. non-zero), then the

and

functions (see below) will be non-blocking.

The

function stops the device as if

is called and frees the handle. Thus, no samples submitted with

are discarded.

Audio samples are interleaved. A frame consists of one sample for each
channel. For example, a 16-bit stereo encoding has two samples per frame
and, two bytes per sample (thus 4 bytes per frame).

The set of parameters of the device that can be controlled is given by
the following structure:

struct sio_par { unsigned int bits; /* bits per sample */ unsigned int
bps; /* bytes per sample */ unsigned int sig; /* 1 = signed, 0 =
unsigned int */ unsigned int le; /* 1 = LE, 0 = BE byte order */
unsigned int msb; /* 1 = MSB, 0 = LSB aligned */ unsigned int rchan; /*
number channels for recording */ unsigned int pchan; /* number channels
for playback */ unsigned int rate; /* frames per second */ unsigned int
appbufsz; /* minimum buffer size without xruns */ unsigned int bufsz; /*
end-to-end buffer size (read-only) */ unsigned int round; /* optimal
buffer size divisor */ #define SIO_IGNORE 0 /* pause during xrun */
#define SIO_SYNC 1 /* resync after xrun */ #define SIO_ERROR 2 /*
terminate on xrun */ unsigned int xrun; /* what to do on
overrun/underrun */ };

The parameters are as follows:

Number of bits per sample: must be between 1 and 32.

Bytes per samples; if specified, it must be large enough to hold all
bits. By default it's set to the smallest power of two large enough to
hold

If set (i.e. non-zero) then the samples are signed, else unsigned.

If set, then the byte order is little endian, else big endian; it's
meaningful only if

If set, then the

are aligned in the packet to the most significant bit (i.e. lower bits
are padded), else to the least significant bit (i.e. higher bits are
padded); it's meaningful only if

The number of recorded channels; meaningful only if

mode was selected.

The number of played channels; meaningful only if

mode was selected.

The sampling frequency in Hz.

The maximum number of frames that may be buffered. This parameter takes
into account any buffers, and can be used for latency calculations. It
is read-only.

Size of the buffer in frames the application must maintain non-empty (on
the play end) or non-full (on the record end) by calling

or

fast enough to avoid overrun or underrun conditions. The audio subsystem
may use additional buffering, thus this parameter cannot be used for
latency calculations.

Optimal number of frames that the application buffers should be a
multiple of, to get best performance. Applications can use this
parameter to round their block size.

The action when the client doesn't accept recorded data or doesn't
provide data to play fast enough; it can be set to one of the

or

constants.

The following approach is recommended to negotiate device parameters:

Initialize a

structure using

and fill it with the desired parameters. Then call

to request the device to use them. Parameters left unset in the

structure will be set to device-specific defaults.

Call

to retrieve the actual parameters of the device and check that they are
usable. If they are not, then fail or set up a conversion layer.
Sometimes the rate set can be slightly different to what was requested.
A difference of about 0.5% is not audible and should be ignored.

Parameters cannot be changed after

has been called,

must be called before parameters can be changed.

If the device is exposed by the

server, which is the default configuration, a transparent emulation
layer will automatically be set up, and in this case any combination of
rate, encoding and numbers of channels is supported.

To ease filling the

structure, the following macros can be used:

Return the smallest value for

that is a power of two and that is large enough to hold

Can be used to set the

parameter when native byte order is required. It is 1 if the native byte
order is little endian or 0 otherwise.

There's no way to get an exhaustive list of all parameter combinations
the device supports. Applications that need to have a set of working
parameter combinations in advance can use the

function. However, for most new applications it's generally not
recommended to use

Instead, follow the recommendations for negotiating device parameters
(see above).

The

structure contains the list of parameter configurations. Each
configuration contains multiple parameter sets. The application must
examine all configurations, and choose its parameter set from

of the configurations. Parameters of different configurations

usable together.

struct sio_cap { struct sio_enc { /* allowed encodings */ unsigned int
bits; unsigned int bps; unsigned int sig; unsigned int le; unsigned int
msb; } enc[SIO_NENC]; unsigned int rchan[SIO_NCHAN]; /* allowed rchans
*/ unsigned int pchan[SIO_NCHAN]; /* allowed pchans */ unsigned int
rate[SIO_NRATE]; /* allowed rates */ unsigned int nconf; /* num. of
confs[] */ struct sio_conf { unsigned int enc; /* bitmask of enc[]
indexes */ unsigned int rchan; /* bitmask of rchan[] indexes */ unsigned
int pchan; /* bitmask of pchan[] indexes */ unsigned int rate; /*
bitmask of rate[] indexes */ } confs[SIO_NCONF]; };

The parameters are as follows:

Array of supported encodings. The tuple of

and

parameters are usable in the corresponding parameters of the

structure.

Array of supported channel numbers for recording usable in the

structure.

Array of supported channel numbers for playback usable in the

structure.

Array of supported sample rates usable in the

structure.

Number of different configurations available, i.e. number of filled
elements of the

array.

Array of available configurations. Each configuration contains bitmasks
indicating which elements of the above parameter arrays are valid for
the given configuration. For instance, if the second bit of

is set, in the

structure, then the second element of the

array of the

structure is valid for this configuration. As such, when reading the
array elements in the

structure, the corresponding

bitmasks should always be used.

The

function prepares the device to start. Once the play buffer is full,
i.e.

samples are queued with

playback starts automatically. If record-only mode is selected, then

starts recording immediately. In full-duplex mode, playback and
recording will start synchronously as soon as the play buffer is full.

The

function puts the audio subsystem in the same state as before

is called. It stops recording, drains the play buffer and then stops
playback. If samples to play are queued but playback hasn't started yet
then playback is forced immediately; playback will actually stop once
the buffer is drained. In no case are samples in the play buffer
discarded.

When record mode is selected, the

function must be called to retrieve recorded data; it must be called
often enough to ensure that internal buffers will not overrun. It will
store at most

bytes at the

location and return the number of bytes stored. Unless the

flag is set, it will block until data becomes available and will return
zero only on error.

Similarly, when play mode is selected, the

function must be called to provide data to play. Unless the

is set,

will block until the requested amount of data is written.

If the

is set on

then the

and

functions will never block; if no data is available, they will return
zero immediately.

The

system call can be used to check if data can be read from or written to
the device. The

function fills the array

of

structures, used by

with

the latter is a bit-mask of

and

constants; refer to

for more details. The

function returns the bit-mask set by

in the

array of

structures. If

is set, recorded samples are available in the device buffer and can be
read with

If

is set, space is available in the device buffer and new samples to play
can be submitted with

may be set if an error occurs, even if it is not selected with

The size of the

array, which the caller must pre-allocate, is provided by the

function.

In order to perform actions at precise positions of the audio stream,
such as displaying video in sync with the audio stream, the application
must be notified in real-time of the exact position in the stream the
hardware is processing.

The

function can be used to register the

callback function called at regular time intervals. The

argument contains the number of frames the hardware played and/or
recorded since the last call of

It is called by

and

When the first sample is played and/or recorded, right after the device
starts, the callback is invoked with a zero

argument. The value of the

pointer is passed to the callback and can contain anything.

If desired, the application can maintain the current position by
starting from zero (when

is called) and adding to the current position

every time

is called.

The playback latency is the delay it will take for the frame just
written to become audible, expressed in number of frames. The exact
playback latency can be obtained by subtracting the current position
from the number of frames written. Once playback is actually started
(first sample audible) the latency will never exceed the

parameter (see the sections above). There's a phase during which

only queues data; once there's enough data, actual playback starts.
During this phase talking about latency is meaningless.

In any cases, at most

frames are buffered. This value takes into account all buffers. The
number of frames stored is equal to the number of frames written minus
the current position.

The recording latency is obtained similarly, by subtracting the number
of frames read from the current position.

Note that

might block even if there is buffer space left; using the buffer usage
to guess if

would block is false and leads to unreliable programs -- consider using

for this.

When the application cannot accept recorded data fast enough, the record
buffer (of size

might overrun; in this case recorded data is lost. Similarly if the
application cannot provide data to play fast enough, the play buffer
underruns and silence is played instead. Depending on the

parameter of the

structure, the audio subsystem will behave as follows:

The device pauses during overruns and underruns, thus the current
position (obtained through

stops being incremented. Once the overrun and/or underrun condition is
gone, the device resumes; play and record are always kept in sync. With
this mode, the application cannot notice underruns and/or overruns and
shouldn't care about them.

This mode is the default. It's suitable for applications, like audio
players and telephony, where time is not important and overruns or
underruns are not short.

If the play buffer underruns, then silence is played, but in order to
reach the right position in time, the same amount of written samples
will be discarded once the application is unblocked. Similarly, if the
record buffer overruns, then samples are discarded, but the same amount
of silence will be returned later. The current position (obtained
through

is still incremented. When the play buffer underruns the play latency
might become negative; when the record buffer overruns, the record
latency might become larger than

This mode is suitable for applications, like music production, where
time is important and where underruns or overruns are short and rare.

With this mode, on the first play buffer underrun or record buffer
overrun, playback and/or recording is terminated and no other function
than

will succeed.

This mode is mostly useful for testing.

The

function can be used to set playback attenuation. The

parameter takes a value between 0 (maximum attenuation) and

(no attenuation). It specifies the weight the audio subsystem will give
to this stream. It is not meant to control hardware parameters like
speaker gain; the

interface should be used for that purpose instead.

An application can use the

function to register a callback function that will be called each time
the volume is changed, including when

is used. The callback is always invoked when

is called in order to provide the initial volume. An application can
safely assume that once

has returned a non-zero value, the callback has been invoked and thus
the current volume is available. If there's no volume setting available,

returns 0 and the callback is never invoked and calls to

are ignored.

The

function can be called with a

argument to check whether a volume knob is available.

Errors related to the audio subsystem (like hardware errors, dropped
connections) and programming errors (e.g. call to

on a play-only stream) are considered fatal. Once an error occurs, all
functions taking a

argument, except

and

stop working (i.e. always return 0). The

function can be used at any stage.

The

function returns the newly created handle on success or

on failure.

The

and

functions return 1 on success and 0 on failure.

The

function returns the number of

structures filled. The

function returns the number of

structures the caller must preallocate in order to be sure that

will never overrun.

The

and

functions return the number of bytes transferred.

The

function returns 0 if there's no pending error, and a non-zero value if
there's an error.

Device to use if

is called with

as the

argument.

The debug level: may be a value between 0 and 2.

These functions first appeared in

The

driver doesn't drain playback buffers, thus if sndio is used to directly
access an

device, the

function will stop playback immediately.

If the application doesn't consume recorded data fast enough then

from the

server are delayed and consequently

callback or volume changes may be delayed.

The

and

functions may block for a very short period of time, thus they should be
avoided in code sections where blocking is not desirable.
