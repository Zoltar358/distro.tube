#+TITLE: Manpages - std___detail__Hash_node_base.3
#+DESCRIPTION: Linux manpage for std___detail__Hash_node_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Hash_node_base

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherited by *std::__detail::_Hash_node< _Value, _Cache_hash_code >*.

** Public Member Functions
*_Hash_node_base* (*_Hash_node_base* *__next) noexcept\\

** Public Attributes
*_Hash_node_base* * *_M_nxt*\\

* Detailed Description
struct _Hash_node_base

Nodes, used to wrap elements stored in the hash table. A policy template
parameter of class template _Hashtable controls whether nodes also store
a hash code. In some cases (e.g. strings) this may be a performance win.

Definition at line *214* of file *hashtable_policy.h*.

* Constructor & Destructor Documentation
** std::__detail::_Hash_node_base::_Hash_node_base ()= [inline]=,
= [noexcept]=
Definition at line *218* of file *hashtable_policy.h*.

** std::__detail::_Hash_node_base::_Hash_node_base (*_Hash_node_base* *
__next)= [inline]=, = [noexcept]=
Definition at line *220* of file *hashtable_policy.h*.

* Member Data Documentation
** *_Hash_node_base** std::__detail::_Hash_node_base::_M_nxt
Definition at line *216* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
