#+TITLE: Manpages - auparse_get_timestamp.3
#+DESCRIPTION: Linux manpage for auparse_get_timestamp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_timestamp - access timestamp of the event

* SYNOPSIS
*#include <auparse.h>*

const au_event_t *auparse_get_timestamp(auparse_state_t *au);

* DESCRIPTION
auparse_get_timestamp provides an accessor function for the event's
timestamp data structure. The data structure is as follows:

#+begin_example
  typedef struct
  {
          time_t sec;             // Event seconds
          unsigned int milli;     // millisecond of the timestamp
          unsigned long serial;   // Serial number of the event
          const char *host;       // Machine's node name
  } au_event_t;
#+end_example

* RETURN VALUE
Returns NULL if an error occurs; otherwise, a valid pointer to the data.

* SEE ALSO
*auparse_get_time*(3), *auparse_get_milli*(3), *auparse_get_serial*(3),
*auparse_get_node*(3), *auparse_timestamp_compare*(3).

* AUTHOR
Steve Grubb
