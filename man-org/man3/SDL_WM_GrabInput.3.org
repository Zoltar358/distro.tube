#+TITLE: Manpages - SDL_WM_GrabInput.3
#+DESCRIPTION: Linux manpage for SDL_WM_GrabInput.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WM_GrabInput - Grabs mouse and keyboard input.

* SYNOPSIS
*#include "SDL.h"*

*SDL_GrabMode SDL_WM_GrabInput*(*SDL_GrabMode mode*);

* DESCRIPTION
Grabbing means that the mouse is confined to the application window, and
nearly all keyboard input is passed directly to the application, and not
interpreted by a window manager, if any.

When *mode* is *SDL_GRAB_QUERY* the grab mode is not changed, but the
current grab mode is returned.

#+begin_example
  typedef enum {
    SDL_GRAB_QUERY,
    SDL_GRAB_OFF,
    SDL_GRAB_ON
  } SDL_GrabMode;
#+end_example

* RETURN VALUE
The current/new *SDL_GrabMode*.
