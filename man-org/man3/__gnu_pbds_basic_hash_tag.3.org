#+TITLE: Manpages - __gnu_pbds_basic_hash_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_basic_hash_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::basic_hash_tag - Basic hash structure.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::associative_tag*.

Inherited by *__gnu_pbds::cc_hash_tag*, and *__gnu_pbds::gp_hash_tag*.

* Detailed Description
Basic hash structure.

Definition at line *138* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
