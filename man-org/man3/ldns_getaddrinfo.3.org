#+TITLE: Manpages - ldns_getaddrinfo.3
#+DESCRIPTION: Linux manpage for ldns_getaddrinfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_getaddrinfo - mimic libc getaddrinfo

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

uint16_t ldns_getaddrinfo(ldns_resolver *res, const ldns_rdf *node,
ldns_rr_class c, ldns_rr_list **list);

* DESCRIPTION
/ldns_getaddrinfo/() This function is a wrapper function for
ldns_get_rr_list_name_by_addr and ldns_get_rr_list_addr_by_name. It's
name is from the getaddrinfo() library call. It tries to mimic that
call, but without the lowlevel stuff. .br *res*: The resolver. If this
value is NULL then a resolver will be created by ldns_getaddrinfo. .br
*node*: the name or ip address to look up .br *c*: the class to look in
.br *list*: put the found RR's in this list .br Returns the number of RR
found.

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
