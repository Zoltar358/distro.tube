#+TITLE: Manpages - sd_machine_get_class.3
#+DESCRIPTION: Linux manpage for sd_machine_get_class.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_machine_get_class, sd_machine_get_ifindices - Determine the class and
network interface indices of a locally running virtual machine or
container

* SYNOPSIS
#+begin_example
  #include <systemd/sd-login.h>
#+end_example

*int sd_machine_get_class(const char* */machine/*, char ***/class/*);*

*int sd_machine_get_ifindices(const char* */machine/*, int
***/ret_ifindices/*);*

* DESCRIPTION
*sd_machine_get_class()* may be used to determine the class of a locally
running virtual machine or container that is registered with
*systemd-machined.service*(8). The string returned is either "vm" or
"container". The returned string needs to be freed with the libc
*free*(3) call after use.

*sd_machine_get_ifindices()* may be used to determine the numeric
indices of the network interfaces on the host that are pointing towards
the specified locally running virtual machine or container. The vm or
container must be registered with *systemd-machined.service*(8). The
output parameter /ret_ifindices/ may be passed as *NULL* when the output
value is not needed. The returned array needs to be freed with the libc
*free*(3) call after use.

* RETURN VALUE
On success, these functions return a non-negative integer.
*sd_machine_get_ifindices()* returns the number of the relevant network
interfaces. On failure, these calls return a negative errno-style error
code.

** Errors
Returned errors may indicate the following problems:

*-ENXIO*

#+begin_quote
  The specified machine does not exist or is currently not running.
#+end_quote

*-EINVAL*

#+begin_quote
  An input parameter was invalid (out of range, or *NULL*, where that is
  not accepted).
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-login*(3), *systemd-machined.service*(8),
*sd_pid_get_machine_name*(3)
