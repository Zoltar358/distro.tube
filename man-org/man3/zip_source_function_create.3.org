#+TITLE: Manpages - zip_source_function_create.3
#+DESCRIPTION: Linux manpage for zip_source_function_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The functions

and

creates a zip source from the user-provided function

which must be of the following type:

or

are used for reporting errors and can be

When called by the library, the first argument is the

argument supplied to the function. The next two arguments are a buffer

of size

when data is passed in or expected to be returned, or else

and 0. The last argument,

specifies which action the function should perform.

Depending on the uses, there are three useful sets of commands to be
supported by a

Providing streamed data (for file data added to archives). Must support

and

Same as previous, but from a source allowing reading from arbitrary
offsets (also for read-only zip archive). Must additionally support

and

Same as previous, but additionally allowing writing (also for writable
zip archives). Must additionally support

and

Return 1 if an empty source should be accepted as a valid zip archive.
This is the default if this command is not supported by a source. File
system backed sources should return 0.

Prepare the source for writing. Use this to create any temporary
file(s).

Prepare the source for writing, keeping the first

bytes of the original file. Only implement this command if it is more
efficient than copying the data, and if it does not destructively
overwrite the original file (you still have to be able to execute

The next write should happen at byte

Reading is done.

Finish writing to the source. Replace the original data with the newly
written data. Clean up temporary files or internal buffers. Subsequently
opening and reading from the source should return the newly written
data.

Get error information.

points to an array of two ints, which should be filled with the libzip
error code and the corresponding system error code for the error that
occurred. See

for details on the error codes. If the source stores error information
in a zip_error_t, use

and return its return value. Otherwise, return 2 * sizeof(int).

Clean up and free all resources, including

The callback function will not be called again.

Provide information about various data. Then the data should be put in
the appropriate entry in the passed

argument, and the appropriate

value must be or'ed into the

member to denote that the corresponding data has been provided. A

structure can be initialized using

If a file is a plaintext file in ASCII. Can be used by extraction tools
to automatically convert line endings (part of the interal file
attributes). Member

flag

The general purpose bit flag in the zip in the local and central
directory headers contain information about the compression method.
Member

and

to denote which members have been set; flag

The external file attributes (usually operating system-specific). Member

flag

A minimum version needed required to unpack this entry (in the usual
"major * 10 + minor" format). Member

flag

One of the operating systems as defined by the

variables (see

This value affects the interpretation of the external file attributes.
Member

flag

Prepare for reading.

Read data into the buffer

of size

Return the number of bytes placed into

on success, and zero for end-of-file.

Remove the underlying file. This is called if a zip archive is empty
when closed.

Abort writing to the source. Discard written data. Clean up temporary
files or internal buffers. Subsequently opening and reading from the
source should return the original data.

Specify position to read next byte from, like

Use

to decode the arguments into the following struct:

struct zip_source_args_seek { zip_int64_t offset; int whence; };

If the size of the source's data is known, use

to validate the arguments and compute the new offset.

Specify position to write next byte to, like

See

for details.

Get meta information for the input data.

points to an allocated

which should be initialized using

and then filled in.

For uncompressed, unencrypted data, all information is optional.
However, fill in as much information as is readily available.

If the data is compressed,

and

must be filled in.

If the data is encrypted,

and

must be filled in.

Information only available after the source has been read (e.g., size)
can be omitted in an earlier call.

may be called with this argument even after being called with

Return sizeof(struct zip_stat) on success.

Return bitmap specifying which commands are supported. Use

If this command is not implemented, the source is assumed to be a read
source without seek support.

Return the current read offset in the source, like

Return the current write offset in the source, like

Write data to the source. Return number of bytes written.

Commands should return -1 on error.

will be called to retrieve the error code. On success, commands return
0, unless specified otherwise in the description above.

The library will always issue

before issuing

or

When it no longer wishes to read from this source, it will issue

If the library wishes to read the data again, it will issue

a second time. If the function is unable to provide the data again, it
should return -1.

or

will be called before

or

When writing is complete, either

or

will be called.

and

can be issued at any time.

will only be issued in response to the function returning -1.

will be the last command issued; if

was called and succeeded,

will be called before

and similarly for

or

and

or

Upon successful completion, the created source is returned. Otherwise,

is returned and the error code in

or

is set to indicate the error (unless it is

fails if:

Required memory could not be allocated.

and

were added in libzip 1.0.

and
