#+TITLE: Manpages - dispatch_once.3
#+DESCRIPTION: Linux manpage for dispatch_once.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

function provides a simple and efficient mechanism to run an initializer
exactly once, similar to

Well designed code hides the use of lazy initialization. For example:

FILE *getlogfile(void) { static dispatch_once_t pred; static FILE
*logfile;

dispatch_once(&pred, ^{ logfile = fopen(MY_LOG_FILE, "a"); });

return logfile; }

The

function is a wrapper around
