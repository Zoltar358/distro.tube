#+TITLE: Manpages - zmq_msg_init_data.3
#+DESCRIPTION: Linux manpage for zmq_msg_init_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_msg_init_data - initialise 0MQ message from a supplied buffer

* SYNOPSIS
*typedef void (zmq_free_fn) (void */*data/*, void */*hint/*);*

*int zmq_msg_init_data (zmq_msg_t */*msg/*, void */*data/*, size_t
*/size/*, zmq_free_fn */*ffn/*, void */*hint/*);*

* DESCRIPTION
The /zmq_msg_init_data()/ function shall initialise the message object
referenced by /msg/ to represent the content referenced by the buffer
located at address /data/, /size/ bytes long. No copy of /data/ shall be
performed and 0MQ shall take ownership of the supplied buffer.

If provided, the deallocation function /ffn/ shall be called once the
data buffer is no longer required by 0MQ, with the /data/ and /hint/
arguments supplied to /zmq_msg_init_data()/.

#+begin_quote
  \\

  *Caution*

  \\

  Never access /zmq_msg_t/ members directly, instead always use the
  /zmq_msg/ family of functions.
#+end_quote

#+begin_quote
  \\

  *Caution*

  \\

  The deallocation function /ffn/ needs to be thread-safe, since it will
  be called from an arbitrary thread.
#+end_quote

#+begin_quote
  \\

  *Caution*

  \\

  If the deallocation function is not provided, the allocated memory
  will not be freed, and this may cause a memory leak.
#+end_quote

#+begin_quote
  \\

  *Caution*

  \\

  The functions /zmq_msg_init()/, /zmq_msg_init_data()/,
  /zmq_msg_init_size()/ and /zmq_msg_init_buffer()/ are mutually
  exclusive. Never initialise the same /zmq_msg_t/ twice.
#+end_quote

* RETURN VALUE
The /zmq_msg_init_data()/ function shall return zero if successful.
Otherwise it shall return -1 and set /errno/ to one of the values
defined below.

* ERRORS
*ENOMEM*

#+begin_quote
  Insufficient storage space is available.
#+end_quote

* EXAMPLE
*Initialising a message from a supplied buffer*.

#+begin_quote
  #+begin_example
    void my_free (void *data, void *hint)
    {
        free (data);
    }

        /*  ...  */

    void *data = malloc (6);
    assert (data);
    memcpy (data, "ABCDEF", 6);
    zmq_msg_t msg;
    rc = zmq_msg_init_data (&msg, data, 6, my_free, NULL);
    assert (rc == 0);
  #+end_example
#+end_quote

* SEE ALSO
*zmq_msg_init_size*(3) *zmq_msg_init_buffer*(3) *zmq_msg_init*(3)
*zmq_msg_close*(3) *zmq_msg_data*(3) *zmq_msg_size*(3) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
