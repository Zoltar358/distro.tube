#+TITLE: Manpages - MPI_File_get_position_shared.3
#+DESCRIPTION: Linux manpage for MPI_File_get_position_shared.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_File_get_position_shared* - Returns the current position of the
shared file pointer.

* SYNTAX
#+begin_example
#+end_example

* C Syntax
#include <mpi.h> int MPI_File_get_position_shared(MPI_File /fh/,
MPI_Offset /*offset/)

* Fortran Syntax (see FORTRAN 77 NOTES)
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_FILE_GET_POSITION_SHARED(FH, OFFSET, IERROR)
  	INTEGER	FH, IERROR
  	INTEGER(KIND=MPI_OFFSET_KIND)	OFFSET
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_File_get_position_shared(fh, offset, ierror)
  	TYPE(MPI_File), INTENT(IN) :: fh
  	INTEGER(KIND=MPI_OFFSET_KIND), INTENT(OUT) :: offset
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  MPI::Offset MPI::File::Get_position_shared() const
#+end_example

* INPUT PARAMETER
- fh :: File handle (handle).

* OUTPUT PARAMETERS
- offset :: Offset of the shared file pointer (integer).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
MPI_File_get_position_shared returns, in /offset,/ the current position
of the shared file pointer in /etype/ units relative to the current
displacement and file type.

* FORTRAN 77 NOTES
The MPI standard prescribes portable Fortran syntax for the /OFFSET/
argument only for Fortran 90. Sun FORTRAN 77 users may use the
non-portable syntax

#+begin_example
       INTEGER*MPI_OFFSET_KIND OFFSET
#+end_example

where MPI_ADDRESS_KIND is a constant defined in mpif.h and gives the
length of the declared integer in bytes.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. For MPI I/O function errors, the default error handler is set to
MPI_ERRORS_RETURN. The error handler may be changed with
MPI_File_set_errhandler; the predefined error handler
MPI_ERRORS_ARE_FATAL may be used to make I/O errors fatal. Note that MPI
does not guarantee that an MPI program can continue past an error.
