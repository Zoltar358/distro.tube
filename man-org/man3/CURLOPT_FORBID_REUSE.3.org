#+TITLE: Manpages - CURLOPT_FORBID_REUSE.3
#+DESCRIPTION: Linux manpage for CURLOPT_FORBID_REUSE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_FORBID_REUSE - make connection get closed at once after use

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FORBID_REUSE, long
close);

* DESCRIPTION
Pass a long. Set /close/ to 1 to make libcurl explicitly close the
connection when done with the transfer. Normally, libcurl keeps all
connections alive when done with one transfer in case a succeeding one
follows that can re-use them. This option should be used with caution
and only if you understand what it does as it can seriously impact
performance.

Set to 0 to have libcurl keep the connection open for possible later
re-use (default behavior).

* DEFAULT
0

* PROTOCOLS
Most

* EXAMPLE
#+begin_example
  {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_FORBID_REUSE, 1L);
    curl_easy_perform(curl);

    /* this second transfer may not reuse the same connection */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_FRESH_CONNECT*(3), *CURLOPT_MAXCONNECTS*(3),
*CURLOPT_MAXLIFETIME_CONN*(3),
