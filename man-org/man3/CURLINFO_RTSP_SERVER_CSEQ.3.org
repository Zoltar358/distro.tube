#+TITLE: Manpages - CURLINFO_RTSP_SERVER_CSEQ.3
#+DESCRIPTION: Linux manpage for CURLINFO_RTSP_SERVER_CSEQ.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLINFO_RTSP_SERVER_CSEQ - get the next RTSP server CSeq

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RTSP_SERVER_CSEQ, long
*cseq);

* DESCRIPTION
Pass a pointer to a long to receive the next CSeq that will be expected
by the application.

Llistening for server initiated requests is currently unimplemented!

Applications wishing to resume an RTSP session on another connection
should retrieve this info before closing the active connection.

* PROTOCOLS
RTSP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode res;
    curl_easy_setopt(curl, CURLOPT_URL, "rtsp://rtsp.example.com");
    res = curl_easy_perform(curl);
    if(res == CURLE_OK) {
      long cseq;
      curl_easy_getinfo(curl, CURLINFO_RTSP_SERVER_CSEQ, &cseq);
    }
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.20.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*curl_easy_getinfo*(3), *curl_easy_setopt*(3),
