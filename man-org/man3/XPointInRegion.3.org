#+TITLE: Manpages - XPointInRegion.3
#+DESCRIPTION: Linux manpage for XPointInRegion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XPointInRegion.3 is found in manpage for: [[../man3/XEmptyRegion.3][man3/XEmptyRegion.3]]