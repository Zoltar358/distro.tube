#+TITLE: Manpages - acl_get_file.3
#+DESCRIPTION: Linux manpage for acl_get_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function retrieves the access ACL associated with a file or directory,
or the default ACL associated with a directory. The pathname for the
file or directory is pointed to by the argument

The ACL is placed into working storage and

returns a pointer to that storage.

In order to read an ACL from an object, a process must have read access
to the object's attributes.

The value of the argument

is used to indicate whether the access ACL or the default ACL associated
with

is returned. If

is ACL_TYPE_ACCESS, the access ACL of

is returned. If

is ACL_TYPE_DEFAULT, the default ACL of

is returned. If

is ACL_TYPE_DEFAULT and no default ACL is associated with the directory

then an ACL containing zero ACL entries is returned. If

specifies a type of ACL that cannot be associated with

then the function fails.

This function may cause memory to be allocated. The caller should free
any releasable memory, when the new ACL is no longer required, by
calling

with the

returned by

as an argument.

On success, this function returns a pointer to the working storage. On
error, a value of

is returned, and

is set appropriately.

If any of the following conditions occur, the

function returns a value of

and sets

to the corresponding value:

Search permission is denied for a component of the path prefix or the
object exists and the process does not have appropriate access rights.

Argument

specifies a type of ACL that cannot be associated with

The argument

is not ACL_TYPE_ACCESS or ACL_TYPE_DEFAULT.

The length of the argument

is too long.

The named object does not exist or the argument

points to an empty string.

The ACL working storage requires more memory than is allowed by the
hardware or system-imposed memory management constraints.

A component of the path prefix is not a directory.

The file system on which the file identified by

is located does not support ACLs, or ACLs are disabled.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
