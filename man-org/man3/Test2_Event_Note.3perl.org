#+TITLE: Manpages - Test2_Event_Note.3perl
#+DESCRIPTION: Linux manpage for Test2_Event_Note.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::Event::Note - Note event type

* DESCRIPTION
Notes, typically rendered to STDOUT.

* SYNOPSIS
use Test2::API qw/context/; use Test2::Event::Note; my $ctx = context();
my $event = $ctx->Note($message);

* ACCESSORS
- $note->message :: The message for the note.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
