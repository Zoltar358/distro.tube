#+TITLE: Manpages - FcPatternObjectCount.3
#+DESCRIPTION: Linux manpage for FcPatternObjectCount.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternObjectCount - Returns the number of the object

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

int FcPatternObjectCount (const FcPattern */p/*);*

* DESCRIPTION
Returns the number of the object /p/ has.

* SINCE
version 2.13.1
