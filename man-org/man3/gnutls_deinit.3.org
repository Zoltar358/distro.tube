#+TITLE: Manpages - gnutls_deinit.3
#+DESCRIPTION: Linux manpage for gnutls_deinit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_deinit - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*void gnutls_deinit(gnutls_session_t */session/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

* DESCRIPTION
This function clears all buffers associated with the /session/ . This
function will also remove session data from the session database if the
session was terminated abnormally.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
