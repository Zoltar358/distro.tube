#+TITLE: Manpages - sd_bus_message_set_expect_reply.3
#+DESCRIPTION: Linux manpage for sd_bus_message_set_expect_reply.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_message_set_expect_reply, sd_bus_message_get_expect_reply,
sd_bus_message_set_auto_start, sd_bus_message_get_auto_start,
sd_bus_message_set_allow_interactive_authorization,
sd_bus_message_get_allow_interactive_authorization - Set and query bus
message metadata

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_message_set_expect_reply(sd_bus_message **/message/*, int
*/b/*);*

*int sd_bus_message_get_expect_reply(sd_bus_message **/message/*);*

*int sd_bus_message_set_auto_start(sd_bus_message **/message/*, int
*/b/*);*

*int sd_bus_message_get_auto_start(sd_bus_message **/message/*);*

*int sd_bus_message_set_allow_interactive_authorization(sd_bus_message
**/message/*, int */b/*);*

*int sd_bus_message_get_allow_interactive_authorization(sd_bus_message
**/message/*);*

* DESCRIPTION
*sd_bus_message_set_expect_reply()* sets or clears the
*NO_REPLY_EXPECTED* flag on the message /m/. This flag matters only for
method call messages and is used to specify that no method return or
error reply is expected. It is ignored for other types. Thus, for a
method call message, calling

#+begin_quote
  #+begin_example
    sd_bus_message_set_expect_reply(..., 0)
  #+end_example
#+end_quote

sets the flag and suppresses the reply.

*sd_bus_message_get_expect_reply()* checks if the *NO_REPLY_EXPECTED*
flag is set on the message /m/. It will return positive if it is not
set, and zero if it is.

*sd_bus_message_set_auto_start()* sets or clears the *NO_AUTO_START*
flag on the message /m/. When the flag is set, the bus must not launch
an owner for the destination name in response to this message. Calling

#+begin_quote
  #+begin_example
    sd_bus_message_set_auto_start(..., 0)
  #+end_example
#+end_quote

sets the flag.

*sd_bus_message_get_auto_start()* checks if the *NO_AUTO_START* flag is
set on the message /m/. It will return positive if it is not set, and
zero if it is.

*sd_bus_message_set_allow_interactive_authorization()* sets or clears
the *ALLOW_INTERACTIVE_AUTHORIZATION* flag on the message /m/. Setting
this flag informs the receiver that the caller is prepared to wait for
interactive authorization via polkit or a similar framework. Note that
setting this flag does not guarantee that the receiver will actually
perform interactive authorization. Also, make sure to set a suitable
message timeout when using this flag since interactive authorization
could potentially take a long time as it depends on user input. If /b/
is non-zero, the flag is set.

*sd_bus_message_get_allow_interactive_authorization()* checks if the
*ALLOW_INTERACTIVE_AUTHORIZATION* flag is set on the message /m/. It
will return a positive integer if the flag is set. Otherwise, it returns
zero.

* RETURN VALUE
On success, these functions return a non-negative integer. On failure,
they return a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  The /message/ parameter is *NULL*.
#+end_quote

*-EPERM*

#+begin_quote
  The message /message/ is sealed when trying to set a flag.

  The message /message/ has wrong type.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_set_description*(3)
