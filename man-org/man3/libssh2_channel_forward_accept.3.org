#+TITLE: Manpages - libssh2_channel_forward_accept.3
#+DESCRIPTION: Linux manpage for libssh2_channel_forward_accept.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_forward_accept - accept a queued connection

* SYNOPSIS
#include <libssh2.h>

LIBSSH2_CHANNEL * libssh2_channel_forward_accept(LIBSSH2_LISTENER
*listener);

* DESCRIPTION
/listener/ is a forwarding listener instance as returned by
*libssh2_channel_forward_listen_ex(3)*.

* RETURN VALUE
A newly allocated channel instance or NULL on failure.

* ERRORS
When this function returns NULL use /libssh2_session_last_errno(3)/ to
extract the error code. If that code is /LIBSSH2_ERROR_EAGAIN/, the
session is set to do non-blocking I/O but the call would block.

* SEE ALSO
*libssh2_channel_forward_listen_ex(3)*
