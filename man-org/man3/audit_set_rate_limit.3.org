#+TITLE: Manpages - audit_set_rate_limit.3
#+DESCRIPTION: Linux manpage for audit_set_rate_limit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_set_rate_limit - Set audit rate limit

* SYNOPSIS
*#include <libaudit.h>*

int audit_set_rate_limit(int fd, uint32_t limit);

* DESCRIPTION
audit_set_rate_limit will set the maximum number of messages that the
kernel will send per second. This can be used to throttle the rate if
systems become unresponsive. Of course the trade off is that events will
be dropped. The default value is 0, meaning no limit.

* RETURN VALUE
The return value is <= 0 on error, otherwise it is the netlink sequence
id number. This function can have any error that sendto would encounter.

* SEE ALSO
*audit_open*(3), *auditd*(8).

* AUTHOR
Steve Grubb
