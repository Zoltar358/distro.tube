#+TITLE: Manpages - WWW_AUR_Iterator.3pm
#+DESCRIPTION: Linux manpage for WWW_AUR_Iterator.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
WWW::AUR::Iterator - An iterator for looping through all AUR packages.

* SYNOPSIS
my $aurobj = WWW:AUR->new(); my $iter = $aurobj->iter(); # or without
WWW::AUR: my $iter = WWW::AUR::Iterator->new(); while ( my $pkg =
$iter->next_obj ) { print $pkg->name, "\n"; } $iter->reset; while ( my
$p = $iter->next ) { print "$_:$p->{$_}\n" for qw{ id name version cat
desc maint }; print "---\n"; } # Retrieve information on the 12,345th
package, alphabetically. $iter->set_pos(12_345); my $pkginfo =
$iter->next;

* DESCRIPTION
A *WWW::AUR::Iterator* object can be used to iterate through /all/
packages currently listed on the AUR webiste.

* CONSTRUCTOR
$OBJ = WWW::AUR::Iterator->new( %PATH_PARAMS );

- %PATH_PARAMS :: The parameters are the same as the WWW::AUR
  constructor. These are propogated to any WWW::AUR::Package objects
  that are created.

- $OBJ :: A WWW::AUR::Iterator object.

* METHODS
** reset
$OBJ->reset;

The iterator is reset to the beginning of all packages available in the
AUR. This starts the iteration over just like creating a new
/WWW::AUR::Iterator/ object.

** next
\%PKGINFO | undef = $OBJ->next();

This package scrapes the <http://aur.archlinux.org/packages.php> webpage
as if it kept clicking the Next button and recording each package.

- "\%PKGINFO" :: A hash reference containing all the easily available
  information about that particular package. The follow table lists each
  key and its corresponding value.
  |------------+------------------------------------------------| | NAME
  | VALUE |
  |------------+------------------------------------------------| | name
  | The name (pkgname) of the package. | | votes | The number of votes
  for the package. | | desc | The description (pkgdesc) of the package.
  | | cat | The AUR category name assigned to the package. | | maint |
  The name of the maintainer of the package. |
  |------------+------------------------------------------------|

- "undef" :: If we have iterated through all packages, then =undef= is
  returned.

** next_obj
$PKGOBJ | undef = $OBJ->next_obj();

This package is like the next method above but creates a new object as a
convenience. Keep in mind an HTTP request to AUR must be made when
creating a new WWW::AUR::Package object. Use the next method if you can,
it is faster.

- $PKGOBJ :: A WWW::AUR::Package object representing the next package in
  the AUR.

- "undef" :: If we have iterated through all packages, then =undef= is
  returned.

** set_pos
undef = $OBJ->set_pos( $POS );

Set the iterator position to the given index in the entire list of
packages from packages.php.

- $POS :: This is not the package ID but simply the list offset on the
  package webpage.

* SEE ALSO
WWW::AUR

* AUTHOR
Justin Davis, =<juster at cpan dot org>=

* BUGS
Please email me any bugs you find. I will try to fix them as quick as I
can.

* SUPPORT
Send me an email if you have any questions or need help.

* LICENSE AND COPYRIGHT
Copyright 2014 Justin Davis.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
