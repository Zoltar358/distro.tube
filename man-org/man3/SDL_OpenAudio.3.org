#+TITLE: Manpages - SDL_OpenAudio.3
#+DESCRIPTION: Linux manpage for SDL_OpenAudio.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_OpenAudio - Opens the audio device with the desired parameters.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_OpenAudio*(*SDL_AudioSpec *desired, SDL_AudioSpec *obtained*);

* DESCRIPTION
This function opens the audio device with the *desired* parameters, and
returns 0 if successful, placing the actual hardware parameters in the
structure pointed to by *obtained*. If *obtained* is NULL, the audio
data passed to the callback function will be guaranteed to be in the
requested format, and will be automatically converted to the hardware
audio format if necessary. This function returns -1 if it failed to open
the audio device, or couldn't set up the audio thread.

To open the audio device a *desired* *SDL_AudioSpec* must be created.

#+begin_example
  SDL_AudioSpec *desired;
  .
  .
  desired=(SDL_AudioSpec *)malloc(sizeof(SDL_AudioSpec));
#+end_example

You must then fill this structure with your desired audio
specifications.

- desired->freq :: 

- desired->format :: 

- desired->samples :: 

- desired->callback :: 

#+begin_example
  void callback(void *userdata, Uint8 *stream, int len);
#+end_example

*userdata* is the pointer stored in *userdata* field of the
*SDL_AudioSpec*. *stream* is a pointer to the audio buffer you want to
fill with information and *len* is the length of the audio buffer in
bytes.

- desired->userdata :: 

*SDL_OpenAudio* reads these fields from the *desired* *SDL_AudioSpec*
structure pass to the function and attempts to find an audio
configuration matching your *desired*. As mentioned above, if the
*obtained* parameter is *NULL* then SDL with convert from your *desired*
audio settings to the hardware settings as it plays.

If *obtained* is *NULL* then the *desired* *SDL_AudioSpec* is your
working specification, otherwise the *obtained* *SDL_AudioSpec* becomes
the working specification and the *desirec* specification can be
deleted. The data in the working specification is used when building
*SDL_AudioCVT*'s for converting loaded data to the hardware format.

*SDL_OpenAudio* calculates the *size* and *silence* fields for both the
*desired* and *obtained* specifications. The *size* field stores the
total size of the audio buffer in bytes, while the *silence* stores the
value used to represent silence in the audio buffer

The audio device starts out playing *silence* when it's opened, and
should be enabled for playing by calling *SDL_PauseAudio*/(/*0*) when
you are ready for your audio *callback* function to be called. Since the
audio driver may modify the requested *size* of the audio buffer, you
should allocate any local mixing buffers after you open the audio
device.

* EXAMPLES
#+begin_example
  /* Prototype of our callback function */
  void my_audio_callback(void *userdata, Uint8 *stream, int len);

  /* Open the audio device */
  SDL_AudioSpec *desired, *obtained;
  SDL_AudioSpec *hardware_spec;

  /* Allocate a desired SDL_AudioSpec */
  desired=(SDL_AudioSpec *)malloc(sizeof(SDL_AudioSpec));

  /* Allocate space for the obtained SDL_AudioSpec */
  obtained=(SDL_AudioSpec *)malloc(sizeof(SDL_AudioSpec));

  /* 22050Hz - FM Radio quality */
  desired->freq=22050;

  /* 16-bit signed audio */
  desired->format=AUDIO_S16LSB;

  /* Mono */
  desired->channels=0;

  /* Large audio buffer reduces risk of dropouts but increases response time */
  desired->samples=8192;

  /* Our callback function */
  desired->callback=my_audio_callback;

  desired->userdata=NULL;

  /* Open the audio device */
  if ( SDL_OpenAudio(desired, obtained) < 0 ){
    fprintf(stderr, "Couldn't open audio: %s
  ", SDL_GetError());
    exit(-1);
  }
  /* desired spec is no longer needed */
  free(desired);
  hardware_spec=obtained;
  .
  .
  /* Prepare callback for playing */
  .
  .
  .
  /* Start playing */
  SDL_PauseAudio(0);
#+end_example

* SEE ALSO
*SDL_AudioSpec*, *SDL_LockAudio*, *SDL_UnlockAudio*, *SDL_PauseAudio*
