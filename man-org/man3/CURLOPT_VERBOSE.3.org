#+TITLE: Manpages - CURLOPT_VERBOSE.3
#+DESCRIPTION: Linux manpage for CURLOPT_VERBOSE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_VERBOSE - verbose mode

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_VERBOSE, long onoff);

* DESCRIPTION
Set the /onoff/ parameter to 1 to make the library display a lot of
verbose information about its operations on this /handle/. Useful for
libcurl and/or protocol debugging and understanding. The verbose
information will be sent to stderr, or the stream set with
/CURLOPT_STDERR(3)/.

You hardly ever want this set in production use, you will almost always
want this when you debug/report problems.

To also get all the protocol data sent and received, consider using the
/CURLOPT_DEBUGFUNCTION(3)/.

* DEFAULT
0, meaning disabled.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* ask libcurl to show us the verbose output */
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    /* Perform the request */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_STDERR*(3), *CURLOPT_DEBUGFUNCTION*(3),
*CURLOPT_ERRORBUFFER*(3),
