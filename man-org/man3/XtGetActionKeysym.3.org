#+TITLE: Manpages - XtGetActionKeysym.3
#+DESCRIPTION: Linux manpage for XtGetActionKeysym.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtGetActionKeysym - obtain corresponding keysym

* SYNTAX
#include <X11/Intrinsic.h>

KeySym XtGetActionKeysym(XEvent* /event/, Modifiers*
/modifiers_return/);

* ARGUMENTS
- event :: Specifies the event pointer passed to the action procedure by
  the Intrinsics.

- modifiers_return :: Returns the modifiers that caused the match, if
  non-NULL.

* DESCRIPTION
If *XtGetActionKeysym* is called after an action procedure has been
invoked by the Intrinsics and before that action procedure returns, and
if the event pointer has the same value as the event pointer passed to
that action routine, and if the event is a *KeyPress* or *KeyRelease*
event, then *XtGetActionKeysym* returns the KeySym that matched the
final event specification in the translation table and, if
/modifiers_return/ is non-NULL, the modifier state actually used to
generate this KeySym; otherwise, if the event is a *KeyPress* or
*KeyRelease* event, then *XtGetActionKeysym* calls *XtTranslateKeycode*
and returns the results; else it returns *NoSymbol* and does not examine
/modifiers_return/.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
