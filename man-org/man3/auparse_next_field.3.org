#+TITLE: Manpages - auparse_next_field.3
#+DESCRIPTION: Linux manpage for auparse_next_field.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_next_field - move field cursor

* SYNOPSIS
*#include <auparse.h>*

int auparse_next_field(auparse_state_t *au);

* DESCRIPTION
auparse_next_field moves the library's internal cursor to point to the
next field in the current record of the current event.

* RETURN VALUE
Returns 0 if no more fields exist and 1 for success.

* SEE ALSO
*auparse_next_record*(3).

* AUTHOR
Steve Grubb
