#+TITLE: Manpages - std___parallel__CRandNumber.3
#+DESCRIPTION: Linux manpage for std___parallel__CRandNumber.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__parallel::_CRandNumber< _MustBeInt > - Functor wrapper for
std::rand().

* SYNOPSIS
\\

=#include <algo.h>=

** Public Member Functions
int *operator()* (int __limit)\\

* Detailed Description
** "template<typename _MustBeInt = int>
\\
struct std::__parallel::_CRandNumber< _MustBeInt >"Functor wrapper for
std::rand().

Definition at line *1544* of file *algo.h*.

* Member Function Documentation
** template<typename _MustBeInt = int> int
*std::__parallel::_CRandNumber*< _MustBeInt >::operator() (int
__limit)= [inline]=
Definition at line *1547* of file *algo.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
