#+TITLE: Manpages - std___detail__Node_iterator.3
#+DESCRIPTION: Linux manpage for std___detail__Node_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Node_iterator< _Value, __constant_iterators, __cache > -
Node iterators, used to iterate through all the hashtable.

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

Inherits *std::__detail::_Node_iterator_base< _Value, __cache >*.

** Public Types
typedef std::ptrdiff_t *difference_type*\\

typedef *std::forward_iterator_tag* *iterator_category*\\

using *pointer* = typename *std::conditional*< __constant_iterators,
const value_type *, value_type * >::type\\

using *reference* = typename *std::conditional*< __constant_iterators,
const value_type &, value_type & >::type\\

typedef _Value *value_type*\\

** Public Member Functions
*_Node_iterator* (__node_type *__p) noexcept\\

void *_M_incr* () noexcept\\

reference *operator** () const noexcept\\

*_Node_iterator* & *operator++* () noexcept\\

*_Node_iterator* *operator++* (int) noexcept\\

pointer *operator->* () const noexcept\\

** Public Attributes
__node_type * *_M_cur*\\

* Detailed Description
** "template<typename _Value, bool __constant_iterators, bool __cache>
\\
struct std::__detail::_Node_iterator< _Value, __constant_iterators,
__cache >"Node iterators, used to iterate through all the hashtable.

Definition at line *316* of file *hashtable_policy.h*.

* Member Typedef Documentation
** template<typename _Value , bool __constant_iterators, bool __cache>
typedef std::ptrdiff_t *std::__detail::_Node_iterator*< _Value,
__constant_iterators, __cache >::difference_type
Definition at line *325* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
typedef *std::forward_iterator_tag* *std::__detail::_Node_iterator*<
_Value, __constant_iterators, __cache >::*iterator_category*
Definition at line *326* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
using *std::__detail::_Node_iterator*< _Value, __constant_iterators,
__cache >::pointer = typename *std::conditional*<__constant_iterators,
const value_type*, value_type*>::type
Definition at line *328* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
using *std::__detail::_Node_iterator*< _Value, __constant_iterators,
__cache >::reference = typename *std::conditional*<__constant_iterators,
const value_type&, value_type&>::type
Definition at line *331* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
typedef _Value *std::__detail::_Node_iterator*< _Value,
__constant_iterators, __cache >::value_type
Definition at line *324* of file *hashtable_policy.h*.

* Constructor & Destructor Documentation
** template<typename _Value , bool __constant_iterators, bool __cache>
*std::__detail::_Node_iterator*< _Value, __constant_iterators, __cache
>::*_Node_iterator* (__node_type * __p)= [inline]=, = [explicit]=,
= [noexcept]=
Definition at line *337* of file *hashtable_policy.h*.

* Member Function Documentation
** void *std::__detail::_Node_iterator_base*< _Value, _Cache_hash_code
>::_M_incr ()= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *298* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
reference *std::__detail::_Node_iterator*< _Value, __constant_iterators,
__cache >::operator* () const= [inline]=, = [noexcept]=
Definition at line *341* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
*_Node_iterator* & *std::__detail::_Node_iterator*< _Value,
__constant_iterators, __cache >::operator++ ()= [inline]=, = [noexcept]=
Definition at line *349* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
*_Node_iterator* *std::__detail::_Node_iterator*< _Value,
__constant_iterators, __cache >::operator++ (int)= [inline]=,
= [noexcept]=
Definition at line *356* of file *hashtable_policy.h*.

** template<typename _Value , bool __constant_iterators, bool __cache>
pointer *std::__detail::_Node_iterator*< _Value, __constant_iterators,
__cache >::operator-> () const= [inline]=, = [noexcept]=
Definition at line *345* of file *hashtable_policy.h*.

* Member Data Documentation
** __node_type* *std::__detail::_Node_iterator_base*< _Value,
_Cache_hash_code >::_M_cur= [inherited]=
Definition at line *291* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
