#+TITLE: Manpages - XkbKeyType.3
#+DESCRIPTION: Linux manpage for XkbKeyType.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbKeyType - Obtain the index of a key type or the pointer to a key type

* SYNOPSIS
*XkbKeyTypePtr XkbKeyType* *( XkbDescPtr */xkb/* ,* *KeyCode
*/keycode/* ,* *int */group/* );*

* ARGUMENTS
- /- xkb/ :: Xkb description of interest

- /- keycode/ :: keycode of interest

- /- group/ :: group index

* DESCRIPTION
/XkbKeyType/ returns a pointer to the key type in the /types/ vector of
the client map in /xkb/ corresponding to the given /keycode/ and /group/
index.

* STRUCTURES
#+begin_example

  typedef struct {                             /* Key Type */
  	XkbModsRec              mods;           /* modifiers used to compute shift level */
  	unsigned char           num_levels;     /* total # shift levels, do not modify directly */
  	unsigned char           map_count;      /* # entries in map, preserve (if non-NULL) */
  	XkbKTMapEntryPtr        map;            /* vector of modifiers for each shift level */
  	XkbModsPtr              preserve;       /* mods to preserve for corresponding map entry */
  	Atom                    name;           /* name of key type */
  	Atom *                  level_names;    /* array of names of each shift level */
  } XkbKeyTypeRec, *XkbKeyTypePtr;
#+end_example
