#+TITLE: Manpages - hwloc_cudart_get_device_osdev_by_index.3
#+DESCRIPTION: Linux manpage for hwloc_cudart_get_device_osdev_by_index.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_cudart_get_device_osdev_by_index.3 is found in manpage for: [[../man3/hwlocality_cudart.3][man3/hwlocality_cudart.3]]