#+TITLE: Manpages - zip_set_default_password.3
#+DESCRIPTION: Linux manpage for zip_set_default_password.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function sets the default password used when accessing encrypted files.
If

is

or the empty string, the default password is unset.

If you prefer a different password for single files, use

instead of

Usually, however, the same password is used for every file in an zip
archive.

The password is not verified when calling this function. See the

section in

for more details about password handling.

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

fails if:

Required memory could not be allocated.

was added in libzip 0.10.

and
