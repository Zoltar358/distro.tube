#+TITLE: Manpages - ne_malloc.3
#+DESCRIPTION: Linux manpage for ne_malloc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_malloc, ne_calloc, ne_realloc, ne_strdup, ne_strndup,
ne_oom_callback - memory allocation wrappers

* SYNOPSIS
#+begin_example
  #include <ne_alloc.h>
#+end_example

*void *ne_malloc(size_t */size/*);*

*void *ne_calloc(size_t */size/*);*

*void *ne_realloc(void **/size/*, size_t */len/*);*

*char *ne_strdup(const char **/s/*);*

*char *ne_strndup(const char **/s/*, size_t */size/*);*

*void ne_oom_callback(void (**/callback/*)(void));*

* DESCRIPTION
The functions *ne_malloc*, *ne_calloc*, *ne_realloc*, *ne_strdup* and
*ne_strdnup* provide wrappers for the equivalent functions in the
standard C library. The wrappers provide the extra guarantee that if the
C library equivalent returns NULL when no memory is available, an
optional callback will be called, and the library will then call
*abort*().

*ne_oom_callback* registers a callback which will be invoked if an out
of memory error is detected.

* NOTES
If the operating system uses optimistic memory allocation, the C library
memory allocation routines will not return NULL, so it is not possible
to gracefully handle memory allocation failures.

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
