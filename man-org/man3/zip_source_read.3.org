#+TITLE: Manpages - zip_source_read.3
#+DESCRIPTION: Linux manpage for zip_source_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

reads up to

bytes of data from

at the current read offset into the buffer

The zip source

has to be opened for reading by calling

first.

Upon successful completion the number of bytes read is returned. Upon
reading end-of-file, zero is returned. Otherwise, -1 is returned and the
error information in

is set to indicate the error.

was added in libzip 1.0.

and
