#+TITLE: Manpages - Term_UI_History.3pm
#+DESCRIPTION: Linux manpage for Term_UI_History.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Term::UI::History - history function

* SYNOPSIS
use Term::UI::History qw[history]; history("Some message"); ### retrieve
the history in printable form $hist =
Term::UI::History->history_as_string; ### redirect output local
$Term::UI::History::HISTORY_FH = \*STDERR;

* DESCRIPTION
This module provides the =history= function for =Term::UI=, printing and
saving all the =UI= interaction.

Refer to the =Term::UI= manpage for details on usage from =Term::UI=.

This module subclasses =Log::Message::Simple=. Refer to its manpage for
additional functionality available via this package.

* FUNCTIONS
** history("message string" [,VERBOSE])
Records a message on the stack, and prints it to =STDOUT= (or actually
=$HISTORY_FH=, see the =GLOBAL VARIABLES= section below), if the
=VERBOSE= option is true.

The =VERBOSE= option defaults to true.

* GLOBAL VARIABLES
- $HISTORY_FH :: This is the filehandle all the messages sent to
  =history()= are being printed. This defaults to =*STDOUT=.

* See Also
=Log::Message::Simple=, =Term::UI=

* AUTHOR
This module by Jos Boumans <kane@cpan.org>.

* COPYRIGHT
This module is copyright (c) 2005 Jos Boumans <kane@cpan.org>. All
rights reserved.

This library is free software; you may redistribute and/or modify it
under the same terms as Perl itself.
