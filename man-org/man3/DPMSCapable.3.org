#+TITLE: Manpages - DPMSCapable.3
#+DESCRIPTION: Linux manpage for DPMSCapable.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
DPMSCapable - returns the DPMS capability of the X server

* SYNOPSIS
#+begin_example
  cc [ flag ... ] file ... -lXext [ library ... ]
  #include <X11/extensions/dpms.h>
  Bool DPMSCapable ( Display *display  );
#+end_example

* ARGUMENTS
- /display/ :: Specifies the connection to the X server

* DESCRIPTION
The /DPMSCapable/ function returns the Display Power Management
Signaling (DPMS) capability of the X server, either TRUE  (capable of
DPMS ) or FALSE  (incapable of DPMS ).

* RETURN VALUES
- True :: The /DPMSCapable/ function returns True if the X server is
  capable of DPMS.

- False :: The /DPMSCapable/ function returns True if the X server is
  incapable of DPMS.
