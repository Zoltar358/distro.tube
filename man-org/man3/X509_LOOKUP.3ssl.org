#+TITLE: Manpages - X509_LOOKUP.3ssl
#+DESCRIPTION: Linux manpage for X509_LOOKUP.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
X509_LOOKUP, X509_LOOKUP_TYPE, X509_LOOKUP_new, X509_LOOKUP_free,
X509_LOOKUP_init, X509_LOOKUP_shutdown, X509_LOOKUP_set_method_data,
X509_LOOKUP_get_method_data, X509_LOOKUP_ctrl, X509_LOOKUP_load_file,
X509_LOOKUP_add_dir, X509_LOOKUP_get_store, X509_LOOKUP_by_subject,
X509_LOOKUP_by_issuer_serial, X509_LOOKUP_by_fingerprint,
X509_LOOKUP_by_alias - OpenSSL certificate lookup mechanisms

* SYNOPSIS
#include <openssl/x509_vfy.h> typedef x509_lookup_st X509_LOOKUP;
typedef enum X509_LOOKUP_TYPE; X509_LOOKUP
*X509_LOOKUP_new(X509_LOOKUP_METHOD *method); int
X509_LOOKUP_init(X509_LOOKUP *ctx); int X509_LOOKUP_shutdown(X509_LOOKUP
*ctx); void X509_LOOKUP_free(X509_LOOKUP *ctx); int
X509_LOOKUP_set_method_data(X509_LOOKUP *ctx, void *data); void
*X509_LOOKUP_get_method_data(const X509_LOOKUP *ctx); int
X509_LOOKUP_ctrl(X509_LOOKUP *ctx, int cmd, const char *argc, long argl,
char **ret); int X509_LOOKUP_load_file(X509_LOOKUP *ctx, char *name,
long type); int X509_LOOKUP_add_dir(X509_LOOKUP *ctx, char *name, long
type); X509_STORE *X509_LOOKUP_get_store(const X509_LOOKUP *ctx); int
X509_LOOKUP_by_subject(X509_LOOKUP *ctx, X509_LOOKUP_TYPE type,
X509_NAME *name, X509_OBJECT *ret); int
X509_LOOKUP_by_issuer_serial(X509_LOOKUP *ctx, X509_LOOKUP_TYPE type,
X509_NAME *name, ASN1_INTEGER *serial, X509_OBJECT *ret); int
X509_LOOKUP_by_fingerprint(X509_LOOKUP *ctx, X509_LOOKUP_TYPE type,
const unsigned char *bytes, int len, X509_OBJECT *ret); int
X509_LOOKUP_by_alias(X509_LOOKUP *ctx, X509_LOOKUP_TYPE type, const char
*str, int len, X509_OBJECT *ret);

* DESCRIPTION
The *X509_LOOKUP* structure holds the information needed to look up
certificates and CRLs according to an associated
*X509_LOOKUP_METHOD* (3). Multiple *X509_LOOKUP* instances can be added
to an *X509_STORE* (3) to enable lookup in that store.

*X509_LOOKUP_new()* creates a new *X509_LOOKUP* using the given lookup
/method/. It can also be created by calling *X509_STORE_add_lookup* (3),
which will associate an *X509_STORE* with the lookup mechanism.

*X509_LOOKUP_init()* initializes the internal state and resources as
needed by the given *X509_LOOKUP* to do its work.

*X509_LOOKUP_shutdown()* tears down the internal state and resources of
the given *X509_LOOKUP*.

*X509_LOOKUP_free()* destructs the given *X509_LOOKUP*.

*X509_LOOKUP_set_method_data()* associates a pointer to application data
to the given *X509_LOOKUP*.

*X509_LOOKUP_get_method_data()* retrieves a pointer to application data
from the given *X509_LOOKUP*.

*X509_LOOKUP_ctrl()* is used to set or get additional data to or from an
*X509_LOOKUP* structure or its associated *X509_LOOKUP_METHOD* (3). The
arguments of the control command are passed via /argc/ and /argl/, its
return value via /*ret/. The meaning of the arguments depends on the
/cmd/ number of the control command. In general, this function is not
called directly, but wrapped by a macro call, see below. The control
/cmd/s known to OpenSSL are discussed in more depth in Control Commands.

*X509_LOOKUP_load_file()* passes a filename to be loaded immediately
into the associated *X509_STORE*. /type/ indicates what type of object
is expected. This can only be used with a lookup using the
implementation *X509_LOOKUP_file* (3).

*X509_LOOKUP_add_dir()* passes a directory specification from which
certificates and CRLs are loaded on demand into the associated
*X509_STORE*. /type/ indicates what type of object is expected. This can
only be used with a lookup using the implementation
*X509_LOOKUP_hash_dir* (3).

*X509_LOOKUP_load_file()*, *X509_LOOKUP_add_dir()*,
*X509_LOOKUP_add_store()*, and *X509_LOOKUP_load_store()* are
implemented as macros that use *X509_LOOKUP_ctrl()*.

*X509_LOOKUP_by_subject()*, *X509_LOOKUP_by_issuer_serial()*,
*X509_LOOKUP_by_fingerprint()*, and *X509_LOOKUP_by_alias()* look up
certificates and CRLs in the *X509_STORE* (3) associated with the
*X509_LOOKUP* using different criteria, where the looked up object is
stored in /ret/. Some of the underlying *X509_LOOKUP_METHOD*s will also
cache objects matching the criteria in the associated *X509_STORE*,
which makes it possible to handle cases where the criteria have more
than one hit.

** File Types
*X509_LOOKUP_load_file()* and *X509_LOOKUP_add_dir()* take a /type/,
which can be one of the following:

- X509_FILETYPE_PEM :: The file or files that are loaded are expected to
  be in PEM format.

- X509_FILETYPE_ASN1 :: The file or files that are loaded are expected
  to be in raw DER format.

- X509_FILETYPE_DEFAULT :: The default certificate file or directory is
  used. In this case, /name/ is ignored.

** Control Commands
The *X509_LOOKUP_METHOD*s built into OpenSSL recognise the following
*X509_LOOKUP_ctrl()* /cmd/s:

- X509_L_FILE_LOAD :: This is the command that *X509_LOOKUP_load_file()*
  uses. The filename is passed in /argc/, and the type in /argl/.

- X509_L_ADD_DIR :: This is the command that *X509_LOOKUP_add_dir()*
  uses. The directory specification is passed in /argc/, and the type in
  /argl/.

- X509_L_ADD_STORE :: This is the command that *X509_LOOKUP_add_store()*
  uses. The URI is passed in /argc/.

- X509_L_LOAD_STORE :: This is the command that
  *X509_LOOKUP_load_store()* uses. The URI is passed in /argc/.

* RETURN VALUES
*X509_LOOKUP_new()* returns an *X509_LOOKUP* pointer when successful, or
NULL on error.

*X509_LOOKUP_init()* and *X509_LOOKUP_shutdown()* return 1 on success,
or 0 on error.

*X509_LOOKUP_ctrl()* returns -1 if the *X509_LOOKUP* doesn't have an
associated *X509_LOOKUP_METHOD*, or 1 if the doesn't have a control
function. Otherwise, it returns what the control function in the
*X509_LOOKUP_METHOD* returns, which is usually 1 on success and 0 in
error.

*X509_LOOKUP_get_store()* returns an *X509_STORE* pointer if there is
one, otherwise NULL.

*X509_LOOKUP_by_subject()*, *X509_LOOKUP_by_issuer_serial()*,
*X509_LOOKUP_by_fingerprint()*, and *X509_LOOKUP_by_alias()* all return
0 if there is no *X509_LOOKUP_METHOD* or that method doesn't implement
the corresponding function. Otherwise, it returns what the corresponding
function in the *X509_LOOKUP_METHOD* returns, which is usually 1 on
success and 0 in error.

* SEE ALSO
*X509_LOOKUP_METHOD* (3), *X509_STORE* (3)

* COPYRIGHT
Copyright 2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the Apache License 2.0 (the License). You may not use
this file except in compliance with the License. You can obtain a copy
in the file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
