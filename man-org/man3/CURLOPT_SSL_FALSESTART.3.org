#+TITLE: Manpages - CURLOPT_SSL_FALSESTART.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSL_FALSESTART.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSL_FALSESTART - TLS false start

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_FALSESTART, long
enable);

* DESCRIPTION
Pass a long as parameter set to 1L to enable or 0 to disable.

This option determines whether libcurl should use false start during the
TLS handshake. False start is a mode where a TLS client will start
sending application data before verifying the server's Finished message,
thus saving a round trip when performing a full handshake.

* DEFAULT
0

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_SSL_FALSESTART, 1L);
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.42.0. This option is currently only supported by the NSS and
Secure Transport (on iOS 7.0 or later, or OS X 10.9 or later) TLS
backends.

* RETURN VALUE
Returns CURLE_OK if false start is supported by the SSL backend,
otherwise returns CURLE_NOT_BUILT_IN.

* SEE ALSO
*CURLOPT_TCP_FASTOPEN*(3),
