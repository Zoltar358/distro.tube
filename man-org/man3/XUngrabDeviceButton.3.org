#+TITLE: Manpages - XUngrabDeviceButton.3
#+DESCRIPTION: Linux manpage for XUngrabDeviceButton.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XUngrabDeviceButton.3 is found in manpage for: [[../man3/XGrabDeviceButton.3][man3/XGrabDeviceButton.3]]