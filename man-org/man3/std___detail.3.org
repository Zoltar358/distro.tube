#+TITLE: Manpages - std___detail.3
#+DESCRIPTION: Linux manpage for std___detail.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail - Implementation details not part of the namespace std
interface.

* SYNOPSIS
\\

** Classes
struct *_BracketMatcher*\\
Matches a character range (bracket expression)

class *_Compiler*\\
Builds an NFA from an input iterator range.

struct *_Default_ranged_hash*\\
Default ranged hash function H. In principle it should be a function
object composed from objects of type H1 and H2 such that h(k, N) =
h2(h1(k), N), but that would mean making extra copies of h1 and h2. So
instead we'll just use a tag to tell class template hashtable to do that
composition.

struct *_Equality*\\

struct *_Equality< _Key, _Value, _Alloc, _ExtractKey, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, false >*\\
unordered_multiset and unordered_multimap specializations.

struct *_Equality< _Key, _Value, _Alloc, _ExtractKey, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, true >*\\
unordered_map and unordered_set specializations.

class *_Executor*\\
Takes a regex and an input string and does the matching.

struct *_Hash_code_base*\\

struct *_Hash_node*\\

struct *_Hash_node_base*\\

struct *_Hash_node_code_cache*\\

struct *_Hash_node_code_cache< true >*\\

struct *_Hash_node_value_base*\\

struct *_Hashtable_alloc*\\

struct *_Hashtable_base*\\

struct *_Hashtable_ebo_helper*\\

struct *_Hashtable_ebo_helper< _Nm, _Tp, false >*\\
Specialization not using EBO.

struct *_Hashtable_ebo_helper< _Nm, _Tp, true >*\\
Specialization using EBO.

struct *_Hashtable_traits*\\

struct *_Insert*\\

struct *_Insert< _Key, _Value, _Alloc, _ExtractKey, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, false >*\\
Specialization.

struct *_Insert< _Key, _Value, _Alloc, _ExtractKey, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, true >*\\
Specialization.

struct *_Insert_base*\\

struct *_List_node_base*\\
Common part of a node in the list.

struct *_List_node_header*\\
The list node header.

struct *_Local_const_iterator*\\
local const_iterators

struct *_Local_iterator*\\
local iterators

struct *_Local_iterator_base*\\

struct *_Local_iterator_base< _Key, _Value, _ExtractKey, _Hash,
_RangeHash, _Unused, true >*\\
Partial specialization used when nodes contain a cached hash code.

struct *_Map_base*\\

struct *_Map_base< _Key, _Pair, _Alloc, _Select1st, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, false >*\\
Partial specialization, __unique_keys set to false.

struct *_Map_base< _Key, _Pair, _Alloc, _Select1st, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, true >*\\
Partial specialization, __unique_keys set to true.

struct *_Mask_range_hashing*\\
Range hashing function assuming that second arg is a power of 2.

struct *_Mod_range_hashing*\\
Default range hashing function: use division to fold a large number into
the range [0, N).

struct *_Node_const_iterator*\\
Node const_iterators, used to iterate through all the hashtable.

struct *_Node_iterator*\\
Node iterators, used to iterate through all the hashtable.

struct *_Node_iterator_base*\\
Base class for node iterators.

struct *_Power2_rehash_policy*\\
Rehash policy providing power of 2 bucket numbers. Avoids modulo
operations.

struct *_Prime_rehash_policy*\\
Default value for rehash policy. Bucket size is (usually) the smallest
prime that keeps the load factor small enough.

struct *_Quoted_string*\\
Struct for delimited strings.

struct *_Rehash_base*\\

struct *_Rehash_base< _Key, _Value, _Alloc, _ExtractKey, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, false_type >*\\
Specialization when rehash policy doesn't provide load factor
management.

struct *_Rehash_base< _Key, _Value, _Alloc, _ExtractKey, _Equal, _Hash,
_RangeHash, _Unused, _RehashPolicy, _Traits, true_type >*\\
Specialization when rehash policy provide load factor management.

class *_Scanner*\\
Scans an input range for regex tokens.

class *_StateSeq*\\
Describes a sequence of one or more _State, its current start and
end(s). This structure contains fragments of an NFA during construction.

** Typedefs
using *__bare_timed_wait* = __timed_waiter< *std::false_type* >\\

using *__bare_wait* = __waiter< *std::false_type* >\\

template<typename _Iter , typename _TraitsT > using
*__disable_if_contiguous_iter* = __enable_if_t< !__is_contiguous_iter<
_Iter >::value, *std::shared_ptr*< const _NFA< _TraitsT > > >\\

template<typename _Iter , typename _TraitsT > using
*__enable_if_contiguous_iter* = __enable_if_t< __is_contiguous_iter<
_Iter >::value, *std::shared_ptr*< const _NFA< _TraitsT > > >\\

using *__enters_timed_wait* = __timed_waiter< *std::true_type* >\\

using *__enters_wait* = __waiter< *std::true_type* >\\

template<typename _Policy > using *__has_load_factor* = typename
_Policy::__has_load_factor\\

template<typename _Key , typename _Value , typename _ExtractKey ,
typename _Hash , typename _RangeHash , typename _Unused > using
*__hash_code_for_local_iter* = _Hash_code_storage< *_Hash_code_base*<
_Key, _Value, _ExtractKey, _Hash, _RangeHash, _Unused, false > >\\

template<typename _Tp > using *__integer_from_chars_result_type* =
*enable_if_t*< __or_< __is_signed_integer< _Tp >, __is_unsigned_integer<
_Tp >, *is_same*< char, *remove_cv_t*< _Tp > > >::value,
*from_chars_result* >\\

template<typename _Tp > using *__integer_to_chars_result_type* =
*enable_if_t*< __or_< __is_signed_integer< _Tp >, __is_unsigned_integer<
_Tp >, *is_same*< char, *remove_cv_t*< _Tp > > >::value,
*to_chars_result* >\\

using *__platform_wait_t* = uint64_t\\

template<typename _Tp > using *__unsigned_least_t* = typename
__to_chars_unsigned_type< _Tp >::type\\

using *__wait_clock_t* = *chrono::steady_clock*\\

template<typename _CharT > using *_Matcher* = std::function<
bool(_CharT)>\\

typedef long *_StateIdT*\\

** Enumerations
enum *_Opcode* : int { *_S_opcode_unknown*, *_S_opcode_alternative*,
*_S_opcode_repeat*, *_S_opcode_backref*,
*_S_opcode_line_begin_assertion*, *_S_opcode_line_end_assertion*,
*_S_opcode_word_boundary*, *_S_opcode_subexpr_lookahead*,
*_S_opcode_subexpr_begin*, *_S_opcode_subexpr_end*, *_S_opcode_dummy*,
*_S_opcode_match*, *_S_opcode_accept* }\\
Operation codes that define the type of transitions within the base NFA
that represents the regular expression.

enum class *_RegexExecutorPolicy* : int { *_S_auto*, *_S_alternate* }\\

** Functions
template<typename _Up , typename _Tp > constexpr _Up *__absu* (_Tp
__val)\\

template<typename _Up > void *__absu* (bool)=delete\\

template<typename _Tp > bool *__atomic_compare* (const _Tp &__a, const
_Tp &__b)\\

template<typename _Pred , typename _Spin = __default_spin_policy> bool
*__atomic_spin* (_Pred &__pred, _Spin __spin=_Spin{ }) noexcept\\

std::size_t *__clp2* (std::size_t __n) noexcept\\
Compute closest power of 2 not less than __n.

template<typename _TraitsT , typename _FwdIter >
__enable_if_contiguous_iter< _FwdIter, _TraitsT > *__compile_nfa*
(_FwdIter __first, _FwdIter __last, const typename _TraitsT::locale_type
&__loc, *regex_constants::syntax_option_type* __flags)\\

template<typename _TraitsT , typename _FwdIter >
__disable_if_contiguous_iter< _FwdIter, _TraitsT > *__compile_nfa*
(_FwdIter __first, _FwdIter __last, const typename _TraitsT::locale_type
&__loc, *regex_constants::syntax_option_type* __flags)\\

template<typename _Clock , typename _Dur > bool *__cond_wait_until*
(__condvar &__cv, *mutex* &__mx, const *chrono::time_point*< _Clock,
_Dur > &__atime)\\

template<typename _Clock , typename _Dur > bool *__cond_wait_until_impl*
(__condvar &__cv, *mutex* &__mx, const *chrono::time_point*< _Clock,
_Dur > &__atime)\\

template<class _Iterator > *std::iterator_traits*< _Iterator
>::difference_type *__distance_fw* (_Iterator __first, _Iterator
__last)\\

template<class _Iterator > *std::iterator_traits*< _Iterator
>::difference_type *__distance_fw* (_Iterator __first, _Iterator __last,
*std::forward_iterator_tag*)\\

template<class _Iterator > *std::iterator_traits*< _Iterator
>::difference_type *__distance_fw* (_Iterator __first, _Iterator __last,
*std::input_iterator_tag*)\\

template<typename _Container , typename _Predicate >
_Container::size_type *__erase_nodes_if* (_Container &__cont, _Predicate
__pred)\\

template<typename _ValT , typename _CharT , typename _Traits >
*basic_istream*< _CharT, _Traits > & *__extract_params*
(*basic_istream*< _CharT, _Traits > &__is, *vector*< _ValT > &__vals,
size_t __n)\\

template<typename _Tp > bool *__from_chars_alnum* (const char *&__first,
const char *__last, _Tp &__val, int __base)\\
std::from_chars implementation for integers in bases 11 to 26.

constexpr unsigned char *__from_chars_alpha_to_num* (char __c)\\

template<typename _Tp > bool *__from_chars_binary* (const char
*&__first, const char *__last, _Tp &__val)\\
std::from_chars implementation for integers in base 2.

template<typename _Tp > bool *__from_chars_digit* (const char *&__first,
const char *__last, _Tp &__val, int __base)\\
std::from_chars implementation for integers in bases 3 to 10.

template<typename _Tp > constexpr _Tp *__gcd* (_Tp __m, _Tp __n)\\

template<typename _Tp > constexpr _Tp *__lcm* (_Tp __m, _Tp __n)\\

template<typename _InputIterator , typename _OutputIterator , typename
_Tp > _OutputIterator *__normalize* (_InputIterator __first,
_InputIterator __last, _OutputIterator __result, const _Tp &__factor)\\

template<typename _Tp > constexpr bool *__p1_representable_as_double*
(_Tp __x) noexcept\\

template<typename _Tp > bool *__raise_and_add* (_Tp &__val, int __base,
unsigned char __c)\\

template<typename _BiIter , typename _Alloc , typename _CharT , typename
_TraitsT , _RegexExecutorPolicy __policy, bool __match_mode> bool
*__regex_algo_impl* (_BiIter __s, _BiIter __e, *match_results*< _BiIter,
_Alloc > &__m, const *basic_regex*< _CharT, _TraitsT > &__re,
*regex_constants::match_flag_type* __flags)\\

template<typename _Tp > constexpr bool *__representable_as_double* (_Tp
__x) noexcept\\

template<typename _Tp > void *__return_temporary_buffer* (_Tp *__p,
size_t __len)\\

void *__thread_relax* () noexcept\\

void *__thread_yield* () noexcept\\

template<typename _Tp > *to_chars_result* *__to_chars* (char *__first,
char *__last, _Tp __val, int __base) noexcept\\

template<typename _Tp > __integer_to_chars_result_type< _Tp >
*__to_chars_10* (char *__first, char *__last, _Tp __val) noexcept\\

template<typename _Tp > void *__to_chars_10_impl* (char *__first,
unsigned __len, _Tp __val) noexcept\\

template<typename _Tp > __integer_to_chars_result_type< _Tp >
*__to_chars_16* (char *__first, char *__last, _Tp __val) noexcept\\

template<typename _Tp > __integer_to_chars_result_type< _Tp >
*__to_chars_2* (char *__first, char *__last, _Tp __val) noexcept\\

template<typename _Tp > __integer_to_chars_result_type< _Tp >
*__to_chars_8* (char *__first, char *__last, _Tp __val) noexcept\\

template<typename _Tp > constexpr unsigned *__to_chars_len* (_Tp
__value, int __base=10) noexcept\\

template<typename _Tp > constexpr unsigned *__to_chars_len_2* (_Tp
__value) noexcept\\

template<typename _Dur > __wait_clock_t::time_point *__to_wait_clock*
(const *chrono::time_point*< *__wait_clock_t*, _Dur > &__atime)
noexcept\\

template<typename _Clock , typename _Dur > __wait_clock_t::time_point
*__to_wait_clock* (const *chrono::time_point*< _Clock, _Dur > &__atime)
noexcept\\

template<typename _Tp > constexpr bool *_Power_of_2* (_Tp __x)\\

template<typename _CharT , typename _Traits , typename _String >
*std::basic_ostream*< _CharT, _Traits > & *operator<<*
(*std::basic_ostream*< _CharT, _Traits > &__os, const *_Quoted_string*<
_String, _CharT > &__str)\\
Inserter for quoted strings.

template<typename _CharT , typename _Traits > *std::basic_ostream*<
_CharT, _Traits > & *operator<<* (*std::basic_ostream*< _CharT, _Traits
> &__os, const *_Quoted_string*< const _CharT *, _CharT > &__str)\\
Inserter for quoted strings.

template<typename _CharT , typename _Traits , typename _Alloc >
*std::basic_istream*< _CharT, _Traits > & *operator>>*
(*std::basic_istream*< _CharT, _Traits > &__is, const *_Quoted_string*<
*basic_string*< _CharT, _Traits, _Alloc > &, _CharT > &__str)\\
Extractor for delimited strings. The left and right delimiters can be
different.

** Variables
constexpr auto *__atomic_spin_count_1*\\

constexpr auto *__atomic_spin_count_2*\\

static constexpr size_t *__platform_wait_alignment*\\

static const _StateIdT *_S_invalid_state_id*\\

* Detailed Description
Implementation details not part of the namespace std interface.

* Typedef Documentation
** using std::__detail::__bare_timed_wait = typedef
__timed_waiter<*std::false_type*>
Definition at line *376* of file *atomic_timed_wait.h*.

** using std::__detail::__bare_wait = typedef
__waiter<*std::false_type*>
Definition at line *409* of file *atomic_wait.h*.

** using std::__detail::__enters_timed_wait = typedef
__timed_waiter<*std::true_type*>
Definition at line *375* of file *atomic_timed_wait.h*.

** using std::__detail::__enters_wait = typedef
__waiter<*std::true_type*>
Definition at line *408* of file *atomic_wait.h*.

** template<typename _Tp > using
std::__detail::__integer_from_chars_result_type = typedef
*enable_if_t*<__or_<__is_signed_integer<_Tp>,
__is_unsigned_integer<_Tp>, *is_same*<char, *remove_cv_t*<_Tp>
>>::value, *from_chars_result*>
Definition at line *585* of file *charconv*.

** template<typename _Tp > using
std::__detail::__integer_to_chars_result_type = typedef
*enable_if_t*<__or_<__is_signed_integer<_Tp>,
__is_unsigned_integer<_Tp>, *is_same*<char, *remove_cv_t*<_Tp>
>>::value, *to_chars_result*>
Definition at line *82* of file *charconv*.

** using std::__detail::__platform_wait_t = typedef uint64_t
Definition at line *62* of file *atomic_wait.h*.

** template<typename _Tp > using std::__detail::__unsigned_least_t =
typedef typename __to_chars_unsigned_type<_Tp>::type
Definition at line *105* of file *charconv*.

** using *std::__detail::__wait_clock_t* = typedef
*chrono::steady_clock*
Definition at line *54* of file *atomic_timed_wait.h*.

* Enumeration Type Documentation
** enum class std::__detail::_RegexExecutorPolicy : int= [strong]=
Definition at line *45* of file *regex.h*.

* Function Documentation
** template<typename _Up , typename _Tp > constexpr _Up
std::__detail::__absu (_Tp __val)= [constexpr]=
Definition at line *100* of file *std/numeric*.

** template<typename _Tp > bool std::__detail::__atomic_compare (const
_Tp & __a, const _Tp & __b)
Definition at line *185* of file *atomic_wait.h*.

** template<typename _Pred , typename _Spin = __default_spin_policy>
bool std::__detail::__atomic_spin (_Pred & __pred, _Spin __spin =
=_Spin{ }=)= [noexcept]=
Definition at line *159* of file *atomic_wait.h*.

** template<typename _TraitsT , typename _FwdIter >
__disable_if_contiguous_iter< _FwdIter, _TraitsT >
std::__detail::__compile_nfa (_FwdIter __first, _FwdIter __last, const
typename _TraitsT::locale_type & __loc,
*regex_constants::syntax_option_type* __flags)= [inline]=
Definition at line *188* of file *regex_compiler.h*.

** template<typename _Clock , typename _Dur > bool
std::__detail::__cond_wait_until (__condvar & __cv, *mutex* & __mx,
const *chrono::time_point*< _Clock, _Dur > & __atime)
Definition at line *173* of file *atomic_timed_wait.h*.

** template<typename _Clock , typename _Dur > bool
std::__detail::__cond_wait_until_impl (__condvar & __cv, *mutex* & __mx,
const *chrono::time_point*< _Clock, _Dur > & __atime)
Definition at line *146* of file *atomic_timed_wait.h*.

** template<typename _Container , typename _Predicate >
_Container::size_type std::__detail::__erase_nodes_if (_Container &
__cont, _Predicate __pred)
Definition at line *51* of file *erase_if.h*.

** template<typename _ValT , typename _CharT , typename _Traits >
*basic_istream*< _CharT, _Traits > & std::__detail::__extract_params
(*basic_istream*< _CharT, _Traits > & __is, *vector*< _ValT > & __vals,
size_t __n)
Definition at line *2776* of file *bits/random.tcc*.

** template<typename _Tp > bool std::__detail::__from_chars_alnum (const
char *& __first, const char * __last, _Tp & __val, int __base)
std::from_chars implementation for integers in bases 11 to 26.

Definition at line *561* of file *charconv*.

** constexpr unsigned char std::__detail::__from_chars_alpha_to_num
(char __c)= [constexpr]=
Definition at line *472* of file *charconv*.

** template<typename _Tp > bool std::__detail::__from_chars_binary
(const char *& __first, const char * __last, _Tp & __val)
std::from_chars implementation for integers in base 2.

Definition at line *415* of file *charconv*.

** template<typename _Tp > bool std::__detail::__from_chars_digit (const
char *& __first, const char * __last, _Tp & __val, int __base)
std::from_chars implementation for integers in bases 3 to 10.

Definition at line *442* of file *charconv*.

** template<typename _Tp > constexpr _Tp std::__detail::__gcd (_Tp __m,
_Tp __n)= [constexpr]=
Definition at line *113* of file *std/numeric*.

** template<typename _Tp > constexpr _Tp std::__detail::__lcm (_Tp __m,
_Tp __n)= [constexpr]=
Definition at line *149* of file *std/numeric*.

** template<typename _InputIterator , typename _OutputIterator ,
typename _Tp > _OutputIterator std::__detail::__normalize
(_InputIterator __first, _InputIterator __last, _OutputIterator
__result, const _Tp & __factor)
Definition at line *84* of file *bits/random.tcc*.

** template<typename _Tp > constexpr bool
std::__detail::__p1_representable_as_double (_Tp __x)= [constexpr]=,
= [noexcept]=
Definition at line *825* of file *bits/random.tcc*.

** template<typename _Tp > bool std::__detail::__raise_and_add (_Tp &
__val, int __base, unsigned char __c)
Definition at line *404* of file *charconv*.

** template<typename _Tp > constexpr bool
std::__detail::__representable_as_double (_Tp __x)= [constexpr]=,
= [noexcept]=
Definition at line *812* of file *bits/random.tcc*.

** template<typename _Tp > void std::__detail::__return_temporary_buffer
(_Tp * __p, size_t __len)= [inline]=
Definition at line *70* of file *stl_tempbuf.h*.

** void std::__detail::__thread_relax ()= [inline]=, = [noexcept]=
Definition at line *137* of file *atomic_wait.h*.

** void std::__detail::__thread_yield ()= [inline]=, = [noexcept]=
Definition at line *129* of file *atomic_wait.h*.

** template<typename _Tp > *to_chars_result* std::__detail::__to_chars
(char * __first, char * __last, _Tp __val, int __base)= [noexcept]=
Definition at line *121* of file *charconv*.

** template<typename _Tp > __integer_to_chars_result_type< _Tp >
std::__detail::__to_chars_10 (char * __first, char * __last, _Tp
__val)= [inline]=, = [noexcept]=
Definition at line *209* of file *charconv*.

** template<typename _Tp > void std::__detail::__to_chars_10_impl (char
* __first, unsigned __len, _Tp __val)= [noexcept]=
Definition at line *72* of file *charconv.h*.

** template<typename _Tp > __integer_to_chars_result_type< _Tp >
std::__detail::__to_chars_16 (char * __first, char * __last, _Tp
__val)= [noexcept]=
Definition at line *162* of file *charconv*.

** template<typename _Tp > __integer_to_chars_result_type< _Tp >
std::__detail::__to_chars_2 (char * __first, char * __last, _Tp
__val)= [noexcept]=
Definition at line *287* of file *charconv*.

** template<typename _Tp > __integer_to_chars_result_type< _Tp >
std::__detail::__to_chars_8 (char * __first, char * __last, _Tp
__val)= [noexcept]=
Definition at line *233* of file *charconv*.

** template<typename _Tp > constexpr unsigned
std::__detail::__to_chars_len (_Tp __value, int __base =
=10=)= [constexpr]=, = [noexcept]=
Definition at line *47* of file *charconv.h*.

** template<typename _Tp > constexpr unsigned
std::__detail::__to_chars_len_2 (_Tp __value)= [constexpr]=,
= [noexcept]=
Definition at line *115* of file *charconv*.

** template<typename _Dur > __wait_clock_t::time_point
std::__detail::__to_wait_clock (const *chrono::time_point*<
*__wait_clock_t*, _Dur > & __atime)= [noexcept]=
Definition at line *69* of file *atomic_timed_wait.h*.

** template<typename _Clock , typename _Dur > __wait_clock_t::time_point
std::__detail::__to_wait_clock (const *chrono::time_point*< _Clock, _Dur
> & __atime)= [noexcept]=
Definition at line *58* of file *atomic_timed_wait.h*.

** template<typename _Tp > constexpr bool std::__detail::_Power_of_2
(_Tp __x)= [constexpr]=
Definition at line *65* of file *uniform_int_dist.h*.

** template<typename _CharT , typename _Traits , typename _String >
*std::basic_ostream*< _CharT, _Traits > & std::__detail::operator<<
(*std::basic_ostream*< _CharT, _Traits > & __os, const *_Quoted_string*<
_String, _CharT > & __str)
Inserter for quoted strings. _GLIBCXX_RESOLVE_LIB_DEFECTS DR 2344
quoted()'s interaction with padding is unclear

Definition at line *1* of file *quoted_string.h*.

** template<typename _CharT , typename _Traits > *std::basic_ostream*<
_CharT, _Traits > & std::__detail::operator<< (*std::basic_ostream*<
_CharT, _Traits > & __os, const *_Quoted_string*< const _CharT *, _CharT
> & __str)
Inserter for quoted strings. _GLIBCXX_RESOLVE_LIB_DEFECTS DR 2344
quoted()'s interaction with padding is unclear

Definition at line *1* of file *quoted_string.h*.

** template<typename _CharT , typename _Traits , typename _Alloc >
*std::basic_istream*< _CharT, _Traits > & std::__detail::operator>>
(*std::basic_istream*< _CharT, _Traits > & __is, const *_Quoted_string*<
*basic_string*< _CharT, _Traits, _Alloc > &, _CharT > & __str)
Extractor for delimited strings. The left and right delimiters can be
different.

Definition at line *139* of file *quoted_string.h*.

References *std::basic_ios< _CharT, _Traits >::clear()*,
*std::ios_base::flags()*, *std::basic_ios< _CharT, _Traits >::good()*,
*std::ios_base::setf()*, *std::ios_base::skipws*, and
*std::basic_istream< _CharT, _Traits >::unget()*.

* Variable Documentation
** constexpr auto std::__detail::__atomic_spin_count_1= [constexpr]=
Definition at line *146* of file *atomic_wait.h*.

** constexpr auto std::__detail::__atomic_spin_count_2= [constexpr]=
Definition at line *147* of file *atomic_wait.h*.

** constexpr size_t std::__detail::__platform_wait_alignment= [static]=,
= [constexpr]=
Definition at line *63* of file *atomic_wait.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
