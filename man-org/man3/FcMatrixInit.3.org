#+TITLE: Manpages - FcMatrixInit.3
#+DESCRIPTION: Linux manpage for FcMatrixInit.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcMatrixInit - initialize an FcMatrix structure

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcMatrixInit (FcMatrix */matrix/*);*

* DESCRIPTION
*FcMatrixInit* initializes /matrix/ to the identity matrix.
