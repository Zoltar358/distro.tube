#+TITLE: Manpages - auparse_first_record.3
#+DESCRIPTION: Linux manpage for auparse_first_record.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_first_record - reposition record cursor

* SYNOPSIS
*#include <auparse.h>*

int auparse_first_record(auparse_state_t *au);

* DESCRIPTION
auparse_first_record repositions the internal cursors of the parsing
library to point to the first field of the first record in the current
event.

* RETURN VALUE
Returns -1 if an error occurs, 0 if there is no event data, or 1 for
success.

* SEE ALSO
*auparse_next_event*(3), *auparse_next_record*(3).

* AUTHOR
Steve Grubb
