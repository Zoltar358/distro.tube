#+TITLE: Manpages - audit_set_backlog_wait_time.3
#+DESCRIPTION: Linux manpage for audit_set_backlog_wait_time.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_set_backlog_wait_time - Set the audit backlog wait time

* SYNOPSIS
*#include <libaudit.h>*

int audit_set_backlog_wait_time(int fd, uint32_t bwt);

* DESCRIPTION
audit_set_backlog_wait_time sets the time that the kernel will wait
before attempting to send more audit events to be transferred to the
audit daemon when the backlog_limit is reached. This gives the audit
daemon a chance to drain the kernel queue. The default value is 60000 or
60 * HZ setting in the kernel.

* RETURN VALUE
The return value is <= 0 on error, otherwise it is the netlink sequence
id number. This function can have any error that sendto would encounter.

* SEE ALSO
*audit_set_backlog_limit*(3), *audit_open*(3), *auditd*(8),
*auditctl*(8).

* AUTHOR
Steve Grubb
