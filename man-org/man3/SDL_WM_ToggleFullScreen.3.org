#+TITLE: Manpages - SDL_WM_ToggleFullScreen.3
#+DESCRIPTION: Linux manpage for SDL_WM_ToggleFullScreen.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_WM_ToggleFullScreen - Toggles fullscreen mode

* SYNOPSIS
*#include "SDL.h"*

*int SDL_WM_ToggleFullScreen*(*SDL_Surface *surface*);

* DESCRIPTION
Toggles the application between windowed and fullscreen mode, if
supported. (X11 is the only target currently supported, BeOS support is
experimental).

* RETURN VALUE
Returns *0* on failure or *1* on success.
