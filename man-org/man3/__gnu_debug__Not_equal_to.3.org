#+TITLE: Manpages - __gnu_debug__Not_equal_to.3
#+DESCRIPTION: Linux manpage for __gnu_debug__Not_equal_to.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_debug::_Not_equal_to< _Type >

* SYNOPSIS
\\

=#include <safe_sequence.h>=

** Public Member Functions
*_Not_equal_to* (const _Type &__v)\\

bool *operator()* (const _Type &__x) const\\

* Detailed Description
** "template<typename _Type>
\\
class __gnu_debug::_Not_equal_to< _Type >"A simple function object that
returns true if the passed-in value is not equal to the stored value. It
saves typing over using both bind1st and not_equal.

Definition at line *44* of file *safe_sequence.h*.

* Constructor & Destructor Documentation
** template<typename _Type > *__gnu_debug::_Not_equal_to*< _Type
>::*_Not_equal_to* (const _Type & __v)= [inline]=, = [explicit]=
Definition at line *49* of file *safe_sequence.h*.

* Member Function Documentation
** template<typename _Type > bool *__gnu_debug::_Not_equal_to*< _Type
>::operator() (const _Type & __x) const= [inline]=
Definition at line *52* of file *safe_sequence.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
