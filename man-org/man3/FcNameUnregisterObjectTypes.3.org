#+TITLE: Manpages - FcNameUnregisterObjectTypes.3
#+DESCRIPTION: Linux manpage for FcNameUnregisterObjectTypes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcNameUnregisterObjectTypes - Unregister object types

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcNameUnregisterObjectTypes (const FcObjectType */types/*, int
*/ntype/*);*

* DESCRIPTION
Deprecated. Does nothing. Returns FcFalse.
