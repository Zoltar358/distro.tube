#+TITLE: Manpages - queue.3
#+DESCRIPTION: Linux manpage for queue.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about queue.3 is found in manpage for: [[../man7/queue.7][man7/queue.7]]