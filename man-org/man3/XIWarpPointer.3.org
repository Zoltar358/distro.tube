#+TITLE: Manpages - XIWarpPointer.3
#+DESCRIPTION: Linux manpage for XIWarpPointer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XIWarpPointer - move a devices pointer.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput2.h>
#+end_example

#+begin_example
  Bool XIWarpPointer( Display *display,
                      int deviceid,
                      Window src_w,
                      Window dest_w,
                      double src_x,
                      double src_y,
                      int src_width,
                      int src_height,
                      double dest_x,
                      double dest_y);
#+end_example

#+begin_example
  dest_w
         Specifies the destination window or None.
#+end_example

#+begin_example
  dest_x, dest_y
         Specify the x and y coordinates within the destination
         window.
#+end_example

#+begin_example
  deviceid
         Specifies the master pointer device or floating slave
         device to move.
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  src_x, src_y, src_width, src_height
         Specify a rectangle in the source window.
#+end_example

#+begin_example
  src_w
         Specifies the source window or None.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    If dest_w is None, XIWarpPointer moves the pointer by the
    offsets (dest_x, dest_y) relative to the current position of
    the pointer. If dest_w is a window, XIWarpPointer moves the
    pointer to the offsets (dest_x, dest_y) relative to the origin
    of dest_w. However, if src_w is a window, the move only takes
    place if the window src_w contains the pointer and if the
    specified rectangle of src_w contains the pointer.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The src_x and src_y coordinates are relative to the origin of
    src_w. If src_height is zero, it is replaced with the current
    height of src_w minus src_y. If src_width is zero, it is
    replaced with the current width of src_w minus src_x.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    There is seldom any reason for calling this function. The
    pointer should normally be left to the user. If you do use this
    function, however, it generates events just as if the user had
    instantaneously moved the pointer from one position to another.
    Note that you cannot use XIWarpPointer to move the pointer
    outside the confine_to window of an active pointer grab. An
    attempt to do so will only move the pointer as far as the
    closest edge of the confine_to window.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIWarpPointer is identical to XWarpPointer but specifies the
    device explicitly.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIWarpPointer can generate a BadDevice and a BadWindow error.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The device does not
           exist or is not a pointer device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadWindow
           A value for a Window argument does not name a defined
           window.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XWarpPointer(3)
  #+end_example
#+end_quote
