#+TITLE: Manpages - auparse_node_compare.3
#+DESCRIPTION: Linux manpage for auparse_node_compare.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_node_compare - compares node name values

* SYNOPSIS
*#include <auparse.h>*

int auparse_node_compare(au_event_t *e1, au_event_t *e2);

* DESCRIPTION
auparse_node_compare compares the node name values of 2 events.

* RETURN VALUE
Returns -1, 0, or 1 respectively depending on whether e2 is less than,
equal to, or greater than e1. Since this is a string compare, it
probably only matter that they are equal or not equal.

* SEE ALSO
*auparse_get_timestamp*(3).

* AUTHOR
Steve Grubb
