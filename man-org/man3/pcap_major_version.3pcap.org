#+TITLE: Manpages - pcap_major_version.3pcap
#+DESCRIPTION: Linux manpage for pcap_major_version.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_major_version, pcap_minor_version - get the version number of a
savefile

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_major_version(pcap_t *p);
  int pcap_minor_version(pcap_t *p);
#+end_example

* DESCRIPTION
If /p/ refers to a ``savefile'', *pcap_major_version*() returns the
major number of the file format of the ``savefile'' and
*pcap_minor_version*() returns the minor number of the file format of
the ``savefile''. The version number is stored in the ``savefile''; note
that the meaning of its values depends on the type of ``savefile'' (for
example, pcap or pcapng).

If /p/ refers to a live capture, the values returned by
*pcap_major_version*() and *pcap_minor_version*() are not meaningful.

* SEE ALSO
*pcap*(3PCAP)
