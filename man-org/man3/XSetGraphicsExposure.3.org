#+TITLE: Manpages - XSetGraphicsExposure.3
#+DESCRIPTION: Linux manpage for XSetGraphicsExposure.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetGraphicsExposure.3 is found in manpage for: [[../man3/XSetArcMode.3][man3/XSetArcMode.3]]