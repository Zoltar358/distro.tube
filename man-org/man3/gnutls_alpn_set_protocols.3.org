#+TITLE: Manpages - gnutls_alpn_set_protocols.3
#+DESCRIPTION: Linux manpage for gnutls_alpn_set_protocols.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_alpn_set_protocols - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_alpn_set_protocols(gnutls_session_t */session/*, const
gnutls_datum_t * */protocols/*, unsigned */protocols_size/*, unsigned
int */flags/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- const gnutls_datum_t * protocols :: is the protocol names to add.

- unsigned protocols_size :: the number of protocols to add.

- unsigned int flags :: zero or a sequence of *gnutls_alpn_flags_t*

* DESCRIPTION
This function is to be used by both clients and servers, to declare the
supported ALPN protocols, which are used during negotiation with peer.

See *gnutls_alpn_flags_t* description for the documentation of available
flags.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error code is returned.

Since 3.2.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
