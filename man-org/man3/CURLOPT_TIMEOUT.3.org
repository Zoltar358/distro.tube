#+TITLE: Manpages - CURLOPT_TIMEOUT.3
#+DESCRIPTION: Linux manpage for CURLOPT_TIMEOUT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_TIMEOUT - maximum time the transfer is allowed to complete

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TIMEOUT, long timeout);

* DESCRIPTION
Pass a long as parameter containing /timeout/ - the maximum time in
seconds that you allow the libcurl transfer operation to take. Normally,
name lookups can take a considerable time and limiting operations risk
aborting perfectly normal operations. This option may cause libcurl to
use the SIGALRM signal to timeout system calls.

In unix-like systems, this might cause signals to be used unless
/CURLOPT_NOSIGNAL(3)/ is set.

If both /CURLOPT_TIMEOUT(3)/ and /CURLOPT_TIMEOUT_MS(3)/ are set, the
value set last will be used.

Since this option puts a hard limit on how long time a request is
allowed to take, it has limited use in dynamic use cases with varying
transfer times. That is especially apparent when using the multi
interface, which may queue the transfer, and that time is included. You
are advised to explore /CURLOPT_LOW_SPEED_LIMIT(3)/,
/CURLOPT_LOW_SPEED_TIME(3)/ or using /CURLOPT_PROGRESSFUNCTION(3)/ to
implement your own timeout logic.

* DEFAULT
Default timeout is 0 (zero) which means it never times out during
transfer.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* complete within 20 seconds */
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK. Returns CURLE_BAD_FUNCTION_ARGUMENT if set to a
negative value or a value that when converted to milliseconds is too
large.

* SEE ALSO
*CURLOPT_TIMEOUT_MS*(3), *CURLOPT_CONNECTTIMEOUT*(3),
*CURLOPT_LOW_SPEED_LIMIT*(3),
