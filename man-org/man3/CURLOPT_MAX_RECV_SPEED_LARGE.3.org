#+TITLE: Manpages - CURLOPT_MAX_RECV_SPEED_LARGE.3
#+DESCRIPTION: Linux manpage for CURLOPT_MAX_RECV_SPEED_LARGE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_MAX_RECV_SPEED_LARGE - rate limit data download speed

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAX_RECV_SPEED_LARGE,
                            curl_off_t maxspeed);
#+end_example

* DESCRIPTION
Pass a curl_off_t as parameter. If a download exceeds this /maxspeed/
(counted in bytes per second) the transfer will pause to keep the speed
less than or equal to the parameter value. Defaults to unlimited speed.

This is not an exact science. libcurl attempts to keep the average speed
below the given threshold over a period time.

If you set /maxspeed/ to a value lower than /CURLOPT_BUFFERSIZE(3)/,
libcurl might download faster than the set limit initially.

This option does not affect transfer speeds done with FILE:// URLs.

* DEFAULT
0, disabled

* PROTOCOLS
All but file://

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    CURLcode ret;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    /* cap the download speed to 31415 bytes/sec */
    curl_easy_setopt(curl, CURLOPT_MAX_RECV_SPEED_LARGE, (curl_off_t)31415);
    ret = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.15.5

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_MAX_SEND_SPEED_LARGE*(3), *CURLOPT_LOW_SPEED_LIMIT*(3),
