#+TITLE: Manpages - memmem.3
#+DESCRIPTION: Linux manpage for memmem.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
memmem - locate a substring

* SYNOPSIS
#+begin_example
  #define _GNU_SOURCE /* See feature_test_macros(7) */
  #include <string.h>

  void *memmem(const void *haystack, size_t haystacklen,
   const void *needle, size_t needlelen);
#+end_example

* DESCRIPTION
The *memmem*() function finds the start of the first occurrence of the
substring /needle/ of length /needlelen/ in the memory area /haystack/
of length /haystacklen/.

* RETURN VALUE
The *memmem*() function returns a pointer to the beginning of the
substring, or NULL if the substring is not found.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface  | Attribute     | Value   |
| *memmem*() | Thread safety | MT-Safe |

* CONFORMING TO
This function is not specified in POSIX.1, but is present on a number of
other systems.

* BUGS
In glibc 2.0, if /needle/ is empty, *memmem*() returns a pointer to the
last byte of /haystack/. This is fixed in glibc 2.1.

* SEE ALSO
*bstring*(3), *strstr*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
