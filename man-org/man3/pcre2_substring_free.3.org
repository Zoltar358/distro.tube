#+TITLE: Manpages - pcre2_substring_free.3
#+DESCRIPTION: Linux manpage for pcre2_substring_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

*void pcre2_substring_free(PCRE2_UCHAR */buffer/);*

* DESCRIPTION
This is a convenience function for freeing the memory obtained by a
previous call to *pcre2_substring_get_byname()* or
*pcre2_substring_get_bynumber()*. Its only argument is a pointer to the
string. If the argument is NULL, the function does nothing.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
