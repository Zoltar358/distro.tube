#+TITLE: Manpages - FcStrSetEqual.3
#+DESCRIPTION: Linux manpage for FcStrSetEqual.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcStrSetEqual - check sets for equality

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcStrSetEqual (FcStrSet */set_a/*, FcStrSet **/set_b/*);*

* DESCRIPTION
Returns whether /set_a/ contains precisely the same strings as /set_b/.
Ordering of strings within the two sets is not considered.
