#+TITLE: Manpages - std_tr2_bases.3
#+DESCRIPTION: Linux manpage for std_tr2_bases.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::tr2::bases< _Tp > - Sequence abstraction metafunctions for
manipulating a typelist.

* SYNOPSIS
\\

** Public Types
typedef *__reflection_typelist*< __bases(_Tp)... > *type*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::tr2::bases< _Tp >"Sequence abstraction metafunctions for
manipulating a typelist.

Enumerate all the base classes of a class. Form of a typelist.

Definition at line *88* of file *tr2/type_traits*.

* Member Typedef Documentation
** template<typename _Tp > typedef
*__reflection_typelist*<__bases(_Tp)...> *std::tr2::bases*< _Tp
>::*type*
Definition at line *90* of file *tr2/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
