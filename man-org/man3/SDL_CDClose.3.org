#+TITLE: Manpages - SDL_CDClose.3
#+DESCRIPTION: Linux manpage for SDL_CDClose.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDClose - Closes a SDL_CD handle

* SYNOPSIS
*#include "SDL.h"*

*void SDL_CDClose*(*SDL_CD *cdrom*);

* DESCRIPTION
Closes the given *cdrom* handle.

* SEE ALSO
*SDL_CDOpen*, *SDL_CD*
