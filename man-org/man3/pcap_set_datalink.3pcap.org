#+TITLE: Manpages - pcap_set_datalink.3pcap
#+DESCRIPTION: Linux manpage for pcap_set_datalink.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_set_datalink - set the link-layer header type to be used by a
capture device

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_set_datalink(pcap_t *p, int dlt);
#+end_example

* DESCRIPTION
*pcap_set_datalink*() is used to set the current link-layer header type
of the pcap descriptor to the type specified by /dlt/.

* RETURN VALUE
*pcap_set_datalink*() returns *0* on success and *PCAP_ERROR* on
failure. If *PCAP_ERROR* is returned, *pcap_geterr*(3PCAP) or
*pcap_perror*(3PCAP) may be called with /p/ as an argument to fetch or
display the error text.

* SEE ALSO
*pcap*(3PCAP), *pcap_datalink_name_to_val*(3PCAP)
