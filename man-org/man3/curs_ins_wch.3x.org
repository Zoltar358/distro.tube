#+TITLE: Manpages - curs_ins_wch.3x
#+DESCRIPTION: Linux manpage for curs_ins_wch.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*ins_wch*, *mvins_wch*, *mvwins_wch*, *wins_wch* - insert a complex
character and rendition into a window

* SYNOPSIS
#include <curses.h>

*int ins_wch(const cchar_t **/wch/*);*\\
*int wins_wch(WINDOW **/win/*, const cchar_t **/wch/*);*

*int mvins_wch(int */y/*, int */x/*, const cchar_t **/wch/*);*\\
*int mvwins_wch(WINDOW **/win/*, int */y/*, int */x/*, const cchar_t
**/wch/*);*

* DESCRIPTION
These routines, insert the complex character /wch/ with rendition before
the character under the cursor. All characters to the right of the
cursor are moved one space to the right, with the possibility of the
rightmost character on the line being lost. The insertion operation does
not change the cursor position.

* RETURN VALUE
If successful, these functions return *OK*. If not, they return *ERR*.

Functions with a mv prefix first perform a cursor movement using
*wmove*, and return an error if the position is outside the window, or
if the window pointer is null.

* ERRORS
No errors are defined.

* SEE ALSO
*curses*(3X), *curs_insch*(3X).
