#+TITLE: Manpages - XkbSetMap.3
#+DESCRIPTION: Linux manpage for XkbSetMap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbSetMap - Send a complete new set of values for entire components to
the server.

* SYNOPSIS
*Bool XkbSetMap* *( Display **/dpy/* ,* *unsigned int */which/* ,*
*XkbDescPtr */xkb/* );*

* ARGUMENTS
- /dpy/ :: connection to X server

- /which/ :: mask selecting subcomponents to update

- /xkb/ :: description from which new values are taken

* DESCRIPTION
There are two ways to make changes to map components: either change a
local copy of the keyboard map and call /XkbSetMap/ to send the modified
map to the server, or, to reduce network traffic, use an
XkbMapChangesRec structure and call /XkbChangeMap./

Use /XkbSetMap/ to send a complete new set of values for entire
components (for example, all symbols, all actions, and so on) to the
server. The /which/ parameter specifies the components to be sent to the
server, and is a bitwise inclusive OR of the masks listed in Table 1.
The /xkb/ parameter is a pointer to an XkbDescRec structure and contains
the information to be copied to the server. For each bit set in the
/which/ parameter, /XkbSetMap/ takes the corresponding structure values
from the /xkb/ parameter and sends it to the server specified by /dpy/.

If any components specified by /which/ are not present in the /xkb/
parameter, /XkbSetMap/ returns False. Otherwise, it sends the update
request to the server and returns True. /XkbSetMap/ can generate
BadAlloc, BadLength, and BadValue protocol errors.

Key types, symbol maps, and actions are all interrelated; changes in one
require changes in the others. Xkb provides functions to make it easier
to edit these components and handle the interdependencies. Table 1 lists
these helper functions and provides a pointer to where they are defined.

TABLE

The /changed/ field identifies the map components that have changed in
an XkbDescRec structure and may contain any of the bits in Table 1,
which are also shown in Table 2. Every 1 bit in /changed/ also
identifies which other fields in the XkbMapChangesRec structure contain
valid values, as indicated in Table 2. The /min_key_code/ and
/max_key_code/ fields are for reference only; they are ignored on any
requests sent to the server and are always updated by the server
whenever it returns the data for an XkbMapChangesRec.

TABLE

* RETURN VALUES
- True :: The /XkbSetMap/ function returns True all components specified
  by /which/ are present in the /xkb/ parameter.

- False :: The /XkbSetMap/ function returns False if any component
  specified by /which/ is not present in the /xkb/ parameter.

* STRUCTURES
Use the XkbMapChangesRec structure to identify and track partial
modifications to the mapping components and to reduce the amount of
traffic between the server and clients.

#+begin_example

  typedef struct _XkbMapChanges {
      unsigned short   changed;            /* identifies valid components in structure */
      KeyCode          min_key_code;       /* lowest numbered keycode for device */
      KeyCode          max_key_code;       /* highest numbered keycode for device */
      unsigned char    first_type;         /* index of first key type modified */
      unsigned char    num_types;          /* # types modified */
      KeyCode          first_key_sym;      /* first key whose key_sym_map changed */
      unsigned char    num_key_syms;       /* # key_sym_map entries changed */
      KeyCode          first_key_act;      /* first key whose key_acts entry changed */
      unsigned char    num_key_acts;       /* # key_acts entries changed */
      KeyCode          first_key_behavior; /* first key whose behaviors changed */
      unsigned char    num_key_behaviors;  /* # behaviors entries changed */
      KeyCode          first_key_explicit; /* first key whose explicit entry changed */
      unsigned char    num_key_explicit;   /* # explicit entries changed */
      KeyCode          first_modmap_key;   /* first key whose modmap entry changed */
      unsigned char    num_modmap_keys;    /* # modmap entries changed */
      KeyCode          first_vmodmap_key;  /* first key whose vmodmap changed */
      unsigned char    num_vmodmap_keys;   /* # vmodmap entries changed */
      unsigned char    pad1;               /* reserved */
      unsigned short   vmods;              /* mask indicating which vmods changed */
  } XkbMapChangesRec,*XkbMapChangesPtr;
#+end_example

The complete description of an Xkb keyboard is given by an XkbDescRec.
The component structures in the XkbDescRec represent the major Xkb
components.

#+begin_example
  typedef struct {
     struct _XDisplay * display;      /* connection to X server */
     unsigned short     flags;        /* private to Xkb, do not modify */
     unsigned short     device_spec;  /* device of interest */
     KeyCode            min_key_code; /* minimum keycode for device */
     KeyCode            max_key_code; /* maximum keycode for device */
     XkbControlsPtr     ctrls;        /* controls */
     XkbServerMapPtr    server;       /* server keymap */
     XkbClientMapPtr    map;          /* client keymap */
     XkbIndicatorPtr    indicators;   /* indicator map */
     XkbNamesPtr        names;        /* names for all components */
     XkbCompatMapPtr    compat;       /* compatibility map */
     XkbGeometryPtr     geom;         /* physical geometry of keyboard */
  } XkbDescRec, *XkbDescPtr;
#+end_example

The /display/ field points to an X display structure. The /flags field
is private to the library: modifying/ /flags/ may yield unpredictable
results. The /device_spec/ field specifies the device identifier of the
keyboard input device, or XkbUseCoreKeyboard, which specifies the core
keyboard device. The /min_key_code/ and /max_key_code/ fields specify
the least and greatest keycode that can be returned by the keyboard.

Each structure component has a corresponding mask bit that is used in
function calls to indicate that the structure should be manipulated in
some manner, such as allocating it or freeing it. These masks and their
relationships to the fields in the XkbDescRec are shown in Table 3.

TABLE

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadLength* :: The length of a request is shorter or longer than that
  required to minimally contain the arguments

- *BadValue* :: An argument is out of range

* SEE ALSO
*XkbChangeMap*(3), *XkbChangeTypesOfKey*(3), *XkbCopyKeyType*(3),
*XkbCopyKeyTypes*(3), *XkbGetKeyActions*(3), *XkbGetKeyBehaviors*(3),
*XkbGetKeyExplicitComponents*(3), *XkbGetKeyModifierMap*(3),
*XkbGetKeySyms*(3), *XkbGetKeyTypes*(3), *XkbResizeKeyActions*(3),
*XkbResizeKeySyms*(3), *XkbResizeKeyType*(3), *XkbGetVirtualModMap*(3),
*XkbGetVirtualMods*(3)
