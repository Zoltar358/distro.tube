#+TITLE: Manpages - XrmPermStringToQuark.3
#+DESCRIPTION: Linux manpage for XrmPermStringToQuark.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XrmPermStringToQuark.3 is found in manpage for: [[../man3/XrmUniqueQuark.3][man3/XrmUniqueQuark.3]]