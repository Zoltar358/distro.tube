#+TITLE: Manpages - XIGrabEnter.3
#+DESCRIPTION: Linux manpage for XIGrabEnter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XIGrabEnter, XIUngrabEnter, XIGrabFocusIn, XIUngrabFocusIn - grab/ungrab
enter or focus in events.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  int XIGrabEnter( Display *display,
                   int deviceid,
                   Window grab_window,
                   Cursor cursor,
                   int grab_mode,
                   int paired_device_mode,
                   Bool owner_events,
                   XIEventMask *mask,
                   int num_modifiers,
                   XIGrabModifiers *modifiers_inout);
#+end_example

#+begin_example
  int XIUngrabEnter( Display *display,
                     int deviceid,
                     Window grab_window
                     int num_modifiers,
                     XIGrabModifiers *modifiers);
#+end_example

#+begin_example
  int XIGrabFocusIn ( Display *display,
                      int deviceid,
                      Window grab_window
                      int grab_mode,
                      int paired_device_mode,
                      Bool owner_events,
                      XIEventMask *mask,
                      int num_modifiers,
                      XIGrabModifiers *modifiers_inout);
#+end_example

#+begin_example
  int XIUngrabFocusIn( Display *display,
                       int deviceid,
                       Window grab_window
                       int num_modifiers,
                       XIGrabModifiers *modifiers);
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         Specifies the device that is to be grabbed or released
#+end_example

#+begin_example
  num_modifiers
         Number of elements in modifiers or modifiers_inout.
#+end_example

#+begin_example
  modifiers
         Specifies the set of latched and base modifiers or
         XIAnyModifier to ungrab. The data type is for
         consistency with the respective grab request and the
         status code of the XIGrabModifiers struct is ignored.
#+end_example

#+begin_example
  modifiers_inout
         Specifies the set of latched and base modifiers or
         XIAnyModifier to grab. Returns the modifiers that could
         not be grabbed and their status code.
#+end_example

#+begin_example
  grab_window
         Specifies the grab window.
#+end_example

#+begin_example
  owner_events
         Specifies a Boolean value that indicates whether the are
         to be reported as usual or reported with respect to the
         grab window.
#+end_example

#+begin_example
  mask
         Specifies the event mask.
#+end_example

#+begin_example
  grab_mode
         Specifies further processing of events from this device.
         You can pass XIGrabModeSync or XIGrabModeAsync.
#+end_example

#+begin_example
  paired_device_mode
         Specifies further processing of events from the paired
         master device. You can pass XIGrabModeSync or
         XIGrabModeAsync. If deviceid specifies a floating slave
         device, this parameter is ignored.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    XIGrabEnter and XIGrabFocusIn establish a passive grab. The
    modifier device for a enter grab is the paired master device if
    deviceid specifies a master pointer. Otherwise, the modifier
    device is the device specified with deviceid.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    For XIGrabEnter and XIGrabFocusIn, in the future, the device is
    actively grabbed (as for XIGrabDevice, the last-grab time is
    set to the time at which the pointer/focus entered window) if
    the device is not grabbed and the pointer has entered the
    grab_window or the focus has been set to the grab_window when
    the specified modifier keys are logically down on the modifier
    device and no other buttons or modifier keys are logically
    down.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The interpretation of the remaining arguments is as for
    XIGrabDevice. The active grab is terminated automatically when
    the pointer leaves the window or the focus is set to a
    different window (independent of the logical state of the
    modifier keys).
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If the device is an attached slave device, the device is
    automatically detached from the master device when the grab
    activates and reattached to the same master device when the
    grab deactivates. If the master device is removed while the
    device is floating as a result of a grab, the device remains
    floating once the grab deactivates.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    Note that the logical state of a device (as seen by client
    applications) may lag the physical state if device event
    processing is frozen.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    This request overrides all previous grabs by the same client of
    the same type and modifier combinations on the same window. A
    modifiers of XIAnyModifier is equivalent to issuing the grab
    request for all possible modifier combinations (including the
    combination of no modifiers). It is not required that all
    modifiers specified have currently assigned KeyCodes.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If some other client has already issued a XIGrabEnter or
    XIGrabFocusIn  on the same window, a BadAccess error results.
    When using XIAnyModifier, the request fails completely, and a
    XIBadAccess error results (no grabs are established) if there
    is a conflicting grab for any combination. XIGrabEnter and
    XIGrabFocusIn have no effect on an active grab.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    On success, XIGrabEnter and XIGrabFocusIn return 0;
    If one or more modifier combinations could not be grabbed,
    XIGrabEnter and XIGrabFocusIn return the number of failed
    combinations and modifiers_inout contains the failed combinations
    and their respective status codes.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIGrabEnter and XIGrabFocusIn can generate BadDevice,
    BadMatch, BadValue, and BadWindow errors.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIUngrabEnter and XIUngrabFocusIn releases the passive grab on
    the specified window if it was grabbed by this client. A
    modifier of XIAnyModifier is equivalent to issuing the ungrab
    request for all possible modifier combinations, including the
    combination of no modifiers. XIUngrabEnter and XIUngrabFocusIn
    have no effect on an active grab.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XIUngrabEnter and XIUngrabFocusIn can generate BadDevice,
    BadMatch, BadValue and BadWindow errors.
  #+end_example
#+end_quote

* RETURN VALUE

#+begin_quote
  #+begin_example
    XIGrabEnter and XIGrabFocusIn return the number of modifier combination
    that could not establish a passive grab. The modifiers are returned in
    modifiers_inout, along with the respective error for this modifier
    combination. If XIGrabEnter or XIGrabFocusIn return zero, passive grabs
    with all requested modifier combinations were established successfully.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid deviceid was specified.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           This error may occur if XIGrabEnter specified a device
           that has no valuators, or XIGrabFocusIn specified a device
           that has no keys.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls outside the range of values
           accepted by the request. Unless a specific range is
           specified for an argument, the full range defined by the
           arguments type is accepted. Any argument defined as a
           set of alternatives can generate this error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadWindow
           A value for a Window argument does not name a defined
           Window.
  #+end_example
#+end_quote

* BUGS

#+begin_quote
  #+begin_example
    The protocol headers for XI 2.0 did not provide
    XIGrabModeAsync or XIGrabModeSync. Use GrabModeSync and
    GrabModeAsync instead, respectively.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XIAllowEvents(3)
  #+end_example
#+end_quote
