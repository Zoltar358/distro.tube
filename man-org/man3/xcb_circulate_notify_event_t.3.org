#+TITLE: Manpages - xcb_circulate_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_circulate_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_circulate_notify_event_t - NOT YET DOCUMENTED

* SYNOPSIS
*#include <xcb/xproto.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_circulate_notify_event_t {
      uint8_t      response_type;
      uint8_t      pad0;
      uint16_t     sequence;
      xcb_window_t event;
      xcb_window_t window;
      uint8_t      pad1[4];
      uint8_t      place;
      uint8_t      pad2[3];
  } xcb_circulate_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_CIRCULATE_REQUEST/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- event :: Either the restacked window or its parent, depending on
  whether /StructureNotify/ or /SubstructureNotify/ was selected.

- window :: The restacked window.

- place :: 

* DESCRIPTION
* SEE ALSO
*xcb_generic_event_t*(3), *xcb_circulate_window*(3)

* AUTHOR
Generated from xproto.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
