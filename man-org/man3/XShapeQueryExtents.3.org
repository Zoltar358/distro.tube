#+TITLE: Manpages - XShapeQueryExtents.3
#+DESCRIPTION: Linux manpage for XShapeQueryExtents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XShapeQueryExtents.3 is found in manpage for: [[../man3/XShape.3][man3/XShape.3]]