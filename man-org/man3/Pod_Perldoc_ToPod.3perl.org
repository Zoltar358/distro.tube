#+TITLE: Manpages - Pod_Perldoc_ToPod.3perl
#+DESCRIPTION: Linux manpage for Pod_Perldoc_ToPod.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Perldoc::ToPod - let Perldoc render Pod as ... Pod!

* SYNOPSIS
perldoc -opod Some::Modulename

(That's currently the same as the following:)

perldoc -u Some::Modulename

* DESCRIPTION
This is a plug-in class that allows Perldoc to display Pod source as
itself! Pretty Zen, huh?

Currently this class works by just filtering out the non-Pod stuff from
a given input file.

* SEE ALSO
Pod::Perldoc

* COPYRIGHT AND DISCLAIMERS
Copyright (c) 2002 Sean M. Burke. All rights reserved.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

* AUTHOR
Current maintainer: Mark Allen =<mallencpan.org>=

Past contributions from: brian d foy =<bdfoy@cpan.org>= Adriano R.
Ferreira =<ferreira@cpan.org>=, Sean M. Burke =<sburke@cpan.org>=
