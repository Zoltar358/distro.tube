#+TITLE: Manpages - CURLOPT_RTSP_SESSION_ID.3
#+DESCRIPTION: Linux manpage for CURLOPT_RTSP_SESSION_ID.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_RTSP_SESSION_ID - RTSP session ID

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_SESSION_ID, char
*id);

* DESCRIPTION
Pass a char * as a parameter to set the value of the current RTSP
Session ID for the handle. Useful for resuming an in-progress session.
Once this value is set to any non-NULL value, libcurl will return
/CURLE_RTSP_SESSION_ERROR/ if ID received from the server does not
match. If unset (or set to NULL), libcurl will automatically set the ID
the first time the server sets it in a response.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
RTSP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    char *prev_id; /* saved from before somehow */
    curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
    curl_easy_setopt(curl, CURLOPT_RTSP_SESSION_ID, prev_id);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.20.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_RTSP_REQUEST*(3), *CURLOPT_RTSP_STREAM_URI*(3),
