#+TITLE: Manpages - etext.3
#+DESCRIPTION: Linux manpage for etext.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about etext.3 is found in manpage for: [[../man3/end.3][man3/end.3]]