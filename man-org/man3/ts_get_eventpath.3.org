#+TITLE: Manpages - ts_get_eventpath.3
#+DESCRIPTION: Linux manpage for ts_get_eventpath.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_get_eventpath - get the path to the currently opened touchscreen
device file

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  char *ts_get_eventpath(struct tsdev *tsdev);
#+end_example

* DESCRIPTION
*ts_get_eventpath*() This function returns the path to the touchscreen
device file that was opened with ts_open(). In case ts_setup() is used
instead of ts_open() directly, it is often not known to the application
in advance.

* RETURN VALUE
This function returns the path to the touchscreen device file that was
opened with ts_open(). It returns NULL in case of failure.

* SEE ALSO
*ts_setup*(3), *ts_read*(3), *ts.conf*(5)
