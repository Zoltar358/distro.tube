#+TITLE: Manpages - HTTP_Cookies_Microsoft.3pm
#+DESCRIPTION: Linux manpage for HTTP_Cookies_Microsoft.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
HTTP::Cookies::Microsoft - Access to Microsoft cookies files

* VERSION
version 6.10

* SYNOPSIS
use LWP; use HTTP::Cookies::Microsoft; use Win32::TieRegistry(Delimiter
=> "/"); my $cookies_dir = $Registry->
{"CUser/Software/Microsoft/Windows/CurrentVersion/Explorer/Shell
Folders/Cookies"}; $cookie_jar = HTTP::Cookies::Microsoft->new( file =>
"$cookies_dir\\index.dat", delayload => 1, ); my $browser =
LWP::UserAgent->new; $browser->cookie_jar( $cookie_jar );

* DESCRIPTION
This is a subclass of =HTTP::Cookies= which loads Microsoft Internet
Explorer 5.x and 6.x for Windows (MSIE) cookie files.

See the documentation for HTTP::Cookies.

* METHODS
The following methods are provided:

- $cookie_jar = HTTP::Cookies::Microsoft->new; :: The constructor takes
  hash style parameters. In addition to the regular HTTP::Cookies
  parameters, HTTP::Cookies::Microsoft recognizes the following:
  delayload: delay loading of cookie data until a request is actually
  made. This results in faster runtime unless you use most of the
  cookies since only the domains cookie data is loaded on demand.

* CAVEATS
Please note that the code DOESN'T support saving to the MSIE cookie file
format.

* AUTHOR
Johnny Lee <typo_pl@hotmail.com>

* COPYRIGHT
Copyright 2002 Johnny Lee

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

* AUTHOR
Gisle Aas <gisle@activestate.com>

* COPYRIGHT AND LICENSE
This software is copyright (c) 2002 by Gisle Aas.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
