#+TITLE: Manpages - FcCacheNumFont.3
#+DESCRIPTION: Linux manpage for FcCacheNumFont.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCacheNumFont - Returns the number of fonts in cache.

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

int FcCacheNumFont (const FcCache */cache/*);*

* DESCRIPTION
This returns the number of fonts which would be included in the return
from FcCacheCopySet.
