#+TITLE: Manpages - acl_get_tag_type.3
#+DESCRIPTION: Linux manpage for acl_get_tag_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function assigns to the value pointed to by

the tag type for the ACL entry indicated by the argument

The argument

and any other ACL entry descriptors that refer to entries in the same
ACL continue to refer to those entries. The order of all existing
entries in the ACL remain unchanged.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid descriptor for an ACL entry.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
