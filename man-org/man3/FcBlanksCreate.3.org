#+TITLE: Manpages - FcBlanksCreate.3
#+DESCRIPTION: Linux manpage for FcBlanksCreate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcBlanksCreate - Create an FcBlanks

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBlanks * FcBlanksCreate (void*);*

* DESCRIPTION
FcBlanks is deprecated. This function always returns NULL.
