#+TITLE: Manpages - XGetExtensionVersion.3
#+DESCRIPTION: Linux manpage for XGetExtensionVersion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGetExtensionVersion - query the version of the input extension.

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  XExtensionVersion *XGetExtensionVersion( Display *display,
                                           char *name);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  name
         Specifies the extension to be queried. The input
         extension name is defined in the header file XI.h.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The XGetExtensionVersion request is deprecated and should not
    be used in XI2 applications. Clients issuing a XGetExtensionVersion
    request will not be able to use XI2 features.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XGetExtensionVersion request queries the version of the input
    extension, and returns an XExtensionVersion structure. This structure
    contains a major_version and minor_version number which can be compared
    with constants defined in XI.h. Support for additional protocol
    requests added to the input extension after its initial release
    is indicated by a version number corresponding to the added
    requests. Each version contains all the protocol requests
    contained by previous versions.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    You should use XFree to free the XExtensionVersion structure.
  #+end_example
#+end_quote

* STRUCTURES

#+begin_quote
  #+begin_example
    This request returns an XExtensionVersion structure.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    typedef struct {
        int present;
        short major_version;
        short minor_version;
    } XExtensionVersion;
  #+end_example
#+end_quote
