#+TITLE: Manpages - CURLOPT_HSTSWRITEDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_HSTSWRITEDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_HSTSWRITEDATA - pointer passed to the HSTS write callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTSWRITEDATA, void
*pointer);

* DESCRIPTION
Data /pointer/ to pass to the HSTS write function. If you use the
/CURLOPT_HSTSWRITEFUNCTION(3)/ option, this is the pointer you will get
as input in the 4th argument to the callback.

This option does not enable HSTS, you need to use /CURLOPT_HSTS_CTRL(3)/
to do that.

* DEFAULT
NULL

* PROTOCOLS
This feature is only used for HTTP(S) transfer.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  struct MyData this;
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://example.com");

    /* pass pointer that gets passed in to the
       CURLOPT_HSTSWRITEFUNCTION callback */
    curl_easy_setopt(curl, CURLOPT_HSTSWRITEDATA, &this);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.74.0

* RETURN VALUE
This will return CURLE_OK.

* SEE ALSO
*CURLOPT_HSTSWRITEFUNCTION*(3), *CURLOPT_HSTSREADDATA*(3),
*CURLOPT_HSTSREADFUNCTION*(3),
