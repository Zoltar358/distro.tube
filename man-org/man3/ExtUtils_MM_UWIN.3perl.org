#+TITLE: Manpages - ExtUtils_MM_UWIN.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_UWIN.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_UWIN - U/WIN specific subclass of ExtUtils::MM_Unix

* SYNOPSIS
Dont use this module directly. Use ExtUtils::MM and let it choose.

* DESCRIPTION
This is a subclass of ExtUtils::MM_Unix which contains functionality for
the AT&T U/WIN UNIX on Windows environment.

Unless otherwise stated it works just like ExtUtils::MM_Unix.

** Overridden methods
- os_flavor :: In addition to being Unix, we're U/WIN.

- replace_manpage_separator :: 

* AUTHOR
Michael G Schwern <schwern@pobox.com> with code from ExtUtils::MM_Unix

* SEE ALSO
ExtUtils::MM_Win32, ExtUtils::MakeMaker
