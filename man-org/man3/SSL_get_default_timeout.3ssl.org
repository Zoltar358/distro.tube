#+TITLE: Manpages - SSL_get_default_timeout.3ssl
#+DESCRIPTION: Linux manpage for SSL_get_default_timeout.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_get_default_timeout - get default session timeout value

* SYNOPSIS
#include <openssl/ssl.h> long SSL_get_default_timeout(const SSL *ssl);

* DESCRIPTION
*SSL_get_default_timeout()* returns the default timeout value assigned
to SSL_SESSION objects negotiated for the protocol valid for *ssl*.

* NOTES
Whenever a new session is negotiated, it is assigned a timeout value,
after which it will not be accepted for session reuse. If the timeout
value was not explicitly set using *SSL_CTX_set_timeout* (3), the
hardcoded default timeout for the protocol will be used.

*SSL_get_default_timeout()* return this hardcoded value, which is 300
seconds for all currently supported protocols.

* RETURN VALUES
See description.

* SEE ALSO
*ssl* (7), *SSL_CTX_set_session_cache_mode* (3),
*SSL_SESSION_get_time* (3), *SSL_CTX_flush_sessions* (3),
*SSL_get_default_timeout* (3)

* COPYRIGHT
Copyright 2001-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
