#+TITLE: Manpages - curl_mime_init.3
#+DESCRIPTION: Linux manpage for curl_mime_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_mime_init - create a mime handle

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>

  curl_mime *curl_mime_init(CURL *easy_handle);
#+end_example

* DESCRIPTION
/curl_mime_init(3)/ creates a handle to a new empty mime structure
intended to be used with /easy_handle/. This mime structure can be
subsequently filled using the mime API, then attached to /easy_handle/
using option /CURLOPT_MIMEPOST(3)/ within a /curl_easy_setopt(3)/ call.

Using a mime handle is the recommended way to post an HTTP form, format
and send a multi-part e-mail with SMTP or upload such an e-mail to an
IMAP server.

* EXAMPLE
#+begin_example
   CURL *easy = curl_easy_init();
   curl_mime *mime;
   curl_mimepart *part;

   /* Build an HTTP form with a single field named "data", */
   mime = curl_mime_init(easy);
   part = curl_mime_addpart(mime);
   curl_mime_data(part, "This is the field data", CURL_ZERO_TERMINATED);
   curl_mime_name(part, "data");

   /* Post and send it. */
   curl_easy_setopt(easy, CURLOPT_MIMEPOST, mime);
   curl_easy_setopt(easy, CURLOPT_URL, "https://example.com");
   curl_easy_perform(easy);

   /* Clean-up. */
   curl_easy_cleanup(easy);
   curl_mime_free(mime);
#+end_example

* AVAILABILITY
As long as at least one of HTTP, SMTP or IMAP is enabled. Added in
7.56.0.

* RETURN VALUE
A mime struct handle, or NULL upon failure.

* SEE ALSO
*curl_mime_addpart*(3), *curl_mime_free*(3), *CURLOPT_MIMEPOST*(3)
