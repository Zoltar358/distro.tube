#+TITLE: Manpages - curl_share_cleanup.3
#+DESCRIPTION: Linux manpage for curl_share_cleanup.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_share_cleanup - Clean up a shared object

* SYNOPSIS
*#include <curl/curl.h>*

*CURLSHcode curl_share_cleanup(CURLSH **/share_handle/*);*

* DESCRIPTION
This function deletes a shared object. The share handle cannot be used
anymore when this function has been called.

Passing in a NULL pointer in /share_handle/ will make this function
return immediately with no action.

* EXAMPLE
#+begin_example
    CURLSHcode sh
    share = curl_share_init();
    sh = curl_share_setopt(share, CURLSHOPT_SHARE, CURL_LOCK_DATA_CONNECT);
    /* use the share, then ... */
    curl_share_cleanup(share);
#+end_example

* AVAILABILITY
Added in 7.10

* RETURN VALUE
CURLSHE_OK (zero) means that the option was set properly, non-zero means
an error occurred as /<curl/curl.h>/ defines. See the /libcurl-errors.3/
man page for the full list with descriptions. If an error occurs, then
the share object will not be deleted.

* SEE ALSO
*curl_share_init*(3), *curl_share_setopt*(3)
