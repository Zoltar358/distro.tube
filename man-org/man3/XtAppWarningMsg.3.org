#+TITLE: Manpages - XtAppWarningMsg.3
#+DESCRIPTION: Linux manpage for XtAppWarningMsg.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppWarningMsg.3 is found in manpage for: [[../man3/XtAppErrorMsg.3][man3/XtAppErrorMsg.3]]