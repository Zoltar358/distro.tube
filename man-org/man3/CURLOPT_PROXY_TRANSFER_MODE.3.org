#+TITLE: Manpages - CURLOPT_PROXY_TRANSFER_MODE.3
#+DESCRIPTION: Linux manpage for CURLOPT_PROXY_TRANSFER_MODE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PROXY_TRANSFER_MODE - append FTP transfer mode to URL for proxy

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_TRANSFER_MODE,
long enabled);

* DESCRIPTION
Pass a long. If the value is set to 1 (one), it tells libcurl to set the
transfer mode (binary or ASCII) for FTP transfers done via an HTTP
proxy, by appending ;type=a or ;type=i to the URL. Without this setting,
or it being set to 0 (zero, the default), /CURLOPT_TRANSFERTEXT(3)/ has
no effect when doing FTP via a proxy. Beware that not all proxies
support this feature.

* DEFAULT
0, disabled

* PROTOCOLS
FTP over proxy

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/old-server/file.txt");
    curl_easy_setopt(curl, CURLOPT_PROXY, "http://localhost:80");
    curl_easy_setopt(curl, CURLOPT_PROXY_TRANSFER_MODE, 1L);
    curl_easy_setopt(curl, CURLOPT_TRANSFERTEXT, 1L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.18.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
the enabled value is not supported.

* SEE ALSO
*CURLOPT_PROXY*(3), *CURLOPT_HTTPPROXYTUNNEL*(3),
