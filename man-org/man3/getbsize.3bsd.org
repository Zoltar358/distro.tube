#+TITLE: Manpages - getbsize.3bsd
#+DESCRIPTION: Linux manpage for getbsize.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function returns a preferred block size for reporting by system
utilities

and

based on the value of the

environment variable.

may be specified directly in bytes, or in multiples of a kilobyte by
specifying a number followed by ``K'' or ``k'', in multiples of a
megabyte by specifying a number followed by ``M'' or ``m'' or in
multiples of a gigabyte by specifying a number followed by ``G'' or
``g''. Multiples must be integers.

Valid values of

are 512 bytes to 1 gigabyte. Sizes less than 512 bytes are rounded up to
512 bytes, and sizes greater than 1 GB are rounded down to 1 GB. In each
case

produces a warning message.

The

function returns a pointer to a null-terminated string describing the
block size, something like

The memory referenced by

is filled in with the length of the string (not including the
terminating null). The memory referenced by

is filled in with block size, in bytes.

The

function first appeared in
