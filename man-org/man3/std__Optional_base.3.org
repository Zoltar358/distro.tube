#+TITLE: Manpages - std__Optional_base.3
#+DESCRIPTION: Linux manpage for std__Optional_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Optional_base< _Tp, bool, bool > - Class template that provides
copy/move constructors of optional.

* SYNOPSIS
\\

Inherits std::_Optional_base_impl< _Tp, _Dp >.

Inherited by *std::experimental::fundamentals_v1::optional< _Tp
>*= [private]=, and *std::optional< _Tp >*= [private]=.

** Public Member Functions
constexpr *_Optional_base* (*_Optional_base* &&__other)
noexcept(is_nothrow_move_constructible_v< _Tp >)\\

constexpr *_Optional_base* (const *_Optional_base* &__other)\\

template<typename... _Args, *enable_if_t*< is_constructible_v< _Tp,
_Args... >, bool > = false> constexpr *_Optional_base* (in_place_t,
_Args &&... __args)\\

template<typename _Up , typename... _Args, *enable_if_t*<
is_constructible_v< _Tp, *initializer_list*< _Up > &, _Args... >, bool >
= false> constexpr *_Optional_base* (in_place_t, *initializer_list*< _Up
> __il, _Args &&... __args)\\

*_Optional_base* & *operator=* (*_Optional_base* &&)=default\\

*_Optional_base* & *operator=* (const *_Optional_base* &)=default\\

** Public Attributes
_Optional_payload< _Tp > *_M_payload*\\

** Protected Types
using *_Stored_type* = *remove_const_t*< _Tp >\\

** Protected Member Functions
template<typename... _Args> void *_M_construct* (_Args &&... __args)
noexcept(is_nothrow_constructible_v< _Stored_type, _Args... >)\\

void *_M_destruct* () noexcept\\

constexpr const _Tp & *_M_get* () const noexcept\\

constexpr _Tp & *_M_get* () noexcept\\

constexpr bool *_M_is_engaged* () const noexcept\\

constexpr void *_M_reset* () noexcept\\

* Detailed Description
** "template<typename _Tp, bool =
is_trivially_copy_constructible_v<_Tp>, bool =
is_trivially_move_constructible_v<_Tp>>
\\
struct std::_Optional_base< _Tp, bool, bool >"Class template that
provides copy/move constructors of optional.

Such a separate base class template is necessary in order to
conditionally make copy/move constructors trivial.

When the contained value is trivially copy/move constructible, the
copy/move constructors of _Optional_base will invoke the trivial
copy/move constructor of _Optional_payload. Otherwise, they will invoke
_Optional_payload(bool, const _Optional_payload&) or
_Optional_payload(bool, _Optional_payload&&) to initialize the contained
value, if copying/moving an engaged optional.

Whether the other special members are trivial is determined by the
_Optional_payload<_Tp> specialization used for the _M_payload member.

*See also*

#+begin_quote
  optional, _Enable_special_members
#+end_quote

Definition at line *473* of file *std/optional*.

* Member Typedef Documentation
** template<typename _Tp , typename _Dp > using
std::_Optional_base_impl< _Tp, _Dp >::_Stored_type =
*remove_const_t*<_Tp>= [protected]=, = [inherited]=
Definition at line *409* of file *std/optional*.

* Constructor & Destructor Documentation
** template<typename _Tp , bool =
is_trivially_copy_constructible_v<_Tp>, bool =
is_trivially_move_constructible_v<_Tp>> template<typename... _Args,
*enable_if_t*< is_constructible_v< _Tp, _Args... >, bool > = false>
constexpr *std::_Optional_base*< _Tp, bool, bool >::*_Optional_base*
(in_place_t, _Args &&... __args)= [inline]=, = [explicit]=,
= [constexpr]=
Definition at line *483* of file *std/optional*.

** template<typename _Tp , bool =
is_trivially_copy_constructible_v<_Tp>, bool =
is_trivially_move_constructible_v<_Tp>> template<typename _Up ,
typename... _Args, *enable_if_t*< is_constructible_v< _Tp,
*initializer_list*< _Up > &, _Args... >, bool > = false> constexpr
*std::_Optional_base*< _Tp, bool, bool >::*_Optional_base* (in_place_t,
*initializer_list*< _Up > __il, _Args &&... __args)= [inline]=,
= [explicit]=, = [constexpr]=
Definition at line *492* of file *std/optional*.

** template<typename _Tp , bool =
is_trivially_copy_constructible_v<_Tp>, bool =
is_trivially_move_constructible_v<_Tp>> constexpr *std::_Optional_base*<
_Tp, bool, bool >::*_Optional_base* (const *_Optional_base*< _Tp, bool,
bool > & __other)= [inline]=, = [constexpr]=
Definition at line *500* of file *std/optional*.

** template<typename _Tp , bool =
is_trivially_copy_constructible_v<_Tp>, bool =
is_trivially_move_constructible_v<_Tp>> constexpr *std::_Optional_base*<
_Tp, bool, bool >::*_Optional_base* (*_Optional_base*< _Tp, bool, bool >
&& __other)= [inline]=, = [constexpr]=, = [noexcept]=
Definition at line *505* of file *std/optional*.

* Member Function Documentation
** template<typename _Tp , typename _Dp > template<typename... _Args>
void std::_Optional_base_impl< _Tp, _Dp >::_M_construct (_Args &&...
__args)= [inline]=, = [protected]=, = [noexcept]=, = [inherited]=
Definition at line *415* of file *std/optional*.

** template<typename _Tp , typename _Dp > void std::_Optional_base_impl<
_Tp, _Dp >::_M_destruct ()= [inline]=, = [protected]=, = [noexcept]=,
= [inherited]=
Definition at line *425* of file *std/optional*.

** template<typename _Tp , typename _Dp > constexpr const _Tp &
std::_Optional_base_impl< _Tp, _Dp >::_M_get () const= [inline]=,
= [constexpr]=, = [protected]=, = [noexcept]=, = [inherited]=
Definition at line *445* of file *std/optional*.

** template<typename _Tp , typename _Dp > constexpr _Tp &
std::_Optional_base_impl< _Tp, _Dp >::_M_get ()= [inline]=,
= [constexpr]=, = [protected]=, = [noexcept]=, = [inherited]=
Definition at line *438* of file *std/optional*.

** template<typename _Tp , typename _Dp > constexpr bool
std::_Optional_base_impl< _Tp, _Dp >::_M_is_engaged () const= [inline]=,
= [constexpr]=, = [protected]=, = [noexcept]=, = [inherited]=
Definition at line *433* of file *std/optional*.

** template<typename _Tp , typename _Dp > constexpr void
std::_Optional_base_impl< _Tp, _Dp >::_M_reset ()= [inline]=,
= [constexpr]=, = [protected]=, = [noexcept]=, = [inherited]=
Definition at line *430* of file *std/optional*.

* Member Data Documentation
** template<typename _Tp , bool =
is_trivially_copy_constructible_v<_Tp>, bool =
is_trivially_move_constructible_v<_Tp>> _Optional_payload<_Tp>
*std::_Optional_base*< _Tp, bool, bool >::_M_payload
Definition at line *515* of file *std/optional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
