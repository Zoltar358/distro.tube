#+TITLE: Manpages - libalpm_groups.3
#+DESCRIPTION: Linux manpage for libalpm_groups.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libalpm_groups - Groups

- Functions for package groups.

* SYNOPSIS
\\

** Data Structures
struct *alpm_group_t*\\
Package group.

** Functions
*alpm_list_t* * *alpm_find_group_pkgs* (*alpm_list_t* *dbs, const char
*name)\\
Find group members across a list of databases.

* Detailed Description
Functions for package groups.

* Data Structure Documentation
* struct alpm_group_t
Package group.

*Data Fields:*

#+begin_quote
  char * /name/ group name\\

  *alpm_list_t* * /packages/ list of alpm_pkg_t packages\\
#+end_quote

* Function Documentation
** *alpm_list_t* * alpm_find_group_pkgs (*alpm_list_t* * dbs, const char
* name)
Find group members across a list of databases. If a member exists in
several databases, only the first database is used. IgnorePkg is also
handled.

*Parameters*

#+begin_quote
  /dbs/ the list of alpm_db_t *\\
  /name/ the name of the group
#+end_quote

*Returns*

#+begin_quote
  the list of alpm_pkg_t * (caller is responsible for alpm_list_free)
#+end_quote

* Author
Generated automatically by Doxygen for libalpm from the source code.
