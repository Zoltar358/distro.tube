#+TITLE: Manpages - carg.3
#+DESCRIPTION: Linux manpage for carg.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
carg, cargf, cargl - calculate the complex argument

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double carg(double complex z);
  float cargf(float complex z);
  long double cargl(long double complex z);

  Link with -lm.
#+end_example

* DESCRIPTION
These functions calculate the complex argument (also called phase angle)
of /z/, with a branch cut along the negative real axis.

A complex number can be described by two real coordinates. One may use
rectangular coordinates and gets

#+begin_example
      z = x + I * y
#+end_example

where /x = creal(z)/ and /y = cimag(z)/.

Or one may use polar coordinates and gets

#+begin_example
      z = r * cexp(I * a)
#+end_example

where /r = cabs(z)/ is the "radius", the "modulus", the absolute value
of /z/, and /a = carg(z)/ is the "phase angle", the argument of /z/.

One has:

#+begin_example
      tan(carg(z)) = cimag(z) / creal(z)
#+end_example

* RETURN VALUE
The return value is in the range of [-pi,pi].

* VERSIONS
These functions first appeared in glibc in version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *carg*(), *cargf*(), *cargl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*cabs*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
