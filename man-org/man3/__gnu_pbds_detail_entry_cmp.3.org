#+TITLE: Manpages - __gnu_pbds_detail_entry_cmp.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_entry_cmp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::entry_cmp< _VTp, Cmp_Fn, _Alloc, No_Throw > - Entry
compare, primary template.

* SYNOPSIS
\\

* Detailed Description
** "template<typename _VTp, typename Cmp_Fn, typename _Alloc, bool
No_Throw>
\\
struct __gnu_pbds::detail::entry_cmp< _VTp, Cmp_Fn, _Alloc, No_Throw
>"Entry compare, primary template.

Definition at line *50* of file *entry_cmp.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
