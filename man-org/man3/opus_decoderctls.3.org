#+TITLE: Manpages - opus_decoderctls.3
#+DESCRIPTION: Linux manpage for opus_decoderctls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
opus_decoderctls

* SYNOPSIS
\\

** Macros
#define *OPUS_SET_GAIN*(x)\\
Configures decoder gain adjustment.

#define *OPUS_GET_GAIN*(x)\\
Gets the decoder's configured gain adjustment.

#define *OPUS_GET_LAST_PACKET_DURATION*(x)\\
Gets the duration (in samples) of the last packet successfully decoded
or concealed.

#define *OPUS_GET_PITCH*(x)\\
Gets the pitch of the last decoded frame, if available.

* Detailed Description
*See also*

#+begin_quote
  *Generic CTLs*, *Encoder related CTLs*, *Opus Decoder*
#+end_quote

* Macro Definition Documentation
** #define OPUS_GET_GAIN(x)
Gets the decoder's configured gain adjustment.

*See also*

#+begin_quote
  *OPUS_SET_GAIN*
#+end_quote

*Parameters*

#+begin_quote
  /x/ =opus_int32 *=: Amount to scale PCM signal by in Q8 dB units.
#+end_quote

** #define OPUS_GET_LAST_PACKET_DURATION(x)
Gets the duration (in samples) of the last packet successfully decoded
or concealed.

*Parameters*

#+begin_quote
  /x/ =opus_int32 *=: Number of samples (at current sampling rate).
#+end_quote

** #define OPUS_GET_PITCH(x)
Gets the pitch of the last decoded frame, if available. This can be used
for any post-processing algorithm requiring the use of pitch, e.g. time
stretching/shortening. If the last frame was not voiced, or if the pitch
was not coded in the frame, then zero is returned.

This CTL is only implemented for decoder instances.

*Parameters*

#+begin_quote
  /x/ =opus_int32 *=: pitch period at 48 kHz (or 0 if not available)
#+end_quote

** #define OPUS_SET_GAIN(x)
Configures decoder gain adjustment. Scales the decoded output by a
factor specified in Q8 dB units. This has a maximum range of -32768 to
32767 inclusive, and returns OPUS_BAD_ARG otherwise. The default is zero
indicating no adjustment. This setting survives decoder reset.

gain = pow(10, x/(20.0*256))

*Parameters*

#+begin_quote
  /x/ =opus_int32=: Amount to scale PCM signal by in Q8 dB units.
#+end_quote

* Author
Generated automatically by Doxygen for Opus from the source code.
