#+TITLE: Manpages - __gnu_parallel_equal_split_tag.3
#+DESCRIPTION: Linux manpage for __gnu_parallel_equal_split_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::equal_split_tag - Selects the equal splitting variant
for std::find().

* SYNOPSIS
\\

=#include <tags.h>=

Inherits *__gnu_parallel::find_tag*.

* Detailed Description
Selects the equal splitting variant for std::find().

*See also*

#+begin_quote
  _GLIBCXX_FIND_EQUAL_SPLIT
#+end_quote

Definition at line *182* of file *tags.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
