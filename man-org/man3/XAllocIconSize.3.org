#+TITLE: Manpages - XAllocIconSize.3
#+DESCRIPTION: Linux manpage for XAllocIconSize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XAllocIconSize, XSetIconSizes, XGetIconSizes, XIconSize - allocate icon
size structure and set or read a window's WM_ICON_SIZES property

* SYNTAX
XIconSize *XAllocIconSize (void );

int XSetIconSizes ( Display */display/, Window /w/, XIconSize
*/size_list/, int /count/ );

Status XGetIconSizes ( Display */display/, Window /w/, XIconSize
**/size_list_return/, int */count_return/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

- count :: Specifies the number of items in the size list.

- count_return :: Returns the number of items in the size list.

- size_list :: Specifies the size list.

- size_list_return :: Returns the size list.

23. Specifies the window.

* DESCRIPTION
The *XAllocIconSize* function allocates and returns a pointer to a
*XIconSize* structure. Note that all fields in the *XIconSize* structure
are initially set to zero. If insufficient memory is available,
*XAllocIconSize* returns NULL. To free the memory allocated to this
structure, use *XFree*.

The *XSetIconSizes* function is used only by window managers to set the
supported icon sizes.

*XSetIconSizes* can generate *BadAlloc* and *BadWindow* errors.

The *XGetIconSizes* function returns zero if a window manager has not
set icon sizes; otherwise, it return nonzero. *XGetIconSizes* should be
called by an application that wants to find out what icon sizes would be
most appreciated by the window manager under which the application is
running. The application should then use *XSetWMHints* to supply the
window manager with an icon pixmap or window in one of the supported
sizes. To free the data allocated in size_list_return, use *XFree*.

*XGetIconSizes* can generate a *BadWindow* error.

* PROPERTIES
- WM_ICON_SIZES :: The window manager may set this property on the root
  window to specify the icon sizes it supports. The C type of this
  property is *XIconSize*.

* STRUCTURES
The *XIconSize* structure contains:

#+begin_example
  typedef struct {
          int min_width, min_height;
          int max_width, max_height;
          int width_inc, height_inc;
  } XIconSize;
#+end_example

The width_inc and height_inc members define an arithmetic progression of
sizes (minimum to maximum) that represent the supported icon sizes.

* DIAGNOSTICS
- *BadAlloc* :: The server failed to allocate the requested resource or
  server memory.

- *BadWindow* :: A value for a Window argument does not name a defined
  Window.

* SEE ALSO
XAllocClassHint(3), XAllocSizeHints(3), XAllocWMHints(3), XFree(3),
XSetCommand(3), XSetTransientForHint(3), XSetTextProperty(3),
XSetWMClientMachine(3), XSetWMColormapWindows(3), XSetWMIconName(3),
XSetWMName(3), XSetWMProperties(3), XSetWMProtocols(3),
XStringListToTextProperty(3)\\
/Xlib - C Language X Interface/
