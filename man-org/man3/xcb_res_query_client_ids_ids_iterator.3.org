#+TITLE: Manpages - xcb_res_query_client_ids_ids_iterator.3
#+DESCRIPTION: Linux manpage for xcb_res_query_client_ids_ids_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about xcb_res_query_client_ids_ids_iterator.3 is found in manpage for: [[../man3/xcb_res_query_client_ids.3][man3/xcb_res_query_client_ids.3]]