#+TITLE: Manpages - __gnu_cxx_limit_condition_limit_adjustor.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_limit_condition_limit_adjustor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::limit_condition::limit_adjustor - Enter the nth condition.

* SYNOPSIS
\\

=#include <throw_allocator.h>=

Inherits __gnu_cxx::limit_condition::adjustor_base.

** Public Member Functions
*limit_adjustor* (const size_t __l)\\

* Detailed Description
Enter the nth condition.

Definition at line *458* of file *throw_allocator.h*.

* Constructor & Destructor Documentation
** __gnu_cxx::limit_condition::limit_adjustor::limit_adjustor (const
size_t __l)= [inline]=
Definition at line *460* of file *throw_allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
