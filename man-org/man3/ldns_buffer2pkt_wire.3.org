#+TITLE: Manpages - ldns_buffer2pkt_wire.3
#+DESCRIPTION: Linux manpage for ldns_buffer2pkt_wire.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_buffer2pkt_wire - convert buffer/wire format to ldns_pkt

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_status ldns_buffer2pkt_wire(ldns_pkt **packet, const ldns_buffer
*buffer);

* DESCRIPTION
/ldns_buffer2pkt_wire/() converts the data in the ldns_buffer (in wire
format) to a DNS packet. This function will initialize and allocate
memory space for the packet structure.

.br *packet*: pointer to the structure to hold the packet .br *buffer*:
the buffer with the data .br Returns LDNS_STATUS_OK if everything
succeeds, error otherwise

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
