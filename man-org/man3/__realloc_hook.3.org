#+TITLE: Manpages - __realloc_hook.3
#+DESCRIPTION: Linux manpage for __realloc_hook.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about __realloc_hook.3 is found in manpage for: [[../man3/malloc_hook.3][man3/malloc_hook.3]]