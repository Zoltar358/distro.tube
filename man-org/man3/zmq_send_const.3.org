#+TITLE: Manpages - zmq_send_const.3
#+DESCRIPTION: Linux manpage for zmq_send_const.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_send_const - send a constant-memory message part on a socket

* SYNOPSIS
*int zmq_send_const (void */*socket/*, const void */*buf/*, size_t
*/len/*, int */flags/*);*

* DESCRIPTION
The /zmq_send_const()/ function shall queue a message created from the
buffer referenced by the /buf/ and /len/ arguments. The message buffer
is assumed to be constant-memory and will therefore not be copied or
deallocated in any way. The /flags/ argument is a combination of the
flags defined below:

*ZMQ_DONTWAIT*

#+begin_quote
  For socket types (DEALER, PUSH) that block (either with ZMQ_IMMEDIATE
  option set and no peer available, or all peers having full high-water
  mark), specifies that the operation should be performed in
  non-blocking mode. If the message cannot be queued on the /socket/,
  the /zmq_send_const()/ function shall fail with /errno/ set to EAGAIN.
#+end_quote

*ZMQ_SNDMORE*

#+begin_quote
  Specifies that the message being sent is a multi-part message, and
  that further message parts are to follow. Refer to the section
  regarding multi-part messages below for a detailed description.
#+end_quote

#+begin_quote
  \\

  *Note*

  \\

  A successful invocation of /zmq_send_const()/ does not indicate that
  the message has been transmitted to the network, only that it has been
  queued on the /socket/ and 0MQ has assumed responsibility for the
  message.
#+end_quote

** Multi-part messages
A 0MQ message is composed of 1 or more message parts. 0MQ ensures atomic
delivery of messages: peers shall receive either all /message parts/ of
a message or none at all. The total number of message parts is unlimited
except by available memory.

An application that sends multi-part messages must use the /ZMQ_SNDMORE/
flag when sending each message part except the final one.

* RETURN VALUE
The /zmq_send_const()/ function shall return number of bytes in the
message if successful. Otherwise it shall return -1 and set /errno/ to
one of the values defined below.

* ERRORS
*EAGAIN*

#+begin_quote
  Non-blocking mode was requested and the message cannot be sent at the
  moment.
#+end_quote

*ENOTSUP*

#+begin_quote
  The /zmq_send_const()/ operation is not supported by this socket type.
#+end_quote

*EFSM*

#+begin_quote
  The /zmq_send_const()/ operation cannot be performed on this socket at
  the moment due to the socket not being in the appropriate state. This
  error may occur with socket types that switch between several states,
  such as ZMQ_REP. See the /messaging patterns/ section of
  *zmq_socket*(3) for more information.
#+end_quote

*ETERM*

#+begin_quote
  The 0MQ /context/ associated with the specified /socket/ was
  terminated.
#+end_quote

*ENOTSOCK*

#+begin_quote
  The provided /socket/ was invalid.
#+end_quote

*EINTR*

#+begin_quote
  The operation was interrupted by delivery of a signal before the
  message was sent.
#+end_quote

*EHOSTUNREACH*

#+begin_quote
  The message cannot be routed.
#+end_quote

* EXAMPLE
*Sending a multi-part message*.

#+begin_quote
  #+begin_example
    /* Send a multi-part message consisting of three parts to socket */
    rc = zmq_send_const (socket, "ABC", 3, ZMQ_SNDMORE);
    assert (rc == 3);
    rc = zmq_send_const (socket, "DEFGH", 5, ZMQ_SNDMORE);
    assert (rc == 5);
    /* Final part; no more parts to follow */
    rc = zmq_send_const (socket, "JK", 2, 0);
    assert (rc == 2);
  #+end_example
#+end_quote

* SEE ALSO
*zmq_send*(3) *zmq_recv*(3) *zmq_socket*(7) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
