#+TITLE: Manpages - CURLOPT_INTERFACE.3
#+DESCRIPTION: Linux manpage for CURLOPT_INTERFACE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_INTERFACE - source interface for outgoing traffic

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_INTERFACE, char
*interface);

* DESCRIPTION
Pass a char * as parameter. This sets the /interface/ name to use as
outgoing network interface. The name can be an interface name, an IP
address, or a host name.

If the parameter starts with "if!" then it is treated as only as
interface name and no attempt will ever be named to do treat it as an IP
address or to do name resolution on it. If the parameter starts with
"host!" it is treated as either an IP address or a hostname. Hostnames
are resolved synchronously. Using the if! format is highly recommended
when using the multi interfaces to avoid allowing the code to block. If
"if!" is specified but the parameter does not match an existing
interface, CURLE_INTERFACE_FAILED is returned from the libcurl function
used to perform the transfer.

libcurl does not support using network interface names for this option
on Windows.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL, use whatever the TCP stack finds suitable

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

    curl_easy_setopt(curl, CURLOPT_INTERFACE, "eth0");

    ret = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
The "if!" and "host!" syntax was added in 7.24.0.

* RETURN VALUE
Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was
insufficient heap space.

* SEE ALSO
*CURLOPT_SOCKOPTFUNCTION*(3), *CURLOPT_TCP_NODELAY*(3),
