#+TITLE: Manpages - auparse_set_escape_mode.3
#+DESCRIPTION: Linux manpage for auparse_set_escape_mode.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_set_escape_mode - choose escape method

* SYNOPSIS
*#include <auparse.h>*

void auparse_set_escape_mode(auparse_state_t *au, auparse_esc_t mode);

* DESCRIPTION
auparse_set_escape_mode is used to set the escaping method that will be
used to output interpreted text. The choices for the mode variable are:

#+begin_quote
  - *AUPARSE_ESC_RAW* :: No escaping of any kind is done.

  - *AUPARSE_ESC_TTY* :: Escape TTY control characters so that they are
    harmless to display on a terminal. When any control character is
    found, they are displayed as octal numbers. This is the default mode
    that the auparse library is initialized with.

  - *AUPARSE_ESC_SHELL* :: Besides escaping control characters, this
    will escape some characters that can cause problems when used with
    shell scripting. Any escaped control characters are displayed as
    octal numbers. Other escaped characters are proceeded with a
    backslash. The additional characters it escapes are: " ' ` $ \

  - *AUPARSE_ESC_SHELL_QUOTE* :: Similar to /AUPARSE_ESC_SHELL/ but
    expands the character set to include shell operators. Any escaped
    control characters are displayed as octal numbers. Other escaped
    characters are proceeded with a backslash. The additional characters
    it escapes include: ; ' " ` # $ & * ? [ ] < > { } \
#+end_quote

* RETURN VALUE
None

* SEE ALSO
*auparse_interpret_field*(3).

* AUTHOR
Steve Grubb
