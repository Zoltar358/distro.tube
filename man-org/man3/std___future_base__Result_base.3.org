#+TITLE: Manpages - std___future_base__Result_base.3
#+DESCRIPTION: Linux manpage for std___future_base__Result_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__future_base::_Result_base - Base class for results.

* SYNOPSIS
\\

Inherited by *std::__future_base::_Result< _Res >*,
*std::__future_base::_Result< _Res & >*, and
*std::__future_base::_Result< void >*.

** Public Member Functions
*_Result_base* (const *_Result_base* &)=delete\\

virtual void *_M_destroy* ()=0\\

*_Result_base* & *operator=* (const *_Result_base* &)=delete\\

** Public Attributes
*exception_ptr* *_M_error*\\

* Detailed Description
Base class for results.

Definition at line *200* of file *future*.

* Member Data Documentation
** *exception_ptr* std::__future_base::_Result_base::_M_error
Definition at line *202* of file *future*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
