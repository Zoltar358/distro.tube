#+TITLE: Manpages - MIN.3
#+DESCRIPTION: Linux manpage for MIN.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about MIN.3 is found in manpage for: [[../man3/MAX.3][man3/MAX.3]]