#+TITLE: Manpages - XkbFreeKeyboard.3
#+DESCRIPTION: Linux manpage for XkbFreeKeyboard.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFreeKeyboard - Destroys either an entire XkbDescRec or just some of
its members

* SYNOPSIS
*void XkbFreeKeyboard* *( XkbDescPtr */xkb/* ,* *unsigned int
*/which/* ,* *Bool */free_all/* );*

* ARGUMENTS
- /xkb/ :: keyboard description with components to free

- /which/ :: mask selecting components to free

- /free_all/ :: True => free all components and /xkb/

* DESCRIPTION
/XkbFreeKeyboard/ frees the components of /xkb/ specified by /which/ and
sets the corresponding values to NULL. If /free_all / is True,
/XkbFreeKeyboard/ frees every non-NULL component of /xkb/ and then frees
the /xkb/ structure itself.

If kbd is NULL, no operation is performed.

* SEE ALSO
*XkbFreeClientMap*(3), *XkbFreeServerMap*(3), *XkbFreeCompatMap*(3),
*XkbFreeIndicatorMaps*(3), *XkbFreeNames*(3), *XkbFreeGeometry*(3),
*XkbFreeControls*(3)
