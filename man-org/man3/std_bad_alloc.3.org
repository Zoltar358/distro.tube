#+TITLE: Manpages - std_bad_alloc.3
#+DESCRIPTION: Linux manpage for std_bad_alloc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bad_alloc - Exception possibly thrown by =new=.

* SYNOPSIS
\\

Inherits *std::exception*.

Inherited by std::bad_array_new_length.

** Public Member Functions
*bad_alloc* (const *bad_alloc* &)=default\\

*bad_alloc* & *operator=* (const *bad_alloc* &)=default\\

virtual const char * *what* () const throw ()\\

* Detailed Description
Exception possibly thrown by =new=.

=bad_alloc= (or classes derived from it) is used to report allocation
errors from the throwing forms of =new=.\\

Definition at line *55* of file *new*.

* Constructor & Destructor Documentation
** std::bad_alloc::bad_alloc ()= [inline]=
Definition at line *58* of file *new*.

* Member Function Documentation
** virtual const char * std::bad_alloc::what () const= [virtual]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::exception*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
