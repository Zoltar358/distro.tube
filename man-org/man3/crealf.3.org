#+TITLE: Manpages - crealf.3
#+DESCRIPTION: Linux manpage for crealf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about crealf.3 is found in manpage for: [[../man3/creal.3][man3/creal.3]]