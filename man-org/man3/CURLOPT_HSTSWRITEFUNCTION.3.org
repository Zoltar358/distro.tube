#+TITLE: Manpages - CURLOPT_HSTSWRITEFUNCTION.3
#+DESCRIPTION: Linux manpage for CURLOPT_HSTSWRITEFUNCTION.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_HSTSWRITEFUNCTION - write callback for HSTS hosts

* SYNOPSIS
#include <curl/curl.h>

CURLSTScode hstswrite(CURL *easy, struct curl_hstsentry *sts, struct
curl_index *count, void *userp);

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTSWRITEFUNCTION,
hstswrite);

* DESCRIPTION
Pass a pointer to your callback function, as the prototype shows above.

This callback function gets called by libcurl repeatedly to allow the
application to store the in-memory HSTS cache when libcurl is about to
discard it.

Set the /userp/ argument with the /CURLOPT_HSTSWRITEDATA(3)/ option or
it will be NULL.

When the callback is invoked, the /sts/ pointer points to a populated
struct: Read the host name to 'name' (it is 'namelen' bytes long and
null terminated. The 'includeSubDomains' field is non-zero if the entry
matches subdomains. The 'expire' string is a date stamp null-terminated
string using the syntax YYYYMMDD HH:MM:SS.

The callback should return /CURLSTS_OK/ if it succeeded and is prepared
to be called again (for another host) or /CURLSTS_DONE/ if there's
nothing more to do. It can also return /CURLSTS_FAIL/ to signal error.

This option does not enable HSTS, you need to use /CURLOPT_HSTS_CTRL(3)/
to do that.

* DEFAULT
NULL - no callback.

* PROTOCOLS
This feature is only used for HTTP(S) transfer.

* EXAMPLE
#+begin_example
  {
    /* set HSTS read callback */
    curl_easy_setopt(curl, CURLOPT_HSTSWRITEFUNCTION, hstswrite);

    /* pass in suitable argument to the callback */
    curl_easy_setopt(curl, CURLOPT_HSTSWRITEDATA, &hstspreload[0]);

    result = curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.74.0

* RETURN VALUE
This will return CURLE_OK.

* SEE ALSO
*CURLOPT_HSTSWRITEDATA*(3), *CURLOPT_HSTSWRITEFUNCTION*(3),
*CURLOPT_HSTS*(3), *CURLOPT_HSTS_CTRL*(3),
