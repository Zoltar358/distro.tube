#+TITLE: Manpages - __gnu_pbds_basic_branch_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_basic_branch_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::basic_branch_tag - Basic branch structure.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::associative_tag*.

Inherited by *__gnu_pbds::tree_tag*, and *__gnu_pbds::trie_tag*.

* Detailed Description
Basic branch structure.

Definition at line *147* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
