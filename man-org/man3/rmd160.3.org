#+TITLE: Manpages - rmd160.3
#+DESCRIPTION: Linux manpage for rmd160.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The RMD160 functions implement the 160-bit RIPE message digest hash
algorithm (RMD-160). RMD-160 is used to generate a condensed
representation of a message called a message digest. The algorithm takes
a message less than 2^64 bits as input and produces a 160-bit digest
suitable for use as a digital signature.

The RMD160 functions are considered to be more secure than the

and

functions. All share a similar interface.

The

function initializes a RMD160_CTX

for use with

and

The

function adds

of length

to the RMD160_CTX specified by

is called when all data has been added via

and stores a message digest in the

parameter.

The

function can be used to apply padding to the message digest as in

but the current context can still be used with

The

function is used by

to hash 512-bit blocks and forms the core of the algorithm. Most
programs should use the interface provided by

and

instead of calling

directly.

The

function is a front end for

which converts the digest into an

representation of the 160 bit digest in hexadecimal.

The

function calculates the digest for a file and returns the result via

If

is unable to open the file a NULL pointer is returned.

behaves like

but calculates the digest only for that portion of the file starting at

and continuing for

bytes or until end of file is reached, whichever comes first. A zero

can be specified to read until end of file. A negative

or

will be ignored.

The

function calculates the digest of an arbitrary string and returns the
result via

For each of the

and

functions the

parameter should either be a string of at least 41 characters in size or
a NULL pointer. In the latter case, space will be dynamically allocated
via

and should be freed using

when it is no longer needed.

The follow code fragment will calculate the digest for the string "abc"
which is ``0x8eb208f7e05d987a9b044a8e98c6b087f15a0bfc''.

RMD160_CTX rmd; uint8_t results[RMD160_DIGEST_LENGTH]; char *buf; int n;

buf = "abc"; n = strlen(buf); RMD160Init(&rmd); RMD160Update(&rmd,
(uint8_t *)buf, n); RMD160Final(results, &rmd);

/* Print the digest as one long hex value */ printf("0x"); for (n = 0; n
< RMD160_DIGEST_LENGTH; n++) printf("%02x", results[n]); putchar('\n');

Alternately, the helper functions could be used in the following way:

RMD160_CTX rmd; uint8_t output[RMD160_DIGEST_STRING_LENGTH]; char *buf =
"abc";

printf("0x%s\n", RMD160Data(buf, strlen(buf), output));

The RMD-160 functions appeared in

This implementation of RMD-160 was written by Markus Friedl.

The

and

helper functions are derived from code written by Poul-Henning Kamp.

If a message digest is to be copied to a multi-byte type (ie: an array
of five 32-bit integers) it will be necessary to perform byte swapping
on little endian machines such as the i386, alpha, and vax.
