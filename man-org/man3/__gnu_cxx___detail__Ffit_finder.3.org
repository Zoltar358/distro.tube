#+TITLE: Manpages - __gnu_cxx___detail__Ffit_finder.3
#+DESCRIPTION: Linux manpage for __gnu_cxx___detail__Ffit_finder.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::__detail::_Ffit_finder< _Tp > - The class which acts as a
predicate for applying the first-fit memory allocation policy for the
bitmap allocator.

* SYNOPSIS
\\

=#include <bitmap_allocator.h>=

Inherits *std::unary_function< std::pair< _Tp, _Tp >, bool >*.

** Public Types
typedef *std::pair*< _Tp, _Tp > *argument_type*\\
=argument_type= is the type of the argument

typedef bool *result_type*\\
=result_type= is the return type

** Public Member Functions
std::size_t * *_M_get* () const throw ()\\

_Counter_type *_M_offset* () const throw ()\\

bool *operator()* (*_Block_pair* __bp) throw ()\\

* Detailed Description
** "template<typename _Tp>
\\
class __gnu_cxx::__detail::_Ffit_finder< _Tp >"The class which acts as a
predicate for applying the first-fit memory allocation policy for the
bitmap allocator.

Definition at line *329* of file *bitmap_allocator.h*.

* Member Typedef Documentation
** typedef *std::pair*< _Tp, _Tp > *std::unary_function*< *std::pair*<
_Tp, _Tp > , bool >::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef bool *std::unary_function*< *std::pair*< _Tp, _Tp > , bool
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > *__gnu_cxx::__detail::_Ffit_finder*< _Tp
>::*_Ffit_finder* ()= [inline]=
Definition at line *340* of file *bitmap_allocator.h*.

* Member Function Documentation
** template<typename _Tp > std::size_t *
*__gnu_cxx::__detail::_Ffit_finder*< _Tp >::_M_get () const= [inline]=
Definition at line *379* of file *bitmap_allocator.h*.

** template<typename _Tp > _Counter_type
*__gnu_cxx::__detail::_Ffit_finder*< _Tp >::_M_offset ()
const= [inline]=
Definition at line *383* of file *bitmap_allocator.h*.

** template<typename _Tp > bool *__gnu_cxx::__detail::_Ffit_finder*< _Tp
>::operator() (*_Block_pair* __bp)= [inline]=
Definition at line *344* of file *bitmap_allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
