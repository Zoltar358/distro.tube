#+TITLE: Manpages - DoesBackingStore.3
#+DESCRIPTION: Linux manpage for DoesBackingStore.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about DoesBackingStore.3 is found in manpage for: [[../man3/BlackPixelOfScreen.3][man3/BlackPixelOfScreen.3]]