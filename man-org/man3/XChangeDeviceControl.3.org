#+TITLE: Manpages - XChangeDeviceControl.3
#+DESCRIPTION: Linux manpage for XChangeDeviceControl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XChangeDeviceControl.3 is found in manpage for: [[../man3/XGetDeviceControl.3][man3/XGetDeviceControl.3]]