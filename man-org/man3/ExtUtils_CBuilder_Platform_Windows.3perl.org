#+TITLE: Manpages - ExtUtils_CBuilder_Platform_Windows.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_CBuilder_Platform_Windows.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::CBuilder::Platform::Windows - Builder class for Windows
platforms

* DESCRIPTION
This module implements the Windows-specific parts of ExtUtils::CBuilder.
Most of the Windows-specific stuff has to do with compiling and linking
C code. Currently we support the 3 compilers perl itself supports: MSVC,
BCC, and GCC.

This module inherits from =ExtUtils::CBuilder::Base=, so any
functionality not implemented here will be implemented there. The
interfaces are defined by the ExtUtils::CBuilder documentation.

* AUTHOR
Ken Williams <ken@mathforum.org>

Most of the code here was written by Randy W. Sims
<RandyS@ThePierianSpring.org>.

* SEE ALSO
*perl* (1), *ExtUtils::CBuilder* (3), *ExtUtils::MakeMaker* (3)
