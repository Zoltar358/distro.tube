#+TITLE: Manpages - pcre2_get_ovector_count.3
#+DESCRIPTION: Linux manpage for pcre2_get_ovector_count.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  uint32_t pcre2_get_ovector_count(pcre2_match_data *match_data);
#+end_example

* DESCRIPTION
This function returns the number of pairs of offsets in the ovector that
forms part of the given match data block.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
