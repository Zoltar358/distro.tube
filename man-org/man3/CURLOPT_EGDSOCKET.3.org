#+TITLE: Manpages - CURLOPT_EGDSOCKET.3
#+DESCRIPTION: Linux manpage for CURLOPT_EGDSOCKET.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_EGDSOCKET - EGD socket path

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_EGDSOCKET, char *path);

* DESCRIPTION
Pass a char * to the null-terminated path name to the Entropy Gathering
Daemon socket. It will be used to seed the random engine for TLS.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_EGDSOCKET, "/var/egd.socket");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
If built with TLS enabled. Only the OpenSSL backend will use this.

* RETURN VALUE
Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or
CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_RANDOM_FILE*(3),
