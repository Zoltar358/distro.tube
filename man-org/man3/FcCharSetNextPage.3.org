#+TITLE: Manpages - FcCharSetNextPage.3
#+DESCRIPTION: Linux manpage for FcCharSetNextPage.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetNextPage - Continue enumerating charset contents

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar32 FcCharSetNextPage (const FcCharSet */a/*,
FcChar32[FC_CHARSET_MAP_SIZE] */map/*, FcChar32 **/next/*);*

* DESCRIPTION
Builds an array of bits in /map/ marking the Unicode coverage of /a/ for
page containing /*next/ (see the *FcCharSetFirstPage* description for
details). /*next/ is set to contains the base code point for the next
page in /a/. Returns the base of code point for the page, or
FC_CHARSET_DONE if /a/ does not contain /*next/.
