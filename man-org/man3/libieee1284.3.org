#+TITLE: Manpages - libieee1284.3
#+DESCRIPTION: Linux manpage for libieee1284.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libieee1284 - IEEE1284 communications library

* SYNOPSIS
#+begin_example
      #include <ieee1284.h>
      cc files... -lieee1284
#+end_example

* OVERVIEW
The libieee1284 library is a library for accessing parallel port
devices.

The model presented to the user is fairly abstract: a list of parallel
ports with arbitrary names, with functions to access them in various
ways ranging from bit operations to block data transfer in one of the
IEEE 1284 sanctioned protocols.

Although the library resides in user space the speed penalty may not be
as bad as you initially think, since the operating system may well
provide assistance with block data transfer operations; in fact, the
operating system may even use hardware assistance to get the job done.
So, using libieee1284, ECP transfers using DMA are possible.

The normal sequence of events will be that the application

#+begin_quote
  1.

  calls *ieee1284_find_ports* to get a list of available ports
#+end_quote

#+begin_quote
  2.

  then *ieee1284_get_deviceid* to look for a device on each port that it
  is interested in
#+end_quote

#+begin_quote
  3.

  and then *ieee1284_open* to open each port it finds a device it can
  control on.
#+end_quote

#+begin_quote
  4.

  The list of ports returned from *ieee1284_find_ports* can now be
  disposed of using *ieee1284_free_ports*.
#+end_quote

#+begin_quote
  5.

  Then when it wants to control the device, it will call
  *ieee1284_claim* to prevent other drivers from using the port
#+end_quote

#+begin_quote
  6.

  then perhaps do some data transfers
#+end_quote

#+begin_quote
  7.

  and then *ieee1284_release* when it is finished that that particular
  command. This claim-control-release sequence will be repeated each
  time it wants to tell the device to do something.
#+end_quote

#+begin_quote
  8.

  Finally when the application is finished with the device it will call
  *ieee1284_close*.
#+end_quote

Usually a port needs to be claimed before it can be used. This is to
prevent multiple drivers from trampling on each other if they both want
to use the same port. The exception to this rule is the collection of
IEEE 1284 Device IDs, which has an implicit open-claim-release-close
sequence. The reason for this is that it may be possible to collect a
Device ID from the operating system, without bothering the device with
it.

* CONFIGURATION
When *ieee1284_find_ports* is first called, the library will look for a
configuration file, /etc/ieee1284.conf.

Comments begin with a # character and extend to the end of the line.
Everything else is freely-formatted tokens. A non-quoted (or
double-quoted) backslash character \ preserves the literal value of the
next character, and single and double quotes may be used for preserving
white-space. Braces and equals signs are recognised as tokens, unless
quoted or escaped.

The only configuration instruction that is currently recognised is
“disallow method ppdev”, for preventing the use of the Linux ppdev
driver.

* ENVIRONMENT
You can enable debugging output from the library by setting the
environment variable *LIBIEEE1284_DEBUG* to any value.

* FILES
/etc/ieee1284.conf

#+begin_quote
  Configuration file.
#+end_quote

* SEE ALSO
parport(3), parport_list(3), ieee1284_find_ports(3),
ieee1284_free_ports(3), ieee1284_get_deviceid(3), ieee1284_open(3),
ieee1284_close(3), ieee1284_claim(3), ieee1284_release(3),
ieee1284_data(3), ieee1284_status(3), ieee1284_control(3),
ieee1284_negotiation(3), ieee1284_ecp_fwd_to_rev(3),
ieee1284_transfer(3), ieee1284_get_irq_fd(3), ieee1284_set_timeout(3)

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
