#+TITLE: Manpages - XcmsLookupColor.3
#+DESCRIPTION: Linux manpage for XcmsLookupColor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XcmsLookupColor.3 is found in manpage for: [[../man3/XcmsQueryColor.3][man3/XcmsQueryColor.3]]