#+TITLE: Manpages - __gnu_cxx_encoding_char_traits.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_encoding_char_traits.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::encoding_char_traits< _CharT > - encoding_char_traits

* SYNOPSIS
\\

=#include <codecvt_specializations.h>=

Inherits *std::char_traits< _CharT >*.

** Public Types
typedef _CharT *char_type*\\

typedef *_Char_types*< _CharT >::int_type *int_type*\\

typedef *_Char_types*< _CharT >::off_type *off_type*\\

typedef *std::fpos*< state_type > *pos_type*\\

typedef *encoding_state* *state_type*\\

** Static Public Member Functions
static constexpr void *assign* (char_type &__c1, const char_type
&__c2)\\

static constexpr char_type * *assign* (char_type *__s, std::size_t __n,
char_type __a)\\

static constexpr int *compare* (const char_type *__s1, const char_type
*__s2, std::size_t __n)\\

static constexpr char_type * *copy* (char_type *__s1, const char_type
*__s2, std::size_t __n)\\

static constexpr int_type *eof* ()\\

static constexpr bool *eq* (const char_type &__c1, const char_type
&__c2)\\

static constexpr bool *eq_int_type* (const int_type &__c1, const
int_type &__c2)\\

static constexpr const char_type * *find* (const char_type *__s,
std::size_t __n, const char_type &__a)\\

static constexpr std::size_t *length* (const char_type *__s)\\

static constexpr bool *lt* (const char_type &__c1, const char_type
&__c2)\\

static constexpr char_type * *move* (char_type *__s1, const char_type
*__s2, std::size_t __n)\\

static constexpr int_type *not_eof* (const int_type &__c)\\

static constexpr char_type *to_char_type* (const int_type &__c)\\

static constexpr int_type *to_int_type* (const char_type &__c)\\

* Detailed Description
** "template<typename _CharT>
\\
struct __gnu_cxx::encoding_char_traits< _CharT >"encoding_char_traits

Definition at line *211* of file *codecvt_specializations.h*.

* Member Typedef Documentation
** template<typename _CharT > typedef _CharT *__gnu_cxx::char_traits*<
_CharT >::char_type= [inherited]=
Definition at line *92* of file *char_traits.h*.

** template<typename _CharT > typedef *_Char_types*<_CharT>::int_type
*__gnu_cxx::char_traits*< _CharT >::int_type= [inherited]=
Definition at line *93* of file *char_traits.h*.

** template<typename _CharT > typedef *_Char_types*<_CharT>::off_type
*__gnu_cxx::char_traits*< _CharT >::off_type= [inherited]=
Definition at line *95* of file *char_traits.h*.

** template<typename _CharT > typedef *std::fpos*<state_type>
*__gnu_cxx::encoding_char_traits*< _CharT >::*pos_type*
Definition at line *215* of file *codecvt_specializations.h*.

** template<typename _CharT > typedef *encoding_state*
*__gnu_cxx::encoding_char_traits*< _CharT >::state_type
Definition at line *214* of file *codecvt_specializations.h*.

* Member Function Documentation
** template<typename _CharT > static constexpr void
*__gnu_cxx::char_traits*< _CharT >::assign (char_type & __c1, const
char_type & __c2)= [inline]=, = [static]=, = [constexpr]=,
= [inherited]=
Definition at line *102* of file *char_traits.h*.

** template<typename _CharT > constexpr *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::assign (char_type *
__s, std::size_t __n, char_type __a)= [static]=, = [constexpr]=,
= [inherited]=
Definition at line *223* of file *char_traits.h*.

** template<typename _CharT > constexpr int *__gnu_cxx::char_traits*<
_CharT >::compare (const char_type * __s1, const char_type * __s2,
std::size_t __n)= [static]=, = [constexpr]=, = [inherited]=
Definition at line *154* of file *char_traits.h*.

** template<typename _CharT > constexpr *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::copy (char_type *
__s1, const char_type * __s2, std::size_t __n)= [static]=,
= [constexpr]=, = [inherited]=
Definition at line *212* of file *char_traits.h*.

** template<typename _CharT > static constexpr int_type
*__gnu_cxx::char_traits*< _CharT >::eof ()= [inline]=, = [static]=,
= [constexpr]=, = [inherited]=
Definition at line *144* of file *char_traits.h*.

** template<typename _CharT > static constexpr bool
*__gnu_cxx::char_traits*< _CharT >::eq (const char_type & __c1, const
char_type & __c2)= [inline]=, = [static]=, = [constexpr]=,
= [inherited]=
Definition at line *106* of file *char_traits.h*.

** template<typename _CharT > static constexpr bool
*__gnu_cxx::char_traits*< _CharT >::eq_int_type (const int_type & __c1,
const int_type & __c2)= [inline]=, = [static]=, = [constexpr]=,
= [inherited]=
Definition at line *140* of file *char_traits.h*.

** template<typename _CharT > constexpr const *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::find (const char_type
* __s, std::size_t __n, const char_type & __a)= [static]=,
= [constexpr]=, = [inherited]=
Definition at line *178* of file *char_traits.h*.

** template<typename _CharT > constexpr std::size_t
*__gnu_cxx::char_traits*< _CharT >::length (const char_type *
__s)= [static]=, = [constexpr]=, = [inherited]=
Definition at line *167* of file *char_traits.h*.

** template<typename _CharT > static constexpr bool
*__gnu_cxx::char_traits*< _CharT >::lt (const char_type & __c1, const
char_type & __c2)= [inline]=, = [static]=, = [constexpr]=,
= [inherited]=
Definition at line *110* of file *char_traits.h*.

** template<typename _CharT > constexpr *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::move (char_type *
__s1, const char_type * __s2, std::size_t __n)= [static]=,
= [constexpr]=, = [inherited]=
Definition at line *190* of file *char_traits.h*.

** template<typename _CharT > static constexpr int_type
*__gnu_cxx::char_traits*< _CharT >::not_eof (const int_type &
__c)= [inline]=, = [static]=, = [constexpr]=, = [inherited]=
Definition at line *148* of file *char_traits.h*.

** template<typename _CharT > static constexpr char_type
*__gnu_cxx::char_traits*< _CharT >::to_char_type (const int_type &
__c)= [inline]=, = [static]=, = [constexpr]=, = [inherited]=
Definition at line *132* of file *char_traits.h*.

** template<typename _CharT > static constexpr int_type
*__gnu_cxx::char_traits*< _CharT >::to_int_type (const char_type &
__c)= [inline]=, = [static]=, = [constexpr]=, = [inherited]=
Definition at line *136* of file *char_traits.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
