#+TITLE: Manpages - ldns_zone_set_rrs.3
#+DESCRIPTION: Linux manpage for ldns_zone_set_rrs.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_zone_set_rrs, ldns_zone_set_soa - ldns_zone set content

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

void ldns_zone_set_rrs(ldns_zone *z, ldns_rr_list *rrlist);

void ldns_zone_set_soa(ldns_zone *z, ldns_rr *soa);

* DESCRIPTION
/ldns_zone_set_rrs/() Set the zone's contents .br *z*: the zone to put
the new soa in .br *rrlist*: the rrlist to use

/ldns_zone_set_soa/() Set the zone's soa record .br *z*: the zone to put
the new soa in .br *soa*: the soa to set

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_zone/, /ldns_zone_rrs/, /ldns_zone_soa/. And *perldoc Net::DNS*,
*RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
