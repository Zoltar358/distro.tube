#+TITLE: Manpages - gnutls_tpm_get_registered.3
#+DESCRIPTION: Linux manpage for gnutls_tpm_get_registered.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_tpm_get_registered - API function

* SYNOPSIS
*#include <gnutls/tpm.h>*

*int gnutls_tpm_get_registered(gnutls_tpm_key_list_t * */list/*);*

* ARGUMENTS
- gnutls_tpm_key_list_t * list :: a list to store the keys

* DESCRIPTION
This function will get a list of stored keys in the TPM. The uuid of
those keys

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
3.1.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
