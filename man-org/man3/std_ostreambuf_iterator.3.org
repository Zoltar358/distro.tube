#+TITLE: Manpages - std_ostreambuf_iterator.3
#+DESCRIPTION: Linux manpage for std_ostreambuf_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::ostreambuf_iterator< _CharT, _Traits > - Provides output iterator
semantics for streambufs.

* SYNOPSIS
\\

=#include <streambuf_iterator.h>=

Inherits *std::iterator< output_iterator_tag, void, void, void, void >*.

** Public Types
typedef void *difference_type*\\
Distance between iterators is represented as this type.

typedef *output_iterator_tag* *iterator_category*\\
One of the *tag types*.

typedef void *pointer*\\
This type represents a pointer-to-value_type.

typedef void *reference*\\
This type represents a reference-to-value_type.

typedef void *value_type*\\
The type 'pointed to' by the iterator.

\\

typedef _CharT *char_type*\\
Public typedefs.

typedef _Traits *traits_type*\\
Public typedefs.

typedef *basic_streambuf*< _CharT, _Traits > *streambuf_type*\\
Public typedefs.

typedef *basic_ostream*< _CharT, _Traits > *ostream_type*\\
Public typedefs.

** Public Member Functions
*ostreambuf_iterator* (*ostream_type* &__s) noexcept\\
Construct output iterator from ostream.

*ostreambuf_iterator* (*streambuf_type* *__s) noexcept\\
Construct output iterator from streambuf.

*ostreambuf_iterator* & *_M_put* (const _CharT *__ws, *streamsize*
__len)\\

bool *failed* () const noexcept\\
Return true if previous operator=() failed.

*ostreambuf_iterator* & *operator** ()\\
Return *this.

*ostreambuf_iterator* & *operator++* ()\\
Return *this.

*ostreambuf_iterator* & *operator++* (int)\\
Return *this.

*ostreambuf_iterator* & *operator=* (_CharT __c)\\
Write character to streambuf. Calls streambuf.sputc().

** Friends
template<typename _CharT2 > __gnu_cxx::__enable_if< __is_char< _CharT2
>::__value, *ostreambuf_iterator*< _CharT2 > >::__type *copy*
(*istreambuf_iterator*< _CharT2 >, *istreambuf_iterator*< _CharT2 >,
*ostreambuf_iterator*< _CharT2 >)\\

* Detailed Description
** "template<typename _CharT, typename _Traits>
\\
class std::ostreambuf_iterator< _CharT, _Traits >"Provides output
iterator semantics for streambufs.

Definition at line *238* of file *streambuf_iterator.h*.

* Member Typedef Documentation
** template<typename _CharT , typename _Traits > typedef _CharT
*std::ostreambuf_iterator*< _CharT, _Traits >::*char_type*
Public typedefs.

Definition at line *248* of file *streambuf_iterator.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*difference_type*= [inherited]=
Distance between iterators is represented as this type.

Definition at line *134* of file *stl_iterator_base_types.h*.

** typedef *output_iterator_tag* *std::iterator*< *output_iterator_tag*
, void , void , void , void >::*iterator_category*= [inherited]=
One of the *tag types*.

Definition at line *130* of file *stl_iterator_base_types.h*.

** template<typename _CharT , typename _Traits > typedef
*basic_ostream*<_CharT, _Traits> *std::ostreambuf_iterator*< _CharT,
_Traits >::*ostream_type*
Public typedefs.

Definition at line *251* of file *streambuf_iterator.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*pointer*= [inherited]=
This type represents a pointer-to-value_type.

Definition at line *136* of file *stl_iterator_base_types.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*reference*= [inherited]=
This type represents a reference-to-value_type.

Definition at line *138* of file *stl_iterator_base_types.h*.

** template<typename _CharT , typename _Traits > typedef
*basic_streambuf*<_CharT, _Traits> *std::ostreambuf_iterator*< _CharT,
_Traits >::*streambuf_type*
Public typedefs.

Definition at line *250* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > typedef _Traits
*std::ostreambuf_iterator*< _CharT, _Traits >::*traits_type*
Public typedefs.

Definition at line *249* of file *streambuf_iterator.h*.

** typedef void *std::iterator*< *output_iterator_tag* , void , void ,
void , void >::*value_type*= [inherited]=
The type 'pointed to' by the iterator.

Definition at line *132* of file *stl_iterator_base_types.h*.

* Constructor & Destructor Documentation
** template<typename _CharT , typename _Traits >
*std::ostreambuf_iterator*< _CharT, _Traits >::*ostreambuf_iterator*
(*ostream_type* & __s)= [inline]=, = [noexcept]=
Construct output iterator from ostream.

Definition at line *273* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits >
*std::ostreambuf_iterator*< _CharT, _Traits >::*ostreambuf_iterator*
(*streambuf_type* * __s)= [inline]=, = [noexcept]=
Construct output iterator from streambuf.

Definition at line *277* of file *streambuf_iterator.h*.

* Member Function Documentation
** template<typename _CharT , typename _Traits > *ostreambuf_iterator* &
*std::ostreambuf_iterator*< _CharT, _Traits >::_M_put (const _CharT *
__ws, *streamsize* __len)= [inline]=
Definition at line *311* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > bool
*std::ostreambuf_iterator*< _CharT, _Traits >::failed ()
const= [inline]=, = [noexcept]=
Return true if previous operator=() failed.

Definition at line *307* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > *ostreambuf_iterator* &
*std::ostreambuf_iterator*< _CharT, _Traits >::operator* ()= [inline]=
Return *this.

Definition at line *292* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > *ostreambuf_iterator* &
*std::ostreambuf_iterator*< _CharT, _Traits >::operator++ ()= [inline]=
Return *this.

Definition at line *302* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > *ostreambuf_iterator* &
*std::ostreambuf_iterator*< _CharT, _Traits >::operator++
(int)= [inline]=
Return *this.

Definition at line *297* of file *streambuf_iterator.h*.

** template<typename _CharT , typename _Traits > *ostreambuf_iterator* &
*std::ostreambuf_iterator*< _CharT, _Traits >::operator= (_CharT
__c)= [inline]=
Write character to streambuf. Calls streambuf.sputc().

Definition at line *282* of file *streambuf_iterator.h*.

References *std::basic_streambuf< _CharT, _Traits >::sputc()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
