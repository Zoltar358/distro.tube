#+TITLE: Man3 - B
#+DESCRIPTION: Man3 - B
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* B
#+begin_src bash :exports results
readarray -t starts_with_b < <(find . -type f -iname "b*" | sort)

for x in "${starts_with_b[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./B.3perl.org][B.3perl]]                    |
| [[file:./backtrace.3.org][backtrace.3]]                |
| [[file:./backtrace_symbols.3.org][backtrace_symbols.3]]        |
| [[file:./backtrace_symbols_fd.3.org][backtrace_symbols_fd.3]]     |
| [[file:./base.3perl.org][base.3perl]]                 |
| [[file:./basename.3.org][basename.3]]                 |
| [[file:./bcmp.3.org][bcmp.3]]                     |
| [[file:./B_Concise.3perl.org][B_Concise.3perl]]            |
| [[file:./bcopy.3.org][bcopy.3]]                    |
| [[file:./B_Deparse.3perl.org][B_Deparse.3perl]]            |
| [[file:./be16dec.3bsd.org][be16dec.3bsd]]               |
| [[file:./be16enc.3bsd.org][be16enc.3bsd]]               |
| [[file:./be16toh.3.org][be16toh.3]]                  |
| [[file:./be32dec.3bsd.org][be32dec.3bsd]]               |
| [[file:./be32enc.3bsd.org][be32enc.3bsd]]               |
| [[file:./be32toh.3.org][be32toh.3]]                  |
| [[file:./be64dec.3bsd.org][be64dec.3bsd]]               |
| [[file:./be64enc.3bsd.org][be64enc.3bsd]]               |
| [[file:./be64toh.3.org][be64toh.3]]                  |
| [[file:./Benchmark.3perl.org][Benchmark.3perl]]            |
| [[file:./BF_encrypt.3ssl.org][BF_encrypt.3ssl]]            |
| [[file:./bigint.3perl.org][bigint.3perl]]               |
| [[file:./bignum.3perl.org][bignum.3perl]]               |
| [[file:./bigrat.3perl.org][bigrat.3perl]]               |
| [[file:./bindresvport.3.org][bindresvport.3]]             |
| [[file:./bindresvport.3t.org][bindresvport.3t]]            |
| [[file:./bindtextdomain.3.org][bindtextdomain.3]]           |
| [[file:./bind_textdomain_codeset.3.org][bind_textdomain_codeset.3]]  |
| [[file:./BIO_ADDR.3ssl.org][BIO_ADDR.3ssl]]              |
| [[file:./BIO_ADDRINFO.3ssl.org][BIO_ADDRINFO.3ssl]]          |
| [[file:./BIO_connect.3ssl.org][BIO_connect.3ssl]]           |
| [[file:./BIO_ctrl.3ssl.org][BIO_ctrl.3ssl]]              |
| [[file:./BIO_f_base64.3ssl.org][BIO_f_base64.3ssl]]          |
| [[file:./BIO_f_buffer.3ssl.org][BIO_f_buffer.3ssl]]          |
| [[file:./BIO_f_cipher.3ssl.org][BIO_f_cipher.3ssl]]          |
| [[file:./BIO_find_type.3ssl.org][BIO_find_type.3ssl]]         |
| [[file:./BIO_f_md.3ssl.org][BIO_f_md.3ssl]]              |
| [[file:./BIO_f_null.3ssl.org][BIO_f_null.3ssl]]            |
| [[file:./BIO_f_ssl.3ssl.org][BIO_f_ssl.3ssl]]             |
| [[file:./BIO_get_data.3ssl.org][BIO_get_data.3ssl]]          |
| [[file:./BIO_get_ex_new_index.3ssl.org][BIO_get_ex_new_index.3ssl]]  |
| [[file:./BIO_meth_new.3ssl.org][BIO_meth_new.3ssl]]          |
| [[file:./BIO_new.3ssl.org][BIO_new.3ssl]]               |
| [[file:./BIO_new_CMS.3ssl.org][BIO_new_CMS.3ssl]]           |
| [[file:./BIO_parse_hostserv.3ssl.org][BIO_parse_hostserv.3ssl]]    |
| [[file:./BIO_printf.3ssl.org][BIO_printf.3ssl]]            |
| [[file:./BIO_push.3ssl.org][BIO_push.3ssl]]              |
| [[file:./BIO_read.3ssl.org][BIO_read.3ssl]]              |
| [[file:./BIO_s_accept.3ssl.org][BIO_s_accept.3ssl]]          |
| [[file:./BIO_s_bio.3ssl.org][BIO_s_bio.3ssl]]             |
| [[file:./BIO_s_connect.3ssl.org][BIO_s_connect.3ssl]]         |
| [[file:./BIO_set_callback.3ssl.org][BIO_set_callback.3ssl]]      |
| [[file:./BIO_s_fd.3ssl.org][BIO_s_fd.3ssl]]              |
| [[file:./BIO_s_file.3ssl.org][BIO_s_file.3ssl]]            |
| [[file:./BIO_should_retry.3ssl.org][BIO_should_retry.3ssl]]      |
| [[file:./BIO_s_mem.3ssl.org][BIO_s_mem.3ssl]]             |
| [[file:./BIO_s_null.3ssl.org][BIO_s_null.3ssl]]            |
| [[file:./BIO_s_socket.3ssl.org][BIO_s_socket.3ssl]]          |
| [[file:./bit_alloc.3bsd.org][bit_alloc.3bsd]]             |
| [[file:./bit_clear.3bsd.org][bit_clear.3bsd]]             |
| [[file:./bit_decl.3bsd.org][bit_decl.3bsd]]              |
| [[file:./bit_ffc.3bsd.org][bit_ffc.3bsd]]               |
| [[file:./bit_ffs.3bsd.org][bit_ffs.3bsd]]               |
| [[file:./BitmapBitOrder.3.org][BitmapBitOrder.3]]           |
| [[file:./BitmapPad.3.org][BitmapPad.3]]                |
| [[file:./BitmapUnit.3.org][BitmapUnit.3]]               |
| [[file:./bit_nclear.3bsd.org][bit_nclear.3bsd]]            |
| [[file:./bit_nset.3bsd.org][bit_nset.3bsd]]              |
| [[file:./bit_set.3bsd.org][bit_set.3bsd]]               |
| [[file:./bitstring.3bsd.org][bitstring.3bsd]]             |
| [[file:./bitstr_size.3bsd.org][bitstr_size.3bsd]]           |
| [[file:./bit_test.3bsd.org][bit_test.3bsd]]              |
| [[file:./BlackPixel.3.org][BlackPixel.3]]               |
| [[file:./BlackPixelOfScreen.3.org][BlackPixelOfScreen.3]]       |
| [[file:./blib.3perl.org][blib.3perl]]                 |
| [[file:./blkcnt_t.3.org][blkcnt_t.3]]                 |
| [[file:./blksize_t.3.org][blksize_t.3]]                |
| [[file:./BN_add.3ssl.org][BN_add.3ssl]]                |
| [[file:./BN_add_word.3ssl.org][BN_add_word.3ssl]]           |
| [[file:./BN_BLINDING_new.3ssl.org][BN_BLINDING_new.3ssl]]       |
| [[file:./BN_bn2bin.3ssl.org][BN_bn2bin.3ssl]]             |
| [[file:./BN_cmp.3ssl.org][BN_cmp.3ssl]]                |
| [[file:./BN_copy.3ssl.org][BN_copy.3ssl]]               |
| [[file:./BN_CTX_new.3ssl.org][BN_CTX_new.3ssl]]            |
| [[file:./BN_CTX_start.3ssl.org][BN_CTX_start.3ssl]]          |
| [[file:./BN_generate_prime.3ssl.org][BN_generate_prime.3ssl]]     |
| [[file:./BN_mod_inverse.3ssl.org][BN_mod_inverse.3ssl]]        |
| [[file:./BN_mod_mul_montgomery.3ssl.org][BN_mod_mul_montgomery.3ssl]] |
| [[file:./BN_mod_mul_reciprocal.3ssl.org][BN_mod_mul_reciprocal.3ssl]] |
| [[file:./BN_new.3ssl.org][BN_new.3ssl]]                |
| [[file:./BN_num_bytes.3ssl.org][BN_num_bytes.3ssl]]          |
| [[file:./BN_rand.3ssl.org][BN_rand.3ssl]]               |
| [[file:./BN_security_bits.3ssl.org][BN_security_bits.3ssl]]      |
| [[file:./BN_set_bit.3ssl.org][BN_set_bit.3ssl]]            |
| [[file:./BN_swap.3ssl.org][BN_swap.3ssl]]               |
| [[file:./BN_zero.3ssl.org][BN_zero.3ssl]]               |
| [[file:./B_Op_private.3perl.org][B_Op_private.3perl]]         |
| [[file:./bsd_signal.3.org][bsd_signal.3]]               |
| [[file:./bsearch.3.org][bsearch.3]]                  |
| [[file:./B_Showlex.3perl.org][B_Showlex.3perl]]            |
| [[file:./bstring.3.org][bstring.3]]                  |
| [[file:./bswap_16.3.org][bswap_16.3]]                 |
| [[file:./bswap_32.3.org][bswap_32.3]]                 |
| [[file:./bswap.3.org][bswap.3]]                    |
| [[file:./bswap_64.3.org][bswap_64.3]]                 |
| [[file:./B_Terse.3perl.org][B_Terse.3perl]]              |
| [[file:./btowc.3.org][btowc.3]]                    |
| [[file:./btree.3.org][btree.3]]                    |
| [[file:./BUF_MEM_new.3ssl.org][BUF_MEM_new.3ssl]]           |
| [[file:./Bundle_DBI.3pm.org][Bundle_DBI.3pm]]             |
| [[file:./B_Xref.3perl.org][B_Xref.3perl]]               |
| [[file:./byteorder.3bsd.org][byteorder.3bsd]]             |
| [[file:./byteorder.3.org][byteorder.3]]                |
| [[file:./bytes.3perl.org][bytes.3perl]]                |
| [[file:./bzero.3.org][bzero.3]]                    |
