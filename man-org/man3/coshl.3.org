#+TITLE: Manpages - coshl.3
#+DESCRIPTION: Linux manpage for coshl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about coshl.3 is found in manpage for: [[../man3/cosh.3][man3/cosh.3]]