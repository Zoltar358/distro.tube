#+TITLE: Manpages - Pod_Text_Color.3perl
#+DESCRIPTION: Linux manpage for Pod_Text_Color.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Text::Color - Convert POD data to formatted color ASCII text

* SYNOPSIS
use Pod::Text::Color; my $parser = Pod::Text::Color->new (sentence => 0,
width => 78); # Read POD from STDIN and write to STDOUT.
$parser->parse_from_filehandle; # Read POD from file.pod and write to
file.txt. $parser->parse_from_file (file.pod, file.txt);

* DESCRIPTION
Pod::Text::Color is a simple subclass of Pod::Text that highlights
output text using ANSI color escape sequences. Apart from the color, it
in all ways functions like Pod::Text. See Pod::Text for details and
available options.

Term::ANSIColor is used to get colors and therefore must be installed to
use this module.

* BUGS
This is just a basic proof of concept. It should be seriously expanded
to support configurable coloration via options passed to the
constructor, and *pod2text* should be taught about those.

* AUTHOR
Russ Allbery <rra@cpan.org>.

* COPYRIGHT AND LICENSE
Copyright 1999, 2001, 2004, 2006, 2008, 2009, 2018-2019 Russ Allbery
<rra@cpan.org>

This program is free software; you may redistribute it and/or modify it
under the same terms as Perl itself.

* SEE ALSO
Pod::Text, Pod::Simple

The current version of this module is always available from its web site
at <https://www.eyrie.org/~eagle/software/podlators/>. It is also part
of the Perl core distribution as of 5.6.0.
