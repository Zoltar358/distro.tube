#+TITLE: Manpages - __gnu_cxx___per_type_pool_policy.3
#+DESCRIPTION: Linux manpage for __gnu_cxx___per_type_pool_policy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::__per_type_pool_policy< _Tp, _PoolTp, _Thread > - Policy for
individual __pool objects.

* SYNOPSIS
\\

=#include <mt_allocator.h>=

Inherits __gnu_cxx::__per_type_pool_base< _Tp, _PoolTp, _Thread >.

* Detailed Description
** "template<typename _Tp, template< bool > class _PoolTp, bool _Thread>
\\
struct __gnu_cxx::__per_type_pool_policy< _Tp, _PoolTp, _Thread >"Policy
for individual __pool objects.

Definition at line *555* of file *mt_allocator.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
