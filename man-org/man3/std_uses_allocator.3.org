#+TITLE: Manpages - std_uses_allocator.3
#+DESCRIPTION: Linux manpage for std_uses_allocator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::uses_allocator< _Tp, _Alloc > - Declare uses_allocator so it can be
specialized in <queue> etc.

* SYNOPSIS
\\

=#include <uses_allocator.h>=

Inherits *std::integral_constant< _Tp, __v >*.

** Public Types
typedef *integral_constant*< _Tp, __v > *type*\\

typedef _Tp *value_type*\\

** Public Member Functions
constexpr *operator value_type* () const noexcept\\

constexpr value_type *operator()* () const noexcept\\

** Static Public Attributes
static constexpr _Tp *value*\\

* Detailed Description
** "template<typename _Tp, typename _Alloc>
\\
struct std::uses_allocator< _Tp, _Alloc >"Declare uses_allocator so it
can be specialized in <queue> etc.

[allocator.uses.trait]

Definition at line *67* of file *uses_allocator.h*.

* Member Typedef Documentation
** template<typename _Tp , _Tp __v> typedef *integral_constant*<_Tp,
__v> *std::integral_constant*< _Tp, __v >::*type*= [inherited]=
Definition at line *61* of file *std/type_traits*.

** template<typename _Tp , _Tp __v> typedef _Tp
*std::integral_constant*< _Tp, __v >::value_type= [inherited]=
Definition at line *60* of file *std/type_traits*.

* Member Function Documentation
** template<typename _Tp , _Tp __v> constexpr *std::integral_constant*<
_Tp, __v >::operator value_type () const= [inline]=, = [constexpr]=,
= [noexcept]=, = [inherited]=
Definition at line *62* of file *std/type_traits*.

** template<typename _Tp , _Tp __v> constexpr value_type
*std::integral_constant*< _Tp, __v >::operator() () const= [inline]=,
= [constexpr]=, = [noexcept]=, = [inherited]=
Definition at line *67* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
