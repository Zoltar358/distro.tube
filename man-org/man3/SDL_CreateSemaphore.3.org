#+TITLE: Manpages - SDL_CreateSemaphore.3
#+DESCRIPTION: Linux manpage for SDL_CreateSemaphore.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CreateSemaphore - Creates a new semaphore and assigns an initial
value to it.

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*SDL_sem *SDL_CreateSemaphore*(*Uint32 initial_value*);

* DESCRIPTION
*SDL_CreateSemaphore()* creates a new semaphore and initializes it with
the value *initial_value*. Each locking operation on the semaphore by
/SDL_SemWait/, /SDL_SemTryWait/ or /SDL_SemWaitTimeout/ will atomically
decrement the semaphore value. The locking operation will be blocked if
the semaphore value is not positive (greater than zero). Each unlock
operation by /SDL_SemPost/ will atomically increment the semaphore
value.

* RETURN VALUE
Returns a pointer to an initialized semaphore or *NULL* if there was an
error.

* EXAMPLES
#+begin_example
  SDL_sem *my_sem;

  my_sem = SDL_CreateSemaphore(INITIAL_SEM_VALUE);

  if (my_sem == NULL) {
          return CREATE_SEM_FAILED;
  }
#+end_example

* SEE ALSO
*SDL_DestroySemaphore*, *SDL_SemWait*, *SDL_SemTryWait*,
*SDL_SemWaitTimeout*, *SDL_SemPost*, *SDL_SemValue*
