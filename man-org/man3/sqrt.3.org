#+TITLE: Manpages - sqrt.3
#+DESCRIPTION: Linux manpage for sqrt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sqrt, sqrtf, sqrtl - square root function

* SYNOPSIS
#+begin_example
  #include <math.h>

  double sqrt(double x);
  float sqrtf(float x);
  long double sqrtl(long double x);
#+end_example

Link with /-lm/.

#+begin_quote
  Feature Test Macro Requirements for glibc (see
  *feature_test_macros*(7)):
#+end_quote

*sqrtf*(), *sqrtl*():

#+begin_example
      _ISOC99_SOURCE || _POSIX_C_SOURCE >= 200112L
          || /* Since glibc 2.19: */ _DEFAULT_SOURCE
          || /* Glibc <= 2.19: */ _BSD_SOURCE || _SVID_SOURCE
#+end_example

* DESCRIPTION
These functions return the nonnegative square root of /x/.

* RETURN VALUE
On success, these functions return the square root of /x/.

If /x/ is a NaN, a NaN is returned.

If /x/ is +0 (-0), +0 (-0) is returned.

If /x/ is positive infinity, positive infinity is returned.

If /x/ is less than -0, a domain error occurs, and a NaN is returned.

* ERRORS
See *math_error*(7) for information on how to determine whether an error
has occurred when calling these functions.

The following errors can occur:

- Domain error: /x/ less than -0 :: /errno/ is set to *EDOM*. An invalid
  floating-point exception (*FE_INVALID*) is raised.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *sqrt*(), *sqrtf*(), *sqrtl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

The variant returning /double/ also conforms to SVr4, 4.3BSD, C89.

* SEE ALSO
*cbrt*(3), *csqrt*(3), *hypot*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
