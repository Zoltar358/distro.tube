#+TITLE: Manpages - pcap_file.3pcap
#+DESCRIPTION: Linux manpage for pcap_file.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_file - get the standard I/O stream for a savefile being read

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  FILE *pcap_file(pcap_t *p);
#+end_example

* DESCRIPTION
*pcap_file*() returns the standard I/O stream of the ``savefile,'' if a
``savefile'' was opened with *pcap_open_offline*(3PCAP), or *NULL*, if a
network device was opened with *pcap_create*(3PCAP) and
*pcap_activate*(3PCAP), or with *pcap_open_live*(3PCAP).

Note that the Packet Capture library is usually built with large file
support, so the standard I/O stream of the ``savefile'' might refer to a
file larger than 2 gigabytes; applications that use *pcap_file*()
should, if possible, use calls that support large files on the return
value of *pcap_file*() or the value returned by *fileno*(3) when passed
the return value of *pcap_file*().

* SEE ALSO
*pcap*(3PCAP)
