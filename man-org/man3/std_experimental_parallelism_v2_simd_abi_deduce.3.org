#+TITLE: Manpages - std_experimental_parallelism_v2_simd_abi_deduce.3
#+DESCRIPTION: Linux manpage for std_experimental_parallelism_v2_simd_abi_deduce.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::experimental::parallelism_v2::simd_abi::deduce< _Tp, _Np,... >

* SYNOPSIS
\\

=#include <simd.h>=

Inherits std::experimental::parallelism_v2::__deduce_impl< _Tp, _Np,
typename >.

* Detailed Description
** "template<typename _Tp, size_t _Np, typename...>
\\
struct std::experimental::parallelism_v2::simd_abi::deduce< _Tp, _Np,...
>"

*Template Parameters*

#+begin_quote
  /_Tp/ The requested =value_type= for the elements.\\
  /_Np/ The requested number of elements.\\
  /_Abis/ This parameter is ignored, since this implementation cannot
  make any use of it. Either __a good native ABI is matched and used as
  =type= alias, or the =fixed_size<_Np>= ABI is used, which internally
  is built from the best matching native ABIs.
#+end_quote

Definition at line *2729* of file *simd.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
