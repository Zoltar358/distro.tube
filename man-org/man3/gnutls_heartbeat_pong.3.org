#+TITLE: Manpages - gnutls_heartbeat_pong.3
#+DESCRIPTION: Linux manpage for gnutls_heartbeat_pong.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_heartbeat_pong - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_heartbeat_pong(gnutls_session_t */session/*, unsigned int
*/flags/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- unsigned int flags :: should be zero

* DESCRIPTION
This function replies to a ping by sending a pong to the peer.

* RETURNS
*GNUTLS_E_SUCCESS* on success, otherwise a negative error code.

* SINCE
3.1.2

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
