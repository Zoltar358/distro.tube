#+TITLE: Manpages - termkey_get_fd.3
#+DESCRIPTION: Linux manpage for termkey_get_fd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_get_fd - obtain the file descriptor for the terminal

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  int termkey_get_fd(TermKey *tk);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_get_fd*() returns the file descriptor that the *termkey*(7)
instance is using to read bytes from.

* RETURN VALUE
*termkey_get_fd*() returns the current file descriptor, or -1 if no file
descriptor is associated with this instance.

* SEE ALSO
*termkey_new*(3), *termkey_get_flags*(3), *termkey*(7)
