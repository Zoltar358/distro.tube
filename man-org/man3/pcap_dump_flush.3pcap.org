#+TITLE: Manpages - pcap_dump_flush.3pcap
#+DESCRIPTION: Linux manpage for pcap_dump_flush.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_dump_flush - flush to a savefile packets dumped

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_dump_flush(pcap_dumper_t *p);
#+end_example

* DESCRIPTION
*pcap_dump_flush*() flushes the output buffer to the ``savefile,'' so
that any packets written with *pcap_dump*(3PCAP) but not yet written to
the ``savefile'' will be written.

* RETURN VALUE
*pcap_dump_flush*() returns *0* on success and *PCAP_ERROR* on failure.

* SEE ALSO
*pcap*(3PCAP), *pcap_dump_open*(3PCAP)
