#+TITLE: Manpages - ctanf.3
#+DESCRIPTION: Linux manpage for ctanf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ctanf.3 is found in manpage for: [[../man3/ctan.3][man3/ctan.3]]