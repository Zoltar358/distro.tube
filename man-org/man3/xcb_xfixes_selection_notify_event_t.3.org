#+TITLE: Manpages - xcb_xfixes_selection_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_xfixes_selection_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_xfixes_selection_notify_event_t -

* SYNOPSIS
*#include <xcb/xfixes.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_xfixes_selection_notify_event_t {
      uint8_t         response_type;
      uint8_t         subtype;
      uint16_t        sequence;
      xcb_window_t    window;
      xcb_window_t    owner;
      xcb_atom_t      selection;
      xcb_timestamp_t timestamp;
      xcb_timestamp_t selection_timestamp;
      uint8_t         pad0[8];
  } xcb_xfixes_selection_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_XFIXES_SELECTION_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- subtype :: NOT YET DOCUMENTED.

- window :: NOT YET DOCUMENTED.

- owner :: NOT YET DOCUMENTED.

- selection :: NOT YET DOCUMENTED.

- timestamp :: NOT YET DOCUMENTED.

- selection_timestamp :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xfixes.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
