#+TITLE: Manpages - XkbAllocServerMap.3
#+DESCRIPTION: Linux manpage for XkbAllocServerMap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocServerMap - Allocate and initialize an empty server map
description record

* SYNOPSIS
*Status XkbAllocServerMap* *( XkbDescPtr */xkb/* ,* *unsigned int
*/which/* ,* *unsigned int */count_acts/* );*

* ARGUMENTS
- /- xkb/ :: keyboard description in which to allocate server map

- /- which/ :: mask selecting map components to allocate

- /- count_acts/ :: value of num_acts field in map to be allocated

* DESCRIPTION
/XkbAllocServerMap/ allocates and initializes an empty server map in the
/server/ field of the keyboard description specified by /xkb./ The
/which/ parameter specifies the particular components of the server map
structure to allocate, as specified in Table 1.

TABLE

If the server map of the keyboard description is not NULL and any fields
are already allocated in the server map, /XkbAllocServerMap/ does not
overwrite the existing values. The only exception is with the /acts/
array. If the /count_acts/ parameter is greater than the current
/num_acts/ field of the server map, /XkbAllocServerMap/ resizes the
/acts/ array and resets the /num_acts/ field accordingly.

If /XkbAllocServerMap/ is successful, it returns Success. Otherwise, it
can return either BadMatch or BadAlloc errors.

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadMatch* :: A compatible version of Xkb was not available in the
  server or an argument has correct type and range, but is otherwise
  invalid

- *BadValue* :: An argument is out of range

* NOTES
The /min_key_code/ and /max_key_code/ fields of the /xkb/ parameter must
be legal values. If they are not valid, /XkbAllocServerMap/ returns
BadValue.
