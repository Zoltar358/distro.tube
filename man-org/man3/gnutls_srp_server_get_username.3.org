#+TITLE: Manpages - gnutls_srp_server_get_username.3
#+DESCRIPTION: Linux manpage for gnutls_srp_server_get_username.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_srp_server_get_username - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*const char * gnutls_srp_server_get_username(gnutls_session_t
*/session/*);*

* ARGUMENTS
- gnutls_session_t session :: is a gnutls session

* DESCRIPTION
This function will return the username of the peer. This should only be
called in case of SRP authentication and in case of a server. Returns
NULL in case of an error.

* RETURNS
SRP username of the peer, or NULL in case of error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
