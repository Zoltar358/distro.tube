#+TITLE: Manpages - getwc.3
#+DESCRIPTION: Linux manpage for getwc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getwc.3 is found in manpage for: [[../man3/fgetwc.3][man3/fgetwc.3]]