#+TITLE: Manpages - libssh2_session_get_timeout.3
#+DESCRIPTION: Linux manpage for libssh2_session_get_timeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_session_get_timeout - get the timeout for blocking functions

* SYNOPSIS
#include <libssh2.h>

#+begin_example
  long libssh2_session_get_timeout(LIBSSH2_SESSION *session);
#+end_example

* DESCRIPTION
Returns the *timeout* (in milliseconds) for how long a blocking the
libssh2 function calls may wait until they consider the situation an
error and return LIBSSH2_ERROR_TIMEOUT.

By default libssh2 has no timeout (zero) for blocking functions.

* RETURN VALUE
The value of the timeout setting.

* AVAILABILITY
Added in 1.2.9

* SEE ALSO
*libssh2_session_set_timeout(3)*
