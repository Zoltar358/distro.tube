#+TITLE: Manpages - gnu_get_libc_version.3
#+DESCRIPTION: Linux manpage for gnu_get_libc_version.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnu_get_libc_version, gnu_get_libc_release - get glibc version and
release

* SYNOPSIS
#+begin_example
  #include <gnu/libc-version.h>

  const char *gnu_get_libc_version(void);
  const char *gnu_get_libc_release(void);
#+end_example

* DESCRIPTION
The function *gnu_get_libc_version*() returns a string that identifies
the glibc version available on the system.

The function *gnu_get_libc_release*() returns a string indicates the
release status of the glibc version available on the system. This will
be a string such as /stable/.

* VERSIONS
These functions first appeared in glibc in version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                                          | Attribute     | Value   |
| *gnu_get_libc_version*(), *gnu_get_libc_release*() | Thread safety | MT-Safe |

* CONFORMING TO
These functions are glibc-specific.

* EXAMPLES
When run, the program below will produce output such as the following:

#+begin_example
  $ ./a.out
  GNU libc version: 2.8
  GNU libc release: stable
#+end_example

** Program source
#+begin_example
  #include <gnu/libc-version.h>
  #include <stdlib.h>
  #include <stdio.h>

  int
  main(int argc, char *argv[])
  {
      printf("GNU libc version: %s\n", gnu_get_libc_version());
      printf("GNU libc release: %s\n", gnu_get_libc_release());
      exit(EXIT_SUCCESS);
  }
#+end_example

* SEE ALSO
*confstr*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
