#+TITLE: Manpages - XkbChangeControls.3
#+DESCRIPTION: Linux manpage for XkbChangeControls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbChangeControls - Provides a flexible method for updating the controls
in a server to match those in the changed keyboard description

* SYNOPSIS
*Bool XkbChangeControls* *( Display **/dpy/* ,* *XkbDescPtr */xkb/* ,*
*XkbControlsChangesPtr */changes/* );*

* ARGUMENTS
- /dpy/ :: connection to X server

- /xkb/ :: keyboard description with changed xkb->ctrls

- /changes/ :: which parts of xkb->ctrls have changed

* DESCRIPTION
The XkbControlsChangesRec structure allows applications to track
modifications to an XkbControlsRec structure and thereby reduce the
amount of traffic sent to the server. The same XkbControlsChangesRec
structure may be used in several successive modifications to the same
XkbControlsRec structure, then subsequently used to cause all of the
changes, and only the changes, to be propagated to the server.

The /changed_ctrls/ field is a mask specifying which logical sets of
data in the controls structure have been modified. In this context,
modified means /set/, that is, if a value is set to the same value it
previously contained, it has still been modified, and is noted as
changed. Valid values for /changed_ctrls/ are any combination of the
masks listed in Table 1 that have "ok" in the /changed_ctrls/ column.
Setting a bit implies the corresponding data fields from the "Relevant
XkbControlsRec Data Fields" column in Table 1 have been modified. The
/enabled_ctrls_changes/ field specifies which bits in the
/enabled_ctrls/ field have changed. If the number of keyboard groups has
changed, the /num_groups_changed/ field is set to True.

Table 1 shows the actual values for the individual mask bits used to
select controls for modification and to enable and disable the control.
Note that the same mask bit is used to specify general modifications to
the parameters used to configure the control (which), and to enable and
disable the control (enabled_ctrls). The anomalies in the table (no "ok"
in column) are for controls that have no configurable attributes; and
for controls that are not boolean controls and therefore cannot be
enabled or disabled.

TABLE

Table 2 shows the actual values for the individual mask bits used to
select controls for modification and to enable and disable the control.
Note that the same mask bit is used to specify general modifications to
the parameters used to configure the control (which), and to enable and
disable the control (enabled_ctrls). The anomalies in the table (no "ok"
in column) are for controls that have no configurable attributes; and
for controls that are not boolean controls and therefore cannot be
enabled or disabled.

TABLE

If you have an Xkb description with controls that have been modified and
an XkbControlsChangesRec that describes the changes that have been made,
the /XkbChangeControls/ function provides a flexible method for updating
the controls in a server to match those in the changed keyboard
description.

/XkbChangeControls/ copies any controls fields specified by /changes/
from the keyboard description controls structure, /xkb->ctrls/, to the
server specified by /dpy/.

* STRUCTURES
The XkbControlsChangesRec structure is defined as follows:

#+begin_example

  typedef struct _XkbControlsChanges {
      unsigned int  changed_ctrls;         /* bits indicating changed control data */
      unsigned int  enabled_ctrls_changes; /* bits indicating enabled/disabled controls */
      Bool          num_groups_changed;    /* True if number of keyboard groups changed */
  } XkbControlsChangesRec,*XkbControlsChangesPtr;
#+end_example

* SEE ALSO
XkbChangeControls(3), XkbChangeDeviceInfo(3),
XkbChangeEnabledControls(3), XkbChangeIndicators(3), XkbChangeMap(3),
XkbChangeNames(3), XkbChangeTypesOfKey(3)
