#+TITLE: Manpages - CURLOPT_FILETIME.3
#+DESCRIPTION: Linux manpage for CURLOPT_FILETIME.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_FILETIME - get the modification time of the remote resource

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FILETIME, long gettime);

* DESCRIPTION
Pass a long. If it is 1, libcurl will attempt to get the modification
time of the remote document in this operation. This requires that the
remote server sends the time or replies to a time querying command. The
/curl_easy_getinfo(3)/ function with the /CURLINFO_FILETIME(3)/ argument
can be used after a transfer to extract the received time (if any).

* DEFAULT
0

* PROTOCOLS
HTTP, FTP, SFTP, FILE

* EXAMPLE
#+begin_example
  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url);
    /* Ask for filetime */
    curl_easy_setopt(curl, CURLOPT_FILETIME, 1L);
    res = curl_easy_perform(curl);
    if(CURLE_OK == res) {
      res = curl_easy_getinfo(curl, CURLINFO_FILETIME, &filetime);
      if((CURLE_OK == res) && (filetime >= 0)) {
        time_t file_time = (time_t)filetime;
        printf("filetime %s: %s", filename, ctime(&file_time));
      }
    }
    /* always cleanup */
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Always, for SFTP since 7.49.0

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*curl_easy_getinfo*(3),
