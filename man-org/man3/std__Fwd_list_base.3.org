#+TITLE: Manpages - std__Fwd_list_base.3
#+DESCRIPTION: Linux manpage for std__Fwd_list_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Fwd_list_base< _Tp, _Alloc > - Base class for forward_list.

* SYNOPSIS
\\

=#include <forward_list.h>=

** Public Types
typedef *_Fwd_list_node*< _Tp > *_Node*\\

typedef *_Fwd_list_const_iterator*< _Tp > *const_iterator*\\

typedef *_Fwd_list_iterator*< _Tp > *iterator*\\

** Public Member Functions
*_Fwd_list_base* (*_Fwd_list_base* &&)=default\\

*_Fwd_list_base* (*_Fwd_list_base* &&__lst, _Node_alloc_type &&__a)\\

*_Fwd_list_base* (*_Fwd_list_base* &&__lst, _Node_alloc_type &&__a,
*std::true_type*)\\

*_Fwd_list_base* (_Node_alloc_type &&__a)\\

const _Node_alloc_type & *_M_get_Node_allocator* () const noexcept\\

_Node_alloc_type & *_M_get_Node_allocator* () noexcept\\

** Protected Types
typedef *__gnu_cxx::__alloc_traits*< _Node_alloc_type >
*_Node_alloc_traits*\\

typedef __alloc_rebind< _Alloc, *_Fwd_list_node*< _Tp > >
*_Node_alloc_type*\\

** Protected Member Functions
template<typename... _Args> *_Node* * *_M_create_node* (_Args &&...
__args)\\

*_Fwd_list_node_base* * *_M_erase_after* (*_Fwd_list_node_base*
*__pos)\\

*_Fwd_list_node_base* * *_M_erase_after* (*_Fwd_list_node_base* *__pos,
*_Fwd_list_node_base* *__last)\\

*_Node* * *_M_get_node* ()\\

template<typename... _Args> *_Fwd_list_node_base* * *_M_insert_after*
(*const_iterator* __pos, _Args &&... __args)\\

void *_M_put_node* (*_Node* *__p)\\

** Protected Attributes
_Fwd_list_impl *_M_impl*\\

* Detailed Description
** "template<typename _Tp, typename _Alloc>
\\
struct std::_Fwd_list_base< _Tp, _Alloc >"Base class for forward_list.

Definition at line *287* of file *forward_list.h*.

* Member Typedef Documentation
** template<typename _Tp , typename _Alloc > typedef
*_Fwd_list_node*<_Tp> *std::_Fwd_list_base*< _Tp, _Alloc >::*_Node*
Definition at line *319* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > typedef
*__gnu_cxx::__alloc_traits*<_Node_alloc_type> *std::_Fwd_list_base*<
_Tp, _Alloc >::*_Node_alloc_traits*= [protected]=
Definition at line *291* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > typedef
__alloc_rebind<_Alloc, *_Fwd_list_node*<_Tp> > *std::_Fwd_list_base*<
_Tp, _Alloc >::_Node_alloc_type= [protected]=
Definition at line *290* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > typedef
*_Fwd_list_const_iterator*<_Tp> *std::_Fwd_list_base*< _Tp, _Alloc
>::*const_iterator*
Definition at line *318* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > typedef
*_Fwd_list_iterator*<_Tp> *std::_Fwd_list_base*< _Tp, _Alloc
>::*iterator*
Definition at line *317* of file *forward_list.h*.

* Constructor & Destructor Documentation
** template<typename _Tp , typename _Alloc > *std::_Fwd_list_base*< _Tp,
_Alloc >::*_Fwd_list_base* (_Node_alloc_type && __a)= [inline]=
Definition at line *331* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > *std::_Fwd_list_base*< _Tp,
_Alloc >::*_Fwd_list_base* (*_Fwd_list_base*< _Tp, _Alloc > && __lst,
_Node_alloc_type && __a, *std::true_type*)= [inline]=
Definition at line *335* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > *std::_Fwd_list_base*< _Tp,
_Alloc >::*_Fwd_list_base* (*_Fwd_list_base*< _Tp, _Alloc > && __lst,
_Node_alloc_type && __a)
Definition at line *39* of file *forward_list.tcc*.

** template<typename _Tp , typename _Alloc > *std::_Fwd_list_base*< _Tp,
_Alloc >::~*_Fwd_list_base* ()= [inline]=
Definition at line *345* of file *forward_list.h*.

* Member Function Documentation
** template<typename _Tp , typename _Alloc > template<typename... _Args>
*_Node* * *std::_Fwd_list_base*< _Tp, _Alloc >::_M_create_node (_Args
&&... __args)= [inline]=, = [protected]=
Definition at line *358* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > *_Fwd_list_node_base* *
*std::_Fwd_list_base*< _Tp, _Alloc >::_M_erase_after
(*_Fwd_list_node_base* * __pos)= [protected]=
Definition at line *63* of file *forward_list.tcc*.

** template<typename _Tp , typename _Alloc > *_Fwd_list_node_base* *
*std::_Fwd_list_base*< _Tp, _Alloc >::_M_erase_after
(*_Fwd_list_node_base* * __pos, *_Fwd_list_node_base* *
__last)= [protected]=
Definition at line *77* of file *forward_list.tcc*.

** template<typename _Tp , typename _Alloc > *_Node* *
*std::_Fwd_list_base*< _Tp, _Alloc >::_M_get_node ()= [inline]=,
= [protected]=
Definition at line *350* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > const _Node_alloc_type &
*std::_Fwd_list_base*< _Tp, _Alloc >::_M_get_Node_allocator ()
const= [inline]=, = [noexcept]=
Definition at line *326* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > _Node_alloc_type &
*std::_Fwd_list_base*< _Tp, _Alloc >::_M_get_Node_allocator
()= [inline]=, = [noexcept]=
Definition at line *322* of file *forward_list.h*.

** template<typename _Tp , typename _Alloc > template<typename... _Args>
*_Fwd_list_node_base* * *std::_Fwd_list_base*< _Tp, _Alloc
>::_M_insert_after (*const_iterator* __pos, _Args &&...
__args)= [protected]=
Definition at line *50* of file *forward_list.tcc*.

** template<typename _Tp , typename _Alloc > void *std::_Fwd_list_base*<
_Tp, _Alloc >::_M_put_node (*_Node* * __p)= [inline]=, = [protected]=
Definition at line *381* of file *forward_list.h*.

* Member Data Documentation
** template<typename _Tp , typename _Alloc > _Fwd_list_impl
*std::_Fwd_list_base*< _Tp, _Alloc >::_M_impl= [protected]=
Definition at line *314* of file *forward_list.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
