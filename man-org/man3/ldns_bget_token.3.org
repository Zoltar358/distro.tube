#+TITLE: Manpages - ldns_bget_token.3
#+DESCRIPTION: Linux manpage for ldns_bget_token.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_bget_token, ldns_bgetc, ldns_bskipcs - get tokens from buffers

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ssize_t ldns_bget_token(ldns_buffer *b, char *token, const char *delim,
size_t limit);

int ldns_bgetc(ldns_buffer *buffer);

void ldns_bskipcs(ldns_buffer *buffer, const char *s);

* DESCRIPTION
/ldns_bget_token/() returns a token/char from the buffer b. This
function deals with ( and ) in the buffer, and ignores when it finds
them. .br **b*: the buffer to read from .br **token*: the token is put
here .br **delim*: chars at which the parsing should stop .br **limit*:
how much to read. If 0 the builtin maximum is used .br Returns s 0 on
error of EOF of b. Otherwise return the length of what is read

/ldns_bgetc/() returns the next character from a buffer. Advances the
position pointer with 1. When end of buffer is reached returns EOF. This
is the buffer's equivalent for getc(). .br **buffer*: buffer to read
from .br Returns EOF on failure otherwise return the character

/ldns_bskipcs/() skips all of the characters in the given string in the
buffer, moving the position to the first character that is not in *s.
.br **buffer*: buffer to use .br **s*: characters to skip .br Returns
void

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_buffer/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*,
*RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
