#+TITLE: Manpages - TIFFFieldDataType.3tiff
#+DESCRIPTION: Linux manpage for TIFFFieldDataType.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFFieldDataType - Get TIFF data type from field information

* SYNOPSIS
*#include <tiffio.h>*

*TIFFDataType TIFFFieldDataType(const TIFFField* */fip/*)*

* DESCRIPTION
*TIFFFieldDataType* returns the data type stored in a TIFF field.

/fip/ is a field information pointer previously returned by
*TIFFFindField*, *TIFFFieldWithTag*, or *TIFFFieldWithName*.\\

* RETURN VALUES
\\
*TIFFFieldDataType* returns a member of the enum type *TIFFDataType*.\\

* SEE ALSO
*libtiff*(3TIFF),

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
