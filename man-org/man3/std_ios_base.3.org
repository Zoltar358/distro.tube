#+TITLE: Manpages - std_ios_base.3
#+DESCRIPTION: Linux manpage for std_ios_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::ios_base - The base of the I/O class hierarchy.

* SYNOPSIS
\\

=#include <ios_base.h>=

Inherited by *std::basic_ios< char, _Traits >*, and *std::basic_ios<
_CharT, _Traits >*.

** Classes
class *failure*\\
These are thrown to indicate problems with io.

** Public Types
enum *event* { *erase_event*, *imbue_event*, *copyfmt_event* }\\
The set of events that may be passed to an event callback.

typedef void(* *event_callback*) (*event* __e, *ios_base* &__b, int
__i)\\
The type of an event callback function.

typedef _Ios_Fmtflags *fmtflags*\\
This is a bitmask type.

typedef _Ios_Iostate *iostate*\\
This is a bitmask type.

typedef _Ios_Openmode *openmode*\\
This is a bitmask type.

typedef _Ios_Seekdir *seekdir*\\
This is an enumerated type.

** Public Member Functions
*ios_base* (const *ios_base* &)=delete\\

virtual *~ios_base* ()\\

const *locale* & *_M_getloc* () const\\
Locale access.

*fmtflags* *flags* () const\\
Access to format flags.

*fmtflags* *flags* (*fmtflags* __fmtfl)\\
Setting new format flags all at once.

*locale* *getloc* () const\\
Locale access.

*locale* *imbue* (const *locale* &__loc) throw ()\\
Setting a new locale.

long & *iword* (int __ix)\\
Access to integer array.

*ios_base* & *operator=* (const *ios_base* &)=delete\\

*streamsize* *precision* () const\\
Flags access.

*streamsize* *precision* (*streamsize* __prec)\\
Changing flags.

void *& *pword* (int __ix)\\
Access to void pointer array.

void *register_callback* (*event_callback* __fn, int __index)\\
Add the callback __fn with parameter __index.

*fmtflags* *setf* (*fmtflags* __fmtfl)\\
Setting new format flags.

*fmtflags* *setf* (*fmtflags* __fmtfl, *fmtflags* __mask)\\
Setting new format flags.

void *unsetf* (*fmtflags* __mask)\\
Clearing format flags.

*streamsize* *width* () const\\
Flags access.

*streamsize* *width* (*streamsize* __wide)\\
Changing flags.

** Static Public Member Functions
static bool *sync_with_stdio* (bool __sync=true)\\
Interaction with the standard C I/O objects.

static int *xalloc* () throw ()\\
Access to unique indices.

** Static Public Attributes
static const *fmtflags* *adjustfield*\\
A mask of left|right|internal. Useful for the 2-arg form of =setf=.

static const *openmode* *app*\\
Seek to end before each write.

static const *openmode* *ate*\\
Open and seek to end immediately after opening.

static const *iostate* *badbit*\\
Indicates a loss of integrity in an input or output sequence (such as an
irrecoverable read error from a file).

static const *fmtflags* *basefield*\\
A mask of dec|oct|hex. Useful for the 2-arg form of =setf=.

static const *seekdir* *beg*\\
Request a seek relative to the beginning of the stream.

static const *openmode* *binary*\\
Perform input and output in binary mode (as opposed to text mode). This
is probably not what you think it is; see
https://gcc.gnu.org/onlinedocs/libstdc++/manual/fstreams.html#std.io.filestreams.binary.

static const *fmtflags* *boolalpha*\\
Insert/extract =bool= in alphabetic rather than numeric format.

static const *seekdir* *cur*\\
Request a seek relative to the current position within the sequence.

static const *fmtflags* *dec*\\
Converts integer input or generates integer output in decimal base.

static const *seekdir* *end*\\
Request a seek relative to the current end of the sequence.

static const *iostate* *eofbit*\\
Indicates that an input operation reached the end of an input sequence.

static const *iostate* *failbit*\\
Indicates that an input operation failed to read the expected
characters, or that an output operation failed to generate the desired
characters.

static const *fmtflags* *fixed*\\
Generate floating-point output in fixed-point notation.

static const *fmtflags* *floatfield*\\
A mask of scientific|fixed. Useful for the 2-arg form of =setf=.

static const *iostate* *goodbit*\\
Indicates all is well.

static const *fmtflags* *hex*\\
Converts integer input or generates integer output in hexadecimal base.

static const *openmode* *in*\\
Open for input. Default for =ifstream= and fstream.

static const *fmtflags* *internal*\\
Adds fill characters at a designated internal point in certain generated
output, or identical to =right= if no such point is designated.

static const *fmtflags* *left*\\
Adds fill characters on the right (final positions) of certain generated
output. (I.e., the thing you print is flush left.)

static const *fmtflags* *oct*\\
Converts integer input or generates integer output in octal base.

static const *openmode* *out*\\
Open for output. Default for =ofstream= and fstream.

static const *fmtflags* *right*\\
Adds fill characters on the left (initial positions) of certain
generated output. (I.e., the thing you print is flush right.)

static const *fmtflags* *scientific*\\
Generates floating-point output in scientific notation.

static const *fmtflags* *showbase*\\
Generates a prefix indicating the numeric base of generated integer
output.

static const *fmtflags* *showpoint*\\
Generates a decimal-point character unconditionally in generated
floating-point output.

static const *fmtflags* *showpos*\\
Generates a + sign in non-negative generated numeric output.

static const *fmtflags* *skipws*\\
Skips leading white space before certain input operations.

static const *openmode* *trunc*\\
Truncate an existing stream when opening. Default for =ofstream=.

static const *fmtflags* *unitbuf*\\
Flushes output after each output operation.

static const *fmtflags* *uppercase*\\
Replaces certain lowercase letters with their uppercase equivalents in
generated output.

** Protected Types
enum { *_S_local_word_size* }\\

** Protected Member Functions
void *_M_call_callbacks* (*event* __ev) throw ()\\

void *_M_dispose_callbacks* (void) throw ()\\

_Words & *_M_grow_words* (int __index, bool __iword)\\

void *_M_init* () throw ()\\

void *_M_move* (*ios_base* &) noexcept\\

void *_M_swap* (*ios_base* &__rhs) noexcept\\

** Protected Attributes
_Callback_list * *_M_callbacks*\\

*iostate* *_M_exception*\\

*fmtflags* *_M_flags*\\

*locale* *_M_ios_locale*\\

_Words *_M_local_word* [_S_local_word_size]\\

*streamsize* *_M_precision*\\

*iostate* *_M_streambuf_state*\\

*streamsize* *_M_width*\\

_Words * *_M_word*\\

int *_M_word_size*\\

_Words *_M_word_zero*\\

* Detailed Description
The base of the I/O class hierarchy.

This class defines everything that can be defined about I/O that does
not depend on the type of characters being input or output. Most people
will only see =ios_base= when they need to specify the full name of the
various I/O flags (e.g., the openmodes).

Definition at line *228* of file *ios_base.h*.

* Member Typedef Documentation
** typedef void(* std::ios_base::event_callback) (*event* __e,
*ios_base* &__b, int __i)
The type of an event callback function.

*Parameters*

#+begin_quote
  /__e/ One of the members of the event enum.\\
  /__b/ Reference to the ios_base object.\\
  /__i/ The integer provided when the callback was registered.
#+end_quote

Event callbacks are user defined functions that get called during
several ios_base and basic_ios functions, specifically imbue(),
copyfmt(), and ~ios().

Definition at line *529* of file *ios_base.h*.

** typedef _Ios_Fmtflags std::ios_base::fmtflags
This is a bitmask type. /_Ios_Fmtflags/= is=/ implementation-defined,
but it is valid to perform bitwise operations on these values and expect
the Right Thing to happen. Defined objects of type fmtflags are:/

- boolalpha

- dec

- fixed

- hex

- internal

- left

- oct

- right

- scientific

- showbase

- showpoint

- showpos

- skipws

- unitbuf

- uppercase

- adjustfield

- basefield

- floatfield

Definition at line *341*/ of file /*ios_base.h*/./

** typedef _Ios_Iostate std::ios_base::iostate
This is a bitmask type. /_Ios_Iostate/= is=/ implementation-defined, but
it is valid to perform bitwise operations on these values and expect the
Right Thing to happen. Defined objects of type iostate are:/

- badbit

- eofbit

- failbit

- goodbit

Definition at line *416*/ of file /*ios_base.h*/./

** typedef _Ios_Openmode std::ios_base::openmode
This is a bitmask type. /_Ios_Openmode/= is=/ implementation-defined,
but it is valid to perform bitwise operations on these values and expect
the Right Thing to happen. Defined objects of type openmode are:/

- app

- ate

- binary

- in

- out

- trunc

Definition at line *447*/ of file /*ios_base.h*/./

** typedef _Ios_Seekdir std::ios_base::seekdir
This is an enumerated type. /_Ios_Seekdir/= is=/ implementation-defined.
Defined values of type seekdir are:/

- beg

- cur, equivalent to =SEEK_CUR=/ in the C standard library./

- end, equivalent to =SEEK_END=/ in the C standard library. /

Definition at line *479*/ of file /*ios_base.h*/./

* Member Enumeration Documentation
** anonymous enum= [protected]=
Definition at line *604*/ of file /*ios_base.h*/./

** enum *std::ios_base::event*
The set of events that may be passed to an event callback. erase_event
is used during ~ios() and copyfmt(). imbue_event is used during imbue().
copyfmt_event is used during copyfmt().

Definition at line *512*/ of file /*ios_base.h*/./

* Constructor & Destructor Documentation
** virtual std::ios_base::~ios_base ()= [virtual]=
Invokes each callback with erase_event. Destroys local storage.

Note that the ios_base object for the standard streams never gets
destroyed. As a result, any callbacks registered with the standard
streams will not get invoked with erase_event (unless copyfmt is used).

* Member Function Documentation
** const *locale*/ & std::ios_base::_M_getloc () const/= [inline]=
Locale access.

*Returns*

#+begin_quote
  A reference to the current locale.
#+end_quote

Like getloc above, but returns a reference instead of generating a copy.

Definition at line *804*/ of file /*ios_base.h*/./

Referenced by *std::money_get< _CharT, _InIter >::do_get()*/,
/*std::time_get< _CharT, _InIter >::do_get()*/, /*std::num_get< _CharT,
_InIter >::do_get()*/, /*std::time_get< _CharT, _InIter
>::do_get_date()*/, /*std::time_get< _CharT, _InIter
>::do_get_monthname()*/, /*std::time_get< _CharT, _InIter
>::do_get_time()*/, /*std::time_get< _CharT, _InIter
>::do_get_weekday()*/, /*std::num_put< _CharT, _OutIter >::do_put()*/,
/*std::time_put< _CharT, _OutIter >::do_put()*/, /*std::time_get<
_CharT, _InIter >::get()*/, and /*std::time_put< _CharT, _OutIter
>::put()*/./

** *fmtflags*/ std::ios_base::flags () const/= [inline]=
Access to format flags.

*Returns*

#+begin_quote
  The format control flags for both input and output.
#+end_quote

Definition at line *649*/ of file /*ios_base.h*/./

Referenced by *std::basic_istream< _CharT, _Traits
>::sentry::sentry()*/, /*std::basic_ios< _CharT, _Traits >::copyfmt()*/,
/*std::num_get< _CharT, _InIter >::do_get()*/, /*std::num_put< _CharT,
_OutIter >::do_put()*/, /*std::operator<<()*/, /*std::operator>>()*/,
and /*std::__detail::operator>>()*/./

** *fmtflags*/ std::ios_base::flags (/*fmtflags*/ __fmtfl)/= [inline]=
Setting new format flags all at once.

*Parameters*

#+begin_quote
  /__fmtfl The new flags to set. /
#+end_quote

*Returns*

#+begin_quote
  The previous format control flags.
#+end_quote

This function overwrites all the format flags with /__fmtfl. /

Definition at line *660*/ of file /*ios_base.h*/./

** *locale*/ std::ios_base::getloc () const/= [inline]=
Locale access.

*Returns*

#+begin_quote
  A copy of the current locale.
#+end_quote

If =imbue(loc)=/ has previously been called, then this function returns
/=loc=/. Otherwise, it returns a copy of /=std::locale()=/, the global
C++ locale. /

Definition at line *793*/ of file /*ios_base.h*/./

Referenced by *std::basic_ios< _CharT, _Traits >::copyfmt()*/,
/*std::money_put< _CharT, _OutIter >::do_put()*/, /*std::operator>>()*/,
and /*std::ws()*/./

** *locale*/ std::ios_base::imbue (const /*locale*/ & __loc)/
Setting a new locale.

*Parameters*

#+begin_quote
  /__loc The new locale. /
#+end_quote

*Returns*

#+begin_quote
  The previous locale.
#+end_quote

Sets the new locale for this stream, and then invokes each callback with
imbue_event.

Referenced by *std::basic_ios< _CharT, _Traits >::imbue()*/./

** long & std::ios_base::iword (int __ix)= [inline]=
Access to integer array.

*Parameters*

#+begin_quote
  /__ix Index into the array. /
#+end_quote

*Returns*

#+begin_quote
  A reference to an integer associated with the index.
#+end_quote

The iword function provides access to an array of integers that can be
used for any purpose. The array grows as required to hold the supplied
index. All integers in the array are initialized to 0.

The implementation reserves several indices. You should use xalloc to
obtain an index that is safe to use. Also note that since the array can
grow dynamically, it is not safe to hold onto the reference.

Definition at line *839*/ of file /*ios_base.h*/./

** *streamsize*/ std::ios_base::precision () const/= [inline]=
Flags access.

*Returns*

#+begin_quote
  The precision to generate on certain output operations.
#+end_quote

Be careful if you try to give a definition of /precision here; see
DR 189. /

Definition at line *719*/ of file /*ios_base.h*/./

Referenced by *std::basic_ios< _CharT, _Traits >::copyfmt()*/./

** *streamsize*/ std::ios_base::precision (/*streamsize*/
__prec)/= [inline]=
Changing flags.

*Parameters*

#+begin_quote
  /__prec The new precision value. /
#+end_quote

*Returns*

#+begin_quote
  The previous value of precision().
#+end_quote

Definition at line *728*/ of file /*ios_base.h*/./

** void *& std::ios_base::pword (int __ix)= [inline]=
Access to void pointer array.

*Parameters*

#+begin_quote
  /__ix Index into the array. /
#+end_quote

*Returns*

#+begin_quote
  A reference to a void* associated with the index.
#+end_quote

The pword function provides access to an array of pointers that can be
used for any purpose. The array grows as required to hold the supplied
index. All pointers in the array are initialized to 0.

The implementation reserves several indices. You should use xalloc to
obtain an index that is safe to use. Also note that since the array can
grow dynamically, it is not safe to hold onto the reference.

Definition at line *860*/ of file /*ios_base.h*/./

** void std::ios_base::register_callback (*event_callback*/ __fn, int
__index)/
Add the callback __fn with parameter __index.

*Parameters*

#+begin_quote
  /__fn The function to add. /\\
  /__index The integer to pass to the function when invoked./
#+end_quote

Registers a function as an event callback with an integer parameter to
be passed to the function when invoked. Multiple copies of the function
are allowed. If there are multiple callbacks, they are invoked in the
order they were registered.

** *fmtflags*/ std::ios_base::setf (/*fmtflags*/ __fmtfl)/= [inline]=
Setting new format flags.

*Parameters*

#+begin_quote
  /__fmtfl Additional flags to set. /
#+end_quote

*Returns*

#+begin_quote
  The previous format control flags.
#+end_quote

This function sets additional flags in format control. Flags that were
previously set remain set.

Definition at line *676*/ of file /*ios_base.h*/./

Referenced by *std::__detail::operator>>()*/./

** *fmtflags*/ std::ios_base::setf (/*fmtflags*/ __fmtfl, /*fmtflags*/
__mask)/= [inline]=
Setting new format flags.

*Parameters*

#+begin_quote
  /__fmtfl Additional flags to set. /\\
  /__mask The flags mask for fmtfl. /
#+end_quote

*Returns*

#+begin_quote
  The previous format control flags.
#+end_quote

This function clears /mask in the format flags, then sets fmtfl /=&=/
mask. An example mask is /=ios_base::adjustfield=/. /

Definition at line *693*/ of file /*ios_base.h*/./

** static bool std::ios_base::sync_with_stdio (bool __sync =
=true=/)/= [static]=
Interaction with the standard C I/O objects.

*Parameters*

#+begin_quote
  /__sync Whether to synchronize or not. /
#+end_quote

*Returns*

#+begin_quote
  True if the standard streams were previously synchronized.
#+end_quote

The synchronization referred to is /only that between the standard C
facilities (e.g., stdout) and the standard C++ objects (e.g., cout).
User-declared streams are unaffected. See
https://gcc.gnu.org/onlinedocs/libstdc++/manual/fstreams.html#std.io.filestreams.binary
/

** void std::ios_base::unsetf (*fmtflags*/ __mask)/= [inline]=
Clearing format flags.

*Parameters*

#+begin_quote
  /__mask The flags to unset./
#+end_quote

This function clears /__mask in the format flags. /

Definition at line *708*/ of file /*ios_base.h*/./

** *streamsize*/ std::ios_base::width () const/= [inline]=
Flags access.

*Returns*

#+begin_quote
  The minimum field width to generate on output operations.
#+end_quote

/Minimum field width refers to the number of characters. /

Definition at line *742*/ of file /*ios_base.h*/./

Referenced by *std::basic_ios< _CharT, _Traits >::copyfmt()*/,
/*std::num_put< _CharT, _OutIter >::do_put()*/, and
/*std::operator>>()*/./

** *streamsize*/ std::ios_base::width (/*streamsize*/
__wide)/= [inline]=
Changing flags.

*Parameters*

#+begin_quote
  /__wide The new width value. /
#+end_quote

*Returns*

#+begin_quote
  The previous value of width().
#+end_quote

Definition at line *751*/ of file /*ios_base.h*/./

** static int std::ios_base::xalloc ()= [static]=
Access to unique indices.

*Returns*

#+begin_quote
  An integer different from all previous calls.
#+end_quote

This function returns a unique integer every time it is called. It can
be used for any purpose, but is primarily intended to be a unique index
for the iword and pword functions. The expectation is that an
application calls xalloc in order to obtain an index in the iword and
pword arrays that can be used without fear of conflict.

The implementation maintains a static variable that is incremented and
returned on each invocation. xalloc is guaranteed to return an index
that is safe to use in the iword and pword arrays.

* Member Data Documentation
** _Callback_list* std::ios_base::_M_callbacks= [protected]=
Definition at line *583*/ of file /*ios_base.h*/./

** *iostate*/ std::ios_base::_M_exception/= [protected]=
Definition at line *548*/ of file /*ios_base.h*/./

** *fmtflags*/ std::ios_base::_M_flags/= [protected]=
Definition at line *547*/ of file /*ios_base.h*/./

** *locale*/ std::ios_base::_M_ios_locale/= [protected]=
Definition at line *615*/ of file /*ios_base.h*/./

** _Words std::ios_base::_M_local_word[_S_local_word_size]= [protected]=
Definition at line *605*/ of file /*ios_base.h*/./

** *streamsize*/ std::ios_base::_M_precision/= [protected]=
Definition at line *545*/ of file /*ios_base.h*/./

** *iostate*/ std::ios_base::_M_streambuf_state/= [protected]=
Definition at line *549*/ of file /*ios_base.h*/./

** *streamsize*/ std::ios_base::_M_width/= [protected]=
Definition at line *546*/ of file /*ios_base.h*/./

** _Words* std::ios_base::_M_word= [protected]=
Definition at line *609*/ of file /*ios_base.h*/./

** int std::ios_base::_M_word_size= [protected]=
Definition at line *608*/ of file /*ios_base.h*/./

** _Words std::ios_base::_M_word_zero= [protected]=
Definition at line *600*/ of file /*ios_base.h*/./

** const *fmtflags*/ std::ios_base::adjustfield/= [static]=
A mask of left|right|internal. Useful for the 2-arg form of =setf=/. /

Definition at line *396*/ of file /*ios_base.h*/./

Referenced by *std::num_put< _CharT, _OutIter >::do_put()*/,
/*std::internal()*/, /*std::left()*/, and /*std::right()*/./

** const *openmode*/ std::ios_base::app/= [static]=
Seek to end before each write.

Definition at line *450*/ of file /*ios_base.h*/./

Referenced by *std::basic_filebuf< _CharT, _Traits >::overflow()*/, and
/*std::basic_filebuf< _CharT, _Traits >::xsputn()*/./

** const *openmode*/ std::ios_base::ate/= [static]=
Open and seek to end immediately after opening.

Definition at line *453*/ of file /*ios_base.h*/./

Referenced by *std::basic_filebuf< _CharT, _Traits >::open()*/./

** const *iostate*/ std::ios_base::badbit/= [static]=
Indicates a loss of integrity in an input or output sequence (such as an
irrecoverable read error from a file).

Definition at line *420*/ of file /*ios_base.h*/./

Referenced by *std::basic_istream< _CharT, _Traits
>::sentry::sentry()*/, /*std::basic_ios< _CharT, _Traits >::bad()*/,
/*std::basic_ios< _CharT, _Traits >::fail()*/, /*std::basic_ostream<
_CharT, _Traits >::flush()*/, /*std::basic_istream< _CharT, _Traits
>::get()*/, /*std::basic_istream< char >::get()*/, /*std::basic_istream<
char >::ignore()*/, /*std::basic_ostream< char >::operator<<()*/,
/*std::basic_ostream< _CharT, _Traits >::operator<<()*/,
/*std::operator>>()*/, /*std::basic_istream< _CharT, _Traits
>::operator>>()*/, /*std::basic_istream< _CharT, _Traits >::peek()*/,
/*std::basic_ostream< _CharT, _Traits >::put()*/, /*std::basic_istream<
_CharT, _Traits >::putback()*/, /*std::basic_istream< _CharT, _Traits
>::read()*/, /*std::basic_istream< _CharT, _Traits >::seekg()*/,
/*std::basic_istream< _CharT, _Traits >::sync()*/, /*std::basic_istream<
_CharT, _Traits >::tellg()*/, /*std::basic_ostream< _CharT, _Traits
>::tellp()*/, and /*std::basic_istream< _CharT, _Traits >::unget()*/./

** const *fmtflags*/ std::ios_base::basefield/= [static]=
A mask of dec|oct|hex. Useful for the 2-arg form of =setf=/. /

Definition at line *399*/ of file /*ios_base.h*/./

Referenced by *std::dec()*/, /*std::num_get< _CharT, _InIter
>::do_get()*/, /*std::num_put< _CharT, _OutIter >::do_put()*/,
/*std::hex()*/, and /*std::oct()*/./

** const *seekdir*/ std::ios_base::beg/= [static]=
Request a seek relative to the beginning of the stream.

Definition at line *482*/ of file /*ios_base.h*/./

Referenced by *std::basic_filebuf< _CharT, _Traits >::seekpos()*/, and
/*__gnu_cxx::stdio_sync_filebuf< _CharT, _Traits >::seekpos()*/./

** const *openmode*/ std::ios_base::binary/= [static]=
Perform input and output in binary mode (as opposed to text mode). This
is probably not what you think it is; see
https://gcc.gnu.org/onlinedocs/libstdc++/manual/fstreams.html#std.io.filestreams.binary.

Definition at line *458*/ of file /*ios_base.h*/./

Referenced by *std::basic_filebuf< _CharT, _Traits >::showmanyc()*/./

** const *fmtflags*/ std::ios_base::boolalpha/= [static]=
Insert/extract =bool=/ in alphabetic rather than numeric format. /

Definition at line *344*/ of file /*ios_base.h*/./

Referenced by *std::boolalpha()*/, /*std::num_get< _CharT, _InIter
>::do_get()*/, /*std::num_put< _CharT, _OutIter >::do_put()*/, and
/*std::noboolalpha()*/./

** const *seekdir*/ std::ios_base::cur/= [static]=
Request a seek relative to the current position within the sequence.

Definition at line *485*/ of file /*ios_base.h*/./

Referenced by *std::basic_filebuf< _CharT, _Traits >::imbue()*/,
/*std::basic_filebuf< _CharT, _Traits >::overflow()*/,
/*std::basic_filebuf< _CharT, _Traits >::pbackfail()*/,
/*std::basic_filebuf< _CharT, _Traits >::seekoff()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::seekoff()*/,
/*std::basic_istream< _CharT, _Traits >::tellg()*/, and
/*std::basic_ostream< _CharT, _Traits >::tellp()*/./

** const *fmtflags*/ std::ios_base::dec/= [static]=
Converts integer input or generates integer output in decimal base.

Definition at line *347*/ of file /*ios_base.h*/./

Referenced by *std::dec()*/./

** const *seekdir*/ std::ios_base::end/= [static]=
Request a seek relative to the current end of the sequence.

Definition at line *488*/ of file /*ios_base.h*/./

Referenced by *std::basic_filebuf< _CharT, _Traits >::open()*/, and
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::seekoff()*/./

** const *iostate*/ std::ios_base::eofbit/= [static]=
Indicates that an input operation reached the end of an input sequence.

Definition at line *423*/ of file /*ios_base.h*/./

Referenced by *std::basic_istream< _CharT, _Traits
>::sentry::sentry()*/, /*std::basic_istream< char >::sentry::sentry()*/,
/*std::time_get< _CharT, _InIter >::do_get()*/, /*std::num_get< _CharT,
_InIter >::do_get()*/, /*std::time_get< _CharT, _InIter
>::do_get_date()*/, /*std::time_get< _CharT, _InIter
>::do_get_monthname()*/, /*std::time_get< _CharT, _InIter
>::do_get_time()*/, /*std::time_get< _CharT, _InIter
>::do_get_weekday()*/, /*std::time_get< _CharT, _InIter
>::do_get_year()*/, /*std::basic_ios< _CharT, _Traits >::eof()*/,
/*std::basic_istream< _CharT, _Traits >::get()*/, /*std::time_get<
_CharT, _InIter >::get()*/, /*std::basic_istream< char >::getline()*/,
/*std::basic_istream< _CharT, _Traits >::operator>>()*/,
/*std::operator>>()*/, /*std::basic_istream< _CharT, _Traits
>::peek()*/, /*std::basic_istream< _CharT, _Traits >::putback()*/,
/*std::basic_istream< _CharT, _Traits >::read()*/, /*std::basic_istream<
_CharT, _Traits >::seekg()*/, /*std::basic_istream< char >::sync()*/,
/*std::basic_istream< _CharT, _Traits >::unget()*/, and /*std::ws()*/./

** const *iostate*/ std::ios_base::failbit/= [static]=
Indicates that an input operation failed to read the expected
characters, or that an output operation failed to generate the desired
characters.

Definition at line *428*/ of file /*ios_base.h*/./

Referenced by *std::basic_istream< _CharT, _Traits
>::sentry::sentry()*/, /*std::basic_ostream< _CharT, _Traits
>::sentry::sentry()*/, /*std::num_get< _CharT, _InIter >::do_get()*/,
/*std::time_get< _CharT, _InIter >::do_get_monthname()*/,
/*std::time_get< _CharT, _InIter >::do_get_weekday()*/, /*std::time_get<
_CharT, _InIter >::do_get_year()*/, /*std::basic_ios< _CharT, _Traits
>::fail()*/, /*std::time_get< _CharT, _InIter >::get()*/,
/*std::basic_istream< char >::getline()*/, /*std::operator>>()*/,
/*std::basic_istream< char >::operator>>()*/, /*std::basic_istream<
_CharT, _Traits >::operator>>()*/, /*std::basic_istream< _CharT, _Traits
>::read()*/, /*std::basic_istream< _CharT, _Traits >::seekg()*/, and
/*std::basic_ostream< _CharT, _Traits >::seekp()*/./

** const *fmtflags*/ std::ios_base::fixed/= [static]=
Generate floating-point output in fixed-point notation.

Definition at line *350*/ of file /*ios_base.h*/./

Referenced by *std::fixed()*/, and /*std::hexfloat()*/./

** const *fmtflags*/ std::ios_base::floatfield/= [static]=
A mask of scientific|fixed. Useful for the 2-arg form of =setf=/. /

Definition at line *402*/ of file /*ios_base.h*/./

Referenced by *std::defaultfloat()*/, /*std::fixed()*/,
/*std::hexfloat()*/, and /*std::scientific()*/./

** const *iostate*/ std::ios_base::goodbit/= [static]=
Indicates all is well.

Definition at line *431*/ of file /*ios_base.h*/./

Referenced by *std::basic_istream< _CharT, _Traits
>::sentry::sentry()*/, /*std::time_get< _CharT, _InIter >::do_get()*/,
/*std::num_get< _CharT, _InIter >::do_get()*/, /*std::time_get< _CharT,
_InIter >::do_get_monthname()*/, /*std::time_get< _CharT, _InIter
>::do_get_weekday()*/, /*std::time_get< _CharT, _InIter
>::do_get_year()*/, /*std::basic_ostream< _CharT, _Traits >::flush()*/,
/*std::basic_istream< _CharT, _Traits >::get()*/, /*std::time_get<
_CharT, _InIter >::get()*/, /*std::basic_istream< char >::getline()*/,
/*std::basic_istream< _CharT, _Traits >::ignore()*/,
/*std::basic_ostream< _CharT, _Traits >::operator<<()*/,
/*std::operator>>()*/, /*std::basic_istream< _CharT, _Traits
>::operator>>()*/, /*std::basic_istream< char >::operator>>()*/,
/*std::basic_istream< _CharT, _Traits >::peek()*/, /*std::basic_ostream<
_CharT, _Traits >::put()*/, /*std::basic_istream< _CharT, _Traits
>::putback()*/, /*std::basic_istream< _CharT, _Traits >::read()*/,
/*std::basic_istream< _CharT, _Traits >::readsome()*/,
/*std::basic_istream< _CharT, _Traits >::seekg()*/,
/*std::basic_ostream< _CharT, _Traits >::seekp()*/,
/*std::basic_istream< _CharT, _Traits >::sync()*/, and
/*std::basic_istream< _CharT, _Traits >::unget()*/./

** const *fmtflags*/ std::ios_base::hex/= [static]=
Converts integer input or generates integer output in hexadecimal base.

Definition at line *353*/ of file /*ios_base.h*/./

Referenced by *std::num_get< _CharT, _InIter >::do_get()*/,
/*std::num_put< _CharT, _OutIter >::do_put()*/, and /*std::hex()*/./

** const *openmode*/ std::ios_base::in/= [static]=
Open for input. Default for =ifstream=/ and fstream. /

Definition at line *461*/ of file /*ios_base.h*/./

Referenced by *std::basic_stringbuf< _CharT, _Traits, _Alloc
>::overflow()*/, /*std::basic_filebuf< _CharT, _Traits >::pbackfail()*/,
/*std::basic_istream< _CharT, _Traits >::seekg()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::seekoff()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::seekpos()*/,
/*std::basic_filebuf< _CharT, _Traits >::showmanyc()*/,
/*std::basic_istream< _CharT, _Traits >::tellg()*/,
/*std::basic_filebuf< _CharT, _Traits >::underflow()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::underflow()*/, and
/*std::basic_filebuf< _CharT, _Traits >::xsgetn()*/./

** const *fmtflags*/ std::ios_base::internal/= [static]=
Adds fill characters at a designated internal point in certain generated
output, or identical to =right=/ if no such point is designated. /

Definition at line *358*/ of file /*ios_base.h*/./

Referenced by *std::internal()*/./

** const *fmtflags*/ std::ios_base::left/= [static]=
Adds fill characters on the right (final positions) of certain generated
output. (I.e., the thing you print is flush left.)

Definition at line *362*/ of file /*ios_base.h*/./

Referenced by *std::num_put< _CharT, _OutIter >::do_put()*/, and
/*std::left()*/./

** const *fmtflags*/ std::ios_base::oct/= [static]=
Converts integer input or generates integer output in octal base.

Definition at line *365*/ of file /*ios_base.h*/./

Referenced by *std::oct()*/./

** const *openmode*/ std::ios_base::out/= [static]=
Open for output. Default for =ofstream=/ and fstream. /

Definition at line *464*/ of file /*ios_base.h*/./

Referenced by *std::basic_filebuf< _CharT, _Traits >::overflow()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::overflow()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::pbackfail()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::seekoff()*/,
/*std::basic_ostream< _CharT, _Traits >::seekp()*/,
/*std::basic_stringbuf< _CharT, _Traits, _Alloc >::seekpos()*/,
/*std::basic_ostream< _CharT, _Traits >::tellp()*/, and
/*std::basic_filebuf< _CharT, _Traits >::xsputn()*/./

** const *fmtflags*/ std::ios_base::right/= [static]=
Adds fill characters on the left (initial positions) of certain
generated output. (I.e., the thing you print is flush right.)

Definition at line *369*/ of file /*ios_base.h*/./

Referenced by *std::right()*/./

** const *fmtflags*/ std::ios_base::scientific/= [static]=
Generates floating-point output in scientific notation.

Definition at line *372*/ of file /*ios_base.h*/./

Referenced by *std::hexfloat()*/, and /*std::scientific()*/./

** const *fmtflags*/ std::ios_base::showbase/= [static]=
Generates a prefix indicating the numeric base of generated integer
output.

Definition at line *376*/ of file /*ios_base.h*/./

Referenced by *std::num_put< _CharT, _OutIter >::do_put()*/,
/*std::noshowbase()*/, and /*std::showbase()*/./

** const *fmtflags*/ std::ios_base::showpoint/= [static]=
Generates a decimal-point character unconditionally in generated
floating-point output.

Definition at line *380*/ of file /*ios_base.h*/./

Referenced by *std::noshowpoint()*/, and /*std::showpoint()*/./

** const *fmtflags*/ std::ios_base::showpos/= [static]=
Generates a + sign in non-negative generated numeric output.

Definition at line *383*/ of file /*ios_base.h*/./

Referenced by *std::noshowpos()*/, and /*std::showpos()*/./

** const *fmtflags*/ std::ios_base::skipws/= [static]=
Skips leading white space before certain input operations.

Definition at line *386*/ of file /*ios_base.h*/./

Referenced by *std::basic_istream< _CharT, _Traits
>::sentry::sentry()*/, /*std::noskipws()*/,
/*std::__detail::operator>>()*/, and /*std::skipws()*/./

** const *openmode*/ std::ios_base::trunc/= [static]=
Truncate an existing stream when opening. Default for =ofstream=/. /

Definition at line *467*/ of file /*ios_base.h*/./

** const *fmtflags*/ std::ios_base::unitbuf/= [static]=
Flushes output after each output operation.

Definition at line *389*/ of file /*ios_base.h*/./

Referenced by *std::nounitbuf()*/, and /*std::unitbuf()*/./

** const *fmtflags*/ std::ios_base::uppercase/= [static]=
Replaces certain lowercase letters with their uppercase equivalents in
generated output.

Definition at line *393*/ of file /*ios_base.h*/./

Referenced by *std::num_put< _CharT, _OutIter >::do_put()*/,
/*std::nouppercase()*/, and /*std::uppercase()*/./

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
