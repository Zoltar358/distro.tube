#+TITLE: Manpages - zip_set_archive_comment.3
#+DESCRIPTION: Linux manpage for zip_set_archive_comment.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function sets the comment for the entire zip archive. If

is

and

is 0, the archive comment will be removed.

must be encoded in ASCII or UTF-8.

Upon successful completion 0 is returned. Otherwise, -1 is returned and
the error information in

is set to indicate the error.

fails if:

is less than 0 or longer than the maximum comment length in a zip file
(65535), or

is not a valid UTF-8 encoded string.

Required memory could not be allocated.

was added in libzip 0.7. In libzip 0.11 the type of

was changed from

to

and
