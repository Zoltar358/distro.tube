#+TITLE: Manpages - udev_list_entry_get_value.3
#+DESCRIPTION: Linux manpage for udev_list_entry_get_value.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about udev_list_entry_get_value.3 is found in manpage for: [[../udev_list_entry.3][udev_list_entry.3]]