#+TITLE: Manpages - uid_from_user.3bsd
#+DESCRIPTION: Linux manpage for uid_from_user.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about uid_from_user.3bsd is found in manpage for: [[../man3/pwcache.3bsd][man3/pwcache.3bsd]]