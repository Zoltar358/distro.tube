#+TITLE: Manpages - gnutls_certificate_set_x509_simple_pkcs12_file.3
#+DESCRIPTION: Linux manpage for gnutls_certificate_set_x509_simple_pkcs12_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_certificate_set_x509_simple_pkcs12_file - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int
gnutls_certificate_set_x509_simple_pkcs12_file(gnutls_certificate_credentials_t
*/res/*, const char * */pkcs12file/*, gnutls_x509_crt_fmt_t */type/*,
const char * */password/*);*

* ARGUMENTS
- gnutls_certificate_credentials_t res :: is a
  *gnutls_certificate_credentials_t* type.

- const char * pkcs12file :: filename of file containing PKCS*12* blob.

- gnutls_x509_crt_fmt_t type :: is PEM or DER of the /pkcs12file/ .

- const char * password :: optional password used to decrypt PKCS*12*
  file, bags and keys.

* DESCRIPTION
This function sets a certificate/private key pair and/or a CRL in the
gnutls_certificate_credentials_t type. This function may be called more
than once (in case multiple keys/certificates exist for the server).

PKCS*12* files with a MAC, encrypted bags and PKCS *8* private keys are
supported. However, only password based security, and the same password
for all operations, are supported.

PKCS*12* file may contain many keys and/or certificates, and this
function will try to auto-detect based on the key ID the certificate and
key pair to use. If the PKCS*12* file contain the issuer of the selected
certificate, it will be appended to the certificate to form a chain.

If more than one private keys are stored in the PKCS*12* file, then only
one key will be read (and it is undefined which one).

It is believed that the limitations of this function is acceptable for
most usage, and that any more flexibility would introduce complexity
that would make it harder to use this functionality at all.

Note that, this function by default returns zero on success and a
negative value on error. Since 3.5.6, when the flag
*GNUTLS_CERTIFICATE_API_V2* is set using
*gnutls_certificate_set_flags()* it returns an index (greater or equal
to zero). That index can be used to other functions to refer to the
added key-pair.

* RETURNS
On success this functions returns zero, and otherwise a negative value
on error (see above for modifying that behavior).

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
