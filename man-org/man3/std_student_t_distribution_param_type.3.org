#+TITLE: Manpages - std_student_t_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_student_t_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::student_t_distribution< _RealType >::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *student_t_distribution*< _RealType > *distribution_type*\\

** Public Member Functions
*param_type* (_RealType __n)\\

_RealType *n* () const\\

** Friends
bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

* Detailed Description
** "template<typename _RealType = double>
\\
struct std::student_t_distribution< _RealType >::param_type"Parameter
type.

Definition at line *3306* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef
*student_t_distribution*<_RealType> *std::student_t_distribution*<
_RealType >::*param_type::distribution_type*
Definition at line *3308* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double> *std::student_t_distribution*<
_RealType >::param_type::param_type ()= [inline]=
Definition at line *3310* of file *random.h*.

** template<typename _RealType = double> *std::student_t_distribution*<
_RealType >::param_type::param_type (_RealType __n)= [inline]=,
= [explicit]=
Definition at line *3313* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> _RealType
*std::student_t_distribution*< _RealType >::param_type::n ()
const= [inline]=
Definition at line *3318* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator!= (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *3326* of file *random.h*.

** template<typename _RealType = double> bool operator== (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *3322* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
