#+TITLE: Manpages - zip_file_attributes_init.3
#+DESCRIPTION: Linux manpage for zip_file_attributes_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

initializes a

structure with default values. It must be called before modifying such a
structure for the first time.

was added in libzip 1.7.0.

and
