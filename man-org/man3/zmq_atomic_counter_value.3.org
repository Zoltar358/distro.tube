#+TITLE: Manpages - zmq_atomic_counter_value.3
#+DESCRIPTION: Linux manpage for zmq_atomic_counter_value.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_atomic_counter_value - return value of atomic counter

* SYNOPSIS
*int zmq_atomic_counter_value (void *counter);*

* DESCRIPTION
The /zmq_atomic_counter_value/ function returns the value of an atomic
counter created by /zmq_atomic_counter_new()/. This function uses
platform specific atomic operations.

* RETURN VALUE
The /zmq_atomic_counter_value()/ function returns the value of the
atomic counter. If /counter/ does not point to an atomic counter created
by /zmq_atomic_counter_new()/, the behaviour is undefined.

* EXAMPLE
*Test code for atomic counters*.

#+begin_quote
  #+begin_example
    void *counter = zmq_atomic_counter_new ();
    assert (zmq_atomic_counter_value (counter) == 0);
    assert (zmq_atomic_counter_inc (counter) == 0);
    assert (zmq_atomic_counter_inc (counter) == 1);
    assert (zmq_atomic_counter_inc (counter) == 2);
    assert (zmq_atomic_counter_value (counter) == 3);
    assert (zmq_atomic_counter_dec (counter) == 1);
    assert (zmq_atomic_counter_dec (counter) == 1);
    assert (zmq_atomic_counter_dec (counter) == 0);
    zmq_atomic_counter_set (counter, 2);
    assert (zmq_atomic_counter_dec (counter) == 1);
    assert (zmq_atomic_counter_dec (counter) == 0);
    zmq_atomic_counter_destroy (&counter);
    return 0;
  #+end_example
#+end_quote

* SEE ALSO
*zmq_atomic_counter_new*(3) *zmq_atomic_counter_set*(3)
*zmq_atomic_counter_inc*(3) *zmq_atomic_counter_dec*(3)
*zmq_atomic_counter_destroy*(3)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
