#+TITLE: Manpages - CURLOPT_ACCEPTTIMEOUT_MS.3
#+DESCRIPTION: Linux manpage for CURLOPT_ACCEPTTIMEOUT_MS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_ACCEPTTIMEOUT_MS - timeout waiting for FTP server to connect
back

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ACCEPTTIMEOUT_MS, long
ms);

* DESCRIPTION
Pass a long telling libcurl the maximum number of milliseconds to wait
for a server to connect back to libcurl when an active FTP connection is
used.

* DEFAULT
60000 milliseconds

* PROTOCOLS
FTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/path/file");

    /* wait no more than 5 seconds for FTP server responses */
    curl_easy_setopt(curl, CURLOPT_ACCEPTTIMEOUT_MS, 5000L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.24.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_STDERR*(3), *CURLOPT_DEBUGFUNCTION*(3),
