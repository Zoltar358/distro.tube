#+TITLE: Manpages - sl_init.3bsd
#+DESCRIPTION: Linux manpage for sl_init.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sl_init.3bsd is found in manpage for: [[../man3/stringlist.3bsd][man3/stringlist.3bsd]]