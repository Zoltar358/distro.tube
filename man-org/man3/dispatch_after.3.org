#+TITLE: Manpages - dispatch_after.3
#+DESCRIPTION: Linux manpage for dispatch_after.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The

function submits the

to the given

at the time specified by the

parameter. The

parameter is a value created by

or

Submission of the block may be delayed by the system in order to improve
power consumption and system performance. The system applies a leeway
(see

that is equal to one tenth of the interval between

and the time at which the function is called, with the leeway capped to
at least one millisecond and at most one minute.

For a more detailed description about submitting blocks to queues, see

retains the passed queue.

Specifying

as the

parameter is supported, but is not as efficient as calling

The result of passing

as the

parameter is undefined.

The

function is a wrapper around
