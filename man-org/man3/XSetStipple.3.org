#+TITLE: Manpages - XSetStipple.3
#+DESCRIPTION: Linux manpage for XSetStipple.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetStipple.3 is found in manpage for: [[../man3/XSetTile.3][man3/XSetTile.3]]