#+TITLE: Manpages - XRotateBuffers.3
#+DESCRIPTION: Linux manpage for XRotateBuffers.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XRotateBuffers.3 is found in manpage for: [[../man3/XStoreBytes.3][man3/XStoreBytes.3]]