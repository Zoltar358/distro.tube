#+TITLE: Manpages - LWP_MemberMixin.3pm
#+DESCRIPTION: Linux manpage for LWP_MemberMixin.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
LWP::MemberMixin - Member access mixin class

* SYNOPSIS
package Foo; use base qw(LWP::MemberMixin);

* DESCRIPTION
A mixin class to get methods that provide easy access to member
variables in the =%$self=. Ideally there should be better Perl language
support for this.

* METHODS
There is only one method provided:

** _elem
_elem($elem [, $val])

Internal method to get/set the value of member variable =$elem=. If
=$val= is present it is used as the new value for the member variable.
If it is not present the current value is not touched. In both cases the
previous value of the member variable is returned.
