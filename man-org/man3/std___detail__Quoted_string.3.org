#+TITLE: Manpages - std___detail__Quoted_string.3
#+DESCRIPTION: Linux manpage for std___detail__Quoted_string.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Quoted_string< _String, _CharT > - Struct for delimited
strings.

* SYNOPSIS
\\

=#include <quoted_string.h>=

** Public Member Functions
*_Quoted_string* (_String __str, _CharT __del, _CharT __esc)\\

*_Quoted_string* & *operator=* (*_Quoted_string* &)=delete\\

** Public Attributes
_CharT *_M_delim*\\

_CharT *_M_escape*\\

_String *_M_string*\\

* Detailed Description
** "template<typename _String, typename _CharT>
\\
struct std::__detail::_Quoted_string< _String, _CharT >"Struct for
delimited strings.

Definition at line *49* of file *quoted_string.h*.

* Constructor & Destructor Documentation
** template<typename _String , typename _CharT >
*std::__detail::_Quoted_string*< _String, _CharT >::*_Quoted_string*
(_String __str, _CharT __del, _CharT __esc)= [inline]=
Definition at line *55* of file *quoted_string.h*.

* Member Data Documentation
** template<typename _String , typename _CharT > _CharT
*std::__detail::_Quoted_string*< _String, _CharT >::_M_delim
Definition at line *63* of file *quoted_string.h*.

** template<typename _String , typename _CharT > _CharT
*std::__detail::_Quoted_string*< _String, _CharT >::_M_escape
Definition at line *64* of file *quoted_string.h*.

** template<typename _String , typename _CharT > _String
*std::__detail::_Quoted_string*< _String, _CharT >::_M_string
Definition at line *62* of file *quoted_string.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
