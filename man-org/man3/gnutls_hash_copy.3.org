#+TITLE: Manpages - gnutls_hash_copy.3
#+DESCRIPTION: Linux manpage for gnutls_hash_copy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_hash_copy - API function

* SYNOPSIS
*#include <gnutls/crypto.h>*

*gnutls_hash_hd_t gnutls_hash_copy(gnutls_hash_hd_t */handle/*);*

* ARGUMENTS
- gnutls_hash_hd_t handle :: is a *gnutls_hash_hd_t* type

* DESCRIPTION
This function will create a copy of Message Digest context, containing
all its current state. Copying contexts for Message Digests registered
using *gnutls_crypto_register_digest()* is not supported and will always
result in an error.

* RETURNS
new Message Digest context or NULL in case of an error.

* SINCE
3.6.9

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
