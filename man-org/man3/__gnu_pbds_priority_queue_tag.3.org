#+TITLE: Manpages - __gnu_pbds_priority_queue_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_priority_queue_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::priority_queue_tag - Basic priority-queue.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::container_tag*.

Inherited by *__gnu_pbds::binary_heap_tag*,
*__gnu_pbds::binomial_heap_tag*, *__gnu_pbds::pairing_heap_tag*,
*__gnu_pbds::rc_binomial_heap_tag*, and *__gnu_pbds::thin_heap_tag*.

* Detailed Description
Basic priority-queue.

Definition at line *171* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
