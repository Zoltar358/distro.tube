#+TITLE: Manpages - std_discard_block_engine.3
#+DESCRIPTION: Linux manpage for std_discard_block_engine.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::discard_block_engine< _RandomNumberEngine, __p, __r >

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
template<typename _Sseq > using *_If_seed_seq* = typename *enable_if*<
__detail::__is_seed_seq< _Sseq, *discard_block_engine*, *result_type*
>::value >::type\\

typedef _RandomNumberEngine::result_type *result_type*\\

** Public Member Functions
*discard_block_engine* ()\\
Constructs a default discard_block_engine engine.

*discard_block_engine* (_RandomNumberEngine &&__rng)\\
Move constructs a discard_block_engine engine.

template<typename _Sseq , typename = _If_seed_seq<_Sseq>>
*discard_block_engine* (_Sseq &__q)\\
Generator construct a discard_block_engine engine.

*discard_block_engine* (const _RandomNumberEngine &__rng)\\
Copy constructs a discard_block_engine engine.

*discard_block_engine* (*result_type* __s)\\
Seed constructs a discard_block_engine engine.

const _RandomNumberEngine & *base* () const noexcept\\
Gets a const reference to the underlying generator engine object.

void *discard* (unsigned long long __z)\\
Discard a sequence of random numbers.

*result_type* *operator()* ()\\
Gets the next value in the generated random number sequence.

void *seed* ()\\
Reseeds the discard_block_engine object with the default seed for the
underlying base class generator engine.

template<typename _Sseq > _If_seed_seq< _Sseq > *seed* (_Sseq &__q)\\
Reseeds the discard_block_engine object with the given seed sequence.

void *seed* (*result_type* __s)\\
Reseeds the discard_block_engine object with the default seed for the
underlying base class generator engine.

** Static Public Member Functions
static constexpr *result_type* *max* ()\\
Gets the maximum value in the generated random number range.

static constexpr *result_type* *min* ()\\
Gets the minimum value in the generated random number range.

** Static Public Attributes
static constexpr size_t *block_size*\\

static constexpr size_t *used_block*\\

** Friends
template<typename _RandomNumberEngine1 , size_t __p1, size_t __r1,
typename _CharT , typename _Traits > *std::basic_ostream*< _CharT,
_Traits > & *operator<<* (*std::basic_ostream*< _CharT, _Traits > &__os,
const *std::discard_block_engine*< _RandomNumberEngine1, __p1, __r1 >
&__x)\\
Inserts the current state of a discard_block_engine random number
generator engine =__x= into the output stream =__os=.

bool *operator==* (const *discard_block_engine* &__lhs, const
*discard_block_engine* &__rhs)\\
Compares two discard_block_engine random number generator objects of the
same type for equality.

template<typename _RandomNumberEngine1 , size_t __p1, size_t __r1,
typename _CharT , typename _Traits > *std::basic_istream*< _CharT,
_Traits > & *operator>>* (*std::basic_istream*< _CharT, _Traits > &__is,
*std::discard_block_engine*< _RandomNumberEngine1, __p1, __r1 > &__x)\\
Extracts the current state of a % subtract_with_carry_engine random
number generator engine =__x= from the input stream =__is=.

* Detailed Description
** "template<typename _RandomNumberEngine, size_t __p, size_t __r>
\\
class std::discard_block_engine< _RandomNumberEngine, __p, __r
>"Produces random numbers from some base engine by discarding blocks of
data.

0 <= =__r= <= =__p=

Definition at line *884* of file *random.h*.

* Member Typedef Documentation
** template<typename _RandomNumberEngine , size_t __p, size_t __r>
template<typename _Sseq > using *std::discard_block_engine*<
_RandomNumberEngine, __p, __r >::_If_seed_seq = typename
*enable_if*<__detail::__is_seed_seq< _Sseq, *discard_block_engine*,
*result_type*>::value>::type
Definition at line *894* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
typedef _RandomNumberEngine::result_type *std::discard_block_engine*<
_RandomNumberEngine, __p, __r >::*result_type*
The type of the generated random value.

Definition at line *891* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RandomNumberEngine , size_t __p, size_t __r>
*std::discard_block_engine*< _RandomNumberEngine, __p, __r
>::*discard_block_engine* ()= [inline]=
Constructs a default discard_block_engine engine. The underlying engine
is default constructed as well.

Definition at line *906* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
*std::discard_block_engine*< _RandomNumberEngine, __p, __r
>::*discard_block_engine* (const _RandomNumberEngine &
__rng)= [inline]=, = [explicit]=
Copy constructs a discard_block_engine engine. Copies an existing base
class random number generator.

*Parameters*

#+begin_quote
  /__rng/ An existing (base class) engine object.
#+end_quote

Definition at line *916* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
*std::discard_block_engine*< _RandomNumberEngine, __p, __r
>::*discard_block_engine* (_RandomNumberEngine && __rng)= [inline]=,
= [explicit]=
Move constructs a discard_block_engine engine. Copies an existing base
class random number generator.

*Parameters*

#+begin_quote
  /__rng/ An existing (base class) engine object.
#+end_quote

Definition at line *926* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
*std::discard_block_engine*< _RandomNumberEngine, __p, __r
>::*discard_block_engine* (*result_type* __s)= [inline]=, = [explicit]=
Seed constructs a discard_block_engine engine. Constructs the underlying
generator engine seeded with =__s=.

*Parameters*

#+begin_quote
  /__s/ A seed value for the base class engine.
#+end_quote

Definition at line *936* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
template<typename _Sseq , typename = _If_seed_seq<_Sseq>>
*std::discard_block_engine*< _RandomNumberEngine, __p, __r
>::*discard_block_engine* (_Sseq & __q)= [inline]=, = [explicit]=
Generator construct a discard_block_engine engine.

*Parameters*

#+begin_quote
  /__q/ A seed sequence.
#+end_quote

Definition at line *946* of file *random.h*.

* Member Function Documentation
** template<typename _RandomNumberEngine , size_t __p, size_t __r> const
_RandomNumberEngine & *std::discard_block_engine*< _RandomNumberEngine,
__p, __r >::base () const= [inline]=, = [noexcept]=
Gets a const reference to the underlying generator engine object.

Definition at line *990* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r> void
*std::discard_block_engine*< _RandomNumberEngine, __p, __r >::discard
(unsigned long long __z)= [inline]=
Discard a sequence of random numbers.

Definition at line *1011* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
static constexpr *result_type* *std::discard_block_engine*<
_RandomNumberEngine, __p, __r >::max ()= [inline]=, = [static]=,
= [constexpr]=
Gets the maximum value in the generated random number range.

Definition at line *1004* of file *random.h*.

References *std::max()*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
static constexpr *result_type* *std::discard_block_engine*<
_RandomNumberEngine, __p, __r >::min ()= [inline]=, = [static]=,
= [constexpr]=
Gets the minimum value in the generated random number range.

Definition at line *997* of file *random.h*.

References *std::min()*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
*discard_block_engine*< _RandomNumberEngine, __p, __r >::*result_type*
*std::discard_block_engine*< _RandomNumberEngine, __p, __r >::operator()
Gets the next value in the generated random number sequence.

Definition at line *681* of file *bits/random.tcc*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r> void
*std::discard_block_engine*< _RandomNumberEngine, __p, __r >::seed
()= [inline]=
Reseeds the discard_block_engine object with the default seed for the
underlying base class generator engine.

Definition at line *955* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
template<typename _Sseq > _If_seed_seq< _Sseq >
*std::discard_block_engine*< _RandomNumberEngine, __p, __r >::seed
(_Sseq & __q)= [inline]=
Reseeds the discard_block_engine object with the given seed sequence.

*Parameters*

#+begin_quote
  /__q/ A seed generator function.
#+end_quote

Definition at line *979* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r> void
*std::discard_block_engine*< _RandomNumberEngine, __p, __r >::seed
(*result_type* __s)= [inline]=
Reseeds the discard_block_engine object with the default seed for the
underlying base class generator engine.

Definition at line *966* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RandomNumberEngine , size_t __p, size_t __r>
template<typename _RandomNumberEngine1 , size_t __p1, size_t __r1,
typename _CharT , typename _Traits > *std::basic_ostream*< _CharT,
_Traits > & operator<< (*std::basic_ostream*< _CharT, _Traits > & __os,
const *std::discard_block_engine*< _RandomNumberEngine1, __p1, __r1 > &
__x)= [friend]=
Inserts the current state of a discard_block_engine random number
generator engine =__x= into the output stream =__os=.

*Parameters*

#+begin_quote
  /__os/ An output stream.\\
  /__x/ A discard_block_engine random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The output stream with the state of =__x= inserted or in an error
  state.
#+end_quote

** template<typename _RandomNumberEngine , size_t __p, size_t __r> bool
operator== (const *discard_block_engine*< _RandomNumberEngine, __p, __r
> & __lhs, const *discard_block_engine*< _RandomNumberEngine, __p, __r >
& __rhs)= [friend]=
Compares two discard_block_engine random number generator objects of the
same type for equality.

*Parameters*

#+begin_quote
  /__lhs/ A discard_block_engine random number generator object.\\
  /__rhs/ Another discard_block_engine random number generator object.
#+end_quote

*Returns*

#+begin_quote
  true if the infinite sequences of generated values would be equal,
  false otherwise.
#+end_quote

Definition at line *1035* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
template<typename _RandomNumberEngine1 , size_t __p1, size_t __r1,
typename _CharT , typename _Traits > *std::basic_istream*< _CharT,
_Traits > & operator>> (*std::basic_istream*< _CharT, _Traits > & __is,
*std::discard_block_engine*< _RandomNumberEngine1, __p1, __r1 > &
__x)= [friend]=
Extracts the current state of a % subtract_with_carry_engine random
number generator engine =__x= from the input stream =__is=.

*Parameters*

#+begin_quote
  /__is/ An input stream.\\
  /__x/ A discard_block_engine random number generator engine.
#+end_quote

*Returns*

#+begin_quote
  The input stream with the state of =__x= extracted or in an error
  state.
#+end_quote

* Member Data Documentation
** template<typename _RandomNumberEngine , size_t __p, size_t __r>
constexpr size_t *std::discard_block_engine*< _RandomNumberEngine, __p,
__r >::block_size= [static]=, = [constexpr]=
Definition at line *898* of file *random.h*.

** template<typename _RandomNumberEngine , size_t __p, size_t __r>
constexpr size_t *std::discard_block_engine*< _RandomNumberEngine, __p,
__r >::used_block= [static]=, = [constexpr]=
Definition at line *899* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
