#+TITLE: Manpages - SDL_UserEvent.3
#+DESCRIPTION: Linux manpage for SDL_UserEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_UserEvent - A user-defined event type

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    Uint8 type;
    int code;
    void *data1;
    void *data2;
  } SDL_UserEvent;
#+end_example

* STRUCTURE DATA
- *type* :: *SDL_USEREVENT* through to *SDL_NUMEVENTS-1*

- *code* :: User defined event code

- *data1* :: User defined data pointer

- *data2* :: User defined data pointer

* DESCRIPTION
*SDL_UserEvent* is in the *user* member of the structure *SDL_Event*.
This event is unique, it is never created by SDL but only by the user.
The event can be pushed onto the event queue using *SDL_PushEvent*. The
contents of the structure members or completely up to the programmer,
the only requirement is that *type* is a value from *SDL_USEREVENT* to
*SDL_NUMEVENTS-1* (inclusive).

* EXAMPLES
#+begin_example
  SDL_Event event;

  event.type = SDL_USEREVENT;
  event.user.code = my_event_code;
  event.user.data1 = significant_data;
  event.user.data2 = 0;
  SDL_PushEvent(&event);
#+end_example

* SEE ALSO
*SDL_Event*, *SDL_PushEvent*
