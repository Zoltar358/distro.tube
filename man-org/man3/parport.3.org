#+TITLE: Manpages - parport.3
#+DESCRIPTION: Linux manpage for parport.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
parport - representation of a parallel port

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

* DESCRIPTION
A parport structure represents a parallel port.

* STRUCTURE MEMBERS
The structure has the following members:

#+begin_quote
  #+begin_example
    struct parport {
      /* An artibrary name for the port. */
      const char *name;

      /* The base address of the port, if that has any meaning, or zero. */
      unsigned long base_addr;

      /* The ECR address of the port, if that has any meaning, or zero. */
      unsigned long hibase_addr;

      /* The filename associated with this port,
       * if that has any meaning, or NULL. */
      const char *filename;
    };
  #+end_example
#+end_quote

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
