#+TITLE: Manpages - Xinerama.3
#+DESCRIPTION: Linux manpage for Xinerama.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
Xinerama - API for Xinerama extension to X11 Protocol

* SYNOPSIS
#include <X11/extensions/Xinerama.h>

#+begin_example

  Bool XineramaQueryExtension  ( Display *dpy,
  	int *event_base_return, int *error_base_return );

  Status XineramaQueryVersion  ( Display *dpy,
  	int *major_version_return,
  	int *minor_version_return );

  Bool XineramaIsActive  ( Display *dpy );

  XineramaScreenInfo * XineramaQueryScreens  ( Display *dpy,
  	int *number );
#+end_example

* ARGUMENTS
- display :: Specifies the connection to the X server.

- event_base_return :: Specifies the return location for the assigned
  base event code

- error_base_return :: Specifies the return location for the assigned
  base error code

- major_version_return :: Returns the major version supported by the
  server

- minor_version_return :: Returns the minor version supported by the
  server

- number :: Returns the number of entries in the returned
  XineramaScreenInfo array.

* DESCRIPTION
*Xinerama* is a simple library designed to interface the Xinerama
Extension for retrieving information about physical output devices which
may be combined into a single logical X screen.

* FUNCTIONS
** *XineramaQueryExtension()*
The XineramaQueryExtension function queries the Xserver to determine the
availability of the Xinerama Extension. If the extension is available,
the return value is True, and event_base_return and error_base_return
are set to the base event number and base error number for the
extension, respectively. Otherwise, the return value is False, and the
values of event_base_return and error_base_return are undefined.

** *XineramaQueryVersion()*
The XineramaQueryVersion function returns the version of the Xinerama
extension implemented by the Xserver. The version is returned in
major_version_return and minor_version_return. The major version will be
incremented for protocol incompatible changes, and the minor version
will be incremented for small, upwardly compatible changes.

If the Xinerama library is compatible with the version returned by the
server, it returns nonzero. If the server does not support the XINERAMA
extension, or if there was an error during communications with the
server, or if the server and library protocol versions are incompatible,
it returns zero.

** *XineramaIsActive()*
The XineramaIsActive function returns a Boolean operator used to
determine if Xinerama is activated on the screen. Returns True for
active and False for not active.

** *XineramaQueryScreens()*
The *XineramaQueryScreens()* function returns info about each individual
output device within the Xinerama Screen. The integer pointed to by the
/number/ argument is updated to the number of output devices listed in
the returned array of /XineramaScreenInfo/ structures.
*XineramaQueryScreens()* returns NULL and sets /number/ to 0 if Xinerama
is not active.

The pointer returned should be released with XFree(3) when no longer
needed.

* NOTE
The original API provided for interacting with the XINERAMA extension
used function names beginning with /XPanoramiX/. That API is now
deprecated and this API should be used instead in new software.
