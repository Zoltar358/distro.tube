#+TITLE: Manpages - termkey_keycmp.3
#+DESCRIPTION: Linux manpage for termkey_keycmp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_keycmp - compare two key events

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  int termkey_keycmp(TermKey *tk, const TermKeyKey *key1",
   const TermKeyKey *key2);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_keycmp*() compares two key structures and applies a total
ordering, returning a value that is negative, zero, or positive, to
indicate if the given structures are increasing, identical, or
decreasing. Before comparison, copies of both referenced structures are
taken, and canonicalised according to the rules for
*termkey_canonicalise*(3).

Two structures of differing type are ordered *TERMKEY_TYPE_UNICODE*,
*TERMKEY_TYPE_KEYSYM*, *TERMKEY_TYPE_FUNCTION*, *TERMKEY_TYPE_MOUSE*.
Unicode structures are ordered by codepoint, keysym structures are
ordered by keysym number, function structures are ordered by function
key number, and mouse structures are ordered opaquely by an unspecified
but consistent ordering. Within these values, keys different in modifier
bits are ordered by the modifiers.

* RETURN VALUE
*termkey_keycmp*() returns an integer greater than, equal to, or less
than zero to indicate the relation between the two given key structures.

* SEE ALSO
*termkey_strpkey*(3), *termkey_canonicalise*(3), *termkey*(7)
