#+TITLE: Manpages - std_slice_array.3
#+DESCRIPTION: Linux manpage for std_slice_array.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::slice_array< _Tp > - Reference to one-dimensional subset of an
array.

* SYNOPSIS
\\

=#include <slice_array.h>=

** Public Types
typedef _Tp *value_type*\\

** Public Member Functions
*slice_array* (const *slice_array* &)\\
Copy constructor. Both slices refer to the same underlying array.

template<class _Dom > void *operator%=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator%=* (const *valarray*< _Tp > &) const\\
Modulo slice elements by corresponding elements of /v/.

template<class _Dom > void *operator&=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator&=* (const *valarray*< _Tp > &) const\\
Logical and slice elements with corresponding elements of /v/.

template<class _Dom > void *operator*=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator*=* (const *valarray*< _Tp > &) const\\
Multiply slice elements by corresponding elements of /v/.

template<class _Dom > void *operator+=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator+=* (const *valarray*< _Tp > &) const\\
Add corresponding elements of /v/ to slice elements.

template<class _Dom > void *operator-=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator-=* (const *valarray*< _Tp > &) const\\
Subtract corresponding elements of /v/ from slice elements.

template<class _Dom > void *operator/=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator/=* (const *valarray*< _Tp > &) const\\
Divide slice elements by corresponding elements of /v/.

template<class _Dom > void *operator<<=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator<<=* (const *valarray*< _Tp > &) const\\
Left shift slice elements by corresponding elements of /v/.

template<class _Dom > void *operator=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator=* (const _Tp &) const\\
Assign all slice elements to /t/.

*slice_array* & *operator=* (const *slice_array* &)\\
Assignment operator. Assigns slice elements to corresponding elements of
/a/.

void *operator=* (const *valarray*< _Tp > &) const\\
Assign slice elements to corresponding elements of /v/.

template<class _Dom > void *operator>>=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator>>=* (const *valarray*< _Tp > &) const\\
Right shift slice elements by corresponding elements of /v/.

template<class _Dom > void *operator^=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator^=* (const *valarray*< _Tp > &) const\\
Logical xor slice elements with corresponding elements of /v/.

template<class _Dom > void *operator|=* (const _Expr< _Dom, _Tp > &)
const\\

void *operator|=* (const *valarray*< _Tp > &) const\\
Logical or slice elements with corresponding elements of /v/.

** Friends
class *valarray< _Tp >*\\

* Detailed Description
** "template<typename _Tp>
\\
class std::slice_array< _Tp >"Reference to one-dimensional subset of an
array.

A slice_array is a reference to the actual elements of an array
specified by a slice. The way to get a slice_array is to call
operator[](slice) on a valarray. The returned slice_array then permits
carrying operations out on the referenced subset of elements in the
original valarray. For example, operator+=(valarray) will add values to
the subset of elements in the underlying valarray this slice_array
refers to.

*Parameters*

#+begin_quote
  /Tp/ Element type.
#+end_quote

Definition at line *128* of file *slice_array.h*.

* Member Typedef Documentation
** template<typename _Tp > typedef _Tp *std::slice_array*< _Tp
>::value_type
Definition at line *131* of file *slice_array.h*.

* Friends And Related Function Documentation
** template<typename _Tp > friend class *valarray*< _Tp >= [friend]=
Definition at line *190* of file *slice_array.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
