#+TITLE: Manpages - FcFontSetList.3
#+DESCRIPTION: Linux manpage for FcFontSetList.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFontSetList - List fonts from a set of font sets

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcFontSet * FcFontSetList (FcConfig */config/*, FcFontSet ***/sets/*,
int*/nsets/*, FcPattern **/pattern/*, FcObjectSet **/object_set/*);*

* DESCRIPTION
Selects fonts matching /pattern/ from /sets/, creates patterns from
those fonts containing only the objects in /object_set/ and returns the
set of unique such patterns. If /config/ is NULL, the default
configuration is checked to be up to date, and used.
