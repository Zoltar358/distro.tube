#+TITLE: Manpages - pcre2_converted_pattern_free.3
#+DESCRIPTION: Linux manpage for pcre2_converted_pattern_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  void pcre2_converted_pattern_free(PCRE2_UCHAR *converted_pattern);
#+end_example

* DESCRIPTION
This function is part of an experimental set of pattern conversion
functions. It frees the memory occupied by a converted pattern that was
obtained by calling *pcre2_pattern_convert()* with arguments that caused
it to place the converted pattern into newly obtained heap memory. If
the argument is NULL, the function returns immediately without doing
anything.

The pattern conversion functions are described in the *pcre2convert*
documentation.
