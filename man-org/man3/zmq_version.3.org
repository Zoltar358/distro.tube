#+TITLE: Manpages - zmq_version.3
#+DESCRIPTION: Linux manpage for zmq_version.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_version - report 0MQ library version

* SYNOPSIS
*void zmq_version (int */*major/*, int */*minor/*, int */*patch/*);*

* DESCRIPTION
The /zmq_version()/ function shall fill in the integer variables pointed
to by the /major/, /minor/ and /patch/ arguments with the major, minor
and patch level components of the 0MQ library version.

This functionality is intended for applications or language bindings
dynamically linking to the 0MQ library that wish to determine the actual
version of the 0MQ library they are using.

* RETURN VALUE
There is no return value.

* ERRORS
No errors are defined.

* EXAMPLE
*Printing out the version of the 0MQ library*.

#+begin_quote
  #+begin_example
    int major, minor, patch;
    zmq_version (&major, &minor, &patch);
    printf ("Current 0MQ version is %d.%d.%d\n", major, minor, patch);
  #+end_example
#+end_quote

* SEE ALSO
*zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
