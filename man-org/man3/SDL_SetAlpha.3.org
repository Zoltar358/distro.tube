#+TITLE: Manpages - SDL_SetAlpha.3
#+DESCRIPTION: Linux manpage for SDL_SetAlpha.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_SetAlpha - Adjust the alpha properties of a surface

* SYNOPSIS
*#include "SDL.h"*

*int SDL_SetAlpha*(*SDL_Surface *surface, Uint32 flag, Uint8 alpha*);

* DESCRIPTION

#+begin_quote
  *Note: *

  This function and the semantics of SDL alpha blending have changed
  since version 1.1.4. Up until version 1.1.5, an alpha value of 0 was
  considered opaque and a value of 255 was considered transparent. This
  has now been inverted: 0 (*SDL_ALPHA_TRANSPARENT) is now considered
  transparent and 255 (SDL_ALPHA_OPAQUE) is now considered opaque.*
#+end_quote

*SDL_SetAlpha is used for setting the per-surface alpha value and/or
enabling and disabling alpha blending.*

The*surface* parameter specifies which surface whose alpha attributes
you wish to adjust. *flags* is used to specify whether alpha blending
should be used (*SDL_SRCALPHA*) and whether the surface should use RLE
acceleration for blitting (*SDL_RLEACCEL*). *flags* can be an OR'd
combination of these two options, one of these options or 0. If
*SDL_SRCALPHA* is not passed as a flag then all alpha information is
ignored when blitting the surface. The *alpha* parameter is the
per-surface alpha value; a surface need not have an alpha channel to use
per-surface alpha and blitting can still be accelerated with
*SDL_RLEACCEL*.

#+begin_quote
  *Note: *

  The per-surface alpha value of 128 is considered a special case and is
  optimised, so it's much faster than other per-surface values.
#+end_quote

Alpha effects surface blitting in the following ways:

- RGBA->RGB with *SDL_SRCALPHA* :: The source is alpha-blended with the
  destination, using the alpha channel. *SDL_SRCCOLORKEY and the
  per-surface alpha are ignored.*

- RGBA->RGB without *SDL_SRCALPHA* :: The RGB data is copied from the
  source. The source alpha channel and the per-surface alpha value are
  ignored.

- RGB->RGBA with *SDL_SRCALPHA* :: The source is alpha-blended with the
  destination using the per-surface alpha value. If *SDL_SRCCOLORKEY is
  set, only the pixels not matching the colorkey value are copied. The
  alpha channel of the copied pixels is set to opaque.*

- RGB->RGBA without *SDL_SRCALPHA* :: The RGB data is copied from the
  source and the alpha value of the copied pixels is set to opaque. If
  *SDL_SRCCOLORKEY is set, only the pixels not matching the colorkey
  value are copied. *

- RGBA->RGBA with *SDL_SRCALPHA* :: The source is alpha-blended with the
  destination using the source alpha channel. The alpha channel in the
  destination surface is left untouched. *SDL_SRCCOLORKEY is ignored.*

- RGBA->RGBA without *SDL_SRCALPHA* :: The RGBA data is copied to the
  destination surface. If *SDL_SRCCOLORKEY is set, only the pixels not
  matching the colorkey value are copied.*

- RGB->RGB with *SDL_SRCALPHA* :: The source is alpha-blended with the
  destination using the per-surface alpha value. If *SDL_SRCCOLORKEY is
  set, only the pixels not matching the colorkey value are copied.*

- RGB->RGB without *SDL_SRCALPHA* :: The RGB data is copied from the
  source. If *SDL_SRCCOLORKEY is set, only the pixels not matching the
  colorkey value are copied.*

#+begin_quote
  *Note: *

  Note that RGBA->RGBA blits (with SDL_SRCALPHA set) keep the alpha of
  the destination surface. This means that you cannot compose two
  arbitrary RGBA surfaces this way and get the result you would expect
  from "overlaying" them; the destination alpha will work as a mask.

  Also note that per-pixel and per-surface alpha cannot be combined; the
  per-pixel alpha is always used if available
#+end_quote

* RETURN VALUE
This function returns *0*, or *-1* if there was an error.

* SEE ALSO
*SDL_MapRGBA*, *SDL_GetRGBA*, *SDL_DisplayFormatAlpha*,
*SDL_BlitSurface*
