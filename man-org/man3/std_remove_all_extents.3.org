#+TITLE: Manpages - std_remove_all_extents.3
#+DESCRIPTION: Linux manpage for std_remove_all_extents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::remove_all_extents< _Tp > - remove_all_extents

* SYNOPSIS
\\

** Public Types
typedef _Tp *type*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::remove_all_extents< _Tp >"remove_all_extents

Definition at line *1945* of file *std/type_traits*.

* Member Typedef Documentation
** template<typename _Tp > typedef _Tp *std::remove_all_extents*< _Tp
>::type
Definition at line *1946* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
