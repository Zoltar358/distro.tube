#+TITLE: Manpages - curl_url_dup.3
#+DESCRIPTION: Linux manpage for curl_url_dup.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_url_dup - duplicate a CURLU handle

* SYNOPSIS
*#include <curl/curl.h>*

CURLU *curl_url_dup(CURLU *inhandle);

* DESCRIPTION
Duplicates a given CURLU /inhandle/ and all its contents and returns a
pointer to a new CURLU handle. The new handle also needs to be freed
with /curl_url_cleanup(3)/.

* EXAMPLE
#+begin_example
    CURLUcode rc;
    CURLU *url = curl_url();
    CURLU *url2;
    rc = curl_url_set(url, CURLUPART_URL, "https://example.com", 0);
    if(!rc) {
      url2 = curl_url_dup(url); /* clone it! */
      curl_url_cleanup(url2);
    }
    curl_url_cleanup(url);
#+end_example

* AVAILABILITY
Added in 7.62.0

* RETURN VALUE
Returns a new handle or NULL if out of memory.

* SEE ALSO
*curl_url_cleanup*(3), *curl_url*(3), *curl_url_set*(3),
*curl_url_get*(3), *CURLOPT_CURLU*(3),
