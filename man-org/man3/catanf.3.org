#+TITLE: Manpages - catanf.3
#+DESCRIPTION: Linux manpage for catanf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about catanf.3 is found in manpage for: [[../man3/catan.3][man3/catan.3]]