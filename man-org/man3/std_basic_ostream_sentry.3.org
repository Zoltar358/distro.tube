#+TITLE: Manpages - std_basic_ostream_sentry.3
#+DESCRIPTION: Linux manpage for std_basic_ostream_sentry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::basic_ostream< _CharT, _Traits >::sentry - Performs setup work for
output streams.

* SYNOPSIS
\\

** Public Member Functions
*sentry* (*basic_ostream*< _CharT, _Traits > &__os)\\
The constructor performs preparatory work.

*~sentry* ()\\
Possibly flushes the stream.

*operator bool* () const\\
Quick status checking.

* Detailed Description
** "template<typename _CharT, typename _Traits>
\\
class std::basic_ostream< _CharT, _Traits >::sentry"Performs setup work
for output streams.

Objects of this class are created before all of the standard inserters
are run. It is responsible for /exception-safe prefix and suffix
operations/.

Definition at line *432* of file *ostream*.

* Constructor & Destructor Documentation
** template<typename _CharT , typename _Traits > *std::basic_ostream*<
_CharT, _Traits >::sentry::sentry (*basic_ostream*< _CharT, _Traits > &
__os)= [explicit]=
The constructor performs preparatory work.

*Parameters*

#+begin_quote
  /__os/ The output stream to guard.
#+end_quote

If the stream state is good (/__os.good()/ is true), then if the stream
is tied to another output stream, =is.tie()->flush()= is called to
synchronize the output sequences.

If the stream state is still good, then the sentry state becomes true
(/okay/).

Definition at line *46* of file *ostream.tcc*.

References *std::ios_base::failbit*, *std::basic_ios< _CharT, _Traits
>::good()*, *std::basic_ios< _CharT, _Traits >::setstate()*, and
*std::basic_ios< _CharT, _Traits >::tie()*.

** template<typename _CharT , typename _Traits > *std::basic_ostream*<
_CharT, _Traits >::sentry::~sentry ()= [inline]=
Possibly flushes the stream. If =ios_base::unitbuf= is set in
=os.flags()=, and =std::uncaught_exception()= is true, the sentry
destructor calls =flush()= on the output stream.

Definition at line *462* of file *ostream*.

* Member Function Documentation
** template<typename _CharT , typename _Traits > *std::basic_ostream*<
_CharT, _Traits >::sentry::operator bool () const= [inline]=,
= [explicit]=
Quick status checking.

*Returns*

#+begin_quote
  The sentry state.
#+end_quote

For ease of use, sentries may be converted to booleans. The return value
is that of the sentry state (true == okay).

Definition at line *484* of file *ostream*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
