#+TITLE: Manpages - hwloc_obj_is_in_subtree.3
#+DESCRIPTION: Linux manpage for hwloc_obj_is_in_subtree.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_obj_is_in_subtree.3 is found in manpage for: [[../man3/hwlocality_helper_ancestors.3][man3/hwlocality_helper_ancestors.3]]