#+TITLE: Manpages - Date_Language_Hungarian.3pm
#+DESCRIPTION: Linux manpage for Date_Language_Hungarian.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Date::Language::Hungarian - Magyar format for Date::Format

* SYNOPSIS
my $lang = Date::Language->new(Hungarian); print $lang->time2str("%a %b
%e %T %Y", time); @lt = localtime(time); print
$lang->time2str($template, time); print $lang->strftime($template, @lt);
print $lang->time2str($template, time, $zone); print
$lang->strftime($template, @lt, $zone); print $lang->ctime(time); print
$lang->asctime(@lt); print $lang->ctime(time, $zone); print
$lang->asctime(@lt, $zone);

See Date::Format.

* AUTHOR
Paula Goddard (paula -at- paulacska -dot- com)

* LICENCE
Made available under the same terms as Perl itself.
