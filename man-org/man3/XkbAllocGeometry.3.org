#+TITLE: Manpages - XkbAllocGeometry.3
#+DESCRIPTION: Linux manpage for XkbAllocGeometry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbAllocGeometry - Allocate an entire geometry

* SYNOPSIS
*Status XkbAllocGeometry* *( XkbDescPtr */xkb/* ,* *XkbGeometrySizesPtr
*/sizes/* );*

* ARGUMENTS
- /- xkb/ :: keyboard description for which geometry is to be allocated

- /- sizes/ :: initial sizes for all geometry components

* DESCRIPTION
Xkb provides a number of functions to allocate and free subcomponents of
a keyboard geometry. Use these functions to create or modify keyboard
geometries. Note that these functions merely allocate space for the new
element(s), and it is up to you to fill in the values explicitly in your
code. These allocation functions increase /sz_*/ but never touch /num_*/
(unless there is an allocation failure, in which case they reset both
/sz_*/ and /num_*/ to zero). These functions return Success if they
succeed, BadAlloc if they are not able to allocate space, or BadValue if
a parameter is not as expected.

/XkbAllocGeometry/ allocates a keyboard geometry and adds it to the
keyboard description specified by /xkb./ The keyboard description should
be obtained via the /XkbGetKeyboard/ or /XkbAllocKeyboard/ functions.
The /sizes/ parameter specifies the number of elements to be reserved
for the subcomponents of the keyboard geometry and can be zero or more.
These subcomponents include the /properties, colors, shapes, sections,
and doodads./

To free an entire geometry, use /XkbFreeGeometry./

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadValue* :: An argument is out of range

* SEE ALSO
*XkbAllocKeyboard*(3), *XkbFreeGeometry*(3), *XkbGetKeyboard*(3)
