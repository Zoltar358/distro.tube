#+TITLE: Manpages - libssh2_userauth_publickey.3
#+DESCRIPTION: Linux manpage for libssh2_userauth_publickey.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_userauth_publickey - authenticate using a callback function

* SYNOPSIS
#include <libssh2.h>

#+begin_example
  int libssh2_userauth_publickey(LIBSSH2_SESSION *session,
                                 const char *user,
                                 const unsigned char *pubkeydata,
                                 size_t pubkeydata_len,
                                 sign_callback,
                                 void **abstract);
#+end_example

* DESCRIPTION
Authenticate with the /sign_callback/ callback that matches the
prototype below

* CALLBACK
#+begin_example
  int name(LIBSSH2_SESSION *session, unsigned char **sig, size_t *sig_len,
           const unsigned char *data, size_t data_len, void **abstract);
#+end_example

This function gets called...

* RETURN VALUE
Return 0 on success or negative on failure.

* SEE ALSO
*libssh2_userauth_publickey_fromfile_ex(3)*
