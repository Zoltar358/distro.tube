#+TITLE: Manpages - erfl.3
#+DESCRIPTION: Linux manpage for erfl.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about erfl.3 is found in manpage for: [[../man3/erf.3][man3/erf.3]]