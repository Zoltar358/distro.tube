#+TITLE: Manpages - XGetIMValues.3
#+DESCRIPTION: Linux manpage for XGetIMValues.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetIMValues.3 is found in manpage for: [[../man3/XOpenIM.3][man3/XOpenIM.3]]