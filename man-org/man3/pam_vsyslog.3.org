#+TITLE: Manpages - pam_vsyslog.3
#+DESCRIPTION: Linux manpage for pam_vsyslog.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pam_vsyslog.3 is found in manpage for: [[../man3/pam_syslog.3][man3/pam_syslog.3]]