#+TITLE: Manpages - Test2_Event_Ok.3perl
#+DESCRIPTION: Linux manpage for Test2_Event_Ok.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::Event::Ok - Ok event type

* DESCRIPTION
Ok events are generated whenever you run a test that produces a result.
Examples are =ok()=, and =is()=.

* SYNOPSIS
use Test2::API qw/context/; use Test2::Event::Ok; my $ctx = context();
my $event = $ctx->ok($bool, $name, \@diag);

or:

my $ctx = context(); my $event = $ctx->send_event( Ok, pass => $bool,
name => $name, );

* ACCESSORS
- $rb = $e->pass :: The original true/false value of whatever was passed
  into the event (but reduced down to 1 or 0).

- $name = $e->name :: Name of the test.

- $b = $e->effective_pass :: This is the true/false value of the test
  after TODO and similar modifiers are taken into account.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
