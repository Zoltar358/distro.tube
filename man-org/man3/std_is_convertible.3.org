#+TITLE: Manpages - std_is_convertible.3
#+DESCRIPTION: Linux manpage for std_is_convertible.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_convertible< _From, _To > - is_convertible

* SYNOPSIS
\\

Inherits __is_convertible_helper::type.

* Detailed Description
** "template<typename _From, typename _To>
\\
struct std::is_convertible< _From, _To >"is_convertible

Definition at line *1407* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
