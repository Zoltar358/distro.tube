#+TITLE: Manpages - FcConfigSubstituteWithPat.3
#+DESCRIPTION: Linux manpage for FcConfigSubstituteWithPat.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigSubstituteWithPat - Execute substitutions

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigSubstituteWithPat (FcConfig */config/*, FcPattern **/p/*,
FcPattern **/p_pat/*, FcMatchKind */kind/*);*

* DESCRIPTION
Performs the sequence of pattern modification operations, if /kind/ is
FcMatchPattern, then those tagged as pattern operations are applied,
else if /kind/ is FcMatchFont, those tagged as font operations are
applied and p_pat is used for <test> elements with target=pattern.
Returns FcFalse if the substitution cannot be performed (due to
allocation failure). Otherwise returns FcTrue. If /config/ is NULL, the
current configuration is used.
