#+TITLE: Manpages - std_range_error.3
#+DESCRIPTION: Linux manpage for std_range_error.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::range_error

* SYNOPSIS
\\

Inherits *std::runtime_error*.

** Public Member Functions
*range_error* (const char *) _GLIBCXX_TXN_SAFE\\

*range_error* (const *range_error* &)=default\\

*range_error* (const *string* &__arg) _GLIBCXX_TXN_SAFE\\

*range_error* (*range_error* &&)=default\\

*range_error* & *operator=* (const *range_error* &)=default\\

*range_error* & *operator=* (*range_error* &&)=default\\

virtual const char * *what* () const noexcept\\

* Detailed Description
Thrown to indicate range errors in internal computations.\\

Definition at line *258* of file *stdexcept*.

* Member Function Documentation
** virtual const char * std::runtime_error::what () const= [virtual]=,
= [noexcept]=, = [inherited]=
Returns a C-style character string describing the general cause of the
current error (the same string passed to the ctor).\\

Reimplemented from *std::exception*.

Reimplemented in *std::filesystem::filesystem_error*, and
*std::experimental::filesystem::v1::filesystem_error*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
