#+TITLE: Manpages - hwloc_obj_attr_u_hwloc_numanode_attr_s.3
#+DESCRIPTION: Linux manpage for hwloc_obj_attr_u_hwloc_numanode_attr_s.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_obj_attr_u::hwloc_numanode_attr_s

* SYNOPSIS
\\

=#include <hwloc.h>=

** Data Structures
struct *hwloc_memory_page_type_s*\\

** Data Fields
hwloc_uint64_t *local_memory*\\

unsigned *page_types_len*\\

struct
*hwloc_obj_attr_u::hwloc_numanode_attr_s::hwloc_memory_page_type_s* *
*page_types*\\

* Detailed Description
NUMA node-specific Object Attributes.

* Field Documentation
** hwloc_uint64_t hwloc_obj_attr_u::hwloc_numanode_attr_s::local_memory
Local memory (in bytes)

** struct
*hwloc_obj_attr_u::hwloc_numanode_attr_s::hwloc_memory_page_type_s* *
hwloc_obj_attr_u::hwloc_numanode_attr_s::page_types
** unsigned hwloc_obj_attr_u::hwloc_numanode_attr_s::page_types_len
Size of array =page_types=.

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
