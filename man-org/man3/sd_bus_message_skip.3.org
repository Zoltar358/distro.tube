#+TITLE: Manpages - sd_bus_message_skip.3
#+DESCRIPTION: Linux manpage for sd_bus_message_skip.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_message_skip - Skip elements in a bus message

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_message_skip(sd_bus_message **/m/*, const char* */types/*);*

* DESCRIPTION
*sd_bus_message_skip()* is somewhat similar to *sd_bus_message_read*(3),
but instead of reading the contents of the message, it only moves the
"read pointer". Subsequent read operations will read the elements that
are after the elements that were skipped.

The /types/ argument has the same meaning as in *sd_bus_message_read()*.
It may also be *NULL*, to skip a single element of any type.

* RETURN VALUE
On success, *sd_bus_message_skip()* returns 0 or a positive integer. On
failure, it returns a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  The /m/ parameter is *NULL*.
#+end_quote

*-EBADMSG*

#+begin_quote
  The message cannot be parsed.
#+end_quote

*-EPERM*

#+begin_quote
  The message is not sealed.
#+end_quote

*-ENXIO*

#+begin_quote
  The message end has been reached and the requested elements cannot be
  read.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_message_read*(3),
*sd_bus_message_read_basic*(3)
