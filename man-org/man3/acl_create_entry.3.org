#+TITLE: Manpages - acl_create_entry.3
#+DESCRIPTION: Linux manpage for acl_create_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function creates a new ACL entry in the ACL pointed to by the contents
of the pointer argument

On success, the function returns a descriptor for the new ACL entry via

This function may cause memory to be allocated. The caller should free
any releasable memory, when the new ACL is no longer required, by
calling

with

as an argument. If the ACL working storage cannot be increased in the
current location, then the working storage for the ACL pointed to by

may be relocated and the previous working storage is released. A pointer
to the new working storage is returned via

The components of the new ACL entry are initialized in the following
ways: the ACL tag type component contains ACL_UNDEFINED_TAG, the
qualifier component contains ACL_UNDEFINED_ID, and the set of
permissions has no permissions enabled. Any existing ACL entry
descriptors that refer to entries in the ACL continue to refer to those
entries.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The ACL working storage requires more memory than is allowed by the
hardware or system-imposed memory management constraints.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
