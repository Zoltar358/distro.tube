#+TITLE: Manpages - sd_event_source_set_prepare.3
#+DESCRIPTION: Linux manpage for sd_event_source_set_prepare.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_event_source_set_prepare - Set a preparation callback for event
sources

* SYNOPSIS
#+begin_example
  #include <systemd/sd-event.h>
#+end_example

*int sd_event_source_set_prepare(sd_event_source **/source/*,
sd_event_handler_t */callback/*);*

*typedef int (*sd_event_handler_t)(sd_event_source **/s/*, void
**/userdata/*);*

* DESCRIPTION
*sd_event_source_set_prepare()* may be used to set a preparation
callback for the event source object specified as /source/. The callback
function specified as /callback/ will be invoked immediately before the
event loop goes to sleep to wait for incoming events. It is invoked with
the user data pointer passed when the event source was created. The
event source will be disabled if the callback function returns a
negative error code. The callback function may be used to reconfigure
the precise events to wait for. If the /callback/ parameter is passed as
*NULL* the callback function is reset.

Event source objects have no preparation callback associated when they
are first created with calls such as *sd_event_add_io*(3),
*sd_event_add_time*(3). Preparation callback functions are supported for
all event source types with the exception of those created with
*sd_event_add_exit*(3). Preparation callback functions are dispatched in
the order indicated by the event sources priority field, as set with
*sd_event_source_set_priority*(3). Preparation callbacks of disabled
event sources (see *sd_event_source_set_enabled*(3)) are not invoked.

* RETURN VALUE
On success, *sd_event_source_set_prepare()* returns a non-negative
integer. On failure, it returns a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  /source/ is not a valid pointer to an sd_event_source object.
#+end_quote

*-ESTALE*

#+begin_quote
  The event loop is already terminated.
#+end_quote

*-ENOMEM*

#+begin_quote
  Not enough memory.
#+end_quote

*-ECHILD*

#+begin_quote
  The event loop has been created in a different process.
#+end_quote

*-EDOM*

#+begin_quote
  The specified event source has been created with
  *sd_event_add_exit*(3).
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*sd-event*(3), *sd_event_add_io*(3), *sd_event_add_time*(3),
*sd_event_add_signal*(3), *sd_event_add_child*(3),
*sd_event_add_inotify*(3), *sd_event_add_defer*(3),
*sd_event_source_set_enabled*(3), *sd_event_source_set_priority*(3),
*sd_event_source_set_userdata*(3)
