#+TITLE: Manpages - XtRealizeWidget.3
#+DESCRIPTION: Linux manpage for XtRealizeWidget.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtRealizeWidget, XtIsRealized, XtUnrealizeWidget - realize and unrealize
widgets

* SYNTAX
#include <X11/Intrinsic.h>

void XtRealizeWidget(Widget /w/);

Boolean XtIsRealized(Widget /w/);

void XtUnrealizeWidget(Widget /w/);

* ARGUMENTS

23. Specifies the widget.

* DESCRIPTION
If the widget is already realized, *XtRealizeWidget* simply returns.
Otherwise, it performs the following:

- Binds all action names in the widget's translation table to procedures
  (see Section 10.1.2).

- Makes a post-order traversal of the widget tree rooted at the
  specified widget and calls the change_managed procedure of each
  composite widget that has one or more managed children.

- Constructs an *XSetWindowAttributes* structure filled in with
  information derived from the *Core* widget fields and calls the
  realize procedure for the widget, which adds any widget-specific
  attributes and creates the X window.

- If the widget is not a subclass of *compositeWidgetClass*,
  *XtRealizeWidget* returns; otherwise, it continues and performs the
  following:

  - Descends recursively to each of the widget's managed children and
    calls the realize procedures. Primitive widgets that instantiate
    children are responsible for realizing those children themselves.

  - Maps all of the managed children windows that have
    mapped_when_managed *True*. (If a widget is managed but
    mapped_when_managed is *False*, the widget is allocated visual space
    but is not displayed. Some people seem to like this to indicate
    certain states.)

If the widget is a top-level shell widget (that is, it has no parent),
and mapped_when_managed is *True*, *XtRealizeWidget* maps the widget
window.

The *XtIsRealized* function returns *True* if the widget has been
realized, that is, if the widget has a nonzero X window ID.

Some widget procedures (for example, set_values) might wish to operate
differently after the widget has been realized.

The *XtUnrealizeWidget* function destroys the windows of an existing
widget and all of its children (recursively down the widget tree). To
recreate the windows at a later time, call *XtRealizeWidget* again. If
the widget was managed, it will be unmanaged automatically before its
window is freed.

* SEE ALSO
XtManageChildren(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
