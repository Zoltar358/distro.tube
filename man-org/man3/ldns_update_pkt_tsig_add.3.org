#+TITLE: Manpages - ldns_update_pkt_tsig_add.3
#+DESCRIPTION: Linux manpage for ldns_update_pkt_tsig_add.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_update_pkt_tsig_add - add resolver's tsig credentials to an
ldns_pkt

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_status ldns_update_pkt_tsig_add(ldns_pkt *p, const ldns_resolver
*r);

* DESCRIPTION
/ldns_update_pkt_tsig_add/() add tsig credentials to a packet from a
resolver .br *p*: packet to copy to .br *r*: resolver to copy from

.br Returns status whether successful or not

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_update_pkt_new/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
