#+TITLE: Manpages - audit_close.3
#+DESCRIPTION: Linux manpage for audit_close.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_close - Close the audit netlink socket connection

* SYNOPSIS
#+begin_example
  #include <libaudit.h>

  void audit_close(int fd);
#+end_example

* DESCRIPTION
*audit_close*() closes the NETLINK_AUDIT socket that communicates with
the kernel part of the Linux Audit Subsystem. /fd/ must have been
returned by *audit_open*(3).

* RETURN VALUE
None.

* SEE ALSO
*audit_open*(3), *netlink*(7).

* AUTHOR
Steve Grubb
