#+TITLE: Manpages - SSL_get_SSL_CTX.3ssl
#+DESCRIPTION: Linux manpage for SSL_get_SSL_CTX.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
SSL_get_SSL_CTX - get the SSL_CTX from which an SSL is created

* SYNOPSIS
#include <openssl/ssl.h> SSL_CTX *SSL_get_SSL_CTX(const SSL *ssl);

* DESCRIPTION
*SSL_get_SSL_CTX()* returns a pointer to the SSL_CTX object, from which
*ssl* was created with *SSL_new* (3).

* RETURN VALUES
The pointer to the SSL_CTX object is returned.

* SEE ALSO
*ssl* (7), *SSL_new* (3)

* COPYRIGHT
Copyright 2001-2016 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
