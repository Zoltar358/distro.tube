#+TITLE: Manpages - unibi_short_name_num.3
#+DESCRIPTION: Linux manpage for unibi_short_name_num.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_name_bool, unibi_name_num, unibi_name_str, unibi_short_name_bool,
unibit_short_name_num, unibi_short_name_str - translate capability enums
to names

* SYNOPSIS
#include <unibilium.h> const char *unibi_name_bool(enum unibi_boolean
b); const char *unibi_name_num(enum unibi_numeric n); const char
*unibi_name_str(enum unibi_string s); const char
*unibi_short_name_bool(enum unibi_boolean b); const char
*unibi_short_name_num(enum unibi_numeric n); const char
*unibi_short_name_str(enum unibi_string s);

* DESCRIPTION
These functions return the names of capabilities as strings. By default
long names (variable names) are returned; the =unibi_short_name_*=
variants use the shorter capnames.

* EXAMPLE
#include <stdio.h> #include <unibilium.h> int main(void) { printf("%s -
%s\n", unibi_name_bool(unibi_has_status_line),
unibi_short_name_bool(unibi_has_status_line)); /* Output:
has_status_line - hs */ }

* SEE ALSO
*unibilium.h* (3)
