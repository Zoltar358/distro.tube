#+TITLE: Manpages - xcb_damage_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_damage_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_damage_notify_event_t -

* SYNOPSIS
*#include <xcb/damage.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_damage_notify_event_t {
      uint8_t             response_type;
      uint8_t             level;
      uint16_t            sequence;
      xcb_drawable_t      drawable;
      xcb_damage_damage_t damage;
      xcb_timestamp_t     timestamp;
      xcb_rectangle_t     area;
      xcb_rectangle_t     geometry;
  } xcb_damage_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_DAMAGE_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- level :: NOT YET DOCUMENTED.

- drawable :: NOT YET DOCUMENTED.

- damage :: NOT YET DOCUMENTED.

- timestamp :: NOT YET DOCUMENTED.

- area :: NOT YET DOCUMENTED.

- geometry :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from damage.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
