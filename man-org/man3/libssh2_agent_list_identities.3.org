#+TITLE: Manpages - libssh2_agent_list_identities.3
#+DESCRIPTION: Linux manpage for libssh2_agent_list_identities.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_agent_list_identities - request an ssh-agent to list of public
keys.

* SYNOPSIS
#include <libssh2.h>

int libssh2_agent_list_identities(LIBSSH2_AGENT *agent);

* DESCRIPTION
Request an ssh-agent to list of public keys, and stores them in the
internal collection of the handle. Call /libssh2_agent_get_identity(3)/
to get a public key off the collection.

* RETURN VALUE
Returns 0 if succeeded, or a negative value for error.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_agent_connect(3)* *libssh2_agent_get_identity(3)*
