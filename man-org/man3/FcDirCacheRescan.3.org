#+TITLE: Manpages - FcDirCacheRescan.3
#+DESCRIPTION: Linux manpage for FcDirCacheRescan.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheRescan - Re-scan a directory cache

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcCache * FcDirCacheRescan (const FcChar8 */dir/*, FcConfig
**/config/*);*

* DESCRIPTION
Re-scan directories only at /dir/ and update the cache. returns NULL if
failed.

* SINCE
version 2.11.1
