#+TITLE: Manpages - CURLOPT_XFERINFODATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_XFERINFODATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_XFERINFODATA - pointer passed to the progress callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_XFERINFODATA, void
*pointer);

* DESCRIPTION
Pass a /pointer/ that will be untouched by libcurl and passed as the
first argument in the progress callback set with
/CURLOPT_XFERINFOFUNCTION(3)/.

This is an alias for /CURLOPT_PROGRESSDATA(3)/.

* DEFAULT
The default value of this parameter is NULL.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
   struct progress {
     char *private;
     size_t size;
   };

   static size_t progress_callback(void *clientp,
                                   curl_off_t dltotal,
                                   curl_off_t dlnow,
                                   curl_off_t ultotal,
                                   curl_off_t ulnow)
   {
     struct memory *progress = (struct progress *)userp;

     /* use the values */

     return 0; /* all is good */
   }

   struct progress data;

   /* pass struct to callback  */
   curl_easy_setopt(curl_handle, CURLOPT_XFERINFODATA, &data);

   curl_easy_setopt(curl_handle, CURLOPT_XFERINFOFUNCTION, progress_callback);
#+end_example

* AVAILABILITY
Added in 7.32.0

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_XFERINFOFUNCTION*(3), *CURLOPT_VERBOSE*(3),
