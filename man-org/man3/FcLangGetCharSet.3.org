#+TITLE: Manpages - FcLangGetCharSet.3
#+DESCRIPTION: Linux manpage for FcLangGetCharSet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangGetCharSet - Get character map for a language

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

const FcCharSet * FcLangGetCharSet (const FcChar8 */lang/*);*

* DESCRIPTION
Returns the FcCharMap for a language.
