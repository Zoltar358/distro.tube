#+TITLE: Manpages - termkey_start.3
#+DESCRIPTION: Linux manpage for termkey_start.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_start, termkey_stop, termkey_is_started - enable or disable
terminal operations

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  int termkey_start(TermKey *tk);
  int termkey_stop(TermKey *tk);

  int termkey_is_started(TermKey *tk);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_start*() enables the terminal IO operations of the given
*termkey*(7) instance, including sending a terminal control sequence and
setting the *termios*(3) modes required.

*termkey_stop*() disables terminal IO operations, by reversing the steps
taken by *termkey_start*(). A newly-constructed *termkey* instance will
have terminal IO enabled already.

*termkey_is_started*() enquires whether terminal IO is currently
enabled.

* RETURN VALUE
If successful, *termkey_start*() and *termkey_stop*() return a true
value. On failure, zero is returned with /errno/ set to indicate the
failure. *termkey_is_started*() returns true or false to indicate
whether terminal IO is currently enabled.

* SEE ALSO
*termkey_new*(3), *termkey*(7)
