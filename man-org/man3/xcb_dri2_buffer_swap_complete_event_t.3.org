#+TITLE: Manpages - xcb_dri2_buffer_swap_complete_event_t.3
#+DESCRIPTION: Linux manpage for xcb_dri2_buffer_swap_complete_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_dri2_buffer_swap_complete_event_t -

* SYNOPSIS
*#include <xcb/dri2.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_dri2_buffer_swap_complete_event_t {
      uint8_t        response_type;
      uint8_t        pad0;
      uint16_t       sequence;
      uint16_t       event_type;
      uint8_t        pad1[2];
      xcb_drawable_t drawable;
      uint32_t       ust_hi;
      uint32_t       ust_lo;
      uint32_t       msc_hi;
      uint32_t       msc_lo;
      uint32_t       sbc;
  } xcb_dri2_buffer_swap_complete_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_DRI2_BUFFER_SWAP_COMPLETE/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- event_type :: NOT YET DOCUMENTED.

- drawable :: NOT YET DOCUMENTED.

- ust_hi :: NOT YET DOCUMENTED.

- ust_lo :: NOT YET DOCUMENTED.

- msc_hi :: NOT YET DOCUMENTED.

- msc_lo :: NOT YET DOCUMENTED.

- sbc :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from dri2.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
