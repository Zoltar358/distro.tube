#+TITLE: Manpages - libssh2_poll_channel_read.3
#+DESCRIPTION: Linux manpage for libssh2_poll_channel_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_poll_channel_read - check if data is available

* SYNOPSIS
#include <libssh2.h>

int libssh2_poll_channel_read(LIBSSH2_CHANNEL *channel, int extended);

* DESCRIPTION
This function is deprecated. Do note use.

/libssh2_poll_channel_read(3)/ checks to see if data is available in the
/channel/'s read buffer. No attempt is made with this method to see if
packets are available to be processed. For full polling support, use
/libssh2_poll(3)/.

* RETURN VALUE
Returns 1 when data is available and 0 otherwise.

* SEE ALSO
*libssh2_poll(3)*
