#+TITLE: Manpages - XkbGetKeyModifierMap.3
#+DESCRIPTION: Linux manpage for XkbGetKeyModifierMap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbGetKeyModifierMap - Update the modifier map for one or more of the
keys in a keyboard description

* SYNOPSIS
*Status XkbGetKeyModifierMap* *( Display **/dpy/* ,* *unsigned int
*/first/* ,* *unsigned int */num/* ,* *XkbDescPtr */xkb/* );*

* ARGUMENTS
- /- dpy/ :: connection to X server

- /- first/ :: keycode of first key to get

- /- num/ :: number of keys for which information is desired

- /- xkb/ :: keyboard description to update

* DESCRIPTION
The /modmap/ entry of the client map is an array, indexed by keycode,
specifying the real modifiers bound to a key. Each entry is a mask
composed of a bitwise inclusive OR of the legal real modifiers:
ShiftMask, LockMask, ControlMask, Mod1Mask, Mod2Mask, Mod3Mask,
Mod4Mask, and Mod5Mask. If a bit is set in a /modmap/ entry, the
corresponding key is bound to that modifier.

Pressing or releasing the key bound to a modifier changes the modifier
set and unset state. The particular manner in which the modifier set and
unset state changes is determined by the behavior and actions assigned
to the key.

/XkbGetKeyModifierMap/ sends a request to the server for the modifier
mappings for /num/ keys starting with the key whose keycode is /first./
It waits for a reply and places the results in the /xkb->map->modmap/
array. If successful, /XkbGetKeyModifier/ returns Success. If the map
component of the /xkb/ parameter has not been allocated,
/XkbGetKeyModifierMap/ allocates and initializes it.

If a compatible version of Xkb is not available in the server or the Xkb
extension has not been properly initialized, /XkbGetKeySyms/ returns
BadAccess. If any allocation errors occur while obtaining the modifier
map, /XkbGetKeyModifierMap/ returns BadAlloc.

* DIAGNOSTICS
- *BadAccess* :: The Xkb extension has not been properly initialized

- *BadAlloc* :: Unable to allocate storage
