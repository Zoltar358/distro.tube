#+TITLE: Manpages - SDL_GetKeyName.3
#+DESCRIPTION: Linux manpage for SDL_GetKeyName.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetKeyName - Get the name of an SDL virtual keysym

* SYNOPSIS
*#include "SDL.h"*

*char *SDL_GetKeyName*(*SDLKey key*);

* DESCRIPTION
Returns the SDL-defined name of the *SDLKey* *key*.

* SEE ALSO
*SDLKey*
