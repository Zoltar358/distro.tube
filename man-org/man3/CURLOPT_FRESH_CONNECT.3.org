#+TITLE: Manpages - CURLOPT_FRESH_CONNECT.3
#+DESCRIPTION: Linux manpage for CURLOPT_FRESH_CONNECT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_FRESH_CONNECT - force a new connection to be used

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FRESH_CONNECT, long
fresh);

* DESCRIPTION
Pass a long. Set to 1 to make the next transfer use a new (fresh)
connection by force instead of trying to re-use an existing one. This
option should be used with caution and only if you understand what it
does as it may seriously impact performance.

Related functionality is /CURLOPT_FORBID_REUSE(3)/ which makes sure the
connection is closed after use so that it will not be re-used.

Set /fresh/ to 0 to have libcurl attempt re-using an existing connection
(default behavior).

* DEFAULT
0

* PROTOCOLS
Most

* EXAMPLE
#+begin_example
  {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_FRESH_CONNECT, 1L);
    /* this transfer must use a new connection, not reuse an existing */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_FORBID_REUSE*(3),
