#+TITLE: Manpages - setutxent.3
#+DESCRIPTION: Linux manpage for setutxent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setutxent.3 is found in manpage for: [[../man3/getutent.3][man3/getutent.3]]