#+TITLE: Manpages - strfromf.3
#+DESCRIPTION: Linux manpage for strfromf.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strfromf.3 is found in manpage for: [[../man3/strfromd.3][man3/strfromd.3]]