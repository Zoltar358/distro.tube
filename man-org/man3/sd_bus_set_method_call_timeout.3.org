#+TITLE: Manpages - sd_bus_set_method_call_timeout.3
#+DESCRIPTION: Linux manpage for sd_bus_set_method_call_timeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_set_method_call_timeout, sd_bus_get_method_call_timeout - Set or
query the default D-Bus method call timeout of a bus object

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_set_method_call_timeout(sd_bus **/bus/*, uint64_t
*/usec/*);*

*int sd_bus_get_method_call_timeout(sd_bus **/bus/*, uint64_t
**/ret/*);*

* DESCRIPTION
*sd_bus_set_method_call_timeout()* sets the default D-Bus method call
timeout of /bus/ to /usec/ microseconds.

*sd_bus_get_method_call_timeout()* queries the default D-Bus method call
timeout of /bus/. If no method call timeout was set using
*sd_bus_set_method_call_timeout()*, the timeout is read from the
/$SYSTEMD_BUS_TIMEOUT/ environment variable. If this environment
variable is unset or does not contain a valid timeout, the
implementation falls back to a predefined method call timeout of 25
seconds. Note that /$SYSTEMD_BUS_TIMEOUT/ is read once and cached so
callers should not rely on being able to change the default method call
timeout at runtime by changing the value of /$SYSTEMD_BUS_TIMEOUT/.
Instead, call *sd_bus_set_method_call_timeout()* to change the default
method call timeout.

* RETURN VALUE
On success, these functions return a non-negative integer. On failure,
they return a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  The parameters /bus/ or /ret/ are *NULL*.
#+end_quote

*-ENOPKG*

#+begin_quote
  Bus object /bus/ could not be resolved.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_call*(3)
