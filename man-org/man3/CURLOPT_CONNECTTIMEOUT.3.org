#+TITLE: Manpages - CURLOPT_CONNECTTIMEOUT.3
#+DESCRIPTION: Linux manpage for CURLOPT_CONNECTTIMEOUT.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_CONNECTTIMEOUT - timeout for the connect phase

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONNECTTIMEOUT, long
timeout);

* DESCRIPTION
Pass a long. It should contain the maximum time in seconds that you
allow the connection phase to the server to take. This only limits the
connection phase, it has no impact once it has connected. Set to zero to
switch to the default built-in connection timeout - 300 seconds. See
also the /CURLOPT_TIMEOUT(3)/ option.

In unix-like systems, this might cause signals to be used unless
/CURLOPT_NOSIGNAL(3)/ is set.

If both /CURLOPT_CONNECTTIMEOUT(3)/ and /CURLOPT_CONNECTTIMEOUT_MS(3)/
are set, the value set last will be used.

* DEFAULT
300

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* complete connection within 10 seconds */
    curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 10L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK. Returns CURLE_BAD_FUNCTION_ARGUMENT if set to a
negative value or a value that when converted to milliseconds is too
large.

* SEE ALSO
*CURLOPT_CONNECTTIMEOUT_MS*(3), *CURLOPT_TIMEOUT*(3),
*CURLOPT_LOW_SPEED_LIMIT*(3),
