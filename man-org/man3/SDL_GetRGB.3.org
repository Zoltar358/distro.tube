#+TITLE: Manpages - SDL_GetRGB.3
#+DESCRIPTION: Linux manpage for SDL_GetRGB.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetRGB - Get RGB values from a pixel in the specified pixel format.

* SYNOPSIS
*#include "SDL.h"*

*void SDL_GetRGB*(*Uint32 pixel, SDL_PixelFormat *fmt, Uint8 *r, Uint8
*g, Uint8 *b*);

* DESCRIPTION
Get RGB component values from a pixel stored in the specified pixel
format.

This function uses the entire 8-bit [0..255] range when converting color
components from pixel formats with less than 8-bits per RGB component
(e.g., a completely white pixel in 16-bit RGB565 format would return
[0xff, 0xff, 0xff] not [0xf8, 0xfc, 0xf8]).

* SEE ALSO
*SDL_GetRGBA*, *SDL_MapRGB*, *SDL_MapRGBA*, *SDL_PixelFormat*
