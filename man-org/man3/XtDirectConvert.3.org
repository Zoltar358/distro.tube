#+TITLE: Manpages - XtDirectConvert.3
#+DESCRIPTION: Linux manpage for XtDirectConvert.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtDirectConvert.3 is found in manpage for: [[../man3/XtConvert.3][man3/XtConvert.3]]