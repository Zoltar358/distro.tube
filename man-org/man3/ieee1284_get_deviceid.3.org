#+TITLE: Manpages - ieee1284_get_deviceid.3
#+DESCRIPTION: Linux manpage for ieee1284_get_deviceid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_get_deviceid - retrieve an IEEE 1284 Device ID

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*ssize_t ieee1284_get_deviceid(struct parport **/port/*, int */daisy/*,
int */flags/*, char **/buffer/*, size_t */len/*);*

* DESCRIPTION
This function is for retrieving the IEEE 1284 Device ID of the specified
device. The device is specified by the /port/ to which it is attached,
and optionally an address (/daisy/) on the daisy chain of devices on
that port.

/daisy/ should be -1 to indicate that the device is not participating in
a IEEE 1284.3 daisy chain, meaning it is the last (or only) device on
the port, or should be a number from 0 to 3 inclusive to indicate that
it has the specified daisy chain address (0 is next to the port).

The /flags/ parameter should be a bitwise union of any flags that the
program wants to use. Available flags are:

*F1284_FRESH*

#+begin_quote
  Guarantee a fresh Device ID. A cached or OS-provided ID will not be
  used.
#+end_quote

The provided /buffer/ must be at least /len/ bytes long, and will
contain the Device ID including the initial two-byte length field and a
terminating zero byte on successful return, or as much of the above as
will fit into the buffer.

* RETURN VALUE
A return value less than zero indicates an error as below. Otherwise,
the return value is the number of bytes of /buffer/ that have been
filled. A return value equal to the length of the buffer indicates that
the Device ID may be longer than the buffer will allow.

*E1284_NOID*

#+begin_quote
  The device did not provide an IEEE 1284 Device ID when interrogated
  (perhaps by the operating system if *F1284_FRESH* was not specified).
#+end_quote

*E1284_NOTIMPL*

#+begin_quote
  One or more of the supplied flags is not supported in this
  implementation, or if no flags were supplied then this function is not
  implemented for this type of port or this type of system. This can
  also be returned if a daisy chain address is specified but daisy chain
  Device IDs are not yet supported.
#+end_quote

*E1284_NOTAVAIL*

#+begin_quote
  *F1284_FRESH* was specified and the library is unable to access the
  port to interrogate the device.
#+end_quote

*E1284_NOMEM*

#+begin_quote
  There is not enough memory.
#+end_quote

*E1284_INIT*

#+begin_quote
  There was a problem initializing the port.
#+end_quote

*E1284_INVALIDPORT*

#+begin_quote
  The /port/ parameter is invalid.
#+end_quote

* NOTES
Unless the *F1284_FRESH* flag is given, the library will try to find the
devices ID as unobtrusively as possible. First it will ask the operating
system if it knows it, and then it will try actually asking the device
for it. Because of this, the Device ID may be partially computed (the
length field, for example) or even partially missing if the operating
system has only remembered some parts of the ID. To guarantee that you
are getting the bytes that the device sent, use *F1284_FRESH*. Be aware
that the operating system may allow any user to inspect the Device IDs
that it provides, whereas device access is normally more restricted.

The initial two-byte length field is a big-endian 16 bit unsigned
integer provided by the device and may not be accurate. In particular,
it is meant to indicate the length of the entire string including the
length field itself; however, some manufacturers exclude the length
field or just set the length field to some arbitrary number greater than
the ID length.

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
