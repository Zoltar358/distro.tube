#+TITLE: Manpages - ExtUtils_MM_BeOS.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_BeOS.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_BeOS - methods to override UN*X behaviour in
ExtUtils::MakeMaker

* SYNOPSIS
use ExtUtils::MM_BeOS; # Done internally by ExtUtils::MakeMaker if
needed

* DESCRIPTION
See ExtUtils::MM_Unix for a documentation of the methods provided there.
This package overrides the implementation of these methods, not the
semantics.

- os_flavor :: BeOS is BeOS.

- init_linker :: libperl.a equivalent to be linked to dynamic
  extensions.
