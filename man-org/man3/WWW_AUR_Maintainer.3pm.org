#+TITLE: Manpages - WWW_AUR_Maintainer.3pm
#+DESCRIPTION: Linux manpage for WWW_AUR_Maintainer.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
WWW::AUR::Maintainer - List packages owned by a given maintainer.

* SYNOPSIS
my $maint = $aurobj->maintainer( juster ); # or ... my $maint =
WWW::AUR::Maintainer->new( juster ); my $name = $maint->name; my @pkgs =
$maint->packages;

* CONSTRUCTOR
$OBJ = WWW::AUR::Maintainer->new( $NAME, %PATH_PARAMS? );

If the maintainer matching the given name does not exist, it is hard to
tell. Currently if a bad maintainer name is given, the results of
packages will return an empty list.

- $NAME :: The name of the maintainer.

- %PATH_PARAMS (Optional) :: These are propogated to the
  WWW::AUR::Package objects created by packages. See PATH PARAMETERS in
  WWW::AUR for more info.

* METHODS
** name
$MNAME = $OBJ->name

- $MNAME :: The name of the maintainer as given to the constructor.

** packages
@PKGOBJS = $OBJ->packages

1. A list of WWW::AUR::Package objects. These represent the packages
   that are owned by the given maintainer. The list can be empty. If the
   maintainer named does not exist, the list will be empty.

* SEE ALSO
WWW::AUR

* AUTHOR
Justin Davis, =<juster at cpan dot org>=

* BUGS
Please email me any bugs you find. I will try to fix them as quick as I
can.

* SUPPORT
Send me an email if you have any questions or need help.

* LICENSE AND COPYRIGHT
Copyright 2014 Justin Davis.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
