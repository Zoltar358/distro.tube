#+TITLE: Manpages - CURLOPT_SSLKEYTYPE.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSLKEYTYPE.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSLKEYTYPE - type of the private key file

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLKEYTYPE, char *type);

* DESCRIPTION
Pass a pointer to a null-terminated string as parameter. The string
should be the format of your private key. Supported formats are "PEM",
"DER" and "ENG".

The format "ENG" enables you to load the private key from a crypto
engine. In this case /CURLOPT_SSLKEY(3)/ is used as an identifier passed
to the engine. You have to set the crypto engine with
/CURLOPT_SSLENGINE(3)/. "DER" format key file currently does not work
because of a bug in OpenSSL.

The application does not have to keep the string around after setting
this option.

* DEFAULT
"PEM"

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
    curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
    curl_easy_setopt(curl, CURLOPT_SSLKEYTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
If built TLS enabled.

* RETURN VALUE
Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or
CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_SSLKEY*(3), *CURLOPT_SSLCERT*(3),
