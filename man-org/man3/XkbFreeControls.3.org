#+TITLE: Manpages - XkbFreeControls.3
#+DESCRIPTION: Linux manpage for XkbFreeControls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbFreeControls - Frees memory used by the ctrls member of an XkbDescRec
structure

* SYNOPSIS
*void XkbFreeControls* *( XkbDescPtr */xkb/* ,* *unsigned int
*/which/* ,* *Bool */free_all/* );*

* ARGUMENTS
- /- xkb/ :: Xkb description in which to free controls components

- /- which/ :: mask of components of ctrls to free

- /- free_all/ :: True => free everything + ctrls itself

* DESCRIPTION
/XkbFreeControls/ frees the specified components of the /ctrls/ field in
the /xkb/ keyboard description and sets the corresponding structure
component values to NULL or zero. The /which/ mask specifies the fields
of /ctrls/ to be freed and can contain any of the controls components
specified in Table 1.

Table 1 shows the actual values for the individual mask bits used to
select controls for modification and to enable and disable the control.
Note that the same mask bit is used to specify general modifications to
the parameters used to configure the control (which), and to enable and
disable the control (enabled_ctrls). The anomalies in the table (no "ok"
in column) are for controls that have no configurable attributes; and
for controls that are not boolean controls and therefore cannot be
enabled or disabled.

TABLE

If /free_all/ is True, /XkbFreeControls/ frees every non-NULL structure
component in the controls, frees the XkbControlsRec structure referenced
by the /ctrls/ member of /xkb,/ and sets /ctrls/ to NULL.
