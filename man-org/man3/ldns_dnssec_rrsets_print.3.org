#+TITLE: Manpages - ldns_dnssec_rrsets_print.3
#+DESCRIPTION: Linux manpage for ldns_dnssec_rrsets_print.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_dnssec_rrsets_new, ldns_dnssec_rrsets_free,
ldns_dnssec_rrsets_type, ldns_dnssec_rrsets_set_type,
ldns_dnssec_rrsets_add_rr, ldns_dnssec_rrsets_print - functions for
ldns_dnssec_rrsets

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_dnssec_rrsets* ldns_dnssec_rrsets_new(void);

void ldns_dnssec_rrsets_free(ldns_dnssec_rrsets *rrsets);

ldns_rr_type ldns_dnssec_rrsets_type(const ldns_dnssec_rrsets *rrsets);

ldns_status ldns_dnssec_rrsets_set_type(ldns_dnssec_rrsets *rrsets,
ldns_rr_type type);

ldns_status ldns_dnssec_rrsets_add_rr(ldns_dnssec_rrsets *rrsets,
ldns_rr *rr);

void ldns_dnssec_rrsets_print(FILE *out, const ldns_dnssec_rrsets
*rrsets, bool follow);

* DESCRIPTION
/ldns_dnssec_rrsets_new/() Creates a new list (entry) of RRsets .br
Returns the newly allocated structure

/ldns_dnssec_rrsets_free/() Frees the list of rrsets and their rrs, but
*not* the ldns_rr records in the sets

.br *rrsets*: the data structure to free

/ldns_dnssec_rrsets_type/() Returns the rr type of the rrset (that is
head of the given list)

.br *rrsets*: the rrset to get the type of .br Returns the rr type

/ldns_dnssec_rrsets_set_type/() Sets the RR type of the rrset (that is
head of the given list)

.br *rrsets*: the rrset to set the type of .br *type*: the type to set
.br Returns LDNS_STATUS_OK on success

/ldns_dnssec_rrsets_add_rr/() Add an ldns_rr to the corresponding RRset
in the given list of RRsets. If it is not present, add it as a new RRset
with 1 record.

.br *rrsets*: the list of rrsets to add the RR to .br *rr*: the rr to
add to the list of rrsets .br Returns LDNS_STATUS_OK on success

/ldns_dnssec_rrsets_print/() Print the given list of rrsets to the fiven
file descriptor

.br *out*: the file descriptor to print to .br *rrsets*: the list of
RRsets to print .br *follow*: if set to false, only print the first
RRset

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_dnssec_zone/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
