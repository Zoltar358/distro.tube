#+TITLE: Manpages - termkey_get_keyname.3
#+DESCRIPTION: Linux manpage for termkey_get_keyname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_get_keyname - return a string name for a symbolic key

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  const char *termkey_get_keyname(TermKey *tk, TermKeySym sym);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_get_keyname*() returns a human-readable string name for the
symbolic key value given by /sym/. The returned string is owned by the
*termkey*(7) instance /tk/ so should not be modified or freed. The
returned pointer is guaranteed to be valid until the instance is
released using *termkey_destroy*(3). This function is the inverse of
*termkey_keyname2sym*(3).

* RETURN VALUE
*termkey_get_key*() returns a pointer to a string.

* SEE ALSO
*termkey_lookup_keyname*(3), *termkey_keyname2sym*(3),
*termkey_strfkey*(3), *termkey*(7)
