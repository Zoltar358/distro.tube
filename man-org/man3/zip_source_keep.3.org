#+TITLE: Manpages - zip_source_keep.3
#+DESCRIPTION: Linux manpage for zip_source_keep.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The function

increments the reference count of

was added in libzip 1.0.

and
