#+TITLE: Manpages - get_nprocs.3
#+DESCRIPTION: Linux manpage for get_nprocs.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about get_nprocs.3 is found in manpage for: [[../man3/get_nprocs_conf.3][man3/get_nprocs_conf.3]]