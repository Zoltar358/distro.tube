#+TITLE: Manpages - libssh2_channel_window_read.3
#+DESCRIPTION: Linux manpage for libssh2_channel_window_read.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_channel_window_read - convenience macro for
/libssh2_channel_window_read_ex(3)/ calls

* SYNOPSIS
#include <libssh2.h>

unsigned long libssh2_channel_window_read(LIBSSH2_CHANNEL *channel);

* DESCRIPTION
This is a macro defined in a public libssh2 header file that is using
the underlying function /libssh2_channel_window_read_ex(3)/.

* RETURN VALUE
See /libssh2_channel_window_read_ex(3)/

* ERRORS
See /libssh2_channel_window_read_ex(3)/

* SEE ALSO
*libssh2_channel_window_read_ex(3)*
