#+TITLE: Manpages - CURLOPT_STDERR.3
#+DESCRIPTION: Linux manpage for CURLOPT_STDERR.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_STDERR - redirect stderr to another stream

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_STDERR, FILE *stream);

* DESCRIPTION
Pass a FILE * as parameter. Tell libcurl to use this /stream/ instead of
stderr when showing the progress meter and displaying
/CURLOPT_VERBOSE(3)/ data.

* DEFAULT
stderr

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  FILE *filep = fopen("dump", "wb");
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
    curl_easy_setopt(curl, CURLOPT_STDERR, filep);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_VERBOSE*(3), *CURLOPT_NOPROGRESS*(3),
