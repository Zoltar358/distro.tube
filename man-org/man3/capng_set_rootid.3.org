#+TITLE: Manpages - capng_set_rootid.3
#+DESCRIPTION: Linux manpage for capng_set_rootid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
capng_set_rootid - set namespace root id

* SYNOPSIS
*#include <cap-ng.h>*

int capng_set_rootid(int rootid);

* DESCRIPTION
capng_set_rootid sets the rootid for capabilities operations. This is
only applicable for file system operations.

* RETURN VALUE
On success, it returns 0. It returns -1 if there is an internal error or
the kernel does not suppor V3 filesystem capabilities.

* SEE ALSO
*capng_set_caps_fd*(3), *capabilities*(7)

* AUTHOR
Steve Grubb
