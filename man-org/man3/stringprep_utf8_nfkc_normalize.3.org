#+TITLE: Manpages - stringprep_utf8_nfkc_normalize.3
#+DESCRIPTION: Linux manpage for stringprep_utf8_nfkc_normalize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
stringprep_utf8_nfkc_normalize - API function

* SYNOPSIS
*#include <stringprep.h>*

*char * stringprep_utf8_nfkc_normalize(const char * */str/*, ssize_t
*/len/*);*

* ARGUMENTS
- const char * str :: a UTF-8 encoded string.

- ssize_t len :: length of /str/ , in bytes, or -1 if /str/ is
  nul-terminated.

* DESCRIPTION
Converts a string into canonical form, standardizing such issues as
whether a character with an accent is represented as a base character
and combining accent or as a single precomposed character.

The normalization mode is NFKC (ALL COMPOSE). It standardizes
differences that do not affect the text content, such as the
above-mentioned accent representation. It standardizes the
"compatibility" characters in Unicode, such as SUPERSCRIPT THREE to the
standard forms (in this case DIGIT THREE). Formatting information may be
lost but for most text operations such characters should be considered
the same. It returns a result with composed forms rather than a
maximally decomposed form.

Return value: a newly allocated string, that is the NFKC normalized form
of /str/ .

* REPORTING BUGS
Report bugs to <help-libidn@gnu.org>.\\
General guidelines for reporting bugs: http://www.gnu.org/gethelp/\\
GNU Libidn home page: http://www.gnu.org/software/libidn/

* COPYRIGHT
Copyright © 2002-2021 Simon Josefsson.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libidn* is maintained as a Texinfo manual.
If the *info* and *libidn* programs are properly installed at your site,
the command

#+begin_quote
  *info libidn*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *http://www.gnu.org/software/libidn/manual/*
#+end_quote
