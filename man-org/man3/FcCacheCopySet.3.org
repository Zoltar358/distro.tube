#+TITLE: Manpages - FcCacheCopySet.3
#+DESCRIPTION: Linux manpage for FcCacheCopySet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCacheCopySet - Returns a copy of the fontset from cache

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcFontSet * FcCacheCopySet (const FcCache */cache/*);*

* DESCRIPTION
The returned fontset contains each of the font patterns from /cache/.
This fontset may be modified, but the patterns from the cache are
read-only.
