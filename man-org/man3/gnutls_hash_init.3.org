#+TITLE: Manpages - gnutls_hash_init.3
#+DESCRIPTION: Linux manpage for gnutls_hash_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_hash_init - API function

* SYNOPSIS
*#include <gnutls/crypto.h>*

*int gnutls_hash_init(gnutls_hash_hd_t * */dig/*,
gnutls_digest_algorithm_t */algorithm/*);*

* ARGUMENTS
- gnutls_hash_hd_t * dig :: is a *gnutls_hash_hd_t* type

- gnutls_digest_algorithm_t algorithm :: the hash algorithm to use

* DESCRIPTION
This function will initialize an context that can be used to produce a
Message Digest of data. This will effectively use the current crypto
backend in use by gnutls or the cryptographic accelerator in use.

* RETURNS
Zero or a negative error code on error.

* SINCE
2.10.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
