#+TITLE: Manpages - __gnu_parallel___generic_find_selector.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___generic_find_selector.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__generic_find_selector - Base class of all
__gnu_parallel::__find_template selectors.

* SYNOPSIS
\\

=#include <find_selectors.h>=

Inherited by *__gnu_parallel::__adjacent_find_selector*,
*__gnu_parallel::__find_first_of_selector< _FIterator >*,
*__gnu_parallel::__find_if_selector*, and
*__gnu_parallel::__mismatch_selector*.

* Detailed Description
Base class of all __gnu_parallel::__find_template selectors.

Definition at line *43* of file *find_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
