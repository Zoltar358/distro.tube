#+TITLE: Manpages - XSetClipMask.3
#+DESCRIPTION: Linux manpage for XSetClipMask.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XSetClipMask.3 is found in manpage for: [[../man3/XSetClipOrigin.3][man3/XSetClipOrigin.3]]