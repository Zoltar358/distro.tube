#+TITLE: Manpages - gnutls_pkcs11_obj_get_ptr.3
#+DESCRIPTION: Linux manpage for gnutls_pkcs11_obj_get_ptr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_pkcs11_obj_get_ptr - API function

* SYNOPSIS
*#include <gnutls/pkcs11.h>*

*int gnutls_pkcs11_obj_get_ptr(gnutls_pkcs11_obj_t */obj/*, void **
*/ptr/*, void ** */session/*, void ** */ohandle/*, unsigned long *
*/slot_id/*, unsigned int */flags/*);*

* ARGUMENTS
- gnutls_pkcs11_obj_t obj :: should contain a *gnutls_pkcs11_obj_t* type

- void ** ptr :: will contain the CK_FUNCTION_LIST_PTR pointer (may be
  *NULL*)

- void ** session :: will contain the CK_SESSION_HANDLE of the object

- void ** ohandle :: will contain the CK_OBJECT_HANDLE of the object

- unsigned long * slot_id :: the identifier of the slot (may be *NULL*)

- unsigned int flags :: Or sequence of GNUTLS_PKCS11_OBJ_* flags

* DESCRIPTION
Obtains the PKCS*11* session handles of an object. /session/ and
/ohandle/ must be deinitialized by the caller. The returned pointers are
independent of the /obj/ lifetime.

* RETURNS
*GNUTLS_E_SUCCESS* (0) on success or a negative error code on error.

* SINCE
3.6.3

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
