#+TITLE: Manpages - sha2.3
#+DESCRIPTION: Linux manpage for sha2.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
The SHA2 functions implement the NIST Secure Hash Standard, FIPS PUB
180-2. The SHA2 functions are used to generate a condensed
representation of a message called a message digest, suitable for use as
a digital signature. There are three families of functions, with names
corresponding to the number of bits in the resulting message digest. The
SHA-256 functions are limited to processing a message of less than 2^64
bits as input. The SHA-384 and SHA-512 functions can process a message
of at most 2^128 - 1 bits as input.

The SHA2 functions are considered to be more secure than the

functions with which they share a similar interface. The 256, 384, and
512-bit versions of SHA2 share the same interface. For brevity, only the
256-bit variants are described below.

The

function initializes a SHA2_CTX

for use with

and

The

function adds

of length

to the SHA2_CTX specified by

is called when all data has been added via

and stores a message digest in the

parameter.

The

function can be used to apply padding to the message digest as in

but the current context can still be used with

The

function is used by

to hash 512-bit blocks and forms the core of the algorithm. Most
programs should use the interface provided by

and

instead of calling

directly.

The

function is a front end for

which converts the digest into an

representation of the digest in hexadecimal.

The

function calculates the digest for a file and returns the result via

If

is unable to open the file, a

pointer is returned.

behaves like

but calculates the digest only for that portion of the file starting at

and continuing for

bytes or until end of file is reached, whichever comes first. A zero

can be specified to read until end of file. A negative

or

will be ignored.

The

function calculates the digest of an arbitrary string and returns the
result via

For each of the

and

functions the

parameter should either be a string large enough to hold the resulting
digest (e.g.

or

depending on the function being used) or a

pointer. In the latter case, space will be dynamically allocated via

and should be freed using

when it is no longer needed.

The following code fragment will calculate the SHA-256 digest for the
string

which is

SHA2_CTX ctx; uint8_t results[SHA256_DIGEST_LENGTH]; char *buf; int n;

buf = "abc"; n = strlen(buf); SHA256Init(&ctx); SHA256Update(&ctx,
(uint8_t *)buf, n); SHA256Final(results, &ctx);

/* Print the digest as one long hex value */ printf("0x"); for (n = 0; n
SHA256_DIGEST_LENGTH; n++) printf("%02x", results[n]); putchar('\n');

Alternately, the helper functions could be used in the following way:

uint8_t output[SHA256_DIGEST_STRING_LENGTH]; char *buf = "abc";

printf("0x%s\n", SHA256Data(buf, strlen(buf), output));

The SHA2 functions appeared in

This implementation of the SHA functions was written by Aaron D.
Gifford.

The

and

helper functions are derived from code written by Poul-Henning Kamp.

This implementation of the Secure Hash Standard has not been validated
by NIST and as such is not in official compliance with the standard.

If a message digest is to be copied to a multi-byte type (i.e. an array
of 32-bit integers) it will be necessary to perform byte swapping on
little endian machines such as the i386, alpha, and vax.
