#+TITLE: Manpages - FcMatrixScale.3
#+DESCRIPTION: Linux manpage for FcMatrixScale.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcMatrixScale - Scale a matrix

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcMatrixScale (FcMatrix */matrix/*, double */sx/*, double */dy/*);*

* DESCRIPTION
*FcMatrixScale* multiplies /matrix/ x values by /sx/ and y values by
/dy/. This is done by multiplying by the matrix:

#+begin_example
     sx  0
     0   dy
#+end_example
