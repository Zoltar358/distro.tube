#+TITLE: Manpages - acl_get_perm.3
#+DESCRIPTION: Linux manpage for acl_get_perm.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function tests if the permission specified by the argument

(one of ACL_READ, ACL_WRITE, ACL_EXECUTE) is contained in the ACL
permission set pointed to by the argument

Any existing descriptors that refer to

continue to refer to that permission set.

If successful, the

function returns

if the permission specified by

is contained in the ACL permission set

and

if the permission is not contained in the permission set. Otherwise, the
value

is returned and the global variable

is set to indicate the error.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid descriptor for a permission set within an ACL entry.

The argument

is not a valid

value.

This is a non-portable, Linux specific extension to the ACL manipulation
functions defined in IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned).

Written by
