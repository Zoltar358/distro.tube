#+TITLE: Manpages - FcLangSetContains.3
#+DESCRIPTION: Linux manpage for FcLangSetContains.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcLangSetContains - check langset subset relation

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcLangSetContains (const FcLangSet */ls_a/*, const FcLangSet
**/ls_b/*);*

* DESCRIPTION
*FcLangSetContains* returns FcTrue if /ls_a/ contains every language in
/ls_b/. /ls_a/ will 'contain' a language from /ls_b/ if /ls_a/ has
exactly the language, or either the language or /ls_a/ has no territory.
