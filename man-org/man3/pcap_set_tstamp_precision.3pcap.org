#+TITLE: Manpages - pcap_set_tstamp_precision.3pcap
#+DESCRIPTION: Linux manpage for pcap_set_tstamp_precision.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_set_tstamp_precision - set the time stamp precision returned in
captures

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_set_tstamp_precision(pcap_t *p, int tstamp_precision);
#+end_example

* DESCRIPTION
*pcap_set_tstamp_precision*() sets the precision of the time stamp
desired for packets captured on the pcap descriptor to the type
specified by /tstamp_precision/. It must be called on a pcap descriptor
created by *pcap_create*(3PCAP) that has not yet been activated by
*pcap_activate*(3PCAP). Two time stamp precisions are supported,
microseconds and nanoseconds. One can use options
*PCAP_TSTAMP_PRECISION_MICRO and* *PCAP_TSTAMP_PRECISION_NANO* to
request desired precision. By default, time stamps are in microseconds.

* RETURN VALUE
*pcap_set_tstamp_precision*() returns *0* on success if the specified
time stamp precision is expected to be supported by the capture device,
*PCAP_ERROR_TSTAMP_PRECISION_NOTSUP* if the capture device does not
support the requested time stamp precision, *PCAP_ERROR_ACTIVATED* if
called on a capture handle that has been activated.

* BACKWARD COMPATIBILITY
This function became available in libpcap release 1.5.1. In previous
releases, time stamps from a capture device or savefile are always given
in seconds and microseconds.

* SEE ALSO
*pcap*(3PCAP), *pcap_get_tstamp_precision*(3PCAP), *pcap-tstamp*(7)
