#+TITLE: Manpages - acl_delete_def_file.3
#+DESCRIPTION: Linux manpage for acl_delete_def_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function deletes a default ACL from the directory whose pathname is
pointed to by the argument

The effective user ID of the process must match the owner of the file or
directory or the process must have the CAP_FOWNER capability for the
request to succeed.

If the argument

is not a directory, then the function fails. It is no error if the
directory whose pathname is pointed to by the argument

does not have a default ACL.

If any of the following conditions occur, the

function returns the value

and and sets

to the corresponding value:

The file referred to by

is not a directory.

The file system on which the file identified by

is located does not support ACLs, or ACLs are disabled.

The process does not have appropriate privilege to perform the operation
to delete the default ACL.

This function requires modification of a file system which is currently
read-only.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
