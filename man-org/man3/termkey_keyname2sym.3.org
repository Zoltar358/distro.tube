#+TITLE: Manpages - termkey_keyname2sym.3
#+DESCRIPTION: Linux manpage for termkey_keyname2sym.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
termkey_keyname2sym - look up a symbolic key value for a string name

* SYNOPSIS
#+begin_example
  #include <termkey.h>

  TermKeySym termkey_keyname2sym(TermKey *tk, const char *keyname);
#+end_example

Link with /-ltermkey/.

* DESCRIPTION
*termkey_keyname2sym*() looks up the symbolic key value represented by
the given string name. This is a case-sensitive comparison. If the given
name is not found, *TERMKEY_SYM_UNKNOWN* is returned instead. This
function is the inverse of *termkey_get_keyname*(3), and is a more
specific form of *termkey_lookup_keyname*(3) which only recognises names
as complete strings.

Because the key names are stored in an array indexed by the symbol
number, this function has to perform a linear search of the names. Use
of this function should be restricted to converting key names into
symbolic values during a program's initialisation, so that efficient
comparisons can be done while it is running.

* RETURN VALUE
*termkey_keyname2sym*() returns a symbolic key constant, or
*TERMKEY_SYM_UNKNOWN*.

* SEE ALSO
*termkey_get_keyname*(3), *termkey_lookup_keyname*(3),
*termkey_strpkey*(3), *termkey*(7)
