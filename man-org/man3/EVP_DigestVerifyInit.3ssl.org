#+TITLE: Manpages - EVP_DigestVerifyInit.3ssl
#+DESCRIPTION: Linux manpage for EVP_DigestVerifyInit.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_DigestVerifyInit, EVP_DigestVerifyUpdate, EVP_DigestVerifyFinal,
EVP_DigestVerify - EVP signature verification functions

* SYNOPSIS
#include <openssl/evp.h> int EVP_DigestVerifyInit(EVP_MD_CTX *ctx,
EVP_PKEY_CTX **pctx, const EVP_MD *type, ENGINE *e, EVP_PKEY *pkey); int
EVP_DigestVerifyUpdate(EVP_MD_CTX *ctx, const void *d, size_t cnt); int
EVP_DigestVerifyFinal(EVP_MD_CTX *ctx, const unsigned char *sig, size_t
siglen); int EVP_DigestVerify(EVP_MD_CTX *ctx, const unsigned char
*sigret, size_t siglen, const unsigned char *tbs, size_t tbslen);

* DESCRIPTION
The EVP signature routines are a high-level interface to digital
signatures.

*EVP_DigestVerifyInit()* sets up verification context *ctx* to use
digest *type* from ENGINE *e* and public key *pkey*. *ctx* must be
created with *EVP_MD_CTX_new()* before calling this function. If *pctx*
is not NULL, the EVP_PKEY_CTX of the verification operation will be
written to **pctx*: this can be used to set alternative verification
options. Note that any existing value in **pctx* is overwritten. The
EVP_PKEY_CTX value returned must not be freed directly by the
application if *ctx* is not assigned an EVP_PKEY_CTX value before being
passed to *EVP_DigestVerifyInit()* (which means the EVP_PKEY_CTX is
created inside *EVP_DigestVerifyInit()* and it will be freed
automatically when the EVP_MD_CTX is freed).

No *EVP_PKEY_CTX* will be created by *EVP_DigestSignInit()* if the
passed *ctx* has already been assigned one via
*EVP_MD_CTX_set_pkey_ctx* (3). See also *SM2* (7).

*EVP_DigestVerifyUpdate()* hashes *cnt* bytes of data at *d* into the
verification context *ctx*. This function can be called several times on
the same *ctx* to include additional data. This function is currently
implemented using a macro.

*EVP_DigestVerifyFinal()* verifies the data in *ctx* against the
signature in *sig* of length *siglen*.

*EVP_DigestVerify()* verifies *tbslen* bytes at *tbs* against the
signature in *sig* of length *siglen*.

* RETURN VALUES
*EVP_DigestVerifyInit()* and *EVP_DigestVerifyUpdate()* return 1 for
success and 0 for failure.

*EVP_DigestVerifyFinal()* and *EVP_DigestVerify()* return 1 for success;
any other value indicates failure. A return value of zero indicates that
the signature did not verify successfully (that is, *tbs* did not match
the original data or the signature had an invalid form), while other
values indicate a more serious error (and sometimes also indicate an
invalid signature form).

The error codes can be obtained from *ERR_get_error* (3).

* NOTES
The *EVP* interface to digital signatures should almost always be used
in preference to the low-level interfaces. This is because the code then
becomes transparent to the algorithm used and much more flexible.

*EVP_DigestVerify()* is a one shot operation which verifies a single
block of data in one function. For algorithms that support streaming it
is equivalent to calling *EVP_DigestVerifyUpdate()* and
*EVP_DigestVerifyFinal()*. For algorithms which do not support streaming
(e.g. PureEdDSA) it is the only way to verify data.

In previous versions of OpenSSL there was a link between message digest
types and public key algorithms. This meant that clone digests such as
*EVP_dss1()* needed to be used to sign using SHA1 and DSA. This is no
longer necessary and the use of clone digest is now discouraged.

For some key types and parameters the random number generator must be
seeded. If the automatic seeding or reseeding of the OpenSSL CSPRNG
fails due to external circumstances (see *RAND* (7)), the operation will
fail.

The call to *EVP_DigestVerifyFinal()* internally finalizes a copy of the
digest context. This means that *EVP_VerifyUpdate()* and
*EVP_VerifyFinal()* can be called later to digest and verify additional
data.

Since only a copy of the digest context is ever finalized, the context
must be cleaned up after use by calling *EVP_MD_CTX_free()* or a memory
leak will occur.

* SEE ALSO
*EVP_DigestSignInit* (3), *EVP_DigestInit* (3), *evp* (7), *HMAC* (3),
*MD2* (3), *MD5* (3), *MDC2* (3), *RIPEMD160* (3), *SHA1* (3),
*dgst* (1), *RAND* (7)

* HISTORY
*EVP_DigestVerifyInit()*, *EVP_DigestVerifyUpdate()* and
*EVP_DigestVerifyFinal()* were added in OpenSSL 1.0.0.

* COPYRIGHT
Copyright 2006-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
