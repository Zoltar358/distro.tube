#+TITLE: Manpages - XResQueryClientResources.3
#+DESCRIPTION: Linux manpage for XResQueryClientResources.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XResQueryClientResources.3 is found in manpage for: [[../man3/XRes.3][man3/XRes.3]]