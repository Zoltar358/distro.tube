#+TITLE: Manpages - sd-bus-errors.3
#+DESCRIPTION: Linux manpage for sd-bus-errors.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd-bus-errors, SD_BUS_ERROR_FAILED, SD_BUS_ERROR_NO_MEMORY,
SD_BUS_ERROR_SERVICE_UNKNOWN, SD_BUS_ERROR_NAME_HAS_NO_OWNER,
SD_BUS_ERROR_NO_REPLY, SD_BUS_ERROR_IO_ERROR, SD_BUS_ERROR_BAD_ADDRESS,
SD_BUS_ERROR_NOT_SUPPORTED, SD_BUS_ERROR_LIMITS_EXCEEDED,
SD_BUS_ERROR_ACCESS_DENIED, SD_BUS_ERROR_AUTH_FAILED,
SD_BUS_ERROR_NO_SERVER, SD_BUS_ERROR_TIMEOUT, SD_BUS_ERROR_NO_NETWORK,
SD_BUS_ERROR_ADDRESS_IN_USE, SD_BUS_ERROR_DISCONNECTED,
SD_BUS_ERROR_INVALID_ARGS, SD_BUS_ERROR_FILE_NOT_FOUND,
SD_BUS_ERROR_FILE_EXISTS, SD_BUS_ERROR_UNKNOWN_METHOD,
SD_BUS_ERROR_UNKNOWN_OBJECT, SD_BUS_ERROR_UNKNOWN_INTERFACE,
SD_BUS_ERROR_UNKNOWN_PROPERTY, SD_BUS_ERROR_PROPERTY_READ_ONLY,
SD_BUS_ERROR_UNIX_PROCESS_ID_UNKNOWN, SD_BUS_ERROR_INVALID_SIGNATURE,
SD_BUS_ERROR_INCONSISTENT_MESSAGE, SD_BUS_ERROR_MATCH_RULE_NOT_FOUND,
SD_BUS_ERROR_MATCH_RULE_INVALID,
SD_BUS_ERROR_INTERACTIVE_AUTHORIZATION_REQUIRED - Standard D-Bus error
names

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

#+begin_example
  #define SD_BUS_ERROR_FAILED                     "org.freedesktop.DBus.Error.Failed"
  #define SD_BUS_ERROR_NO_MEMORY                  "org.freedesktop.DBus.Error.NoMemory"
  #define SD_BUS_ERROR_SERVICE_UNKNOWN            "org.freedesktop.DBus.Error.ServiceUnknown"
  #define SD_BUS_ERROR_NAME_HAS_NO_OWNER          "org.freedesktop.DBus.Error.NameHasNoOwner"
  #define SD_BUS_ERROR_NO_REPLY                   "org.freedesktop.DBus.Error.NoReply"
  #define SD_BUS_ERROR_IO_ERROR                   "org.freedesktop.DBus.Error.IOError"
  #define SD_BUS_ERROR_BAD_ADDRESS                "org.freedesktop.DBus.Error.BadAddress"
  #define SD_BUS_ERROR_NOT_SUPPORTED              "org.freedesktop.DBus.Error.NotSupported"
  #define SD_BUS_ERROR_LIMITS_EXCEEDED            "org.freedesktop.DBus.Error.LimitsExceeded"
  #define SD_BUS_ERROR_ACCESS_DENIED              "org.freedesktop.DBus.Error.AccessDenied"
  #define SD_BUS_ERROR_AUTH_FAILED                "org.freedesktop.DBus.Error.AuthFailed"
  #define SD_BUS_ERROR_NO_SERVER                  "org.freedesktop.DBus.Error.NoServer"
  #define SD_BUS_ERROR_TIMEOUT                    "org.freedesktop.DBus.Error.Timeout"
  #define SD_BUS_ERROR_NO_NETWORK                 "org.freedesktop.DBus.Error.NoNetwork"
  #define SD_BUS_ERROR_ADDRESS_IN_USE             "org.freedesktop.DBus.Error.AddressInUse"
  #define SD_BUS_ERROR_DISCONNECTED               "org.freedesktop.DBus.Error.Disconnected"
  #define SD_BUS_ERROR_INVALID_ARGS               "org.freedesktop.DBus.Error.InvalidArgs"
  #define SD_BUS_ERROR_FILE_NOT_FOUND             "org.freedesktop.DBus.Error.FileNotFound"
  #define SD_BUS_ERROR_FILE_EXISTS                "org.freedesktop.DBus.Error.FileExists"
  #define SD_BUS_ERROR_UNKNOWN_METHOD             "org.freedesktop.DBus.Error.UnknownMethod"
  #define SD_BUS_ERROR_UNKNOWN_OBJECT             "org.freedesktop.DBus.Error.UnknownObject"
  #define SD_BUS_ERROR_UNKNOWN_INTERFACE          "org.freedesktop.DBus.Error.UnknownInterface"
  #define SD_BUS_ERROR_UNKNOWN_PROPERTY           "org.freedesktop.DBus.Error.UnknownProperty"
  #define SD_BUS_ERROR_PROPERTY_READ_ONLY         "org.freedesktop.DBus.Error.PropertyReadOnly"
  #define SD_BUS_ERROR_UNIX_PROCESS_ID_UNKNOWN    "org.freedesktop.DBus.Error.UnixProcessIdUnknown"
  #define SD_BUS_ERROR_INVALID_SIGNATURE          "org.freedesktop.DBus.Error.InvalidSignature"
  #define SD_BUS_ERROR_INCONSISTENT_MESSAGE       "org.freedesktop.DBus.Error.InconsistentMessage"
  #define SD_BUS_ERROR_MATCH_RULE_NOT_FOUND       "org.freedesktop.DBus.Error.MatchRuleNotFound"
  #define SD_BUS_ERROR_MATCH_RULE_INVALID         "org.freedesktop.DBus.Error.MatchRuleInvalid"
  #define SD_BUS_ERROR_INTERACTIVE_AUTHORIZATION_REQUIRED \
                                                  "org.freedesktop.DBus.Error.InteractiveAuthorizationRequired"
#+end_example

* DESCRIPTION
In addition to the error names user programs define, D-Bus knows a
number of generic, standardized error names that are listed below.

In addition to this list, in sd-bus, the special error namespace
"System.Error." is used to map arbitrary Linux system errors (as defined
by *errno*(3)) to D-Bus errors and back. For example, the error
*EUCLEAN* is mapped to "System.Error.EUCLEAN" and back.

*SD_BUS_ERROR_FAILED*

#+begin_quote
  A generic error indication. See the error message for further details.
  This error name should be avoided, in favor of a more expressive error
  name.
#+end_quote

*SD_BUS_ERROR_NO_MEMORY*

#+begin_quote
  A memory allocation failed, and the requested operation could not be
  completed.
#+end_quote

*SD_BUS_ERROR_SERVICE_UNKNOWN*

#+begin_quote
  The contacted bus service is unknown and cannot be activated.
#+end_quote

*SD_BUS_ERROR_NAME_HAS_NO_OWNER*

#+begin_quote
  The specified bus service name currently has no owner.
#+end_quote

*SD_BUS_ERROR_NO_REPLY*

#+begin_quote
  A message did not receive a reply. This error is usually generated
  after a timeout.
#+end_quote

*SD_BUS_ERROR_IO_ERROR*

#+begin_quote
  Generic input/output error, for example when accessing a socket or
  other I/O context.
#+end_quote

*SD_BUS_ERROR_BAD_ADDRESS*

#+begin_quote
  The specified D-Bus bus address string is malformed.
#+end_quote

*SD_BUS_ERROR_NOT_SUPPORTED*

#+begin_quote
  The requested operation is not supported on the local system.
#+end_quote

*SD_BUS_ERROR_LIMITS_EXCEEDED*

#+begin_quote
  Some limited resource has been exhausted.
#+end_quote

*SD_BUS_ERROR_ACCESS_DENIED*

#+begin_quote
  Access to a resource has been denied due to security restrictions.
#+end_quote

*SD_BUS_ERROR_AUTH_FAILED*

#+begin_quote
  Authentication did not complete successfully.
#+end_quote

*SD_BUS_ERROR_NO_SERVER*

#+begin_quote
  Unable to connect to the specified server.
#+end_quote

*SD_BUS_ERROR_TIMEOUT*

#+begin_quote
  An operation timed out. Note that method calls which timeout generate
  a *SD_BUS_ERROR_NO_REPLY*.
#+end_quote

*SD_BUS_ERROR_NO_NETWORK*

#+begin_quote
  No network available to execute requested network operation on.
#+end_quote

*SD_BUS_ERROR_ADDRESS_IN_USE*

#+begin_quote
  The specified network address is already being listened on.
#+end_quote

*SD_BUS_ERROR_DISCONNECTED*

#+begin_quote
  The connection has been terminated.
#+end_quote

*SD_BUS_ERROR_INVALID_ARGS*

#+begin_quote
  One or more invalid arguments have been passed.
#+end_quote

*SD_BUS_ERROR_FILE_NOT_FOUND*

#+begin_quote
  The requested file could not be found.
#+end_quote

*SD_BUS_ERROR_FILE_EXISTS*

#+begin_quote
  The requested file already exists.
#+end_quote

*SD_BUS_ERROR_UNKNOWN_METHOD*

#+begin_quote
  The requested method does not exist in the selected interface.
#+end_quote

*SD_BUS_ERROR_UNKNOWN_OBJECT*

#+begin_quote
  The requested object does not exist in the selected service.
#+end_quote

*SD_BUS_ERROR_UNKNOWN_INTERFACE*

#+begin_quote
  The requested interface does not exist on the selected object.
#+end_quote

*SD_BUS_ERROR_UNKNOWN_PROPERTY*

#+begin_quote
  The requested property does not exist in the selected interface.
#+end_quote

*SD_BUS_ERROR_PROPERTY_READ_ONLY*

#+begin_quote
  A write operation was requested on a read-only property.
#+end_quote

*SD_BUS_ERROR_UNIX_PROCESS_ID_UNKNOWN*

#+begin_quote
  The requested PID is not known.
#+end_quote

*SD_BUS_ERROR_INVALID_SIGNATURE*

#+begin_quote
  The specified message signature is not valid.
#+end_quote

*SD_BUS_ERROR_INCONSISTENT_MESSAGE*

#+begin_quote
  The passed message does not validate correctly.
#+end_quote

*SD_BUS_ERROR_MATCH_RULE_NOT_FOUND*

#+begin_quote
  The specified match rule does not exist.
#+end_quote

*SD_BUS_ERROR_MATCH_RULE_INVALID*

#+begin_quote
  The specified match rule is invalid.
#+end_quote

*SD_BUS_ERROR_INTERACTIVE_AUTHORIZATION_REQUIRED*

#+begin_quote
  Access to the requested operation is not permitted. However, it might
  be available after interactive authentication. This is usually
  returned by method calls supporting a framework for additional
  interactive authorization, when interactive authorization was not
  enabled with the
  *sd_bus_message_set_allow_interactive_authorization*(3) for the method
  call message.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_error*(3),
*sd_bus_message_set_allow_interactive_authorization*(3), *errno*(3),
*strerror*(3)
