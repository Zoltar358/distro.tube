#+TITLE: Manpages - XCloseDisplay.3
#+DESCRIPTION: Linux manpage for XCloseDisplay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XCloseDisplay.3 is found in manpage for: [[../man3/XOpenDisplay.3][man3/XOpenDisplay.3]]