#+TITLE: Manpages - ExtUtils_MM_Darwin.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_Darwin.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_Darwin - special behaviors for OS X

* SYNOPSIS
For internal MakeMaker use only

* DESCRIPTION
See ExtUtils::MM_Unix or ExtUtils::MM_Any for documentation on the
methods overridden here.

** Overridden Methods
/init_dist/

Turn off Apple tar's tendency to copy resource forks as ._foo files.

/cflags/

Over-ride Apple's automatic setting of -Werror
