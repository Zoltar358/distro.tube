#+TITLE: Manpages - cap_copy_int.3
#+DESCRIPTION: Linux manpage for cap_copy_int.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about cap_copy_int.3 is found in manpage for: [[../man3/cap_copy_ext.3][man3/cap_copy_ext.3]]