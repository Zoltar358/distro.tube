#+TITLE: Manpages - gnutls_x509_crt_get_raw_issuer_dn.3
#+DESCRIPTION: Linux manpage for gnutls_x509_crt_get_raw_issuer_dn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_x509_crt_get_raw_issuer_dn - API function

* SYNOPSIS
*#include <gnutls/x509.h>*

*int gnutls_x509_crt_get_raw_issuer_dn(gnutls_x509_crt_t */cert/*,
gnutls_datum_t * */dn/*);*

* ARGUMENTS
- gnutls_x509_crt_t cert :: should contain a *gnutls_x509_crt_t* type

- gnutls_datum_t * dn :: will hold the starting point of the DN

* DESCRIPTION
This function will return a pointer to the DER encoded DN structure and
the length. This points to allocated data that must be free'd using
*gnutls_free()*.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.or a negative error code on error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
