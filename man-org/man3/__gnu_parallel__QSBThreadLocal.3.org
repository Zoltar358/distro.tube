#+TITLE: Manpages - __gnu_parallel__QSBThreadLocal.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__QSBThreadLocal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_QSBThreadLocal< _RAIter > - Information local to one
thread in the parallel quicksort run.

* SYNOPSIS
\\

=#include <balanced_quicksort.h>=

** Public Types
typedef _TraitsType::difference_type *_DifferenceType*\\

typedef *std::pair*< _RAIter, _RAIter > *_Piece*\\
Continuous part of the sequence, described by an iterator pair.

typedef *std::iterator_traits*< _RAIter > *_TraitsType*\\

** Public Member Functions
*_QSBThreadLocal* (int __queue_size)\\
Constructor.

** Public Attributes
volatile _DifferenceType * *_M_elements_leftover*\\
Pointer to a counter of elements left over to sort.

*_Piece* *_M_global*\\
The complete sequence to sort.

*_Piece* *_M_initial*\\
Initial piece to work on.

*_RestrictedBoundedConcurrentQueue*< *_Piece* > *_M_leftover_parts*\\
Work-stealing queue.

*_ThreadIndex* *_M_num_threads*\\
Number of threads involved in this algorithm.

* Detailed Description
** "template<typename _RAIter>
\\
struct __gnu_parallel::_QSBThreadLocal< _RAIter >"Information local to
one thread in the parallel quicksort run.

Definition at line *65* of file *balanced_quicksort.h*.

* Member Typedef Documentation
** template<typename _RAIter > typedef _TraitsType::difference_type
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::_DifferenceType
Definition at line *68* of file *balanced_quicksort.h*.

** template<typename _RAIter > typedef *std::pair*<_RAIter, _RAIter>
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::*_Piece*
Continuous part of the sequence, described by an iterator pair.

Definition at line *72* of file *balanced_quicksort.h*.

** template<typename _RAIter > typedef *std::iterator_traits*<_RAIter>
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::*_TraitsType*
Definition at line *67* of file *balanced_quicksort.h*.

* Constructor & Destructor Documentation
** template<typename _RAIter > *__gnu_parallel::_QSBThreadLocal*<
_RAIter >::*_QSBThreadLocal* (int __queue_size)= [inline]=
Constructor.

*Parameters*

#+begin_quote
  /__queue_size/ size of the work-stealing queue.
#+end_quote

Definition at line *91* of file *balanced_quicksort.h*.

* Member Data Documentation
** template<typename _RAIter > volatile _DifferenceType*
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::_M_elements_leftover
Pointer to a counter of elements left over to sort.

Definition at line *84* of file *balanced_quicksort.h*.

Referenced by *__gnu_parallel::__parallel_sort_qsb()*.

** template<typename _RAIter > *_Piece*
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::_M_global
The complete sequence to sort.

Definition at line *87* of file *balanced_quicksort.h*.

** template<typename _RAIter > *_Piece*
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::_M_initial
Initial piece to work on.

Definition at line *75* of file *balanced_quicksort.h*.

Referenced by *__gnu_parallel::__qsb_conquer()*, and
*__gnu_parallel::__qsb_local_sort_with_helping()*.

** template<typename _RAIter >
*_RestrictedBoundedConcurrentQueue*<*_Piece*>
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::_M_leftover_parts
Work-stealing queue.

Definition at line *78* of file *balanced_quicksort.h*.

** template<typename _RAIter > *_ThreadIndex*
*__gnu_parallel::_QSBThreadLocal*< _RAIter >::_M_num_threads
Number of threads involved in this algorithm.

Definition at line *81* of file *balanced_quicksort.h*.

Referenced by *__gnu_parallel::__qsb_local_sort_with_helping()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
