#+TITLE: Manpages - XtSendSelectionRequest.3
#+DESCRIPTION: Linux manpage for XtSendSelectionRequest.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtSendSelectionRequest.3 is found in manpage for: [[../man3/XtCreateSelectionRequest.3][man3/XtCreateSelectionRequest.3]]