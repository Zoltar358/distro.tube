#+TITLE: Manpages - sd_booted.3
#+DESCRIPTION: Linux manpage for sd_booted.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_booted - Test whether the system is running the systemd init system

* SYNOPSIS
#+begin_example
  #include <systemd/sd-daemon.h>
#+end_example

*int sd_booted(void);*

* DESCRIPTION
*sd_booted()* checks whether the system was booted up using the systemd
init system.

* RETURN VALUE
On failure, this call returns a negative errno-style error code. If the
system was booted up with systemd as init system, this call returns a
positive return value, zero otherwise.

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

Internally, this function checks whether the directory
/run/systemd/system/ exists. A simple check like this can also be
implemented trivially in shell or any other language.

* SEE ALSO
*systemd*(1), *sd-daemon*(3)
