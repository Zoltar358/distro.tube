#+TITLE: Manpages - rtcGetSceneDevice.3embree3
#+DESCRIPTION: Linux manpage for rtcGetSceneDevice.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcGetSceneDevice - returns the device the scene got created in
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  RTCDevice rtcGetSceneDevice(RTCScene scene);
#+end_example

** DESCRIPTION
This function returns the device object the scene got created in. The
returned handle own one additional reference to the device object, thus
you should need to call =rtcReleaseDevice= when the returned handle is
no longer required.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcReleaseDevice]
