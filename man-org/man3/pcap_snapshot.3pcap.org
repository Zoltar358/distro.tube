#+TITLE: Manpages - pcap_snapshot.3pcap
#+DESCRIPTION: Linux manpage for pcap_snapshot.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_snapshot - get the snapshot length

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_snapshot(pcap_t *p);
#+end_example

* DESCRIPTION
*pcap_snapshot*() returns the snapshot length specified when
*pcap_set_snaplen*(3PCAP) or *pcap_open_live*(3PCAP) was called, for a
live capture, or the snapshot length from the capture file, for a
``savefile''.

It must not be called on a pcap descriptor created by
*pcap_create*(3PCAP) that has not yet been activated by
*pcap_activate*(3PCAP).

* RETURN VALUE
*pcap_snapshot*() returns the snapshot length on success and
*PCAP_ERROR_NOT_ACTIVATED* if called on a capture handle that has been
created but not activated.

* SEE ALSO
*pcap*(3PCAP)
