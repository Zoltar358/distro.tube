#+TITLE: Manpages - XGrabDeviceButton.3
#+DESCRIPTION: Linux manpage for XGrabDeviceButton.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGrabDeviceButton, XUngrabDeviceButton - grab/ungrab extension input
device buttons

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  int XGrabDeviceButton( Display *display,
                         XDevice *device,
                         unsigned int button,
                         unsigned int modifiers,
                         XDevice* modifier_device,
                         Window grab_window,
                         Bool owner_events,
                         unsigned int event_count,
                         XEventClass *event_list,
                         int this_device_mode,
                         int other_devices_mode);
#+end_example

#+begin_example
  int XUngrabDeviceButton( Display *display,
                           XDevice *device,
                           unsigned int button,
                           unsigned int modifiers,
                           XDevice* modifier_device,
                           Window grab_window);
#+end_example

#+begin_example
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         Specifies the device that is to be grabbed or released
#+end_example

#+begin_example
  button
         Specifies the device button that is to be grabbed or
         released or AnyButton.
#+end_example

#+begin_example
  modifiers
         Specifies the set of keymasks or AnyModifier. The mask is
         the bitwise inclusive OR of the valid keymask bits.
         Valid bits are: ShiftMask, LockMask, ControlMask,
         Mod1Mask, Mod2Mask, Mod3Mask, Mod4Mask, Mod5Mask.
#+end_example

#+begin_example
  modifier_device
         specifies the device whose modifiers are to be used. If
         the modifier_device specified is NULL, the X keyboard
         will be used as the modifier_device.
#+end_example

#+begin_example
  grab_window
         Specifies the grab window.
#+end_example

#+begin_example
  owner_events
         Specifies a Boolean value that indicates whether the
         device events are to be reported as usual or reported
         with respect to the grab window if selected by the event
         list.
#+end_example

#+begin_example
  event_count
         Specifies the number of event classes in the event list.
#+end_example

#+begin_example
  event_list
         Specifies which events are reported to the client.
#+end_example

#+begin_example
  this_device_mode
         Specifies further processing of events from this
         device. You can pass GrabModeSync or GrabModeAsync.
#+end_example

#+begin_example
  other_devices_mode
         Specifies further processing of events from all other
         devices. You can pass GrabModeSync or GrabModeAsync.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    The XGrabDeviceButton request establishes a passive grab. In
    the future, the device is actively grabbed (as for XGrabDevice,
    the last-grab time is set to the time at which the button was
    pressed (as transmitted in the DeviceButtonPress event), and
    the DeviceButtonPress event is reported if all of the following
    conditions are true:
      * The device is not grabbed, and the specified button is
        logically pressed when the specified modifier keys are
        logically down on the specified modifier device and no
        other buttons or modifier keys are logically down.
      * Either the grab window is an ancestor of (or is) the focus
        window, OR the grab window is a descendent of the focus
        window and contains the device.
      * A passive grab on the same button/modifier combination does
        not exist on any ancestor of grab_window.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The interpretation of the remaining arguments is as for
    XGrabDevice. The active grab is terminated automatically when
    the logical state of the device has all buttons released
    (independent of the logical state of the modifier keys).
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    Note that the logical state of a device (as seen by client
    applications) may lag the physical state if device event
    processing is frozen.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    This request overrides all previous grabs by the same client on
    the same button/modifier combinations on the same window. A
    modifiers of AnyModifier is equivalent to issuing the grab
    request for all possible modifier combinations (including the
    combination of no modifiers). It is not required that all
    modifiers specified have currently assigned KeyCodes. A button
    of AnyButton is equivalent to issuing the request for all
    possible buttons. Otherwise, it is not required that the
    specified button currently be assigned to a physical button.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    A modifier_device of NULL indicates that the X keyboard is to
    be used as the modifier_device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If some other client has already issued a XGrabDeviceButton
    with the same button/modifier combination on the same window, a
    BadAccess error results. When using AnyModifier or AnyButton ,
    the request fails completely, and a BadAccess error results (no
    grabs are established) if there is a conflicting grab for any
    combination. XGrabDeviceButton has no effect on an active grab.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XGrabDeviceButton can generate BadClass, BadDevice, BadMatch,
    BadValue, and BadWindow errors.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The XUngrabDeviceButton request releases the passive grab for a
    button/modifier combination on the specified window if it was
    grabbed by this client. A modifier of AnyModifier is equivalent to
    issuing the ungrab request for all possible modifier combinations,
    including the combination of no modifiers. A button of AnyButton is
    equivalent to issuing the request for all possible buttons.
    XUngrabDeviceButton has no effect on an active grab.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    A modifier_device of NULL indicates that the X keyboard should
    be used as the modifier_device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XUngrabDeviceButton can generate BadDevice, BadMatch, BadValue
    and BadWindow errors.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The specified device
           does not exist or has not been opened by this client via
           XOpenInputDevice. This error may also occur if the
           specified device is the X keyboard or X pointer device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           This error may occur if an XGrabDeviceButton request was
           made specifying a device that has no buttons, or
           specifying a modifier device that has no keys.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls outside the range of values
           accepted by the request. Unless a specific range is
           specified for an argument, the full range defined by the
           arguments type is accepted. Any argument defined as a
           set of alternatives can generate this error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadWindow
           A value for a Window argument does not name a defined
           Window.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XAllowDeviceEvents(3), XGrabDevice(3), XGrabDeviceKey(3)
  #+end_example
#+end_quote
