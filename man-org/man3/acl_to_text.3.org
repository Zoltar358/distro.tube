#+TITLE: Manpages - acl_to_text.3
#+DESCRIPTION: Linux manpage for acl_to_text.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function translates the ACL pointed to by the argument

into a

terminated character string. If the pointer

is not

then the function returns the length of the string (not including the

terminator) in the location pointed to by

The format of the text string returned by

is the long text form defined in

The ACL referred to by

is not changed.

This function allocates any memory necessary to contain the string and
returns a pointer to the string. The caller should free any releasable
memory, when the new string is no longer required, by calling

with the

returned by

as an argument.

On success, this function returns a pointer to the long text form of the
ACL. On error, a value of

is returned, and

is set appropriately.

If any of the following conditions occur, the

function returns a value of

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The ACL referenced by

contains one or more improperly formed ACL entries, or for some other
reason cannot be translated into a text form of an ACL.

The character string to be returned requires more memory than is allowed
by the hardware or system-imposed memory management constraints.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
