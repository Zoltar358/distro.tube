#+TITLE: Manpages - readproctab.3
#+DESCRIPTION: Linux manpage for readproctab.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
readproctab, freeproctab - read information for all current processes at
once

* SYNOPSIS
*#include <proc/readproc.h>*

*proc_t** readproctab(int */flags/*, ... );*\\
*void freeproctab(proc_t ***/p/*);*

* DESCRIPTION
*readproctab* reads information on all processes matching the criteria
from /flags/, allocating memory for everything as needed. It returns a
NULL-terminated list of /proc_t/ pointers. For more information on the
arguments of *readproctab*, see *openproc*(3).

*freeproctab* frees all memory allocated by *readproctab*.

The /proc_t/ structure is defined in /<proc/readproc.h>/, please look
there for a definition of all fields.

* SEE ALSO
*openproc*(3), *readproc*(3), */proc/*, */usr/include/proc/readproc.h*.

* REPORTING BUGS
Please send bug reports to [[file:procps@freelists.org][]]
