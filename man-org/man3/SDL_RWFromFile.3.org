#+TITLE: Manpages - SDL_RWFromFile.3
#+DESCRIPTION: Linux manpage for SDL_RWFromFile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_FunctionName - Short description of function

* SYNOPSIS
*#include "SDL.h"*

*return typeSDL_FunctionName*(*parameter*);

* DESCRIPTION
Full description

* EXAMPLES
examples here

* SEE ALSO
/SDL_AnotherFunction/
