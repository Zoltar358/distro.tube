#+TITLE: Manpages - gnutls_x509_crt_set_spki.3
#+DESCRIPTION: Linux manpage for gnutls_x509_crt_set_spki.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_x509_crt_set_spki - API function

* SYNOPSIS
*#include <gnutls/x509.h>*

*int gnutls_x509_crt_set_spki(gnutls_x509_crt_t */crt/*, const
gnutls_x509_spki_t */spki/*, unsigned int */flags/*);*

* ARGUMENTS
- gnutls_x509_crt_t crt :: a certificate of type *gnutls_x509_crt_t*

- const gnutls_x509_spki_t spki :: a SubjectPublicKeyInfo structure of
  type *gnutls_x509_spki_t*

- unsigned int flags :: must be zero

* DESCRIPTION
This function will set the certificate's subject public key information
explicitly. This is intended to be used in the cases where a single
public key (e.g., RSA) can be used for multiple signature algorithms
(RSA PKCS1-1.5, and RSA-PSS).

To export the public key (i.e., the SubjectPublicKeyInfo part), check
*gnutls_pubkey_import_x509()*.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
3.6.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
