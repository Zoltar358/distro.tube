#+TITLE: Manpages - curl_getenv.3
#+DESCRIPTION: Linux manpage for curl_getenv.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_getenv - return value for environment name

* SYNOPSIS
*#include <curl/curl.h>*

*char *curl_getenv(const char **/name/*);*

* DESCRIPTION
curl_getenv() is a portable wrapper for the getenv() function, meant to
emulate its behavior and provide an identical interface for all
operating systems libcurl builds on (including win32).

You must /curl_free(3)/ the returned string when you are done with it.

* EXAMPLE
#+begin_example
    char *width = curl_getenv("COLUMNS");
    if(width) {
      /* it was set! */
      curl_free(width);
    }
#+end_example

* AVAILABILITY
This function will be removed from the public libcurl API in a near
future. It will instead be made "available" by source code access only,
and then as curlx_getenv().

* RETURN VALUE
A pointer to a null-terminated string or NULL if it failed to find the
specified name.

* NOTE
Under unix operating systems, there is no point in returning an
allocated memory, although other systems will not work properly if this
is not done. The unix implementation thus has to suffer slightly from
the drawbacks of other systems.

* SEE ALSO
*getenv*(3C),
