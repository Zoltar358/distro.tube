#+TITLE: Manpages - archive_entry.3
#+DESCRIPTION: Linux manpage for archive_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

These functions create and manipulate data objects that represent
entries within an archive. You can think of a

as a heavy-duty version of

it includes everything from

plus associated pathname, textual group and user names, etc. These
objects are used by

to represent the metadata associated with a particular entry in an
archive.

There are functions to allocate, destroy, clear, and copy

objects:

Erases the object, resetting all internal fields to the same state as a
newly-created object. This is provided to allow you to quickly recycle
objects without thrashing the heap.

A deep copy operation; all text fields are duplicated.

Releases the

object.

Allocate and return a blank

object.

Due to high number of functions, the accessor functions can be found in
man pages grouped by the purpose.

Access Control List manipulation

Path name manipulation

User, group and mode manipulation

Functions not in the other groups and copying to/from

Time field manipulation

Most of the functions set or read entries in an object. Such functions
have one of the following forms:

Stores the provided data in the object. In particular, for strings, the
pointer is stored, not the referenced string.

As above, except that the referenced data is copied into the object.

Returns the specified data. In the case of strings, a const-qualified
pointer to the string is returned.

String data can be set or accessed as wide character strings or normal

strings. The functions that use wide character strings are suffixed with

Note that these are different representations of the same data: For
example, if you store a narrow string and read the corresponding wide
string, the object will transparently convert formats using the current
locale. Similarly, if you store a wide string and then store a narrow
string for the same data, the previously-set wide string will be
discarded in favor of the new data.

The

library first appeared in

The

library was written by
