#+TITLE: Manpages - std_is_swappable_with.3
#+DESCRIPTION: Linux manpage for std_is_swappable_with.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_swappable_with< _Tp, _Up > - is_swappable_with

* SYNOPSIS
\\

Inherits __is_swappable_with_impl::type.

* Detailed Description
** "template<typename _Tp, typename _Up>
\\
struct std::is_swappable_with< _Tp, _Up >"is_swappable_with

Definition at line *2770* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
