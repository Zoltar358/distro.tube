#+TITLE: Manpages - fork.3am
#+DESCRIPTION: Linux manpage for fork.3am
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fork, wait, waitpid - basic process management

* SYNOPSIS
@load "fork"

pid = fork()

ret = waitpid(pid)

ret = wait();

* DESCRIPTION
The /fork/ extension adds three functions, as follows.

- *fork()* :: This function creates a new process. The return value is
  the zero in the child and the process-id number of the child in the
  parent, or -1 upon error. In the latter case, *ERRNO* indicates the
  problem. In the child, *PROCINFO["pid"]* and *PROCINFO["ppid"]* are
  updated to reflect the correct values.

- *waitpid()* :: This function takes a numeric argument, which is the
  process-id to wait for. The return value is that of the /waitpid/(2)
  system call.

- *wait()* :: This function waits for the first child to die. The return
  value is that of the /wait/(2) system call.

* BUGS
There is no corresponding *exec()* function.

The interfaces could be enhanced to provide more facilities, including
pulling out the various bits of the return status.

* EXAMPLE
#+begin_example
  @load "fork"
  ...
  if ((pid = fork()) == 0)
      print "hello from the child"
  else
      print "hello from the parent"
#+end_example

* SEE ALSO
/GAWK: Effective AWK Programming/, /filefuncs/(3am), /fnmatch/(3am),
/inplace/(3am), /ordchr/(3am), /readdir/(3am), /readfile/(3am),
/revoutput/(3am), /rwarray/(3am), /time/(3am).

/fork/(2), /wait/(2), /waitpid/(2).

* AUTHOR
Arnold Robbins, *arnold@skeeve.com*.

* COPYING PERMISSIONS
Copyright © 2012, 2013, 2018, Free Software Foundation, Inc.

Permission is granted to make and distribute verbatim copies of this
manual page provided the copyright notice and this permission notice are
preserved on all copies.

Permission is granted to process this file through troff and print the
results, provided the printed document carries copying permission notice
identical to this one except for the removal of this paragraph (this
paragraph not being relevant to the printed manual page).

Permission is granted to copy and distribute modified versions of this
manual page under the conditions for verbatim copying, provided that the
entire resulting derived work is distributed under the terms of a
permission notice identical to this one.

Permission is granted to copy and distribute translations of this manual
page into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the Foundation.
