#+TITLE: Manpages - fetestexcept.3
#+DESCRIPTION: Linux manpage for fetestexcept.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fetestexcept.3 is found in manpage for: [[../man3/fenv.3][man3/fenv.3]]