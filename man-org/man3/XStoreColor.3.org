#+TITLE: Manpages - XStoreColor.3
#+DESCRIPTION: Linux manpage for XStoreColor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XStoreColor.3 is found in manpage for: [[../man3/XStoreColors.3][man3/XStoreColors.3]]