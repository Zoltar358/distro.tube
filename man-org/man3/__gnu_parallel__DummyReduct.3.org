#+TITLE: Manpages - __gnu_parallel__DummyReduct.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__DummyReduct.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_DummyReduct - Reduction function doing nothing.

* SYNOPSIS
\\

=#include <for_each_selectors.h>=

** Public Member Functions
bool *operator()* (bool, bool) const\\

* Detailed Description
Reduction function doing nothing.

Definition at line *298* of file *for_each_selectors.h*.

* Member Function Documentation
** bool __gnu_parallel::_DummyReduct::operator() (bool, bool)
const= [inline]=
Definition at line *301* of file *for_each_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
