#+TITLE: Manpages - CPAN_HandleConfig.3perl
#+DESCRIPTION: Linux manpage for CPAN_HandleConfig.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
CPAN::HandleConfig - internal configuration handling for CPAN.pm

** "CLASS->safe_quote ITEM"
Quotes an item to become safe against spaces in shell interpolation. An
item is enclosed in double quotes if:

- the item contains spaces in the middle - the item does not start with
a quote

This happens to avoid shell interpolation problems when whitespace is
present in directory names.

This method uses =commands_quote= to determine the correct quote. If
=commands_quote= is a space, no quoting will take place.

if it starts and ends with the same quote character: leave it as it is

if it contains no whitespace: leave it as it is

if it contains whitespace, then

if it contains quotes: better leave it as it is

else: quote it with the correct quote type for the box we're on

* LICENSE
This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.
