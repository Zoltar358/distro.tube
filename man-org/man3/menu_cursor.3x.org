#+TITLE: Manpages - menu_cursor.3x
#+DESCRIPTION: Linux manpage for menu_cursor.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*pos_menu_cursor* - position a menu's cursor

* SYNOPSIS
*#include <menu.h>*

*int pos_menu_cursor(const MENU **/menu/*);*\\

* DESCRIPTION
The function *pos_menu_cursor* restores the cursor to the current
position associated with the menu's selected item. This is useful after
*curses* routines have been called to do screen-painting in response to
a menu select.

* RETURN VALUE
This routine returns one of the following:

- *E_OK* :: The routine succeeded.

- *E_SYSTEM_ERROR* :: System error occurred (see *errno*(3)).

- *E_BAD_ARGUMENT* :: Routine detected an incorrect or out-of-range
  argument.

- *E_NOT_POSTED* :: The menu has not been posted.

* SEE ALSO
*curses*(3X), *menu*(3X).

* NOTES
The header file *<menu.h>* automatically includes the header file
*<curses.h>*.

* PORTABILITY
These routines emulate the System V menu library. They were not
supported on Version 7 or BSD versions.

* AUTHORS
Juergen Pfeifer. Manual pages and adaptation for new curses by Eric S.
Raymond.
