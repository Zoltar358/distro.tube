#+TITLE: Manpages - XtRemoveActionHook.3
#+DESCRIPTION: Linux manpage for XtRemoveActionHook.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtRemoveActionHook.3 is found in manpage for: [[../man3/XtAppAddActionHook.3][man3/XtAppAddActionHook.3]]