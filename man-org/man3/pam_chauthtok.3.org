#+TITLE: Manpages - pam_chauthtok.3
#+DESCRIPTION: Linux manpage for pam_chauthtok.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_chauthtok - updating authentication tokens

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

*int pam_chauthtok(pam_handle_t **/pamh/*, int */flags/*);*

* DESCRIPTION
The *pam_chauthtok* function is used to change the authentication token
for a given user (as indicated by the state associated with the handle
/pamh/).

The /pamh/ argument is an authentication handle obtained by a prior call
to pam_start(). The flags argument is the binary or of zero or more of
the following values:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

PAM_CHANGE_EXPIRED_AUTHTOK

#+begin_quote
  This argument indicates to the modules that the users authentication
  token (password) should only be changed if it has expired. If this
  argument is not passed, the application requires that all
  authentication tokens are to be changed.
#+end_quote

* RETURN VALUES
PAM_AUTHTOK_ERR

#+begin_quote
  A module was unable to obtain the new authentication token.
#+end_quote

PAM_AUTHTOK_RECOVERY_ERR

#+begin_quote
  A module was unable to obtain the old authentication token.
#+end_quote

PAM_AUTHTOK_LOCK_BUSY

#+begin_quote
  One or more of the modules was unable to change the authentication
  token since it is currently locked.
#+end_quote

PAM_AUTHTOK_DISABLE_AGING

#+begin_quote
  Authentication token aging has been disabled for at least one of the
  modules.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  Permission denied.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The authentication token was successfully updated.
#+end_quote

PAM_TRY_AGAIN

#+begin_quote
  Not all of the modules were in a position to update the authentication
  token(s). In such a case none of the users authentication tokens are
  updated.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User unknown to password service.
#+end_quote

* SEE ALSO
*pam_start*(3), *pam_authenticate*(3), *pam_setcred*(3),
*pam_get_item*(3), *pam_strerror*(3), *pam*(8)
