#+TITLE: Manpages - idn2_to_unicode_4z4z.3
#+DESCRIPTION: Linux manpage for idn2_to_unicode_4z4z.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idn2_to_unicode_4z4z - API function

* SYNOPSIS
*#include <idn2.h>*

*int idn2_to_unicode_4z4z(const uint32_t * */input/*, uint32_t **
*/output/*, int */flags/*);*

* ARGUMENTS
- const uint32_t * input :: Input zero-terminated UTF-32 string.

- uint32_t ** output :: Newly allocated UTF-32 output string.

- int flags :: Currently unused.

* DESCRIPTION
Converts a possibly ACE encoded domain name in UTF-32 format into a
UTF-32 string (punycode decoding). The output buffer will be
zero-terminated and must be deallocated by the caller.

/output/ may be NULL to test lookup of /input/ without allocating
memory.

* SINCE
2.0.0

* SEE ALSO
The full documentation for *libidn2* is maintained as a Texinfo manual.
If the *info* and *libidn2* programs are properly installed at your
site, the command

#+begin_quote
  *info libidn2*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libidn/libidn2/manual/*
#+end_quote
