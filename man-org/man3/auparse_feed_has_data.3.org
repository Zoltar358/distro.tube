#+TITLE: Manpages - auparse_feed_has_data.3
#+DESCRIPTION: Linux manpage for auparse_feed_has_data.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_feed_has_data - check if there is any data accumulating that
might need flushing.

* SYNOPSIS
*#include <auparse.h>*

int auparse_feed_has_data(const auparse_state_t *au);

* DESCRIPTION
/auparse_feed_has_data/ may be called to determine if there is any
records that are accumulating but not yet ready to emit.

* RETURN VALUE
Returns 1 if any records are accumulating otherwise 0 if empty.

* SEE ALSO
*auparse_feed*(3), *auparse_feed_age_events*(3)

* AUTHOR
Steve Grubb
