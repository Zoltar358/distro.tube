#+TITLE: Manpages - zip_open.3
#+DESCRIPTION: Linux manpage for zip_open.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function opens the zip archive specified by

and returns a pointer to a

used to manipulate the archive. The

are specified by

the following values, or 0 for none of them.

Perform additional stricter consistency checks on the archive, and error
if they fail.

Create the archive if it does not exist.

Error if archive already exists.

If archive exists, ignore its current contents. In other words, handle
it the same way as an empty archive.

Open archive in read-only mode.

If an error occurs and

is

it will be set to the corresponding error code.

The

function opens a zip archive encapsulated by the zip_source

using the provided

In case of error, the zip_error

is filled in.

Upon successful completion

and

return a

pointer. Otherwise,

is returned and

sets

to indicate the error, while

sets

to indicate the error.

The archive specified by

is opened unless:

The file specified by

exists and

is set.

Inconsistencies were found in the file specified by

This error is often caused by specifying

but can also happen without it.

The

argument is

Required memory could not be allocated.

The file specified by

does not exist and

is not set.

The file specified by

is not a zip archive.

The file specified by

could not be opened.

A read error occurred; see

for details.

The file specified by

does not allow seeks.

and

were added in libzip 1.0.

and
