#+TITLE: Manpages - asn1_delete_element.3
#+DESCRIPTION: Linux manpage for asn1_delete_element.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
asn1_delete_element - API function

* SYNOPSIS
*#include <libtasn1.h>*

*int asn1_delete_element(asn1_node */structure/*, const char *
*/element_name/*);*

* ARGUMENTS
- asn1_node structure :: pointer to the structure that contains the
  element you want to delete.

- const char * element_name :: element's name you want to delete.

* DESCRIPTION
Deletes the element named * /element_name/ inside * /structure/ .

* RETURNS
*ASN1_SUCCESS* if successful, *ASN1_ELEMENT_NOT_FOUND* if the
/element_name/ was not found.

* COPYRIGHT
Copyright © 2006-2021 Free Software Foundation, Inc..\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *libtasn1* is maintained as a Texinfo manual.
If the *info* and *libtasn1* programs are properly installed at your
site, the command

#+begin_quote
  *info libtasn1*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libtasn1/manual/*
#+end_quote
