#+TITLE: Manpages - program_invocation_short_name.3
#+DESCRIPTION: Linux manpage for program_invocation_short_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about program_invocation_short_name.3 is found in manpage for: [[../man3/program_invocation_name.3][man3/program_invocation_name.3]]