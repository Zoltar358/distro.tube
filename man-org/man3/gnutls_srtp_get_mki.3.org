#+TITLE: Manpages - gnutls_srtp_get_mki.3
#+DESCRIPTION: Linux manpage for gnutls_srtp_get_mki.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_srtp_get_mki - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*int gnutls_srtp_get_mki(gnutls_session_t */session/*, gnutls_datum_t *
*/mki/*);*

* ARGUMENTS
- gnutls_session_t session :: is a *gnutls_session_t* type.

- gnutls_datum_t * mki :: will hold the MKI

* DESCRIPTION
This function exports the negotiated Master Key Identifier, received by
the peer if any. The returned value in /mki/ should be treated as
constant and valid only during the session's lifetime.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error code is returned.

Since 3.1.4

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
