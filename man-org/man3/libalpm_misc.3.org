#+TITLE: Manpages - libalpm_misc.3
#+DESCRIPTION: Linux manpage for libalpm_misc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libalpm_misc - Miscellaneous Functions

- Various libalpm functions.

* SYNOPSIS
\\

** Typedefs
typedef int64_t *alpm_time_t*\\
The time type used by libalpm.

** Enumerations
enum *alpm_caps* { *ALPM_CAPABILITY_NLS* = (1 << 0),
*ALPM_CAPABILITY_DOWNLOADER* = (1 << 1), *ALPM_CAPABILITY_SIGNATURES* =
(1 << 2) }\\
Enum of possible compile time features.

** Functions
*alpm_pkg_t* * *alpm_sync_get_new_version* (*alpm_pkg_t* *pkg,
*alpm_list_t* *dbs_sync)\\
Check for new version of pkg in syncdbs.

char * *alpm_compute_md5sum* (const char *filename)\\
Get the md5 sum of file.

char * *alpm_compute_sha256sum* (const char *filename)\\
Get the sha256 sum of file.

int *alpm_unlock* (*alpm_handle_t* *handle)\\
Remove the database lock file.

const char * *alpm_version* (void)\\
Get the version of library.

int *alpm_capabilities* (void)\\
Get the capabilities of the library.

* Detailed Description
Various libalpm functions.

* Typedef Documentation
** typedef int64_t *alpm_time_t*
The time type used by libalpm. Represents a unix time stamp

* Enumeration Type Documentation
** enum *alpm_caps*
Enum of possible compile time features.

*Enumerator*

- /ALPM_CAPABILITY_NLS / :: localization

- /ALPM_CAPABILITY_DOWNLOADER / :: Ability to download.

- /ALPM_CAPABILITY_SIGNATURES / :: Signature checking.

* Function Documentation
** int alpm_capabilities (void)
Get the capabilities of the library.

*Returns*

#+begin_quote
  a bitmask of the capabilities
#+end_quote

** char * alpm_compute_md5sum (const char * filename)
Get the md5 sum of file.

*Parameters*

#+begin_quote
  /filename name of the file /
#+end_quote

*Returns*

#+begin_quote
  the checksum on success, NULL on error
#+end_quote

** char * alpm_compute_sha256sum (const char * filename)
Get the sha256 sum of file.

*Parameters*

#+begin_quote
  /filename name of the file /
#+end_quote

*Returns*

#+begin_quote
  the checksum on success, NULL on error
#+end_quote

** *alpm_pkg_t*/ * alpm_sync_get_new_version (/*alpm_pkg_t*/ * pkg,
/*alpm_list_t*/ * dbs_sync)/
Check for new version of pkg in syncdbs. If the same package appears
multiple dbs only the first will be checked

This only checks the syncdb for a newer version. It does not access the
network at all. See *alpm_db_update*/ to update a database. /

** int alpm_unlock (*alpm_handle_t*/ * handle)/
Remove the database lock file.

*Parameters*

#+begin_quote
  /handle the context handle /
#+end_quote

*Returns*

#+begin_quote
  0 on success, -1 on error
#+end_quote

*Note*

#+begin_quote
  Safe to call from inside signal handlers.
#+end_quote

** const char * alpm_version (void)
Get the version of library.

*Returns*

#+begin_quote
  the library version, e.g. '6.0.4'
#+end_quote

* Author
Generated automatically by Doxygen for libalpm from the source code.
