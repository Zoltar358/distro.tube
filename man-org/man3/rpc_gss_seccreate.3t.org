#+TITLE: Manpages - rpc_gss_seccreate.3t
#+DESCRIPTION: Linux manpage for rpc_gss_seccreate.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
This function is used to establish a security context between an
application and a remote peer using the RPSEC_GSS protocol.

An RPC handle which is connected to the remote peer

The name of the service principal on the remote peer. For instance, a
principal such as

might be used by an application which needs to contact an NFS server

The name of the GSS_API mechanism to use for the new security context.
"kerberos_v5" is currently the only supported mechanism.

Type of service requested.

The default - typically the same as

RPC headers only are integrity protected by a checksum.

RPC headers and data are integrity protected by a checksum.

RPC headers are integrity protected by a checksum and data is encrypted.

The name of the Quality of Protection to use for the new security
context, or NULL to use the default QOP. "GSS_C_QOP_DEFAULT" is
currently the only supported QOP.

Extra security context options to be passed to the underlying GSS-API
mechanism. Pass

to supply default values.

Various values returned by the underlying GSS-API mechanism. Pass

if these values are not required.

If the security context was created successfully, a pointer to an

structure that represents the context is returned. To use this security
context for subsequent RPC calls, set

to this value.

The

function is part of libtirpc.

This manual page was written by
