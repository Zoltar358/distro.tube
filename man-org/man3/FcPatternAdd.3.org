#+TITLE: Manpages - FcPatternAdd.3
#+DESCRIPTION: Linux manpage for FcPatternAdd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcPatternAdd - Add a value to a pattern

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcPatternAdd (FcPattern */p/*, const char **/object/*, FcValue
*/value/*, FcBool */append/*);*

* DESCRIPTION
Adds a single value to the list of values associated with the property
named `object/. If `append/ is FcTrue, the value is added at the end of
any existing list, otherwise it is inserted at the beginning. `value' is
saved (with FcValueSave) when inserted into the pattern so that the
library retains no reference to any application-supplied data structure.
