#+TITLE: Manpages - pam_prompt.3
#+DESCRIPTION: Linux manpage for pam_prompt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_prompt, pam_vprompt - interface to conversation function

* SYNOPSIS
#+begin_example
  #include <security/pam_ext.h>
#+end_example

*int pam_prompt(pam_handle_t **/pamh/*, int */style/*, char
***/response/*, const char **/fmt/*, */.../*);*

*int pam_vprompt(pam_handle_t **/pamh/*, int */style/*, char
***/response/*, const char **/fmt/*, va_list */args/*);*

* DESCRIPTION
The *pam_prompt* function constructs a message from the specified format
string and arguments and passes it to the conversation function as set
by the service. Upon successful return, /response/ is set to point to a
string returned from the conversation function. This string is allocated
on heap and should be freed.

* RETURN VALUES
PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_CONV_ERR

#+begin_quote
  Conversation failure.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Conversation succeeded, response is set.
#+end_quote

PAM_SYSTEM_ERR

#+begin_quote
  System error.
#+end_quote

* SEE ALSO
*pam*(8), *pam_conv*(3)

* STANDARDS
The *pam_prompt* and *pam_vprompt* functions are Linux-PAM extensions.
