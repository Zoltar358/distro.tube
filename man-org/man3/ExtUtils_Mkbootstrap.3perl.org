#+TITLE: Manpages - ExtUtils_Mkbootstrap.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_Mkbootstrap.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::Mkbootstrap - make a bootstrap file for use by DynaLoader

* SYNOPSIS
Mkbootstrap

* DESCRIPTION
Mkbootstrap typically gets called from an extension Makefile.

There is no =*.bs= file supplied with the extension. Instead, there may
be a =*_BS= file which has code for the special cases, like posix for
berkeley db on the NeXT.

This file will get parsed, and produce a maybe empty
=@DynaLoader::dl_resolve_using= array for the current architecture. That
will be extended by =$BSLOADLIBS=, which was computed by
*ExtUtils::Liblist::ext()*. If this array still is empty, we do nothing,
else we write a .bs file with an =@DynaLoader::dl_resolve_using= array.

The =*_BS= file can put some code into the generated =*.bs= file by
placing it in =$bscode=. This is a handy 'escape' mechanism that may
prove useful in complex situations.

If =@DynaLoader::dl_resolve_using= contains =-L*= or =-l*= entries then
Mkbootstrap will automatically add a *dl_findfile()* call to the
generated =*.bs= file.
