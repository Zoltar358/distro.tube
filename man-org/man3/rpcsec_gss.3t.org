#+TITLE: Manpages - rpcsec_gss.3t
#+DESCRIPTION: Linux manpage for rpcsec_gss.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a security mechanism for the RPC protocol. It uses the Generic
Security Service API (GSS-API) to establish a security context between a
client and a server and to ensure that all subsequent communication
between client and server are properly authenticated. Optionally, extra
protection can be applied to the connection. The integrity service uses
checksums to ensure that all data sent by a peer is received without
modification. The privacy service uses encryption to ensure that no
third party can access the data for a connection.

To use this system, an application must first use

to establish a security context.

Data structures used by

appear below.

This type defines the types of security service required for

typedef enum { rpc_gss_svc_default = 0, rpc_gss_svc_none = 1,
rpc_gss_svc_integrity = 2, rpc_gss_svc_privacy = 3 } rpc_gss_service_t;

This structure contains various optional values which are used while
creating a security context.

typedef struct { int req_flags; /* GSS request bits */ int time_req; /*
requested lifetime */ gss_cred_id_t my_cred; /* GSS credential */
gss_channel_bindings_t input_channel_bindings; } rpc_gss_options_req_t;

Various details of the created security context are returned using this
structure.

typedef struct { int major_status; int minor_status; u_int
rpcsec_version; int ret_flags; int time_req; gss_ctx_id_t gss_context;
char actual_mechanism[MAX_GSS_MECH]; } rpc_gss_options_ret_t;

This type is used to refer to an client principal which is represented
in GSS-API exported name form (see

for more details). Names in this format may be stored in access control
lists or compared with other names in exported name form. This structure
is returned by

and is also referenced by the

structure.

typedef struct { int len; char name[1]; } *rpc_gss_principal_t;

This structure is used to access the raw credentials associated with a
security context.

typedef struct { u_int version; /* RPC version number */ const char
*mechanism; /* security mechanism */ const char *qop; /* quality of
protection */ rpc_gss_principal_t client_principal; /* client name */
const char *svc_principal; /* server name */ rpc_gss_service_t service;
/* service type */ } rpc_gss_rawcred_t;

Unix credentials which are derived form the raw credentials, accessed
via

typedef struct { uid_t uid; /* user ID */ gid_t gid; /* group ID */
short gidlen; gid_t *gidlist; /* list of groups */ } rpc_gss_ucred_t;

Structure used to enforce a particular QOP and service.

typedef struct { bool_t locked; rpc_gss_rawcred_t *raw_cred; }
rpc_gss_lock_t;

Callback structure used by

typedef struct { u_int program; /* RPC program number */ u_int version;
/* RPC version number */ /* user defined callback */ bool_t
(*callback)(struct svc_req *req, gss_cred_id_t deleg, gss_ctx_id_t
gss_context, rpc_gss_lock_t *lock, void **cookie); } rpc_gss_callback_t;

Structure used to return error information by

typedef struct { int rpc_gss_error; int system_error; /* same as errno
*/ } rpc_gss_error_t;

/* * Values for rpc_gss_error */ #define RPC_GSS_ER_SUCCESS 0 /* no
error */ #define RPC_GSS_ER_SYSTEMERROR 1 /* system error */

Create a new security context

Set service and quality of protection for a context

Calculate maximum client message sizes.

Get details of the last error

Convert a mechanism name to the corresponding GSS-API oid.

Convert a GSS-API oid to a mechanism name

Convert a quality of protection name to the corresponding number

Get a list of security mechanisms.

Return extra information about a security mechanism

Return the maximum and minimum supported versions of the

protocol

Query for the presence of a particular security mechanism

Set the name of a service principal which matches a given RPC program
plus version pair

Get credential details for the security context of an RPC request

Install a callback routine which is called on the server when new
security contexts are created

Create a client principal name from various strings

Calculate maximum server message sizes.

These functions are part of libtirpc.

This manual page was written by
