#+TITLE: Manpages - std_uniform_real_distribution.3
#+DESCRIPTION: Linux manpage for std_uniform_real_distribution.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::uniform_real_distribution< _RealType > - Uniform continuous
distribution for random numbers.

* SYNOPSIS
\\

=#include <random.h>=

** Classes
struct *param_type*\\

** Public Types
typedef _RealType *result_type*\\

** Public Member Functions
*uniform_real_distribution* ()\\
Constructs a uniform_real_distribution object.

*uniform_real_distribution* (_RealType __a, _RealType
__b=_RealType(1))\\
Constructs a uniform_real_distribution object.

*uniform_real_distribution* (const *param_type* &__p)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng)\\

template<typename _ForwardIterator , typename
_UniformRandomNumberGenerator > void *__generate* (_ForwardIterator __f,
_ForwardIterator __t, _UniformRandomNumberGenerator &__urng, const
*param_type* &__p)\\

template<typename _UniformRandomNumberGenerator > void *__generate*
(*result_type* *__f, *result_type* *__t, _UniformRandomNumberGenerator
&__urng, const *param_type* &__p)\\

*result_type* *a* () const\\

*result_type* *b* () const\\

*result_type* *max* () const\\
Returns the inclusive upper bound of the distribution range.

*result_type* *min* () const\\
Returns the inclusive lower bound of the distribution range.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng)\\
Generating functions.

template<typename _UniformRandomNumberGenerator > *result_type*
*operator()* (_UniformRandomNumberGenerator &__urng, const *param_type*
&__p)\\

*param_type* *param* () const\\
Returns the parameter set of the distribution.

void *param* (const *param_type* &__param)\\
Sets the parameter set of the distribution.

void *reset* ()\\
Resets the distribution state.

** Friends
bool *operator==* (const *uniform_real_distribution* &__d1, const
*uniform_real_distribution* &__d2)\\
Return true if two uniform real distributions have the same parameters.

* Detailed Description
** "template<typename _RealType = double>
\\
class std::uniform_real_distribution< _RealType >"Uniform continuous
distribution for random numbers.

A continuous random distribution on the range [min, max) with equal
probability throughout the range. The URNG should be real-valued and
deliver number in the range [0, 1).

Definition at line *1742* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef _RealType
*std::uniform_real_distribution*< _RealType >::*result_type*
The type of the range of the distribution.

Definition at line *1749* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double>
*std::uniform_real_distribution*< _RealType
>::*uniform_real_distribution* ()= [inline]=
Constructs a uniform_real_distribution object. The lower bound is set to
0.0 and the upper bound to 1.0

Definition at line *1792* of file *random.h*.

** template<typename _RealType = double>
*std::uniform_real_distribution*< _RealType
>::*uniform_real_distribution* (_RealType __a, _RealType __b =
=_RealType(1)=)= [inline]=, = [explicit]=
Constructs a uniform_real_distribution object.

*Parameters*

#+begin_quote
  /__a/ [IN] The lower bound of the distribution.\\
  /__b/ [IN] The upper bound of the distribution.
#+end_quote

Definition at line *1801* of file *random.h*.

** template<typename _RealType = double>
*std::uniform_real_distribution*< _RealType
>::*uniform_real_distribution* (const *param_type* & __p)= [inline]=,
= [explicit]=
Definition at line *1806* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::uniform_real_distribution*< _RealType >::__generate
(_ForwardIterator __f, _ForwardIterator __t,
_UniformRandomNumberGenerator & __urng)= [inline]=
Definition at line *1876* of file *random.h*.

** template<typename _RealType = double> template<typename
_ForwardIterator , typename _UniformRandomNumberGenerator > void
*std::uniform_real_distribution*< _RealType >::__generate
(_ForwardIterator __f, _ForwardIterator __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *1883* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > void *std::uniform_real_distribution*<
_RealType >::__generate (*result_type* * __f, *result_type* * __t,
_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *1890* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::uniform_real_distribution*< _RealType >::a () const= [inline]=
Definition at line *1819* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::uniform_real_distribution*< _RealType >::b () const= [inline]=
Definition at line *1823* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::uniform_real_distribution*< _RealType >::max () const= [inline]=
Returns the inclusive upper bound of the distribution range.

Definition at line *1852* of file *random.h*.

** template<typename _RealType = double> *result_type*
*std::uniform_real_distribution*< _RealType >::min () const= [inline]=
Returns the inclusive lower bound of the distribution range.

Definition at line *1845* of file *random.h*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::uniform_real_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng)= [inline]=
Generating functions.

Definition at line *1860* of file *random.h*.

References *std::uniform_real_distribution< _RealType >::operator()()*.

Referenced by *std::uniform_real_distribution< _RealType
>::operator()()*.

** template<typename _RealType = double> template<typename
_UniformRandomNumberGenerator > *result_type*
*std::uniform_real_distribution*< _RealType >::operator()
(_UniformRandomNumberGenerator & __urng, const *param_type* &
__p)= [inline]=
Definition at line *1865* of file *random.h*.

** template<typename _RealType = double> *param_type*
*std::uniform_real_distribution*< _RealType >::param () const= [inline]=
Returns the parameter set of the distribution.

Definition at line *1830* of file *random.h*.

Referenced by *std::operator>>()*.

** template<typename _RealType = double> void
*std::uniform_real_distribution*< _RealType >::param (const *param_type*
& __param)= [inline]=
Sets the parameter set of the distribution.

*Parameters*

#+begin_quote
  /__param/ The new parameter set of the distribution.
#+end_quote

Definition at line *1838* of file *random.h*.

** template<typename _RealType = double> void
*std::uniform_real_distribution*< _RealType >::reset ()= [inline]=
Resets the distribution state. Does nothing for the uniform real
distribution.

Definition at line *1816* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator== (const
*uniform_real_distribution*< _RealType > & __d1, const
*uniform_real_distribution*< _RealType > & __d2)= [friend]=
Return true if two uniform real distributions have the same parameters.

Definition at line *1900* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
