#+TITLE: Manpages - SDL_GL_GetAttribute.3
#+DESCRIPTION: Linux manpage for SDL_GL_GetAttribute.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GL_GetAttribute - Get the value of a special SDL/OpenGL attribute

* SYNOPSIS
*#include "SDL.h"*

*int SDL_GL_GetAttribute*(*SDLGLattr attr, int *value*);

* DESCRIPTION
Places the value of the SDL/OpenGL /attribute/ *attr* into *value*. This
is useful after a call to *SDL_SetVideoMode* to check whether your
attributes have been /set/ as you expected.

* RETURN VALUE
Returns *0* on success, or *-1* on an error.

* SEE ALSO
*SDL_GL_SetAttribute*, /GL Attributes/
