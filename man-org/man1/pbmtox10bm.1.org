#+TITLE: Man1 - pbmtox10bm.1
#+DESCRIPTION: Linux manpage for pbmtox10bm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtox10bm - convert a PBM image to an X11 bitmap

* DESCRIPTION
*pbmtox10bm* was replaced in Netpbm 10.37 (December 2006) by
*pbmtoxbm*(1)

*pbmtoxbm* with the *-x10* option is backward compatible with
*pbmtox10bm*. *pbmtoxbm* also can generate X11 bitmaps.

You should not make any new use of *pbmtox10bm* and if you modify an
existing use, you should upgrade to *pbmtoxbm*. But note that if you
write a program that might have to be used with old Netpbm, *pbmtox10bm*
is the only way to do that.
