#+TITLE: Man1 - ppmtoeyuv.1
#+DESCRIPTION: Linux manpage for ppmtoeyuv.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmtoeyuv - convert a PPM image into a Berkeley YUV file

* SYNOPSIS
*ppmtoeyuv*

[/ppmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmtoeyuv* reads a PPM image as input and produces a Berkeley Encoder
YUV (not the same as Abekas YUV) file on the Standard Output file.

With no argument, *ppmtoeyuv*takes input from Standard Input. Otherwise,
/ppmfile/ is the file specification of the input file.

*ppmtoeyuv* handles multi-image PPM input streams, outputting
consecutive eyuv images. There must be at least one image, though.

* SEE ALSO
*eyuvtoppm*(1) , *ppmtoyuv*(1) , *ppm*(5)
