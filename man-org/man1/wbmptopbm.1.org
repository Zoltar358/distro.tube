#+TITLE: Man1 - wbmptopbm.1
#+DESCRIPTION: Linux manpage for wbmptopbm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
wbmptopbm - convert a wireless bitmap (wbmp) file to a PBM

* SYNOPSIS
*wbmptopbm*

[/wbmpfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*wbmptopbm* reads a wbmp file as input and produces a PBM image as
output.

* LIMITATIONS
*wbmptopbm* recognizes only WBMP type 0. This is the only type specified
in the WAP 1.1 specifications.

* SEE ALSO
*pbm*(5) , *pbmtowbmp*(1) ,

Wireless Application Environment Specification.

* AUTHOR
Copyright (C) 1999 Terje Sannum </terje@looplab.com/>.
