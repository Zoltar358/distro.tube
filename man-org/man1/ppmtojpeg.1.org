#+TITLE: Man1 - ppmtojpeg.1
#+DESCRIPTION: Linux manpage for ppmtojpeg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*ppmtojpeg* - replaced by pnmtojpeg

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmtojpeg* was replaced in Netpbm 9.19 (September 2001) by
*pnmtojpeg*(1)

*pnmtojpeg* is backward compatible with *ppmtojpeg* except that with PGM
or PBM input, it generates JPEG output in the special grayscale format.
