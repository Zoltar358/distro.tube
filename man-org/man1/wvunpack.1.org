#+TITLE: Man1 - wvunpack.1
#+DESCRIPTION: Linux manpage for wvunpack.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wvunpack - decodes wavpack encoded files

* SYNOPSIS
*wvunpack* [/-options/] /INFILE/... [*-o */OUTFILE/]

* DESCRIPTION
*wvunpack* decodes WavPack files back to their original uncompressed
form using the options provided. Unless overridden with the *-o* switch,
the output filename will be identical to the source filename but with
the original file extension replacing WavPacks “.wv” extension. It is
also possible to output raw audio without headers (see *--raw* option).
Multiple WavPack input files may be specified resulting in multiple
output files, and in that case *-o* may be used to specify an alternate
target directory. Stdin and stdout may be specified with “-”. It is also
possible to export to one of the alternate file formats listed below,
but in that case the information in the original headers and trailers
will be lost (even if the alternate format is the same as the source
format). WavPack files are generally created with the *wavpack* program.

** OUTPUT FILE FORMATS

#+begin_quote
  ·

  Microsoft RIFF, extension “.wav”, force with *-w* or *--wav*, creates
  RF64 if > 4 GB
#+end_quote

#+begin_quote
  ·

  Sony Wave64, extension “.w64”, force with *--w64*
#+end_quote

#+begin_quote
  ·

  Apple Core Audio, extension “.caf”, force with *--caf-be* or
  *--caf-le*
#+end_quote

#+begin_quote
  ·

  Raw PCM or DSD, extension “.raw”, force with *-r* or *--raw*
#+end_quote

#+begin_quote
  ·

  Philips DSDIFF, extension “.dff”, force with *--dsdiff* or *--dff*
#+end_quote

#+begin_quote
  ·

  Sony DSD Stream, extension “.dsf”, force with *--dsf*
#+end_quote

* OPTIONS
*-b*

#+begin_quote
  blindly decode all stream blocks and ignore length info
#+end_quote

*-c*

#+begin_quote
  do not decode audio but instead just extract cuesheet stored in APEv2
  tag to stdout (equivalent to *-x “cuesheet”*)
#+end_quote

*-cc*

#+begin_quote
  extract cuesheet stored in APEv2 tag to source-name.cue file in same
  directory as decoded audio file (equivalent to *-xx
  “cuesheet=%a.cue”*)
#+end_quote

*--caf-be*, *--caf-le*

#+begin_quote
  force output to big-endian or little-endian Core Audio, extension
  “.caf”
#+end_quote

*-d*

#+begin_quote
  delete source file if successful (use with caution!)
#+end_quote

*--dff*, *--dsdiff*

#+begin_quote
  force output to Philips DSDIFF, DSD audio source only, extension
  “.dff”
#+end_quote

*--dsf*

#+begin_quote
  force output to Sony DSF, DSD audio source only, extension “.dsf”
#+end_quote

*-f*

#+begin_quote
  do not decode audio but simply display summary information about
  WavPack file to stdout in a machine-parsable format (see
  doc/wavpack_doc.html or cli/wvunpack.c in repository for format
  details)
#+end_quote

*--help*

#+begin_quote
  display extended help
#+end_quote

*-i*

#+begin_quote
  ignore .wvc file (forces hybrid lossy decompression)
#+end_quote

*-m*

#+begin_quote
  calculate and display MD5 signature; verify if lossless
#+end_quote

*-n*

#+begin_quote
  no audio decoding (use with *-xx* to extract tags only)
#+end_quote

*--normalize-floats*

#+begin_quote
  normalize float audio to +/-1.0 if it isnt already (rarely the case,
  but alters audio and fails MD5)
#+end_quote

*--no-utf8-convert*

#+begin_quote
  leave extracted text tags in UTF-8 encoding during extraction or
  display
#+end_quote

*-o */OUTFILE/

#+begin_quote
  specify output filename (only if single source file) or target
  directory (must exist)
#+end_quote

*-q*

#+begin_quote
  quiet (keep console output to a minimum)
#+end_quote

*-r*, *--raw*

#+begin_quote
  force raw PCM or DSD audio decode by skipping headers & trailers,
  results in source-name.raw
#+end_quote

*-s*

#+begin_quote
  do not decode audio but simply display summary information about
  WavPack file to stdout
#+end_quote

*-ss*

#+begin_quote
  do not decode audio but simply display summary and tag information
  about WavPack file to stdout
#+end_quote

* --skip=[-][*/sample/*|*/hh/*:*/mm/*:*/ss.ss/*] *

#+begin_quote
  start decoding at specified sample or time index, specifying a *-*
  causes sample/time to be relative to EOF
#+end_quote

*-t*

#+begin_quote
  copy input files time stamp to output file(s)
#+end_quote

* --until=[+|-][*/sample/*|*/hh/*:*/mm/*:*/ss.ss/*] *

#+begin_quote
  stop decoding at specified sample or time index, specifying a *+*
  causes sample/time to be relative to *--skip* point, specifying a *-*
  causes sample/time to be relative to EOF
#+end_quote

*-v*

#+begin_quote
  verify source data only (no output file created)
#+end_quote

*-vv*

#+begin_quote
  quick verify (no output, version 5+ files only)
#+end_quote

*--version*

#+begin_quote
  write program version to stdout
#+end_quote

*-w*, *--wav*

#+begin_quote
  force output to Microsoft RIFF/RF64, extension “.wav”
#+end_quote

*--w64*

#+begin_quote
  force output to Sony Wave64, extension “.w64”
#+end_quote

*-x “*/Field/”

#+begin_quote
  do not decode audio but instead just extract the specified tag field
  to stdout
#+end_quote

*-xx “*/Field/[=/file/]”

#+begin_quote
  extract the specified tag field to named file in same directory as
  decoded audio file; optional filename specification may contain *%a*
  which is replaced with the audio file base name, *%t* replaced with
  the tag field name (note: comes from data for binary tags) and *%e*
  replaced with the extension from the binary tag source file (or “txt”
  for text tag).
#+end_quote

*-y*

#+begin_quote
  yes to overwrite warning (use with caution!)
#+end_quote

*-z[*/n/*]*

#+begin_quote
  dont set (n = 0 or omitted) or set (n = 1) console title to indicate
  progress (leaves "WvUnpack Completed")
#+end_quote

* SEE ALSO
*wavpack*(1), *wvgain*(1), *wvtag*(1)

Please visit www.wavpack.com for more information

* COPYRIGHT
This manual page was written by Sebastian Dröge <slomo@debian.org> and
David Bryant <david@wavpack.com>. Permission is granted to copy,
distribute and/or modify this document under the terms of the BSD
License.

* AUTHORS
*Sebastian Dröge* <slomo@debian.org>

#+begin_quote
  Original author
#+end_quote

*David Bryant* <david@wavpack.com>

#+begin_quote
  Updates
#+end_quote

* COPYRIGHT
\\
Copyright © 2005 Sebastian Dröge\\
Copyright © 2021 David Bryant\\
