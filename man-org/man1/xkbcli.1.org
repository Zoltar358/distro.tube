#+TITLE: Man1 - xkbcli.1
#+DESCRIPTION: Linux manpage for xkbcli.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a commandline tool to query, compile and test XKB keymaps, layouts
and other elements.

Print help and exit

Print the version and exit

Compile an XKB keymap, see

Show how to type a given Unicode codepoint, see

Interactive debugger for XKB keymaps for X11, see

Interactive debugger for XKB keymaps for Wayland, see

Interactive debugger for XKB keymaps for evdev, see

List available layouts and more, see

Note that not all tools may be available on your system.

exited successfully

an error occured

program was called with invalid arguments
