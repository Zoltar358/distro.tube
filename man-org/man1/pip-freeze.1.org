#+TITLE: Man1 - pip-freeze.1
#+DESCRIPTION: Linux manpage for pip-freeze.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pip-freeze - description of pip freeze command

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know why you
    came to this page and what on it helped you and what did not. (/Read
    more about this research/)
  #+end_quote
#+end_quote

* DESCRIPTION
Output installed packages in requirements format.

packages are listed in a case-insensitive sorted order.

* USAGE

#+begin_quote

  #+begin_quote
    #+begin_example
      python -m pip freeze [options]
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *-r, --requirement <file>* :: Use the order in the given
    requirements file and its comments when generating output. This
    option can be used multiple times.
#+end_quote

#+begin_quote
  - *-f, --find-links <url>* :: URL for finding packages, which will be
    added to the output.
#+end_quote

#+begin_quote
  - *-l, --local* :: If in a virtualenv that has global access, do not
    output globally-installed packages.
#+end_quote

#+begin_quote
  - *--user* :: Only output packages installed in user-site.
#+end_quote

#+begin_quote
  - *--path <path>* :: Restrict to the specified installation path for
    listing packages (can be used multiple times).
#+end_quote

#+begin_quote
  - *--all* :: Do not skip these packages in the output: pip,
    setuptools, distribute, wheel
#+end_quote

#+begin_quote
  - *--exclude-editable* :: Exclude editable package from output.
#+end_quote

#+begin_quote
  - *--exclude <package>* :: Exclude specified package from the output
#+end_quote

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know:

    #+begin_quote

      1. What problem were you trying to solve when you came to this
         page?

      2. What content was useful?

      3. What content was not useful?
    #+end_quote
  #+end_quote
#+end_quote

* AUTHOR
pip developers

* COPYRIGHT
2008-2021, PyPA
