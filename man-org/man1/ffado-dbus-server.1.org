#+TITLE: Man1 - ffado-dbus-server.1
#+DESCRIPTION: Linux manpage for ffado-dbus-server.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dbus - expose the mixer features of connected FFADO devices through DBus

* SYNOPSIS
*ffado-dbus-server [OPTION...]*

* DESCRIPTION
*ffado-dbus-server* exposes the mixer and control features of connected
FFADO devices though a DBUS interface. The extent of support for
controls on a given device depends entirely on the status of that
device's FFADO driver.

By using DBus it is possible for control applications to be developed
independently of FFADO. The DBus interface provided by
*ffado-dbus-server* is used by *ffado-mixer* but there is no reason
other implementations could not be written.

* OPTIONS
- *-?, --help, --usage* :: Show brief usage information and exit

- *-V, --version* :: Print the program version and exit

- *-q, -q, --quiet, --silent* :: Don't produce any output

- *-n, --node=ID* :: Only expose the mixer of a device on a specific
  node with an ID of /ID/

- *-p, --port=NUM* :: Specify use of IEEE1394 port /NUM/

- *"-v, --verbose=LEVEL* :: Produce verbose output. The higher the
  /LEVEL/ the more verbose the messages. The some verbose messages may
  only be available of FFADO was compiled with debugging enabled.

* SEE ALSO
*ffado-mixer*(1)
