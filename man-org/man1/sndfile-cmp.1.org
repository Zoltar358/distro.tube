#+TITLE: Man1 - sndfile-cmp.1
#+DESCRIPTION: Linux manpage for sndfile-cmp.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
compares the audio data of two sound files. For two files to compare as
being the same, their channel counts, sample rate, audio data lengths
and actual audio data must match. Other differences such as string
metadata like song title, artist etc and their presence or absence are
ignored.

The audio data is the same.

The audio data differs.
