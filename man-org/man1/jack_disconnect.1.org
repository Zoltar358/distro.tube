#+TITLE: Manpages - jack_disconnect.1
#+DESCRIPTION: Linux manpage for jack_disconnect.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about jack_disconnect.1 is found in manpage for: [[../man1/jack_connect.1][man1/jack_connect.1]]