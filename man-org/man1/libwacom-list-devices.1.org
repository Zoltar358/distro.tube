#+TITLE: Man1 - libwacom-list-devices.1
#+DESCRIPTION: Linux manpage for libwacom-list-devices.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libwacom-list-devices - utility to list supported tablet devices

* SYNOPSIS
*libwacom-list-devices [--format=yaml|datafile]*

* DESCRIPTION
libwacom-list-devices is a debug utility to list all supported tablet
devices identified by libwacom. It is usually used to check whether a
libwacom installation is correct after adding custom data files.

* OPTIONS
- *--format=yaml|datafile* :: Sets the output format to be used. If
  /yaml/, the output format is YAML comprising the bus type, vendor and
  product ID and the device name. If /datafile/, the output format
  matches the tablet data files. The default is /yaml/.
