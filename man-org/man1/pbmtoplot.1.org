#+TITLE: Man1 - pbmtoplot.1
#+DESCRIPTION: Linux manpage for pbmtoplot.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtoplot - convert a PBM image into a Unix 'plot' file

* SYNOPSIS
*pbmtoplot* [/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtoplot* reads a PBM image as input and produces a Unix *plot* file
as output.

Note that there is no plottopbm tool - this transformation is one-way.

* SEE ALSO
*pbm*(5) , *plot*(1)

* AUTHOR
Copyright (C) 1990 by Arthur David Olson.
