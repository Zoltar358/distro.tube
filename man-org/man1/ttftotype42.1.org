#+TITLE: Man1 - ttftotype42.1
#+DESCRIPTION: Linux manpage for ttftotype42.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ttftotype42 - create PostScript Type 42 wrapper of TrueType font

* SYNOPSIS
*ttftotype42* [/input/ [/output/]]

* DESCRIPTION
*Ttftotype42* converts TrueType or TrueType-flavored OpenType font
programs into PostScript Type 42 format, which is a wrapper for the
TrueType outlines. This conversion preserves all outlines and hint
information from the original TrueType font. The Type 42 wrapper uses
glyph names identical to those expected by **() or **() so encoding
files suitable for TrueType fonts and *pdflatex* will also work for the
Type 42 fonts and **() If the file /output/ is not specified output goes
to the standard output. If the file /input/ is not specified input comes
from the standard input.

* OPTIONS
- *-o*/ file, /*--output*/ file/ :: Write output font to /file/ instead
  of the standard output.

- *-q*, *--quiet* :: Do not generate any error messages.

- *-h*, *--help* :: Print usage information and exit.

- *-v*, *--version* :: Print the version number and some short
  non-warranty information and exit.

* SEE ALSO
**() **() **() **()

Adobe Technical Note #5012, /The Type 42 Font Format Specification/

/OpenType Specification/, Version 1.4

* AUTHOR
Eddie Kohler (ekohler@gmail.com)

The latest version is available from:\\
http://www.lcdf.org/type/
