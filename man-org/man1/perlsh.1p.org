#+TITLE: Man1 - perlsh.1p
#+DESCRIPTION: Linux manpage for perlsh.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
perlsh - one-line perl evaluator with line editing function and variable
name completion function

* SYNOPSIS
perlsh

* DESCRIPTION
This program reads input a line, and evaluates it by perl interpreter,
and prints the result. If the result is a list value then each value of
the list is printed line by line. This program can be used as a very
strong calculator which has whole perl functions.

This is a sample program Term::ReadLine::Gnu module. When you input a
line, the line editing function of GNU Readline Library is available.
Perl symbol name completion function is also available.

Before invoking, this program reads /~/.perlshrc/ and evaluates the
content of the file.

When this program is terminated, the content of the history buffer is
saved in a file /~/.perlsh_history/, and it is read at next invoking.

* VARIABLES
You can customize the behavior of =perlsh= by setting following
variables in /~/.perlshrc/;

- $PerlSh::PS1 :: The primary prompt string. The following
  backslash-escaped special characters can be used. \h: host name \u:
  user name \w: package name \!: history number The default value is
  `=\w[\!]$ ='.

- $PerlSh::PS2 :: The secondary prompt string. The default value is
  `=> ='.

- $PerlSh::HISTFILE :: The name of the file to which the command history
  is saved. The default value is =~/.perlsh_history=.

- $PerlSh::HISTSIZE :: If not =undef=, this is the maximum number of
  commands to remember in the history. The default value is 256.

- $PerlSh::STRICT :: If true, restrict unsafe constructs. See
  =use strict= in perl man page. The default value is 0;

* FILES
- ~/.perlshrc :: This file is eval-ed at initialization. If a subroutine
  =afterinit= is defined in this file, it will be eval-ed after
  initialization. Here is a sample. # -*- mode: perl -*- # decimal to
  hexa sub h { map { sprintf("0x%x", $_ ) } @_;} sub tk {
  $t->tkRunning(1); use Tk; $mw = MainWindow->new(); } # for debugging
  Term::ReadLine::Gnu sub afterinit { *t = \$PerlSh::term; *a =
  \$PerlSh::attribs; }

- ~/.perlsh_history :: 

- ~/.inputrc :: 

A initialization file for the GNU Readline Library. Refer its manual for
details.

* SEE ALSO
Term::ReadLine::Gnu <http://search.cpan.org/dist/Term-ReadLine-Gnu/>

GNU Readline Library
<https://tiswww.cwru.edu/php/chet/readline/rltop.html>

* AUTHOR
Hiroo Hayashi <hiroo.hayashi@computer.org>
