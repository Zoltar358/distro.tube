#+TITLE: Man1 - xml2pmx.1
#+DESCRIPTION: Linux manpage for xml2pmx.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* DESCRIPTION
xml2pmx translates MusicXML files to input suitable for PMX processing

* SYNOPSIS
*xml2pmx* [*-v | --version | -h | --help*]

*xml2pmx* /input/.xml /output/.pmx [ p | d | v | s | l ] ..

The *musixtex.lua* script from version 0.23 supports *xml2pmx* as a
pre-preprocessor for MusicXML files.

* OPTIONS
The meanings of the letter options are as follows:

p XML parser output; not recommended unless the terminal window process
has enough storage

d information on "directions" (dynamical marks etc)

v distribution of voices over instruments and measures

s statistics of MusicXML tags like <note>, <measure> etc

l for extracting lyrics in a separate file

* BUGS
*xml2pmx* processes input in UTF-8 encoding only. MusicXML files in
UTF-16 encoding should be converted using, for example, *recode* (in
Unix-like systems) or by importing into an editor like *notepad* and
saving in UTF-8 encoding.

* SEE ALSO
*pmx*(1) *obc*(1) *musixtex*(1) *recode*(1)

* AUTHORS
This man page was written by Bob Tennent <rdt@cs.queensu.ca>. *xml2pmx*
was written by Dieter Gloetzel <d.gloetzel@web.de> and adapted for
compilation by obc (the Oxford Oberon Compiler) by Mike Spivey
<mike@cs.ox.ac.uk>.
