#+TITLE: Man1 - gts-config.1
#+DESCRIPTION: Linux manpage for gts-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gts-config - defines values for programs using gts.

* SYNOPSIS
*gts-config* [ /OPTIONS/ ] [ /LIBRARIES/ ]

* DESCRIPTION
This manual page documents briefly the *gts-config* command.

* OPTIONS
These programs follow the usual GNU command line syntax, with long
options starting with two dashes (`-'). A summary of options is included
below.

- *--prefix[ =*/DIR/* ]* :: 

- *--exec-prefix[ =*/DIR/* ]* :: 

- *--version* :: 

- *--libs* :: 

- *--cflags* :: 

- *--check* :: 

* LIBRARIES
- *gts* :: 

- *gmodule* :: 

- *gthread* :: 

* AUTHOR
gts-config was written by Stephane Popinet
<popinet@users.sourceforge.net>.

This manual page was written by Ruben Molina <rmolina@udea.edu.co>, for
the Debian project (but may be used by others).
