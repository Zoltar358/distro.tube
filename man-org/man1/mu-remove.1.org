#+TITLE: Man1 - mu-remove.1
#+DESCRIPTION: Linux manpage for mu-remove.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*mu remove* is the *mu* command to remove messages from the database.

* SYNOPSIS
*mu remove [options] <file> [<files>]*

* DESCRIPTION
*mu remove* removes specific messages from the database, each of them
specified by their filename. The files do not have to exist in the file
system.

* OPTIONS
*mu remove* does not have its own options, but the general options for
determining the location of the database (/--muhome/) are available. See
*mu-index(1)* for more information.

* RETURN VALUE
*mu remove* returns 0 upon success; in general, the following error
codes are returned:

#+begin_example
  | code | meaning                           |
  |------+-----------------------------------|
  |    0 | ok                                |
  |    1 | general error                     |
  |    5 | some database update error        |
#+end_example

* BUGS
Please report bugs if you find them: *https://github.com/djcb/mu/issues*

* AUTHOR
Dirk-Jan C. Binnema <djcb@djcbsoftware.nl>

* SEE ALSO
*mu*(1), *mu-index*(1), *mu-add*(1)
