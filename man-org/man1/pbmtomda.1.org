#+TITLE: Man1 - pbmtomda.1
#+DESCRIPTION: Linux manpage for pbmtomda.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtomda - convert a PBM image to a Microdesign .mda

* SYNOPSIS
*pbmtomda*

[*-d*] [*-i*] [*--*]

[/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtomda* reads a PBM image as input and produces a MicroDesign 2 area
file (.MDA) as output.

If you do not specify /pbmfile/, *pbmtomda* uses Standard Input.

* OPTIONS
- *-d* :: Halve the height of the output file, to compensate for the
  aspect ratio used in MicroDesign files.

- *-i* :: Invert the colors used.

- *--* :: End of options (use this if the filename starts with '-')

* LIMITATIONS
There's no way to produce files in MicroDesign 3 format. MD3 itself and
*mdatopbm*(1) can read files in either format.

* SEE ALSO
*mdatopbm*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1999 John Elliott </jce@seasip.demon.co.uk/>.
