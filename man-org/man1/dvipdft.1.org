#+TITLE: Man1 - dvipdft.1
#+DESCRIPTION: Linux manpage for dvipdft.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
dvipdft - create thumbnail images for use with dvipdfm

* SYNOPSIS
*dvipdft* [/DVIPDFM-OPTIONS/...] /filename/ [*.dvi*]

* DESCRIPTION
*dvipdft* creates thumbnail pictures of the pages in your file and
subsequently runs dvipdfm to create a PDF file with these thumbnails.

* OPTIONS
All options are simply handed on to dvipdfm

* SEE ALSO

#+begin_quote
  *dvipdfm*(1)
#+end_quote

* BUGS
None known.

* AUTHOR
*dvipdft* was written by Mark A. Wicks and Thomas Esser.

This manual page was written by Frank Küster <frank@kuesterei.ch>, for
the Debian GNU/Linux system. It may be used by others without contacting
the author. Any mistakes or omissions in the manual page are my fault;
inquiries about or corrections to this manual page should be directed to
me (and not to the primary author).
