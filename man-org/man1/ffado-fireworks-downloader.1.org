#+TITLE: Man1 - ffado-fireworks-downloader.1
#+DESCRIPTION: Linux manpage for ffado-fireworks-downloader.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ffado-fireworks-downloader - firmware download application for Echo
Fireworks devices

* SYNOPSIS
*"ffado-fireworks-downloader*/[OPTION...]/*OPERATION*

* DESCRIPTION
*ffado-bridgeco-downloader* permits firmware updates to be made to Echo
Fireworks devices under Linux. /OPERATION/ describes the action to be
carried out. An /OPERATION/ comprises one of the following keywords and
any arguments required by that keyword.

- *list* :: List devices on the firewire bus.

- *display* :: Display information about a device and its firmware.

- *upload FILE* :: Upload the firmware contained in /FILE/ to the device

- *download FILE START_ADDR LEN* :: Download the flash contents from the
  device. /LEN/ bytes will be downloaded starting at /START_ADDR/ and
  written to the file /FILE/

- *verify FILE* :: Verify that the firmware contained in the device
  corresponds to the one in /FILE/

- *session_display* :: Show information about the session on the device.

- *session_info FILE* :: Show information about the device session
  stored in /FILE/

- *session_download FILE* :: Download the session content from the
  device into /FILE/

- *session_upload FILE* :: Upload the session from /FILE/ to the device.

* OPTIONS
- *-h, --help* :: Display brief usage information and exit

- *-p, --port=NUM* :: Specify use of IEEE1394 port /NUM/

- *-v, --verbose=LEVEL* :: Produce verbose output. The higher the
  /LEVEL/ the more verbose the messages. The some verbose messages may
  only be available of FFADO was compiled with debugging enabled.

- *-g, --guid=GUID* :: Specify the GUID of the device to interact with.
  If this is not specified an attempt will be made to automatically
  detect a suitable device.

- *-m, --magic=MAGIC* :: A magic number you have to obtain before this
  program will work. Specifying it on the command line means that you
  accept the risks which come with this tool. The magic number can be
  obtained in the source code.

* NOTES
Manipulating firmware can cause your device to stop working. The FFADO
project accepts no responsibility if this occurs as a result of using
this tool.
