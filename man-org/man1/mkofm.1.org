#+TITLE: Man1 - mkofm.1
#+DESCRIPTION: Linux manpage for mkofm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mkofm - front end to *mktextfm*(1)

* SYNOPSIS
*mkofm* /ARGS/

* DESCRIPTION
*mkofm* takes all /ARGS/ and passes them to *mktextfm*(1). For more
information about appropriate arguments, see *mktextfm*(1).

* SEE ALSO
*mktextfm*(1)

* BUGS
None known.

* AUTHOR
Author unknown.

This manual page was written by C.M. Connelly <cmc@debian.org>, for the
Debian GNU/Linux system. It may be used by other distributions without
contacting the author. Any mistakes or omissions in the manual page are
my fault; inquiries about or corrections to this manual page should be
directed to me (and not to the primary author).
