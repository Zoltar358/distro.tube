#+TITLE: Manpages - dltest.1
#+DESCRIPTION: Linux manpage for dltest.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
dltest - A simple library symbol test

* SYNOPSIS
*dltest* ® /library symbol/ ®

* DESCRIPTION
*dltest* is simple test of occurence of the /symbol/ in the /library/.
The /library/ must be a full (with path) file name of the shared object,
in which the search for /symbol/ should be performed.

Without any parameters, dltest print short help message.

* EXAMPLES
To determine if the symbol *printf* is found in the /libc-2.18.so/:

#+begin_quote
  $ dltest /usr/lib/libc-2.18.so printf
#+end_quote

* AUTHORS
The authors of unixODBC are [[mailto:pharvey@codebydesign.com][Peter
Harvey]] and [[mailto:nick@lurcher.org][Nick Gorham]]. For the full list
of contributors see the /AUTHORS/ file.

* COPYRIGHT
unixODBC is licensed under the GNU Lesser General Public License. For
details about the license, see the /COPYING/ file.

Information about dltest.1 is found in manpage for: [[../libc-2\.18\.so :
dltest /usr/lib/libc-2.18.so printf][libc-2\.18\.so :
dltest /usr/lib/libc-2.18.so printf]]