#+TITLE: Man1 - pw-cli.1
#+DESCRIPTION: Linux manpage for pw-cli.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pw-cli - The PipeWire Command Line Interface

* SYNOPSIS
#+begin_example
  pw-cli [command]
#+end_example

* DESCRIPTION
Interact with a PipeWire instance.

When a command is given, *pw-cli* will execute the command and exit

When no command is given, *pw-cli* starts an interactive session with
the default PipeWire instance /pipewire-0/.

Connections to other, remote instances can be made. The current instance
name is displayed at the prompt. Some commands operate on the current
instance and some on the local instance.

Use the 'help' command to list the available commands.

* GENERAL COMMANDS

#+begin_quote
  - *help* :: Show a quick help on the commands available.

  - *quit* :: Exit from *pw-cli*
#+end_quote

* MODULE MANAGEMENT
#+begin_example
  Modules are loaded and unloaded in the local instance and can add functionality or objects to the local instance.
#+end_example

#+begin_quote
  - *load-module /name/ [/arguments.../]* :: Load a module specified by
    its name and arguments. For most modules it is OK to be loaded more
    than once.

  This command returns a module variable that can be used to unload the
  module.

  - *unload-module /module-var/* :: Unload a module, specified either by
    its variable.
#+end_quote

* OBJECT INTROSPECTION

#+begin_quote
  - *list-objects* :: List the objects of the current instance.

  Objects are listed with their /id/, /type/ and /version/.

  - *info /id/ | /all/* :: Get information about a specific object or
    /all/ objects.

  Requesting info about an object will also notify you of changes.
#+end_quote

* WORKING WITH REMOTES

#+begin_quote
  - *connect [/remote-name/]* :: Connect to a remote instance and make
    this the new current instance.

  If no remote name is specified, a connection is made to the default
  remote instance, usually /pipewire-0/.

  This command returns a remote var that can be used to disconnect or
  switch remotes.

  - *disconnect [/remote-var/]* :: Disconnect from a /remote instance/.

  If no remote name is specified, the current instance is disconnected.

  - *list-remotes* :: List all /remote instances/.

  - *switch-remote [/remote-var/]* :: Make the specified /remote/ the
    current instance.

  If no remote name is specified, the local instance is made current.
#+end_quote

* NODE MANAGEMENT

#+begin_quote
  - *create-node /factory-name/ [/properties.../]* :: Create a node from
    a factory in the current instance.

  Properties are key=value pairs separated by whitespace.

  This command returns a /node variable/.

  - *destroy-node /node-var/* :: Destroy a node.

  - *export-node /node-id/ [/remote-var/]* :: Export a node from the
    local instance to the specified instance. When no instance is
    specified, the node will be exported to the current instance.
#+end_quote

* LINK MANAGEMENT

#+begin_quote
  - *create-link /node-id/ /port-id/ /node-id/ /port-id/
    [/properties.../]* :: Create a link between 2 nodes and ports.

  Port /ids/ can be /-1/ to automatically select an available port.

  Properties are key=value pairs separated by whitespace.

  This command returns a /link variable/.

  - *destroy-link /link-var/* :: Destroy a link.
#+end_quote

* EXAMPLES
* AUTHORS
The PipeWire Developers
</https://gitlab.freedesktop.org/pipewire/pipewire/issues/>; PipeWire is
available from /https://pipewire.org/

* SEE ALSO
*pipewire(1)*, *pw-mon(1)*,
