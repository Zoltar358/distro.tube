#+TITLE: Man1 - ziptool.1
#+DESCRIPTION: Linux manpage for ziptool.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
modifies the zip archive

according to the

given.

Supported options:

Check zip archive consistency when opening it.

Error if archive already exists (only useful with

Guess file name encoding (for

command).

Display help.

Only read

bytes of archive. See also

Create archive if it doesn't exist. See also

Start reading input archive from

See also

Print raw file name encoding without translation (for

command).

Follow file name convention strictly (for

command).

Disregard current file contents, if any.

use this with care, it deletes all existing file contents when you
modify the archive.

For all commands below, the index is zero-based. In other words, the
first entry in the zip archive has index 0.

Supported commands and arguments are:

Add file called

using the string

from the command line as data.

Add directory

Add file

to archive, using

bytes from the file

as input data, starting at

Add file called

to archive using data from another zip archive

using the entry with index

and reading

bytes from

Output file contents for entry

to stdout.

Print the number of extra fields for archive entry

using

Print number of extra fields of type

for archive entry

using

Remove entry at

from zip archive.

Remove extra field number

from archive entry

using

Remove extra field number

of type

from archive entry

using

Print archive comment.

Print extra field

for archive entry

using

Print extra field

of type

for archive entry

using

Get file comment for archive entry

Print number of entries in archive using

Find entry in archive with the filename

using

and print its index.

Rename archive entry

to

Replace file contents for archive entry

with the string

Set archive comment to

Set extra field number

of type

for archive entry

using

to

Set file comment for archive entry

to string

Set file compression method for archive entry

to

using

Currently,

are ignored.

Set file encryption method for archive entry

to

with password

Set file modification time for archive entry

to UNIX mtime

Set file modification time for all archive entries to UNIX mtime

Set default password for encryption/decryption to

Print information about archive entry

Some commands take flag arguments. Supported flags are:

Some commands take compression method arguments. Supported methods are:

Some commands take encryption method arguments. Supported methods are:

Add a file called

to the zip archive

with data

where

is replaced with a newline character:

ziptool testbuffer.zip add teststring.txt \"This is a test.\n\"

Delete the first file from the zip archive

ziptool testfile.zip delete 0

was added in libzip 1.1.

and
