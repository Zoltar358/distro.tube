#+TITLE: Man1 - trizen.1
#+DESCRIPTION: Linux manpage for trizen.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
trizen - a lightweight wrapper for AUR, written in Perl.

* SYNOPSIS
/trizen/ <operation> [options] [targets]

* OPERATIONS
*-S, --sync*

#+begin_quote
  Synchronize packages. Packages are installed directly from the remote
  repositories or from the AUR, including all dependencies required to
  run the packages.
#+end_quote

*-C, --comments*

#+begin_quote
  Display AUR comments for a package.
#+end_quote

*-G, --get*

#+begin_quote
  Clone an AUR package in the current directory.
#+end_quote

*-R, --remove*

#+begin_quote
  Remove package(s) from the system. See: /pacman -Rh/
#+end_quote

*-Q, --query*

#+begin_quote
  Query the package database. See: /pacman -Qh/
#+end_quote

*-F, --files*

#+begin_quote
  Query the files database. See: /pacman -Fh/
#+end_quote

*-D, --database*

#+begin_quote
  Operate on the package database. See: /pacman -Dh/
#+end_quote

*-T, --deptest*

#+begin_quote
  Check dependencies. See: /pacman -Th/
#+end_quote

*-U, --upgrade*

#+begin_quote
  Install built packages from /--clone-dir/ or /`pwd`/. See: /pacman
  -Uh/
#+end_quote

* OPTIONS
*-q, --quiet*

#+begin_quote
  Display minimal output.
#+end_quote

*-a, --aur*

#+begin_quote
  Perform only AUR operations.
#+end_quote

*-r, --regular*

#+begin_quote
  Perform only regular /pacman/ operations (the opposite of /--aur/).
#+end_quote

*--stats*

#+begin_quote
  Display statistics for the installed packages.
#+end_quote

*--nocolors*

#+begin_quote
  Disable ANSI colors.
#+end_quote

*--forcecolors*

#+begin_quote
  Enable colors even when not writing to STDOUT.
#+end_quote

*--debug*

#+begin_quote
  Enable verbose mode.
#+end_quote

* SYNC OPTIONS (APPLY TO FI-SFR)
*-s, --search*

#+begin_quote
  Search for packages. By default, it includes packages from the pacman
  repos and from the AUR.
#+end_quote

*-i, --info*

#+begin_quote
  Display info for packages.
#+end_quote

*-m, --maintainer*

#+begin_quote
  Display the AUR packages maintained by a given /username/.
#+end_quote

*-p, --pkgbuild*

#+begin_quote
  Display the PKGBUILD of a given AUR package. For repo packages, it
  displays the download link.
#+end_quote

*-l, --local*

#+begin_quote
  Build and install packages from the current directory. This option is
  best used in combination with /-G/.

  After a package is cloned in the current directory, the user can
  inspect or edit the build files, then /-Sl <pkgname>/ will build and
  install the package, without pulling new changes from the AUR.
#+end_quote

*-u, --sysupgrade*

#+begin_quote
  Upgrades all packages that are out-of-date.
#+end_quote

*-y, --refresh*

#+begin_quote
  Refresh package databases.
#+end_quote

*-c, --clean*

#+begin_quote
  Clean the cache directories of /pacman/ and /trizen/. Add /-r/ to
  clean only pacman's cache, or /-a/ to clean only trizen's cache.
#+end_quote

*--devel*

#+begin_quote
  Update VCS packages during /-Su/.

  In combination with /--needed/, trizen will check if a package really
  needs to be updated (i.e. has a new commit). Currently, only git
  sources are supported.
#+end_quote

*--show-ood*

#+begin_quote
  Show out-of-date flagged packages during /-Su/.
#+end_quote

*--noinfo*

#+begin_quote
  Do not display package information after cloning.
#+end_quote

*--nopull*

#+begin_quote
  Do not /`git pull`/ new changes from the AUR when the build files of a
  package already exist locally.
#+end_quote

*--noedit*

#+begin_quote
  Do not prompt to edit the build files of packages.
#+end_quote

*--nobuild*

#+begin_quote
  Do not build packages (implies /--noedit/). Assumes a built package
  already exists.
#+end_quote

*--noinstall*

#+begin_quote
  Do not install packages after building.
#+end_quote

*--noconfirm*

#+begin_quote
  Do not ask for any confirmation.
#+end_quote

*--needed*

#+begin_quote
  Do not reinstall up-to-date packages.
#+end_quote

*--asdeps*

#+begin_quote
  Install packages as dependencies (non-explicit).
#+end_quote

*--asexplicit*

#+begin_quote
  Install packages as explicitly installed.
#+end_quote

*--skipinteg*

#+begin_quote
  Pass the /--skipinteg/ argument to /makepkg/.
#+end_quote

*--movepkg*

#+begin_quote
  Move built packages in pacman's cache directory.
#+end_quote

*--movepkg-dir='...'*

#+begin_quote
  Move built packages into this directory (implies /--movepkg/).
#+end_quote

*--clone-dir='...'*

#+begin_quote
  Directory where to clone and build packages.
#+end_quote

*--editor='...'*

#+begin_quote
  Editor command used to edit the build files of AUR packages. The
  default editor is /$VISUAL/ or /$EDITOR/ (in this order).
#+end_quote

*--pager-mode*

#+begin_quote
  Display the build files of packages in pager mode.
#+end_quote

*--pager='...'*

#+begin_quote
  Pager command used to display the build files of packages (with
  /--pager-mode/). The default pager is /$PAGER/.
#+end_quote

*--ignore='...'*

#+begin_quote
  Space-separated list of packages to ignore during /-Su/.
#+end_quote

* QUERY AUR OPTIONS
*-Qua*

#+begin_quote
  Display AUR updates only.
#+end_quote

*-Qma*

#+begin_quote
  Display foreign installed packages that do not exist in repos or in
  the AUR.
#+end_quote

* GET OPTIONS (APPLY TO FI-GFR)
*-d, --with-deps*

#+begin_quote
  Clone a package along with all its AUR dependencies that are not
  already installed.
#+end_quote

* INTERACTIVE MODE
Search and select packages to install, using the following syntax:

#+begin_quote
  /trizen [keywords]/
#+end_quote

By default, the results include both repo and AUR packages. Add
/--regular/ or /--aur/ to search only for repo or AUR packages,
respectively.

* EXAMPLES
#+begin_example
    trizen -S  <package>     # install <package>
    trizen -Ss <keyword>     # search for <keyword>
    trizen -Si <package>     # show info about <package>

    trizen -G  <package>     # clones <package>
    trizen -Gd <package>     # clones <package> along with its AUR dependencies
#+end_example

* CONFIGURATION
Configuration file: ~/.config/trizen/trizen.conf

* CONFIGURATION OPTIONS
Each configuration key can be used as a command-line argument, by
preceding it with `/--/` and (optionally) replacing underscores (`/_/`)
with dashes (`/-/`).

Example:

#+begin_quote
  /trizen --stats --packages-in-stats=50/
#+end_quote

The above command will display 50 packages in /--stats/.

* AUTHOR
Daniel Șuteu <trizen@protonmail.com>

* SEE ALSO
Upstream repository: https://github.com/trizen/trizen
