#+TITLE: Man1 - passwd.1ssl
#+DESCRIPTION: Linux manpage for passwd.1ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
openssl-passwd, passwd - compute password hashes

* SYNOPSIS
*openssl passwd* [*-help*] [*-crypt*] [*-1*] [*-apr1*] [*-aixmd5*]
[*-5*] [*-6*] [*-salt* /string/] [*-in* /file/] [*-stdin*] [*-noverify*]
[*-quiet*] [*-table*] [*-rand file...*] [*-writerand file*] {/password/}

* DESCRIPTION
The *passwd* command computes the hash of a password typed at run-time
or the hash of each password in a list. The password list is taken from
the named file for option *-in file*, from stdin for option *-stdin*, or
from the command line, or from the terminal otherwise. The Unix standard
algorithm *crypt* and the MD5-based BSD password algorithm *1*, its
Apache variant *apr1*, and its AIX variant are available.

* OPTIONS
- -help :: Print out a usage message.

- -crypt :: Use the *crypt* algorithm (default).

- -1 :: Use the MD5 based BSD password algorithm *1*.

- -apr1 :: Use the *apr1* algorithm (Apache variant of the BSD
  algorithm).

- -aixmd5 :: Use the *AIX MD5* algorithm (AIX variant of the BSD
  algorithm).

- -5 :: 

- -6 :: 

Use the *SHA256* / *SHA512* based algorithms defined by Ulrich Drepper.
See <https://www.akkadia.org/drepper/SHA-crypt.txt>.

- -salt string :: Use the specified salt. When reading a password from
  the terminal, this implies *-noverify*.

- -in file :: Read passwords from /file/.

- -stdin :: Read passwords from *stdin*.

- -noverify :: Don't verify when reading a password from the terminal.

- -quiet :: Don't output warnings when passwords given at the command
  line are truncated.

- -table :: In the output list, prepend the cleartext password and a TAB
  character to each password hash.

- -rand file... :: A file or files containing random data used to seed
  the random number generator. Multiple files can be specified separated
  by an OS-dependent character. The separator is *;* for MS-Windows, *,*
  for OpenVMS, and *:* for all others.

- [-writerand file] :: Writes random data to the specified /file/ upon
  exit. This can be used with a subsequent *-rand* flag.

* EXAMPLES
% openssl passwd -crypt -salt xx password xxj31ZMTZzkVA % openssl passwd
-1 -salt xxxxxxxx password $1$xxxxxxxx$UYCIxa628.9qXjpQCjM4a. % openssl
passwd -apr1 -salt xxxxxxxx password
$apr1$xxxxxxxx$dxHfLAsjHkDRmG83UXe8K0 % openssl passwd -aixmd5 -salt
xxxxxxxx password xxxxxxxx$8Oaipk/GPKhC64w/YVeFD/

* COPYRIGHT
Copyright 2000-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
