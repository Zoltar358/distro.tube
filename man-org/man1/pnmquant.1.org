#+TITLE: Man1 - pnmquant.1
#+DESCRIPTION: Linux manpage for pnmquant.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pnmquant - quantize the colors in a Netpbm image to a smaller set

* SYNOPSIS
*pnmquant* [*-center*|*-meancolor*|*-meanpixel*] [*-floyd*|*-fs*]
[*-nofloyd*|*-nofs*] [*-spreadbrightness*|*-spreadluminosity*] /ncolors/
[/pnmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmquant* reads a PNM image as input. It chooses /ncolors/ colors to
best represent the image, maps the existing colors to the new ones, and
writes a PNM image as output.

All options can be abbreviated to their shortest unique prefix. You may
use two hyphens instead of one to designate an option. You may use
either white space or equals signs between an option name and its value.

This program is simply a combination of *pnmcolormap* and *pnmremap*,
where the colors of the input are remapped using a color map which is
generated from the colors in that same input. The options have the same
meaning as in those programs. See their documentation to understand
*pnmquant*.

It is much faster to call *pnmcolormap* and *pnmremap* directly than to
run *pnmquant*. You save the overhead of the Perl interpreter and
creating two extra processes. *pnmquant* is just a convenience.

Here is an example of the relationship between the programs:

This:

#+begin_example
      $ pnmquant 256 myimage.pnm >/tmp/colormap.pnm >myimage256.pnm
#+end_example

does essentially this:

#+begin_example
      $ pnmcolormap 256 myimage.pnm >/tmp/colormap.pnm
      $ pnmremap -mapfile=/tmp/colormap.pnm myimage.pnm >myimage256.pnm
#+end_example

*pnmquant* did not exist before Netpbm 9.21 (January 2001). Before that,
*ppmquant* did the same thing, but only on PPM images. *ppmquant*
continues to exist, but is only a front end (for name compatibility) to
*pnmquant*.

* SEE ALSO
*pnmcolormap*(1) , *pnmremap*(1) , *ppmquantall*(1) , *pamdepth*(1) ,
*ppmdither*(1) , *ppmquant*(1) , *pnm*(5)
