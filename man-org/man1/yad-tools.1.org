#+TITLE: Man1 - yad-tools.1
#+DESCRIPTION: Linux manpage for yad-tools.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
yad-tools - show some miscellaneous stuff.

* SYNOPSIS
*yad-tools* [OPTIONS] STRING

* DESCRIPTION
*yad-tools* is a small command-line utility for getting information that
may be usefull in scripts.

* OPTIONS
** General options
- *-f*, *--pfd* :: This mode is intended to transform font names from
  pango to xft specification and vice versa. Additional argument is font
  name.

- *-i*, *--icon* :: This mode is intended to get full path to icon file
  from icon name. Additional argument is icon name.

- *--show-langs* :: Show list of available languages for spell checking.
  Works only if yad built with /GSpell/ support.

- *--show-themes* :: Show list of available GtkSourceView themes. Works
  only if yad built with /GtkSourceView/ support.

** PFD mode options
- *-x*, *--xft* :: Print font name in xft format. This is default.

- *-p*, *--pango* :: Print font name in pango format.

** Icon mode options
- *-s*, *--size*=/SIZE/ :: Use specified icon size. Default is 24.

- *-t*, *--type*=/TYPE/ :: Obtain icon size from one of predefined GTK+
  types. /TYPE/ is one of the following /menu/, /button/,
  /small_toolbar/, /large_toolbar/, /dnd/ or /dialog/,

- *--theme*=/THEME/ :: Use specified icon theme instead of default.
