#+TITLE: Man1 - uuidcdef.1
#+DESCRIPTION: Linux manpage for uuidcdef.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
uuidgen - a universally unique identifier (UUID) generator for XPLC

* SYNOPSIS
*uuidgen* [ /OPTION/ ] ...\\
*uuidcdef* [ /OPTION/ ] ...

* DESCRIPTION
*uuidgen* generates a new universally unique identifier (UUID). This
UUID can be assumed to be unique to any other UUIDs generated on the
local system, as well as those created on any other systems, at any time
in the past or future.

By default, *uuidgen* will try to generate a UUID with as much
randomness as possible. It tries to pick out a high-quality randomness
source. If it cannot find one, it will fall back on using a time-based
algorithm.

* OPTIONS
- *-c, --cdef* :: Print the UUID as an initializer for a C structure,
  compatible with the type used by XPLC.

This is the default mode for *uuidcdef*.

- *-r, --random* :: Demand that the random-based algorithm be used to
  generate the UUID. This will prefer a high-quality randomness source,
  but may fall back on a low-entropy one.

- *-t, --time* :: Demand that the time-based algorithm be used to
  generate the UUID.

- *-u, --uuid* :: Print the UUID like:
  "26175a27-c35d-4615-8dfb-1509eab28ef8". This is the *printf*(3)
  format: "%08x-%04x-%04x-%04x-%012x".

This is the default mode for *uuidgen*.

* AUTHOR
Simon Law

* SEE ALSO
*uuidgen*(1).
