#+TITLE: Man1 - ffado-bridgeco-downloader.1
#+DESCRIPTION: Linux manpage for ffado-bridgeco-downloader.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ffado-bridgeco-downloader - firmware download application for BridgeCo
devices

* SYNOPSIS
*"ffado-bridgeco-downloader*/[OPTION...]/*OPERATION*

* DESCRIPTION
*ffado-bridgeco-downloader* permits firmware updates to be made to
BridgeCo (aka BeBoB) devices under Linux. /OPERATION/ describes the
action to be carried out. An /OPERATION/ is introduced with the GUID of
the device to operate on. This is followed by one of the following
keywords and any arguments required by that keyword.

- *display* :: Display information about the interface

- *setguid NEW_GUID* :: Set the device's GUID to /NEW_GUID/

- *firmware FILE* :: Download the firmware file /FILE/ to the device.

- *cne FILE* :: Download the CnE file /FILE/ to the device.

- *bcd FILE* :: Parse the BeBoB BCD file /FILE/ and display information
  about the contents.

* OPTIONS
- *-?, --help, --usage* :: Display brief usage information and exit

- *-V, --version* :: Show the program version and exit

- *-p, --port=NUM* :: Specify use of IEEE1394 port /NUM/

- *-v, --verbose=LEVEL* :: Produce verbose output. The higher the
  /LEVEL/ the more verbose the messages. The some verbose messages may
  only be available of FFADO was compiled with debugging enabled.

- *-b, --noboot* :: Do not start the bootloader. Use this if the
  bootloader is already running.

- *-f, --force* :: Force firmware download in the event the program
  doesn't think it's a good idea. Use with caution.

- *-m, --magic=MAGIC* :: A magic number you have to obtain before this
  program will work. Specifying it on the command line means that you
  accept the risks which come with this tool. The magic number can be
  obtained in the source code.

* NOTES
Manipulating firmware can cause your device to stop working. The FFADO
project accepts no responsibility if this occurs as a result of using
this tool.
