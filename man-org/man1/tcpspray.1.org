#+TITLE: Man1 - tcpspray.1
#+DESCRIPTION: Linux manpage for tcpspray.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tcpspray - TCP/IP bandwidth measurement tool (Discard and Echo client)

* SYNOPSIS
*tcpspray* [*-46ev*] [*-b block_size*] [*-d wait_�s*] [ *-f filename*]
[*-n count*] <*hostname*> [*port*]

* DESCRIPTON
*tcpspray* uses the /Discard/ resp. /Echo/ protocol (RFC 863 resp.
RFC 862) to perform bandwidth measurements of /TCP/ sessions between the
local system, and a /Discard/ resp. /Echo/ server.

Unix-based hosts can provide a Discard and/or Echo servers with the
Internet /super-server/ *inetd*".*On*Windows*NT,*the /simple network
protocols/ optional component will do the same.

The name or address of the server node must be specified. tcpspray will
automatically try to use IPv6 when available. If not, or if it fails, it
will fallback to IPv4. However, tcpspray4 resp. tcpspray6 only try to
use IPv4 resp. IPv6.

* OPTIONS
- *-4* or *--ipv4* :: Force usage of TCP over IPv4.

- *-6* or *--ipv6* :: Force usage of TCP over IPv6.

- *-b block_size* or *--bsize block_size* :: Send block of the specified
  byte size (default: 1024).

- *-d wait_�s* or *--delay wait_�s* :: Waits for the given amount of
  microseconds after any given was sent before attempting to send the
  next one. There is no delay by default.

- *-e* or *--echo* :: Use the Echo protocol instead of Discard. tcpspray
  will measure the time required to send data and receive it back,
  instead of simply sending it.

- *-f filename* or *--fill filename* :: Read data from the specified
  file to fill sent blocks with. If the file is smaller than the size of
  blocks, or if no file were specified, the remaining trailing bytes are
  all set to zero.

- *-h* or *--help* :: Display some help and exit.

- *-n block_count* or *--count block_count* :: Send the specified amount
  of data blocks for the measurements (default: 100).

- *-V* or *--version* :: Display program version and license and exit.

- *-v* or *--verbose* :: Display more verbose informations. In
  particular, tcpspray will print a dot each time a block is sent. If
  the Echo protocol is used (option -e), dots will be erased as data is
  received back.

* DIAGNOSTICS
If you get no response while you know the remote host is up, it is most
likely that it has no Discard/Echo service running, or that these
services are blocked by a firewall. Running tcptraceroute6(8) resp.
tcptraceroute(8) toward the IPv6 resp. IPv4 remote host might help
detecting such a situation.

* SECURITY
tcpspray does not require any privilege to run.

* SEE ALSO
tcp(7), inetd(8), tcptraceroute6(8), tcptraceroute(8)

* AUTHOR
R�mi Denis-Courmont <remi at remlab dot net>

http://www.remlab.net/ndisc6/
