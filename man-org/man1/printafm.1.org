#+TITLE: Man1 - printafm.1
#+DESCRIPTION: Linux manpage for printafm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
printafm - Print the metrics from a Postscript font in AFM format using
ghostscript

* SYNOPSIS
*printafm* /fontname/

* DESCRIPTION
This script invokes *gs*(1) to print the metrics from a font in AFM
format. Output goes to stdout.

* SEE ALSO
gs(1)

* VERSION
This document was last revised for Ghostscript version 9.55.0.

* AUTHOR
Artifex Software, Inc. are the primary maintainers of Ghostscript. This
manpage by George Ferguson.
