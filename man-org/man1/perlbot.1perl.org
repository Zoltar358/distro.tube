#+TITLE: Man1 - perlbot.1perl
#+DESCRIPTION: Linux manpage for perlbot.1perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
perlbot - Links to information on object-oriented programming in Perl

* DESCRIPTION
For information on OO programming with Perl, please see perlootut and
perlobj.

(The above documents supersede the collection of tricks that was
formerly here in perlbot.)
