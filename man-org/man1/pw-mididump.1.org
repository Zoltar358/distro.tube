#+TITLE: Man1 - pw-mididump.1
#+DESCRIPTION: Linux manpage for pw-mididump.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pw-mididump - The PipeWire MIDI dump

* SYNOPSIS
#+begin_example
  pw-mididump [options] [FILE]
#+end_example

* DESCRIPTION
Dump MIDI messages to stdout.

When a MIDI file is given, the events inside the file are printed.

When no file is given, *pw-mididump* creates a PipeWire MIDI input
stream and will print all MIDI events received on the port to stdout.

* OPTIONS

#+begin_quote
  - *-r | --remote=NAME* :: The name the remote instance to monitor. If
    left unspecified, a connection is made to the default PipeWire
    instance.

  - *-h | --help* :: Show help.
#+end_quote

#+begin_quote
  - *--version* :: Show version information.
#+end_quote

* AUTHORS
The PipeWire Developers
</https://gitlab.freedesktop.org/pipewire/pipewire/issues/>; PipeWire is
available from /https://pipewire.org/

* SEE ALSO
*pipewire(1)*, *pw-cat(1)*,
