#+TITLE: Man1 - picom-trans.1
#+DESCRIPTION: Linux manpage for picom-trans.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
picom-trans - an opacity setter tool

* SYNOPSIS
*picom-trans* [-w /WINDOW_ID/] [-n /WINDOW_NAME/] [-c] [-s] /OPACITY/

* DESCRIPTION
*picom-trans* is a bash script that sets /_NET_WM_WINDOW_OPACITY/
attribute of a window using standard X11 command-line utilities,
including *xprop*(1) and *xwininfo*(1). It is similar to *transset*(1)
or *transset-df*(1).

* OPTIONS
*-w* /WINDOW_ID/

#+begin_quote
  Specify the window id of the target window.
#+end_quote

*-n* /WINDOW_NAME/

#+begin_quote
  Specify and try to match a window name.
#+end_quote

*-c*

#+begin_quote
  Specify the currently active window as target. Only works if EWMH
  /_NET_ACTIVE_WINDOW/ property exists on root window.
#+end_quote

*-s*

#+begin_quote
  Select target window with mouse cursor. This is the default if no
  window has been specified.
#+end_quote

*-o* /OPACITY/

#+begin_quote
  Specify the new opacity value for the window. This value can be
  anywhere from 1-100. If it is prefixed with a plus or minus (+/-),
  this will increment or decrement from the target window's current
  opacity instead.
#+end_quote

* EXAMPLES

#+begin_quote
  ·

  Set the opacity of the window with specific window ID to 75%:

  #+begin_quote
    #+begin_example
      picom-trans -w "$WINDOWID" 75
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Set the opacity of the window with the name "urxvt" to 75%:

  #+begin_quote
    #+begin_example
      picom-trans -n "urxvt" 75
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Set current window to opacity of 75%:

  #+begin_quote
    #+begin_example
      picom-trans -c 75
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Select target window and set opacity to 75%:

  #+begin_quote
    #+begin_example
      picom-trans -s 75
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Increment opacity of current active window by 5%:

  #+begin_quote
    #+begin_example
      picom-trans -c +5
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  ·

  Decrement opacity of current active window by 5%:

  #+begin_quote
    #+begin_example
      picom-trans -c -- -5
    #+end_example
  #+end_quote
#+end_quote

* BUGS
Please submit bug reports to https://github.com/yshui/picom.

* SEE ALSO
*picom*(1), *xprop*(1), *xwininfo*(1)
