#+TITLE: Man1 - F
#+DESCRIPTION: Man1 - F
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* F
#+begin_src bash :exports results
readarray -t starts_with_f < <(find . -type f -iname "f*" | sort)

for x in "${starts_with_f[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./faac.1.org][faac.1]]                       |
| [[file:./faad.1.org][faad.1]]                       |
| [[file:./factor.1.org][factor.1]]                     |
| [[file:./faked.1.org][faked.1]]                      |
| [[file:./fakeroot.1.org][fakeroot.1]]                   |
| [[file:./fallocate.1.org][fallocate.1]]                  |
| [[file:./false.1.org][false.1]]                      |
| [[file:./fax2ps.1.org][fax2ps.1]]                     |
| [[file:./fax2tiff.1.org][fax2tiff.1]]                   |
| [[file:./fc-cache.1.org][fc-cache.1]]                   |
| [[file:./fc-cat.1.org][fc-cat.1]]                     |
| [[file:./fc-conflist.1.org][fc-conflist.1]]                |
| [[file:./fc-list.1.org][fc-list.1]]                    |
| [[file:./fc-match.1.org][fc-match.1]]                   |
| [[file:./fc-pattern.1.org][fc-pattern.1]]                 |
| [[file:./fc-query.1.org][fc-query.1]]                   |
| [[file:./fc-scan.1.org][fc-scan.1]]                    |
| [[file:./fc-validate.1.org][fc-validate.1]]                |
| [[file:./fd.1.org][fd.1]]                         |
| [[file:./feh.1.org][feh.1]]                        |
| [[file:./fetch-ebook-metadata.1.org][fetch-ebook-metadata.1]]       |
| [[file:./ffado-bridgeco-downloader.1.org][ffado-bridgeco-downloader.1]]  |
| [[file:./ffado-dbus-server.1.org][ffado-dbus-server.1]]          |
| [[file:./ffado-diag.1.org][ffado-diag.1]]                 |
| [[file:./ffado-dice-firmware.1.org][ffado-dice-firmware.1]]        |
| [[file:./ffado-fireworks-downloader.1.org][ffado-fireworks-downloader.1]] |
| [[file:./ffado-mixer.1.org][ffado-mixer.1]]                |
| [[file:./ffmpeg.1.org][ffmpeg.1]]                     |
| [[file:./ffmpeg-all.1.org][ffmpeg-all.1]]                 |
| [[file:./ffmpeg-bitstream-filters.1.org][ffmpeg-bitstream-filters.1]]   |
| [[file:./ffmpeg-codecs.1.org][ffmpeg-codecs.1]]              |
| [[file:./ffmpeg-devices.1.org][ffmpeg-devices.1]]             |
| [[file:./ffmpeg-filters.1.org][ffmpeg-filters.1]]             |
| [[file:./ffmpeg-formats.1.org][ffmpeg-formats.1]]             |
| [[file:./ffmpeg-protocols.1.org][ffmpeg-protocols.1]]           |
| [[file:./ffmpeg-resampler.1.org][ffmpeg-resampler.1]]           |
| [[file:./ffmpeg-scaler.1.org][ffmpeg-scaler.1]]              |
| [[file:./ffmpegthumbnailer.1.org][ffmpegthumbnailer.1]]          |
| [[file:./ffmpeg-utils.1.org][ffmpeg-utils.1]]               |
| [[file:./ffplay.1.org][ffplay.1]]                     |
| [[file:./ffplay-all.1.org][ffplay-all.1]]                 |
| [[file:./ffprobe.1.org][ffprobe.1]]                    |
| [[file:./ffprobe-all.1.org][ffprobe-all.1]]                |
| [[file:./fftwf-wisdom.1.org][fftwf-wisdom.1]]               |
| [[file:./fftwl-wisdom.1.org][fftwl-wisdom.1]]               |
| [[file:./fftwq-wisdom.1.org][fftwq-wisdom.1]]               |
| [[file:./fftw-wisdom.1.org][fftw-wisdom.1]]                |
| [[file:./fftw-wisdom-to-conf.1.org][fftw-wisdom-to-conf.1]]        |
| [[file:./fgconsole.1.org][fgconsole.1]]                  |
| [[file:./fgrep.1.org][fgrep.1]]                      |
| [[file:./fiascotopnm.1.org][fiascotopnm.1]]                |
| [[file:./file.1.org][file.1]]                       |
| [[file:./FileCheck.1.org][FileCheck.1]]                  |
| [[file:./filezilla.1.org][filezilla.1]]                  |
| [[file:./fincore.1.org][fincore.1]]                    |
| [[file:./find.1.org][find.1]]                       |
| [[file:./find-libdeps.1.org][find-libdeps.1]]               |
| [[file:./find-libprovides.1.org][find-libprovides.1]]           |
| [[file:./fish.1.org][fish.1]]                       |
| [[file:./fish_indent.1.org][fish_indent.1]]                |
| [[file:./fish_key_reader.1.org][fish_key_reader.1]]            |
| [[file:./fitstopnm.1.org][fitstopnm.1]]                  |
| [[file:./fixproc.1.org][fixproc.1]]                    |
| [[file:./fix-qdf.1.org][fix-qdf.1]]                    |
| [[file:./flac.1.org][flac.1]]                       |
| [[file:./flex.1.org][flex.1]]                       |
| [[file:./flock.1.org][flock.1]]                      |
| [[file:./floppyd.1.org][floppyd.1]]                    |
| [[file:./floppyd_installtest.1.org][floppyd_installtest.1]]        |
| [[file:./fltk-config.1.org][fltk-config.1]]                |
| [[file:./fluid.1.org][fluid.1]]                      |
| [[file:./fluidsynth.1.org][fluidsynth.1]]                 |
| [[file:./fmt.1.org][fmt.1]]                        |
| [[file:./fmtutil.1.org][fmtutil.1]]                    |
| [[file:./fmtutil-sys.1.org][fmtutil-sys.1]]                |
| [[file:./fold.1.org][fold.1]]                       |
| [[file:./fontinst.1.org][fontinst.1]]                   |
| [[file:./foomatic-combo-xml.1.org][foomatic-combo-xml.1]]         |
| [[file:./foomatic-compiledb.1.org][foomatic-compiledb.1]]         |
| [[file:./foomatic-configure.1.org][foomatic-configure.1]]         |
| [[file:./foomatic-perl-data.1.org][foomatic-perl-data.1]]         |
| [[file:./foomatic-ppdfile.1.org][foomatic-ppdfile.1]]           |
| [[file:./foomatic-ppd-options.1.org][foomatic-ppd-options.1]]       |
| [[file:./foomatic-printjob.1.org][foomatic-printjob.1]]          |
| [[file:./foomatic-rip.1.org][foomatic-rip.1]]               |
| [[file:./free.1.org][free.1]]                       |
| [[file:./fstopgm.1.org][fstopgm.1]]                    |
| [[file:./ftp.1.org][ftp.1]]                        |
| [[file:./funzip.1.org][funzip.1]]                     |
| [[file:./fuser.1.org][fuser.1]]                      |
| [[file:./fusermount.1.org][fusermount.1]]                 |
| [[file:./fusermount3.1.org][fusermount3.1]]                |
| [[file:./fzf.1.org][fzf.1]]                        |
| [[file:./fzf-tmux.1.org][fzf-tmux.1]]                   |
| [[file:./fzputtygen.1.org][fzputtygen.1]]                 |
| [[file:./fzsftp.1.org][fzsftp.1]]                     |
