#+TITLE: Man1 - system-config-printer.1
#+DESCRIPTION: Linux manpage for system-config-printer.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
system-config-printer - configure a CUPS server

* SYNOPSIS
*system-config-printer* [[--show-jobs /printer/] | [--debug] | [--help]]

* DESCRIPTION
*system-config-printer* configures a CUPS server. It uses the CUPS API
(bound to Python with pycups) to do this. The communication with the
server is performed using IPP. As a result, it is equally able to
configure a remote CUPS server as a local one.

* OPTIONS
*--help*

#+begin_quote
  Display a short usage message.
#+end_quote

*--show-jobs */printer/

#+begin_quote
  Show the named print queue.
#+end_quote

*--debug*

#+begin_quote
  Enable debugging output.
#+end_quote

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote
