#+TITLE: Manpages - zsoelim.1
#+DESCRIPTION: Linux manpage for zsoelim.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
zsoelim - satisfy .so requests in roff input

* SYNOPSIS
*zsoelim* [ *-CVh* ] [ /file/ . . . ]

* DESCRIPTION
*zsoelim* parses /file/ arguments, or if none are specified, its
standard input for lines of the form:

*.so* < /filename/ >

These requests are replaced by the contents of the /filename/ specified.
If the request cannot be met, *zsoelim* looks for /filename.ext/ where
/.ext/ can be one of *.gz*, *.Z* or *.z*. Other extension types may be
supported depending upon compile time options. If the request can be met
by a compressed file, this file is decompressed using an appropriate
decompressor and its output is used to satisfy the request.

Traditionally, *soelim* programs were used to allow roff preprocessors
to be able to preprocess the files referred to by the requests. This
particular version was written to circumvent problems created by support
for compressed manual pages.

* OPTIONS
- *-C*, *--compatible* :: This flag is available for compatibility with
  other *soelim* programs. Its use is to enable .so requests followed by
  something other than whitespace. As this is already the default
  behaviour, it is ignored.

- *-h*, *--help* :: Print a help message and exit.

- *-V*, *--version* :: Display version information.

* SEE ALSO
*groff*(1), *man*(1), *nroff*(1), *troff*(1)

* AUTHOR
#+begin_example
  Wilf. (G.Wilford@ee.surrey.ac.uk).
  Fabrizio Polacco (fpolacco@debian.org).
  Colin Watson (cjwatson@debian.org).
#+end_example

* BUGS
https://savannah.nongnu.org/bugs/?group=man-db

Information about zsoelim.1 is found in manpage for: [[../\- satisfy .so requests in roff input
use is to enable .so requests followed by something other than][\- satisfy .so requests in roff input
use is to enable .so requests followed by something other than]]