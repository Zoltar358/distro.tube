#+TITLE: Man1 - lxsession-default-apps.1
#+DESCRIPTION: Linux manpage for lxsession-default-apps.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxsession-default-apps - Configure default applications for LXSession

* SYNOPSIS
*lxsession-default-apps*

* DESCRIPTION
*lxsession-default-apps* is an utility to set up default applications
and other options for LXSession.

* AUTHORS
Julien Lavergne (gilir@ubuntu.com)

Man page written to conform with Debian by Julien Lavergne.
