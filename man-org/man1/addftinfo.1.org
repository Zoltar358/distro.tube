#+TITLE: Man1 - addftinfo.1
#+DESCRIPTION: Linux manpage for addftinfo.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
addftinfo - add information to troff font files for use with groff

* SYNOPSIS
*addftinfo* [ *-asc-height* n ] [ *-body-depth* n ] [ *-body-height* n ]
[ *-cap-height* n ] [ *-comma-depth* n ] [ *-desc-depth* n ] [
*-fig-height* n ] [ *-x-height* n ] /res/ /unitwidth/ /font/ *addftinfo*
*--help* *addftinfo* *-v* *addftinfo* *--version*

* DESCRIPTION
*addftinfo* reads a troff font file and adds some additional font-metric
information that is used by the groff system. The font file with the
information added is written on the standard output. The information
added is guessed using some parametric information about the font and
assumptions about the traditional troff names for characters. The main
information added is the heights and depths of characters. The /res/ and
/unitwidth/ arguments should be the same as the corresponding parameters
in the DESC file; /font/ is the name of the file describing the font; if
/font/ ends with *I* the font will be assumed to be italic.

* OPTIONS
*-v* prints the version number.

All other options change one of the parameters that are used to derive
the heights and depths. Like the existing quantities in the font file,
each /value/ is in inches//res/ for a font whose point size is
/unitwidth/. /param-option/ must be one of:

- *-x-height* :: The height of lowercase letters without ascenders such
  as x.

- *-fig-height* :: The height of figures (digits).

- *-asc-height* :: The height of characters with ascenders, such as b, d
  or l.

- *-body-height* :: The height of characters such as parentheses.

- *-cap-height* :: The height of uppercase letters such as A.

- *-comma-depth* :: The depth of a comma.

- *-desc-depth* :: The depth of characters with descenders, such as p,
  q, or y.

- *-body-depth* :: The depth of characters such as parentheses.

*addftinfo* makes no attempt to use the specified parameters to guess
the unspecified parameters. If a parameter is not specified the default
will be used. The defaults are chosen to have the reasonable values for
a Times font.

* SEE ALSO
*groff_font*(5), *groff*(1), *groff_char*(7)

You can view a man page /name/*(*/n/*)* with

#+begin_quote
  #+begin_example
    man n name
  #+end_example
#+end_quote

in text mode and with

#+begin_quote
  #+begin_example
    groffer n name
  #+end_example
#+end_quote

in graphical mode (PDF, etc.).
