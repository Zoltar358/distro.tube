#+TITLE: Man1 - cargo-package.1
#+DESCRIPTION: Linux manpage for cargo-package.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-package - Assemble the local package into a distributable tarball

* SYNOPSIS
*cargo package* [/options/]

* DESCRIPTION
This command will create a distributable, compressed *.crate* file with
the source code of the package in the current directory. The resulting
file will be stored in the *target/package* directory. This performs the
following steps:

#+begin_quote
  1.Load and check the current workspace, performing some basic checks.

  #+begin_quote
    ·Path dependencies are not allowed unless they have a version key.
    Cargo will ignore the path key for dependencies in published
    packages. *dev-dependencies* do not have this restriction.
  #+end_quote
#+end_quote

#+begin_quote
  2.Create the compressed *.crate* file.

  #+begin_quote
    ·The original *Cargo.toml* file is rewritten and normalized.
  #+end_quote

  #+begin_quote
    ·*[patch]*, *[replace]*, and *[workspace]* sections are removed from
    the manifest.
  #+end_quote

  #+begin_quote
    ·*Cargo.lock* is automatically included if the package contains an
    executable binary or example target. *cargo-install*(1) will use the
    packaged lock file if the *--locked* flag is used.
  #+end_quote

  #+begin_quote
    ·A *.cargo_vcs_info.json* file is included that contains information
    about the current VCS checkout hash if available (not included with
    *--allow-dirty*).
  #+end_quote
#+end_quote

#+begin_quote
  3.Extract the *.crate* file and build it to verify it can build.

  #+begin_quote
    ·This will rebuild your package from scratch to ensure that it can
    be built from a pristine state. The *--no-verify* flag can be used
    to skip this step.
  #+end_quote
#+end_quote

#+begin_quote
  4.Check that build scripts did not modify any source files.
#+end_quote

The list of files included can be controlled with the *include* and
*exclude* fields in the manifest.

See /the reference/
<https://doc.rust-lang.org/cargo/reference/publishing.html> for more
details about packaging and publishing.

** .cargo_vcs_info.json format
Will generate a *.cargo_vcs_info.json* in the following format

#+begin_quote
  #+begin_example
    {
     "git": {
       "sha1": "aac20b6e7e543e6dd4118b246c77225e3a3a1302"
     },
     "path_in_vcs": ""
    }
  #+end_example
#+end_quote

*path_in_vcs* will be set to a repo-relative path for packages in
subdirectories of the version control repository.

* OPTIONS
** Package Options
*-l*, *--list*

#+begin_quote
  Print files included in a package without making one.
#+end_quote

*--no-verify*

#+begin_quote
  Don't verify the contents by building them.
#+end_quote

*--no-metadata*

#+begin_quote
  Ignore warnings about a lack of human-usable metadata (such as the
  description or the license).
#+end_quote

*--allow-dirty*

#+begin_quote
  Allow working directories with uncommitted VCS changes to be packaged.
#+end_quote

** Package Selection
By default, when no package selection options are given, the packages
selected depend on the selected manifest file (based on the current
working directory if *--manifest-path* is not given). If the manifest is
the root of a workspace then the workspaces default members are
selected, otherwise only the package defined by the manifest will be
selected.

The default members of a workspace can be set explicitly with the
*workspace.default-members* key in the root manifest. If this is not
set, a virtual workspace will include all workspace members (equivalent
to passing *--workspace*), and a non-virtual workspace will include only
the root crate itself.

*-p* /spec/..., *--package* /spec/...

#+begin_quote
  Package only the specified packages. See *cargo-pkgid*(1) for the SPEC
  format. This flag may be specified multiple times and supports common
  Unix glob patterns like ***, *?* and *[]*. However, to avoid your
  shell accidentally expanding glob patterns before Cargo handles them,
  you must use single quotes or double quotes around each pattern.
#+end_quote

*--workspace*

#+begin_quote
  Package all members in the workspace.
#+end_quote

*--exclude* /SPEC/...

#+begin_quote
  Exclude the specified packages. Must be used in conjunction with the
  *--workspace* flag. This flag may be specified multiple times and
  supports common Unix glob patterns like ***, *?* and *[]*. However, to
  avoid your shell accidentally expanding glob patterns before Cargo
  handles them, you must use single quotes or double quotes around each
  pattern.
#+end_quote

** Compilation Options
*--target* /triple/

#+begin_quote
  Package for the given architecture. The default is the host
  architecture. The general format of the triple is
  *<arch><sub>-<vendor>-<sys>-<abi>*. Run *rustc --print target-list*
  for a list of supported targets.

  This may also be specified with the *build.target* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.

  Note that specifying this flag makes Cargo run in a different mode
  where the target artifacts are placed in a separate directory. See the
  /build cache/ <https://doc.rust-lang.org/cargo/guide/build-cache.html>
  documentation for more details.
#+end_quote

*--target-dir* /directory/

#+begin_quote
  Directory for all generated artifacts and intermediate files. May also
  be specified with the *CARGO_TARGET_DIR* environment variable, or the
  *build.target-dir* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  *target* in the root of the workspace.
#+end_quote

** Feature Selection
The feature flags allow you to control which features are enabled. When
no feature options are given, the *default* feature is activated for
every selected package.

See /the features documentation/
<https://doc.rust-lang.org/cargo/reference/features.html#command-line-feature-options>
for more details.

*--features* /features/

#+begin_quote
  Space or comma separated list of features to activate. Features of
  workspace members may be enabled with *package-name/feature-name*
  syntax. This flag may be specified multiple times, which enables all
  specified features.
#+end_quote

*--all-features*

#+begin_quote
  Activate all available features of all selected packages.
#+end_quote

*--no-default-features*

#+begin_quote
  Do not activate the *default* feature of the selected packages.
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Miscellaneous Options
*-j* /N/, *--jobs* /N/

#+begin_quote
  Number of parallel jobs to run. May also be specified with the
  *build.jobs* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  the number of CPUs.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Create a compressed *.crate* file of the current package:

  #+begin_quote
    #+begin_example
      cargo package
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-publish*(1)
