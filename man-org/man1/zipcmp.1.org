#+TITLE: Man1 - zipcmp.1
#+DESCRIPTION: Linux manpage for zipcmp.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
compares the zip archives or directories

and

and checks if they contain the same files, comparing their names,
uncompressed sizes, and CRCs. File order and compressed size differences
are ignored.

Supported options:

Check consistency of archives. Results in an error if archive is
inconsistent or not valid according to the zip specification.

Display a short help message and exit.

Compare names ignoring case distinctions.

Enable paranoid checks. Compares extra fields, comments, and other meta
data. (Automatically disabled if one of the archives is a directory.)
These checks are skipped for files where the data differs.

Quiet mode. Compare

Test zip files by comparing the contents to their checksums.

Display version information and exit.

Verbose mode. Print details about differences to stdout. (This is the
default.)

exits 0 if the two archives contain the same files, 1 if they differ,
and >1 if an error occurred.

was added in libzip 0.6.

and
