#+TITLE: Man1 - polybar.1
#+DESCRIPTION: Linux manpage for polybar.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
polybar - A fast and easy-to-use tool status bar

* SYNOPSIS
*polybar* [/OPTIONS/]... /BAR/

* DESCRIPTION
Polybar aims to help users build beautiful and highly customizable
status bars for their desktop environment, without the need of having a
black belt in shell scripting.

* OPTIONS

#+begin_quote
  - *-h, --help* :: Display help text and exit
#+end_quote

#+begin_quote
  - *-v, --version* :: Display build details and exit
#+end_quote

#+begin_quote
  - *-l, --log=LEVEL* :: 

  #+begin_example
    Set the logging verbosity (default: notice)
    LEVEL is one of: error, warning, notice, info, trace
  #+end_example
#+end_quote

#+begin_quote
  - *-q, --quiet* :: Be quiet (will override -l)
#+end_quote

#+begin_quote
  - *-c, --config=FILE* :: Specify the path to the configuration file.
    By default, the configuration file is loaded from:

  #+begin_example

    $XDG_CONFIG_HOME/polybar/config
    $HOME/.config/polybar/config
  #+end_example
#+end_quote

#+begin_quote
  - *-r, --reload* :: Reload the application when the config file has
    been modified
#+end_quote

#+begin_quote
  - *-d, --dump=PARAM* :: Print the value of the specified parameter
    /PARAM/ in bar section and exit
#+end_quote

#+begin_quote
  - *-m, --list-monitors* :: Print list of available monitors and exit

  If some monitors are cloned, this will exclude all but one of them
#+end_quote

#+begin_quote
  - *-M, --list-all-monitors* :: Print list of available monitors and
    exit

  This will also include all cloned monitors.
#+end_quote

#+begin_quote
  - *-w, --print-wmname* :: Print the generated /WM_NAME/ and exit
#+end_quote

#+begin_quote
  - *-s, --stdout* :: Output the data to stdout instead of drawing it to
    the X window
#+end_quote

#+begin_quote
  - *-p, --png=FILE* :: Save png snapshot to /FILE/ after running for 3
    seconds
#+end_quote

* AUTHOR
#+begin_example
  Michael Carlberg <c@rlberg.se>
  Contributors can be listed on GitHub.
#+end_example

* REPORTING BUGS
Report issues on GitHub </https://github.com/polybar/polybar/>

* SEE ALSO
#+begin_example
  Full documentation at: <https://github.com/polybar/polybar>
  Project wiki: <https://github.com/polybar/polybar/wiki>
#+end_example

*polybar(5)*

* COPYRIGHT
2016-2021, Michael Carlberg & contributors
