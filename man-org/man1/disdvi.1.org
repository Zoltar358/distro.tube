#+TITLE: Man1 - disdvi.1
#+DESCRIPTION: Linux manpage for disdvi.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
disdvi - `disassemble' a (p)TeX or XeTeX DVI file

* SYNOPSIS
*disdvi* [*-p*] [/dvi_file/[/*.dvi*/]]\\
*disdvi -x* [/xdv_file/[/*.xdv*/]]\\
*disdvi -h*

* DESCRIPTION
/disdvi/ interprets the contents of /dvi_file/ or /xdv_file/ printing
the DVI commands and text in a human-readable ASCII format on the
standard output.

* SEE ALSO
*dvi2tty*(1)

* AUTHOR
Marcel J.E. Mol, Delft University of Technology, The Netherlands\\
marcel@mesa.nl, MESA Consulting
