#+TITLE: Man1 - faac.1
#+DESCRIPTION: Linux manpage for faac.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
faac - open source MPEG-4 and MPEG-2 AAC encoder

* SYNOPSIS
*faac* [/options/] [-o /outfile/] /infiles/ ...

</infiles/> and/or </outfile/> can be "-", which means stdin/stdout.

* DESCRIPTION
*FAAC* is an open source MPEG-4 and MPEG-2 AAC encoder, it is licensed
under the LGPL license. Note that the quality of *FAAC* is not up to par
with the currently best AAC encoders available.

* FEATURES
- * Portable :: 

- * Fast :: 

- * LC, Main, LTP support :: 

- * DRM support through DreaM (optional) :: 

* HELP OPTIONS
- *-h* :: Short help on using FAAC

- *-H* :: Description of all options for FAAC

- *--license* :: License terms for FAAC.

- *--help-qual* :: Quality-related options

- *--help-io* :: Input/output options

- *--help-mp4* :: MP4 specific options Quality-related options

- *--help-advanced* :: Advanced options, only for testing purposes

* QUALITY-RELATED OPTIONS
- *-q </quality/>* :: Set encoding quality. Set default variable bitrate
  (VBR) quality level in percent. max. 5000, min. 10. default: 100,
  averages at approx. 120 kbps VBR for a normal stereo input file with
  16 bit and 44.1 kHz sample rate

- *-b </bitrate/>* :: Set average bitrate (ABR) to approximately
  <bitrate> kbps. max. ~500 (stereo)

- *-c </freq/>* :: Set the bandwidth in Hz. The actual frequency is
  adjusted to maximize upper spectral band usage.

* INPUT/OUTPUT OPTIONS
- *-o </filename/>* :: Set output file to X (only for one input file)
  only for one input file; you can use *.aac, *.mp4, *.m4a or *.m4b as
  file extension, and the file format will be set automatically to ADTS
  or MP4).

- *-* :: Use stdin/stdout. If you simply use a hyphen/minus sign instead
  of a filename, FAAC can encode directly from stdin, thus enabling
  piping from other applications and utilities. The same works for
  stdout as well, so FAAC can pipe its output to other apps such as a
  server.

- *-v </vebose/>* :: Verbosity level (-v0 is quiet mode)

- *-r* :: Use RAW AAC output file. Generate raw AAC bitstream (i.e.
  without any headers). Not advised!!!, RAW AAC files are practically
  useless!!!

- *-P* :: Raw PCM input mode (default 44100Hz 16bit stereo). Raw PCM
  input mode (default: off, i.e. expecting a WAV header; necessary for
  input files or bitstreams without a header; using only -P assumes the
  default values for -R, -B and -C in the input file).

- *-R </samplerate/>* :: Raw PCM input rate. Raw PCM input sample rate
  in Hz (default: 44100 Hz, max. 96 kHz)

- *-B </samplebits/>* :: Raw PCM input sample size (8, 16 (default), 24
  or 32bits). Raw PCM input sample size (default: 16, also possible 8,
  24, 32 bit fixed or float input).

- *-C </channels/>* :: Raw PCM input channels. Raw PCM input channels
  (default: 2, max. 33 + 1 LFE).

- *-X* :: Raw PCM swap input bytes Raw PCM swap input bytes (default:
  bigendian).

- *-I </C[,LFE]/>* :: Input channel config, default is 3,4 (Center
  third, LF fourth) Input multichannel configuration (default: 3,4 which
  means Center is third and LFE is fourth like in 5.1 WAV, so you only
  have to specify a different position of these two mono channels in
  your multichannel input files if they haven't been reordered already).

- *--ignorelength* :: Ignore wav length from header (useful with files
  over 4 GB)

- *--overwrite* :: Overwrite existing output file

* MP4 SPECIFIC OPTIONS
- *-w* :: Wrap AAC data in MP4 container. (default for *.mp4, *.m4a and
  *.m4b)

- *--tag </tagname,tagvalue/>* :: Add named tag (iTunes '----')

- *--artist </name/>* :: Set artist name

- *--composer </name/>* :: Set composer name

- *--title </name/>* :: Set title/track name

- *--genre </number/>* :: Set genre number

- *--album </name/>* :: Set album/performer

- *--compilation* :: Mark as compilation

- *--track </number/total/>* :: Set track number

- *--disc </number/total/>* :: Set disc number

- *--year </number/>* :: Set year

- *--cover-art </filename/>* :: Read cover art from file X Supported
  image formats are GIF, JPEG, and PNG.

- *--comment </string/>* :: Set comment

* ADVANCED OPTIONS, ONLY FOR TESTING PURPOSES
- *--tns* :: Enable coding of TNS, temporal noise shaping.

- *--no-tns* :: Disable coding of TNS, temporal noise shaping.

- *--no-midside* :: Don't use mid/side coding.

- *--mpeg-vers /X/* :: Force AAC MPEG version, X can be 2 or 4

- *--shortctl /X/* :: Enforce block type (0 = both (default); 1 = no
  short; 2 = no long).

* AUTHORS
*FAAC* was written by M. Bakker <menno@audiocoding.com>.

Developed and maintained by Krzysztof Nikiel
<knik@users.sourceforge.net>.

This manpage was written by Fabian Greffrath
<fabian@debian-unofficial.org> for the Debian Unofficial project (but
may be used by others, of course).
