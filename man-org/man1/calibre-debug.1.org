#+TITLE: Man1 - calibre-debug.1
#+DESCRIPTION: Linux manpage for calibre-debug.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
calibre-debug - calibre-debug

#+begin_quote

  #+begin_quote
    #+begin_example
      calibre-debug [options]
    #+end_example
  #+end_quote
#+end_quote

Various command line interfaces useful for debugging calibre. With no
options, this command starts an embedded Python interpreter. You can
also run the main calibre GUI, the calibre E-book viewer and the calibre
editor in debug mode.

It also contains interfaces to various bits of calibre that do not have
dedicated command line tools, such as font subsetting, the E-book diff
tool and so on.

You can also use *calibre-debug* to run standalone scripts. To do that
use it like this:

#+begin_quote

  #+begin_quote
    *calibre-debug* myscript.py *--* *--option1* *--option2* file1 file2
    ...
  #+end_quote
#+end_quote

Everything after the *--* is passed to the script.

Whenever you pass arguments to *calibre-debug* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--add-simple-plugin* :: Add a simple plugin (i.e. a plugin that
    consists of only a .py file), by specifying the path to the py file
    containing the plugin code.
#+end_quote

#+begin_quote
  - *--command, -c* :: Run Python code.
#+end_quote

#+begin_quote
  - *--debug-device-driver, -d* :: Debug device detection
#+end_quote

#+begin_quote
  - *--default-programs* :: (Un)register calibre from Windows Default
    Programs. /--default-programs/ = *(register|unregister)*
#+end_quote

#+begin_quote
  - *--diff* :: Run the calibre diff tool. For example: calibre-debug
    /--diff/ file1 file2
#+end_quote

#+begin_quote
  - *--edit-book, -t* :: Launch the calibre *"*Edit book*"* tool in
    debug mode.
#+end_quote

#+begin_quote
  - *--exec-file, -e* :: Run the Python code in file.
#+end_quote

#+begin_quote
  - *--explode-book, -x* :: Explode the book into the specified folder.
    Usage: -x file.epub output_dir Exports the book as a collection of
    HTML files and metadata, which you can edit using standard HTML
    editing tools. Works with EPUB, AZW3, HTMLZ and DOCX files.
#+end_quote

#+begin_quote
  - *--export-all-calibre-data* :: Export all calibre data
    (books/settings/plugins). Normally, you will be asked for the export
    folder and the libraries to export. You can also specify them as
    command line arguments to skip the questions. Use absolute paths for
    the export folder and libraries. The special keyword *"*all*"* can
    be used to export all libraries.
#+end_quote

#+begin_quote
  - *--fix-multiprocessing* :: For internal use
#+end_quote

#+begin_quote
  - *--gui, -g* :: Run the GUI with debugging enabled. Debug output is
    printed to stdout and stderr.
#+end_quote

#+begin_quote
  - *--gui-debug* :: Run the GUI with a debug console, logging to the
    specified path. For internal use only, use the -g option to run the
    GUI in debug mode
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--implode-book, -i* :: Implode a previously exploded book. Usage:
    -i output_dir file.epub Imports the book from the files in
    output_dir which must have been created by a previous call to
    /--explode-book/. Be sure to specify the same file type as was used
    when exploding.
#+end_quote

#+begin_quote
  - *--import-calibre-data* :: Import previously exported calibre data
#+end_quote

#+begin_quote
  - *--inspect-mobi, -m* :: Inspect the MOBI file(s) at the specified
    path(s)
#+end_quote

#+begin_quote
  - *--paths* :: Output the paths necessary to setup the calibre
    environment
#+end_quote

#+begin_quote
  - *--reinitialize-db* :: Re-initialize the sqlite calibre database at
    the specified path. Useful to recover from db corruption.
#+end_quote

#+begin_quote
  - *--run-plugin, -r* :: Run a plugin that provides a command line
    interface. For example: calibre-debug -r *"*Add Books*"* *--* file1
    *--option1* Everything after the *--* will be passed to the plugin
    as arguments.
#+end_quote

#+begin_quote
  - *--shutdown-running-calibre, -s* :: Cause a running calibre
    instance, if any, to be shutdown. Note that if there are running
    jobs, they will be silently aborted, so use with care.
#+end_quote

#+begin_quote
  - *--subset-font, -f* :: Subset the specified font. Use *--* after
    this option to pass option to the font subsetting program.
#+end_quote

#+begin_quote
  - *--test-build* :: Test binary modules in build
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

#+begin_quote
  - *--viewer, -w* :: Run the E-book viewer in debug mode
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
