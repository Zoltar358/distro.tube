#+TITLE: Man1 - t1rawafm.1
#+DESCRIPTION: Linux manpage for t1rawafm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
t1rawafm - produce raw AFM metrics from a PostScript Type 1 font

* SYNOPSIS
*t1rawafm* [OPTIONS...] [/font/ [/outputfile/]]

* DESCRIPTION
*T1rawafm* generates an AFM file with the information available in a
PostScript Type 1 font. The AFM file will lack kerns, ligature
information, and composite characters, but is otherwise usable. The AFM
file is written to the standard output (but see the *--output* option).
If no input font file is supplied, *t1rawafm* reads a PFA or PFB font
from the standard input.

* OPTIONS
- *--output*=/file/, *-o* /file/ :: Send output to /file/ instead of
  standard output.

- *-h*, *--help* :: Print usage information and exit.

- *--version* :: Print the version number and some short non-warranty
  information and exit.

* RETURN VALUES
*T1rawafm* exits with value 0 if an AFM metrics file was successfully
generated, and 1 otherwise.

* SEE ALSO
/Adobe Type 1 Font Format/, /Adobe Font Metrics File Format
Specification v4.1/

* AUTHOR
Eddie Kohler (ekohler@gmail.com)
