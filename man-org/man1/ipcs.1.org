#+TITLE: Man1 - ipcs.1
#+DESCRIPTION: Linux manpage for ipcs.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ipcs - show information on IPC facilities

* SYNOPSIS
*ipcs* [options]

* DESCRIPTION
*ipcs* shows information on System V inter-process communication
facilities. By default it shows information about all three resources:
shared memory segments, message queues, and semaphore arrays.

* OPTIONS
*-i*, *--id* /id/

#+begin_quote
  Show full details on just the one resource element identified by /id/.
  This option needs to be combined with one of the three resource
  options: *-m*, *-q* or *-s*.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

** Resource options
*-m*, *--shmems*

#+begin_quote
  Write information about active shared memory segments.
#+end_quote

*-q*, *--queues*

#+begin_quote
  Write information about active message queues.
#+end_quote

*-s*, *--semaphores*

#+begin_quote
  Write information about active semaphore sets.
#+end_quote

*-a*, *--all*

#+begin_quote
  Write information about all three resources (default).
#+end_quote

** Output formats
Of these options only one takes effect: the last one specified.

*-c*, *--creator*

#+begin_quote
  Show creator and owner.
#+end_quote

*-l*, *--limits*

#+begin_quote
  Show resource limits.
#+end_quote

*-p*, *--pid*

#+begin_quote
  Show PIDs of creator and last operator.
#+end_quote

*-t*, *--time*

#+begin_quote
  Write time information. The time of the last control operation that
  changed the access permissions for all facilities, the time of the
  last *msgsnd*(2) and *msgrcv*(2) operations on message queues, the
  time of the last *shmat*(2) and *shmdt*(2) operations on shared
  memory, and the time of the last *semop*(2) operation on semaphores.
#+end_quote

*-u*, *--summary*

#+begin_quote
  Show status summary.
#+end_quote

** Representation
These affect only the *-l* (*--limits*) option.

*-b*, *--bytes*

#+begin_quote
  Print sizes in bytes.
#+end_quote

*--human*

#+begin_quote
  Print sizes in human-readable format.
#+end_quote

* CONFORMING TO
The Linux *ipcs* utility is not fully compatible to the POSIX *ipcs*
utility. The Linux version does not support the POSIX *-a*, *-b* and
*-o* options, but does support the *-l* and *-u* options not defined by
POSIX. A portable application shall not use the *-a*, *-b*, *-o*, *-l*,
and *-u* options.

* NOTES
The current implementation of *ipcs* obtains information about available
IPC resources by parsing the files in //proc/sysvipc/. Before util-linux
version v2.23, an alternate mechanism was used: the *IPC_STAT* command
of *msgctl*(2), *semctl*(2), and *shmctl*(2). This mechanism is also
used in later util-linux versions in the case where //proc/ is
unavailable. A limitation of the *IPC_STAT* mechanism is that it can
only be used to retrieve information about IPC resources for which the
user has read permission.

* AUTHORS
* SEE ALSO
*ipcmk*(1), *ipcrm*(1), *msgrcv*(2), *msgsnd*(2), *semget*(2),
*semop*(2), *shmat*(2), *shmdt*(2), *shmget*(2), *sysvipc*(7)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *ipcs* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
