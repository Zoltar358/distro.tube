#+TITLE: Man1 - ffado-dice-firmware.1
#+DESCRIPTION: Linux manpage for ffado-dice-firmware.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ffado-dice-firmware - firmware download application for DICE-based
devices

* SYNOPSIS
*"ffado-dice-firmware*/[OPTION...]/

* DESCRIPTION
*ffado-dice-firmware* permits firmware updates to be made to DICE-based
devices under Linux.

* OPTIONS
- *-h, --help* :: Display brief usage information and exit

- *-p, --port=NUM* :: Specify use of IEEE1394 port /NUM/

- *-v, --verbose=LEVEL* :: Produce verbose output. The higher the
  /LEVEL/ the more verbose the messages. The some verbose messages may
  only be available of FFADO was compiled with debugging enabled.

- *-d, --delete=IMAGENAME* :: Delete image by name.

- *-e, --dump=FILENAME* :: Extract current DICE device flash contents
  into file /FILENAME/

- *-f, --flash=FILENAME* :: Write firmware file /FILENAME/ to DICE
  device.

- *-i, --image-info* :: Show detailed information about each image
  within the firmware on the device.

- *-m, --flash-info* :: Show information about the flash memory on the
  device.

- *-s, --dice-info* :: Show DICE image vendor and product information.

- *-t, --test OPERATION* :: Invoke a test/debug operation. Possible
  /OPERATIONs/ are 1 (POKE, write to address) and 2 (PEEK, read from
  address). Refer to the source code for further information.

- *-n, --node NODE* :: Specify the node to operate on.

* NOTES
Manipulating firmware can cause your device to stop working. The FFADO
project accepts no responsibility if this occurs as a result of using
this tool.
