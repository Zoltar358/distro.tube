#+TITLE: Man1 - kpackagetool5.1
#+DESCRIPTION: Linux manpage for kpackagetool5.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kpackagetool5 - Install, list, remove Plasma packages

* SYNOPSIS
*kpackagetool5*

[*-v, --version*] [*-h, --help*] [*--hash* / path/] [*-g, --global*]
[*-t, --type* / type/] [*-i, --install* / path/] [*-s, --show* / name/]
[*-u, --upgrade* / path/] [*-l, --list*] [*--list-types*] [*-r,
--remove* / name/] [*-p, --packageroot* / path/] [*--generate-index*]

* DESCRIPTION
*kpackagetool5* is a command line tool to install, list, remove Plasma
packages.

* OPTIONS
*-v, --version*

#+begin_quote
  Displays version information.
#+end_quote

*-h, --help*

#+begin_quote
  Displays this help.
#+end_quote

*--hash* / path/

#+begin_quote
  Generate a SHA1 hash for the package at “path”.
#+end_quote

*-g, --global*

#+begin_quote
  For install or remove, operates on packages installed for all users.
#+end_quote

*-t, --type* / type/

#+begin_quote
  The type of package, corresponding to the service type of the package
  plugin, e.g. KPackage/Generic, Plasma/Theme, Plasma/Wallpaper,
  Plasma/Applet, etc.
#+end_quote

*-i, --install* / path/

#+begin_quote
  Install the package at “path”.
#+end_quote

*-s, --show* / name/

#+begin_quote
  Show information of package “name”.
#+end_quote

*-u, --upgrade* / path/

#+begin_quote
  Upgrade the package at “path”.
#+end_quote

*-l, --list*

#+begin_quote
  List installed packages.
#+end_quote

*--list-types*

#+begin_quote
  Lists all known Package types that can be installed.
#+end_quote

*-r, --remove* / name/

#+begin_quote
  Remove the package named “name”.
#+end_quote

*-p, --packageroot* / path/

#+begin_quote
  Absolute path to the package root. If not supplied, then the standard
  data directories for this Plasma session will be searched instead.
#+end_quote

*--generate-index* / path/

#+begin_quote
  Recreate the plugin index. To be used in conjunction with either the
  option *-t* or *-g*. Recreates the index for the given type or package
  root. Operates in the user directory, unless *-g* is used.
#+end_quote

* SEE ALSO
More detailed user documentation is available from
*help:/plasma-desktop* (either enter this URL into Konqueror, or run
*khelpcenter */help:/plasma-desktop/).

* AUTHORS
*kpackagetool5* was written by Aaron Seigo <aseigo@kde.org>.
