#+TITLE: Man1 - llvm-dis.1
#+DESCRIPTION: Linux manpage for llvm-dis.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-dis - LLVM disassembler

* SYNOPSIS
*llvm-dis* [/options/] [/filename/]

* DESCRIPTION
The *llvm-dis* command is the LLVM disassembler. It takes an LLVM
bitcode file and converts it into human-readable LLVM assembly language.

If filename is omitted or specified as *-*, *llvm-dis* reads its input
from standard input.

If the input is being read from standard input, then *llvm-dis* will
send its output to standard output by default. Otherwise, the output
will be written to a file named after the input file, with a *.ll*
suffix added (any existing *.bc* suffix will first be removed). You can
override the choice of output file using the *-o* option.

* OPTIONS
*-f*

#+begin_quote

  #+begin_quote
    Enable binary output on terminals. Normally, *llvm-dis* will refuse
    to write raw bitcode output if the output stream is a terminal. With
    this option, *llvm-dis* will write raw bitcode regardless of the
    output device.
  #+end_quote
#+end_quote

*-help*

#+begin_quote

  #+begin_quote
    Print a summary of command line options.
  #+end_quote
#+end_quote

*-o* /filename/

#+begin_quote

  #+begin_quote
    Specify the output file name. If /filename/ is -, then the output is
    sent to standard output.
  #+end_quote
#+end_quote

* EXIT STATUS
If *llvm-dis* succeeds, it will exit with 0. Otherwise, if an error
occurs, it will exit with a non-zero value.

* SEE ALSO
*llvm-as(1)*

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
