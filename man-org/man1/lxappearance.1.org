#+TITLE: Man1 - lxappearance.1
#+DESCRIPTION: Linux manpage for lxappearance.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxappearance - GTK+ theme switcher

* SYNOPSIS
*lxappearance*

* DESCRIPTION
This manual page documents briefly the *lxappearance* command.

*lxappearance* is a program to change GTK+ themes, icon themes, and
fonts used by applications. It is a feature-rich GTK+ theme switcher.

* AUTHOR
This manual page was written by paulliu <grandpaul@gmail.com> for the
Debian system (but may be used by others). Permission is granted to
copy, distribute and/or modify this document under the terms of the GNU
General Public License, Version 2 any later version published by the
Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

The source code of this manual page should be in .sgml format. It is
better not to modify the .1 file directly.

* AUTHOR
*Ying-Chun Liu*

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2008 paulliu\\
