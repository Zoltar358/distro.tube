#+TITLE: Man1 - bat.1
#+DESCRIPTION: Linux manpage for bat.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
bat - a cat(1) clone with syntax highlighting and Git integration.

* USAGE
- bat [OPTIONS] [FILE]... :: 

- "bat :: 

* DESCRIPTION
bat prints the syntax-highlighted content of a collection of FILEs to
the terminal. If no FILE is specified, or when FILE is '-', it reads
from standard input.

bat supports a large number of programming and markup languages. It also
communicates with git(1) to show modifications with respect to the git
index. bat automatically pipes its output through a pager (by default:
less).

Whenever the output of bat goes to a non-interactive terminal, i.e. when
the output is piped into another process or into a file, bat will act as
a drop-in replacement for cat(1) and fall back to printing the plain
file contents.

* OPTIONS
General remarks: Command-line options like '-l'/'--language' that take
values can be specified as either '--language value',
'--language=value', '-l value' or '-lvalue'.

*-A*, *--show-all*

#+begin_quote
  Show non-printable characters like space, tab or newline. Use '--tabs'
  to control the width of the tab-placeholders.
#+end_quote

*-p*, *--plain*

#+begin_quote
  Only show plain style, no decorations. This is an alias for
  '--style=plain'. When '-p' is used twice ('-pp'), it also disables
  automatic paging (alias for '--style=plain *--pager*=/never/').
#+end_quote

*-l*, *--language* <language>

#+begin_quote
  Explicitly set the language for syntax highlighting. The language can
  be specified as a name (like 'C++' or 'LaTeX') or possible file
  extension (like 'cpp', 'hpp' or 'md'). Use '--list-languages' to show
  all supported language names and file extensions.
#+end_quote

*-H*, *--highlight-line* <N:M>...

#+begin_quote
  Highlight the specified line ranges with a different background color.
  For example:
#+end_quote

#+begin_quote
  - --highlight-line 40 :: highlights line 40

  - --highlight-line 30:40 :: highlights lines 30 to 40

  - --highlight-line :40 :: highlights lines 1 to 40

  - --highlight-line 40: :: highlights lines 40 to the end of the file
#+end_quote

*--file-name* <name>...

#+begin_quote
  Specify the name to display for a file. Useful when piping data to bat
  from STDIN when bat does not otherwise know the filename. Note that
  the provided file name is also used for syntax detection.
#+end_quote

*-d*, *--diff*

#+begin_quote
  Only show lines that have been added/removed/modified with respect to
  the Git index. Use '--diff-context=N' to control how much context you
  want to see.
#+end_quote

*--diff-context* <N>...

#+begin_quote
  Include N lines of context around added/removed/modified lines when
  using '--diff'.
#+end_quote

*--tabs* <T>

#+begin_quote
  Set the tab width to T spaces. Use a width of 0 to pass tabs through
  directly
#+end_quote

*--wrap* <mode>

#+begin_quote
  Specify the text-wrapping mode (*auto*, never, character). The
  '--terminal-width' option can be used in addition to control the
  output width.
#+end_quote

*--terminal-width* <width>

#+begin_quote
  Explicitly set the width of the terminal instead of determining it
  automatically. If prefixed with '+' or '-', the value will be treated
  as an offset to the actual terminal width. See also: '--wrap'.
#+end_quote

*-n*, *--number*

#+begin_quote
  Only show line numbers, no other decorations. This is an alias for
  '--style=numbers'
#+end_quote

*--color* <when>

#+begin_quote
  Specify when to use colored output. The automatic mode only enables
  colors if an interactive terminal is detected. Possible values:
  *auto*, never, always.
#+end_quote

*--italic-text* <when>

#+begin_quote
  Specify when to use ANSI sequences for italic text in the output.
  Possible values: always, *never*.
#+end_quote

*--decorations* <when>

#+begin_quote
  Specify when to use the decorations that have been specified via
  '--style'. The automatic mode only enables decorations if an
  interactive terminal is detected. Possible values: *auto*, never,
  always.
#+end_quote

*-f*, *--force-colorization*

#+begin_quote
  Alias for '--decorations=always --color=always'. This is useful if the
  output of bat is piped to another program, but you want to keep the
  colorization/decorations.
#+end_quote

*--paging* <when>

#+begin_quote
  Specify when to use the pager. To disable the pager, use
  '--paging=never' or its alias, *-P*. To disable the pager permanently,
  set BAT_PAGER to an empty string. To control which pager is used, see
  the '--pager' option. Possible values: *auto*, never, always.
#+end_quote

*--pager* <command>

#+begin_quote
  Determine which pager is used. This option will override the PAGER and
  BAT_PAGER environment variables. The default pager is 'less'. To
  control when the pager is used, see the '--paging' option. Example:
  '--pager "less *-RF*"'.
#+end_quote

*-m*, *--map-syntax* <glob-pattern:syntax-name>...

#+begin_quote
  Map a glob pattern to an existing syntax name. The glob pattern is
  matched on the full path and the filename. For example, to highlight
  *.build files with the Python syntax, use -m '*.build:Python'. To
  highlight files named '.myignore' with the Git Ignore syntax, use -m
  '.myignore:Git Ignore'. Note that the right-hand side is the *name* of
  the syntax, not a file extension.
#+end_quote

*--theme* <theme>

#+begin_quote
  Set the theme for syntax highlighting. Use '--list-themes' to see all
  available themes. To set a default theme, add the '--theme="..."'
  option to the configuration file or export the BAT_THEME environment
  variable (e.g.: export BAT_THEME="...").
#+end_quote

*--list-themes*

#+begin_quote
  Display a list of supported themes for syntax highlighting.
#+end_quote

*--style* <style-components>

#+begin_quote
  Configure which elements (line numbers, file headers, grid borders,
  Git modifications, ..) to display in addition to the file contents.
  The argument is a comma-separated list of components to display (e.g.
  'numbers,changes,grid') or a pre-defined style ('full'). To set a
  default style, add the '--style=".."' option to the configuration file
  or export the BAT_STYLE environment variable (e.g.: export
  BAT_STYLE=".."). Possible values: *auto*, full, plain, changes,
  header, grid, rule, numbers, snip.
#+end_quote

*-r*, *--line-range* <N:M>...

#+begin_quote
  Only print the specified range of lines for each file. For example:
#+end_quote

#+begin_quote
  - --line-range 30:40 :: prints lines 30 to 40

  - --line-range :40 :: prints lines 1 to 40

  - --line-range 40: :: prints lines 40 to the end of the file
#+end_quote

*-L*, *--list-languages*

#+begin_quote
  Display a list of supported languages for syntax highlighting.
#+end_quote

*-u*, *--unbuffered*

#+begin_quote
  This option exists for POSIX-compliance reasons ('u' is for
  'unbuffered'). The output is always unbuffered - this option is simply
  ignored.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print this help message.
#+end_quote

*-V*, *--version*

#+begin_quote
  Show version information.
#+end_quote

* POSITIONAL ARGUMENTS
*<FILE>...*

#+begin_quote
  Files to print and concatenate. Use a dash ('-') or no argument at all
  to read from standard input.
#+end_quote

* SUBCOMMANDS
*cache* - Modify the syntax-definition and theme cache.

* FILES
bat can also be customized with a configuration file. The location of
the file is dependent on your operating system. To get the default path
for your system, call:

*bat --config-file*

Alternatively, you can use the BAT_CONFIG_PATH environment variable to
point bat to a non-default location of the configuration file.

To generate a default configuration file, call:

*bat --generate-config-file*

* ADDING CUSTOM LANGUAGES
bat supports Sublime Text *.sublime-syntax* language files, and can be
customized to add additional languages to your local installation. To do
this, add the *.sublime-syntax* language files to `*$(bat
--config-dir)/syntaxes*` and run `*bat cache --build*`.

*Example:*

#+begin_quote
  mkdir -p "$(bat --config-dir)/syntaxes"\\
  cd "$(bat --config-dir)/syntaxes"

  # Put new '.sublime-syntax' language definition files\\
  # in this folder (or its subdirectories), for example:\\
  git clone https://github.com/tellnobody1/sublime-purescript-syntax

  # And then build the cache.\\
  bat cache --build
#+end_quote

Once the cache is built, the new language will be visible in `*bat
--list-languages*`.\\
If you ever want to remove the custom languages, you can clear the cache
with `*bat cache --clear*`.

* ADDING CUSTOM THEMES
Similarly to custom languages, bat supports Sublime Text *.tmTheme*
themes. These can be installed to `*$(bat --config-dir)/themes*`, and
are added to the cache with `*bat cache --build`.*

* MORE INFORMATION
For more information and up-to-date documentation, visit the bat repo:\\
*https://github.com/sharkdp/bat*
