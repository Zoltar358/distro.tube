#+TITLE: Man1 - gtk4-update-icon-cache.1
#+DESCRIPTION: Linux manpage for gtk4-update-icon-cache.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gtk4-update-icon-cache - Icon theme caching utility

* SYNOPSIS
*gtk4-update-icon-cache* [--force] [--ignore-theme-index] [--index-only
| --include-image-data] [--source /NAME/] [--quiet] [--validate] /PATH/

* DESCRIPTION
*gtk4-update-icon-cache* creates mmapable cache files for icon themes.

It expects to be given the /PATH/ to an icon theme directory containing
an index.theme, e.g. /usr/share/icons/hicolor, and writes a
icon-theme.cache containing cached information about the icons in the
directory tree below the given directory.

GTK can use the cache files created by *gtk4-update-icon-cache* to avoid
a lot of system call and disk seek overhead when the application starts.
Since the format of the cache files allows them to be shared across
multiple processes, for instance using the POSIX *mmap()* system call,
the overall memory consumption is reduced as well.

* OPTIONS
--force, -f

#+begin_quote
  Overwrite an existing cache file even if it appears to be up-to-date.
#+end_quote

--ignore-theme-index, -t

#+begin_quote
  Dont check for the existence of index.theme in the icon theme
  directory. Without this option, *gtk4-update-icon-cache* refuses to
  create an icon cache in a directory which does not appear to be the
  toplevel directory of an icon theme.
#+end_quote

--index-only, -i

#+begin_quote
  Dont include image data in the cache.
#+end_quote

--include-image-data

#+begin_quote
  Include image data in the cache.
#+end_quote

--source, -c

#+begin_quote
  Output a C header file declaring a constant /NAME/ with the contents
  of the icon cache.
#+end_quote

--quiet, -q

#+begin_quote
  Turn off verbose output.
#+end_quote

--validate, -v

#+begin_quote
  Validate existing icon cache.
#+end_quote
