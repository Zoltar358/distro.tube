#+TITLE: Man1 - pfbtopfa.1
#+DESCRIPTION: Linux manpage for pfbtopfa.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pfbtopfa - Convert Postscript .pfb fonts to .pfa format using
ghostscript

* SYNOPSIS
*pfbtopfa* /input.pfb/ /[output.pfa]/

* DESCRIPTION
This script invokes *gs*(1) to convert a .pfb file into a .pfa file.

* SEE ALSO
gs(1)

* VERSION
This document was last revised for Ghostscript version 9.55.0.

* AUTHOR
Artifex Software, Inc. are the primary maintainers of Ghostscript. This
manpage by George Ferguson.
