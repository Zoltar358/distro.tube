#+TITLE: Man1 - lxsession-logout.1
#+DESCRIPTION: Linux manpage for lxsession-logout.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxsession-logout - program to logout from LXSession

* SYNOPSIS
*lxsession-logout* [*options*...]

* DESCRIPTION
This manual page documents briefly the *lxsession-logout* command.

*lxsession-logout* is a program that to logout from LXSession. It gives
a good-looking logout dialog.

* OPTIONS
These programs follow the usual GNU command line syntax, with long
options starting with two dashes (`-). A summary of options is included
below.

*--prompt */Your custom message/

#+begin_quote
  Customize the logout message.
#+end_quote

*--banner */Your logo/

#+begin_quote
  Customize the logout logo.
#+end_quote

*--side left|top|right|bottom*

#+begin_quote
  The position of the logo.
#+end_quote

* SEE ALSO
lxsession (1).

* AUTHOR
This manual page was written by paulliu <grandpaul@gmail.com> for the
Debian system (but may be used by others). Permission is granted to
copy, distribute and/or modify this document under the terms of the GNU
General Public License, Version 2 any later version published by the
Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

* AUTHOR
*Ying-Chun Liu*

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2008 paulliu\\
