#+TITLE: Man1 - podofopages.1
#+DESCRIPTION: Linux manpage for podofopages.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
podofopages - move and delete pages in a PDF document

* SYNOPSIS
*podofopages* [--delete] [--move] [inputfile] [outputfile]

* DESCRIPTION
*podofopages* is one of the command line tools from the PoDoFo library
that provide several useful operations to work with PDF files. It can
move and delete pages in a PDF document.

* OPTIONS
*--delete NUMBER*

#+begin_quote
  Deletes the page NUMBER (number is 0-based). The page will not really
  be deleted from the PDF. It is only removed from the so called
  pagestree and therefore becomes hidden. The content of the page can
  still be retrieved from the document though.
#+end_quote

*--move FROM TO*

#+begin_quote
  Moves a page FROM TO in the document (FROM and TO are 0-based).
#+end_quote

* SEE ALSO
*podofobox*(1), *podofocolor*(1), *podofocountpages*(1),
*podofocrop*(1), *podofoencrypt*(1), *podofogc*(1), *podofoimg2pdf*(1),
*podofoimgextract*(1), *podofoimpose*(1), *podofoincrementalupdates*(1),
*podofomerge*(1), *podofopdfinfo*(1), *podofotxt2pdf*(1),
*podofotxtextract*(1), *podofouncompress*(1), *podofoxmp*(1)

* AUTHORS
PoDoFo is written by Dominik Seichter <domseichter@web.de> and others.

This manual page was written by Oleksandr Moskalenko <malex@debian.org>
for the Debian Project (but may be used by others).
