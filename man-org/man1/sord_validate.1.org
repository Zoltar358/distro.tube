#+TITLE: Man1 - sord_validate.1
#+DESCRIPTION: Linux manpage for sord_validate.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*sord_validate - Validate RDF data*

* SYNOPSIS
sord_validate [OPTION]... INPUT...

* OPTIONS
- *-h* :: Print the command line options.

- *-l* :: Print errors on a single line.

- *-v* :: Display version information and exit.

* DESCRIPTION
This is a simple validator which checks that all used properties are
actually defined, and that the domain and range of properties is
explicitly correct. Note that an "error" from this program does not
necessarily mean data is invalid, since it is not required to explicitly
list types in RDF, however it is a good idea to do so.

This program never retrieves data from the web or magical places on the
file system, it only processes files passed directly on the command
line. This means you must pass all used vocabularies to get a useful
result.

If an appropriate schema is available, literals are checked against
datatype definitions (both the explicit datatype of the literal itself
as well as any types implied by the corresponding property). Three XML
Schema Datatypes (XSD) constraints are currently supported: regular
expressions (xsd:pattern), and inclusive range (xsd:minimumInclusive and
xsd:maximumInclusive). Given an appropriate schema, this is enough to
validate against most of the standard XSD datatypes.

* EXAMPLES
sord_validate `find ~/schemas/ -name '*.ttl'` data.ttl

* AUTHOR
sord_validate was written by David Robillard <d@drobilla.net>

* COPYRIGHT
Copyright © 2012-2016 David Robillard.\\
License: <http://www.opensource.org/licenses/isc-license>\\
This is free software; you are free to change and redistribute it.\\
There is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
<http://drobilla.net/software/sord>
