#+TITLE: Man1 - cluster.1
#+DESCRIPTION: Linux manpage for cluster.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cluster - find clusters in a graph and augment the graph with this
information.

* SYNOPSIS
*cluster* [*-v?*] [ *-C*/k/ ] [ *-c*/k/ ] [ *-o* /outfile/ ] [ /files/ ]

* DESCRIPTION
*cluster* takes as input a graph in DOT format, finds node clusters and
augments the graph with this information. The clusters are specified by
the "cluster" attribute attached to nodes; cluster values are
non-negative integers. *cluster* attempts to maximize the modularity of
the clustering. If the edge attribute "weight" is defined, this will be
used in computing the clustering.

* OPTIONS
The following options are supported:

- *-C*/k/ :: specifies a targeted number of clusters that should be
  generated. The specified number /k/ is only a suggestion and may not
  be realisable. If /k == 0/, the default, the number of clusters that
  approximately optimizes the modularity is returned.

- *-c*/k/ :: specifies clustering method. If /k == 0/, the default,
  modularity clustering will be used. If /k == 1/ modularity quality
  will be used.

- *-o*/outfile/ :: Specifies that output should go into the file
  /outfile/. By default, /stdout/ is used.

- *-v* :: Verbose mode.

- *-?* :: Prints the usage and exits.

* EXAMPLES
Applying *cluster* to the following graph,

#+begin_example
     graph {
         1--2 [weight=10.]
         2--3 [weight=1]
         3--4 [weight=10.]
         4--5 [weight=10]
         5--6 [weight=10]
         3--6 [weight=0.1]
         4--6 [weight=10.]
        }
#+end_example

gives

#+begin_example
     graph {
           node [cluster="-1"];
           1 [cluster=1];
           2 [cluster=1];
           3 [cluster=2];
           4 [cluster=2];
           5 [cluster=2];
           6 [cluster=2];
           1 -- 2 [weight="10."];
           2 -- 3 [weight=1];
           3 -- 4 [weight="10."];
           4 -- 5 [weight=10];
           5 -- 6 [weight=10];
           3 -- 6 [weight="0.1"];
           4 -- 6 [weight="10."];
     }
#+end_example

* AUTHOR
Yifan Hu <yifanhu@yahoo.com>

* SEE ALSO
gvmap(1)

Blondel, V.D., Guillaume, J.L., Lambiotte, R., Lefebvre, E.: Fast
unfolding of communities in large networks. Journal of Statistical
Mechanics: Theory and Experiment (2008), P10008.
