#+TITLE: Man1 - javaws-jre8.1
#+DESCRIPTION: Linux manpage for javaws-jre8.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
javaws - Starts Java Web Start.

* SYNOPSIS
#+begin_example

  javaws [ run-options ] jnlp
#+end_example

#+begin_example

  javaws [ control-options ]
#+end_example

- /run-options/ :: Command-line run-options. The run-options can be in
  any order. See Run-Options.

- /jnlp/ :: Either the path of or the Uniform Resource Locator (URL) of
  the Java Network Launching Protocol (JNLP) file.

- /control-options/ :: Command-line control-options. The control-options
  can be in any order. See Control-Options.

* DESCRIPTION
/Note:/ The javaws command is not available on Oracle Solaris.

The javaws command starts Java Web Start, which is the reference
implementation of the JNLP. Java Web Start starts Java applications and
applets hosted on a network.

If a JNLP file is specified, then the javaws command starts the Java
application or applet specified in the JNLP file.

The javaws launcher has a set of options that are supported in the
current release. However, the options may be removed in a future
release.

* RUN-OPTIONS
- -offline :: \\
  Runs Java Web Start in offline mode.

- -Xnosplash :: \\
  Does not display the initial splash screen.

- -open /arguments/ :: \\
  When specified, this option replaces the arguments in the JNLP file
  with -openarguments.

- -print /arguments/ :: \\
  When specified, this option replaces the arguments in the JNLP file
  with -printarguments.

- -online :: \\
  Uses online mode. This is the default behavior.

- -wait :: \\
  The javaws process does not exit until the application exits. This
  option does not function as described on Windows platforms.

- -verbose :: \\
  Displays additional output.

- -J/option/ :: \\
  Passes option to the Java Virtual Machine, where option is one of the
  options described on the reference page for the Java application
  launcher. For example, -J-Xms48m sets the startup memory to 48 MB. See
  java(1).

- -system :: \\
  Runs the application from the system cache only.

* CONTROL-OPTIONS
- -viewer :: \\
  Shows the cache viewer in the Java Control Panel.

- -clearcache :: \\
  Removes all non-installed applications from the cache.

- -userConfig /property-name/ :: \\
  Clears the specified deployment property.

- -userConfig /property-name property-value/ :: \\
  Sets the specified deployment property to the specified value.

- -uninstall :: \\
  Removes all applications from the cache.

- -uninstall /jnlp/ :: \\
  Removes the application from the cache.

- -print /import-options/jnlp :: \\
  Imports the application to the cache.

* IMPORT-OPTIONS
- -silent :: \\
  Imports silently (with no user interface).

- -system :: \\
  Imports application to the system cache.

- -codebase /url/ :: \\
  Retrieves resources from the specified codebase.

- -shortcut :: \\
  Installs shortcuts if the user allows a prompt. This option has no
  effect unless the -silent option is also used.

- -association :: \\
  Installs associations if the user allows a prompt. This option has no
  effect unless the -silent option is also used.

* FILES
For information about the user and system cache and
deployment.properties files, see Deployment Configuration File and
Properties at
http://docs.oracle.com/javase/8/docs/technotes/guides/deployment/deployment-guide/properties.html

* SEE ALSO
- · :: Java Web Start at
  http://docs.oracle.com/javase/8/docs/technotes/guides/javaws/index.html

\\
