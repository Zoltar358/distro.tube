#+TITLE: Man1 - J
#+DESCRIPTION: Man1 - J
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* J
#+begin_src bash :exports results
readarray -t starts_with_j < <(find . -type f -iname "j*" | sort)

for x in "${starts_with_j[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./jack_bufsize.1.org][jack_bufsize.1]]         |
| [[file:./jack_connect.1.org][jack_connect.1]]         |
| [[file:./jackd.1.org][jackd.1]]                |
| [[file:./jack_disconnect.1.org][jack_disconnect.1]]      |
| [[file:./jack_freewheel.1.org][jack_freewheel.1]]       |
| [[file:./jack_impulse_grabber.1.org][jack_impulse_grabber.1]] |
| [[file:./jack_iodelay.1.org][jack_iodelay.1]]         |
| [[file:./jack_load.1.org][jack_load.1]]            |
| [[file:./jack_lsp.1.org][jack_lsp.1]]             |
| [[file:./jack_metro.1.org][jack_metro.1]]           |
| [[file:./jack_monitor_client.1.org][jack_monitor_client.1]]  |
| [[file:./jack_netsource.1.org][jack_netsource.1]]       |
| [[file:./jack_property.1.org][jack_property.1]]        |
| [[file:./jackrec.1.org][jackrec.1]]              |
| [[file:./jack_samplerate.1.org][jack_samplerate.1]]      |
| [[file:./jack_showtime.1.org][jack_showtime.1]]        |
| [[file:./jack_simple_client.1.org][jack_simple_client.1]]   |
| [[file:./jack_transport.1.org][jack_transport.1]]       |
| [[file:./jack_unload.1.org][jack_unload.1]]          |
| [[file:./jack_wait.1.org][jack_wait.1]]            |
| [[file:./jar-openjdk17.1.org][jar-openjdk17.1]]        |
| [[file:./jarsigner-openjdk17.1.org][jarsigner-openjdk17.1]]  |
| [[file:./jarsigner-zulu-8.1.org][jarsigner-zulu-8.1]]     |
| [[file:./jar-zulu-8.1.org][jar-zulu-8.1]]           |
| [[file:./jasper.1.org][jasper.1]]               |
| [[file:./javac-openjdk17.1.org][javac-openjdk17.1]]      |
| [[file:./javac-zulu-8.1.org][javac-zulu-8.1]]         |
| [[file:./javadoc-openjdk17.1.org][javadoc-openjdk17.1]]    |
| [[file:./javadoc-zulu-8.1.org][javadoc-zulu-8.1]]       |
| [[file:./javah-zulu-8.1.org][javah-zulu-8.1]]         |
| [[file:./java-jre8.1.org][java-jre8.1]]            |
| [[file:./java-openjdk17.1.org][java-openjdk17.1]]       |
| [[file:./javap-openjdk17.1.org][javap-openjdk17.1]]      |
| [[file:./javap-zulu-8.1.org][javap-zulu-8.1]]         |
| [[file:./javaws-jre8.1.org][javaws-jre8.1]]          |
| [[file:./java-zulu-8.1.org][java-zulu-8.1]]          |
| [[file:./jbig2dec.1.org][jbig2dec.1]]             |
| [[file:./jbigtopnm.1.org][jbigtopnm.1]]            |
| [[file:./jcmd-openjdk17.1.org][jcmd-openjdk17.1]]       |
| [[file:./jcmd-zulu-8.1.org][jcmd-zulu-8.1]]          |
| [[file:./jconsole-openjdk17.1.org][jconsole-openjdk17.1]]   |
| [[file:./jconsole-zulu-8.1.org][jconsole-zulu-8.1]]      |
| [[file:./jdb-openjdk17.1.org][jdb-openjdk17.1]]        |
| [[file:./jdb-zulu-8.1.org][jdb-zulu-8.1]]           |
| [[file:./jdeprscan-openjdk17.1.org][jdeprscan-openjdk17.1]]  |
| [[file:./jdeps-openjdk17.1.org][jdeps-openjdk17.1]]      |
| [[file:./jdeps-zulu-8.1.org][jdeps-zulu-8.1]]         |
| [[file:./jfr-openjdk17.1.org][jfr-openjdk17.1]]        |
| [[file:./jhat-zulu-8.1.org][jhat-zulu-8.1]]          |
| [[file:./jhsdb-openjdk17.1.org][jhsdb-openjdk17.1]]      |
| [[file:./jinfo-openjdk17.1.org][jinfo-openjdk17.1]]      |
| [[file:./jinfo-zulu-8.1.org][jinfo-zulu-8.1]]         |
| [[file:./jiv.1.org][jiv.1]]                  |
| [[file:./jjs-jre8.1.org][jjs-jre8.1]]             |
| [[file:./jjs-zulu-8.1.org][jjs-zulu-8.1]]           |
| [[file:./jlink-openjdk17.1.org][jlink-openjdk17.1]]      |
| [[file:./jmap-openjdk17.1.org][jmap-openjdk17.1]]       |
| [[file:./jmap-zulu-8.1.org][jmap-zulu-8.1]]          |
| [[file:./jmod-openjdk17.1.org][jmod-openjdk17.1]]       |
| [[file:./join.1.org][join.1]]                 |
| [[file:./journalctl.1.org][journalctl.1]]           |
| [[file:./jpackage-openjdk17.1.org][jpackage-openjdk17.1]]   |
| [[file:./jpeg2ktopam.1.org][jpeg2ktopam.1]]          |
| [[file:./jpeg2yuv.1.org][jpeg2yuv.1]]             |
| [[file:./jpegtopnm.1.org][jpegtopnm.1]]            |
| [[file:./jpegtran.1.org][jpegtran.1]]             |
| [[file:./jpgicc.1.org][jpgicc.1]]               |
| [[file:./jps-openjdk17.1.org][jps-openjdk17.1]]        |
| [[file:./jps-zulu-8.1.org][jps-zulu-8.1]]           |
| [[file:./jq.1.org][jq.1]]                   |
| [[file:./jrunscript-openjdk17.1.org][jrunscript-openjdk17.1]] |
| [[file:./jrunscript-zulu-8.1.org][jrunscript-zulu-8.1]]    |
| [[file:./jsadebugd-zulu-8.1.org][jsadebugd-zulu-8.1]]     |
| [[file:./jshell-openjdk17.1.org][jshell-openjdk17.1]]     |
| [[file:./json-glib-format.1.org][json-glib-format.1]]     |
| [[file:./json-glib-validate.1.org][json-glib-validate.1]]   |
| [[file:./json_pp.1perl.org][json_pp.1perl]]          |
| [[file:./jstack-openjdk17.1.org][jstack-openjdk17.1]]     |
| [[file:./jstack-zulu-8.1.org][jstack-zulu-8.1]]        |
| [[file:./jstatd-openjdk17.1.org][jstatd-openjdk17.1]]     |
| [[file:./jstatd-zulu-8.1.org][jstatd-zulu-8.1]]        |
| [[file:./jstat-openjdk17.1.org][jstat-openjdk17.1]]      |
| [[file:./jstat-zulu-8.1.org][jstat-zulu-8.1]]         |
