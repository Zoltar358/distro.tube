#+TITLE: Man1 - bioradtopgm.1
#+DESCRIPTION: Linux manpage for bioradtopgm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
bioradtopgm - convert a Biorad confocal file into a PGM image

* SYNOPSIS
*bioradtopgm* [*-image#*] [/imagedata/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*bioradtopgm* reads a Biorad confocal file as input and produces a PGM
image as output. If the resulting image is upside down, run it through
*pamflip -tb*.

* OPTIONS
- *-image#* :: A Biorad image file may contain more than one image. With
  this option, you can specify which image to extract (only one at a
  time). The first image in the file has number zero. If no image number
  is supplied, only information about the image size and the number of
  images in the input is printed out. No output is produced.

* SEE ALSO
*pamflip*(1) , *pgm*(5)

* AUTHORS
Copyright (C) 1993 by Oliver Trepte
