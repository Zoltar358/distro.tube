#+TITLE: Man1 - g-ir-generate.1
#+DESCRIPTION: Linux manpage for g-ir-generate.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
g-ir-generate - Typelib generator

* SYNOPSIS
*g-ir-generate* [OPTION...] FILES...

* DESCRIPTION
g-ir-generate is an GIR generator, using the repository API. It
generates GIR files from a raw typelib or in a shared library
(*--shlib*). The output will be written to standard output unless the
*--output* is specified.

* OPTIONS

#+begin_quote
  - *--help* :: Show help options

  - *--shlib*=**/FILENAME/ :: The shared library to read the symbols
    from.

  - *--output*=**/FILENAME/ :: Save the resulting output in FILENAME.

  - *--version* :: Show program's version number and exit
#+end_quote

* BUGS
Report bugs at
/https://gitlab.gnome.org/GNOME/gobject-introspection/issues/

* HOMEPAGE AND CONTACT
/https://gi.readthedocs.io//

* AUTHORS
Mattias Clasen
