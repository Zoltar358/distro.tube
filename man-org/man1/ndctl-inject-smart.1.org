#+TITLE: Man1 - ndctl-inject-smart.1
#+DESCRIPTION: Linux manpage for ndctl-inject-smart.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ndctl-inject-smart - perform smart threshold/injection operations on a
DIMM

* SYNOPSIS
#+begin_example
  ndctl inject-smart <dimm> [<options>]
#+end_example

\\

* DESCRIPTION
A generic DIMM device object, named /dev/nmemX, is registered for each
memory device indicated in the ACPI NFIT table, or other platform NVDIMM
resource discovery mechanism.

ndctl-inject-smart can be used to set smart thresholds, and inject smart
attributes.

* EXAMPLES
Set smart controller temperature and spares threshold for DIMM-0 to 32C,
spares threshold to 8, and enable the spares alarm.

#+begin_example
  ndctl inject-smart --ctrl-temperature-threshold=32 --spares-threshold=8 --spares-alarm nmem0
#+end_example

\\

Inject a media temperature value of 52 and fatal health status flag for
DIMM-0

#+begin_example
  ndctl inject-smart --media-temperature=52 --health=fatal nmem0
#+end_example

\\

* OPTIONS
-b, --bus=

#+begin_quote
  A bus id number, or a provider string (e.g. "ACPI.NFIT"). Restrict the
  operation to the specified bus(es). The keyword /all/ can be specified
  to indicate the lack of any restriction, however this is the same as
  not supplying a --bus option at all.
#+end_quote

-m, --media-temperature=

#+begin_quote
  Inject <value> for the media temperature smart attribute.
#+end_quote

-M, --media-temperature-threshold=

#+begin_quote
  Set <value> for the smart media temperature threshold.
#+end_quote

--media-temperature-alarm=

#+begin_quote
  Enable or disable the smart media temperature alarm. Options are /on/
  or /off/.
#+end_quote

--media-temperature-uninject

#+begin_quote
  Uninject any media temperature previously injected.
#+end_quote

-c, --ctrl-temperature=

#+begin_quote
  Inject <value> for the controller temperature smart attribute.
#+end_quote

-C, --ctrl-temperature-threshold=

#+begin_quote
  Set <value> for the smart controller temperature threshold.
#+end_quote

--ctrl-temperature-alarm=

#+begin_quote
  Enable or disable the smart controller temperature alarm. Options are
  /on/ or /off/.
#+end_quote

--ctrl-temperature-uninject

#+begin_quote
  Uninject any controller temperature previously injected.
#+end_quote

-s, --spares=

#+begin_quote
  Inject <value> for the spares smart attribute.
#+end_quote

-S, --spares-threshold=

#+begin_quote
  Set <value> for the smart spares threshold.
#+end_quote

--spares-alarm=

#+begin_quote
  Enable or disable the smart spares alarm. Options are /on/ or /off/.
#+end_quote

--spares-uninject

#+begin_quote
  Uninject any spare percentage previously injected.
#+end_quote

-f, --fatal

#+begin_quote
  Set the flag to spoof fatal health status.
#+end_quote

--fatal-uninject

#+begin_quote
  Uninject the fatal health status flag.
#+end_quote

-U, --unsafe-shutdown

#+begin_quote
  Set the flag to spoof an unsafe shutdown on the next power down.
#+end_quote

--unsafe-shutdown-uninject

#+begin_quote
  Uninject the unsafe shutdown flag.
#+end_quote

-N, --uninject-all

#+begin_quote
  Uninject all possible smart fields/values irrespective of whether they
  have been previously injected or not.
#+end_quote

-v, --verbose

#+begin_quote
  Emit debug messages for the error injection process
#+end_quote

-u, --human

#+begin_quote
  Format numbers representing storage sizes, or offsets as human
  readable strings with units instead of the default machine-friendly
  raw-integer data. Convert other numeric fields into hexadecimal
  strings.
#+end_quote

* COPYRIGHT
Copyright © 2016 - 2020, Intel Corporation. License GPLv2: GNU GPL
version 2 <http://gnu.org/licenses/gpl.html>. This is free software: you
are free to change and redistribute it. There is NO WARRANTY, to the
extent permitted by law.

* SEE ALSO
ndctl-list(1),
