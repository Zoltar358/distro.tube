#+TITLE: Man1 - pbmreduce.1
#+DESCRIPTION: Linux manpage for pbmreduce.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmreduce - read a PBM image and reduce it N times

* SYNOPSIS
*pbmreduce* [*-floyd*|*-fs*|*-threshold*] [*-value* /val/] /N/
[/pbmfile/]

You can abbreviate any option to its shortest unique prefix.

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmreduce* reads a PBM image as input and reduces it by a factor of
/N/, producing a PBM image as output.

*pbmreduce* duplicates a lot of the functionality of *pamditherbw*; you
could do something like =pamscale |= pamditherbw, but *pbmreduce* is a
lot faster.

You can use *pbmreduce* to 're-halftone' an image. Let's say you have a
scanner that only produces black&white, not grayscale, and it does a
terrible job of halftoning (most b&w scanners fit this description). One
way to fix the halftoning is to scan at the highest possible resolution,
say 300 dpi, and then reduce by a factor of three or so using
*pbmreduce*. You can even correct the brightness of an image, by using
the *-value* option.

* OPTIONS
By default, *pbmreduce* does the halftoning after the reduction via
boustrophedonic Floyd-Steinberg error diffusion; however, you can use
the *-threshold* option to specify simple thresholding. This gives
better results when reducing line drawings.

The *-value* option alters the thresholding value for all quantizations.
It should be a real number between 0 and 1. Above 0.5 means darker
images; below 0.5 means lighter.

* SEE ALSO
*pamenlarge*(1) , *pamscale*(1) , *pamditherbw*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1988 by Jef Poskanzer.
