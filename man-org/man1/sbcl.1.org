#+TITLE: Man1 - sbcl.1
#+DESCRIPTION: Linux manpage for sbcl.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SBCL -- Steel Bank Common Lisp

* DESCRIPTION
SBCL is an implementation of ANSI Common Lisp, featuring a
high-performance native compiler, native threads on several platforms, a
socket interface, a source-level debugger, a statistical profiler, and
much more.

It is free software, mostly in the public domain, but with some
subsystems under BSD-style licenses which allow modification and reuse
as long as credit is given. It is provided "as is", with no warranty of
any kind.

For more information about license issues, see the COPYING file in the
distribution. For more information about history, see the CREDITS file
in the distribution.

* RUNNING SBCL
To run SBCL, type "sbcl". After startup messages a prompt ("=*=")
appears. Enter a Lisp expression, and SBCL will read and execute it,
print any values returned, give you another prompt, and wait for your
next input.

$ sbcl ...[startup messages elided]... * (+ 1 2 3)

6 * (exit)

Most people like to run SBCL as a subprocess under Emacs. The Emacs
"Slime" mode provides many convenient features, like command line
editing, tab completion, and various kinds of coupling between Common
Lisp source files and the interactive SBCL subprocess.

For information on creating "standalone executables" using SBCL, see
=SB-EXT:SAVE-LISP-AND-DIE= in the User Manual.

* COMMAND LINE SYNTAX
For ordinary interactive use, no command line arguments should be
necessary.

In order to understand the SBCL command line syntax, it is helpful to
understand that the system is composed of two parts: a runtime
environment, and the Common Lisp system it supports. Some command line
arguments are processed during the initialization of the runtime, and
some during the initialization of the Lisp system -- any remaining
command line arguments are passed on to user code.

The overall command line syntax is:

#+begin_quote
  *sbcl [runtime options] --end-runtime-options [toplevel options]
  --end-toplevel-options [user options]*
#+end_quote

Both --end-runtime-options and --end-toplevel-options are optional, and
may be omitted. They are intended for use in situations where any
command line options are under user control (/e.g./ in batch files): by
using them you can prevent options intended for your program being
accidentally processed by SBCL.

Supported runtime options are

- *--core <corefilename>* :: Use the specified Lisp core file instead of
  the default. (See the FILES section for the standard core, or the
  system documentation for =SB-EXT:SAVE-LISP-AND-DIE= for information
  about how to create a custom core.) Note that if the Lisp core file is
  a user-created core file, it may run a nonstandard toplevel which does
  not recognize the standard toplevel options.

- *--dynamic-space-size <megabytes>* :: Size of the dynamic space
  reserved on startup in megabytes. Default value is platform dependent.

- *--control-stack-size <megabytes>* :: Size of control stack reserved
  for each thread in megabytes. Default value is 2.

- *--noinform* :: Suppress the printing of any banner or other
  informational message at startup. (This makes it easier to write Lisp
  programs which work cleanly in Unix pipelines. See also the
  "--noprint" and "--disable-debugger" options.)

- *--disable-ldb* :: Disable the low-level debugger. Only effective if
  SBCL is compiled with LDB.

- *--lose-on-corruption* :: There are some dangerous low level errors
  (for instance, control stack exhausted, memory fault) that (or whose
  handlers) can corrupt the image. By default SBCL prints a warning,
  then tries to continue and handle the error in Lisp, but this will not
  always work and SBCL may malfunction or even hang. With this option,
  upon encountering such an error SBCL will invoke ldb (if present and
  enabled) or else exit.

- *--script <filename>* :: As a runtime option equivalent to --noinform
  --disable-ldb --lose-on-corruption --end-runtime-options --script
  <filename>. See the description of --script as a toplevel option
  below.

- *--merge-core-pages* :: When platform support is present, provide
  hints to the operating system that identical pages may be shared
  between processes until they are written to. This can be useful to
  reduce the memory usage on systems with multiple SBCL processes
  started from similar but differently-named core files, or from
  compressed cores. Without platform support, do nothing. By default
  only compressed cores trigger hinting.

- *--no-merge-core-pages* :: Ensures that no sharing hint is provided to
  the operating system.

- *--help* :: Print some basic information about SBCL, then exit.

- *--version* :: Print SBCL's version information, then exit.

In the future, runtime options may be added to control behavior such as
lazy allocation of memory.

Runtime options, including any --end-runtime-options option, are
stripped out of the command line before the Lisp toplevel logic gets a
chance to see it.

The toplevel options supported by the standard SBCL core are

- *--sysinit <filename>* :: Load filename instead of the default
  system-wide initialization file. (See the FILES section.)

- *--no-sysinit* :: Do not load a system-wide initialization file. If
  this option is given, the --sysinit option is ignored.

- *--userinit <filename>* :: Load filename instead of the default user
  initialization file. (See the FILES section.)

- *--no-userinit* :: Do not load a user initialization file. If this
  option is given, the --userinit option is ignored.

- *--eval <command>* :: After executing any initialization file, but
  before starting the read-eval-print loop on standard input, read and
  evaluate the command given. More than one --eval option can be used,
  and all will be read and executed, in the order they appear on the
  command line.

- *--load <filename>* :: This is equivalent to --eval '(load
  "<filename>")'. The special syntax is intended to reduce quoting
  headaches when invoking SBCL from shell scripts.

- *--noprint* :: When ordinarily the toplevel "read-eval-print loop"
  would be executed, execute a "read-eval loop" instead, /i.e./ don't
  print a prompt and don't echo results. Combined with the --noinform
  runtime option, this makes it easier to write Lisp "scripts" which
  work cleanly in Unix pipelines.

- *--disable-debugger* :: By default when SBCL encounters an error, it
  enters the builtin debugger, allowing interactive diagnosis and
  possible intercession. This option disables the debugger, causing
  errors to print a backtrace and exit with status 1 instead -- which is
  a mode of operation better suited for batch processing. See the User
  Manual on =SB-EXT:DISABLE-DEBUGGER= for details.

- *--quit* :: At the end of toplevel option processing, exit SBCL with a
  successful code of zero. Note that the effect of this option is
  delayed until after toplevel options following this one.

- *--non-interactive* :: This option disables the read-eval-print loop
  for both exceptional and non-exceptional reasons. It is short for
  --disable-debugger and --quit in combination and is useful for batch
  uses where the special option processing implied by --script is not
  desired.

- *--script <filename>* :: Implies --no-sysinit --no-userinit
  --disable-debugger --end-toplevel-options.

Causes the system to load the specified file and exit immediately
afterwards, instead of entering the read-eval-print loop. If the file
begins with a shebang line, it is ignored.

Regardless of the order in which toplevel options appear on the command
line, the order of actions is:

- . :: Debugger is disabled, if requested.

- [step]. :: Any system initialization file is loaded, unless
  prohibited.

- [step]. :: Any user initialization file is loaded, unless prohibited.

- [step]. :: --eval and --load options are processed in the order given.

Finally, either the read-eval-print loop is entered or the file
specified with --script option is loaded.

When running in the read-eval-print loop the system exits on end of
file. Similarly, the system exits immediately after processing the file
specified with --script.

Note that when running SBCL with the --core option, using a core file
created by a user call to the =SB-EXT:SAVE-LISP-AND-DIE=, the toplevel
options may be under the control of user code passed as arguments to
=SB-EXT:SAVE-LISP-AND-DIE=. For this purpose, the --end-toplevel-options
option itself can be considered a toplevel option, /i.e./ the user core,
at its option, may not support it.

In the standard SBCL startup sequence (/i.e./ with no user core
involved) toplevel options and any --end-toplevel-options option are
stripped out of the command line argument list before user code gets a
chance to see it.

* OVERVIEW
SBCL is derived from the CMU CL. (The name is intended to acknowledge
the connection: steel and banking are the industries where Carnegie and
Mellon made the big bucks.)

SBCL compiles by default: even functions entered in the read-eval-print
loop are compiled to native code, unless the evaluator has been
explicitly turned on. (Even today, some 40 years after the MacLisp
compiler, people will tell you that Lisp is an interpreted language.
Ignore them.)

SBCL aims for but has not completely achieved compliance with the ANSI
standard for Common Lisp. More information about this is available in
the BUGS section below.

SBCL also includes various non-ANSI extensions, described more fully in
the User Manual. Some of these are in the base system and others are
"contrib" modules loaded on request using =REQUIRE=. For example, to
load the =SB-BSD-SOCKETS= module that provides TCP/IP connectivity, *
(require 'asdf) * (require 'sb-bsd-sockets)

For more information, see the User Manual.

* THE COMPILER
SBCL inherits from CMU CL the "Python" native code compiler. (Though we
often avoid that name in order to avoid confusion with the scripting
language also called Python.) This compiler is very clever about
understanding the type system of Common Lisp and using it to optimize
code, and about producing notes to let the user know when the compiler
doesn't have enough type information to produce efficient code. It also
tries (almost always successfully) to follow the unusual but very useful
principle that "declarations are assertions", /i.e./ type declarations
should be checked at runtime unless the user explicitly tells the system
that speed is more important than safety.

The compiled code uses garbage collection to automatically manage
memory. The garbage collector implementation varies considerably from
CPU to CPU. In particular, on some CPUs the GC is nearly exact, while on
others it's more conservative, and on some CPUs the GC is generational,
while on others simpler stop and copy strategies are used.

For more information about the compiler, see the user manual.

* SYSTEM REQUIREMENTS
SBCL currently runs on X86 (Linux, FreeBSD, OpenBSD, and NetBSD), X86-64
(Linux), PPC (Linux), SPARC (Linux and Solaris 2.x), and MIPS (Linux).
For information on other ongoing and possible ports, see the sbcl-devel
mailing list, and/or the web site.

SBCL requires on the order of 16Mb RAM to run on X86 systems, though all
but the smallest programs would be happier with 32Mb or more.

* KNOWN BUGS
This section attempts to list the most serious and long-standing bugs.
For more detailed and current information on bugs, see the BUGS file in
the distribution.

It is possible to get in deep trouble by exhausting heap memory. The
SBCL system overcommits memory at startup, so, on typical Unix-alikes
like Linux and FreeBSD, this means that if the SBCL system turns out to
use more virtual memory than the system has available for it, other
processes tend to be killed randomly (!).

The compiler's handling of function return values unnecessarily violates
the "declarations are assertions" principle that it otherwise adheres
to. Using =PROCLAIM= or =DECLAIM= to specify the return type of a
function causes the compiler to believe you without checking. Thus
compiling a file containing (DECLAIM (FTYPE (FUNCTION (T) NULL)
SOMETIMES)) (DEFUN SOMETIMES (X) (ODDP X)) (DEFUN FOO (X) (IF (SOMETIMES
X) 'THIS-TIME 'NOT-THIS-TIME))\\
then running =(FOO 1)= gives =NOT-THIS-TIME=, because the compiler
relied on the truth of the =DECLAIM= without checking it.

Some things are implemented very inefficiently.

- -- :: Multidimensional arrays are inefficient, especially
  multidimensional arrays of floating point numbers.

- -- :: SBCL, like most (maybe all?) implementations of Common Lisp on
  stock hardware, has trouble passing floating point numbers around
  efficiently, because a floating point number, plus a few extra bits to
  identify its type, is larger than a machine word. (Thus, they get
  "boxed" in heap-allocated storage, causing GC overhead.) Within a
  single compilation unit, or when doing built-in operations like =SQRT=
  and =AREF=, or some special operations like structure slot accesses,
  this is avoidable: see the user manual for some efficiency hints. But
  for general function calls across the boundaries of compilation units,
  passing the result of a floating point calculation as a function
  argument (or returning a floating point result as a function value) is
  a fundamentally slow operation.

* REPORTING BUGS
To report a bug, please send mail to the mailing lists sbcl-help or
sbcl-devel. You can find the complete mailing list addresses on the web
pages at <=http://sbcl.sourceforge.net/=>; note that as a spam reduction
measure you must subscribe to the lists before you can post. (You may
also find fancy SourceForge bug-tracking machinery there, but don't be
fooled. As of 2002-07-25 anyway, we don't actively monitor that
machinery, and it exists only because we haven't been able to figure out
how to turn it off.)

As with any software bug report, it's most helpful if you can provide
enough information to reproduce the symptoms reliably, and if you say
clearly what the symptoms are. For example, "There seems to be something
wrong with TAN of very small negative arguments. When I execute
=(TAN LEAST-NEGATIVE-SINGLE-FLOAT)= interactively on sbcl-1.2.3 on my
Linux 4.5 X86 box, I get an =UNBOUND-VARIABLE= error."

* DIFFERENCES FROM CMU CL
SBCL can be built from scratch using a plain vanilla ANSI Common Lisp
system and a C compiler, and all of its properties are specified by the
version of the source code that it was created from. This clean
bootstrappability was the immediate motivation for forking off of the
CMU CL development tree. A variety of implementation differences are
motivated by this design goal.

Maintenance work in SBCL since the fork has diverged somewhat from the
maintenance work in CMU CL. Many but not all bug fixes and improvements
have been shared between the two projects, and sometimes the two
projects disagree about what would be an improvement.

Most extensions supported by CMU CL have been unbundled from SBCL,
including Motif support, the Hemlock editor, search paths, the WIRE
protocol, various user-level macros and functions (/e.g./ =LETF=,
=ITERATE=, =MEMQ=, =REQUIRED-ARGUMENT=), and many others.

(Why doesn't SBCL support more extensions natively? Why drop all those
nice extensions from CMU CL when the code already exists? This is a
frequently asked question on the mailing list. There are two principal
reasons. First, it's a design philosophy issue: arguably SBCL has done
its job by supplying a stable FFI, and the right design decision is to
move functionality derived from that, like socket support, into separate
libraries. Some of these are distributed with SBCL as "contrib" modules,
others are distributed as separate software packages by separate
maintainers. Second, it's a practical decision - focusing on a smaller
number of things will, we hope, let us do a better job on them.)

* SUPPORT
Various information about SBCL is available at <=http://www.sbcl.org/=>.
The mailing lists there are the recommended place to look for support.

* AUTHORS
Dozens of people have made substantial contributions to SBCL and its
subsystems, and to the CMU CL system on which it was based, over the
years. See the CREDITS file in the distribution for more information.

* ENVIRONMENT
- *SBCL_HOME* :: This variable controls where files like "sbclrc",
  "sbcl.core", and the add-on "contrib" systems are searched for. If it
  is not set, then sbcl looks in *../lib/sbcl/* relative to the location
  of the executable, or in the current directory. If your executable is
  in the default location */usr/local/bin/* then it will look in
  */usr/local/lib/sbcl/*.

* FILES
- /sbcl/ :: executable program containing some low-level runtime support
  and a loader, used to read sbcl.core

- /sbcl.core/ :: dumped memory image containing most of SBCL, to be
  loaded by the `sbcl' executable. Looked for in *$SBCL_HOME*, unless
  overridden by the =--core= option.

- /sbclrc/ :: optional system-wide startup script, looked for in
  *$SBCL_HOME* then */etc*, unless overridden by the =--sysinit= command
  line option.

- /.sbclrc/ :: optional per-user customizable startup script (in user's
  home directory, or as specified by =--userinit=)

* SEE ALSO
Full SBCL documentation is maintained as a Texinfo manual. If it has
been installed, the command

#+begin_quote
  *info sbcl*
#+end_quote

should give you access to the complete manual. Depending on your
installation it may also be available in HTML and PDF formats in /e.g./

#+begin_quote
  */usr/local/share/doc/sbcl/*
#+end_quote

See the SBCL homepage

#+begin_quote
  <=http://www.sbcl.org/=>
#+end_quote

for more information, including directions on how to subscribe to the
sbcl-devel and sbcl-help mailing-lists.
