#+TITLE: Man1 - libfm-pref-apps.1
#+DESCRIPTION: Linux manpage for libfm-pref-apps.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libfm-pref-apps - sets preferred applications for programs based on
libfm

* SYNOPSIS
*libfm-pref-apps*

* DESCRIPTION
libfm-pref-apps is an application that changes preferred applications -
web browser and mail client. Those setting are appliable for programs
based on libfm but should work also for other XDG standard compliant
programs.

* FILES
~/.local/share/applications/mimeapps.list

* SEE ALSO
*pcmanfm*(1)

* AUTHOR
libfm-pref-apps was written by Hong Jen Yee <pcman.tw@gmail.com>.

This manual page was written by Sergey Slipchenko <faergeek@gmail.com>,
for the Debian project (and may be used by others). Updated by Andriy
Grytsenko <andrej@rep.kiev.ua>.
