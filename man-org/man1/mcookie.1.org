#+TITLE: Man1 - mcookie.1
#+DESCRIPTION: Linux manpage for mcookie.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mcookie - generate magic cookies for xauth

* SYNOPSIS
*mcookie* [options]

* DESCRIPTION
*mcookie* generates a 128-bit random hexadecimal number for use with the
X authority system. Typical usage:

#+begin_quote
  *xauth add :0 . *=mcookie=\\
#+end_quote

The "random" number generated is actually the MD5 message digest of
random information coming from one of the sources
*getrandom*=(2) system call, =//dev/urandom/=, =//dev/random/=, or the =/libc
pseudo-random
functions/=, in this preference order. See also the option =*--file*=.=

* OPTIONS
*-f*=, =*--file*= =/file/

#+begin_quote
  Use this
  /file/= as an additional source of randomness (for example =//dev/urandom/=). When =/file/= is '-', characters are read from standard input.=
#+end_quote

*-m*=, =*--max-size*= =/number/

#+begin_quote
  Read from
  /file/= only this =/number/= of bytes. This option is meant to be used when reading additional randomness from a file or device.=

  The
  /number/= argument may be followed by the multiplicative suffixes KiB=1024, MiB=1024*1024, and so on for GiB, TiB, PiB, EiB, ZiB and YiB (the "iB" is optional, e.g., "K" has the same meaning as "KiB") or the suffixes KB=1000, MB=1000*1000, and so on for GB, TB, PB, EB, ZB and YB.=
#+end_quote

*-v*=, =*--verbose*

#+begin_quote
  Inform where randomness originated, with amount of entropy read from
  each source.
#+end_quote

*-V*=, =*--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*=, =*--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* FILES
//dev/urandom/

//dev/random/

* BUGS
It is assumed that none of the randomness sources will block.

* SEE ALSO
*md5sum*=(1),= *X*=(7),= *xauth*=(1),= *rand*=(3)=

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The
*mcookie*= command is part of the util-linux package which can be downloaded from =
/Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
