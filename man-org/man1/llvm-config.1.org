#+TITLE: Man1 - llvm-config.1
#+DESCRIPTION: Linux manpage for llvm-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-config - Print LLVM compilation options

* SYNOPSIS
*llvm-config* /option/ [/components/...]

* DESCRIPTION
*llvm-config* makes it easier to build applications that use LLVM. It
can print the compiler flags, linker flags and object libraries needed
to link against LLVM.

* EXAMPLES
To link against the JIT:

#+begin_quote

  #+begin_quote
    #+begin_example
      g++ `llvm-config --cxxflags` -o HowToUseJIT.o -c HowToUseJIT.cpp
      g++ `llvm-config --ldflags` -o HowToUseJIT HowToUseJIT.o \
          `llvm-config --libs engine bcreader scalaropts`
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS
*--version*

#+begin_quote

  #+begin_quote
    Print the version number of LLVM.
  #+end_quote
#+end_quote

*-help*

#+begin_quote

  #+begin_quote
    Print a summary of *llvm-config* arguments.
  #+end_quote
#+end_quote

*--prefix*

#+begin_quote

  #+begin_quote
    Print the installation prefix for LLVM.
  #+end_quote
#+end_quote

*--src-root*

#+begin_quote

  #+begin_quote
    Print the source root from which LLVM was built.
  #+end_quote
#+end_quote

*--obj-root*

#+begin_quote

  #+begin_quote
    Print the object root used to build LLVM.
  #+end_quote
#+end_quote

*--bindir*

#+begin_quote

  #+begin_quote
    Print the installation directory for LLVM binaries.
  #+end_quote
#+end_quote

*--includedir*

#+begin_quote

  #+begin_quote
    Print the installation directory for LLVM headers.
  #+end_quote
#+end_quote

*--libdir*

#+begin_quote

  #+begin_quote
    Print the installation directory for LLVM libraries.
  #+end_quote
#+end_quote

*--cxxflags*

#+begin_quote

  #+begin_quote
    Print the C++ compiler flags needed to use LLVM headers.
  #+end_quote
#+end_quote

*--ldflags*

#+begin_quote

  #+begin_quote
    Print the flags needed to link against LLVM libraries.
  #+end_quote
#+end_quote

*--libs*

#+begin_quote

  #+begin_quote
    Print all the libraries needed to link against the specified LLVM
    /components/, including any dependencies.
  #+end_quote
#+end_quote

*--libnames*

#+begin_quote

  #+begin_quote
    Similar to *--libs*, but prints the bare filenames of the libraries
    without *-l* or pathnames. Useful for linking against a
    not-yet-installed copy of LLVM.
  #+end_quote
#+end_quote

*--libfiles*

#+begin_quote

  #+begin_quote
    Similar to *--libs*, but print the full path to each library file.
    This is useful when creating makefile dependencies, to ensure that a
    tool is relinked if any library it uses changes.
  #+end_quote
#+end_quote

*--components*

#+begin_quote

  #+begin_quote
    Print all valid component names.
  #+end_quote
#+end_quote

*--targets-built*

#+begin_quote

  #+begin_quote
    Print the component names for all targets supported by this copy of
    LLVM.
  #+end_quote
#+end_quote

*--build-mode*

#+begin_quote

  #+begin_quote
    Print the build mode used when LLVM was built (e.g. Debug or
    Release)
  #+end_quote
#+end_quote

* COMPONENTS
To print a list of all available components, run *llvm-config*
--components. In most cases, components correspond directly to LLVM
libraries. Useful "virtual" components include:

*all*

#+begin_quote

  #+begin_quote
    Includes all LLVM libraries. The default if no components are
    specified.
  #+end_quote
#+end_quote

*backend*

#+begin_quote

  #+begin_quote
    Includes either a native backend or the C backend.
  #+end_quote
#+end_quote

*engine*

#+begin_quote

  #+begin_quote
    Includes either a native JIT or the bitcode interpreter.
  #+end_quote
#+end_quote

* EXIT STATUS
If *llvm-config* succeeds, it will exit with 0. Otherwise, if an error
occurs, it will exit with a non-zero value.

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
