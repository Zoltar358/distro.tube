#+TITLE: Man1 - makerepropkg.1
#+DESCRIPTION: Linux manpage for makerepropkg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
makerepropkg - Rebuild a package to see if it is reproducible

* SYNOPSIS
makerepropkg [OPTIONS] [<package_file|pkgname>...]

* DESCRIPTION
Given the path to a built pacman package(s), attempt to rebuild it using
the PKGBUILD in the current directory. The package will be built in an
environment as closely matching the environment of the initial package
as possible, by building up a chroot to match the information exposed in
the package's *BUILDINFO*(5) manifest. On success, the resulting package
will be compared to the input package, and makerepropkg will report
whether the artifacts are identical.

When given multiple packages, additional package files are assumed to be
split packages and will be treated as additional artifacts to compare
during the verification step.

A valid target(s) for pacman -S can be specified instead, and
makerepropkg will download it to the cache if needed. This is mostly
useful to specify which repository to retrieve from. If no positional
arguments are specified, the targets will be sourced from the PKGBUILD.

In either case, the package name will be converted to a filename from
the cache, and makerepropkg will proceed as though this filename was
initially specified.

This implements a verifier for pacman/libalpm packages in accordance
with the Reproducible Builds project.

* OPTIONS
*-d*

#+begin_quote
  If packages are not reproducible, compare them using diffoscope.
#+end_quote

*-c*

#+begin_quote
  Set the pacman cache directory.
#+end_quote

*-M* <file>

#+begin_quote
  Location of a makepkg config file.
#+end_quote

*-h*

#+begin_quote
  Show this usage message
#+end_quote

* BUGS
Bugs can be reported on the bug tracker /https://bugs.archlinux.org/ in
the Arch Linux category and title prefixed with [devtools] or via
arch-projects@archlinux.org.

* AUTHORS
Maintainers:

#+begin_quote
  ·

  Aaron Griffin <aaronmgriffin@gmail.com>
#+end_quote

#+begin_quote
  ·

  Allan McRae <allan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dan McGee <dan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Evangelos Foutras <evangelos@foutrelis.com>
#+end_quote

#+begin_quote
  ·

  Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
#+end_quote

#+begin_quote
  ·

  Jelle van der Waa <jelle@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Levente Polyak <anthraxx@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Pierre Schmitz <pierre@archlinux.de>
#+end_quote

#+begin_quote
  ·

  Sébastien Luttringer <seblu@seblu.net>
#+end_quote

#+begin_quote
  ·

  Sven-Hendrik Haase <svenstaro@gmail.com>
#+end_quote

#+begin_quote
  ·

  Thomas Bächler <thomas@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the devtools.git
repository.
