#+TITLE: Man1 - ppmdmkfont.1
#+DESCRIPTION: Linux manpage for ppmdmkfont.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmdmkfont - Create Ppmdfont 'standard'.

* SYNOPSIS
*ppmdmkfont*

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmdmkfont* creates the 'standard' Ppmdfont font as a Ppmdfont file. It
has no input; it always generates identical files.

(There are no arguments or options)

This program is useful mainly as an example for creating other fonts.
*libnetpbm* has the 'standard' font built in.

See *Libnetpbm*PPM*Drawing*Function Manual (1) for details on Ppmdfont
files.

* SEE ALSO
*ppmdraw*(1) , *ppmddumpfont*(1) , *ppmdcfont*(1) ,
*Libnetpbm*PPM*Drawing*Function*Manual*(1)
