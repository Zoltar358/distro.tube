#+TITLE: Man1 - pnmnoraw.1
#+DESCRIPTION: Linux manpage for pnmnoraw.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pnmnoraw* - replaced by pnmtoplainpnm

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmnoraw* was replaced in Netpbm 8.2 (March 2000) by *pnmtoplainpnm*(1)
, which was obsoleted by *pnmtopnm* in Netpbm 10.23 (July 2004).

*pnmtoplainpnm* was actually the same program; it was just renamed to
make it clear that is just a format converter.

*pnmtopnm* is more general, in that it can go both directions. *pnmtopnm
-plain* is the same as *pnmnoraw*.
