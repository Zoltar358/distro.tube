#+TITLE: Man1 - bibtex8.1
#+DESCRIPTION: Linux manpage for bibtex8.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
bibtex8 - 8-bit Big BibTeX

* SYNOPSIS
*bibtex8* [/options/] /aux-file/

* DESCRIPTION
*BibTeX8* is an 8-bit implementation of BibTeX written in C. It has been
enhanced by adding the following features:

- "big" (32-bit) and customisable capacity

- flexible support for non-English languages using 8-bit character sets

*BibTeX8* accepts 8-bit characters in its input files, and writes 8-bit
characters to its output files. The character set is defined by an
external configuration file --- the Codepage and Sort order ("CS") file.
By default, it reads a specific CS file "88591lat.csf", which defines
ISO 8859-1 character set and its alphabetical sorting order including
accented characters.

* OPTIONS
- *-?* *--help* :: display some brief help text.

- *-7* *--traditional* :: operate in the original 7-bit mode, no CS file
  used. Only 7-bit ASCII characters are supported, and they are sorted
  by ASCII code value.

- *-8* *--8bit* :: force 8-bit mode, no CS file used. All 8-bit
  characters (code > 127) are treated as letters, and they are sorted by
  their code value.

- *-c* *--csfile* FILE :: read FILE as the BibTeX character set and sort
  definition file. Note that options *-7*, *-8* and *-c* are exclusive.

- *-d* *--debug* TYPE :: report debugging information. TYPE is one or
  more of all, csf, io, mem, misc, search.

- *-s* *--statistics* :: report internal statistics.

- *-t* *--trace* :: report execution tracing.

- *-v* *--version* :: report BibTeX version.

- *-B* *--big* :: set large BibTeX capacity.

- *-H* *--huge* :: set huge BibTeX capacity.

- *-W* *--wolfgang* :: set really huge BibTeX capacity for Wolfgang.

- *-M* *--min_crossrefs* ## :: set min_crossrefs to ##.

- *--mcites* ## :: allow ## \cites in the .aux files (deprecated).

- *--mentints* ## :: allow ## integer entries in the .bib databases
  (deprecated).

- *--mentstrs* ## :: allow ## string entries in the .bib databases
  (deprecated).

- *--mfields* ## :: allow ## fields in the .bib databases (deprecated).

- *--mpool* ## :: set the string pool to ## bytes (deprecated).

- *--mstrings* ## :: allow ## unique strings.

- *--mwizfuns* ## :: allow ## wizard functions (deprecated).

* SEE ALSO
More detailed description of *BibTeX8* is available at
$TEXMFDIST/doc/bibtex8/00readme.txt. The syntax of Codepage and Sort
order (CS) File can be found at $TEXMFDIST/doc/bibtex8/csfile.txt.

* AUTHORS
*BibTeX8* was written by Niel Kempson <kempson@snowyowl.co.uk> and
Alejandro Aguilar-Sierra <asierra@servidor.unam.mx>. This manpage was
written for TeX Live, based on the work by Norbert Preining for
Debian/GNU Linux.
