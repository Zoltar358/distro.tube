#+TITLE: Man1 - wvgain.1
#+DESCRIPTION: Linux manpage for wvgain.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
wvgain - adds ReplayGain information to wavpack files

* SYNOPSIS
*wvgain* [/-options/] /INFILE/...

* DESCRIPTION
*wvgain* perceptually analyzes WavPack audio files for volume,
calculates ReplayGain information based on this, and adds the
appropriate APEv2 tag fields. ReplayGain-enabled players will use this
information to produce the same perceived loudness on all tracks. Both
individual track and whole album ReplayGain information can be
calculated.

* OPTIONS
*-a*

#+begin_quote
  album mode (all files scanned are considered an album)
#+end_quote

*-c*

#+begin_quote
  clean ReplayGain values from all files
#+end_quote

*-d*

#+begin_quote
  display calculated values only (no files are modified)
#+end_quote

*-i*

#+begin_quote
  ignore .wvc file (forces hybrid lossy)
#+end_quote

*-n*

#+begin_quote
  new files only (skip files with track info, or album info if album
  mode specified)
#+end_quote

*-q*

#+begin_quote
  quiet (keep console output to a minimum)
#+end_quote

*-s*

#+begin_quote
  show stored values only (no analysis)
#+end_quote

*-v*

#+begin_quote
  write program version to stdout
#+end_quote

*-z[*/n/*]*

#+begin_quote
  dont set (n = 0 or omitted) or set (n = 1) console title to indicate
  progress (leaves "WvGain Completed")
#+end_quote

* SEE ALSO
*wavpack*(1), *wvunpack*(1), *wvtag*(1)

Please visit www.wavpack.com for more information

* COPYRIGHT
This manual page was written by Sebastian Dröge <slomo@debian.org> and
David Bryant <david@wavpack.com>. Permission is granted to copy,
distribute and/or modify this document under the terms of the BSD
License.

* AUTHORS
*Sebastian Dröge* <slomo@debian.org>

#+begin_quote
  Original author
#+end_quote

*David Bryant* <david@wavpack.com>

#+begin_quote
  Updates
#+end_quote

* COPYRIGHT
\\
Copyright © 2005 Sebastian Dröge\\
Copyright © 2021 David Bryant\\
