#+TITLE: Man1 - extconv.1
#+DESCRIPTION: Linux manpage for extconv.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
extconv - convert a TeX document in either Big 5+ or GBK encoding into
`preprocessed' form.

* SYNOPSIS
*extconv* < /infile/ > /outfile/

* DESCRIPTION
Big 5+ and GBK encodings use the characters {, }, and \ which have
special meanings in TeX documents.

After processing a LaTeX 2e document which contains such characters with
*extconv* you need not care about these special characters.

This filter is part of the *CJK* macro package for LaTeX 2e.

* SEE ALSO
*bg5conv*(1), *sjisconv*(1), *cefconv*(1), *cef5conv*(1),
*cefsconv*(1),\\
the *CJK* documentation files.

* AUTHOR
Werner Lemberg <wl@gnu.org>
