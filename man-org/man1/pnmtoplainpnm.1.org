#+TITLE: Man1 - pnmtoplainpnm.1
#+DESCRIPTION: Linux manpage for pnmtoplainpnm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pnmtoplainpnm* - replaced by pnmtopnm

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmtoplainpnm* was obsoleted in Netpbm 10.23 (July 2004) by
*pnmtopnm*(1)

*-plain*.

*pnmtoplainpnm* exists today for backward compatibility; all it does is
call *pnmtopnm -plain*.

*pnmtoplainpnm* was new in Netpbm 8.2 (March 2000) as a renaming of
*pnmnoraw*, which was new in Pbmplus in November 1989.
