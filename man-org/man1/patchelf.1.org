#+TITLE: Manpages - patchelf.1
#+DESCRIPTION: Linux manpage for patchelf.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
patchelf - Modify ELF files

* SYNOPSIS
*patchelf* /OPTION/

/FILE/

* DESCRIPTION
PatchELF is a simple utility for modifying existing ELF executables and
libraries. It can change the dynamic loader ("ELF interpreter") of
executables and change the RPATH of executables and libraries.

* OPTIONS
The single option given operates on each FILE, editing in place.

- --page-size SIZE :: Uses the given page size instead of the default.

- --set-interpreter INTERPRETER :: Change the dynamic loader ("ELF
  interpreter") of executable given to INTERPRETER.

- --print-interpreter :: Prints the ELF interpreter of the executable.

- --print-soname :: Prints DT_SONAME entry of .dynamic section. Raises
  an error if DT_SONAME doesn't exist.

- --set-soname SONAME :: Sets DT_SONAME entry of a library to SONAME.

- --set-rpath RPATH :: Change the RPATH of the executable or library to
  RPATH.

- --add-rpath RPATH :: Add RPATH to the existing RPATH of the executable
  or library.

- --remove-rpath :: Removes the DT_RPATH or DT_RUNPATH entry of the
  executable or library.

- --shrink-rpath :: Remove from the RPATH all directories that do not
  contain a library referenced by DT_NEEDED fields of the executable or
  library.

For instance, if an executable references one library libfoo.so, has an
RPATH "/lib:/usr/lib:/foo/lib", and libfoo.so can only be found in
/foo/lib, then the new RPATH will be "/foo/lib".

- --allowed-rpath-prefixes PREFIXES :: Combined with the
  "--shrink-rpath" option, this can be used for further rpath tuning.
  For instance, if an executable has an RPATH
  "/tmp/build-foo/.libs:/foo/lib", it is probably desirable to keep the
  "/foo/lib" reference instead of the "/tmp" entry.

- --print-rpath :: Prints the RPATH for an executable or library.

- --force-rpath :: Forces the use of the obsolete DT_RPATH in the file
  instead of DT_RUNPATH. By default DT_RPATH is converted to DT_RUNPATH.

- --add-needed LIBRARY :: Adds a declared dependency on a dynamic
  library (DT_NEEDED). This option can be give multiple times.

- --replace-needed LIB_ORIG LIB_NEW :: Replaces a declared dependency on
  a dynamic library with another one (DT_NEEDED). This option can be
  give multiple times.

- --remove-needed LIBRARY :: Removes a declared dependency on LIBRARY
  (DT_NEEDED entry). This option can be given multiple times.

- --no-default-lib :: Marks the object that the search for dependencies
  of this object will ignore any default library search paths.

- --output FILE :: Set the output file name. If not specified, the input
  will be modified in place.

- --debug :: Prints details of the changes made to the input file.

- --version :: Shows the version of patchelf.

* AUTHOR
Eelco Dolstra <e.dolstra@tudelft.nl>

* SEE ALSO
*elf*(5), *ld.so*(8)

Information about patchelf.1 is found in manpage for: [[../RPATH "/lib:/usr/lib:/foo/lib", and libfoo.so can only be found
ld.so (8)][RPATH "/lib:/usr/lib:/foo/lib", and libfoo.so can only be found
ld.so (8)]]