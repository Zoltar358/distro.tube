#+TITLE: Man1 - chezdav.1
#+DESCRIPTION: Linux manpage for chezdav.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
chezdav - simple WebDAV server

* SYNOPSIS
*chezdav* [/OPTIONS.../]

* DESCRIPTION
The chezdav(1) command starts a WebDAV server. This allows clients to
access your files, and modify them. A simple Web browser is sufficient
to browse for files. The default shared folder is the home directory,
but you may specifiy a different folder with the *-P* option.

* OPTIONS
*-d, --htdigest*=PATH

#+begin_quote
  Path to a htdigest file, to protect server with DIGEST method.
#+end_quote

*-P, --path*=PATH

#+begin_quote
  Path to export.
#+end_quote

*-p, --port*=PORT

#+begin_quote
  Port to listen from. By default, a port is allocated randomly.
#+end_quote

*-v, --verbose*

#+begin_quote
  Verbosely print running information.
#+end_quote

*--version*

#+begin_quote
  Print program version number.
#+end_quote

* EXIT STATUS
*0*

#+begin_quote
  Success
#+end_quote

*1*

#+begin_quote
  Failure (syntax or usage error; unexpected error).
#+end_quote

* AUTHOR
chezdav was originally written by Marc-André Lureau.

* RESOURCES
Main web site: *http://wiki.gnome.org/phodav*

* COPYING
Copyright (C) 2013-2014 Red Hat, Inc. Free use of this software is
granted under the terms of the GNU Lesser General Public License (lGPL).
