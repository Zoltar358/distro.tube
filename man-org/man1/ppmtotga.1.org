#+TITLE: Man1 - ppmtotga.1
#+DESCRIPTION: Linux manpage for ppmtotga.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmtotga - replaced by pamtotga

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmtotga* was replaced in Netpbm 10.6 (July 2002) by *pamtotga*(1)

*pamtotga* is backward compatible with *ppmtotga*, but also recognizes
PAM input, including that with an alpha channel.
