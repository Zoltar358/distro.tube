#+TITLE: Man1 - grub-editenv.1
#+DESCRIPTION: Linux manpage for grub-editenv.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-editenv - edit GRUB environment block

* SYNOPSIS
*grub-editenv* [/OPTION/...] /FILENAME COMMAND/

* DESCRIPTION
Tool to edit environment block.

#+begin_quote
  Commands:
#+end_quote

- create :: Create a blank environment block file.

- list :: List the current variables.

- set [NAME=VALUE ...] :: Set variables.

- unset [NAME ...] :: Delete variables.

  Options:

- -?, *--help* :: give this help list

- *--usage* :: give a short usage message

- *-v*, *--verbose* :: print verbose messages.

- *-V*, *--version* :: print program version

If FILENAME is `-', the default value //boot/grub/grubenv is used.

There is no `delete' command; if you want to delete the whole
environment block, use `rm //boot/grub/grubenv'.

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-reboot*(8), *grub-set-default*(8)

The full documentation for *grub-editenv* is maintained as a Texinfo
manual. If the *info* and *grub-editenv* programs are properly installed
at your site, the command

#+begin_quote
  *info grub-editenv*
#+end_quote

should give you access to the complete manual.
