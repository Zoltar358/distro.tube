#+TITLE: Man1 - gtk4-builder-tool.1
#+DESCRIPTION: Linux manpage for gtk4-builder-tool.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gtk4-builder-tool - GtkBuilder file utility

* SYNOPSIS
*gtk4-builder-tool* [/COMMAND/] [/OPTION/...] /FILE/

* DESCRIPTION
*gtk4-builder-tool* can perform various operations on GtkBuilder .ui
files.

The *validate* command validates the .ui file and reports errors to
stderr.

The *enumerate* command lists all the named objects that are created in
the .ui file.

The *preview* command displays the .ui file. This command accepts
options to specify the ID of the toplevel object and a .css file to use.

The *simplify* command simplifies the .ui file by removing properties
that are set to their default values and writes the resulting XML to
stdout, or back to the input file.

When the *--3to4* is specified, *simplify* interprets the input as a GTK
3 ui file and attempts to convert it to GTK 4 equivalents. It performs
various conversions, such as renaming properties, translating child
properties to layout properties, rewriting the setup for GtkNotebook,
GtkStack, GtkAssistant or changing toolbars into boxes.

You should always test the modified .ui files produced by
gtk4-builder-tool before using them in production.

Note in particular that the conversion done with *--3to4* is meant as a
starting point for a port from GTK 3 to GTK 4. It is expected that you
will have to do manual fixups after the initial conversion.

* SIMPLIFY OPTIONS
The *simplify* command accepts the following options:

*--replace*

#+begin_quote
  Write the content back to the .ui file instead of stdout.
#+end_quote

*--3to4*

#+begin_quote
  Transform a GTK 3 ui file to GTK 4
#+end_quote

* PREVIEW OPTIONS
The *preview* command accepts the following options:

*--id=ID*

#+begin_quote
  The ID of the object to preview. If not specified, gtk4-builder-tool
  will choose a suitable object on its own.
#+end_quote

*--css=FILE*

#+begin_quote
  Load style information from the given .css file.
#+end_quote
