#+TITLE: Manpages - dvilualatex-dev.1
#+DESCRIPTION: Linux manpage for dvilualatex-dev.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about dvilualatex-dev.1 is found in manpage for: [[../man1/latex-dev.1][man1/latex-dev.1]]