#+TITLE: Man1 - mktextfm.1
#+DESCRIPTION: Linux manpage for mktextfm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mktextfm - create a TFM file for a font

* SYNOPSIS
*mktextfm* [/options/] /font/

* DESCRIPTION
*mktextfm* is used to generate a tfm file from the Metafont source files
for /font/, or *hbf2gf*(1), if possible. If /destdir/ is given, the
generated file will be installed there, otherwise a (rather complicated)
heuristic is used. If the tfm file already exists in the destination
location, this is reported and nothing is done.

If a GF (Generic Font) bitmap file is also generated, as is typical with
ΜF, it is converted to PK (Packed Font) and installed similarly.

The full pathname of the generated file is printed on standard output.

If available, the *mf-nowin*(1) variant of Metafont is used to generate
fonts to avoid the possibility of online display.

*mktextfm* is typically called by other programs, via Kpathsea, rather
than from the command line.

* OPTIONS
*mktextfm* accepts the following options:

- *--destdir*/ dir/ :: A directory name. If the directory is absolute,
  it is used as-is. Otherwise, it is appended to the root destination
  directory set in the script.

- *--help* :: Print help message and exit successfully.

- *--version* :: Print version information and exit successfully.

* ENVIRONMENT
One environment variable is specific to *mktextfm*:
*MF_MODE_EXTRA_INFO*. If this is set to a non-empty value, a Metafont
macro *mode_include_extra_info* will be invoked when the font is made.
The standard *modes.mf* file defines this, as of modes.mf version 4.0,
released in 2020. This causes the so-called Xerox-world information,
notably including the *CODINGSCHEME* for the font, to be included in the
tfm file. (This is not done by default since it is too intrusive to
redefine the necessary primitives, per Don Knuth.) The *mftrace*(1)
program, for example, can use this to get a clue about the font
encoding, although the information is not always perfectly definitive or
unambiguous.

For more about the encodings of Metafont fonts, see the section ``Bitmap
font encodings'' in the Dvips manual (e.g.,
https://tug.org/texinfohtml/dvips.html).

The many other environment variables and various configuration files
that control a TeX system also affect the behavior of *mktextfm*, as
usual.

* SEE ALSO
*mf*(1), *mktexmf*(1), *mktexpk*(1).

* REPORTING BUGS
Report bugs to: tex-k@tug.org (https://lists.tug.org/tex-k)\\
TeX Live home page: https://tug.org/texlive/
