#+TITLE: Man1 - idevicenotificationproxy.1
#+DESCRIPTION: Linux manpage for idevicenotificationproxy.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idevicenotificationproxy - Post or observe notifications on a device.

* SYNOPSIS
*idevicenotificationproxy* [OPTIONS] COMMAND

* DESCRIPTION
Post or observe notifications on an iOS device from the command line.

* OPTIONS
- *-u, --udid UDID* :: target specific device by UDID.

- *-n, --network* :: connect to network device.

- *-d, --debug* :: enable communication debugging.

- *-h, --help* :: prints usage information.

- *-v, --version* :: prints version information.

* COMMANDS
- *post ID [ID...]* :: post notification IDs to device and exit.

- *observe ID [ID...]* :: observe notification IDs in the foreground
  until CTRL+C or signal is received.

* AUTHORS
Martin Szulecki

* ON THE WEB
https://libimobiledevice.org

https://github.com/libimobiledevice/libimobiledevice
