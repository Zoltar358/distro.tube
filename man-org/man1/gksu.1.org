#+TITLE: Man1 - gksu.1
#+DESCRIPTION: Linux manpage for gksu.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gksu - GTK+ frontend for su and sudo

* SYNOPSIS
*gksu*

*gksu* [/-u <user>/] [/options/] /<command>/

*gksudo* [/-u <user>/] [/options/] /<command>/

* DESCRIPTION
This manual page documents briefly *gksu* and *gksudo*

gksu is a frontend to su and gksudo is a frontend to sudo. Their primary
purpose is to run graphical commands that need root without the need to
run an X terminal emulator and using su directly.

Notice that all the magic is done by the underlying library, libgksu.
Also notice that the library will decide if it should use su or sudo as
backend using the */apps/gksu/sudo-mode* gconf key, if you call the
*gksu* command. You can force the backend by using the *gksudo* command,
or by using the *--sudo-mode* and *--su-mode* options.

If no command is given, the *gksu* program will display a small window
that allows you to type in a command to be run, and to select what user
the program should be run as. The other options are disregarded, right
now, in this mode.

* OPTIONS
*--debug*, *-d*

#+begin_quote
  Print information on the screen that might be useful for diagnosing
  and/or solving problems.
#+end_quote

*--user* <user>, *-u* <user>

#+begin_quote
  Call <command> as the specified user.
#+end_quote

*--disable-grab*, *-g*

#+begin_quote
  Disable the "locking" of the keyboard, mouse, and focus done by the
  program when asking for password.
#+end_quote

*--prompt*, *-P*

#+begin_quote
  Ask the user if they want to have their keyboard and mouse grabbed
  before doing so.
#+end_quote

*--preserve-env*, *-k*

#+begin_quote
  Preserve the current environments, does not set $HOME nor $PATH, for
  example.
#+end_quote

*--login*, *-l*

#+begin_quote
  Make this a login shell. Beware this may cause problems with the
  Xauthority magic. Run xhost to allow the target user to open windows
  on your display!
#+end_quote

*--description* <description|file>, *-D* <description|file>

#+begin_quote
  Provide a descriptive name for the command to be used in the default
  message, making it nicer. You can also provide the absolute path for a
  .desktop file. The Name key for will be used in this case.
#+end_quote

*--message* <message>, *-m* <message>

#+begin_quote
  Replace the standard message shown to ask for password for the
  argument passed to the option. Only use this if *--description* does
  not suffice.
#+end_quote

*--print-pass*, *-p*

#+begin_quote
  Ask gksu to print the password to stdout, just like ssh-askpass.
  Useful to use in scripts with programs that accept receiving the
  password on stdin.
#+end_quote

*--su-mode*, *-w*

#+begin_quote
  Force gksu to use su(1) as its backend for running the programs.
#+end_quote

*--sudo-mode*, *-S*

#+begin_quote
  Force gksu to use sudo(1) as its backend for running the programs.
#+end_quote

* SEE ALSO
*su*(1), *sudo*(1)
