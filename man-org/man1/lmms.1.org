#+TITLE: Man1 - lmms.1
#+DESCRIPTION: Linux manpage for lmms.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lmms - software for easy music production

* SYNOPSIS
*lmms* "[*globaloptions...]*[*action[*action**parameters...]]\\

* DESCRIPTION
*LMMS* is a free cross-platform alternative to commercial programs like
FL Studio®, which allow you to produce music with your computer. This
includes the creation of melodies and beats, the synthesis and mixing of
sounds, and arranging of samples. You can have fun with your
MIDI-keyboard and much more; all in a user-friendly and modern
interface.

LMMS features components such as a Song Editor, a Beat+Bassline Editor,
a Piano Roll, an FX Mixer as well as many powerful instruments and
effects.

* ACTIONS
- "<no :: Start LMMS in normal GUI mode.

- "dump :: Dump XML of compressed (MMPZ) file /in/.

- "render :: Render given project file.

- "rendertracks :: Render each track to a different file.

- "upgrade :: Upgrade file /in/ and save as /out/. Standard out is used
  if no output file is specifed.

* GLOBAL OPTIONS
- "  :: Bypass root user startup check (use with caution).

- "-c, :: Get the configuration from /configfile/* instead of
  ~/.lmmsrc.xml (default).*

- "-h, :: Show usage information and exit.

- "-v, :: Show version information and exit.

* OPTIONS IF NO ACTION IS GIVEN
- "  :: Specify the prefered size and position of the main window.\\
  /geometry/* syntax is */xsize/*x*/ysize/*+*/xoffset/*+*/yoffset/*.*\\
  Default: full screen.

- "  :: Import MIDI or Hydrogen file /in/*.*\\

* OPTIONS FOR RENDER AND RENDERTRACKS
- "-a, :: Use 32bit float bit depth.

- "-b, :: Specify output bitrate in KBit/s (for OGG encoding only),
  default is 160.

- "-f, :: Specify format of render-output where /format/* is either
  'wav', 'ogg' or 'mp3'.*

- "-i, :: Specify interpolation method - possible values are /linear/*,
  */sincfastest/* (default), */sincmedium/*, */sincbest/*.*

If -e is specified lmms exits after importing the file.

- "-l, :: Render the given file as a loop, i.e. stop rendering at
  exactly the end of the song. Additional silence or reverb tails at the
  end of the song are not rendered.

- "-m, :: Set the stereo mode used for the MP3 export. /stereomode/* can
  be either 's' (stereo mode), 'j' (joint stereo) or 'm' (mono). If no
  mode is given 'j' is used as the default.*

- "-o, :: Render into /path/*.*\\
  For --render, this is interpreted as a file path.\\
  For --render-tracks, this is interpreted as a path to an existing
  directory.

- "-p, :: Dump profiling information to file /out/*.*

- "-s, :: Specify output samplerate in Hz - range is 44100 (default)
  to 192000.

- "-x, :: Specify oversampling, possible values: 1, 2 (default), 4, 8.

* SEE ALSO
*https://lmms.io/* *https://lmms.io/documentation/*
*https://github.com/LMMS/lmms*

* AUTHORS
Tobias Doerffel <tobydox/at/users.sourceforge.net>, Paul Giblock and
others. See AUTHORS for details.
