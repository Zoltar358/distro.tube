#+TITLE: Manpages - dvilj6.1
#+DESCRIPTION: Linux manpage for dvilj6.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about dvilj6.1 is found in manpage for: [[../man1/dvilj.1][man1/dvilj.1]]