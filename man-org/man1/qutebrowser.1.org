#+TITLE: Man1 - qutebrowser.1
#+DESCRIPTION: Linux manpage for qutebrowser.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
qutebrowser - a keyboard-driven, vim-like browser based on PyQt5.

* SYNOPSIS
*qutebrowser* [/-OPTION/ [/.../]] [/:COMMAND/ [/.../]] [/URL/ [/.../]]

* DESCRIPTION
qutebrowser is a keyboard-focused browser with a minimal GUI. It's based
on Python and Qt5 and is free software, licensed under the GPL.

It was inspired by other browsers/addons like dwb and
Vimperator/Pentadactyl.

Note the commands and settings of qutebrowser are not described in this
manpage, but in the help integrated in qutebrowser - use the ":help"
command to show it.

* OPTIONS
** positional arguments
/:command/

#+begin_quote
  Commands to execute on startup.
#+end_quote

/URL/

#+begin_quote
  URLs to open on startup (empty as a window separator).
#+end_quote

** optional arguments
*-h*, *--help*

#+begin_quote
  show this help message and exit
#+end_quote

*-B* /BASEDIR/, *--basedir* /BASEDIR/

#+begin_quote
  Base directory for all storage.
#+end_quote

*-C* /CONFIG/, *--config-py* /CONFIG/

#+begin_quote
  Path to config.py.
#+end_quote

*-V*, *--version*

#+begin_quote
  Show version and quit.
#+end_quote

*-s* /OPTION/ /VALUE/, *--set* /OPTION/ /VALUE/

#+begin_quote
  Set a temporary setting for this session.
#+end_quote

*-r* /SESSION/, *--restore* /SESSION/

#+begin_quote
  Restore a named session.
#+end_quote

*-R*, *--override-restore*

#+begin_quote
  Don't restore a session even if one would be restored.
#+end_quote

*--target*
/{auto,tab,tab-bg,tab-silent,tab-bg-silent,window,private-window}/

#+begin_quote
  How URLs should be opened if there is already a qutebrowser instance
  running.
#+end_quote

*--backend* /{webkit,webengine}/

#+begin_quote
  Which backend to use.
#+end_quote

*--desktop-file-name* /DESKTOP_FILE_NAME/

#+begin_quote
  Set the base name of the desktop entry for this application. Used to
  set the app_id under Wayland. See
  https://doc.qt.io/qt-5/qguiapplication.html#desktopFileName-prop
#+end_quote

*--untrusted-args*

#+begin_quote
  Mark all following arguments as untrusted, which enforces that they
  are URLs/search terms (and not flags or commands)
#+end_quote

** debug arguments
*-l* /{critical,error,warning,info,debug,vdebug}/, *--loglevel*
/{critical,error,warning,info,debug,vdebug}/

#+begin_quote
  Override the configured console loglevel
#+end_quote

*--logfilter* /LOGFILTER/

#+begin_quote
  Comma-separated list of things to be logged to the debug log on
  stdout.
#+end_quote

*--loglines* /LOGLINES/

#+begin_quote
  How many lines of the debug log to keep in RAM (-1: unlimited).
#+end_quote

*-d*, *--debug*

#+begin_quote
  Turn on debugging options.
#+end_quote

*--json-logging*

#+begin_quote
  Output log lines in JSON format (one object per line).
#+end_quote

*--nocolor*

#+begin_quote
  Turn off colored logging.
#+end_quote

*--force-color*

#+begin_quote
  Force colored logging
#+end_quote

*--nowindow*

#+begin_quote
  Don't show the main window.
#+end_quote

*-T*, *--temp-basedir*

#+begin_quote
  Use a temporary basedir.
#+end_quote

*--no-err-windows*

#+begin_quote
  Don't show any error windows (used for tests/smoke.py).
#+end_quote

*--qt-arg* /NAME/ /VALUE/

#+begin_quote
  Pass an argument with a value to Qt. For example, you can do --qt-arg
  geometry 650x555+200+300 to set the window geometry.
#+end_quote

*--qt-flag* /QT_FLAG/

#+begin_quote
  Pass an argument to Qt as flag.
#+end_quote

*-D* /DEBUG_FLAGS/, *--debug-flag* /DEBUG_FLAGS/

#+begin_quote
  Pass name of debugging feature to be turned on.
#+end_quote

* FILES

#+begin_quote
  ·

  /~/.config/qutebrowser/config.py/: Configuration file.
#+end_quote

#+begin_quote
  ·

  /~/.config/qutebrowser/autoconfig.yml/: Configuration done via the
  GUI.
#+end_quote

#+begin_quote
  ·

  /~/.config/qutebrowser/quickmarks/: Saved quickmarks.
#+end_quote

#+begin_quote
  ·

  /~/.local/share/qutebrowser//: Various state information.
#+end_quote

#+begin_quote
  ·

  /~/.cache/qutebrowser//: Temporary data.
#+end_quote

Note qutebrowser conforms to the XDG basedir specification - if
/XDG_CONFIG_HOME/, /XDG_DATA_HOME/ or /XDG_CACHE_HOME/ are set in the
environment, the directories configured there are used instead of the
above defaults.

* BUGS
Bugs are tracked in the Github issue tracker at
https://github.com/qutebrowser/qutebrowser/issues.

If you found a bug, use the built-in /:report/ command to create a bug
report with all information needed.

If you prefer, you can also write to the mailinglist at
qutebrowser@lists.qutebrowser.org instead.

For security bugs, please contact me directly at me@the-compiler.org,
GPG ID 0xFD55A072.

* COPYRIGHT
This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see https://www.gnu.org/licenses/.

* RESOURCES

#+begin_quote
  ·

  Website: https://www.qutebrowser.org/
#+end_quote

#+begin_quote
  ·

  Mailinglist: qutebrowser@lists.qutebrowser.org /
  https://lists.schokokeks.org/mailman/listinfo.cgi/qutebrowser
#+end_quote

#+begin_quote
  ·

  Announce-only mailinglist: qutebrowser-announce@lists.qutebrowser.org
  /
  https://lists.schokokeks.org/mailman/listinfo.cgi/qutebrowser-announce
#+end_quote

#+begin_quote
  ·

  IRC: #qutebrowser on Libera Chat (webchat, via Matrix)
#+end_quote

#+begin_quote
  ·

  Github: https://github.com/qutebrowser/qutebrowser
#+end_quote

* AUTHOR
*qutebrowser* was written by Florian Bruhin. All contributors can be
found in the README file distributed with qutebrowser.
