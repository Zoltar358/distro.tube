#+TITLE: Man1 - kwallet-query.1
#+DESCRIPTION: Linux manpage for kwallet-query.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.SH "NAME" kwallet-query - KDE Wallet command-line manipulation tool

.SH "SYNOPSIS"

.sp *kwallet-query* /OPTIONS/ /wallet/

.SH "DESCRIPTION"

.sp *kwallet-query* comes in handy when shell scripts need to read or
update the KDE Wallet. It works by manipulating the entries displayed in
the *KDE Wallet Manager* utility. It's only parameter is the
/wallet/name the tool should read or update. The operation mode is
specified by the options.

.SH "OPTIONS"

.PP *-h,--help*

#+begin_quote
  Display a short help message.

  .RE .PP *-l,--list-entries*

  #+begin_quote
    List password entries. These are the folder names displayed in the
    *KDE Wallet Manager* utility. If the *-f* option is given, this will
    only display the subfolders of the specified folder.

    .RE .PP *-r,--read-password* /Entry/

    #+begin_quote
      Read the contents of the given /Entry/ from the *Folder*section of
      the /wallet/ and output it on the standard output. Maps are
      exported as JSON object.

      .RE .PP *-w,--write-password* /Entry/

      #+begin_quote
        Write secrets to the given /Entry/ under the *Folder* section of
        the given /wallet/. The secrets are read from the standard
        input. Maps take in input a well-formed JSON object. *IMPORTANT*
        previous wallet entry value will be overwritten by this option,
        so be careful when using it!

        .RE .PP *-f,--folder* /Folder/

        #+begin_quote
          Set the /wallet/ folder to /Folder/ value. By default
          *Passwords* is used.

          .RE .PP *-v,--verbose*

          #+begin_quote
            Output more information when performing the operation, to
            help debugging.

            .RE .SH "EXIT STATUS"

            .PP *0*

            #+begin_quote
              Success.

              .RE .PP *1*

              #+begin_quote
                The wallet /wallet/ was not found.

                .RE .PP *2*

                #+begin_quote
                  The wallet /wallet/ could not be opened. For example,
                  that would be an indication of a bad password entry or
                  some other problem with the KDE Wallet system.

                  .RE .PP *3*

                  #+begin_quote
                    The *Folder* section was not found inside the wallet
                    /wallet/. Perhaps the wallet file is corrupt?

                    .RE .PP *4*

                    #+begin_quote
                      The read or write operation has failed for some
                      reason.

                      .RE .SH "BUGS"

                      .sp Please report all bugs on the KDE bug
                      reporting website: bugs.kde.org. Be sure to select
                      “kwallet-query” when submitting your bug-report.

                      .SH "AUTHOR"

                      .sp *kwallet-query* was originally written by
                      Valentin Rusu and is part of KDE.

                      .SH "COPYING"

                      .sp Copyright (C) 2015 Valentin Rusu. Free use of
                      this software is granted under the terms of the
                      GNU General Public License (GPL).
                    #+end_quote
                  #+end_quote
                #+end_quote
              #+end_quote
            #+end_quote
          #+end_quote
        #+end_quote
      #+end_quote
    #+end_quote
  #+end_quote
#+end_quote

* AUTHOR
*Valentin Rusu* <kde@rusu.info>

#+begin_quote
  Original author
#+end_quote
