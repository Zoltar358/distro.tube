#+TITLE: Man1 - pgmnorm.1
#+DESCRIPTION: Linux manpage for pgmnorm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pgmnorm* - replaced by pnmnorm

* DESCRIPTION
This program is part of *Netpbm*(1)

*pgmnorm* was replaced in Netpbm 9.25 (March 2002) by *pnmnorm*(1)

*pnmnorm* is backward compatible with *pgmnorm*, but it also handles PPM
images.
