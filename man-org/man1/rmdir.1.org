#+TITLE: Man1 - rmdir.1
#+DESCRIPTION: Linux manpage for rmdir.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rmdir - remove empty directories

* SYNOPSIS
*rmdir* [/OPTION/]... /DIRECTORY/...

* DESCRIPTION
Remove the DIRECTORY(ies), if they are empty.

*--ignore-fail-on-non-empty*

#+begin_quote
  ignore each failure that is solely because a directory

  is non-empty
#+end_quote

- *-p*, *--parents* :: remove DIRECTORY and its ancestors; e.g., 'rmdir
  *-p* a/b/c' is similar to 'rmdir a/b/c a/b a'

- *-v*, *--verbose* :: output a diagnostic for every directory processed

- *--help* :: display this help and exit

- *--version* :: output version information and exit

* AUTHOR
Written by David MacKenzie.

* REPORTING BUGS
GNU coreutils online help: <https://www.gnu.org/software/coreutils/>\\
Report any translation bugs to <https://translationproject.org/team/>

* COPYRIGHT
Copyright © 2021 Free Software Foundation, Inc. License GPLv3+: GNU GPL
version 3 or later <https://gnu.org/licenses/gpl.html>.\\
This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
rmdir(2)

\\
Full documentation <https://www.gnu.org/software/coreutils/rmdir>\\
or available locally via: info '(coreutils) rmdir invocation'
