#+TITLE: Man1 - plugreport.1
#+DESCRIPTION: Linux manpage for plugreport.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
*\$1*

- \$1 :: 

* NAME
plugreport - a program to read all MPR/PCR registers from all devices
and report them.

* SYNOPSIS
*plugreport*

* DESCRIPTION
This manual page documents briefly the *plugreport* and was written for
the Debian distribution because the original program does not have a
manual page.

*plugreport* is a program to read all MPR/PCR registers from all devices
and report them.

* SEE ALSO
plugctl (1).

* AUTHOR
This manual page was written by Marcio Roberto Teixeira
<marciotex@gmail.com> for the Debian system (but may be used by others).
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU General Public License, Version 2 any later
version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

* AUTHOR
Marcio Teixeira.
