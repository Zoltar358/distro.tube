#+TITLE: Man1 - vdeterm.1
#+DESCRIPTION: Linux manpage for vdeterm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vdeterm - (simple) Remote terminal for vde management sockets

* SYNOPSIS
*vdeterm* /socket/\\

* DESCRIPTION
A *vdeterm* is a terminal application for a vde tools stream socket.

It has been created as a part of the vde-2 project: it is used as a
remote console for vde_switch or for the wirefilter application.

vdeterm provides command editing, history of previous commands, command
completion. Debug asynchronous messages do not interfere with the
command editing line.

* NOTICE
Virtual Distributed Ethernet is not related in any way with www.vde.com
("Verband der Elektrotechnik, Elektronik und Informationstechnik" i.e.
the German "Association for Electrical, Electronic & Information
Technologies").

* SEE ALSO
*vde_switch*(1), *wirefilter*(1).\\

* AUTHOR
VDE is a project by Renzo Davoli <renzo@cs.unibo.it>
