#+TITLE: Man1 - calcurse.1
#+DESCRIPTION: Linux manpage for calcurse.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
calcurse - terminal-based organizer for interactive and command line use

* SYNOPSIS
*calcurse* [-D /datadir/] [-C /confdir/] [-c /calendar-file/]

*calcurse* -Q [*--from* /date/] [*--to* /date/ | *--days* /number/]

*calcurse* -a | -d /date/ | -d /number/ | -n | -r[/number/] | -s[/date/]
| -t[/number/]

*calcurse* -G [/filter options/] [/format options/] | -P [/filter
options/] [/format options/]

*calcurse* -h | --status | -g | -i /file/ | -x[/file/] | --daemon

The first form shows how to invoke calcurse interactively; the remainder
is command line forms.

The second form shows queries (as opposed to interactive use). For
convenience, common queries have abbreviated forms shown in the third
line. All queries may be combined with filter options as well as format
options.

The fourth form shows operations on the calcurse data files, one for
display of entries, the other for removal of them.

The fifth form is miscellaneous help and support functions.

All details are in OPTIONS.

* DESCRIPTION
calcurse is a calendar and scheduling application for use in a terminal
session (terminal emulator). When invoked without options, calcurse
enters interactive mode; in most other cases, calcurse produces output
on the terminal and exits. It helps keeping track of events,
appointments and everyday tasks. Interactive mode is used when data are
entered or when already existing entries are inspected or edited. All
data are saved to disk as text files. Command line mode is used for
queries and administrative tasks and for automating tasks in scripts.

The interactive interface is based on ncurses and can be customized to
suit user preferences. Customization includes program behaviour as well
as visual appearance and key bindings, and is performed interactively;
the result is automatically saved to disk and loaded on every
invocation. Available actions are documented in an online help system. A
configurable notification system issues reminders of upcoming deadlines.

When leaving the interactive program, a background daemon may continue
running and issue reminders; it stops automatically when the interactive
mode is reentered.

This man page mainly describes the command-line mode. The following two
subsections contain some general descriptions of command line options
and usage.

** Input and Output Date Format
Many options require a /date/ argument, and query results per day are
set apart by a leading /date line/.

The input format of /date/ options and the output format of /date lines/
are taken from the configuration file (see FILES). The formats are set
in the "General Options" submenu in interactive mode. Particularly in
scripts it may be desirable that formats are independent of the local
user configuration. For this purpose use the options *--input-datefmt*
and *--output-datefmt*.

An input date consists of /date/, /month/ and /year/. Here /day/ must be
in the range 1-31 and /month/ in 1-12. Depending on the operating system
/year/ must be in the range 1902-2037 or 1902-?. Additionally, some
short forms are allowed with the obvious meaning: today, tomorrow,
yesterday, now and weekdays mon, ..., sun.

Optionally, a /date/ argument for a filter option (see Filter Options)
may be followed by a time-of-day specification in hours and minutes
(24-hour clock). The specification has the fixed format hh:mm (example:
"2018-12-1 20:30" when the input date format is the ISO standard
format). Note that the entire date specification must be quoted to
create one argument.

** Filter, format and day range options
These options do not accomplish anything in and of themselves. They
influence other options and are in a way opposites: filter options
affect the input to, format and day range options the output from
calcurse. Specifically, filter options limit what is loaded from the
data files into calcurse, day range options limit what is output (see
-Q), and format options describe how it is presented.

Filter options have effect on queries (*-Q* and query short-forms), grep
(*-G*), purge (*-P*) and export (*-x*). Format options have effect on
queries, grep and *--dump-imported*. Day range options have effect on
queries only.

* OPTIONS
Most options imply command line mode. Options compatible with
interactive mode are marked "(/also interactively/)".

*-a*, *--appointment*

#+begin_quote
  Print the appointments and events for the current day. Equivalent to
  *-Q --filter-type cal*.
#+end_quote

*-c* /file/, *--calendar* /file/

#+begin_quote
  (/also interactively/) Specify the calendar file to use. The default
  calendar is located at *<datadir>/apts* (see FILES). If /file/ is not
  an absolute path name, it is interpreted relative to the current
  working directory. The option has precedence over *-D*.
#+end_quote

*-C* /dir/, *--confdir* /dir/

#+begin_quote
  (/also interactively/) Specify the configuration directory to use. See
  section FILES for the default directory and the interaction with *-D*.
#+end_quote

*-D* /dir/, *--datadir* /dir/

#+begin_quote
  (/also interactively/) Specify the (data) directory to use. See
  section FILES for the default directory and the interaction with *-C*.
#+end_quote

*-d* /date|num/, *--day* /date|num/

#+begin_quote
  Print appointments and events for the given date or given range of
  days, depending on the argument format:

  #+begin_quote
    ·

    a /date/
  #+end_quote

  #+begin_quote
    ·

    a number /num/
  #+end_quote

  In the first case, appointments and events for /date/ are returned,
  while in the second case those for /num/ days are returned. Positive
  values of /num/ means the future, negative the past; the range either
  starts or ends with the current day. As an example calcurse -d 3
  displays appointments and events for today, tomorrow and the day after
  tomorrow, while calcurse -d -2 displays those for yesterday and today.
  The first form is equivalent to *-Q --filter-type cal --from* /date/,
  the second to *-Q --filter-type cal --days* /num/.
#+end_quote

*--daemon*

#+begin_quote
  Start calcurse in background mode; restart, if the daemon was already
  running. Usually done automatically by setting the configuration
  option daemon.enable in the /Notify/ submenu in interactive mode.
#+end_quote

*--days* /num/

#+begin_quote
  Specify the range of days when used with *-Q*. Can be combined with
  *--from*, but not with *--to*. Without *--from*, the first day of the
  range defaults to the current day. The number may be negative, see -Q
  --query.
#+end_quote

*--dump-imported*

#+begin_quote
  When importing items, print each newly created object to stdout.
  Format options can be used to specify which details are printed. See
  also Format Options.
#+end_quote

*--export-uid*

#+begin_quote
  When exporting items, add the hash of each item to the exported object
  as a UID property.
#+end_quote

*--from* /date/

#+begin_quote
  Specify the start date of the day range when used with *-Q*. When used
  without *-to* or *--days* the range is one day (the specified day),
  see -Q --query.
#+end_quote

*-F*, *--filter*

#+begin_quote
  Deprecated, see -P. Note that this option is for backward
  compatibility and not the same as *-P* (it does not use the invert
  filter option).
#+end_quote

*-g*, *--gc*

#+begin_quote
  Run the garbage collector for note files. The garbage collector
  removes files from the notes directory (see FILES) that are no longer
  linked to an item. Usually done automatically by setting the
  configuration option general.autogc in the /General Options/ submenu
  in interactive mode.
#+end_quote

*-G*, *--grep*

#+begin_quote
  Print appointments, events and TODO items in calcurse data file
  format.
#+end_quote

*-h*, *--help*

#+begin_quote
  Print a short help text describing the supported command-line options.
#+end_quote

*-i* /file/, *--import* /file/

#+begin_quote
  Import the icalendar data contained in /file/.
#+end_quote

*--input-datefmt* /format/

#+begin_quote
  For command line and script use only: override the configuration file
  setting of the option format.inputdate (/General Options/ submenu in
  interactive mode). A valid /format/ is any of 1, 2, 3, or 4, with 1 =
  mm/dd/yyyy, 2 = dd/mm/yyyy, 3 = yyyy/mm/dd, 4 = yyyy-mm-dd.
#+end_quote

*-l* /num/, *--limit* /num/

#+begin_quote
  Limit the number of results printed to /num/.
#+end_quote

*-n*, *--next*

#+begin_quote
  Print the first appointment within the next 24 hours. The printed time
  is the number of hours and minutes left before this appointment.
#+end_quote

*--output-datefmt* /format/

#+begin_quote
  For command line and script use only: override the configuration file
  setting of the option format.outputdate (/General Options/ submenu in
  interactive mode). A valid /format/ is any strftime(3) format string.
#+end_quote

*-P*, *--purge*

#+begin_quote
  Load items from the data files and save them back; the items are
  described by suitable filter options (see Filter Options). It may be
  used to drop specific items from the data files, see EXAMPLES.

  The matching items are (silently) /removed/ from the data files. Any
  combination of filter options, except --filter-invert, may be used in
  describing the items to be removed. The invert filter is used
  internally by the purge option, and its simultaneous use on the
  command line may result in unpredictable behaviour.

  /Warning:/ Be careful with this option, specifying the wrong filter
  options may result in data loss. It is highly recommended to test with
  *-G* first and fine-tune the filters to show the items to be removed.
  Then run the same command with -P instead of -G. In any case, make a
  backup of the data files in advance.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  (/also interactively/) Be quiet. Do not show system dialogs.
#+end_quote

*-Q*, *--query*

#+begin_quote
  Print all appointments and events in a given /range of days/ followed
  by all TODO items. The calendar part is displayed per day with a
  leading line containing the date and a trailing empty line (much like
  the calendar panel in interactive mode).

  The /day range/ defaults to the current day and is changed with the
  options *--from* and *--to*/*--days*. The range --from /a/ --to /z/
  includes both /a/ and /z/. The range --from /a/ --days /n/, includes
  /a/ as the first day, if /n/ is positive, or last day, if /n/ is
  negative.

  Day range has an effect on queries only.
#+end_quote

*-r*[/num/], *--range*[=/num/]

#+begin_quote
  Print appointments and events for /num/ number of days starting with
  the current day. If /num/ is left out, a range of 1 day is used. The
  number may be negative in which case the range is in the past, ending
  with the current day. Equivalent to *-Q --filter-type cal --days*
  /num/.
#+end_quote

*--read-only*

#+begin_quote
  (/also interactively/) Do not save configuration nor appointments and
  todos.

  /Warning:/ If you run calcurse interactively in read-only mode, all
  changes from that session will be lost without warning!
#+end_quote

*-s*[/date/], *--startday*[=/date/]

#+begin_quote
  Print events and appointments from the optional /date/; default is the
  current day. Equivalent to *-Q --filter-type cal --from* /date/.
#+end_quote

*-S* /regex/, *--search* /regex/

#+begin_quote
  When used with any of *-a*, *-d*, *-r*, *-s*, or *-t* print only the
  items having a description that matches the given regular expression.
  Equivalent to *-Q --filter-pattern* /regex/.
#+end_quote

*--status*

#+begin_quote
  Display the status of running instances of calcurse, interactive or
  background mode. The process pid is also printed.
#+end_quote

*-t*[/num/], *--todo*[=/num/]

#+begin_quote
  Print the *todo* list. If the optional number /num/ is given, then
  only uncompleted (open) todos having a priority equal to /num/ will be
  returned. The priority number must be between 1 (highest) and 9
  (lowest). It is also possible to specify 0 for the priority, in which
  case only completed (closed) tasks will be shown. Equivalent to *-Q
  --filter-type todo*, combined with *--filter-priority* and
  *--filter-completed* or *--filter-uncompleted*.
#+end_quote

*--to* /date/

#+begin_quote
  Specify the end date of the day range when used with *-Q*. When used
  without *--from* the start day is the current day. Cannot be combined
  with *--days*, see -Q --query.
#+end_quote

*-v*, *--version*

#+begin_quote
  Display *calcurse* version.
#+end_quote

*-x*[/format/], *--export*[=/format/]

#+begin_quote
  Export user data in the specified format. Events, appointments and
  todos are converted and echoed to stdout. Two formats are available:
  ical and pcal. The default /format/ is ical.
#+end_quote

* FILTER OPTIONS
Filter options have effect on queries (*-Q* and query short-forms), grep
(*-G*), purge (*-P*) and export (*-x*), see also options in the
DESCRIPTION section.

Several filter options may be given. For an item to be loaded into
calcurse it must match all filters. In other words, filters are
logically "and"-ed. The *--filter-type* option has a default value which
matches any item. All other filter options have no default value and
only apply when explicitly set.

The filter options fall into three groups: general, calendar, todo. The
/general filters/ apply to all items, the /calendar filters/ concern
start and end times and apply to appointments and events only, and the
/todo filters/ concern priority and completeness and apply to TODOs
only.

Outside the three groups is the invert filter.

*--filter-invert*

#+begin_quote
  Invert the combined effect of any other filters, i.e. load the items
  that do /not/ match them.
#+end_quote

** General filters
*--filter-type* /type/

#+begin_quote
  Include items that match /type/. The type value is a comma-separated
  list of type descriptions selected from event, apt, recur-event,
  recur-apt and todo. You can also use recur as a shorthand for
  recur-event,recur-apt and cal as a shorthand for event,apt,recur.
#+end_quote

*--filter-pattern* /pattern/

#+begin_quote
  Include items with a description that matches the pattern. The pattern
  is interpreted as an extended regular expression.
#+end_quote

*--filter-hash* /string/

#+begin_quote
  Include items with a hash starting with /string/. The filter can be
  negated by prepending an exclamation mark (!): include items with a
  hash string /not/ starting with /string/. For the (SHA1) hash of an
  item refer to Extended format specifiers.
#+end_quote

** Calendar filters
For filter options ending in *-from*, *-to*, *-after*, *-before* and
*-range*, start or end time is the filter criterion.

An event is an all-day appointment for which no times are displayed. The
start time of an event is the beginning of the event day (midnight), the
end time is the end of the event day (one second before next midnight).

The *-start-* options ending in *-from*, *-after* and *-range* refer to
the same filter criterion and cannot be used together. The same is the
case for options ending in *-to*, *-before* and *-range*. Similar
restrictions apply to *-end-* options.

Start and end times of a recurrent item refer to the very first
occurrence, not to those of any of the repetitions. If a recurrent item
meets the criterion, all of the repetitions are displayed in queries,
even though they may not meet the criterion. If they are unwanted, they
may be removed from the output with the day range options, although this
will also remove other items in that range.

*--filter-start-from* /date/

#+begin_quote
  Include items that start at or after a given date.
#+end_quote

*--filter-start-to* /date/

#+begin_quote
  Include items that start at or before a given date.
#+end_quote

*--filter-start-after* /date/

#+begin_quote
  Include items that start after a given date.
#+end_quote

*--filter-start-before* /date/

#+begin_quote
  Include items that start before a given date.
#+end_quote

*--filter-start-range* /range/

#+begin_quote
  Include items with a start date that belongs to a given range. A range
  consists of a start date and an end date, separated by a comma.
#+end_quote

*--filter-end-from* /date/

#+begin_quote
  Include items that end at or after a given date.
#+end_quote

*--filter-end-to* /date/

#+begin_quote
  Include items that end at or before a given date.
#+end_quote

*--filter-end-after* /date/

#+begin_quote
  Include items that end after a given date.
#+end_quote

*--filter-end-before* /date/

#+begin_quote
  Include items that end before a given date.
#+end_quote

*--filter-end-range* /range/

#+begin_quote
  Include items with an end date that belongs to a given range. A range
  consists of a start date and an end date, separated by a comma.
#+end_quote

** Todo filters
*--filter-priority* /priority/

#+begin_quote
  Include TODO items with a given priority.
#+end_quote

*--filter-completed*

#+begin_quote
  Include completed TODO items.
#+end_quote

*--filter-uncompleted*

#+begin_quote
  Include uncompleted TODO items.
#+end_quote

* FORMAT OPTIONS
Format options have effect on queries, grep and *--dump-imported*.

The options specify a format for appointments, recurring appointments,
events, recurring events or todo items, respectively.

*--format-apt* /format/, *--format-recur-apt* /format/, *--format-event*
/format/, *--format-recur-event* /format/, *--format-todo* /format/

#+begin_quote
  The /format/ argument is a string composed of printf-style format
  specifiers, which are replaced as explained below, and ordinary
  characters, which are copied to stdout without modification. Each
  option has a default /format/ string which is used when the option is
  not given. Default strings are described in Default Format Strings.

  /Note/: Use of a format option requires explicit formatting of field
  separation and line spacing.
#+end_quote

** Default format strings
Each specifier is introduced by a % followed by a character which tells
what to print. The available specifiers depend on the item type. Times
are printed as hours and minutes (hh:mm) unless otherwise noted; time
formats can be changed with extended specifiers.

For each format option there is a default format string which is used
when the option is not given. In query results the default format
options are:

#+begin_quote
  #+begin_example
    --format-apt " - %S -> %E\n\t%m\n"
    --format-recur-apt " - %S -> %E\n\t%m\n"
    --format-event " * %m\n"
    --format-recur-event " * %m\n"
    --format-todo "%p. %m\n"
  #+end_example
#+end_quote

In all other cases (grep and dump-imported) the default format string is
"%(raw)".

** Appointments
*%d*

#+begin_quote
  Print the duration of the appointment in seconds
#+end_quote

*%e*

#+begin_quote
  Print the end time of the appointment as the Unix time in seconds
#+end_quote

*%E*

#+begin_quote
  Print the end time of the appointment or the marker ..:.. if it ends
  after midnight
#+end_quote

*%m*

#+begin_quote
  Print the description of the item
#+end_quote

*%n*

#+begin_quote
  Print the name of the note file belonging to the item
#+end_quote

*%N*

#+begin_quote
  Print the note belonging to the item
#+end_quote

*%r*

#+begin_quote
  Print the remaining time before the appointment
#+end_quote

*%s*

#+begin_quote
  Print the start time of the appointment as the Unix time in seconds
#+end_quote

*%S*

#+begin_quote
  Print the start time of the appointment or the marker ..:.. if it
  begins before midnight
#+end_quote

** Events
*%m*

#+begin_quote
  Print the description of the item
#+end_quote

*%n*

#+begin_quote
  Print the name of the note file belonging to the item
#+end_quote

*%N*

#+begin_quote
  Print the note belonging to the item
#+end_quote

** Todo items
*%m*

#+begin_quote
  Print the description of the item
#+end_quote

*%n*

#+begin_quote
  Print the name of the note file belonging to the item
#+end_quote

*%N*

#+begin_quote
  Print the note belonging to the item
#+end_quote

*%p*

#+begin_quote
  Print the priority of the item
#+end_quote

** Extended format specifiers
Extended format specifiers can be used to control the printing of times
for some of the single-letter specifiers. Additionally there are two
specifiers that do not have any corresponding short form and are
intended for use in scripting.

*%(duration*[*:*/format/]*)*

#+begin_quote
  extended form of *%d*
#+end_quote

*%(remaining*[*:*/format/]*)*

#+begin_quote
  extended form of *%r*

  /format/ may contain any of the strftime(3) specifiers *%d*, *%H*,
  *%M* or *%S* with the following modifications: 1) days are not limited
  to the "calendar" values 0-31 (hours, minutes and seconds are "clock"
  values, but see *E* in the following) 2) each number is by default
  padded with zeros to two decimal places, 3) the *%* character may be
  followed by one or two optional flags: *-*, which suppresses the
  zero-padding, *E*, which will suppress the "clock" limits on *%H*,
  *%M* and *%S*; if both are used, *-* must precede *E*, 4) no other
  flags or width specifications are supported
#+end_quote

*%(start*[*:*/format/]*)*

#+begin_quote
  extended form of *%s*
#+end_quote

*%(end*[*:*/format/]*)*

#+begin_quote
  extended form of *%e*

  /format/ may be any strftime(3) format specifier or one of the strings
  *epoch* or *default*; the former is equivalent to the (calcurse)
  specifiers *%s* and *%e* (seconds since the Epoch); the latter is
  equivalent to the (calcurse) specifiers *%S* and *%E* or the
  (strftime) format string *%H:%M*, except that the continuation marker
  ..:.. is printed if the start/end time belongs to another day
#+end_quote

*%(raw)*

#+begin_quote
  the text file format of an item as saved on disk; the default format
  for the grep and dump-imported options; can be used with all format
  options
#+end_quote

*%(hash)*

#+begin_quote
  the (SHA1) hash of the above; can be used with all format options
#+end_quote

* EXAMPLES
calcurse -d tomorrow

#+begin_quote
  Display the calendar for tomorrow (same as calcurse -Q --filter-type
  cal --from tomorrow).
#+end_quote

calcurse -d friday

#+begin_quote
  Display the calendar for the upcoming friday.
#+end_quote

calcurse -d 7

#+begin_quote
  Display the calendar for the next seven days (same as calcurse -Q
  -filter-type cal --days 7).
#+end_quote

calcurse -r7 --format-apt " - %S -> %E\n\t%m\n%N"

#+begin_quote
  Print appointments and events for the next seven days. Also, print the
  notes attached to each regular appointment.
#+end_quote

calcurse -r7 --format-apt " - %m (%S to %E)\n" --format-recur-apt " - %m
(%S to %E)\n"

#+begin_quote
  Print appointments and events for the next seven days and use a custom
  format for (recurrent) appointments:

  - Some appointment (18:30 to 21:30)
#+end_quote

calcurse -t --format-todo "(%p) %m\n"

#+begin_quote
  List all todo items and put parentheses around the priority
  specifiers.
#+end_quote

If the calcurse data files contain many entries which are no longer
needed or wanted, they can, of course, be removed interactively. If
there are many, it can be a tedious task and may be done better as in
the following two examples.

calcurse --input-datefmt 4 -G --filter-start-before 2015-1-1

#+begin_quote
  List event and appointment entries in the data files with a start time
  before 1 January 2015, and all TODO entries.

  *Purge*. When -G is replaced by -P, those entries are removed. This
  may remove recurring items that have occurrences after 1 January 2015.
#+end_quote

calcurse --input-datefmt 1 -G --filter-start-from 11/1/2015
--filter-type event,apt

#+begin_quote
  List (ordinary) event and appointment entries with a start time of 1
  November 2015 or later.
#+end_quote

calcurse -G --filter-type apt --format-apt "%(hash) %m\n"

#+begin_quote
  For each appointment list the SHA1 hash of the data file entry
  followed by the description.
#+end_quote

calcurse -G --filter-type apt --format-apt
"%(duration:%d/%EH/%EM)\t%m\n"

#+begin_quote
  For each appointment list the (total) duration as either days, hours
  or minutes followed by the description.
#+end_quote

calcurse -G --filter-type apt --format-apt "%(start:%c) %(duration:%d
%H:%M)\t%m\n"

#+begin_quote
  For each appointment list the start time in a localized standard
  format, followed by the duration in days, hours and minutes, followed
  by the description.
#+end_quote

* FILES
The following structure is created by default in your home directory the
first time calcurse is run without any options:

#+begin_quote
  #+begin_example
    $XDG_DATA_HOME/calcurse/        $XDG_CONFIG_HOME/calcurse/
                      |___apts                          |___conf
                      |___notes/                        |___hooks/
                      |___todo                          |___keys
  #+end_example
#+end_quote

$XDG_DATA_HOME defaults to $HOME/.local/share and $XDG_CONFIG_HOME
defaults to $HOME/.config.

The files are of two different kinds: data and configuration. The data
files constitute the calcurse database and are independent of the
calcurse release version; the configuration files depend on the calcurse
version although backwards compatibility is always aimed at.

** Data files
The calendar file apts contains all of the user's appointments and
events, and the todo file contains the todo list. The notes subdirectory
contains the notes which are attached to appointments, events or todos.
One text file is created per note, whose name is the SHA1 message digest
of the note itself.

The (hidden) lock files of the calcurse (.calcurse.pid) and daemon
(.daemon.log) programs are present when they are running. If daemon log
activity has been enabled in the notification configuration menu, the
file daemon.log is present.

An alternative calendar file may be specified with the *-c* option.

** Configuration files
The conf file contains the user configuration and the keys file the
user-defined key bindings. The hooks directory contains user-supplied
scripts, see Hooks.

** Directory configuration
An alternative directory for /both/ the configuration files and the data
directory may be specified with the *-D* option.

An alternative directory for the configuration files /only/ may be
specified with the *-C* option; in that case data files are either in
the default directory or in the directory specified by *-D*. If both
*-D* and *-C* are present, configuration files in the data directory, if
any, are ignored.

If $HOME/.calcurse exists, then it will be used as the default for both
the data directory and the configuration directory.

#+begin_quote
  #+begin_example
    <datadir>      <confdir>
         |             |
         |__ apts      |___ conf
         |__ todo      |___ keys
         |__ notes/    |___ hooks/

    defaults:
         <datadir>: $XDG_DATA_HOME/calcurse ($HOME/.local/share/calcurse)
         <confdir>: $XDG_CONFIG_HOME/calcurse ($HOME/.config/calcurse)
         both: $HOME/.calcurse (only if it exists)
  #+end_example
#+end_quote

calcurse may switch between two configuration setups, but still access
the same data files e.g. with:

#+begin_quote
  #+begin_example
    $ calcurse

    $ calcurse -C "$HOME/.config/calcurse/config"
  #+end_example
#+end_quote

** Hooks
Scripts placed in <confdir>/calcurse/hooks/ (see Directory
configuration) trigger actions at certain events. To enable a hook, add
a script with one of the following names to this directory. Also make
sure the script is executable.

*pre-load*

#+begin_quote
  Executed before the data files are loaded.
#+end_quote

*post-load*

#+begin_quote
  Executed after the data files are loaded.
#+end_quote

*pre-save*

#+begin_quote
  Executed before the data files are saved.
#+end_quote

*post-save*

#+begin_quote
  Executed after the data files are saved.
#+end_quote

Some examples can be found in the contrib/hooks/ directory of the
calcurse source tree.

* ENVIRONMENT
A few environment variables affect how calcurse operates.

*CALCURSE_EDITOR*, *VISUAL*, *EDITOR*

#+begin_quote
  Specifies the external editor to use for writing notes. They are tried
  in the order listed until one is found. If none of them are set, vi is
  used.
#+end_quote

*CALCURSE_PAGER*, *PAGER*

#+begin_quote
  Specifies - as for the editor - the default viewer to be used for
  reading notes. Default is less.
#+end_quote

*MERGETOOL*

#+begin_quote
  Tool used to merge two files to solve a save conflict. Default is
  vimdiff. The program is called with two file names as the only
  arguments.
#+end_quote

See also FILES.

* BUGS
If you find a bug, please send a report to bugs@calcurse.org, or, if you
are a Github user, raise an issue at https://github.com/lfos/calcurse.

* SEE ALSO
The ical specification (rfc2445) can be found at:
http://tools.ietf.org/html/rfc2445

The pcal project page: http://pcal.sourceforge.net/

calcurse home page: http://calcurse.org/

calcurse at GitHub: https://github.com/lfos/calcurse

The complete manual, maintained in html format, can be found in the doc/
directory of the source package, or at:
http://calcurse.org/files/manual.html

* AUTHORS

#+begin_quote
  ·

  *Frederic Culot* <frederic@culot.org>
#+end_quote

#+begin_quote
  ·

  *Lukas Fleischer* <lfleischer@calcurse.org>
#+end_quote

* COPYRIGHT
Copyright (c) 2004-2020 calcurse Development Team. This software is
released under the BSD License.
