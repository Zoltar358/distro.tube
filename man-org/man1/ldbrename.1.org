#+TITLE: Man1 - ldbrename.1
#+DESCRIPTION: Linux manpage for ldbrename.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldbrename - Edit LDB databases using your favorite editor

* SYNOPSIS
*ldbrename* [-h] [-o options] {olddn} {newdn}

* DESCRIPTION
ldbrename is a utility that allows you to rename trees in an LDB
database based by DN. This utility takes two arguments: the original DN
name of the top element and the DN to change it to.

* OPTIONS
-h

#+begin_quote
  Show list of available options.
#+end_quote

-H <ldb-url>

#+begin_quote
  LDB URL to connect to. See ldb(3) for details.
#+end_quote

-o options

#+begin_quote
  Extra ldb options, such as modules.
#+end_quote

* ENVIRONMENT
LDB_URL

#+begin_quote
  LDB URL to connect to (can be overridden by using the -H command-line
  option.)
#+end_quote

* VERSION
This man page is correct for version 1.1 of LDB.

* SEE ALSO
ldb(3), ldbmodify, ldbdel, ldif(5)

* AUTHOR
ldb was written by *Andrew Tridgell*[1].

If you wish to report a problem or make a suggestion then please see the
*http://ldb.samba.org/* web site for current contact and maintainer
information.

This manpage was written by Jelmer Vernooij.

* NOTES
-  1. :: Andrew Tridgell

  https://www.samba.org/~tridge/
