#+TITLE: Man1 - remmina-file-wrapper.1
#+DESCRIPTION: Linux manpage for remmina-file-wrapper.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Wrapper script to execute Remmina with protocol handlers.
