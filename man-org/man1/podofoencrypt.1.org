#+TITLE: Man1 - podofoencrypt.1
#+DESCRIPTION: Linux manpage for podofoencrypt.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
podofoencrypt - encrypt PDF files and set PDF security settings

* SYNOPSIS
*podofoencrypt* [--rc4v1] [--rc4v2] [--aes] [-u <userpassword>] -o
<ownerpassword> <inputfile> <outputfile>

* DESCRIPTION
*podofoencrypt* is one of the command line tools from the PoDoFo library
that provide several useful operations to work with PDF files. It can
encrypt PDF files using RC4 or AES encoding and can set PDF security
settings.

* OPTIONS
*--help*

#+begin_quote
  Display the help text
#+end_quote

Algorithm:

#+begin_quote
  *--rc4v1*

  #+begin_quote
    Use rc4v1 encryption
  #+end_quote

  *--rc4v2*

  #+begin_quote
    Use rc4v2 encryption (Default value)
  #+end_quote

  *--aes*

  #+begin_quote
    Use aes encryption (currently not supported)
  #+end_quote
#+end_quote

Passwords:

#+begin_quote
  *-u <password>*

  #+begin_quote
    An optional user password
  #+end_quote

  *-o <password>*

  #+begin_quote
    The required owner password
  #+end_quote
#+end_quote

Permissions:

#+begin_quote
  *--print*

  #+begin_quote
    Allow printing the document
  #+end_quote

  *--edit*

  #+begin_quote
    Allow modification of the document besides annotations, form fields
    or changing pages
  #+end_quote

  *--copy*

  #+begin_quote
    Allow extraction of text and graphics
  #+end_quote

  *--editnotes*

  #+begin_quote
    Add or modify text annotations or form fields (if
    ePdfPermissions_Edit is set also allow the creation of interactive
    form fields including signature)
  #+end_quote

  *--fillandsign*

  #+begin_quote
    Fill in existing form or signature fields
  #+end_quote

  *--accessible*

  #+begin_quote
    Extract text and graphics to support user with disabillities
  #+end_quote

  *--assemble*

  #+begin_quote
    Assemble the document: insert, create, rotate delete pages or add
    bookmarks
  #+end_quote

  *--highprint *

  #+begin_quote
    Print a high resolution version of the document
  #+end_quote
#+end_quote

* SEE ALSO
*podofobox*(1), *podofocolor*(1), *podofocountpages*(1),
*podofocrop*(1), *podofogc*(1), *podofoimg2pdf*(1),
*podofoimgextract*(1), *podofoincrementalupdates*(1), *podofoimpose*(1),
*podofomerge*(1), *podofopages*(1), *podofopdfinfo*(1),
*podofotxt2pdf*(1), *podofotxtextract*(1), *podofouncompress*(1),
*podofoxmp*(1)

* AUTHORS
PoDoFo is written by Dominik Seichter <domseichter@web.de> and others.

This manual page was written by Oleksandr Moskalenko <malex@debian.org>
for the Debian Project (but may be used by others).
