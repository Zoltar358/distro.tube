#+TITLE: Man1 - perlopenbsd.1perl
#+DESCRIPTION: Linux manpage for perlopenbsd.1perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
perlopenbsd - Perl version 5 on OpenBSD systems

* DESCRIPTION
This document describes various features of OpenBSD that will affect how
Perl version 5 (hereafter just Perl) is compiled and/or runs.

** OpenBSD core dumps from getprotobyname_r and getservbyname_r with
ithreads
When Perl is configured to use ithreads, it will use re-entrant library
calls in preference to non-re-entrant versions. There is an
incompatibility in OpenBSD's =getprotobyname_r= and =getservbyname_r=
function in versions 3.7 and later that will cause a SEGV when called
without doing a =bzero= on their return structs prior to calling these
functions. Current Perl's should handle this problem correctly. Older
threaded Perls (5.8.6 or earlier) will run into this problem. If you
want to run a threaded Perl on OpenBSD 3.7 or higher, you will need to
upgrade to at least Perl 5.8.7.

* AUTHOR
Steve Peters <steve@fisharerojo.org>

Please report any errors, updates, or suggestions to
<https://github.com/Perl/perl5/issues>.
