#+TITLE: Man1 - ppmrelief.1
#+DESCRIPTION: Linux manpage for ppmrelief.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmrelief - run a Laplacian relief filter on a PPM image

* SYNOPSIS
*ppmrelief*

[/ppmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmrelief* reads a PPM image as input, does a Laplacian relief filter,
and writes a PPM image as output.

The Laplacian relief filter is described in 'Beyond Photography' by
Holzmann, equation 3.19. It's a sort of edge-detection.

* SEE ALSO
*pgmbentley*(1) , *pgmoil*(1) , *ppm*(5)

* AUTHOR
Copyright (C) 1990 by Wilson Bent (/whb@hoh-2.att.com/)
