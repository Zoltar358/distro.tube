#+TITLE: Man1 - i3-sensible-pager.1
#+DESCRIPTION: Linux manpage for i3-sensible-pager.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
i3-sensible-pager - launches $PAGER with fallbacks

* SYNOPSIS
i3-sensible-pager [arguments]

* DESCRIPTION
i3-sensible-pager is used by i3-nagbar(1) when you click on the view
button.

It tries to start one of the following (in that order):

#+begin_quote
  ·

  $PAGER
#+end_quote

#+begin_quote
  ·

  less
#+end_quote

#+begin_quote
  ·

  most
#+end_quote

#+begin_quote
  ·

  w3m
#+end_quote

#+begin_quote
  ·

  i3-sensible-editor(1)
#+end_quote

Please don't complain about the order: If the user has any preference,
they will have $PAGER set.

* SEE ALSO
i3(1)

* AUTHOR
Michael Stapelberg and contributors
