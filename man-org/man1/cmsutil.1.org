#+TITLE: Man1 - cmsutil.1
#+DESCRIPTION: Linux manpage for cmsutil.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cmsutil - Performs basic cryptograpic operations, such as encryption and
decryption, on Cryptographic Message Syntax (CMS) messages.

* SYNOPSIS
*cmsutil* [/options/] [[/arguments/]]

* STATUS
This documentation is still work in progress. Please contribute to the
initial review in *Mozilla NSS bug 836477*[1]

* DESCRIPTION
The *cmsutil* command-line uses the S/MIME Toolkit to perform basic
operations, such as encryption and decryption, on Cryptographic Message
Syntax (CMS) messages.

To run cmsutil, type the command cmsutil option [arguments] where option
and arguments are combinations of the options and arguments listed in
the following section. Each command takes one option. Each option may
take zero or more arguments. To see a usage string, issue the command
without options.

* OPTIONS AND ARGUMENTS
*Options*

Options specify an action. Option arguments modify an action. The
options and arguments for the cmsutil command are defined as follows:

-C

#+begin_quote
  Encrypt a message.
#+end_quote

-D

#+begin_quote
  Decode a message.
#+end_quote

-E

#+begin_quote
  Envelope a message.
#+end_quote

-O

#+begin_quote
  Create a certificates-only message.
#+end_quote

-S

#+begin_quote
  Sign a message.
#+end_quote

*Arguments*

Option arguments modify an action.

-b

#+begin_quote
  Decode a batch of files named in infile.
#+end_quote

-c content

#+begin_quote
  Use this detached content (decode only).
#+end_quote

-d dbdir

#+begin_quote
  Specify the key/certificate database directory (default is ".")
#+end_quote

-e envfile

#+begin_quote
  Specify a file containing an enveloped message for a set of recipients
  to which you would like to send an encrypted message. If this is the
  first encrypted message for that set of recipients, a new enveloped
  message will be created that you can then use for future messages
  (encrypt only).
#+end_quote

-f pwfile

#+begin_quote
  Use password file to set password on all PKCS#11 tokens.
#+end_quote

-G

#+begin_quote
  Include a signing time attribute (sign only).
#+end_quote

-H hash

#+begin_quote
  Use specified hash algorithm (default:SHA1).
#+end_quote

-h num

#+begin_quote
  Generate email headers with info about CMS message (decode only).
#+end_quote

-i infile

#+begin_quote
  Use infile as a source of data (default is stdin).
#+end_quote

-k

#+begin_quote
  Keep decoded encryption certs in permanent cert db.
#+end_quote

-N nickname

#+begin_quote
  Specify nickname of certificate to sign with (sign only).
#+end_quote

-n

#+begin_quote
  Suppress output of contents (decode only).
#+end_quote

-o outfile

#+begin_quote
  Use outfile as a destination of data (default is stdout).
#+end_quote

-P

#+begin_quote
  Include an S/MIME capabilities attribute.
#+end_quote

-p password

#+begin_quote
  Use password as key database password.
#+end_quote

-r recipient1,recipient2, ...

#+begin_quote
  Specify list of recipients (email addresses) for an encrypted or
  enveloped message. For certificates-only message, list of certificates
  to send.
#+end_quote

-T

#+begin_quote
  Suppress content in CMS message (sign only).
#+end_quote

-u certusage

#+begin_quote
  Set type of cert usage (default is certUsageEmailSigner).
#+end_quote

-v

#+begin_quote
  Print debugging information.
#+end_quote

-Y ekprefnick

#+begin_quote
  Specify an encryption key preference by nickname.
#+end_quote

* USAGE
Encrypt Example

#+begin_quote
  #+begin_example
    cmsutil -C [-i infile] [-o outfile] [-d dbdir] [-p password] -r "recipient1,recipient2, . . ." -e envfile
          
  #+end_example
#+end_quote

Decode Example

#+begin_quote
  #+begin_example
    cmsutil -D [-i infile] [-o outfile] [-d dbdir] [-p password] [-c content] [-n] [-h num]
          
  #+end_example
#+end_quote

Envelope Example

#+begin_quote
  #+begin_example
    cmsutil -E [-i infile] [-o outfile] [-d dbdir] [-p password] -r "recipient1,recipient2, ..."
          
  #+end_example
#+end_quote

Certificate-only Example

#+begin_quote
  #+begin_example
    cmsutil -O [-i infile] [-o outfile] [-d dbdir] [-p password] -r "cert1,cert2, . . ."
          
  #+end_example
#+end_quote

Sign Message Example

#+begin_quote
  #+begin_example
    cmsutil -S [-i infile] [-o outfile] [-d dbdir] [-p password] -N nickname[-TGP] [-Y ekprefnick]
          
  #+end_example
#+end_quote

* SEE ALSO
certutil(1)

* ADDITIONAL RESOURCES
For information about NSS and other tools related to NSS (like JSS),
check out the NSS project wiki at
*http://www.mozilla.org/projects/security/pki/nss/*. The NSS site
relates directly to NSS code changes and releases.

Mailing lists: https://lists.mozilla.org/listinfo/dev-tech-crypto

IRC: Freenode at #dogtag-pki

* AUTHORS
The NSS tools were written and maintained by developers with Netscape,
Red Hat, Sun, Oracle, Mozilla, and Google.

Authors: Elio Maldonado <emaldona@redhat.com>, Deon Lackey
<dlackey@redhat.com>.

* LICENSE
Licensed under the Mozilla Public License, v. 2.0. If a copy of the MPL
was not distributed with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.

* NOTES
-  1. :: Mozilla NSS bug 836477

  https://bugzilla.mozilla.org/show_bug.cgi?id=836477
