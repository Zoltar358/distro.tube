#+TITLE: Man1 - jjs-jre8.1
#+DESCRIPTION: Linux manpage for jjs-jre8.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
jjs - Invokes the Nashorn engine.

* SYNOPSIS

#+begin_quote
  #+begin_example
    jjs [options] [script-files] [-- arguments]
  #+end_example
#+end_quote

/options/

#+begin_quote
  One or more options of the *jjs* command, separated by spaces. For
  more information, see Options.
#+end_quote

/script-files/

#+begin_quote
  One or more script files which you want to interpret using Nashorn,
  separated by spaces. If no files are specified, an interactive shell
  is started.
#+end_quote

/arguments/

#+begin_quote
  All values after the double hyphen marker (*--*) are passed through to
  the script or the interactive shell as arguments. These values can be
  accessed by using the *arguments* property (see Example 3).
#+end_quote

* DESCRIPTION
The *jjs* command-line tool is used to invoke the Nashorn engine. You
can use it to interpret one or several script files, or to run an
interactive shell.

* OPTIONS
The options of the *jjs* command control the conditions under which
scripts are interpreted by Nashorn.

-cp /path/\\
-classpath /path/

#+begin_quote
  Specifies the path to the supporting class files To set multiple
  paths, the option can be repeated, or you can separate each path with
  a colon (:).
#+end_quote

-D/name/=/value/

#+begin_quote
  Sets a system property to be passed to the script by assigning a value
  to a property name. The following example shows how to invoke Nashorn
  in interactive mode and assign *myValue* to the property named
  *myKey*:

  #+begin_quote
    #+begin_example
      >> jjs -DmyKey=myValue
      jjs> java.lang.System.getProperty("myKey")
      myValue
      jjs>
       
    #+end_example
  #+end_quote

  This option can be repeated to set multiple properties.
#+end_quote

-doe\\
--dump-on-error

#+begin_quote
  Provides a full stack trace when an error occurs. By default, only a
  brief error message is printed.
#+end_quote

-fv\\
--fullversion

#+begin_quote
  Prints the full Nashorn version string.
#+end_quote

-fx

#+begin_quote
  Launches the script as a JavaFX application.
#+end_quote

-h\\
-help

#+begin_quote
  Prints the list of options and their descriptions.
#+end_quote

--language=[es5]

#+begin_quote
  Specifies the ECMAScript language version. The default version is ES5.
#+end_quote

-ot\\
--optimistic-types=[true|false]

#+begin_quote
  Enables or disables optimistic type assumptions with deoptimizing
  recompilation. Running with optimistic types will yield higher final
  speed, but may increase warmup time.
#+end_quote

-scripting

#+begin_quote
  Enables shell scripting features.
#+end_quote

-strict

#+begin_quote
  Enables strict mode, which enforces stronger adherence to the standard
  (ECMAScript Edition 5.1), making it easier to detect common coding
  errors.
#+end_quote

-t=/zone/\\
-timezone=/zone/

#+begin_quote
  Sets the specified time zone for script execution. It overrides the
  time zone set in the OS and used by the *Date* object.
#+end_quote

-v\\
-version

#+begin_quote
  Prints the Nashorn version string.
#+end_quote

* EXAMPLES
*Example 1 *Running a Script with Nashorn

#+begin_quote

  #+begin_quote
    #+begin_example
      jjs script.js
       
    #+end_example
  #+end_quote
#+end_quote

*Example 2 *Running Nashorn in Interactive Mode

#+begin_quote

  #+begin_quote
    #+begin_example
      >> jjs
      jjs> println("Hello, World!")
      Hello, World!
      jjs> quit()
      >>
       
    #+end_example
  #+end_quote
#+end_quote

*Example 3 *Passing Arguments to Nashorn

#+begin_quote

  #+begin_quote
    #+begin_example
      >> jjs -- a b c
      jjs> arguments.join(", ")
      a, b, c
      jjs>
       
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*jrunscript*\\
