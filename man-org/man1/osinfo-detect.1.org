#+TITLE: Man1 - osinfo-detect.1
#+DESCRIPTION: Linux manpage for osinfo-detect.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
osinfo-detect - Detect the operating system on installable media or
trees

* SYNOPSIS
osinfo-detect [OPTIONS...] PATH|URI

* DESCRIPTION
Examine the =PATH= or =URI= to determine what (if any) operating system
it is for, and whether it is installable or is a Live image. By default
=PATH= or =URI= will be interpreted as pointing to ISO media. To request
examination of an install tree instead, the option *--type=tree* should
be given.

The output information is formatted for humans;

* OPTIONS
- --format=plain :: 

- --type=media|tree :: 

Switch between looking for CD/DVD ISO media (*media*, the default) or
install trees (*tree*)

* EXIT STATUS
The exit status will be 0 if an operating system was detected, or 1 if
none was found.

* AUTHORS
Zeeshan Ali (Khattak) <zeeshanak@gnome.org>, Daniel P. Berrange
<berrange@redhat.com>

* COPYRIGHT
Copyright (C) 2011-2012 Red Hat, Inc.

* LICENSE
=osinfo-detect= is distributed under the termsof the GNU LGPL v2
license. This is free software; see the source for copying conditions.
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE
