#+TITLE: Man1 - gtk4-launch.1
#+DESCRIPTION: Linux manpage for gtk4-launch.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gtk4-launch - Launch an application

* SYNOPSIS
*gtk4-launch* [OPTION...] APPLICATION [URI...]

* DESCRIPTION
*gtk4-launch* launches an application using the given name. The
application is started with proper startup notification on a default
display, unless specified otherwise.

*gtk4-launch* takes at least one argument, the name of the application
to launch. The name should match application desktop file name, as
residing in the applications subdirectories of the XDG data directories,
with or without the .desktop suffix.

If called with more than one argument, the rest of them besides the
application name are considered URI locations and are passed as
arguments to the launched application.

* OPTIONS
The following options are understood:

*-?*, *--help*

#+begin_quote
  Prints a short help text and exits.
#+end_quote

*--version*

#+begin_quote
  Prints the program version and exits.
#+end_quote

* ENVIRONMENT
Some environment variables affect the behavior of *gtk4-launch*.

*XDG_DATA_HOME*, *XDG_DATA_DIRS*

#+begin_quote
  The environment variables specifying the XDG dta directories.
#+end_quote
