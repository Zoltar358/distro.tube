#+TITLE: Man1 - trust.1
#+DESCRIPTION: Linux manpage for trust.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
trust - Tool for operating on the trust policy store

* SYNOPSIS
*trust list*

*trust extract* --filter=<what> --format=<type> /path/to/destination

*trust anchor* /path/to/certificate.crt

*trust dump*

* DESCRIPTION
*trust* is a command line tool to examine and modify the shared trust
policy store.

See the various sub commands below. The following global options can be
used:

*-v, --verbose*

#+begin_quote
  Run in verbose mode with debug output.
#+end_quote

*-q, --quiet*

#+begin_quote
  Run in quiet mode without warning or failure messages.
#+end_quote

* LIST
List trust policy store items.

#+begin_quote
  #+begin_example
    $ trust list
  #+end_example
#+end_quote

List information about the various items in the trust policy store. Each
item is listed with its PKCS#11 URI and some descriptive information.

You can specify the following options to control what to list.

*--filter=<what>*

#+begin_quote
  Specifies what certificates to extract. You can specify the following
  values:

  *ca-anchors*

  #+begin_quote
    Certificate anchors
  #+end_quote

  *trust-policy*

  #+begin_quote
    Anchors and blocklist (default)
  #+end_quote

  *blocklist*

  #+begin_quote
    Distrusted certificates
  #+end_quote

  *certificates*

  #+begin_quote
    All certificates
  #+end_quote

  *pkcs11:object=xx*

  #+begin_quote
    A PKCS#11 URI to filter with
  #+end_quote

  If an output format is chosen that cannot support type what has been
  specified by the filter, a message will be printed.

  None of the available formats support storage of blocklist entries
  that do not contain a full certificate. Thus any certificates
  distrusted by their issuer and serial number alone, are not included
  in the extracted blocklist.
#+end_quote

*--purpose=<usage>*

#+begin_quote
  Limit to certificates usable for the given purpose You can specify one
  of the following values:

  *server-auth*

  #+begin_quote
    For authenticating servers
  #+end_quote

  *client-auth*

  #+begin_quote
    For authenticating clients
  #+end_quote

  *email*

  #+begin_quote
    For email protection
  #+end_quote

  *code-signing*

  #+begin_quote
    For authenticated signed code
  #+end_quote

  *1.2.3.4.5...*

  #+begin_quote
    An arbitrary purpose OID
  #+end_quote
#+end_quote

* ANCHOR
Store or remove trust anchors.

#+begin_quote
  #+begin_example
    $ trust anchor /path/to/certificate.crt
    $ trust anchor --remove /path/to/certificate.crt
    $ trust anchor --remove "pkcs11:id=%AA%BB%CC%DD%EE;type=cert"
  #+end_example
#+end_quote

Store or remove trust anchors in the trust policy store. These are
usually root certificate authorities.

Specify either the *--store* or *--remove* operations. If no operation
is specified then *--store* is assumed.

When storing, one or more certificate files are expected on the command
line. These are stored as anchors, unless they are already present.

When removing an anchor, either specify certificate files or PKCS#11
URIs on the command line. Matching anchors will be removed.

It may be that this command needs to be run as root in order to modify
the system trust policy store, if no user specific store is available.

You can specify the following options.

*--remove*

#+begin_quote
  Remove one or more anchors from the trust policy store. Specify
  certificate files or PKCS#11 URIs on the command line.
#+end_quote

*--store*

#+begin_quote
  Store one or more anchors to the trust policy store. Specify
  certificate files on the command line.
#+end_quote

* EXTRACT
Extract trust policy from the shared trust policy store.

#+begin_quote
  #+begin_example
    $ trust extract --format=x509-directory --filter=ca-anchors /path/to/directory
  #+end_example
#+end_quote

You can specify the following options to control what to extract. The
*--filter* and *--format* arguments should be specified. By default this
command will not overwrite the destination file or directory.

*--comment*

#+begin_quote
  Add identifying comments to PEM bundle output files before each
  certificate.
#+end_quote

*--filter=<what>*

#+begin_quote
  Specifies what certificates to extract. You can specify the following
  values:

  *ca-anchors*

  #+begin_quote
    Certificate anchors (default)
  #+end_quote

  *trust-policy*

  #+begin_quote
    Anchors and blocklist
  #+end_quote

  *blocklist*

  #+begin_quote
    Distrusted certificates
  #+end_quote

  *certificates*

  #+begin_quote
    All certificates
  #+end_quote

  *pkcs11:object=xx*

  #+begin_quote
    A PKCS#11 URI
  #+end_quote

  If an output format is chosen that cannot support type what has been
  specified by the filter, a message will be printed.

  None of the available formats support storage of blocklist entries
  that do not contain a full certificate. Thus any certificates
  distrusted by their issuer and serial number alone, are not included
  in the extracted blocklist.
#+end_quote

*--format=<type>*

#+begin_quote
  The format of the destination file or directory. You can specify one
  of the following values:

  *x509-file*

  #+begin_quote
    DER X.509 certificate file
  #+end_quote

  *x509-directory*

  #+begin_quote
    directory of X.509 certificates
  #+end_quote

  *pem-bundle*

  #+begin_quote
    File containing one or more certificate PEM blocks
  #+end_quote

  *pem-directory*

  #+begin_quote
    Directory of PEM files each containing one certificate
  #+end_quote

  *pem-directory-hash*

  #+begin_quote
    Directory of PEM files each containing one certificate, with hash
    symlinks
  #+end_quote

  *openssl-bundle*

  #+begin_quote
    OpenSSL specific PEM bundle of certificates
  #+end_quote

  *openssl-directory*

  #+begin_quote
    Directory of OpenSSL specific PEM files
  #+end_quote

  *java-cacerts*

  #+begin_quote
    Java keystore cacerts certificate bundle
  #+end_quote
#+end_quote

*--overwrite*

#+begin_quote
  Overwrite output file or directory.
#+end_quote

*--purpose=<usage>*

#+begin_quote
  Limit to certificates usable for the given purpose You can specify one
  of the following values:

  *server-auth*

  #+begin_quote
    For authenticating servers
  #+end_quote

  *client-auth*

  #+begin_quote
    For authenticating clients
  #+end_quote

  *email*

  #+begin_quote
    For email protection
  #+end_quote

  *code-signing*

  #+begin_quote
    For authenticated signed code
  #+end_quote

  *1.2.3.4.5...*

  #+begin_quote
    An arbitrary purpose OID
  #+end_quote
#+end_quote

* EXTRACT COMPAT
Extract compatibility trust certificate bundles.

#+begin_quote
  #+begin_example
    $ trust extract-compat
  #+end_example
#+end_quote

OpenSSL, Java and some versions of GnuTLS cannot currently read trust
information directly from the trust policy store. This command extracts
trust information such as certificate anchors for use by these
libraries.

What this command does, and where it extracts the files is distribution
or site specific. Packagers or administrators are expected customize
this command.

* DUMP
Dump PKCS#11 items in the various tokens.

#+begin_quote
  #+begin_example
    $ trust dump
  #+end_example
#+end_quote

Dump information about the various PKCS#11 items in the tokens. Each
item is dumped with its PKCS#11 URI and information in the .p11-kit
persistence format.

You can specify the following options to control what to dump.

*--filter=<what>*

#+begin_quote
  Specifies what certificates to extract. You can specify the following
  values:

  *all*

  #+begin_quote
    All objects. This is the default
  #+end_quote

  *pkcs11:object=xx*

  #+begin_quote
    A PKCS#11 URI to filter with
  #+end_quote
#+end_quote

* BUGS
Please send bug reports to either the distribution bug tracker or the
upstream bug tracker at *https://github.com/p11-glue/p11-kit/issues/*.

* SEE ALSO
*p11-kit*(8)

An explanatory document about storing trust policy:
*https://p11-glue.github.io/p11-glue/doc/storing-trust-policy/*

Further details available in the p11-kit online documentation at
*https://p11-glue.github.io/p11-glue/p11-kit/manual/*.
