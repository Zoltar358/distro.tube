#+TITLE: Man1 - fltk-config.1
#+DESCRIPTION: Linux manpage for fltk-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fltk-config - script to get information about the installed version of
fltk.

* SYNOPSIS
fltk-config [ --prefix /[=DIR]/ ] [ --exec-prefix /[=DIR]/ ] [ --version
] [ --api-version ] [ --use-gl ] [ --use-images ] [ --use-glut ] [ --cc
] [ --cxx ] [ --cflags ] [ --cxxflags ] [ --ldflags ] [ --ldstaticflags
] [ --libs ] [ -D/name[=value]/ ] [ -g ] [ --compile /program.cxx/ ]

* DESCRIPTION
/fltk-config/ is a utility script that can be used to get information
about the current version of FLTK that is installed on the system, what
compiler and linker options to use when building FLTK-based
applications, and to build simple FLTK applications.

The following options are supported:

- --api-version :: \\
  Displays the current FLTK API version number, e.g. "1.1".

- --cc :: 

- --cxx :: \\
  Displays the C/C++ compiler that was used to compile FLTK.

- --cflags :: 

- --cxxflags :: \\
  Displays the C/C++ compiler options to use when compiling source files
  that use FLTK.

- --compile /program.cxx/ :: \\
  Compiles the source file /program.cxx/ into /program/. This option
  implies "--post /program/".

- -g :: \\
  Enables debugging information when compiling with the /--compile/
  option.

- --ldflags :: \\
  Displays the linker options to use when linking a FLTK application.

- --ldstaticflags :: \\
  Displays the linker options to use when linking a FLTK application to
  the static FLTK libraries.

- --libs :: \\
  Displays the full path to the FLTK library files, to be used for
  dependency checking.

- --use-gl :: \\
  Enables OpenGL support.

- --use-glut :: \\
  Enables GLUT support.

- --use-images :: \\
  Enables image file support.

- --version :: \\
  Displays the current FLTK version number, e.g. "1.1.0".

* SEE ALSO
fluid(1), fltk(3)\\
FLTK Programming Manual\\
FLTK Web Site, http://www.fltk.org/

* AUTHORS
Bill Spitzak and others.
