#+TITLE: Man1 - ldbdel.1
#+DESCRIPTION: Linux manpage for ldbdel.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldbdel - Command-line program for deleting LDB records

* SYNOPSIS
*ldbdel* [-h] [-H LDB-URL] [dn] [...]

* DESCRIPTION
ldbdel deletes records from an ldb(3) database. It deletes the records
identified by the dns specified on the command-line.

ldbdel uses either the database that is specified with the -H option or
the database specified by the LDB_URL environment variable.

* OPTIONS
-h

#+begin_quote
  Show list of available options.
#+end_quote

-H <ldb-url>

#+begin_quote
  LDB URL to connect to. See ldb(3) for details.
#+end_quote

* ENVIRONMENT
LDB_URL

#+begin_quote
  LDB URL to connect to (can be overridden by using the -H command-line
  option.)
#+end_quote

* VERSION
This man page is correct for version 1.1 of LDB.

* SEE ALSO
ldb(3), ldbmodify, ldbadd, ldif(5)

* AUTHOR
ldb was written by *Andrew Tridgell*[1].

If you wish to report a problem or make a suggestion then please see the
*http://ldb.samba.org/* web site for current contact and maintainer
information.

ldbdel was written by Andrew Tridgell.

This manpage was written by Jelmer Vernooij.

* NOTES
-  1. :: Andrew Tridgell

  https://www.samba.org/~tridge/
