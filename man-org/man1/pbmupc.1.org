#+TITLE: Man1 - pbmupc.1
#+DESCRIPTION: Linux manpage for pbmupc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmupc - create a Universal Product Code PBM image

* SYNOPSIS
*pbmupc*

[*-s1* | *-s2*] /type/ /manufacturer/ /product/

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmupc* generates an image of a Universal Product Code symbol. The
three arguments are: a one digit product type, a five digit manufacturer
code, and a five digit product code. For example, '0 72890 00011' is the
code for Heineken.

As presently configured, *pbmupc* produces an image 230 bits wide and
175 bits high. The size can be altered by changing the defines at the
beginning of the program, or by running the output through *pamenlarge*
or *pamscale*.

* OPTIONS
The *-s1* and *-s2* options select the style of UPC to generate. The
default, *-s1*, looks more or less like this:

#+begin_example
   ||||||||||||||||
   ||||||||||||||||
   ||||||||||||||||
   ||||||||||||||||
  0||12345||67890||5
#+end_example

The other style, *-s2*, puts the product type digit higher up, and
doesn't display the checksum digit:

#+begin_example
   ||||||||||||||||
   ||||||||||||||||
  0||||||||||||||||
   ||||||||||||||||
   ||12345||67890||
#+end_example

* SEE ALSO
*pbm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
