#+TITLE: Man1 - pip-search.1
#+DESCRIPTION: Linux manpage for pip-search.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pip-search - description of pip search command

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know why you
    came to this page and what on it helped you and what did not. (/Read
    more about this research/)
  #+end_quote
#+end_quote

* DESCRIPTION
Search for PyPI packages whose name or summary contains <query>.

* USAGE

#+begin_quote

  #+begin_quote
    #+begin_example
      python -m pip search [options] <query>
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *-i, --index <url>* :: Base URL of Python Package Index (default
    /https://pypi.org/pypi/)
#+end_quote

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know:

    #+begin_quote

      1. What problem were you trying to solve when you came to this
         page?

      2. What content was useful?

      3. What content was not useful?
    #+end_quote
  #+end_quote
#+end_quote

* AUTHOR
pip developers

* COPYRIGHT
2008-2021, PyPA
