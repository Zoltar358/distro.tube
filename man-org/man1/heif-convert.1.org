#+TITLE: Man1 - heif-convert.1
#+DESCRIPTION: Linux manpage for heif-convert.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
heif-convert - convert HEIC/HEIF image

* SYNOPSIS
*heif-convert* [*-q* /QUALITY/] /filename/ /output[.jpg|.png|.y4m]/

* DESCRIPTION
*heif-convert* Convert HEIC/HEIF image to a different image format.

* OPTIONS
- *-q /QUALITY/* :: Defines quality level between 0 and 100 for the
  generated output file.

* EXIT STATUS
*0*

#+begin_quote
  Success
#+end_quote

*1*

#+begin_quote
  Failure (syntax or usage error; error while loading, converting or
  writing image).
#+end_quote

* NOTES
The available output formats depend on the libraries that were available
at compile time. Supported are JPEG, PNG and Y4M, the file type is
determined based on the extension of the output file.

* BUGS
Please reports bugs or issues at https://github.com/strukturag/libheif

* AUTHORS
Joachim Bauch, struktur AG\\
Dirk Farin, struktur AG

* COPYRIGHT
Copyright © 2017 struktur AG
