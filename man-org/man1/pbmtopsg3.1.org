#+TITLE: Man1 - pbmtopsg3.1
#+DESCRIPTION: Linux manpage for pbmtopsg3.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtopsg3 - convert PBM images to Postscript with G3 fax compression

* SYNOPSIS
*pbmtopsg3* [*--title=*/title/] [*--dpi=*/dpi/] [/filespec/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtopsg3* converts the PBM images in the input PBM file to pages in a
Postscript file encoded with G3 fax compression.

If you don't specify /filespec/, the input is from Standard Input.

Remember that you can create a multi-image PBM file simply by
concatenating single-image PBM files, so if each page is in a different
file, you might do:

#+begin_example
  cat faxpage* | pbmtopsg3 >fax.ps
#+end_example

* OPTIONS
- *-title* :: The Postscript title value. Default is no title.

- *-dpi* :: The resolution of the Postscript output. Default is 72 dpi.

* SEE ALSO
*pnmtops*(1) , *pstopnm*(1) , *gs*(1), *pstopnm*(1) , *pbmtolps*(1) ,
*pbmtoepsi*(1) , *pbmtog3*(1) , *g3topbm*(1) , *pbm*(5)
