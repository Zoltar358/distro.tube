#+TITLE: Man1 - chvt.1
#+DESCRIPTION: Linux manpage for chvt.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
chvt - change foreground virtual terminal

* SYNOPSIS
*chvt* [/option/...] /N/

* DESCRIPTION
The command *chvt* /N/ makes //dev/ttyN/ the foreground terminal. (The
corresponding screen is created if it did not exist yet. To get rid of
unused VTs, use *deallocvt*(1).) The key combination (Ctrl-)LeftAlt-F/N/
(with /N/ in the range 1-12) usually has a similar effect.

* OPTIONS
- /-V, --version/ :: print program version and exit.

- /-h, --help/ :: show this text and exit.
