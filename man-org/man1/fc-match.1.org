#+TITLE: Man1 - fc-match.1
#+DESCRIPTION: Linux manpage for fc-match.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
fc-match - match available fonts

* SYNOPSIS
*fc-match* [ *-asvVh* ] [ *--all* ] [ *--sort* ] [ *--verbose* ] [ *-f
*/format/ | *--format */format/ ] [ *--version* ] [ *--help* ] [
/pattern/* [ */element .../* ] * ]

* DESCRIPTION
*fc-match* matches /pattern/ (empty pattern by default) using the normal
fontconfig matching rules to find the best font available. If *--sort*
is given, the sorted list of best matching fonts is displayed. The
*--all* option works like *--sort* except that no pruning is done on the
list of fonts.

If any elements are specified, only those are printed. Otherwise short
file name, family, and style are printed, unless verbose output is
requested.

* OPTIONS
This program follows the usual GNU command line syntax, with long
options starting with two dashes (`-'). A summary of options is included
below.

- *-a --all * :: Displays sorted list of best matching fonts, but do not
  do any pruning on the list.

- *-s --sort * :: Displays sorted list of best matching fonts.

- *-v --verbose * :: Print verbose output of the whole font pattern for
  each match, or /element/s if any is provided.

- *-f --format */format/* * :: Format output according to the format
  specifier /format/.

- *-V --version * :: Show version of the program and exit.

- *-h --help * :: Show summary of options.

- /pattern/* * :: Displays fonts matching /pattern/ (uses empty pattern
  by default).

- /element/* * :: If set, the /element/ property is displayed for
  matching fonts.

* EXAMPLES
- *fc-match sans* :: Display the best matching font categorized into
  sans-serif generic family, filtered by current locale

- *fc-match sans:lang=en* :: Display the best matching font categorized
  into sans-serif generic family, filtered by English language

- *fc-match sans:lang=en:weight=bold* :: Display the best matching font
  categorized into sans-serif generic family, filtered by English
  language and weight is bold.

* SEE ALSO
*fc-list*(1) *FcFontMatch*(3) *FcFontSort*(3) *FcPatternFormat*(3)
*fc-cat*(1) *fc-cache*(1) *fc-pattern*(1) *fc-query*(1) *fc-scan*(1)

The fontconfig user's guide, in HTML format:
//usr/share/doc/fontconfig/fontconfig-user.html/.

* AUTHOR
This manual page was updated by Patrick Lam <plam@csail.mit.edu>.
