#+TITLE: Manpages - rdf2com.1
#+DESCRIPTION: Linux manpage for rdf2com.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about rdf2com.1 is found in manpage for: [[../man1/rdf2bin.1][man1/rdf2bin.1]]