#+TITLE: Manpages - localc.1
#+DESCRIPTION: Linux manpage for localc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about localc.1 is found in manpage for: [[../man1/libreoffice.1][man1/libreoffice.1]]