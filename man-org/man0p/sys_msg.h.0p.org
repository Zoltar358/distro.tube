#+TITLE: Manpages - sys_msg.h.0p
#+DESCRIPTION: Linux manpage for sys_msg.h.0p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sys/msg.h --- XSI message queue structures

* SYNOPSIS
#+begin_example
  #include <sys/msg.h>
#+end_example

* DESCRIPTION
The /<sys/msg.h>/ header shall define the following data types through
*typedef*:

- msgqnum_t :: Used for the number of messages in the message queue.

- msglen_t :: Used for the number of bytes allowed in a message queue.

These types shall be unsigned integer types that are able to store
values at least as large as a type *unsigned short*.

The /<sys/msg.h>/ header shall define the following symbolic constant as
a message operation flag:

- MSG_NOERROR :: No error if big message.

The /<sys/msg.h>/ header shall define the *msqid_ds* structure, which
shall include the following members:

#+begin_quote
  #+begin_example

    struct ipc_perm msg_perm   Operation permission structure.
    msgqnum_t       msg_qnum   Number of messages currently on queue.
    msglen_t        msg_qbytes Maximum number of bytes allowed on queue.
    pid_t           msg_lspid  Process ID of last msgsnd ( ).
    pid_t           msg_lrpid  Process ID of last msgrcv ( ).
    time_t          msg_stime  Time of last msgsnd ( ).
    time_t          msg_rtime  Time of last msgrcv ( ).
    time_t          msg_ctime  Time of last change.
  #+end_example
#+end_quote

The /<sys/msg.h>/ header shall define the *pid_t*, *size_t*, *ssize_t*,
and *time_t* types as described in /<sys/types.h>/.

The following shall be declared as functions and may also be defined as
macros. Function prototypes shall be provided.

#+begin_quote
  #+begin_example

    int       msgctl(int, int, struct msqid_ds *);
    int       msgget(key_t, int);
    ssize_t   msgrcv(int, void *, size_t, long, int);
    int       msgsnd(int, const void *, size_t, int);
  #+end_example
#+end_quote

In addition, the /<sys/msg.h>/ header shall include the /<sys/ipc.h>/
header.

/The following sections are informative./

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/*<sys_ipc.h>*/, /*<sys_types.h>*/

The System Interfaces volume of POSIX.1‐2017, //msgctl/ ( )/,
//msgget/ ( )/, //msgrcv/ ( )/, //msgsnd/ ( )/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
