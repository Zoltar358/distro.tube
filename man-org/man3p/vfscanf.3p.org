#+TITLE: Manpages - vfscanf.3p
#+DESCRIPTION: Linux manpage for vfscanf.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
vfscanf, vscanf, vsscanf --- format input of a stdarg argument list

* SYNOPSIS
#+begin_example
  #include <stdarg.h>
  #include <stdio.h>
  int vfscanf(FILE *restrict stream, const char *restrict format,
      va_list arg);
  int vscanf(const char *restrict format, va_list arg);
  int vsscanf(const char *restrict s, const char *restrict format,
      va_list arg);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

The /vscanf/(), /vfscanf/(), and /vsscanf/() functions shall be
equivalent to the /scanf/(), /fscanf/(), and /sscanf/() functions,
respectively, except that instead of being called with a variable number
of arguments, they are called with an argument list as defined in the
/<stdarg.h>/ header. These functions shall not invoke the /va_end/
macro. As these functions invoke the /va_arg/ macro, the value of /ap/
after the return is unspecified.

* RETURN VALUE
Refer to //fscanf/ ( )/.

* ERRORS
Refer to //fscanf/ ( )/.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
Applications using these functions should call /va_end/( /ap/)
afterwards to clean up.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.5/, /Standard I/O Streams/, //fscanf/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<stdarg.h>*/,
/*<stdio.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
