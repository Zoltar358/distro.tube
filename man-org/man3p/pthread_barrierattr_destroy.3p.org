#+TITLE: Manpages - pthread_barrierattr_destroy.3p
#+DESCRIPTION: Linux manpage for pthread_barrierattr_destroy.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
pthread_barrierattr_destroy, pthread_barrierattr_init --- destroy and
initialize the barrier attributes object

* SYNOPSIS
#+begin_example
  #include <pthread.h>
  int pthread_barrierattr_destroy(pthread_barrierattr_t *attr);
  int pthread_barrierattr_init(pthread_barrierattr_t *attr);
#+end_example

* DESCRIPTION
The /pthread_barrierattr_destroy/() function shall destroy a barrier
attributes object. A destroyed /attr/ attributes object can be
reinitialized using /pthread_barrierattr_init/(); the results of
otherwise referencing the object after it has been destroyed are
undefined. An implementation may cause /pthread_barrierattr_destroy/()
to set the object referenced by /attr/ to an invalid value.

The /pthread_barrierattr_init/() function shall initialize a barrier
attributes object /attr/ with the default value for all of the
attributes defined by the implementation.

If /pthread_barrierattr_init/() is called specifying an already
initialized /attr/ attributes object, the results are undefined.

After a barrier attributes object has been used to initialize one or
more barriers, any function affecting the attributes object (including
destruction) shall not affect any previously initialized barrier.

The behavior is undefined if the value specified by the /attr/ argument
to /pthread_barrierattr_destroy/() does not refer to an initialized
barrier attributes object.

* RETURN VALUE
If successful, the /pthread_barrierattr_destroy/() and
/pthread_barrierattr_init/() functions shall return zero; otherwise, an
error number shall be returned to indicate the error.

* ERRORS
The /pthread_barrierattr_init/() function shall fail if:

- *ENOMEM* :: Insufficient memory exists to initialize the barrier
  attributes object.

These functions shall not return an error code of *[EINTR]*.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
If an implementation detects that the value specified by the /attr/
argument to /pthread_barrierattr_destroy/() does not refer to an
initialized barrier attributes object, it is recommended that the
function should fail and report an *[EINVAL]* error.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//pthread_barrierattr_getpshared/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<pthread.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
