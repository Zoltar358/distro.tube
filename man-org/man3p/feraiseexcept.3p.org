#+TITLE: Manpages - feraiseexcept.3p
#+DESCRIPTION: Linux manpage for feraiseexcept.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
feraiseexcept --- raise floating-point exception

* SYNOPSIS
#+begin_example
  #include <fenv.h>
  int feraiseexcept(int excepts);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

The /feraiseexcept/() function shall attempt to raise the supported
floating-point exceptions represented by the /excepts/ argument. The
order in which these floating-point exceptions are raised is
unspecified, except that if the /excepts/ argument represents IEC 60559
valid coincident floating-point exceptions for atomic operations (namely
overflow and inexact, or underflow and inexact), then overflow or
underflow shall be raised before inexact. Whether the /feraiseexcept/()
function additionally raises the inexact floating-point exception
whenever it raises the overflow or underflow floating-point exception is
implementation-defined.

* RETURN VALUE
If the argument is zero or if all the specified exceptions were
successfully raised, /feraiseexcept/() shall return zero. Otherwise, it
shall return a non-zero value.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
The effect is intended to be similar to that of floating-point
exceptions raised by arithmetic operations. Hence, enabled traps for
floating-point exceptions raised by this function are taken.

* RATIONALE
Raising overflow or underflow is allowed to also raise inexact because
on some architectures the only practical way to raise an exception is to
execute an instruction that has the exception as a side-effect. The
function is not restricted to accept only valid coincident expressions
for atomic operations, so the function can be used to raise exceptions
accrued over several operations.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//feclearexcept/ ( )/, //fegetexceptflag/ ( )/, //fetestexcept/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<fenv.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
