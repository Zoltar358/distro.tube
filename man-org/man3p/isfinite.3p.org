#+TITLE: Manpages - isfinite.3p
#+DESCRIPTION: Linux manpage for isfinite.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
isfinite --- test for finite value

* SYNOPSIS
#+begin_example
  #include <math.h>
  int isfinite(real-floating x);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

The /isfinite/() macro shall determine whether its argument has a finite
value (zero, subnormal, or normal, and not infinite or NaN). First, an
argument represented in a format wider than its semantic type is
converted to its semantic type. Then determination is based on the type
of the argument.

* RETURN VALUE
The /isfinite/() macro shall return a non-zero value if and only if its
argument has a finite value.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//fpclassify/ ( )/, //isinf/ ( )/, //isnan/ ( )/, //isnormal/ ( )/,
//signbit/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<math.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
