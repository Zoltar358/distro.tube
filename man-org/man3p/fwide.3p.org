#+TITLE: Manpages - fwide.3p
#+DESCRIPTION: Linux manpage for fwide.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
fwide --- set stream orientation

* SYNOPSIS
#+begin_example
  #include <stdio.h>
  #include <wchar.h>
  int fwide(FILE *stream, int mode);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

The /fwide/() function shall determine the orientation of the stream
pointed to by /stream/. If /mode/ is greater than zero, the function
first attempts to make the stream wide-oriented. If /mode/ is less than
zero, the function first attempts to make the stream byte-oriented.
Otherwise, /mode/ is zero and the function does not alter the
orientation of the stream.

If the orientation of the stream has already been determined, /fwide/()
shall not change it.

The /fwide/() function shall not change the setting of /errno/ if
successful.

Since no return value is reserved to indicate an error, an application
wishing to check for error situations should set /errno/ to 0, then call
/fwide/(), then check /errno/, and if it is non-zero, assume an error
has occurred.

* RETURN VALUE
The /fwide/() function shall return a value greater than zero if, after
the call, the stream has wide-orientation, a value less than zero if the
stream has byte-orientation, or zero if the stream has no orientation.

* ERRORS
The /fwide/() function may fail if:

- *EBADF* :: The /stream/ argument is not a valid stream.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
A call to /fwide/() with /mode/ set to zero can be used to determine the
current orientation of a stream.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /*<stdio.h>*/,
/*<wchar.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
