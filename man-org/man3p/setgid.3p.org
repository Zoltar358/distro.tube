#+TITLE: Manpages - setgid.3p
#+DESCRIPTION: Linux manpage for setgid.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
setgid --- set-group-ID

* SYNOPSIS
#+begin_example
  #include <unistd.h>
  int setgid(gid_t gid);
#+end_example

* DESCRIPTION
If the process has appropriate privileges, /setgid/() shall set the real
group ID, effective group ID, and the saved set-group-ID of the calling
process to /gid/.

If the process does not have appropriate privileges, but /gid/ is equal
to the real group ID or the saved set-group-ID, /setgid/() shall set the
effective group ID to /gid/; the real group ID and saved set-group-ID
shall remain unchanged.

The /setgid/() function shall not affect the supplementary group list in
any way.

Any supplementary group IDs of the calling process shall remain
unchanged.

* RETURN VALUE
Upon successful completion, 0 is returned. Otherwise, -1 shall be
returned and /errno/ set to indicate the error.

* ERRORS
The /setgid/() function shall fail if:

- *EINVAL* :: The value of the /gid/ argument is invalid and is not
  supported by the implementation.

- *EPERM* :: The process does not have appropriate privileges and /gid/
  does not match the real group ID or the saved set-group-ID.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
Refer to the RATIONALE section in //setuid/ ( )/.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//exec/ /, //getegid/ ( )/, //geteuid/ ( )/, //getgid/ ( )/,
//getuid/ ( )/, //setegid/ ( )/, //seteuid/ ( )/, //setregid/ ( )/,
//setreuid/ ( )/, //setuid/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<sys_types.h>*/,
/*<unistd.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
