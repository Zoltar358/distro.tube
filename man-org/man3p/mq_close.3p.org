#+TITLE: Manpages - mq_close.3p
#+DESCRIPTION: Linux manpage for mq_close.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
mq_close --- close a message queue (*REALTIME*)

* SYNOPSIS
#+begin_example
  #include <mqueue.h>
  int mq_close(mqd_t mqdes);
#+end_example

* DESCRIPTION
The /mq_close/() function shall remove the association between the
message queue descriptor, /mqdes/, and its message queue. The results of
using this message queue descriptor after successful return from this
/mq_close/(), and until the return of this message queue descriptor from
a subsequent /mq_open/(), are undefined.

If the process has successfully attached a notification request to the
message queue via this /mqdes/, this attachment shall be removed, and
the message queue is available for another process to attach for
notification.

* RETURN VALUE
Upon successful completion, the /mq_close/() function shall return a
value of zero; otherwise, the function shall return a value of -1 and
set /errno/ to indicate the error.

* ERRORS
The /mq_close/() function shall fail if:

- *EBADF* :: The /mqdes/ argument is not a valid message queue
  descriptor.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//mq_open/ ( )/, //mq_unlink/ ( )/, //msgctl/ ( )/, //msgget/ ( )/,
//msgrcv/ ( )/, //msgsnd/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<mqueue.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
