#+TITLE: Manpages - sqrt.3p
#+DESCRIPTION: Linux manpage for sqrt.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

delim $$

* NAME
sqrt, sqrtf, sqrtl --- square root function

* SYNOPSIS
#+begin_example
  #include <math.h>
  double sqrt(double x);
  float sqrtf(float x);
  long double sqrtl(long double x);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

These functions shall compute the square root of their argument /x/,
$sqrt {x}$.

An application wishing to check for error situations should set /errno/
to zero and call /feclearexcept/(FE_ALL_EXCEPT) before calling these
functions. On return, if /errno/ is non-zero or
/fetestexcept/(FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW)
is non-zero, an error has occurred.

* RETURN VALUE
Upon successful completion, these functions shall return the square root
of /x/.

For finite values of /x/ < -0, a domain error shall occur, and either a
NaN (if supported), or an implementation-defined value shall be
returned.

If /x/ is NaN, a NaN shall be returned.

If /x/ is ±0 or +Inf, /x/ shall be returned.

If /x/ is -Inf, a domain error shall occur, and a NaN shall be returned.

* ERRORS
These functions shall fail if:

- Domain Error :: The finite value of /x/ is < -0, or /x/ is -Inf.

  If the integer expression (/math_errhandling/ & MATH_ERRNO) is
  non-zero, then /errno/ shall be set to *[EDOM]*. If the integer
  expression (/math_errhandling/ & MATH_ERREXCEPT) is non-zero, then the
  invalid floating-point exception shall be raised.

/The following sections are informative./

* EXAMPLES
** Taking the Square Root of 9.0

#+begin_quote
  #+begin_example

    #include <math.h>
    ...
    double x = 9.0;
    double result;
    ...
    result = sqrt(x);
  #+end_example
#+end_quote

* APPLICATION USAGE
On error, the expressions (/math_errhandling/ & MATH_ERRNO) and
(/math_errhandling/ & MATH_ERREXCEPT) are independent of each other, but
at least one of them must be non-zero.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//feclearexcept/ ( )/, //fetestexcept/ ( )/, //isnan/ ( )/

The Base Definitions volume of POSIX.1‐2017, /Section 4.20/, /Treatment
of Error Conditions for Mathematical Functions/, /*<math.h>*/,
/*<stdio.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
