#+TITLE: Manpages - sigdelset.3p
#+DESCRIPTION: Linux manpage for sigdelset.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sigdelset --- delete a signal from a signal set

* SYNOPSIS
#+begin_example
  #include <signal.h>
  int sigdelset(sigset_t *set, int signo);
#+end_example

* DESCRIPTION
The /sigdelset/() function deletes the individual signal specified by
/signo/ from the signal set pointed to by /set/.

Applications should call either /sigemptyset/() or /sigfillset/() at
least once for each object of type *sigset_t* prior to any other use of
that object. If such an object is not initialized in this way, but is
nonetheless supplied as an argument to any of /pthread_sigmask/(),
/sigaction/(), /sigaddset/(), /sigdelset/(), /sigismember/(),
/sigpending/(), /sigprocmask/(), /sigsuspend/(), /sigtimedwait/(),
/sigwait/(), or /sigwaitinfo/(), the results are undefined.

* RETURN VALUE
Upon successful completion, /sigdelset/() shall return 0; otherwise, it
shall return -1 and set /errno/ to indicate the error.

* ERRORS
The /sigdelset/() function may fail if:

- *EINVAL* :: The /signo/ argument is not a valid signal number, or is
  an unsupported signal number.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
/Section 2.4/, /Signal Concepts/, //pthread_sigmask/ ( )/,
//sigaction/ ( )/, //sigaddset/ ( )/, //sigemptyset/ ( )/,
//sigfillset/ ( )/, //sigismember/ ( )/, //sigpending/ ( )/,
//sigsuspend/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<signal.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
