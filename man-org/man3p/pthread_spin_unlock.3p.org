#+TITLE: Manpages - pthread_spin_unlock.3p
#+DESCRIPTION: Linux manpage for pthread_spin_unlock.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
pthread_spin_unlock --- unlock a spin lock object

* SYNOPSIS
#+begin_example
  #include <pthread.h>
  int pthread_spin_unlock(pthread_spinlock_t *lock);
#+end_example

* DESCRIPTION
The /pthread_spin_unlock/() function shall release the spin lock
referenced by /lock/ which was locked via the /pthread_spin_lock/() or
/pthread_spin_trylock/() functions.

The results are undefined if the lock is not held by the calling thread.

If there are threads spinning on the lock when /pthread_spin_unlock/()
is called, the lock becomes available and an unspecified spinning thread
shall acquire the lock.

The results are undefined if this function is called with an
uninitialized thread spin lock.

* RETURN VALUE
Upon successful completion, the /pthread_spin_unlock/() function shall
return zero; otherwise, an error number shall be returned to indicate
the error.

* ERRORS
This function shall not return an error code of *[EINTR]*.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
If an implementation detects that the value specified by the /lock/
argument to /pthread_spin_unlock/() does not refer to an initialized
spin lock object, it is recommended that the function should fail and
report an *[EINVAL]* error.

If an implementation detects that the value specified by the /lock/
argument to /pthread_spin_unlock/() refers to a spin lock object for
which the current thread does not hold the lock, it is recommended that
the function should fail and report an *[EPERM]* error.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//pthread_spin_destroy/ ( )/, //pthread_spin_lock/ ( )/

The Base Definitions volume of POSIX.1‐2017, /Section 4.12/, /Memory
Synchronization/, /*<pthread.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
