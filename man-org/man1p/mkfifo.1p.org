#+TITLE: Manpages - mkfifo.1p
#+DESCRIPTION: Linux manpage for mkfifo.1p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
mkfifo --- make FIFO special files

* SYNOPSIS
#+begin_example
  mkfifo [-m mode] file...
#+end_example

* DESCRIPTION
The /mkfifo/ utility shall create the FIFO special files specified by
the operands, in the order specified.

For each /file/ operand, the /mkfifo/ utility shall perform actions
equivalent to the /mkfifo/() function defined in the System Interfaces
volume of POSIX.1‐2017, called with the following arguments:

-  1. :: The /file/ operand is used as the /path/ argument.

-  2. :: The value of the bitwise-inclusive OR of S_IRUSR, S_IWUSR,
  S_IRGRP, S_IWGRP, S_IROTH, and S_IWOTH is used as the /mode/ argument.
  (If the *-m* option is specified, the value of the /mkfifo/() /mode/
  argument is unspecified, but the FIFO shall at no time have
  permissions less restrictive than the *-m* /mode/ option-argument.)

* OPTIONS
The /mkfifo/ utility shall conform to the Base Definitions volume of
POSIX.1‐2017, /Section 12.2/, /Utility Syntax Guidelines/.

The following option shall be supported:

- -m mode :: Set the file permission bits of the newly-created FIFO to
  the specified /mode/ value. The /mode/ option-argument shall be the
  same as the /mode/ operand defined for the /chmod/ utility. In the
  /symbolic_mode/ strings, the /op/ characters *'+'* and *'-'* shall be
  interpreted relative to an assumed initial mode of /a/= /rw/.

* OPERANDS
The following operand shall be supported:

- file :: A pathname of the FIFO special file to be created.

* STDIN
Not used.

* INPUT FILES
None.

* ENVIRONMENT VARIABLES
The following environment variables shall affect the execution of
/mkfifo/:

- LANG :: Provide a default value for the internationalization variables
  that are unset or null. (See the Base Definitions volume of
  POSIX.1‐2017, /Section 8.2/, /Internationalization Variables/ for the
  precedence of internationalization variables used to determine the
  values of locale categories.)

- LC_ALL :: If set to a non-empty string value, override the values of
  all the other internationalization variables.

- LC_CTYPE :: Determine the locale for the interpretation of sequences
  of bytes of text data as characters (for example, single-byte as
  opposed to multi-byte characters in arguments).

- LC_MESSAGES :: \\
  Determine the locale that should be used to affect the format and
  contents of diagnostic messages written to standard error.

- NLSPATH :: Determine the location of message catalogs for the
  processing of /LC_MESSAGES/.

* ASYNCHRONOUS EVENTS
Default.

* STDOUT
Not used.

* STDERR
The standard error shall be used only for diagnostic messages.

* OUTPUT FILES
None.

* EXTENDED DESCRIPTION
None.

* EXIT STATUS
The following exit values shall be returned:

-  0 :: All the specified FIFO special files were created successfully.

- >0 :: An error occurred.

* CONSEQUENCES OF ERRORS
Default.

/The following sections are informative./

* APPLICATION USAGE
None.

* EXAMPLES
None.

* RATIONALE
This utility was added to permit shell applications to create FIFO
special files.

The *-m* option was added to control the file mode, for consistency with
the similar functionality provided by the /mkdir/ utility.

Early proposals included a *-p* option similar to the /mkdir/ *-p*
option that created intermediate directories leading up to the FIFO
specified by the final component. This was removed because it is not
commonly needed and is not common practice with similar utilities.

The functionality of /mkfifo/ is described substantially through a
reference to the /mkfifo/() function in the System Interfaces volume of
POSIX.1‐2017. For example, by default, the mode of the FIFO file is
affected by the file mode creation mask in accordance with the specified
behavior of the /mkfifo/() function. In this way, there is less
duplication of effort required for describing details of the file
creation.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//chmod/ /, //umask/ /

The Base Definitions volume of POSIX.1‐2017, /Chapter 8/, /Environment
Variables/, /Section 12.2/, /Utility Syntax Guidelines/

The System Interfaces volume of POSIX.1‐2017, //mkfifo/ ( )/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
