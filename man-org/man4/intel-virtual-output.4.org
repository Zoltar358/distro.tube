#+TITLE: Manpages - intel-virtual-output.4
#+DESCRIPTION: Linux manpage for intel-virtual-output.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
intel-virtual-output - Utility for connecting the Integrated Intel GPU
to discrete outputs

* SYNOPSIS
#+begin_example
  intel-virtual-output [<remote display>] <output name>...
#+end_example

* DESCRIPTION
*intel-virtual-output* is a utility for Intel integrated graphics
chipsets on hybrid systems. The tool connects local VirtualHeads to a
remote output, allowing the primary display to extend onto the remote
outputs.

* REPORTING BUGS
The xf86-video-intel driver is part of the X.Org and Freedesktop.org
umbrella projects. Details on bug reporting can be found at
https://01.org/linuxgraphics/documentation/how-report-bugs. Mailing
lists are also commonly used to report experiences and ask questions
about configuration and other topics. See lists.freedesktop.org for more
information (the xorg@lists.freedesktop.org mailing list is the most
appropriate place to ask X.Org and driver related questions).

* SEE ALSO
intel(4), Xorg(1), xorg.conf(5), Xserver(1), X(7)
