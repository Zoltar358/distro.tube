#+TITLE: Manpages - ati.4
#+DESCRIPTION: Linux manpage for ati.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ati - ATI video driver

* SYNOPSIS
#+begin_example
  Section Device
   Identifier devname
   Driver ati
    ...
  EndSection
#+end_example

* DESCRIPTION
*ati* is an Xorg wrapper driver for ATI video cards. It autodetects
whether your hardware has a Radeon, Rage 128, or Mach64 or earlier class
of chipset, and loads the radeon(4), r128(4), or mach64 driver as
appropriate.

* SUPPORTED HARDWARE
The *ati* driver supports Radeon, Rage 128, and Mach64 and earlier
chipsets by loading those drivers. See those manpages for specific cards
supported.

* CONFIGURATION DETAILS
Please refer to xorg.conf(5) for general configuration details, and the
specific card driver for driver configuration details.

* SEE ALSO
Xorg(1), xorg.conf(5), Xserver(1), X(7), r128(4), radeon(4)

* AUTHORS
See the individual driver pages for authors.
