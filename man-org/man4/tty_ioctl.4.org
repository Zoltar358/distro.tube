#+TITLE: Manpages - tty_ioctl.4
#+DESCRIPTION: Linux manpage for tty_ioctl.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about tty_ioctl.4 is found in manpage for: [[../man2/ioctl_tty.2][man2/ioctl_tty.2]]