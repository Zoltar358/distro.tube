#+TITLE: Manpages - urandom.4
#+DESCRIPTION: Linux manpage for urandom.4
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about urandom.4 is found in manpage for: [[../man4/random.4][man4/random.4]]