#+TITLE: Manpages - ssetmask.2
#+DESCRIPTION: Linux manpage for ssetmask.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ssetmask.2 is found in manpage for: [[../man2/sgetmask.2][man2/sgetmask.2]]