#+TITLE: Manpages - exit_group.2
#+DESCRIPTION: Linux manpage for exit_group.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
exit_group - exit all threads in a process

* SYNOPSIS
#+begin_example
  #include <sys/syscall.h> /* Definition of SYS_* constants */
  #include <unistd.h>

  noreturn void syscall(SYS_exit_group, int status);
#+end_example

/Note/: glibc provides no wrapper for *exit_group*(), necessitating the
use of *syscall*(2).

* DESCRIPTION
This system call is equivalent to *_exit*(2) except that it terminates
not only the calling thread, but all threads in the calling process's
thread group.

* RETURN VALUE
This system call does not return.

* VERSIONS
This call is present since Linux 2.5.35.

* CONFORMING TO
This call is Linux-specific.

* NOTES
Since glibc 2.3, this is the system call invoked when the *_exit*(2)
wrapper function is called.

* SEE ALSO
*exit*(2)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
