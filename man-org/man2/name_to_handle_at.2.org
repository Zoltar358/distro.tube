#+TITLE: Manpages - name_to_handle_at.2
#+DESCRIPTION: Linux manpage for name_to_handle_at.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about name_to_handle_at.2 is found in manpage for: [[../man2/open_by_handle_at.2][man2/open_by_handle_at.2]]