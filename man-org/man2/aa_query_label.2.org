#+TITLE: Manpages - aa_query_label.2
#+DESCRIPTION: Linux manpage for aa_query_label.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
aa_query_label - query access permission associated with a label

aa_query_file_path, aa_query_file_path_len - query access permissions of
a file path

aa_query_link_path, aa_query_link_path_len - query access permissions of
a link path

* SYNOPSIS
*#include <sys/apparmor.h>*

*int aa_query_label(uint32_t mask, char *query, size_t size, int
*allowed, int *audited);*

*int aa_query_file_path(uint32_t mask, const char *label, size_t
label_len, const char *path, int *allowed, int *audited);*

*int aa_query_file_path_len(uint32_t mask, const char *label, size_t
label_len, const char *path, size_t path_len, int *allowed, int
*audited);*

*int aa_query_link_path(const char *label, const char *target, const
char *link, int *allowed, int *audited);*

*int aa_query_link_path_len(const char *label, size_t label_len, const
char *target, size_t target_len, const char *link, size_t link_len, int
*allowed, int *audited);*

Link with *-lapparmor* when compiling.

* DESCRIPTION
The *aa_query_label* function fetches the current permissions granted by
the specified /label/ in the /query/ string.

The query is a raw binary formatted query, containing the label and
permission query to make. The returned /allowed/ and /audited/ values
are interpreted boolean values, simply stating whether the query is
allowed and if it is audited.

The mask of the query string is a bit mask of permissions to query and
is class type dependent (see *AA_CLASS_xxx* entries in
/sys/apparmor.h/).

The format of the query string is also dependent on the *AA_CLASS* and
as such the *aa_query_xxx* helper functions should usually be used
instead of directly using *aa_query_label*. If directly using the
interface the /query/ string is required to have a header of
*AA_QUERY_CMD_LABEL_SIZE* that will be used by *aa_query_label*.

The *aa_query_file_path* and *aa_query_file_path_len* functions are
helper function that assemble a properly formatted file path query for
the *aa_query_label* function. The /label/ is a valid apparmor label as
returned by /aa_splitcon/ with /label_len/ being the length of the
/label/. The /path/ is any valid filesystem path to query permissions
for. For the *aa_query_file_path_len* variant the /path_len/ parameter
specifies the number of bytes in the /path/ to use as part of the query.

The *aa_query_link_path* and *aa_query_link_path_len* functions are
helper functions that assemble a properly formatted link path query for
the *aa_query_label* function. The /link_len/ and /target_len/
parameters specify the number of bytes in the /link/ and /target/ to use
as part of the query.

* RETURN VALUE
On success 0 is returned, and the /allowed/ and /audited/ parameters
contain a boolean value of 0 not allowed/audited or 1 allowed/audited.
On error, -1 is returned, and *errno* (3) is set appropriately.

* ERRORS
- EINVAL :: The requested /mask/ is empty. The /size/ of the query is
  less than the query *AA_QUERY_CMD_LABEL_SIZE* The apparmor kernel
  module is not loaded or the kernel interface access interface is not
  available

- ENOMEM :: Insufficient memory was available.

- EACCES :: Access to the specified /label/ or query interface was
  denied.

- ENOENT :: The specified /label/ does not exist or is not visible.

- ERANGE :: The confinement data is too large to fit in the supplied
  buffer.

* NOTES
The label permissions returned are only valid for the time of the query
and can change at any point in the future.

* BUGS
None known. If you find any, please report them at
<https://gitlab.com/apparmor/apparmor/-/issues>.

* SEE ALSO
*apparmor* (7), *apparmor.d* (5), *apparmor_parser* (8),
*aa_getcon* (2), *aa_splitcon* (3) and <https://wiki.apparmor.net>.
