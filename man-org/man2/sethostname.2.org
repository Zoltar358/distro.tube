#+TITLE: Manpages - sethostname.2
#+DESCRIPTION: Linux manpage for sethostname.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sethostname.2 is found in manpage for: [[../man2/gethostname.2][man2/gethostname.2]]