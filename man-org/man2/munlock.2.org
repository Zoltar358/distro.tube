#+TITLE: Manpages - munlock.2
#+DESCRIPTION: Linux manpage for munlock.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about munlock.2 is found in manpage for: [[../man2/mlock.2][man2/mlock.2]]