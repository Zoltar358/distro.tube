#+TITLE: Manpages - setuid32.2
#+DESCRIPTION: Linux manpage for setuid32.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setuid32.2 is found in manpage for: [[../man2/setuid.2][man2/setuid.2]]