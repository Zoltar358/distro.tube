#+TITLE: Manpages - newfstatat.2
#+DESCRIPTION: Linux manpage for newfstatat.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about newfstatat.2 is found in manpage for: [[../man2/fstatat.2][man2/fstatat.2]]