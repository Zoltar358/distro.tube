#+TITLE: Manpages - munmap.2
#+DESCRIPTION: Linux manpage for munmap.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about munmap.2 is found in manpage for: [[../man2/mmap.2][man2/mmap.2]]