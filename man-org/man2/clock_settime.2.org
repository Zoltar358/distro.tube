#+TITLE: Manpages - clock_settime.2
#+DESCRIPTION: Linux manpage for clock_settime.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about clock_settime.2 is found in manpage for: [[../man2/clock_getres.2][man2/clock_getres.2]]