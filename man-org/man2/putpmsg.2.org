#+TITLE: Manpages - putpmsg.2
#+DESCRIPTION: Linux manpage for putpmsg.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about putpmsg.2 is found in manpage for: [[../man2/unimplemented.2][man2/unimplemented.2]]