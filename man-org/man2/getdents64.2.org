#+TITLE: Manpages - getdents64.2
#+DESCRIPTION: Linux manpage for getdents64.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getdents64.2 is found in manpage for: [[../man2/getdents.2][man2/getdents.2]]