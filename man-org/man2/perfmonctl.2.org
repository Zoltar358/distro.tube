#+TITLE: Manpages - perfmonctl.2
#+DESCRIPTION: Linux manpage for perfmonctl.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
perfmonctl - interface to IA-64 performance monitoring unit

* SYNOPSIS
#+begin_example
  #include <syscall.h>
  #include <perfmon.h>

  long perfmonctl(int fd, int cmd, void *arg, int narg);
#+end_example

/Note/: There is no glibc wrapper for this system call; see NOTES.

* DESCRIPTION
The IA-64-specific *perfmonctl*() system call provides an interface to
the PMU (performance monitoring unit). The PMU consists of PMD
(performance monitoring data) registers and PMC (performance monitoring
control) registers, which gather hardware statistics.

*perfmonctl*() applies the operation /cmd/ to the input arguments
specified by /arg/. The number of arguments is defined by /narg/. The
/fd/ argument specifies the perfmon context to operate on.

Supported values for /cmd/ are:

- *PFM_CREATE_CONTEXT* :: 

#+begin_example
  perfmonctl(int fd, PFM_CREATE_CONTEXT, pfarg_context_t *ctxt, 1);
#+end_example

Set up a context.

#+begin_quote
  The /fd/ parameter is ignored. A new perfmon context is created as
  specified in /ctxt/ and its file descriptor is returned in
  /ctxt->ctx_fd/.

  The file descriptor can be used in subsequent calls to *perfmonctl*()
  and can be used to read event notifications (type /pfm_msg_t/) using
  *read*(2). The file descriptor is pollable using *select*(2),
  *poll*(2), and *epoll*(7).

  The context can be destroyed by calling *close*(2) on the file
  descriptor.
#+end_quote

- *PFM_WRITE_PMCS* :: 

#+begin_example
  perfmonctl(int fd, PFM_WRITE_PMCS, pfarg_reg_t *pmcs, n);
#+end_example

Set PMC registers.

- *PFM_WRITE_PMDS* :: 

#+begin_example
  perfmonctl(int fd, PFM_WRITE_PMDS, pfarg_reg_t *pmds, n);
#+end_example

Set PMD registers.

- *PFM_READ_PMDS* :: 

#+begin_example
  perfmonctl(int fd, PFM_READ_PMDS, pfarg_reg_t *pmds, n);
#+end_example

Read PMD registers.

- *PFM_START* :: 

#+begin_example
  perfmonctl(int fd, PFM_START, NULL, 0);
#+end_example

Start monitoring.

- *PFM_STOP* :: 

#+begin_example
  perfmonctl(int fd, PFM_STOP, NULL, 0);
#+end_example

Stop monitoring.

- *PFM_LOAD_CONTEXT* :: 

#+begin_example
  perfmonctl(int fd, PFM_LOAD_CONTEXT, pfarg_load_t *largs, 1);
#+end_example

Attach the context to a thread.

- *PFM_UNLOAD_CONTEXT* :: 

#+begin_example
  perfmonctl(int fd, PFM_UNLOAD_CONTEXT, NULL, 0);
#+end_example

Detach the context from a thread.

- *PFM_RESTART* :: 

#+begin_example
  perfmonctl(int fd, PFM_RESTART, NULL, 0);
#+end_example

Restart monitoring after receiving an overflow notification.

- *PFM_GET_FEATURES* :: 

#+begin_example
  perfmonctl(int fd, PFM_GET_FEATURES, pfarg_features_t *arg, 1);
#+end_example

- *PFM_DEBUG* :: 

#+begin_example
  perfmonctl(int fd, PFM_DEBUG, val, 0);
#+end_example

If /val/ is nonzero, enable debugging mode, otherwise disable.

- *PFM_GET_PMC_RESET_VAL* :: 

#+begin_example
  perfmonctl(int fd, PFM_GET_PMC_RESET_VAL, pfarg_reg_t *req, n);
#+end_example

Reset PMC registers to default values.

* RETURN VALUE
*perfmonctl*() returns zero when the operation is successful. On error,
-1 is returned and /errno/ is set to indicate the error.

* VERSIONS
*perfmonctl*() was added in Linux 2.4; it was removed in Linux 5.10.

* CONFORMING TO
*perfmonctl*() is Linux-specific and is available only on the IA-64
architecture.

* NOTES
This system call was broken for many years, and ultimately removed in
Linux 5.10.

Glibc does not provide a wrapper for this system call; on kernels where
it exists, call it using *syscall*(2).

* SEE ALSO
*gprof*(1)

The perfmon2 interface specification

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
