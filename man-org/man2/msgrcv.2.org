#+TITLE: Manpages - msgrcv.2
#+DESCRIPTION: Linux manpage for msgrcv.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about msgrcv.2 is found in manpage for: [[../man2/msgop.2][man2/msgop.2]]