#+TITLE: Manpages - fchown.2
#+DESCRIPTION: Linux manpage for fchown.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fchown.2 is found in manpage for: [[../man2/chown.2][man2/chown.2]]