#+TITLE: Manpages - unimplemented.2
#+DESCRIPTION: Linux manpage for unimplemented.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
afs_syscall, break, fattach, fdetach, ftime, getmsg, getpmsg, gtty,
isastream, lock, madvise1, mpx, prof, profil, putmsg, putpmsg, security,
stty, tuxcall, ulimit, vserver - unimplemented system calls

* SYNOPSIS
#+begin_example
  Unimplemented system calls.
#+end_example

* DESCRIPTION
These system calls are not implemented in the Linux kernel.

* RETURN VALUE
These system calls always return -1 and set /errno/ to *ENOSYS*.

* NOTES
Note that *ftime*(3), *profil*(3), and *ulimit*(3) are implemented as
library functions.

Some system calls, like *alloc_hugepages*(2), *free_hugepages*(2),
*ioperm*(2), *iopl*(2), and *vm86*(2) exist only on certain
architectures.

Some system calls, like *ipc*(2), *create_module*(2), *init_module*(2),
and *delete_module*(2) exist only when the Linux kernel was built with
support for them.

* SEE ALSO
*syscalls*(2)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
