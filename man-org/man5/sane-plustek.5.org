#+TITLE: Manpages - sane-plustek.5
#+DESCRIPTION: Linux manpage for sane-plustek.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sane-plustek - SANE backend for LM983[1/2/3] based USB flatbed scanners

* DESCRIPTION
The *sane-plustek* library implements a SANE (Scanner Access Now Easy)
backend that provides access to USB flatbed scanners based on National
Semiconductor Merlin chipsets (LM9831, 9832 and 9833). If you're looking
for parallel-port support for Plustek scanner please refer to the
*sane-plustek_pp*(5) backend.

* SUPPORTED DEVICES
The Backend is able to support USB scanner based on the National
Semiconductor chipsets LM9831, LM9832 and LM9833. The following tables
show various devices which are currently reported to work. If your
Plustek scanner has another Product ID, then the device is *NOT*
supported by this backend.\\

Vendor Plustek - ID: 0x07B3\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  OpticPro U12       LM9831  600x1200dpi 42bit 512Kb 0x0010
  OpticPro UT12      LM9831  600x1200dpi 42bit 512Kb 0x0013
  OpticPro UT12      LM9832  600x1200dpi 42bit 512Kb 0x0017
  OpticPro UT16      LM9832  600x1200dpi 42bit 512Kb 0x0017
  OpticPro U24       LM9831 1200x2400dpi 42bit   2Mb 0x0011
  OpticPro U24       LM9832 1200x2400dpi 42bit   2Mb 0x0015
  OpticPro UT24      LM9832 1200x2400dpi 42bit   2Mb 0x0017
#+end_example

Vendor KYE/Genius - ID: 0x0458\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  Colorpage HR6 V2   LM9832  600x1200dpi 42bit 512Kb 0x2007
  Colorpage HR6 V2   LM9832  600x1200dpi 42bit 512Kb 0x2008
  Colorpage HR6A     LM9832  600x1200dpi 42bit 512Kb 0x2009
  Colorpage HR7      LM9832  600x1200dpi 42bit 512Kb 0x2013
  Colorpage HR7LE    LM9832  600x1200dpi 42bit 512Kb 0x2015
  Colorpage HR6X     LM9832  600x1200dpi 42bit 512Kb 0x2016
#+end_example

Vendor Hewlett-Packard - ID: 0x03F0\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  ScanJet 2100C      LM9831  600x1200dpi 42bit 512Kb 0x0505
  ScanJet 2200C      LM9832  600x1200dpi 42bit 512Kb 0x0605
#+end_example

Vendor Mustek - ID: 0x0400\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  BearPaw 1200       LM9831  600x1200dpi 42bit 512Kb 0x1000
  BearPaw 1200       LM9832  600x1200dpi 42bit 512Kb 0x1001*
  BearPaw 2400       LM9832 1200x2400dpi 42bit   2Mb 0x1001
#+end_example

* see also description for model override switch below!

Vendor UMAX - ID: 0x1606\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  UMAX 3400          LM9832  600x1200dpi 42bit 512Kb 0x0050
  UMAX 3400/3450     LM9832  600x1200dpi 42bit 512Kb 0x0060
  UMAX 5400          LM9832 1200x2400dpi 42bit 512Kb 0x0160
#+end_example

Vendor COMPAQ - ID: 0x049F\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  S4-100             LM9832  600x1200dpi 42bit 512Kb 0x001A
#+end_example

Vendor Epson - ID: 0x04B8\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  Perfection 1250    LM9832 1200x2400dpi 42bit 512Kb 0x010F
  Perfection 1260    LM9832 1200x2400dpi 42bit 512Kb 0x011D
#+end_example

Vendor CANON - ID: 0x04A9\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  CanoScan N650/656U LM9832  600x1200dpi 42bit 512Kb 0x2206
  CanoScan N1220U    LM9832 1200x2400dpi 42bit 512Kb 0x2207
  CanoScan D660U     LM9832  600x1200dpi 42bit 512Kb 0x2208
  CanoScan N670/676U LM9833  600x1200dpi 48bit 512Kb 0x220D
  CanoScan N1240U    LM9833 1200x2400dpi 48bit 512Kb 0x220E
  CanoScan LIDE20    LM9833  600x1200dpi 48bit 512Kb 0x220D
  CanoScan LIDE25    LM9833 1200x2400dpi 48bit 512Kb 0x2220
  CanoScan LIDE30    LM9833 1200x2400dpi 48bit 512Kb 0x220E
#+end_example

Vendor Syscan - ID: 0x0A82\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  Travelscan 662     LM9833  600x1200dpi 48bit 512Kb 0x6620
  Travelscan 464     LM9833  600x1200dpi 48bit 512Kb 0x4600
#+end_example

Vendor Portable Peripheral Co., Ltd. - ID: 0x0A53\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  Q-Scan USB001      LM9832   300x600dpi 42bit 512Kb 0x1000
  Q-Scan USB201      LM9832   300x600dpi 42bit 512Kb 0x2000
#+end_example

Vendor Visioneer - ID: 0x04A7\\

#+begin_example
  ----------------------------------------------------------
  USB Model:         ASIC:  Properties:              Prod-ID
  ----------------------------------------------------------
  Strobe XP100       LM9833  600x1200dpi 48bit 512Kb 0x0427
#+end_example

* OTHER PLUSTEK SCANNERS
For parallelport device support see the *sane-plustek_pp*(5) backend.\\
The SCSI scanner OpticPro 19200S is a rebadged Artec AM12S scanner and
is supported by the *sane-artec*(5) backend.\\
Only the National Semiconductor LM983[1/2/] based devices of Plustek are
supported by this backend. Older versions of the U12, the UT12, the
U1212 and U1248 (GrandTech chipset) are not supported.

#+begin_example
  Model             Chipset    backend
  ------------------------------------
  U1248             GrandTech  gt68xx
  UT16B             GrandTech  gt68xx
  OpticSlim 1200    GrandTech  gt68xx
  OpticSlim 2400    GrandTech  gt68xx
  U12                P98003     u12
  UT12               P98003     u12
  1212U              P98003     u12
#+end_example

For a more complete and up to date list see:
/http://www.sane-project.org/sane-supported-devices.html/.

* CONFIGURATION
To use your scanner with this backend, you need at least two entries in
the configuration file //etc/sane.d/plustek.conf/

#+begin_quote
  /[usb] vendor-id product-id/\\
  /device /dev/usbscanner/
#+end_quote

/[usb]/ tells the backend, that the following devicename (here
//dev/usbscanner/) has to be interpreted as USB scanner device. If
vendor- and product-id has not been specified, the backend tries to
detect this by its own. If device is set to /auto/ then the next
matching device is used.\\
The following options can be used for a default setup of your device.
Most of them are also available through the frontend.

*The Options:*

option warmup t

#+begin_quote
  /t/ specifies the warmup period in seconds, if set to -1, the
  automatic warmup function will be used
#+end_quote

option lampOff t

#+begin_quote
  /t/ is the time in seconds for switching off the lamps in standby mode
#+end_quote

option lOffonEnd b

#+begin_quote
  /b/ specifies the behaviour when closing the backend, 1 --> switch
  lamps off, 0 --> do not change lamp status
#+end_quote

option mov m

#+begin_quote
  /m/ is the model override switch. It works only with Mustek BearPaw
  devices.\\
  \\

  #+begin_example
    m/PID |    0x1000    |    0x1001
    ------+--------------+--------------
      0   | BearPaw 1200 | BearPaw 2400
      1   |  no function | BearPaw 1200
  #+end_example
#+end_quote

option invertNegatives b

#+begin_quote
  /b/ 0 --> do not invert the picture during negative scans,\\
  1 --> invert picture
#+end_quote

option cacheCalData b

#+begin_quote
  /b/ 0 --> do not save calibration results,\\
  1 --> save results of calibration in /~/.sane// directory
#+end_quote

option altCalibration b

#+begin_quote
  /b/ 0 --> use standard calibration routines,\\
  1 --> use alternate calibration (only non Plustek devices, standard
  for CIS devices)
#+end_quote

option skipFine b

#+begin_quote
  /b/ 0 --> perform fine calibration,\\
  1 --> skip fine calibration (only non Plustek devices)
#+end_quote

option skipFineWhite b

#+begin_quote
  /b/ 0 --> perform white fine calibration,\\
  1 --> skip white fine calibration (only non Plustek devices)
#+end_quote

option skipDarkStrip b

#+begin_quote
  /b/ 0 --> perform dark calibration, with enabled lamp using the dark
  calibration strip of the scanner. If the scanner does not have such a
  strip, the alternative way is to switch off the lamp during this
  step.\\
  1 --> always switch off the lamp for dark calibration, even a black
  strip is available
#+end_quote

option skipCalibration b

#+begin_quote
  /b/ 0 --> perform calibration,\\
  1 --> skip calibration (only non Plustek devices)
#+end_quote

option enableTPA b

#+begin_quote
  /b/ 0 --> default behaviour, specified by the internal tables,\\
  1 --> override internal tables and allow TPA mode (EPSON/UMAX only)
#+end_quote

option posOffX x\\
option posOffY y\\
option tpaOffX x\\
option tpaOffY y\\
option negOffX x\\
option negOffY y

#+begin_quote
  /x y/ By using this settings, the user can adjust the given image
  positions. *Please note, that there's no internal range checking for*
  *this feature.*
#+end_quote

option posShadingY p\\
option tpaShadingY p\\
option negShadingY p

#+begin_quote
  /p/ overrides the internal shading position. The values are in steps.
  *Please note, that there's no internal range checking for* *this
  feature.*
#+end_quote

option redGamma r\\
option greenGamma g\\
option blueGamma b\\
option grayGamma gr

#+begin_quote
  /r g b gr/
#+end_quote

By using these values, the internal linear gamma table (r,g,b,gr = 1.0)
can be adjusted.

option red_gain r\\
option red_offset ro\\
option green_gain g\\
option green_offset go\\
option blue_gain b\\
option blue_offset bo

#+begin_quote
  /r g b ro go bo/ These values can be used to set the gain and offset
  values of the AFE for each channel. The range is between 0 and 63. -1
  means autocalibration.
#+end_quote

See the plustek.conf file for examples.

*Note:*\\
You have to make sure, that the USB subsystem is loaded correctly and
you have access to the device-node. For more details see *sane-usb*(5)
manpage. You might use *sane-find-scanner*(1) to check that you have
access to your device.

*Note:*\\
If there's no configuration file, the backend defaults to *device auto*

* FILES
- //etc/sane.d/plustek.conf/ :: The backend configuration file

- //usr/lib/sane/libsane-plustek.a/ :: The static library implementing
  this backend.

- //usr/lib/sane/libsane-plustek.so/ :: The shared library implementing
  this backend (present on systems that support dynamic loading).

* ENVIRONMENT
- *SANE_CONFIG_DIR* :: This environment variable specifies the list of
  directories that may contain the configuration file. Under UNIX, the
  directories are separated by a colon (`:'), under OS/2, they are
  separated by a semi-colon (`;'). If this variable is not set, the
  configuration file is searched in two default directories: first, the
  current working directory (".") and then in //etc/sane.d/. If the
  value of the environment variable ends with the directory separator
  character, then the default directories are searched after the
  explicitly specified directories. For example, setting
  *SANE_CONFIG_DIR* to "/tmp/config:" would result in directories
  /tmp/config/, /./, and //etc/sane.d/ being searched (in this order).

- *SANE_DEBUG_PLUSTEK* :: If the library was compiled with debug support
  enabled, this environment variable controls the debug level for this
  backend. Higher debug levels increase the verbosity of the output.

Example: export SANE_DEBUG_PLUSTEK=10

* SEE ALSO
*sane*(7), *sane-usb*(5), *sane-u12*(5), *sane-gt68xx*(5),
*sane-stek_pp*(5), *sane-find-scanner*(1), *scanimage*(1),\\
//usr/share/doc/sane/plustek/Plustek-USB.changes/

* CONTACT AND BUG-REPORTS
Please send any information and bug-reports to:\\
*SANE Mailing List*

Additional info and hints can be obtained from our\\
Mailing-List archive at:\\
/http://www.sane-project.org/mailing-lists.html/

To obtain debug messages from the backend, please set the
environment-variable *SANE_DEBUG_PLUSTEK* before calling your favorite
scan-frontend (i.e. *scanimage*(1)),*i.e.:*

\\
/export SANE_DEBUG_PLUSTEK=20 ; scanimage/

The value controls the verbosity of the backend. Please note, that
values greater than 24 force the backend to output raw data files, which
could be rather large. The ending of these files is ".raw". For problem
reports it should be enough the set the verbosity to 13.

* KNOWN BUGS & RESTRICTIONS
* The driver does not support these manic scalings up to 16 times the
physical resolution. The only scaling is done on resolutions between the
physical resolution of the CCD-/CIS-sensor and the stepper motor i.e.
you have a 600x1200 dpi scanner and you are scanning using 800dpi, so
scaling is necessary, because the sensor only delivers 600dpi but the
motor is capable to perform 1200dpi steps.

* Plusteks' model policy is somewhat inconsistent. They sell technically
different devices under the same product name. Therefore it is possible
that some devices like the UT12 or U12 won't work - please check the
model list above and compare the product-id to the one your device has.

* Negative/Slide scanning quality is poor.
