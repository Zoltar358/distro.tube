#+TITLE: Manpages - protocols.5
#+DESCRIPTION: Linux manpage for protocols.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
protocols - protocols definition file

* DESCRIPTION
This file is a plain ASCII file, describing the various DARPA internet
protocols that are available from the TCP/IP subsystem. It should be
consulted instead of using the numbers in the ARPA include files, or,
even worse, just guessing them. These numbers will occur in the protocol
field of any IP header.

Keep this file untouched since changes would result in incorrect IP
packages. Protocol numbers and names are specified by the IANA (Internet
Assigned Numbers Authority).

Each line is of the following format:

#+begin_quote
  /protocol number aliases .../
#+end_quote

where the fields are delimited by spaces or tabs. Empty lines are
ignored. If a line contains a hash mark (#), the hash mark and the part
of the line following it are ignored.

The field descriptions are:

- /protocol/ :: the native name for the protocol. For example /ip/,
  /tcp/, or /udp/.

- /number/ :: the official number for this protocol as it will appear
  within the IP header.

- /aliases/ :: optional aliases for the protocol.

This file might be distributed over a network using a network-wide
naming service like Yellow Pages/NIS or BIND/Hesiod.

* FILES
- //etc/protocols/ :: The protocols definition file.

* SEE ALSO
*getprotoent*(3)

[[http://www.iana.org/assignments/protocol-numbers][]]

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
