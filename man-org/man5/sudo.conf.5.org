#+TITLE: Manpages - sudo.conf.5
#+DESCRIPTION: Linux manpage for sudo.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
The

file is used to configure the

front end. It specifies the security policy and I/O logging plugins,
debug flags as well as plugin-agnostic path names and settings.

The

file supports the following directives, described in detail below.

a security policy or I/O logging plugin

a plugin-agnostic path

a front end setting, such as

or

debug flags to aid in debugging

and the

plugin.

The pound sign

is used to indicate a comment. Both the comment character and any text
after it, up to the end of the line, are ignored.

Long lines can be continued with a backslash

as the last character on the line. Note that leading white space is
removed from the beginning of lines even when the continuation character
is used.

Non-comment lines that don't begin with

or

are silently ignored.

The

file is always parsed in the

locale.

supports a plugin architecture for security policies and input/output
logging. Third parties can develop and distribute their own policy and
I/O logging plugins to work seamlessly with the

front end. Plugins are dynamically loaded based on the contents of

A

line consists of the

keyword, followed by the

and the

to the dynamic shared object that contains the plugin. The

is the name of the

or

struct contained in the plugin. If a plugin implements multiple plugin
types, there must be a

line for each unique symbol name. The

may be fully qualified or relative. If not fully qualified, it is
relative to the directory specified by the

setting, which defaults to

In other words:

Plugin sudoers_policy sudoers.so

is equivalent to:

Plugin sudoers_policy /usr/lib/sudo/sudoers.so

If the plugin was compiled statically into the

binary instead of being installed as a dynamic shared object, the

should be specified without a leading directory, as it does not actually
exist in the file system. For example:

Plugin sudoers_policy sudoers.so

Starting with

1.8.5, any additional parameters after the

are passed as arguments to the plugin's

function. For example, to override the compile-time default sudoers file
mode:

Plugin sudoers_policy sudoers.so sudoers_mode=0440

See the

manual for a list of supported arguments.

The same dynamic shared object may contain multiple plugins, each with a
different symbol name. The file must be owned by uid 0 and only writable
by its owner. Because of ambiguities that arise from composite policies,
only a single policy plugin may be specified. This limitation does not
apply to I/O plugins.

If no

file is present, or if it contains no

lines, the

plugin will be used as the default security policy, for I/O logging (if
enabled by the policy) and for auditing. This is equivalent to the
following:

Plugin sudoers_policy sudoers.so Plugin sudoers_io sudoers.so Plugin
sudoers_audit sudoers.so

Starting with

version 1.9.1, some of the logging functionality of the

plugin has been moved from the policy plugin to an audit plugin. To
maintain compatibility with

files from older

versions, if

is configured as the security policy, it will be used as an audit plugin
as well. This guarantees that the logging behavior will be consistnet
with that of

versions 1.9.0 and below.

For more information on the

plugin architecture, see the

manual.

A

line consists of the

keyword, followed by the name of the path to set and its value. For
example:

Path intercept /usr/lib/sudo/sudo_intercept.so Path noexec
/usr/lib/sudo/sudo_noexec.so Path askpass /usr/X11R6/bin/ssh-askpass

If no path name is specified, features relying on the specified setting
will be disabled. Disabling

settings is only supported in

version 1.8.16 and higher.

The following plugin-agnostic paths may be set in the

file:

The fully qualified path to a helper program used to read the user's
password when no terminal is available. This may be the case when

is executed from a graphical (as opposed to text-based) application. The
program specified by

should display the argument passed to it as the prompt and write the
user's password to the standard output. The value of

may be overridden by the

environment variable.

An ordered, colon-separated search path of directories to look in for
device nodes. This is used when mapping the process's tty device number
to a device name on systems that do not provide such a mechanism. Sudo
will

recurse into sub-directories. If terminal devices may be located in a
sub-directory of

that path must be explicitly listed in

The default value is

This option is ignored on systems that support either the

or

functions, for example

macOS and Solaris.

The fully-qualified path to a shared library containing a wrappers for
the

and

library functions that intercepts attempts to run further commands and
performs a policy check before allowing them to be executed. This is
used to implement the

functionality on systems that support

or its equivalent. The default value is

The fully-qualified path to a shared library containing wrappers for the

and

library functions that prevent the execution of further commands. This
is used to implement the

functionality on systems that support

or its equivalent. The default value is

The default directory to use when searching for plugins that are
specified without a fully qualified path name. The default value is

The

file also supports the following front end settings:

Core dumps of

itself are disabled by default to prevent the disclosure of potentially
sensitive information. To aid in debugging

crashes, you may wish to re-enable core dumps by setting

to false in

as follows:

Set disable_coredump false

All modern operating systems place restrictions on core dumps from
set-user-ID processes like

so this option can be enabled without compromising security. To actually
get a

core file you will likely need to enable core dumps for set-user-ID
processes. On

and Linux systems this is accomplished in the

command. On Solaris, the

command is used to configure core dump behavior.

This setting is only available in

version 1.8.4 and higher.

By default

refuses to load plugins which can be modified by other than the root
user. The plugin should be owned by root and write access permissions
should be disabled for

and

.

To make development of a plugin easier, you can disable that by setting

option to true in

as follows:

Set developer_mode true

Please note that this creates a security risk, so it is not recommended
on critical systems such as a desktop machine for daily use, but is
intended to be used in development environments (VM, container, etc).
Before enabling developer mode, ensure you understand the implications.

This setting is only available in

version 1.9.0 and higher.

passes the invoking user's group list to the policy and I/O plugins. On
most systems, there is an upper limit to the number of groups that a
user may belong to simultaneously (typically 16 for compatibility with
NFS). On systems with the

utility, running:

will return the maximum number of groups.

However, it is still possible to be a member of a larger number of
groups--they simply won't be included in the group list returned by the
kernel for the user. Starting with

version 1.8.7, if the user's kernel group list has the maximum number of
entries,

will consult the group database directly to determine the group list.
This makes it possible for the security policy to perform matching by
group name even when the user is a member of more than the maximum
number of groups.

The

setting allows the administrator to change this default behavior.
Supported values for

are:

Use the static group list that the kernel returns. Retrieving the group
list this way is very fast but it is subject to an upper limit as
described above. It is

in that it does not reflect changes to the group database made after the
user logs in. This was the default behavior prior to

1.8.7.

Always query the group database directly. It is

in that changes made to the group database after the user logs in will
be reflected in the group list. On some systems, querying the group
database for all of a user's groups can be time consuming when querying
a network-based group database. Most operating systems provide an
efficient method of performing such queries. Currently,

supports efficient group queries on AIX,

HP-UX, Linux, macOS and Solaris. This is the default behavior on macOS
in

1.9.6 and higher.

Only query the group database if the static group list returned by the
kernel has the maximum number of entries. This is the default behavior
on systems other than macOS in

1.8.7 and higher.

For example, to cause

to only use the kernel's static list of groups for the user:

Set group_source static

This setting is only available in

version 1.8.7 and higher.

The maximum number of user groups to retrieve from the group database.
Values less than one or larger than 1024 will be ignored. This setting
is only used when querying the group database directly. It is intended
to be used on systems where it is not possible to detect when the array
to be populated with group entries is not sufficiently large. By
default,

will allocate four times the system's maximum number of groups (see
above) and retry with double that number if the group database query
fails.

This setting is only available in

version 1.8.7 and higher. It should not be required in

versions 1.8.24 and higher and may be removed in a later release.

By default,

will probe the system's network interfaces and pass the IP address of
each enabled interface to the policy plugin. This makes it possible for
the plugin to match rules based on the IP address without having to
query DNS. On Linux systems with a large number of virtual interfaces,
this may take a non-negligible amount of time. If IP-based matching is
not required, network interface probing can be disabled as follows:

Set probe_interfaces false

This setting is only available in

version 1.8.10 and higher.

versions 1.8.4 and higher support a flexible debugging framework that
can log what

is doing internally if there is a problem.

A

line consists of the

keyword, followed by the name of the program, plugin or shared object to
debug, the debug file name and a comma-separated list of debug flags.
The debug flag syntax used by

the

plugin and its associated programs and shared objects is

but a third-party plugin is free to use a different format so long as it
does not include a comma

Examples:

Debug sudo /var/log/sudo_debug all@warn,plugin@info

would log all debugging statements at the

level and higher in addition to those at the

level for the plugin subsystem.

Debug sudo_intercept.so /var/log/intercept_debug all@debug

would log all debugging statements, regardless of level, for the

shared object that implements

intercept functionality.

As of

1.8.12, multiple

entries may be specified per program. Older versions of

only support a single

entry per program. Plugin-specific

entries are also supported starting with

1.8.12 and are matched by either the base name of the plugin that was
loaded (for example

or by the plugin's fully-qualified path name. Previously, the

plugin shared the same

entry as the

front end and could not be configured separately.

The following priorities are supported, in order of decreasing severity:

and

Each priority, when specified, also includes all priorities higher than
it. For example, a priority of

would include debug messages logged at

and higher.

The priorities

and

also include function call tracing which logs when a function is entered
and when it returns. For example, the following trace is for the

function located in src/sudo.c:

sudo[123] -> get_user_groups @ src/sudo.c:385 sudo[123] <-
get_user_groups @ src/sudo.c:429 := groups=10,0,5

When the function is entered, indicated by a right arrow

the program, process ID, function, source file and line number are
logged. When the function returns, indicated by a left arrow

the same information is logged along with the return value. In this
case, the return value is a string.

The following subsystems are used by the

front-end:

matches every subsystem

command line argument processing

user conversation

sudoedit

event subsystem

command execution

main function

network interface handling

communication with the plugin

plugin configuration

pseudo-terminal related code

SELinux-specific handling

utility functions

utmp handling

The

plugin includes support for additional subsystems.

front end configuration

# # Default /etc/sudo.conf file # # Sudo plugins: # Plugin plugin_name
plugin_path plugin_options ... # # The plugin_path is relative to
/usr/lib/sudo unless # fully qualified. # The plugin_name corresponds to
a global symbol in the plugin # that contains the plugin interface
structure. # The plugin_options are optional. # # The sudoers plugin is
used by default if no Plugin lines are present. #Plugin sudoers_policy
sudoers.so #Plugin sudoers_io sudoers.so #Plugin sudoers_audit
sudoers.so

# # Sudo askpass: # Path askpass /path/to/askpass # # An askpass helper
program may be specified to provide a graphical # password prompt for
"sudo -A" support. Sudo does not ship with its # own askpass program but
can use the OpenSSH askpass. # # Use the OpenSSH askpass #Path askpass
/usr/X11R6/bin/ssh-askpass # # Use the Gnome OpenSSH askpass #Path
askpass /usr/libexec/openssh/gnome-ssh-askpass

# # Sudo device search path: # Path devsearch /dev/path1:/dev/path2:/dev
# # A colon-separated list of paths to check when searching for a user's
# terminal device. # #Path devsearch
/dev/pts:/dev/vt:/dev/term:/dev/zcons:/dev/pty:/dev

# # Sudo command interception: # Path intercept
/path/to/sudo_intercept.so # # Path to a shared library containing
replacements for the execv() # and execve() library functions that
perform a policy check to verify # the command is allowed and simply
return an error if not. This is # used to implement the "intercept"
functionality on systems that # support LD_PRELOAD or its equivalent. #
# The compiled-in value is usually sufficient and should only be changed
# if you rename or move the sudo_intercept.so file. # #Path intercept
/usr/lib/sudo/sudo_intercept.so

# # Sudo noexec: # Path noexec /path/to/sudo_noexec.so # # Path to a
shared library containing replacements for the execv() # family of
library functions that just return an error. This is # used to implement
the "noexec" functionality on systems that support # LD_PRELOAD or its
equivalent. # # The compiled-in value is usually sufficient and should
only be changed # if you rename or move the sudo_noexec.so file. # #Path
noexec /usr/lib/sudo/sudo_noexec.so

# # Sudo plugin directory: # Path plugin_dir /path/to/plugins # # The
default directory to use when searching for plugins that are # specified
without a fully qualified path name. # #Path plugin_dir /usr/lib/sudo

# # Sudo developer mode: # Set developer_mode true|false # # Allow
loading of plugins that are owned by non-root or are writable # by
"group" or "other". Should only be used during plugin development. #Set
developer_mode true

# # Core dumps: # Set disable_coredump true|false # # By default, sudo
disables core dumps while it is executing (they # are re-enabled for the
command that is run). # To aid in debugging sudo problems, you may wish
to enable core # dumps by setting "disable_coredump" to false. # #Set
disable_coredump false

# # User groups: # Set group_source static|dynamic|adaptive # # Sudo
passes the user's group list to the policy plugin. # If the user is a
member of the maximum number of groups (usually 16), # sudo will query
the group database directly to be sure to include # the full list of
groups. # # On some systems, this can be expensive so the behavior is
configurable. # The "group_source" setting has three possible values: #
static - use the user's list of groups returned by the kernel. #
dynamic - query the group database to find the list of groups. #
adaptive - if user is in less than the maximum number of groups. # use
the kernel list, else query the group database. # #Set group_source
static

# # Sudo interface probing: # Set probe_interfaces true|false # # By
default, sudo will probe the system's network interfaces and # pass the
IP address of each enabled interface to the policy plugin. # On systems
with a large number of virtual interfaces this may take # a noticeable
amount of time. # #Set probe_interfaces false

# # Sudo debug files: # Debug program /path/to/debug_log
subsystem@priority[,subsyste@priority] # # Sudo and related programs
support logging debug information to a file. # The program is typically
sudo, sudoers.so, sudoreplay or visudo. # # Subsystems vary based on the
program; "all" matches all subsystems. # Priority may be crit, err,
warn, notice, diag, info, trace or debug. # Multiple subsystem@priority
may be specified, separated by a comma. # #Debug sudo
/var/log/sudo_debug all@debug #Debug sudoers.so /var/log/sudoers_debug
all@debug

See the HISTORY file in the

distribution (https://www.sudo.ws/history.html) for a brief history of
sudo.

Many people have worked on

over the years; this version consists of code written primarily by:

See the CONTRIBUTORS file in the

distribution (https://www.sudo.ws/contributors.html) for an exhaustive
list of people who have contributed to

If you feel you have found a bug in

please submit a bug report at https://bugzilla.sudo.ws/

Limited free support is available via the sudo-users mailing list, see
https://www.sudo.ws/mailman/listinfo/sudo-users to subscribe or search
the archives.

is provided

and any express or implied warranties, including, but not limited to,
the implied warranties of merchantability and fitness for a particular
purpose are disclaimed. See the LICENSE file distributed with

or https://www.sudo.ws/license.html for complete details.

Information about sudo.conf.5 is found in manpage for: [[../sudoers_policy sudoers.so sudoers_mode=0440
/usr/lib/sudo/sudo_intercept.so .
/usr/lib/sudo/sudo_noexec.so .
sudo_intercept.so /var/log/intercept_debug all@debug
sudoers.so )
if you rename or move the sudo_intercept.so file.
if you rename or move the sudo_noexec.so file.
sudoers.so /var/log/sudoers_debug all@debug][sudoers_policy sudoers.so sudoers_mode=0440
/usr/lib/sudo/sudo_intercept.so .
/usr/lib/sudo/sudo_noexec.so .
sudo_intercept.so /var/log/intercept_debug all@debug
sudoers.so )
if you rename or move the sudo_intercept.so file.
if you rename or move the sudo_noexec.so file.
sudoers.so /var/log/sudoers_debug all@debug]]