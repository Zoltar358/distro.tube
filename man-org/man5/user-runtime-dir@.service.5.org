#+TITLE: Manpages - user-runtime-dir@.service.5
#+DESCRIPTION: Linux manpage for user-runtime-dir@.service.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about user-runtime-dir@.service.5 is found in manpage for: [[../user@.service.5][user@.service.5]]