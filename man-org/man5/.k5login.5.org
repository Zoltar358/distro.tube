#+TITLE: Manpages - .k5login.5
#+DESCRIPTION: Linux manpage for .k5login.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about .k5login.5 is found in manpage for: [[../man5/k5login.5][man5/k5login.5]]