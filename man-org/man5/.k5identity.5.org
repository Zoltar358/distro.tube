#+TITLE: Manpages - .k5identity.5
#+DESCRIPTION: Linux manpage for .k5identity.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about .k5identity.5 is found in manpage for: [[../man5/k5identity.5][man5/k5identity.5]]