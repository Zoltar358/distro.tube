#+TITLE: Manpages - faillog.5
#+DESCRIPTION: Linux manpage for faillog.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
faillog - login failure logging file

* DESCRIPTION
/var/log/faillog maintains a count of login failures and the limits for
each account.

The file contains fixed length records, indexed by numerical UID. Each
record contains the count of login failures since the last successful
login; the maximum number of failures before the account is disabled;
the line on which the last login failure occurred; the date of the last
login failure; and the duration (in seconds) during which the account
will be locked after a failure.

The structure of the file is:

#+begin_quote
  #+begin_example
    struct	faillog {
    	short   fail_cnt;
    	short   fail_max;
    	char    fail_line[12];
    	time_t  fail_time;
    	long    fail_locktime;
    };
  #+end_example
#+end_quote

* FILES
/var/log/faillog

#+begin_quote
  Failure logging file.
#+end_quote

* SEE ALSO
*faillog*(8)
