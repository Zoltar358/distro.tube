#+TITLE: Manpages - aura.conf.5
#+DESCRIPTION: Linux manpage for aura.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_quote
  #+begin_example
  #+end_example
#+end_quote

* NAME
aura.conf - aura package manager configuration file

* SYNOPSIS
/etc/aura.conf

* DESCRIPTION
Aura can be configured at runtime by commandline flags, but for users
who use the same options all the time (say, build paths or language
flags) this can be verbose. /aura.conf/ lets you make these common
settings permanent.

* EXAMPLE
Overview of options in /aura.conf/: # # aura.conf #

#[ Language ] language = en

#[ Build Settings ] user = <you> buildpath = /tmp vcspath =
/var/cache/aura/vcs

#[ Misc. ] editor = vi

* OPTIONS
- *language* :: Aura can be used in different human languages. This
  field must be a 2-letter language code, similar to those used in
  LOCALE. The available codes are:

| Code | Language   |
|      |            |
| nl   | Dutch      |
| en   | English    |
| de   | German     |
| nb   | Norwegian  |
| sv   | Swedish    |
|      |            |
| fr   | French     |
| it   | Italian    |
| pt   | Portuguese |
| es   | Spanish    |
|      |            |
| hr   | Croatian   |
| pl   | Polish     |
| ru   | Russian    |
| sr   | Serbian    |
| uk   | Ukrainian  |
|      |            |
| ar   | Arabic     |
|      |            |
| zh   | Chinese    |
| id   | Indonesian |
| ja   | Japanese   |
|      |            |
| eo   | Esperanto  |

- *user* :: The real, non-sudo user to build packages as.

- *buildpath* :: The location on the filesystem in which to build
  packages.

- *allsourcepath* :: The location on the filesystem in which to save
  "source packages" built with *--allsource*.

- *vcspath* :: The location on the filesystem in which to store the
  cloned repositories of /*-git/, etc., packages.

- *analyse* :: Aura automatically scans PKGBUILDs for malicious bash
  usage and other "bad practices". Set this to *False* to turn off.

- *editor* :: The editor to use during *--hotedit*.

* SEE ALSO
*aura*(8)

* AUTHOR
Colin Woodbury <colin@fosskers.ca>
