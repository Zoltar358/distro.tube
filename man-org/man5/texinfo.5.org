#+TITLE: Manpages - texinfo.5
#+DESCRIPTION: Linux manpage for texinfo.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
texinfo - software documentation system

* DESCRIPTION
Texinfo is a documentation system that uses a single source file to
produce both online information and printed output. It is primarily
designed for writing software manuals.

For a full description of the Texinfo language and associated tools,
please see the Texinfo manual (written in Texinfo itself). Most likely,
running this command from your shell:

#+begin_example
  info texinfo
#+end_example

or this key sequence from inside Emacs:

#+begin_example
  M-x info RET m texinfo RET
#+end_example

will get you there.

* AVAILABILITY
http://www.gnu.org/software/texinfo/\\
or any GNU mirror site.

* REPORTING BUGS
Please send bug reports to bug-texinfo@gnu.org, general questions and
discussion to help-texinfo@gnu.org.

* SEE ALSO
info(1), install-info(1), makeinfo(1), texi2dvi(1), texindex(1).\\
emacs(1), tex(1).\\
info(5).
