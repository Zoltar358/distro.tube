#+TITLE: Manpages - pppoe.conf.5
#+DESCRIPTION: Linux manpage for pppoe.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pppoe.conf - Configuration file used by *pppoe-start*(8),
*pppoe-stop*(8), *pppoe-status(8)* and *pppoe-connect*(8).

* DESCRIPTION
*/etc/ppp/pppoe.conf* is a shell script which contains configuration
information for RP-PPPoE scripts. Note that *pppoe.conf* is used only by
the various pppoe-* shell scripts, not by *pppoe* itself.

*pppoe.conf* consists of a sequence of shell variable assignments. The
variables and their meanings are:

- *ETH* :: The Ethernet interface connected to the DSL modem (for
  example, eth0).

- *USER* :: The PPPoE user-id (for example, b1xxnxnx@sympatico.ca).

- *SERVICENAME* :: If this is not blank, then it is passed with the *-S*
  option to *pppoe*. It specifies a service name to ask for. Usually,
  you should leave it blank.

- *ACNAME* :: If this is not blank, then it is passed with the *-C*
  option to *pppoe*. It specifies the name of the access concentrator to
  connect to. Usually, you should leave it blank.

- *DEMAND* :: If set to a number, the link is activated on demand and
  brought down after after *DEMAND* seconds. If set to *no*, the link is
  kept up all the time rather than being activated on demand.

- *DNSTYPE* :: One of *NOCHANGE*, *SPECIFY* or *SERVER*. If set to
  NOCHANGE, *pppoe-connect* will not adjust the DNS setup in any way. If
  set to SPECIFY, it will re-write /etc/resolv.conf with the values of
  DNS1 and DNS2. If set to *SERVER*, it will supply the /usepeerdns/
  option to *pppd*, and make a symlink from /etc/resolv.conf to
  /etc/ppp/resolv.conf.

- *DNS1, DNS2* :: IP addresses of DNS servers if you use
  DNSTYPE=SPECIFY.

- *NONROOT* :: If the line *NONROOT=OK* (exactly like that; no
  whitespace or comments) appears in the configuration file, then
  *pppoe-wrapper* will allow non-root users to bring the conneciton up
  or down. The wrapper is installed only if you installed the
  rp-pppoe-gui package.

- *USEPEERDNS* :: If set to "yes", then *pppoe-connect* will supply the
  /usepeerdns/ option to *pppd*, which causes it to obtain DNS server
  addresses from the peer and create a new */etc/resolv.conf* file.
  Otherwise, *pppoe-connect* will not supply this option, and *pppd*
  will not modify */etc/resolv.conf*.

- *CONNECT_POLL* :: How often (in seconds) *pppoe-start* should check to
  see if a new PPP interface has come up. If this is set to 0, the
  *pppoe-start* simply initiates the PPP session, but does not wait to
  see if it comes up successfully.

- *CONNECT_TIMEOUT* :: How long (in seconds) *pppoe-start* should wait
  for a new PPP interface to come up before concluding that
  *pppoe-connect* has failed and killing the session.

- *PING* :: A character which is echoed every *CONNECT_POLL* seconds
  while *pppoe-start* is waiting for the PPP interface to come up.

- *FORCEPING* :: A character which is echoed every *CONNECT_POLL*
  seconds while *pppoe-start* is waiting for the PPP interface to come
  up. Similar to *PING*, but the character is echoed even if
  *pppoe-start*'s standard output is not a tty.

- *PIDFILE* :: A file in which to write the process-ID of the
  pppoe-connect process (for example, */var/run/pppoe.pid*). Two
  additional files ($PIDFILE.pppd and $PIDFILE.pppoe) hold the
  process-ID's of the *pppd* and *pppoe* processes, respectively.

- *SYNCHRONOUS* :: An indication of whether or not to use synchronous
  PPP (*yes* or *no*). Synchronous PPP is safe on Linux machines with
  the n_hdlc line discipline. (If you have a file called "n_hdlc.o" in
  your modules directory, you have the line discipline.) It is /not/
  recommended on other machines or on Linux machines without the n_hdlc
  line discipline due to some known and unsolveable race conditions in a
  user-mode client.

- *CLAMPMSS* :: The value at which to "clamp" the advertised MSS for TCP
  sessions. The default of 1412 should be fine.

- *LCP_INTERVAL* :: How often (in seconds) *pppd* sends out LCP
  echo-request packets.

- *LCP_FAILURE* :: How many unanswered LCP echo-requests must occur
  before *pppd* concludes the link is dead.

- *PPPOE_TIMEOUT* :: If this many seconds elapse without any activity
  seen by *pppoe*, then *pppoe* exits.

- *FIREWALL* :: One of NONE, STANDALONE or MASQUERADE. If NONE, then
  *pppoe-connect* does not add any firewall rules. If STANDALONE, then
  it clears existing firewall rules and sets up basic rules for a
  standalone machine. If MASQUERADE, then it clears existing firewall
  rules and sets up basic rules for an Internet gateway. If you run
  services on your machine, these simple firewall scripts are
  inadequate; you'll have to make your own firewall rules and set
  FIREWALL to NONE.

- *PPPOE_EXTRA* :: Any extra arguments to pass to *pppoe*

- *PPPD_EXTRA* :: Any extra arguments to pass to *pppd*

- *LINUX_PLUGIN* :: If non-blank, the full path of the Linux kernel-mode
  PPPoE plugin (typically */etc/ppp/plugins/rp-pppoe.so*.) This forces
  *pppoe-connect* to use kernel-mode PPPoE on Linux 2.4.x systems. This
  code is experimental and unsupported. Use of the plugin causes
  *pppoe-connect* to ignore CLAMPMSS, PPPOE_EXTRA, SYNCHRONOUS and
  PPPOE_TIMEOUT.

By using different configuration files with different PIDFILE settings,
you can manage multiple PPPoE connections. Just specify the
configuration file as an argument to *pppoe-start* and *pppoe-stop*.

* SEE ALSO
pppoe(8), pppoe-connect(8), pppoe-start(8), pppoe-stop(8), pppd(8),
pppoe-setup(8), pppoe-wrapper(8)
