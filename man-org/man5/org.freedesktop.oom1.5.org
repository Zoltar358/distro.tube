#+TITLE: Manpages - org.freedesktop.oom1.5
#+DESCRIPTION: Linux manpage for org.freedesktop.oom1.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
org.freedesktop.oom1 - The D-Bus interface of systemd-oomd

* INTRODUCTION
*systemd-oomd.service*(8) is a system service which implements a
userspace out-of-memory (OOM) killer. This page describes the D-Bus
interface.

* THE MANAGER OBJECT
The service exposes the following interfaces on the Manager object on
the bus:

#+begin_quote
  #+begin_example
    node /org/freedesktop/oom1 {
      interface org.freedesktop.oom1.Manager {
        methods:
          DumpByFileDescriptor(out h fd);
      };
      interface org.freedesktop.DBus.Peer { ... };
      interface org.freedesktop.DBus.Introspectable { ... };
      interface org.freedesktop.DBus.Properties { ... };
    };
        
  #+end_example
#+end_quote

** Methods
...

* VERSIONING
These D-Bus interfaces follow *the usual interface versioning
guidelines*[1].

* NOTES
-  1. :: the usual interface versioning guidelines

  http://0pointer.de/blog/projects/versioning-dbus.html
