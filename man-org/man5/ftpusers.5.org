#+TITLE: Manpages - ftpusers.5
#+DESCRIPTION: Linux manpage for ftpusers.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ftpusers - list of users that may not log in via the FTP daemon

* DESCRIPTION
The text file *ftpusers* contains a list of users that may not log in
using the File Transfer Protocol (FTP) server daemon. This file is used
not merely for system administration purposes but also for improving
security within a TCP/IP networked environment.

The *ftpusers* file will typically contain a list of the users that
either have no business using ftp or have too many privileges to be
allowed to log in through the FTP server daemon. Such users usually
include root, daemon, bin, uucp, and news.

If your FTP server daemon doesn't use *ftpusers*, then it is suggested
that you read its documentation to find out how to block access for
certain users. Washington University FTP server Daemon (wuftpd) and
Professional FTP Daemon (proftpd) are known to make use of *ftpusers*.

** Format
The format of *ftpusers* is very simple. There is one account name (or
username) per line. Lines starting with a # are ignored.

* FILES
//etc/ftpusers/

* SEE ALSO
*passwd*(5), *proftpd*(8), *wuftpd*(8)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
