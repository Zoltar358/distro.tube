#+TITLE: Manpages - resolvconf.conf.5
#+DESCRIPTION: Linux manpage for resolvconf.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is the configuration file for

The

file is a shell script that is sourced by

meaning that

must contain valid shell commands. Listed below are the standard

variables that may be set. If the values contain whitespace, wildcards
or other special shell characters, ensure they are quoted and escaped
correctly. See the

variable for an example on quoting.

After updating this file, you may wish to run

to apply the new configuration.

When a dynamically generated list is appended or prepended to, the whole
is made unique where left-most wins.

Set to NO to disable

from running any subscribers. Defaults to YES.

If set, only these interfaces will be processed.

If set, these interfaces will not be processed.

These interfaces will always be processed first. If unset, defaults to
the following:-

lo lo[0-9]*

These interfaces will be processed next, unless they have a metric. If
unset, defaults to the following:-

tap[0-9]* tun[0-9]* vpn vpn[0-9]* wg[0-9]* ppp[0-9]* ippp[0-9]*

Ignore any exclusive marking for these interfaces. This is handy when
3rd party integrations force the

option and you want to disable it easily.

If unset, defaults to the following:-

127.* 0.0.0.0 255.255.255.255 ::1

Prepend search domains to the dynamically generated list.

Append search domains to the dynamically generated list.

A list of domains to be removed from consideration. To remove a domain,
you can use foo.* To remove a sub domain, you can use *.bar

Prepend name servers to the dynamically generated list. You should set
this to 127.0.0.1 if you use a local name server other than libc.

Append name servers to the dynamically generated list.

A list of name servers to be removed from consideration. The default is
0.0.0.0 as some faulty routers send it via DHCP. To remove a block, you
can use 192.168.*

These interfaces name servers will only be queried for the domains
listed in their resolv.conf. Useful for VPN domains. Setting

will stop the forwarding of the root zone and allows the local resolver
to recursively query the root servers directly. Requires a local
nameserver other than libc. This is equivalent to the

option.

Force these interface to be public, overriding the private marking. This
is handy when 3rd party integrations force the

option and you want to disable it easily.

Is a space separated list of replacement keywords. The syntax is this:

Example, given this resolv.conf:

domain foo.org search foo.org dead.beef nameserver 1.2.3.4 nameserver
2.3.4.5

and this configuaration:

replace="search/foo*/bar.com" replace="$replace
nameserver/1.2.3.4/5.6.7.8" replace="$replace nameserver/2.3.4.5/"

you would get this resolv.conf instead:

domain foo.org search bar.com nameserver 5.6.7.8

Works the same way as

except it works on each space separated value rather than the whole
line, so it's useful for the replacing a single domain within the search
directive. Using the same example resolv.conf and changing

to

you would get this resolv.conf instead:

domain foo.org search bar.com dead.beef nameserver 5.6.7.8

Override the default state directory of

This should not be changed once

is in use unless the old directory is copied to the new one.

The following variables affect

directly:-

Defaults to

if not set.

A list of libc resolver options, as specified in

When set to YES the latest resolv.conf is written to

without any alteration. When set to /dev/null or NULL,

is defaulted to NO,

is unset unless overridden and only the information set in

is written to

A libc resolver sortlist, as specified in

If a local name server is configured then the default is just to specify
that and ignore all other entries as they will be configured for the
local name server. Set this to NO to also list non-local nameservers.
This will give you working DNS even if the local nameserver stops
functioning at the expense of duplicated server queries.

Append name servers to the dynamically generated list.

Prepend name servers to the dynamically generated list.

Append search domains to the dynamically generated list.

Prepend search domains to the dynamically generated list.

openresolv ships with subscribers for the name servers

and

Each subscriber can create configuration files which should be included
in the subscribers main configuration file.

To disable a subscriber, simply set it's name to NO. For example, to
disable the libc subscriber you would set:

libc=NO

This file tells dnsmasq which name servers to use for specific domains.

This file tells dnsmasq which name servers to use for global lookups.

Example resolvconf.conf for dnsmasq:

name_servers=127.0.0.1 dnsmasq_conf=/etc/dnsmasq-conf.conf
dnsmasq_resolv=/etc/dnsmasq-resolv.conf

Example dnsmasq.conf:

listen-address=127.0.0.1 # If dnsmasq is compiled for DBus then we can
take # advantage of not having to restart dnsmasq. enable-dbus
conf-file=/etc/dnsmasq-conf.conf resolv-file=/etc/dnsmasq-resolv.conf

Include this file in the named options block. This file tells named
which name servers to use for global lookups.

Include this file in the named global scope, after the options block.
This file tells named which name servers to use for specific domains.

Example resolvconf.conf for named:

name_servers=127.0.0.1 named_options=/etc/named-options.conf
named_zones=/etc/named-zones.conf

Example named.conf:

options { listen-on { 127.0.0.1; }; include "/etc/named-options.conf";
};

include "/etc/named-zones.conf";

This is the main pdnsd configuration file which we modify to add our
forward domains to. If this variable is not set then we rely on the
pdnsd configuration file setup to read

as documented below.

This file tells pdnsd about global name servers. If this variable is not
set then it's written to

Example resolvconf.conf for pdnsd:

name_servers=127.0.0.1 pdnsd_conf=/etc/pdnsd.conf #
pdnsd_resolv=/etc/pdnsd-resolv.conf

Example pdnsd.conf:

global { server_ip = 127.0.0.1; status_ctl = on; } server { # A server
definition is required, even if empty. label="empty"; proxy_only=on; #
file="/etc/pdnsd-resolv.conf"; }

This file tells pdns_recursor about specific and global name servers.

Example resolvconf.conf for pdns_recursor:

name_servers=127.0.0.1 pdns_zones=/etc/pdns/recursor-zones.conf

Example recursor.conf:

allow-from=127.0.0.0/8, ::1/128
forward-zones-file=/etc/pdns/recursor-zones.conf

This file tells unbound about specific and global name servers.

When set to YES, unbound marks the domains as insecure, thus ignoring
DNSSEC.

Example resolvconf.conf for unbound:

name_servers=127.0.0.1 unbound_conf=/etc/unbound-resolvconf.conf

Example unbound.conf:

include: /etc/unbound-resolvconf.conf

Not all distributions store the files the subscribers need in the same
locations. For example, named service scripts have been called named,
bind and rc.bind and they could be located in a directory called
/etc/rc.d, /etc/init.d or similar. Each subscriber attempts to
automatically configure itself, but not every distribution has been
catered for. Also, users could equally want to use a different version
from the one installed by default, such as bind8 and bind9. To
accommodate this, the subscribers have these files in configurable
variables, documented below.

Name of the dnsmasq service.

Command to restart the dnsmasq service.

Location of the dnsmasq pidfile.

Name of the libc service.

Command to restart the libc service.

Name of the named service.

Command to restart the named service.

Command to restart the pdnsd service.

Command to restart the pdns_recursor service.

Command to restart the pdns_recursor service.

Name of the unbound service.

Command to restart the unbound service.

Location of the unbound pidfile.

Each distribution is a special snowflake and likes to name the same
thing differently, namely the named service script.

Please report them to
