#+TITLE: Manpages - loader.conf.5
#+DESCRIPTION: Linux manpage for loader.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
loader.conf - Configuration file for systemd-boot

* SYNOPSIS
/ESP//loader/loader.conf, /ESP//loader/entries/*.conf

* DESCRIPTION
*systemd-boot*(7) will read /ESP//loader/loader.conf and any files with
the ".conf" extension under /ESP//loader/entries/ on the EFI system
partition (ESP).

Each configuration file must consist of an option name, followed by
whitespace, and the option value. "#" may be used to start a comment
line. Empty and comment lines are ignored.

Boolean arguments may be written as "yes"/"y"/"true"/"1" or
"no"/"n"/"false"/"0".

* OPTIONS
The following configuration options in loader.conf are understood:

default

#+begin_quote
  A glob pattern to select the default entry. The default entry may be
  changed in the boot menu itself, in which case the name of the
  selected entry will be stored as an EFI variable, overriding this
  option.
#+end_quote

timeout

#+begin_quote
  How long the boot menu should be shown before the default entry is
  booted, in seconds. This may be changed in the boot menu itself and
  will be stored as an EFI variable in that case, overriding this
  option.

  If the timeout is disabled, the default entry will be booted
  immediately. The menu can be shown by pressing and holding a key
  before systemd-boot is launched.
#+end_quote

console-mode

#+begin_quote
  This option configures the resolution of the console. Takes a number
  or one of the special values listed below. The following values may be
  used:

  0

  #+begin_quote
    Standard UEFI 80x25 mode
  #+end_quote

  1

  #+begin_quote
    80x50 mode, not supported by all devices
  #+end_quote

  2

  #+begin_quote
    the first non-standard mode provided by the device firmware, if any
  #+end_quote

  auto

  #+begin_quote
    Pick a suitable mode automatically using heuristics
  #+end_quote

  max

  #+begin_quote
    Pick the highest-numbered available mode
  #+end_quote

  keep

  #+begin_quote
    Keep the mode selected by firmware (the default)
  #+end_quote
#+end_quote

editor

#+begin_quote
  Takes a boolean argument. Enable (the default) or disable the editor.
  The editor should be disabled if the machine can be accessed by
  unauthorized persons.
#+end_quote

auto-entries

#+begin_quote
  Takes a boolean argument. Enable (the default) or disable entries for
  other boot entries found on the boot partition. In particular, this
  may be useful when loader entries are created to show replacement
  descriptions for those entries.
#+end_quote

auto-firmware

#+begin_quote
  Takes a boolean argument. Enable (the default) or disable the "Reboot
  into firmware" entry.
#+end_quote

random-seed-mode

#+begin_quote
  Takes one of "off", "with-system-token" and "always". If "off" no
  random seed data is read off the ESP, nor passed to the OS. If
  "with-system-token" (the default) *systemd-boot* will read a random
  seed from the ESP (from the file /loader/random-seed) only if the
  /LoaderSystemToken/ EFI variable is set, and then derive the random
  seed to pass to the OS from the combination. If "always" the boot
  loader will do so even if /LoaderSystemToken/ is not set. This mode is
  useful in environments where protection against OS image reuse is not
  a concern, and the random seed shall be used even with no further
  setup in place. Use *bootctl random-seed* to initialize both the
  random seed file in the ESP and the system token EFI variable.

  See *Random Seeds*[1] for further information.
#+end_quote

* EXAMPLE

#+begin_quote
  #+begin_example
    # /boot/efi/loader/loader.conf
    timeout 0
    default 01234567890abcdef1234567890abdf0-*
    editor no
        
  #+end_example
#+end_quote

The menu will not be shown by default (the menu can still be shown by
pressing and holding a key during boot). One of the entries with files
with a name starting with "01234567890abcdef1234567890abdf0-" will be
selected by default. If more than one entry matches, the one with the
highest priority will be selected (generally the one with the highest
version number). The editor will be disabled, so it is not possible to
alter the kernel command line.

* SEE ALSO
*systemd-boot*(7), *bootctl*(1)

* NOTES
-  1. :: Random Seeds

  https://systemd.io/RANDOM_SEEDS
