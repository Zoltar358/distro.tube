#+TITLE: Manpages - BUILDINFO.5
#+DESCRIPTION: Linux manpage for BUILDINFO.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
BUILDINFO - Makepkg package build information file

* SYNOPSIS
This manual page describes the format of a BUILDINFO file found in the
root of a package created by makepkg. The file contains a description of
the package's build environment. The information is formatted in
key-value pairs separated by a /=/, one value per line. Arrays are
represented multiple keys with the same value.

* DESCRIPTION
This is a description of the contents of version /1/ of the BUILDINFO
file format.

*format*

#+begin_quote
  Denotes the file format version, represented by a plain positive
  integer.
#+end_quote

*pkgname*

#+begin_quote
  The name of the package.
#+end_quote

*pkgbase*

#+begin_quote
  The base name of a package, usually the same as the pkgname except for
  split packages.
#+end_quote

*pkgver*

#+begin_quote
  The version of the package including pkgrel and epoch.
#+end_quote

*pkgarch*

#+begin_quote
  The architecture of the package.
#+end_quote

*pkgbuild_sha256sum*

#+begin_quote
  The sha256sum in hex format of the PKGBUILD used to build the package.
#+end_quote

*packager*

#+begin_quote
  The details of the packager that built the package.
#+end_quote

*builddate*

#+begin_quote
  The build date of the package in epoch.
#+end_quote

*builddir*

#+begin_quote
  The directory where the package was built.
#+end_quote

*startdir*

#+begin_quote
  The directory from which makepkg was executed.
#+end_quote

*buildenv (array)*

#+begin_quote
  The build environment specified in makepkg.conf.
#+end_quote

*options (array)*

#+begin_quote
  The options set specified when building the package.
#+end_quote

*installed (array)*

#+begin_quote
  The installed packages at build time including the version information
  of the package. Formatted as "$pkgname-$pkgver-$pkgrel-$pkgarch".
#+end_quote

* SEE ALSO
*makepkg*(8), *pacman*(8), *makepkg.conf*(5)

See the pacman website at https://archlinux.org/pacman/ for current
information on pacman and its related tools.

* BUGS
Bugs? You must be kidding; there are no bugs in this software. But if we
happen to be wrong, submit a bug report with as much detail as possible
at the Arch Linux Bug Tracker in the Pacman section.

* AUTHORS
Current maintainers:

#+begin_quote
  ·

  Allan McRae <allan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Andrew Gregory <andrew.gregory.8@gmail.com>
#+end_quote

#+begin_quote
  ·

  Eli Schwartz <eschwartz@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Morgan Adamiec <morganamilo@archlinux.org>
#+end_quote

Past major contributors:

#+begin_quote
  ·

  Judd Vinet <jvinet@zeroflux.org>
#+end_quote

#+begin_quote
  ·

  Aurelien Foret <aurelien@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Aaron Griffin <aaron@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dan McGee <dan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Xavier Chantry <shiningxc@gmail.com>
#+end_quote

#+begin_quote
  ·

  Nagy Gabor <ngaba@bibl.u-szeged.hu>
#+end_quote

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the pacman.git
repository.
