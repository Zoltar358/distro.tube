#+TITLE: Manpages - netctl.profile.5
#+DESCRIPTION: Linux manpage for netctl.profile.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
netctl.profile - Profile options

* SYNOPSIS
netctl.profile

* DESCRIPTION
Profiles for netctl live under //etc/netctl// and are plain text files.
The files consist of variable definitions following the bash shell
syntax and are not expected to execute any code. It is good to omit as
much quoting as possible. For a few WPA-related variables, special
quoting rules (see below) apply.

The name of the profile is the name of the file. Profile names must not
contain newlines and should not end in /.action/, /.conf/, or
/.service/. Whenever a profile is read, all executable scripts in
//etc/netctl/hooks// and any executable script in
//etc/netctl/interfaces// with the name of the interface for the profile
are sourced. The name of the current profile is available via the
/Profile/ variable in the environment of the sourced scripts. On
starting or stopping a profile, the respective command is available via
the /Command/ variable. Declarations in an interface script override
declarations in a profile, which override declarations in hooks. For
each connection type, there are example profile files in
//etc/netctl/examples//.

* AVAILABLE CONNECTION TYPES
ethernet

#+begin_quote
  For wired connections.
#+end_quote

wireless

#+begin_quote
  For wireless connections. This connection type requires
  *wpa_supplicant* to be available.
#+end_quote

bond

#+begin_quote
  For bonded interfaces.
#+end_quote

bridge

#+begin_quote
  For bridge interfaces.
#+end_quote

dummy

#+begin_quote
  For dummy interfaces.
#+end_quote

ppp

#+begin_quote
  For PPP connections (currently only PPPoE).
#+end_quote

pppoe

#+begin_quote
  For PPPoE connections.
#+end_quote

mobile_ppp

#+begin_quote
  For mobile broadband PPP connections that use a USB modem.
#+end_quote

openvswitch

#+begin_quote
  For Open vSwitch bridge interfaces.
#+end_quote

tunnel

#+begin_quote
  For tunnel interfaces.
#+end_quote

tuntap

#+begin_quote
  For TUN/TAP interfaces.
#+end_quote

vlan

#+begin_quote
  For VLANs on ethernet-like connections.
#+end_quote

macvlan

#+begin_quote
  For MACVLANs on ethernet-like connections.
#+end_quote

wireguard

#+begin_quote
  For WireGuard interfaces.
#+end_quote

* GENERAL OPTIONS
/Description=/

#+begin_quote
  A description of the profile.
#+end_quote

/Connection=/ [mandatory for all profiles]

#+begin_quote
  The connection type used by the profile.
#+end_quote

/Interface=/ [mandatory for all profiles]

#+begin_quote
  The name of the associated network interface. The interface name
  should not be quoted.
#+end_quote

/BindsToInterfaces=()/

#+begin_quote
  An array of physical network interfaces that this profile needs before
  it can be started. For ‘enabled' profiles, *systemd* will wait for the
  presence of the specified interfaces before starting a profile. If
  this variable is not specified, it defaults to the value of
  /Interface=/.
#+end_quote

/MACAddress=/

#+begin_quote
  Optional MAC address for newly created interfaces. When set to the
  name of an existing interface, the address of that interface is used.
  The connection types that create an interface and are able to set a
  MAC address are bond, bridge, dummy, vlan, and macvlan.
#+end_quote

/After=()/

#+begin_quote
  An array of profile names that should be started before this profile
  is started. This is only an ordering dependency and is not intended to
  be a list of profiles that this profile requires. The meaning is the
  same as /After=/ in *systemd.unit*(5).
#+end_quote

/WaitOnline=/

#+begin_quote
  Set to ‘yes' to consider the profile activated only when it is online.
  Defaults to ‘no'.
#+end_quote

/ExecUpPost=/

#+begin_quote
  A command that is executed after a connection is established. If the
  specified command returns anything other than 0 (success), *netctl*
  will abort and stop the profile. If the command should be allowed to
  fail, add ‘|| true' to the end of it.
#+end_quote

/ExecDownPre=/

#+begin_quote
  A command that is executed before a connection is brought down.
  Similar precautions should be taken as with /ExecUpPost=/.
#+end_quote

/TimeoutUp/

#+begin_quote
  Maximum time, in seconds, to wait for an interface to get up. Defaults
  to ‘5'.
#+end_quote

/ForceConnect=/

#+begin_quote
  Set to ‘yes' to force connecting even if the interface is up. Do not
  use this unless you know what you are doing.
#+end_quote

/ExcludeAuto=/

#+begin_quote
  Whether or not to exclude this profile from automatic profile
  selection. Defaults to ‘no' for wireless and DHCP enabled connections
  and to ‘yes' otherwise.
#+end_quote

/NETCTL_DEBUG=/

#+begin_quote
  Set to ‘yes' to generate debugging output.
#+end_quote

* IP OPTIONS
These options apply to all connections that set up an IP-enabled
network. In particular, these connection types are ethernet, wireless,
bond, bridge, tunnel, tuntap, and vlan.

/IP=/ [mandatory for IPv4]

#+begin_quote
  One of ‘static', ‘dhcp', or ‘no', depending on the desired way of
  obtaining an address.
#+end_quote

/IP6=/ [mandatory for IPv6]

#+begin_quote
  One of ‘static', ‘stateless', ‘dhcp-noaddr', ‘dhcp', ‘no' or left out
  (empty) altogether. The difference between not specifying and setting
  to ‘no' is in the handling of /router advertisement/ packages, which
  is blocked by ‘no'.
#+end_quote

/Address=()/ [requires /IP=static/]

#+begin_quote
  An array of IP addresses suffixed with ‘/<netmask>'. Leaving out
  brackets for arrays consisting of a single element is accepted in the
  Bash syntax.
#+end_quote

/Gateway=/ [requires /IP=static/]

#+begin_quote
  An IP routing gateway address.
#+end_quote

/Routes=()/

#+begin_quote
  An array of custom routes of the form

  ‘*<address range>* via *<gateway>*'.
#+end_quote

/Address6=()/ [requires /IP6=static/ or /IP6=stateless/]

#+begin_quote
  An array of IPv6 addresses. Prefix length may be specified via
  ‘1234:bcd::11/64' syntax. It is possible to specify modifiers, in
  particular, ‘1234:bcd::11/64 nodad' disables Duplicate Address
  Detection for the address.
#+end_quote

/Gateway6=/ [requires /IP6=static/ or /IP6=stateless/]

#+begin_quote
  An IPv6 routing gateway address.
#+end_quote

/Routes6=()/

#+begin_quote
  An array of custom routes of the form

  ‘*<address range>* via *<gateway>*'.
#+end_quote

/DHCPClient=/ [requires /IP=dhcp/]

#+begin_quote
  The name of the DHCP client to use. Clients may accept additional
  options through client-specific variables. By default, *netctl* comes
  with support for ‘dhcpcd' and ‘dhclient'. Defaults to ‘dhcpcd'.
#+end_quote

/DHCP6Client=/ [requires /IP6=dhcp/ or /IP6=dhcp-noaddr/]

#+begin_quote
  The name of the DHCPv6 client to use. By default, ‘dhcpcd' and
  ‘dhclient' are supported. Defaults to ‘dhclient'.
#+end_quote

/DHCPReleaseOnStop=/

#+begin_quote
  Set to ‘yes' to release the DHCP lease when the profile is stopped.
#+end_quote

/IPCustom=()/

#+begin_quote
  An array of argument lines to pass to ip. This can be used to achieve
  complicated configurations within the framework of *netctl*.
#+end_quote

/Hostname=/

#+begin_quote
  A transient hostname for the system.
#+end_quote

/DNS=()/

#+begin_quote
  An array of DNS nameservers. Simply specify the IP addresses of each
  of the DNS nameservers.
#+end_quote

/DNSDomain=/

#+begin_quote
  A ‘domain' line for //etc/resolv.conf/, passed to *resolvconf*(5).
#+end_quote

/DNSSearch=/

#+begin_quote
  A ‘search' line for //etc/resolv.conf/, passed to *resolvconf*(5).
#+end_quote

/DNSOptions=()/

#+begin_quote
  An array of ‘options' lines for //etc/resolv.conf/, passed to
  *resolvconf*(5).
#+end_quote

/TimeoutDHCP=/

#+begin_quote
  Maximum time, in seconds, to wait for DHCP to be successful. Defaults
  to ‘30'.
#+end_quote

/TimeoutDAD=/

#+begin_quote
  Maximum time, in seconds, to wait for IPv6's Duplicate Address
  Detection to succeed. Defaults to ‘3'.
#+end_quote

/SkipDAD=/

#+begin_quote
  Whether or not to bypass Duplicate Address Detection altogether.
  Defaults to ‘no'.
#+end_quote

* OPTIONS FOR ‘ETHERNET' CONNECTIONS
Next to the *ip options*, the following are understood for connections
of the ‘ethernet' type:

/Auth8021X=/

#+begin_quote
  Set to ‘yes' to use 802.1x authentication.
#+end_quote

/WPAConfigFile=/

#+begin_quote
  Path to a *wpa_supplicant* configuration file. Defaults to
  //etc/wpa_supplicant.conf/.
#+end_quote

/WPADriver=/

#+begin_quote
  The *wpa_supplicant* driver to use for 802.1x authentication. Defaults
  to ‘wired'.
#+end_quote

/TimeoutCarrier=/

#+begin_quote
  Maximum time, in seconds, to wait for a carrier. Defaults to ‘5'.
#+end_quote

/TimeoutWPA=/

#+begin_quote
  Maximum time, in seconds, to wait for 802.1x authentication to
  succeed. Defaults to ‘15'.
#+end_quote

/SkipNoCarrier=/

#+begin_quote
  Whether or not the absence of a carrier (plugged-in cable) is
  acceptable. Defaults to ‘no'.
#+end_quote

/Priority=/

#+begin_quote
  Priority level of the profile. In case of automatic profile selection,
  profiles are tried in decreasing order of priority. Defaults to ‘1' in
  DHCP enabled profiles and to ‘0' otherwise.
#+end_quote

* OPTIONS FOR ‘WIRELESS' CONNECTIONS
Next to the *ip options*, the following are understood for connections
of the ‘wireless' type:

/Security=/

#+begin_quote
  One of ‘none', ‘wep', ‘wpa', ‘wpa-configsection', or ‘wpa-config'.
  Defaults to ‘none'.
#+end_quote

/ESSID=/ [mandatory]

#+begin_quote
  The name of the network to connect to. Special quoting rules (see
  below) apply.
#+end_quote

/AP=/

#+begin_quote
  The BSSID (MAC address) of the access point to connect to.
#+end_quote

/Key=/

#+begin_quote
  The secret key to a WEP, or WPA encrypted network. Special quoting
  rules (see below) apply.
#+end_quote

/Hidden=/

#+begin_quote
  Whether or not the specified network is a hidden network. Defaults to
  ‘no'.
#+end_quote

/AdHoc=/

#+begin_quote
  Whether or not to use ad-hoc mode. Defaults to ‘no'.
#+end_quote

/ScanFrequencies=/

#+begin_quote
  A space-separated list of frequencies in MHz to scan when searching
  for the network. Defaults to all available frequencies.
#+end_quote

/Frequency=/

#+begin_quote
  A frequency in MHz to use in ad-hoc mode when a new IBSS is created
  (i.e. the network is not already present).
#+end_quote

/Priority=/

#+begin_quote
  Priority group for the network. In case of automatic profile
  selection, the matched network with the highest priority will be
  selected. Defaults to ‘0'.
#+end_quote

/WPAConfigSection=()/ [mandatory for /Security=wpa-configsection/]

#+begin_quote
  Array of lines that form a network block for *wpa_supplicant*. All of
  the above options will be ignored.
#+end_quote

/WPAConfigFile=/

#+begin_quote
  Path to a *wpa_supplicant* configuration file. Used only for
  /Security=wpa-config/. All options except /WPADriver=/, /TimeoutWPA=/,
  and /RFKill=/ will be ignored. The profile is excluded from automatic
  profile selection. Defaults to //etc/wpa_supplicant.conf/.
#+end_quote

/Country=/

#+begin_quote
  The country for which frequency regulations will be enforced.
#+end_quote

/WPAGroup=/

#+begin_quote
  Group that has the authority to configure *wpa_supplicant* via its
  control interface. Defaults to ‘wheel'.
#+end_quote

/WPADriver=/

#+begin_quote
  The *wpa_supplicant* driver to use. Defaults to ‘nl80211,wext'.
#+end_quote

/TimeoutWPA=/

#+begin_quote
  Maximum time, in seconds, to wait for steps in the association and
  authentication to succeed. Defaults to ‘15'.
#+end_quote

/RFKill=/

#+begin_quote
  The name of an *rfkill* device. When specified, the device is used to
  block/unblock the interface when appropriate. Names can be found in
  //sys/class/rfkill/rfkillX/name/. It is also possible to set this
  variable to ‘auto'. In that case an *rfkill* device that is associated
  with the network interface is used.
#+end_quote

* OPTIONS FOR ‘BOND' CONNECTIONS
The interfaces of /BindsToInterfaces=/ are bound together in the
interface named by /Interface=/. Next to the *ip options*, the following
is understood for connections of the ‘bond' type:

/Mode=/

#+begin_quote
  The bonding policy. See the kernel documentation on bonding for
  details.
#+end_quote

/LinkOptions=/

#+begin_quote
  Additional options to be passed to *ip link*. Run *ip link add type
  bond help* to see the available options.
#+end_quote

* OPTIONS FOR ‘BRIDGE' CONNECTIONS
The interfaces of /BindsToInterfaces=/ take part in the bridge named by
/Interface=/. Next to the *ip options*, the following is understood for
connections of the ‘bridge' type:

/SkipForwardingDelay=/

#+begin_quote
  Skip (R)STP and immediately activate all bridge members. This can be
  useful when DHCP is used on the bridge.
#+end_quote

/LinkOptions=/

#+begin_quote
  Additional options to be passed to *ip link*. Run *ip link add type
  bridge help* to see the available options.
#+end_quote

* OPTIONS FOR ‘DUMMY' CONNECTIONS
The name of the dummy interface is specified in /Interface=/. Only the
*ip options* are understood for connections of the ‘dummy' type.

* OPTIONS FOR ‘PPP' CONNECTIONS
This connection type is identical to the ‘pppoe' type below, with the
ethernet interface specified in /BindsToInterfaces=/. The value of
/Interface=/ must be of the form ‘ppp<n>', where n is passed on to
/PPPUnit=/.

* OPTIONS FOR ‘PPPOE' CONNECTIONS
The interface to dial peer-to-peer over ethernet is specified in
/Interface=/. The following options are understood for connections of
the ‘pppoe' type:

/User=/ and /Password=/

#+begin_quote
  The username and password to connect with.
#+end_quote

/ConnectionMode=/

#+begin_quote
  This option specifies how a connection should be established, and may
  take either ‘persist' or ‘demand' as its argument.
#+end_quote

/IdleTimeout=/

#+begin_quote
  This option specifies the idle time (in seconds) after which ‘pppd'
  should disconnect. This option is only valid if /ConnectionMode=/ is
  set to ‘demand'.
#+end_quote

/MaxFail=/

#+begin_quote
  The number of consecutive failed connection attempts to tolerate. A
  value of 0 means no limit. Defaults to ‘5'.
#+end_quote

/DefaultRoute=/

#+begin_quote
  Use the default route provided by the peer (defaults to ‘true').
#+end_quote

/UsePeerDNS=/

#+begin_quote
  Use the DNS provided by the peer (defaults to ‘true').
#+end_quote

/PPPUnit=/

#+begin_quote
  Set the ppp unit number in the interface name (ppp0, ppp1, etc.).
#+end_quote

/LCPEchoInterval=/ and /LCPEchoFailure=/

#+begin_quote
  These options override default LCP parameters from ‘/etc/ppp/options'.
#+end_quote

/OptionsFile=/

#+begin_quote
  A file to read additional pppd options from.
#+end_quote

The following advanced options are also understood:

/PPPoEService=/

#+begin_quote
  This option specifies the PPPoE service name.
#+end_quote

/PPPoEAC=/

#+begin_quote
  This option specifies the PPPoE access concentrator name.
#+end_quote

/PPPoESession=/

#+begin_quote
  This option specifies an existing session to attach to, and is of the
  form ‘sessid:macaddr'.
#+end_quote

/PPPoEMAC=/

#+begin_quote
  Only connect to specified MAC address
#+end_quote

/PPPoEIP6=/

#+begin_quote
  Enable IPv6 support
#+end_quote

* OPTIONS FOR ‘MOBILE_PPP' CONNECTIONS
The name of the USB serial device is specified in /Interface=/. The
following options are understood for connections of the ‘mobile_ppp'
type:

/User=/ and /Password=/

#+begin_quote
  The username and password to connect with. These are unset by default,
  as they are often not required.
#+end_quote

/AccessPointName=/

#+begin_quote
  The access point (apn) to connect on. This is specific to your ISP.
#+end_quote

/Pin=/

#+begin_quote
  If your modem requires a PIN to unlock, set it here.
#+end_quote

/PhoneNumber/

#+begin_quote
  The number to dial. Defaults to ‘*99#'.
#+end_quote

/Mode=/

#+begin_quote
  This option is used to specify the connection mode. Can be one of
  ‘3Gpref', ‘3Gonly', ‘GPRSpref', ‘GPRSonly', ‘None', or a custom
  AT^SYSCFG=... line specified as ‘SYSCFG=...'. This generates AT
  commands specific to certain Huawei modems; all other devices should
  leave this option unset, or set it to ‘None'.
#+end_quote

/Init=/

#+begin_quote
  An initialization string sent to the modem before dialing. This string
  is sent after sending “ATZ”. Defaults to ‘ATQ0 V1 E1 S0=0 &C1 &D2
  +FCLASS=0'.
#+end_quote

/ChatScript=/

#+begin_quote
  Path to a chat file. If specified, no chat script will be generated
  and all of the above options except /User=/ and /Password=/ will be
  ignored.
#+end_quote

/MaxFail=/

#+begin_quote
  The number of consecutive failed connection attempts to tolerate. A
  value of 0 means no limit. Defaults to ‘5'.
#+end_quote

/DefaultRoute=/

#+begin_quote
  Use the default route provided by the peer. Defaults to ‘true'.
#+end_quote

/UsePeerDNS=/

#+begin_quote
  Use the DNS provided by the peer. Defaults to ‘true'.
#+end_quote

/PPPUnit=/

#+begin_quote
  Set the ppp unit number in the interface name (ppp0, ppp1, etc.).
#+end_quote

/OptionsFile=/

#+begin_quote
  A file to read additional pppd options from.
#+end_quote

/RFKill=/

#+begin_quote
  The name of an *rfkill* device. When specified, the device is used to
  block/unblock the interface when appropriate. Names can be found in
  //sys/class/rfkill/rfkillX/name/.
#+end_quote

* OPTIONS FOR ‘OPENVSWITCH' CONNECTIONS
The interfaces of /BindsToInterfaces=/ take part in the bridge named by
/Interface=/. Only the *ip options* are understood for connections of
the ‘openvswitch' type.

* OPTIONS FOR ‘TUNNEL' CONNECTIONS
The name of the tunnel interface is specified in /Interface=/. Next to
the *ip options*, the following are understood for connections of the
‘tunnel' type:

/Mode=/

#+begin_quote
  The tunnel type (e.g. ‘sit'). See *ip-tunnel*(8) for available modes.
#+end_quote

/Local=/

#+begin_quote
  The address of the local end of the tunnel.
#+end_quote

/Remote=/

#+begin_quote
  The address of the remote end of the tunnel.
#+end_quote

/Key=/ [requires /Mode=gre/]

#+begin_quote
  A key identifying an individual traffic flow within a tunnel.
#+end_quote

* OPTIONS FOR ‘TUNTAP' CONNECTIONS
The name of the tuntap interface is specified in /Interface=/. Next to
the *ip options*, the following are understood for connections of the
‘tuntap' type:

/Mode=/

#+begin_quote
  Either ‘tun', or ‘tap'.
#+end_quote

/User=/

#+begin_quote
  The owning user of the tun/tap interface.
#+end_quote

/Group=/

#+begin_quote
  The owning group of the tun/tap interface.
#+end_quote

* OPTIONS FOR ‘VLAN' CONNECTIONS
The name of the vlan interface is specified in /Interface=/. The
underlying physical interface is specified in /BindsToInterfaces=/.
Hence, for vlan profiles, /BindsToInterfaces=/ contains the name of a
single network interface.

All options for connections of the ‘ethernet' type are understood for
connections of the ‘vlan' type. Additionally, connections of the ‘vlan'
type must set a vlan identifier using /VLANID=/. See *ip*(8) for
details.

* OPTIONS FOR ‘MACVLAN' CONNECTIONS
The name of the macvlan interface is specified in /Interface=/. The
underlying physical interface is specified in /BindsToInterfaces=/.
Hence, for macvlan profiles, /BindsToInterfaces=/ contains the name of a
single network interface.

All options for connections of the ‘ethernet' type are understood for
connections of the ‘macvlan' type. Next to the *ip options*, the
following are understood for connections of the ‘macvlan' type:

/Mode=/

#+begin_quote
  Either ‘bridge', ‘vepa', ‘private', or ‘passthru'. See *ip*(8) for
  details.
#+end_quote

* OPTIONS FOR ‘WIREGUARD' CONNECTIONS
The name of the WireGuard interface is specified in /Interface=/. Next
to the *ip options*, the following are understood for connections of the
‘wireguard' type:

/WGConfigFile=/

#+begin_quote
  Path to a *WireGuard* configuration file. Defaults to
  //etc/wireguard/$Interface.conf/.
#+end_quote

* SPECIAL QUOTING RULES
Configuration files for *wpa_supplicant* use non-standard quoting.
Therefore, non-standard quoting rules exist for some variables for
connections of the ‘wireless' type. In particular, these variables are
/ESSID=/, and /Key=/.

A variable is considered *quoted* by *wpa_supplicant* if it is enclosed
in double quotes ("). A variable is considered *non-quoted* by
*wpa_supplicant* if it does not start with a double quote. Hexadecimal
values are specified *non-quoted* in configuration files of
*wpa_supplicant*. In *netctl*, variables are written to *wpa_supplicant*
configuration files *quoted* by default. When special quoting rules
apply, it is possible to specify an unquoted (hexadecimal) value using a
special syntax.

The special quoting rules of *netctl* are as follows. A string that
starts with a literal double quote is considered *non-quoted*. Any other
string is considered *quoted*. It is possible to specify quoted strings
that start with a double quote by quoting manually. An extreme example
is the specification of a *quoted* double quote: /X=""""/. On the other
end of the spectrum there is the *non-quoted* backslash: /X=\"\\/.

Further examples of *quoted* strings (all equivalent):

#+begin_quote
  #+begin_example
    X=string
    X="string"
    X=""string"
  #+end_example
#+end_quote

Further examples of *non-quoted* strings (all equivalent):

#+begin_quote
  #+begin_example
    X=\"string
    X="\"string"
    X="string
  #+end_example
#+end_quote

A mnemonic is to think of the prefix ‘\"' as saying ‘non'-‘quote'.

* SEE ALSO
*netctl*(1), *resolvconf.conf*(5)
