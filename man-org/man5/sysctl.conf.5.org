#+TITLE: Manpages - sysctl.conf.5
#+DESCRIPTION: Linux manpage for sysctl.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sysctl.conf - sysctl preload/configuration file

* DESCRIPTION
*sysctl.conf* is a simple file containing sysctl values to be read in
and set by *sysctl*. The syntax is simply as follows:

#+begin_quote
  #+begin_example
    # comment
    ; comment

    token = value
  #+end_example
#+end_quote

Note that blank lines are ignored, and whitespace before and after a
token or value is ignored, although a value can contain whitespace
within. Lines which begin with a /#/ or /;/ are considered comments and
ignored.

If a line begins with a single -, any attempts to set the value that
fail will be ignored.

* NOTES
As the */etc/sysctl.conf* file is used to override default kernel
parameter values, only a small number of parameters is predefined in the
file. Use //sbin/sysctl -a/ or follow *sysctl*(8) to list all possible
parameters. The description of individual parameters can be found in the
kernel documentation.

* EXAMPLE

#+begin_quote
  #+begin_example
    # sysctl.conf sample
    #
      kernel.domainname = example.com
    ; this one has a space which will be written to the sysctl!
      kernel.modprobe = /sbin/mod probe
  #+end_example
#+end_quote

* FILES
//etc/sysctl.d/*.conf/\\
//run/sysctl.d/*.conf/\\
//usr/local/lib/sysctl.d/*.conf/\\
//usr/lib/sysctl.d/*.conf/\\
//lib/sysctl.d/*.conf/\\
//etc/sysctl.conf/

The paths where *sysctl* preload files usually exist. See also *sysctl*
option /--system/.

* SEE ALSO
*sysctl*(8)

* AUTHOR
[[file:staikos@0wned.org][George Staikos]]

* REPORTING BUGS
Please send bug reports to [[file:procps@freelists.org][]]
