#+TITLE: Manpages - nologin.5
#+DESCRIPTION: Linux manpage for nologin.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
nologin - prevent unprivileged users from logging into the system

* DESCRIPTION
If the file //etc/nologin/ exists and is readable, *login*(1) will allow
access only to root. Other users will be shown the contents of this file
and their logins will be refused. This provides a simple way of
temporarily disabling all unprivileged logins.

* FILES
//etc/nologin/

* SEE ALSO
*login*(1), *shutdown*(8)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
