#+TITLE: Manpages - era_check.8
#+DESCRIPTION: Linux manpage for era_check.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*era_check *- validate era metadata on device or file.

* SYNOPSIS
#+begin_example
  era_check [options] {device|file}
#+end_example

* DESCRIPTION
*era_check checks era metadata created by the device-mapper era target
on a* device or file.

This tool cannot be run on live metadata.

* OPTIONS
- **-h, --help** :: Print help and exit.

- **-V, --version** :: Print version information and exit.

- **-q, --quiet** :: Suppress output messages, return only exit code.

- **--super-block-only** :: Only check the superblock is present.

* EXAMPLE
Analyse thin provisioning metadata on logical volume /dev/vg/metadata:

#+begin_example
      $ era_check /dev/vg/metadata
#+end_example

The device may not be actively used by the target when running.

* DIAGNOSTICS
*era_check returns an exit code of 0 for success or 1 for error.*

* SEE ALSO
*era_dump(8), era_restore(8), era_invalidate(8)*

* AUTHOR
Joe Thornber <ejt@redhat.com>
