#+TITLE: Manpages - nm-initrd-generator.8
#+DESCRIPTION: Linux manpage for nm-initrd-generator.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
nm-initrd-generator - early boot NetworkManager configuration generator

* SYNOPSIS
*nm-initrd-generator* [/OPTIONS/...] -- [/CMDLINE/...]

* DESCRIPTION
*nm-initrd-generator* scans the command line for options relevant to
network configuration and creates configuration files for an early
instance of NetworkManager run from the initial ramdisk during early
boot.

* OPTIONS
*-c* | *--connections-dir* /path/

#+begin_quote
  Output connection directory.
#+end_quote

*-i* | *--initrd-data-dir* /path/

#+begin_quote
  Output directory for initrd data (e.g. hostname).
#+end_quote

*-d* | *--sysfs-dir* /path/

#+begin_quote
  The sysfs mount point.
#+end_quote

*-r* | *--run-config-dir* /path/

#+begin_quote
  Output directory for config files.
#+end_quote

*-s* | *--stdout*

#+begin_quote
  Dump connections to standard output. Useful for debugging.
#+end_quote

/CMDLINE/

#+begin_quote
  The options that appear on the kernel command line. The following
  options are recognized:

  #+begin_quote
    *ip*
  #+end_quote

  #+begin_quote
    *rd.route*
  #+end_quote

  #+begin_quote
    *bridge*
  #+end_quote

  #+begin_quote
    *bond*
  #+end_quote

  #+begin_quote
    *team*
  #+end_quote

  #+begin_quote
    *vlan*
  #+end_quote

  #+begin_quote
    *ib.pkey*
  #+end_quote

  #+begin_quote
    *bootdev*
  #+end_quote

  #+begin_quote
    *nameserver*
  #+end_quote

  #+begin_quote
    *rd.peerdns*
  #+end_quote

  #+begin_quote
    *rd.bootif*
  #+end_quote

  #+begin_quote
    *rd.ethtool*
  #+end_quote

  #+begin_quote
    *rd.net.timeout.dhcp*
  #+end_quote

  #+begin_quote
    *rd.net.timeout.carrier*
  #+end_quote

  #+begin_quote
    *rd.znet*
  #+end_quote

  #+begin_quote
    *BOOTIF*
  #+end_quote

  Please consult the *dracut.cmdline*(7) manual for the documentation of
  the precise format of the values supported.
#+end_quote

* DIFFERENCES FROM THE NETWORK-LEGACY DRACUT MODULE
*nm-initrd-generator* generates a set of connections that are then
configured by the NetworkManager instance running in the initrd. There
are some differences in behavior compared to the network-legacy dracut
module:

#+begin_quote
  ·

  When an interface is configured with a static address and a gateway,
  the network-legacy module waits that the gateway responds to arping
  requests before proceeding, while NetworkManager doesnt.
#+end_quote

#+begin_quote
  ·

  network-legacy configures interfaces one by one in the order in which
  they are announced by udev. If multiple interfaces specify a hostname
  (from command line or from DHCP), the one from the last interface
  activated wins. With NetworkManager, hostnames from command line have
  higher precedence over ones from DHCP, and the last that appears in
  the command line wins.
#+end_quote

#+begin_quote
  ·

  NetworkManager supports the *ib.pkey*=/PARENT/./PKEY/ argument to set
  up an Infiniband partition on IPoIB parent device /PARENT/ using the
  specified partition key /PKEY/. The partition key must be in
  hexadecimal notation without leading "0x", for example
  "ib.pkey=ib0.8004".
#+end_quote

#+begin_quote
  ·

  NetworkManager supports the *rd.ethtool*=/INTERFACE/:/AUTONEG/:/SPEED/
  kernel command line option to set up ethtool NIC configuration
  parameters /AUTONEG/ and /SPEED/. The /INTERFACE/ being configured
  must be specified, and the other parameters are optional and can be
  left blank. When /SPEED/ is set, duplex mode is automatically set to
  full. /INTERFACE/ accepts string values, /AUTONEG/ accepts boolean
  values (true and false / on or off / 0 or 1), and /SPEED/ accepts
  positive integer values.
#+end_quote

* EXIT STATUS
*nm-initrd-generator* exits with status 0. It ignores unrecognized
options and prints an error message if it encounters a malformed option.

* SEE ALSO
*dracut.cmdline*(7), *NetworkManager*(8).
