#+TITLE: Manpages - rdma-statistic.8
#+DESCRIPTION: Linux manpage for rdma-statistic.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rdma-statistic - RDMA statistic counter configuration

* SYNOPSIS
*rdma* [ /OPTIONS/ ] *statistic* { /COMMAND/ | *help* }

*rdma statistic* { /OBJECT/ } *show*

*rdma statistic* [ /OBJECT/ ] *show link* [ /DEV/PORT_INDX/ ] [
/FILTER_NAME/ /FILTER_VALUE/ ]

*rdma statistic* /OBJECT/ *mode*

*rdma statistic* /OBJECT/ *set* /COUNTER_SCOPE/ [ /DEV/PORT_INDEX/ ]
*auto* { /CRITERIA/ | *off* }

*rdma statistic* /OBJECT/ *bind* /COUNTER_SCOPE/ [ /DEV/PORT_INDEX/ ] [
/OBJECT-ID/ ] [ /COUNTER-ID/ ]

*rdma statistic* /OBJECT/ *unbind* /COUNTER_SCOPE/ [ /DEV/PORT_INDEX/ ]
[ /COUNTER-ID/ ] [ /OBJECT-ID/ ]

/COUNTER_SCOPE/ := { *link* | *dev* }

/OBJECT/ := { *qp* | *mr* }

/CRITERIA/ := { *type* | *pid* }

/FILTER_NAME/ := { *cntn* | *lqpn* | *pid* | *qp-type* }

* DESCRIPTION
** rdma statistic [object] show - Queries the specified RDMA device for
RDMA and driver-specific statistics. Show the default hw counters if
object is not specified
/DEV/ - specifies counters on this RDMA device to show.

/PORT_INDEX/ - specifies counters on this RDMA port to show.

/"FILTER_NAME/ - specifies a filter to show only the results matching
it.

** rdma statistic <object> set - configure counter statistic auto-mode
for a specific device/port
In auto mode all objects belong to one category are bind automatically
to a single counter set. The "off" is global for all auto modes
together. Not applicable for MR's.

** rdma statistic <object> bind - manually bind an object (e.g., a qp)
with a counter
When bound the statistics of this object are available in this counter.
Not applicable for MR's.

** rdma statistic <object> unbind - manually unbind an object (e.g., a
qp) from the counter previously bound
When unbound the statistics of this object are no longer available in
this counter; And if object id is not specified then all objects on this
counter will be unbound. Not applicable for MR's.

/COUNTER-ID/ - specifies the id of the counter to be bound. If this
argument is omitted then a new counter will be allocated.

* EXAMPLES
rdma statistic show

#+begin_quote
  Shows the state of the default counter of all RDMA devices on the
  system.
#+end_quote

rdma statistic show link mlx5_2/1

#+begin_quote
  Shows the state of the default counter of specified RDMA port
#+end_quote

rdma statistic qp show

#+begin_quote
  Shows the state of all qp counters of all RDMA devices on the system.
#+end_quote

rdma statistic qp show link mlx5_2/1

#+begin_quote
  Shows the state of all qp counters of specified RDMA port.
#+end_quote

rdma statistic qp show link mlx5_2 pid 30489

#+begin_quote
  Shows the state of all qp counters of specified RDMA port and
  belonging to pid 30489
#+end_quote

rdma statistic qp show link mlx5_2 qp-type UD

#+begin_quote
  Shows the state of all qp counters of specified RDMA port and with QP
  type UD
#+end_quote

rdma statistic qp mode

#+begin_quote
  List current counter mode on all devices
#+end_quote

rdma statistic qp mode link mlx5_2/1

#+begin_quote
  List current counter mode of device mlx5_2 port 1
#+end_quote

rdma statistic qp set link mlx5_2/1 auto type on

#+begin_quote
  On device mlx5_2 port 1, for each new user QP bind it with a counter
  automatically. Per counter for QPs with same qp type.
#+end_quote

rdma statistic qp set link mlx5_2/1 auto pid on

#+begin_quote
  On device mlx5_2 port 1, for each new user QP bind it with a counter
  automatically. Per counter for QPs with same pid.
#+end_quote

rdma statistic qp set link mlx5_2/1 auto pid,type on

#+begin_quote
  On device mlx5_2 port 1, for each new user QP bind it with a counter
  automatically. Per counter for QPs with same pid and same type.
#+end_quote

rdma statistic qp set link mlx5_2/1 auto off

#+begin_quote
  Turn-off auto mode on device mlx5_2 port 1. The allocated counters can
  be manually accessed.
#+end_quote

rdma statistic qp bind link mlx5_2/1 lqpn 178

#+begin_quote
  On device mlx5_2 port 1, allocate a counter and bind the specified qp
  on it
#+end_quote

rdma statistic qp unbind link mlx5_2/1 cntn 4 lqpn 178

#+begin_quote
  On device mlx5_2 port 1, bind the specified qp on the specified
  counter
#+end_quote

rdma statistic qp unbind link mlx5_2/1 cntn 4

#+begin_quote
  On device mlx5_2 port 1, unbind all QPs on the specified counter.
  After that this counter will be released automatically by the kernel.
#+end_quote

rdma statistic show mr

#+begin_quote
  List all currently allocated MR's and their counters.
#+end_quote

rdma statistic show mr mrn 6

#+begin_quote
  Dump a specific MR statistics with mrn 6. Dumps nothing if does not
  exists.
#+end_quote

* SEE ALSO
*rdma*(8), *rdma-dev*(8), *rdma-link*(8), *rdma-resource*(8),\\

* AUTHORS
Mark Zhang <markz@mellanox.com>\\
Erez Alfasi <ereza@mellanox.com>
