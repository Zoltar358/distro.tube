#+TITLE: Manpages - exfatlabel.8
#+DESCRIPTION: Linux manpage for exfatlabel.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*exfatlabel* - get or set an exFAT file system label

* SYNOPSIS
*exfatlabel* [ *-V* ] /device/ [ /label/ ]

* DESCRIPTION
*exfatlabel* reads or changes an exFAT file system label (volume name).

If /label/ argument is present, *exfatlabel* sets the new volume name.
Empty label ('') removes volume name. Label can be up to 15 characters.
This limit is shorter if characters beyond Unicode BMP are used because
internally label is stored in UTF-16.

If /label/ argument is omitted, *exfatlabel* just prints current volume
name.

* COMMAND LINE OPTIONS
Command line options available:

- *-V* :: Print version and copyright.

* EXIT CODES
Zero is returned on success. Any other code means an error.

* AUTHOR
Andrew Nayenko

* SEE ALSO
*mkexfatfs*(8)
