#+TITLE: Manpages - vercmp.8
#+DESCRIPTION: Linux manpage for vercmp.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vercmp - version comparison utility

* SYNOPSIS
/vercmp/ [-h] [--help] <version1> <version2>

* DESCRIPTION
/vercmp/ is used to determine the relationship between two given version
numbers. It outputs values as follows:

#+begin_quote
  ·

  < 0 : if ver1 < ver2
#+end_quote

#+begin_quote
  ·

  = 0 : if ver1 == ver2
#+end_quote

#+begin_quote
  ·

  > 0 : if ver1 > ver2
#+end_quote

Version comparison operates as follows:

#+begin_quote
  #+begin_example
    Alphanumeric:
      1.0a < 1.0b < 1.0beta < 1.0p < 1.0pre < 1.0rc < 1.0 < 1.0.a < 1.0.1
    Numeric:
      1 < 1.0 < 1.1 < 1.1.1 < 1.2 < 2.0 < 3.0.0
  #+end_example
#+end_quote

Additionally, version strings can have an /epoch/ value defined that
will overrule any version comparison, unless the epoch values are equal.
This is specified in an epoch:version-rel format. For example, 2:1.0-1
is always greater than 1:3.6-1.

Keep in mind that the /pkgrel/ is only compared if it is available on
both versions given to this tool. For example, comparing 1.5-1 and 1.5
will yield 0; comparing 1.5-1 and 1.5-2 will yield < 0 as expected. This
is mainly for supporting versioned dependencies that do not include the
/pkgrel/.

* OPTIONS
*-h, --help*

#+begin_quote
  Display summary of the available return codes. Must be the first
  option specified.
#+end_quote

* EXAMPLES

#+begin_quote
  #+begin_example
    $ vercmp 1 2
    -1
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    $ vercmp 2 1
    1
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    $ vercmp 2.0-1 1.7-6
    1
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    $ vercmp 2.0 2.0-13
    0
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    $ vercmp 4.34 1:001
    -1
  #+end_example
#+end_quote

* SEE ALSO
*pacman*(8), *makepkg*(8), *libalpm*(3)

See the pacman website at https://archlinux.org/pacman/ for current
information on pacman and its related tools.

* BUGS
Bugs? You must be kidding; there are no bugs in this software. But if we
happen to be wrong, submit a bug report with as much detail as possible
at the Arch Linux Bug Tracker in the Pacman section.

* AUTHORS
Current maintainers:

#+begin_quote
  ·

  Allan McRae <allan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Andrew Gregory <andrew.gregory.8@gmail.com>
#+end_quote

#+begin_quote
  ·

  Eli Schwartz <eschwartz@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Morgan Adamiec <morganamilo@archlinux.org>
#+end_quote

Past major contributors:

#+begin_quote
  ·

  Judd Vinet <jvinet@zeroflux.org>
#+end_quote

#+begin_quote
  ·

  Aurelien Foret <aurelien@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Aaron Griffin <aaron@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dan McGee <dan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Xavier Chantry <shiningxc@gmail.com>
#+end_quote

#+begin_quote
  ·

  Nagy Gabor <ngaba@bibl.u-szeged.hu>
#+end_quote

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the pacman.git
repository.
