#+TITLE: Manpages - intro.8
#+DESCRIPTION: Linux manpage for intro.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
intro - introduction to administration and privileged commands

* DESCRIPTION
Section 8 of the manual describes commands which either can be or are
used only by the superuser, like system-administration commands,
daemons, and hardware-related commands.

As with the commands described in Section 1, the commands described in
this section terminate with an exit status that indicates whether the
command succeeded or failed. See *intro*(1) for more information.

* NOTES
** Authors and copyright conditions
Look at the header of the manual page source for the author(s) and
copyright conditions. Note that these can be different from page to
page!

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
