#+TITLE: Manpages - x86_64.8
#+DESCRIPTION: Linux manpage for x86_64.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about x86_64.8 is found in manpage for: [[../man8/setarch.8][man8/setarch.8]]