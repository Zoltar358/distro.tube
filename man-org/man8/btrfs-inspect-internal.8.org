#+TITLE: Manpages - btrfs-inspect-internal.8
#+DESCRIPTION: Linux manpage for btrfs-inspect-internal.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
btrfs-inspect-internal - query various internal information

* SYNOPSIS
*btrfs inspect-internal* /<subcommand>/ /<args>/

* DESCRIPTION
This command group provides an interface to query internal information.
The functionality ranges from a simple UI to an ioctl or a more complex
query that assembles the result from several internal structures. The
latter usually requires calls to privileged ioctls.

* SUBCOMMAND
*dump-super* [options] /<device>/ [device...]

#+begin_quote
  (replaces the standalone tool *btrfs-show-super*)

  Show btrfs superblock information stored on given devices in textual
  form. By default the first superblock is printed, more details about
  all copies or additional backup data can be printed.

  Besides verification of the filesystem signature, there are no other
  sanity checks. The superblock checksum status is reported, the device
  item and filesystem UUIDs are checked and reported.

  #+begin_quote
    \\

    *Note*

    \\
    the meaning of option /-s/ has changed in version 4.8 to be
    consistent with other tools to specify superblock copy rather the
    offset. The old way still works, but prints a warning. Please update
    your scripts to use /--bytenr/ instead. The option /-i/ has been
    deprecated.
  #+end_quote

  *Options*

  -f|--full

  #+begin_quote
    print full superblock information, including the system chunk array
    and backup roots
  #+end_quote

  -a|--all

  #+begin_quote
    print information about all present superblock copies (cannot be
    used together with /-s/ option)
  #+end_quote

  -i /<super>/

  #+begin_quote
    (deprecated since 4.8, same behaviour as /--super/)
  #+end_quote

  --bytenr /<bytenr>/

  #+begin_quote
    specify offset to a superblock in a non-standard location at
    /bytenr/, useful for debugging (disables the /-f/ option)

    If there are multiple options specified, only the last one applies.
  #+end_quote

  -F|--force

  #+begin_quote
    attempt to print the superblock even if a valid BTRFS signature is
    not found; the result may be completely wrong if the data does not
    resemble a superblock
  #+end_quote

  -s|--super /<bytenr>/

  #+begin_quote
    (see compatibility note above)

    specify which mirror to print, valid values are 0, 1 and 2 and the
    superblock must be present on the device with a valid signature, can
    be used together with /--force/
  #+end_quote
#+end_quote

*dump-tree* [options] /<device>/ [device...]

#+begin_quote
  (replaces the standalone tool *btrfs-debug-tree*)

  Dump tree structures from a given device in textual form, expand keys
  to human readable equivalents where possible. This is useful for
  analyzing filesystem state or inconsistencies and has a positive
  educational effect on understanding the internal filesystem structure.

  #+begin_quote
    \\

    *Note*

    \\
    contains file names, consider that if you're asked to send the dump
    for analysis. Does not contain file data.
  #+end_quote

  *Options*

  -e|--extents

  #+begin_quote
    print only extent-related information: extent and device trees
  #+end_quote

  -d|--device

  #+begin_quote
    print only device-related information: tree root, chunk and device
    trees
  #+end_quote

  -r|--roots

  #+begin_quote
    print only short root node information, ie. the root tree keys
  #+end_quote

  -R|--backups

  #+begin_quote
    same as --roots plus print backup root info, ie. the backup root
    keys and the respective tree root block offset
  #+end_quote

  -u|--uuid

  #+begin_quote
    print only the uuid tree information, empty output if the tree does
    not exist
  #+end_quote

  -b /<block_num>/

  #+begin_quote
    print info of the specified block only, can be specified multiple
    times
  #+end_quote

  --follow

  #+begin_quote
    use with /-b/, print all children tree blocks of /<block_num>/
  #+end_quote

  --dfs

  #+begin_quote
    (default up to 5.2)

    use depth-first search to print trees, the nodes and leaves are
    intermixed in the output
  #+end_quote

  --bfs

  #+begin_quote
    (default since 5.3)

    use breadth-first search to print trees, the nodes are printed
    before all leaves
  #+end_quote

  --hide-names

  #+begin_quote
    print a placeholder /HIDDEN/ instead of various names, useful for
    developers to inspect the dump while keeping potentially sensitive
    information hidden

    This is:

    #+begin_quote
      ·

      directory entries (files, directories, subvolumes)
    #+end_quote

    #+begin_quote
      ·

      default subvolume
    #+end_quote

    #+begin_quote
      ·

      extended attributes (name, value)
    #+end_quote

    #+begin_quote
      ·

      hardlink names (if stored inside another item or as extended
      references in standalone items)

      #+begin_quote
        \\

        *Note*

        \\
        lengths are not hidden because they can be calculated from the
        item size anyway.
      #+end_quote
    #+end_quote
  #+end_quote

  --csum-headers

  #+begin_quote
    print b-tree node checksums stored in headers (metadata)
  #+end_quote

  --csum-items

  #+begin_quote
    print checksums stored in checksum items (data)
  #+end_quote

  --noscan

  #+begin_quote
    do not automatically scan the system for other devices from the same
    filesystem, only use the devices provided as the arguments
  #+end_quote

  -t /<tree_id>/

  #+begin_quote
    print only the tree with the specified ID, where the ID can be
    numerical or common name in a flexible human readable form

    The tree id name recognition rules:

    #+begin_quote
      ·

      case does not matter
    #+end_quote

    #+begin_quote
      ·

      the C source definition, eg. BTRFS_ROOT_TREE_OBJECTID
    #+end_quote

    #+begin_quote
      ·

      short forms without BTRFS_ prefix, without _TREE and _OBJECTID
      suffix, eg. ROOT_TREE, ROOT
    #+end_quote

    #+begin_quote
      ·

      convenience aliases, eg. DEVICE for the DEV tree, CHECKSUM for
      CSUM
    #+end_quote

    #+begin_quote
      ·

      unrecognized ID is an error
    #+end_quote
  #+end_quote
#+end_quote

*inode-resolve* [-v] /<ino>/ /<path>/

#+begin_quote
  (needs root privileges)

  resolve paths to all files with given inode number /ino/ in a given
  subvolume at /path/, ie. all hardlinks

  *Options*

  -v

  #+begin_quote
    (deprecated) alias for global /-v/ option
  #+end_quote
#+end_quote

*logical-resolve* [-Pvo] [-s /<bufsize>/] /<logical>/ /<path>/

#+begin_quote
  (needs root privileges)

  resolve paths to all files at given /logical/ address in the linear
  filesystem space

  *Options*

  -P

  #+begin_quote
    skip the path resolving and print the inodes instead
  #+end_quote

  -o

  #+begin_quote
    ignore offsets, find all references to an extent instead of a single
    block. Requires kernel support for the V2 ioctl (added in 4.15). The
    results might need further processing to filter out unwanted extents
    by the offset that is supposed to be obtained by other means.
  #+end_quote

  -s /<bufsize>/

  #+begin_quote
    set internal buffer for storing the file names to /bufsize/, default
    is 64k, maximum 16m. Buffer sizes over 64K require kernel support
    for the V2 ioctl (added in 4.15).
  #+end_quote

  -v

  #+begin_quote
    (deprecated) alias for global /-v/ option
  #+end_quote
#+end_quote

*min-dev-size* [options] /<path>/

#+begin_quote
  (needs root privileges)

  return the minimum size the device can be shrunk to, without
  performing any resize operation, this may be useful before executing
  the actual resize operation

  *Options*

  --id /<id>/

  #+begin_quote
    specify the device /id/ to query, default is 1 if this option is not
    used
  #+end_quote
#+end_quote

*rootid* /<path>/

#+begin_quote
  for a given file or directory, return the containing tree root id, but
  for a subvolume itself return its own tree id (ie. subvol id)

  #+begin_quote
    \\

    *Note*

    \\
    The result is undefined for the so-called empty subvolumes
    (identified by inode number 2), but such a subvolume does not
    contain any files anyway
  #+end_quote
#+end_quote

*subvolid-resolve* /<subvolid>/ /<path>/

#+begin_quote
  (needs root privileges)

  resolve the absolute path of the subvolume id /subvolid/
#+end_quote

*tree-stats* [options] /<device>/

#+begin_quote
  (needs root privileges)

  Print sizes and statistics of trees.

  *Options*

  -b

  #+begin_quote
    Print raw numbers in bytes.
  #+end_quote
#+end_quote

* EXIT STATUS
*btrfs inspect-internal* returns a zero exit status if it succeeds. Non
zero is returned in case of failure.

* AVAILABILITY
*btrfs* is part of btrfs-progs. Please refer to the btrfs wiki
*http://btrfs.wiki.kernel.org* for further details.

* SEE ALSO
*mkfs.btrfs*(8)
