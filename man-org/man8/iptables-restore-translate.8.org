#+TITLE: Manpages - iptables-restore-translate.8
#+DESCRIPTION: Linux manpage for iptables-restore-translate.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about iptables-restore-translate.8 is found in manpage for: [[../man8/xtables-translate.8][man8/xtables-translate.8]]