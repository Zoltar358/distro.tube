#+TITLE: Manpages - pam_deny.8
#+DESCRIPTION: Linux manpage for pam_deny.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_deny - The locking-out PAM module

* SYNOPSIS
*pam_deny.so*

* DESCRIPTION
This module can be used to deny access. It always indicates a failure to
the application through the PAM framework. It might be suitable for
using for default (the /OTHER/) entries.

* OPTIONS
This module does not recognise any options.

* MODULE TYPES PROVIDED
All module types (*account*, *auth*, *password* and *session*) are
provided.

* RETURN VALUES
PAM_AUTH_ERR

#+begin_quote
  This is returned by the account and auth services.
#+end_quote

PAM_CRED_ERR

#+begin_quote
  This is returned by the setcred function.
#+end_quote

PAM_AUTHTOK_ERR

#+begin_quote
  This is returned by the password service.
#+end_quote

PAM_SESSION_ERR

#+begin_quote
  This is returned by the session service.
#+end_quote

* EXAMPLES

#+begin_quote
  #+begin_example
    #%PAM-1.0
    #
    # If we dont have config entries for a service, the
    # OTHER entries are used. To be secure, warn and deny
    # access to everything.
    other auth     required       pam_warn.so
    other auth     required       pam_deny.so
    other account  required       pam_warn.so
    other account  required       pam_deny.so
    other password required       pam_warn.so
    other password required       pam_deny.so
    other session  required       pam_warn.so
    other session  required       pam_deny.so
        
  #+end_example
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_deny was written by Andrew G. Morgan <morgan@kernel.org>
