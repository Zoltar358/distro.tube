#+TITLE: Manpages - systemd-hostnamed.service.8
#+DESCRIPTION: Linux manpage for systemd-hostnamed.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-hostnamed.service, systemd-hostnamed - Daemon to control system
hostname from programs

* SYNOPSIS
systemd-hostnamed.service

/usr/lib/systemd/systemd-hostnamed

* DESCRIPTION
systemd-hostnamed.service is a system service that may be used to change
the systems hostname and related machine metadata from user programs. It
is automatically activated on request and terminates itself when unused.

It currently offers access to five variables:

#+begin_quote
  ·

  The current hostname (Example: "dhcp-192-168-47-11")
#+end_quote

#+begin_quote
  ·

  The static (configured) hostname (Example: "lennarts-computer")
#+end_quote

#+begin_quote
  ·

  The pretty hostname (Example: "Lennarts Computer")
#+end_quote

#+begin_quote
  ·

  A suitable icon name for the local host (Example: "computer-laptop")
#+end_quote

#+begin_quote
  ·

  A chassis type (Example: "tablet")
#+end_quote

The static hostname is stored in /etc/hostname, see *hostname*(5) for
more information. The pretty hostname, chassis type, and icon name are
stored in /etc/machine-info, see *machine-info*(5).

The tool *hostnamectl*(1) is a command line client to this service.

See *org.freedesktop.hostname1*(5) and *org.freedesktop.LogControl1*(5)
for a description of the D-Bus API.

* SEE ALSO
*systemd*(1), *hostname*(5), *machine-info*(5), *hostnamectl*(1),
*sethostname*(2)
