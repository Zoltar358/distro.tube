#+TITLE: Manpages - sg_rep_zones.8
#+DESCRIPTION: Linux manpage for sg_rep_zones.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sg_rep_zones - send SCSI REPORT ZONES, REALMS or ZONE DOMAINS command

* SYNOPSIS
*sg_rep_zones* [/--domain/] [/--help/] [/--hex/] [/--inhex=FN/]
[/--locator=LBA/] [/--maxlen=LEN/] [/--num=NUM/] [/--partial/] [/--raw/]
[/--readonly/] [/--realm/] [/--report=OPT/] [/--start=LBA/]
[/--verbose/] [/--version/] [/--wp/] /DEVICE/

* DESCRIPTION
Sends a SCSI REPORT ZONES, REPORT REALMS or REPORT ZONE DOMAINS command
to /DEVICE/ and decodes (or simply outputs) the data returned. These
commands is found in the ZBC-2 draft standard, revision 10
(zbc2r05.pdf). Only the REPORT ZONES command is defined in the original
ZBC standard (INCITS 536-2017) and it is the default.

The REPORT ZONE DOMAINS command will be sent (or decoded) when the
/--domain/ option is given. The REPORT REALMS command will be sent (or
decoded) when the /--realm/ option is given.

Rather than send a SCSI command to /DEVICE/, if the /--inhex=FN/ option
is given, then the contents of the file named /FN/ are decoded as ASCII
hex (or binary if /--raw/ is also given) and then processed as if it was
the response of the command. By default the REPORT ZONES command
response is assumed; if the /--domain/ or /--realm/ option is given then
the corresponding command response is assumed.

* OPTIONS
Arguments to long options are mandatory for short options as well.

- *-d*, *--domain* :: send or decode the SCSI REPORT ZONE DOMAINS
  command.

- *-h*, *--help* :: output the usage message then exit.

- *-H*, *--hex* :: output the response in hexadecimal to stdout. When
  used once the whole response is output in ASCII hexadecimal with a
  leading address (starting at 0) on each line. When used twice each
  zone descriptor in the response is output separately in hexadecimal.
  When used thrice the whole response is output in hexadecimal with no
  leading address (on each line).\\
  The output format when this option is given thrice is suitable
  contents for a later invocation with the /--inhex=FN/ option.

- *-i*, *--inhex*=/FN/ :: where /FN/ is a file name whose contents are
  assumed to be ASCII hexadecimal. If /DEVICE/ is also given then
  /DEVICE/ is ignored, a warning is issued and the utility continues,
  decoding the file named /FN/. See the "FORMAT OF FILES CONTAINING
  ASCII HEX" section in the sg3_utils manpage for more information. If
  the /--raw/ option is also given then the contents of /FN/ are treated
  as binary.\\
  Note that by default this utility assumes then contents are the
  response from a REPORT ZONES command. Use the /--domain/ or /--realm/
  option for decoding the other two commands.

- *-l*, *--locator*=/LBA/ :: where /LBA/ plays a similar role as it does
  in /--start=LBA/. It is the field name used in the REPORT REALMS and
  REPORT ZONE DOMAINS commands.

- *-m*, *--maxlen*=/LEN/ :: where /LEN/ is the (maximum) response length
  in bytes. It is placed in the cdb's "allocation length" field. If not
  given (or /LEN/ is zero) then 8192 is used. The maximum allowed value
  of /LEN/ is 1048576.

- *-n*, *--num*=/NUM/ :: where /NUM/ is the (maximum) number of zone
  descriptors to print out. The default value is zero which is taken to
  mean print out all zone descriptors returned by the REPORT ZONES
  command.

- *-p*, *--partial* :: set the PARTIAL bit in the cdb.

- *-r*, *--raw* :: output response in binary (to stdout) unless the
  /--inhex=FN/ option is also given. In that case the input file name
  (/FN/) is decoded as binary (and the output is _not_ in binary (but
  may be hex)).

- *-R*, *--readonly* :: open the /DEVICE/ read-only (e.g. in Unix with
  the O_RDONLY flag). The default is to open it read-write.

- *-e*, *--realm* :: send or decode the SCSI REPORT REALMS command.

- *-o*, *--report*=/OPT/ :: where /OPT/ will become the contents of the
  REPORTING OPTION field in the cdb. The reporting options differ
  between REPORT ZONES, REPORT ZONE DOMAINS and REPORT REALMS. If the
  /--help/ option is given twice ( or the equivalent '-hh') a list of
  available reporting options (as of writing) for each command is
  output.\\
  The default value for REPORT ZONES is 0 which means report a list of
  all zones. Some other values are 1 for list zones with a zone
  condition of empty; 2 for list zones with a zone condition of
  implicitly opened; 3 for list zones with a zone condition of
  explicitly opened; 4 for list zones with a zone condition of closed; 5
  for list zones with a zone condition of full; 6 for list zones with a
  zone condition of read only; 7 for list zones with a zone condition of
  offline. Other values are 0x10 for list zones with 'RWP recommended'
  set to true; 0x11 for list zones with non-sequential write resource
  active set to true and 0x3f for list zones with a zone condition of
  'not write pointer'.

- *-s*, *--start*=/LBA/ :: where /LBA/ is at the start or within the
  first zone to be reported. The default value is 0. If /LBA/ is not a
  zone start LBA then the preceding zone start LBA is used for
  reporting. Assumed to be in decimal unless prefixed with '0x' or has a
  trailing 'h' which indicate hexadecimal.\\
  The zone start LBA field used in the REPORT ZONES command was changed
  to the zone domain/realm locator field for the two newer ZBC-2
  commands. For this utility /--locator=LBA/ and /--start=LBA/ are
  interchangeable.

- *-v*, *--verbose* :: increase the level of verbosity, (i.e. debug
  output).

- *-V*, *--version* :: print the version string and then exit.

- *-w*, *--wp* :: print the write pointer (in hex) only. In the absence
  of errors, then a hex LBA will be printed on each line, one line for
  each zone. Can be usefully combined with the /--num=NUM/ and
  /--start=LBA/ options.

* EXIT STATUS
The exit status of sg_rep_zones is 0 when it is successful. Otherwise
see the sg3_utils(8) man page.

* AUTHORS
Written by Douglas Gilbert.

* REPORTING BUGS
Report bugs to <dgilbert at interlog dot com>.

* COPYRIGHT
Copyright © 2014-2021 Douglas Gilbert\\
This software is distributed under a FreeBSD license. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.

* SEE ALSO
*sg_reset_wp,sg_zone(sg3_utils)*
