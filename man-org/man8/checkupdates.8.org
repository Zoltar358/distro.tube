#+TITLE: Manpages - checkupdates.8
#+DESCRIPTION: Linux manpage for checkupdates.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
checkupdates - safely print a list of pending updates

* SYNOPSIS
/checkupdates/ [options]

* DESCRIPTION
Checks for pending updates using a separate Pacman database located in
*TMPDIR* if *CHECKUPDATES_DB* is not set and outputs a list of updates
with the old and new version.

* OPTIONS
*-d, --download*

#+begin_quote
  Download any pending updates to the pacman cache.
#+end_quote

*-h, --help*

#+begin_quote
  Display syntax and command line options.
#+end_quote

* ENVIRONMENT
*CHECKUPDATES_DB*

#+begin_quote
  Override the default update db location.
#+end_quote

*TMPDIR*

#+begin_quote
  Overrides the default //tmp/ temporary directory.
#+end_quote

* ERRORS
On exit, checkpkg will return one of the following error codes.

0

#+begin_quote
  Normal exit condition.
#+end_quote

1

#+begin_quote
  Unknown cause of failure.
#+end_quote

2

#+begin_quote
  No updates are available.
#+end_quote

* SEE ALSO
*pacman*(8), *pacman.conf*(5)

* BUGS
Bugs? You must be kidding; there are no bugs in this software. But if we
happen to be wrong, send us an email with as much detail as possible to
pacman-contrib@lists.archlinux.org.

* AUTHORS
Current maintainers:

#+begin_quote
  ·

  Johannes Löthberg <johannes@kyriasis.com>
#+end_quote

#+begin_quote
  ·

  Daniel M. Capella <polyzen@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the
pacman-contrib.git repository.
