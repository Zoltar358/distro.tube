#+TITLE: Manpages - shutdown.8
#+DESCRIPTION: Linux manpage for shutdown.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
shutdown - Halt, power-off or reboot the machine

* SYNOPSIS
*shutdown* [OPTIONS...] [TIME] [WALL...]

* DESCRIPTION
*shutdown* may be used to halt, power-off or reboot the machine.

The first argument may be a time string (which is usually "now").
Optionally, this may be followed by a wall message to be sent to all
logged-in users before going down.

The time string may either be in the format "hh:mm" for hour/minutes
specifying the time to execute the shutdown at, specified in 24h clock
format. Alternatively it may be in the syntax "+m" referring to the
specified number of minutes m from now. "now" is an alias for "+0", i.e.
for triggering an immediate shutdown. If no time argument is specified,
"+1" is implied.

Note that to specify a wall message you must specify a time argument,
too.

If the time argument is used, 5 minutes before the system goes down the
/run/nologin file is created to ensure that further logins shall not be
allowed.

* OPTIONS
The following options are understood:

*--help*

#+begin_quote
  Print a short help text and exit.
#+end_quote

*-H*, *--halt*

#+begin_quote
  Halt the machine.
#+end_quote

*-P*, *--poweroff*

#+begin_quote
  Power-off the machine (the default).
#+end_quote

*-r*, *--reboot*

#+begin_quote
  Reboot the machine.
#+end_quote

*-h*

#+begin_quote
  Equivalent to *--poweroff*, unless *--halt* is specified.
#+end_quote

*-k*

#+begin_quote
  Do not halt, power-off, reboot, just write wall message.
#+end_quote

*--no-wall*

#+begin_quote
  Do not send wall message before halt, power-off, reboot.
#+end_quote

*-c*

#+begin_quote
  Cancel a pending shutdown. This may be used to cancel the effect of an
  invocation of *shutdown* with a time argument that is not "+0" or
  "now".
#+end_quote

* EXIT STATUS
On success, 0 is returned, a non-zero failure code otherwise.

* SEE ALSO
*systemd*(1), *systemctl*(1), *halt*(8), *wall*(1)
