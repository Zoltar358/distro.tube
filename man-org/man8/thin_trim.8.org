#+TITLE: Manpages - thin_trim.8
#+DESCRIPTION: Linux manpage for thin_trim.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*thin_trim *- Issue discard requests for free pool space (offline tool).

* SYNOPSIS
#+begin_example
  thin_trim [options] --metadata-dev {device|file} --data-dev {device|file}
#+end_example

* DESCRIPTION
*thin_trim sends discard requests to the pool device for unprovisioned
areas.*

This tool cannot be run on live metadata.

* OPTIONS
- **-h, --help** :: Print help and exit.

- **-V, --version** :: Print version information and exit.

* SEE ALSO
*thin_dump(8), thin_repair(8), thin_restore(8), thin_rmap(8),
thin_metadata_size(8)*

* AUTHOR
Joe Thornber <ejt@redhat.com>
