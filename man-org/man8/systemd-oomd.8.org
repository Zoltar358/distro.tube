#+TITLE: Manpages - systemd-oomd.8
#+DESCRIPTION: Linux manpage for systemd-oomd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about systemd-oomd.8 is found in manpage for: [[../systemd-oomd.service.8][systemd-oomd.service.8]]