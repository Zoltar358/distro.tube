#+TITLE: Manpages - pam_pwhistory.8
#+DESCRIPTION: Linux manpage for pam_pwhistory.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_pwhistory - PAM module to remember last passwords

* SYNOPSIS
*pam_pwhistory.so* [debug] [use_authtok] [enforce_for_root]
[remember=/N/] [retry=/N/] [authtok_type=/STRING/]

* DESCRIPTION
This module saves the last passwords for each user in order to force
password change history and keep the user from alternating between the
same password too frequently.

This module does not work together with kerberos. In general, it does
not make much sense to use this module in conjunction with NIS or LDAP,
since the old passwords are stored on the local machine and are not
available on another machine for password history checking.

* OPTIONS
*debug*

#+begin_quote
  Turns on debugging via *syslog*(3).
#+end_quote

*use_authtok*

#+begin_quote
  When password changing enforce the module to use the new password
  provided by a previously stacked *password* module (this is used in
  the example of the stacking of the *pam_passwdqc* module documented
  below).
#+end_quote

*enforce_for_root*

#+begin_quote
  If this option is set, the check is enforced for root, too.
#+end_quote

*remember=*/N/

#+begin_quote
  The last /N/ passwords for each user are saved in
  /etc/security/opasswd. The default is /10/. Value of /0/ makes the
  module to keep the existing contents of the opasswd file unchanged.
#+end_quote

*retry=*/N/

#+begin_quote
  Prompt user at most /N/ times before returning with error. The default
  is /1/.
#+end_quote

*authtok_type=*/STRING/

#+begin_quote
  See *pam_get_authtok*(3) for more details.
#+end_quote

* MODULE TYPES PROVIDED
Only the *password* module type is provided.

* RETURN VALUES
PAM_AUTHTOK_ERR

#+begin_quote
  No new password was entered, the user aborted password change or new
  password couldnt be set.
#+end_quote

PAM_IGNORE

#+begin_quote
  Password history was disabled.
#+end_quote

PAM_MAXTRIES

#+begin_quote
  Password was rejected too often.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User is not known to system.
#+end_quote

* EXAMPLES
An example password section would be:

#+begin_quote
  #+begin_example
    #%PAM-1.0
    password     required       pam_pwhistory.so
    password     required       pam_unix.so        use_authtok
          
  #+end_example
#+end_quote

In combination with *pam_passwdqc*:

#+begin_quote
  #+begin_example
    #%PAM-1.0
    password     required       pam_passwdqc.so    config=/etc/passwdqc.conf
    password     required       pam_pwhistory.so   use_authtok
    password     required       pam_unix.so        use_authtok
          
  #+end_example
#+end_quote

* FILES
/etc/security/opasswd

#+begin_quote
  File with password history
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8) *pam_get_authtok*(3)

* AUTHOR
pam_pwhistory was written by Thorsten Kukuk <kukuk@thkukuk.de>

Information about pam_pwhistory.8 is found in manpage for: [[../    required       pam_unix\&.so        use_authtok
    required       pam_passwdqc\&.so    config=/etc/passwdqc\&.conf
    required       pam_pwhistory\&.so   use_authtok
    required       pam_unix\&.so        use_authtok][    required       pam_unix\&.so        use_authtok
    required       pam_passwdqc\&.so    config=/etc/passwdqc\&.conf
    required       pam_pwhistory\&.so   use_authtok
    required       pam_unix\&.so        use_authtok]]