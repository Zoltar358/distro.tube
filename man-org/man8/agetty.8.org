#+TITLE: Manpages - agetty.8
#+DESCRIPTION: Linux manpage for agetty.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
agetty - alternative Linux getty

* SYNOPSIS
*agetty* [options] /port/ [/baud_rate/...] [/term/]

* DESCRIPTION
*agetty* opens a tty port, prompts for a login name and invokes the
/bin/login command. It is normally invoked by *init*(8).

*agetty* has several /non-standard/ features that are useful for
hardwired and for dial-in lines:

#+begin_quote
  ·

  Adapts the tty settings to parity bits and to erase, kill, end-of-line
  and uppercase characters when it reads a login name. The program can
  handle 7-bit characters with even, odd, none or space parity, and
  8-bit characters with no parity. The following special characters are
  recognized: Control-U (kill); DEL and backspace (erase); carriage
  return and line feed (end of line). See also the *--erase-chars* and
  *--kill-chars* options.
#+end_quote

#+begin_quote
  ·

  Optionally deduces the baud rate from the CONNECT messages produced by
  Hayes(tm)-compatible modems.
#+end_quote

#+begin_quote
  ·

  Optionally does not hang up when it is given an already opened line
  (useful for call-back applications).
#+end_quote

#+begin_quote
  ·

  Optionally does not display the contents of the //etc/issue/ file.
#+end_quote

#+begin_quote
  ·

  Optionally displays an alternative issue files or directories instead
  of //etc/issue/ or //etc/issue.d/.
#+end_quote

#+begin_quote
  ·

  Optionally does not ask for a login name.
#+end_quote

#+begin_quote
  ·

  Optionally invokes a non-standard login program instead of
  //bin/login/.
#+end_quote

#+begin_quote
  ·

  Optionally turns on hardware flow control.
#+end_quote

#+begin_quote
  ·

  Optionally forces the line to be local with no need for carrier
  detect.
#+end_quote

This program does not use the //etc/gettydefs/ (System V) or
//etc/gettytab/ (SunOS 4) files.

* ARGUMENTS
/port/

#+begin_quote
  A path name relative to the //dev/ directory. If a "-" is specified,
  *agetty* assumes that its standard input is already connected to a tty
  port and that a connection to a remote user has already been
  established.

  Under System V, a "-" /port/ argument should be preceded by a "--".
#+end_quote

/baud_rate/,...

#+begin_quote
  A comma-separated list of one or more baud rates. Each time *agetty*
  receives a BREAK character it advances through the list, which is
  treated as if it were circular.

  Baud rates should be specified in descending order, so that the null
  character (Ctrl-@) can also be used for baud-rate switching.

  This argument is optional and unnecessary for *virtual terminals*.

  The default for *serial terminals* is keep the current baud rate (see
  *--keep-baud*) and if unsuccessful then default to '9600'.
#+end_quote

/term/

#+begin_quote
  The value to be used for the *TERM* environment variable. This
  overrides whatever *init*(1) may have set, and is inherited by login
  and the shell.

  The default is 'vt100', or 'linux' for Linux on a virtual terminal, or
  'hurd' for GNU Hurd on a virtual terminal.
#+end_quote

* OPTIONS
*-8*, *--8bits*

#+begin_quote
  Assume that the tty is 8-bit clean, hence disable parity detection.
#+end_quote

*-a*, *--autologin* /username/

#+begin_quote
  Automatically log in the specified user without asking for a username
  or password. Using this option causes an *-f* /username/ option and
  argument to be added to the */bin/login* command line. See
  *--login-options*, which can be used to modify this option's behavior.

  Note that *--autologin* may affect the way in which *getty*
  initializes the serial line, because on auto-login *agetty* does not
  read from the line and it has no opportunity optimize the line
  setting.
#+end_quote

*-c*, *--noreset*

#+begin_quote
  Do not reset terminal cflags (control modes). See *termios*(3) for
  more details.
#+end_quote

*-E*, *--remote*

#+begin_quote
  Typically the *login*(1) command is given a remote hostname when
  called by something such as *telnetd*(8). This option allows *agetty*
  to pass what it is using for a hostname to *login*(1) for use in
  *utmp*(5). See *--host*, *login*(1), and *utmp*(5).

  If the *--host* /fakehost/ option is given, then an *-h* /fakehost/
  option and argument are added to the //bin/login/ command line.

  If the *--nohostname* option is given, then an *-H* option is added to
  the */bin/login* command line.

  See *--login-options*.
#+end_quote

*-f*, *--issue-file* /path/

#+begin_quote
  Specifies a ":" delimited list of files and directories to be
  displayed instead of //etc/issue/ (or other). All specified files and
  directories are displayed, missing or empty files are silently
  ignored. If the specified path is a directory then display all files
  with .issue file extension in version-sort order from the directory.
  This allows custom messages to be displayed on different terminals.
  The *--noissue* option will override this option.
#+end_quote

*--show-issue*

#+begin_quote
  Display the current issue file (or other) on the current terminal and
  exit. Use this option to review the current setting, it is not
  designed for any other purpose. Note that output may use some default
  or incomplete information as proper output depends on terminal and
  agetty command line.
#+end_quote

*-h, --flow-control*

#+begin_quote
  Enable hardware (RTS/CTS) flow control. It is left up to the
  application to disable software (XON/XOFF) flow protocol where
  appropriate.
#+end_quote

*-H*, *--host* /fakehost/

#+begin_quote
  Write the specified /fakehost/ into the utmp file. Normally, no login
  host is given, since *agetty* is used for local hardwired connections
  and consoles. However, this option can be useful for identifying
  terminal concentrators and the like.
#+end_quote

*-i*, *--noissue*

#+begin_quote
  Do not display the contents of //etc/issue/ (or other) before writing
  the login prompt. Terminals or communications hardware may become
  confused when receiving lots of text at the wrong baud rate; dial-up
  scripts may fail if the login prompt is preceded by too much text.
#+end_quote

*-I*, *--init-string* /initstring/

#+begin_quote
  Set an initial string to be sent to the tty or modem before sending
  anything else. This may be used to initialize a modem. Non-printable
  characters may be sent by writing their octal code preceded by a
  backslash (\). For example, to send a linefeed character (ASCII 10,
  octal 012), write \12.
#+end_quote

*-J*, *--noclear*

#+begin_quote
  Do not clear the screen before prompting for the login name. By
  default the screen is cleared.
#+end_quote

*-l*, *--login-program* /login_program/

#+begin_quote
  Invoke the specified /login_program/ instead of /bin/login. This
  allows the use of a non-standard login program. Such a program could,
  for example, ask for a dial-up password or use a different password
  file. See *--login-options*.
#+end_quote

*-L*, *--local-line*[=/mode/]

#+begin_quote
  Control the CLOCAL line flag. The optional /mode/ argument is 'auto',
  'always' or 'never'. If the /mode/ argument is omitted, then the
  default is 'always'. If the *--local-line* option is not given at all,
  then the default is 'auto'.

  /always/

  #+begin_quote
    Forces the line to be a local line with no need for carrier detect.
    This can be useful when you have a locally attached terminal where
    the serial line does not set the carrier-detect signal.
  #+end_quote

  /never/

  #+begin_quote
    Explicitly clears the CLOCAL flag from the line setting and the
    carrier-detect signal is expected on the line.
  #+end_quote

  /auto/

  #+begin_quote
    The *agetty* default. Does not modify the CLOCAL setting and follows
    the setting enabled by the kernel.
  #+end_quote
#+end_quote

*-m*, *--extract-baud*

#+begin_quote
  Try to extract the baud rate from the CONNECT status message produced
  by Hayes(tm)-compatible modems. These status messages are of the form:
  "<junk><speed><junk>". *agetty* assumes that the modem emits its
  status message at the same speed as specified with (the first)
  /baud_rate/ value on the command line.

  Since the *--extract-baud* feature may fail on heavily-loaded systems,
  you still should enable BREAK processing by enumerating all expected
  baud rates on the command line.
#+end_quote

*--list-speeds*

#+begin_quote
  Display supported baud rates. These are determined at compilation
  time.
#+end_quote

*-n*, *--skip-login*

#+begin_quote
  Do not prompt the user for a login name. This can be used in
  connection with the *--login-program* option to invoke a non-standard
  login process such as a BBS system. Note that with the *--skip-login*
  option, *agetty* gets no input from the user who logs in and therefore
  will not be able to figure out parity, character size, and newline
  processing of the connection. It defaults to space parity, 7 bit
  characters, and ASCII CR (13) end-of-line character. Beware that the
  program that *agetty* starts (usually /bin/login) is run as root.
#+end_quote

*-N*, *--nonewline*

#+begin_quote
  Do not print a newline before writing out //etc/issue/.
#+end_quote

*-o*, *--login-options* /login_options/

#+begin_quote
  Options and arguments that are passed to *login*(1). Where \u is
  replaced by the login name. For example:

  *--login-options '-h darkstar --- \u'*

  See *--autologin*, *--login-program* and *--remote*.

  Please read the SECURITY NOTICE below before using this option.
#+end_quote

*-p*, *--login-pause*

#+begin_quote
  Wait for any key before dropping to the login prompt. Can be combined
  with *--autologin* to save memory by lazily spawning shells.
#+end_quote

*-r*, *--chroot* /directory/

#+begin_quote
  Change root to the specified directory.
#+end_quote

*-R*, *--hangup*

#+begin_quote
  Call *vhangup*(2) to do a virtual hangup of the specified terminal.
#+end_quote

*-s*, *--keep-baud*

#+begin_quote
  Try to keep the existing baud rate. The baud rates from the command
  line are used when *agetty* receives a BREAK character. If another
  baud rates specified then the original baud rate is also saved to the
  end of the wanted baud rates list. This can be used to return to the
  original baud rate after unexpected BREAKs.
#+end_quote

*-t*, *--timeout* /timeout/

#+begin_quote
  Terminate if no user name could be read within /timeout/ seconds. Use
  of this option with hardwired terminal lines is not recommended.
#+end_quote

*-U*, *--detect-case*

#+begin_quote
  Turn on support for detecting an uppercase-only terminal. This setting
  will detect a login name containing only capitals as indicating an
  uppercase-only terminal and turn on some upper-to-lower case
  conversions. Note that this has no support for any Unicode characters.
#+end_quote

*-w*, *--wait-cr*

#+begin_quote
  Wait for the user or the modem to send a carriage-return or a linefeed
  character before sending the //etc/issue/ file (or others) and the
  login prompt. This is useful with the *--init-string* option.
#+end_quote

*--nohints*

#+begin_quote
  Do not print hints about Num, Caps and Scroll Locks.
#+end_quote

*--nohostname*

#+begin_quote
  By default the hostname will be printed. With this option enabled, no
  hostname at all will be shown.
#+end_quote

*--long-hostname*

#+begin_quote
  By default the hostname is only printed until the first dot. With this
  option enabled, the fully qualified hostname by *gethostname*(3P) or
  (if not found) by *getaddrinfo*(3) is shown.
#+end_quote

*--erase-chars* /string/

#+begin_quote
  This option specifies additional characters that should be interpreted
  as a backspace ("ignore the previous character") when the user types
  the login name. The default additional 'erase' has been '#', but since
  util-linux 2.23 no additional erase characters are enabled by default.
#+end_quote

*--kill-chars* /string/

#+begin_quote
  This option specifies additional characters that should be interpreted
  as a kill ("ignore all previous characters") when the user types the
  login name. The default additional 'kill' has been '@', but since
  util-linux 2.23 no additional kill characters are enabled by default.
#+end_quote

*--chdir* /directory/

#+begin_quote
  Change directory before the login.
#+end_quote

*--delay* /number/

#+begin_quote
  Sleep seconds before open tty.
#+end_quote

*--nice* /number/

#+begin_quote
  Run login with this priority.
#+end_quote

*--reload*

#+begin_quote
  Ask all running agetty instances to reload and update their displayed
  prompts, if the user has not yet commenced logging in. After doing so
  the command will exit. This feature might be unsupported on systems
  without Linux *inotify*(7).
#+end_quote

*--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* EXAMPLE
This section shows examples for the process field of an entry in the
//etc/inittab/ file. You'll have to prepend appropriate values for the
other fields. See *inittab*(5) for more details.

For a hardwired line or a console tty:

#+begin_quote
  */sbin/agetty 9600 ttyS1*\\
#+end_quote

For a directly connected terminal without proper carrier-detect wiring
(try this if your terminal just sleeps instead of giving you a password:
prompt):

#+begin_quote
  */sbin/agetty --local-line 9600 ttyS1 vt100*\\
#+end_quote

For an old-style dial-in line with a 9600/2400/1200 baud modem:

#+begin_quote
  */sbin/agetty --extract-baud --timeout 60 ttyS1 9600,2400,1200*\\
#+end_quote

For a Hayes modem with a fixed 115200 bps interface to the machine (the
example init string turns off modem echo and result codes, makes
modem/computer DCD track modem/modem DCD, makes a DTR drop cause a
disconnection, and turns on auto-answer after 1 ring):

#+begin_quote
  */sbin/agetty --wait-cr --init-string 'ATE0Q1&D2&C1S0=1\015' 115200
  ttyS1*\\
#+end_quote

* SECURITY NOTICE
If you use the *--login-program* and *--login-options* options, be aware
that a malicious user may try to enter lognames with embedded options,
which then get passed to the used login program. Agetty does check for a
leading "-" and makes sure the logname gets passed as one parameter (so
embedded spaces will not create yet another parameter), but depending on
how the login binary parses the command line that might not be
sufficient. Check that the used login program cannot be abused this way.

Some programs use "--" to indicate that the rest of the command line
should not be interpreted as options. Use this feature if available by
passing "--" before the username gets passed by \u.

* ISSUE FILES
The default issue file is //etc/issue/. If the file exists, then
*agetty* also checks for //etc/issue.d/ directory. The directory is
optional extension to the default issue file and content of the
directory is printed after //etc/issue/ content. If the //etc/issue/
does not exist, then the directory is ignored. All files *with .issue
extension* from the directory are printed in version-sort order. The
directory can be used to maintain 3rd-party messages independently on
the primary system //etc/issue/ file.

Since version 2.35 additional locations for issue file and directory are
supported. If the default //etc/issue/ does not exist, then *agetty*
checks for //run/issue/ and //run/issue.d/, thereafter for
//usr/lib/issue/ and //usr/lib/issue.d/. The directory //etc/ is
expected for host specific configuration, //run/ is expected for
generated stuff and //usr/lib/ for static distribution maintained
configuration.

The default path maybe overridden by *--issue-file* option. In this case
specified path has to be file or directory and all the default issue
file and directory locations are ignored.

The issue file feature can be completely disabled by *--noissue* option.

It is possible to review the current issue file by *agetty --show-issue*
on the current terminal.

The issue files may contain certain escape codes to display the system
name, date, time et cetera. All escape codes consist of a backslash (\)
immediately followed by one of the characters listed below.

4 or 4{/interface/}

#+begin_quote
  Insert the IPv4 address of the specified network interface (for
  example: \4{eth0}). If the /interface/ argument is not specified, then
  select the first fully configured (UP, non-LOCALBACK, RUNNING)
  interface. If not any configured interface is found, fall back to the
  IP address of the machine's hostname.
#+end_quote

6 or 6{/interface/}

#+begin_quote
  The same as \4 but for IPv6.
#+end_quote

b

#+begin_quote
  Insert the baudrate of the current line.
#+end_quote

d

#+begin_quote
  Insert the current date.
#+end_quote

e or e{/name/}

#+begin_quote
  Translate the human-readable /name/ to an escape sequence and insert
  it (for example: \e{red}Alert text.\e{reset}). If the /name/ argument
  is not specified, then insert \033. The currently supported names are:
  black, blink, blue, bold, brown, cyan, darkgray, gray, green,
  halfbright, lightblue, lightcyan, lightgray, lightgreen, lightmagenta,
  lightred, magenta, red, reset, reverse, yellow and white. All unknown
  names are silently ignored.
#+end_quote

s

#+begin_quote
  Insert the system name (the name of the operating system). Same as
  'uname -s'. See also the \S escape code.
#+end_quote

S or S{VARIABLE}

#+begin_quote
  Insert the VARIABLE data from //etc/os-release/. If this file does not
  exist then fall back to //usr/lib/os-release/. If the VARIABLE
  argument is not specified, then use PRETTY_NAME from the file or the
  system name (see \s). This escape code can be used to keep
  //etc/issue/ distribution and release independent. Note that
  \S{ANSI_COLOR} is converted to the real terminal escape sequence.
#+end_quote

l

#+begin_quote
  Insert the name of the current tty line.
#+end_quote

m

#+begin_quote
  Insert the architecture identifier of the machine. Same as *uname -m*.
#+end_quote

n

#+begin_quote
  Insert the nodename of the machine, also known as the hostname. Same
  as *uname -n*.
#+end_quote

o

#+begin_quote
  Insert the NIS domainname of the machine. Same as *hostname -d*.
#+end_quote

O

#+begin_quote
  Insert the DNS domainname of the machine.
#+end_quote

r

#+begin_quote
  Insert the release number of the OS. Same as *uname -r*.
#+end_quote

t

#+begin_quote
  Insert the current time.
#+end_quote

u

#+begin_quote
  Insert the number of current users logged in.
#+end_quote

U

#+begin_quote
  Insert the string "1 user" or "<n> users" where <n> is the number of
  current users logged in.
#+end_quote

v

#+begin_quote
  Insert the version of the OS, that is, the build-date and such.
#+end_quote

An example. On my system, the following //etc/issue/ file:

#+begin_quote
  #+begin_example
    This is \n.\o (\s \m \r) \t
  #+end_example
#+end_quote

displays as:

#+begin_quote
  #+begin_example
    This is thingol.orcan.dk (Linux i386 1.1.9) 18:29:30
  #+end_example
#+end_quote

* FILES
//var/run/utmp/

#+begin_quote
  the system status file.
#+end_quote

//etc/issue/

#+begin_quote
  printed before the login prompt.
#+end_quote

//etc/os-release /usr/lib/os-release/

#+begin_quote
  operating system identification data.
#+end_quote

//dev/console/

#+begin_quote
  problem reports (if *syslog*(3) is not used).
#+end_quote

//etc/inittab/

#+begin_quote
  *init*(8) configuration file for SysV-style init daemon.
#+end_quote

* BUGS
The baud-rate detection feature (the *--extract-baud* option) requires
that *agetty* be scheduled soon enough after completion of a dial-in
call (within 30 ms with modems that talk at 2400 baud). For robustness,
always use the *--extract-baud* option in combination with a multiple
baud rate command-line argument, so that BREAK processing is enabled.

The text in the //etc/issue/ file (or other) and the login prompt are
always output with 7-bit characters and space parity.

The baud-rate detection feature (the *--extract-baud* option) requires
that the modem emits its status message /after/ raising the DCD line.

* DIAGNOSTICS
Depending on how the program was configured, all diagnostics are written
to the console device or reported via the *syslog*(3) facility. Error
messages are produced if the /port/ argument does not specify a terminal
device; if there is no utmp entry for the current process (System V
only); and so on.

* AUTHORS
The original *agetty* for serial terminals was written by

and ported to Linux by

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *agetty* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
