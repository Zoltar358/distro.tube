#+TITLE: Manpages - reboot.8
#+DESCRIPTION: Linux manpage for reboot.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about reboot.8 is found in manpage for: [[../halt.8][halt.8]]