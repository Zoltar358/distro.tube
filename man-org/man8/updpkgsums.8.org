#+TITLE: Manpages - updpkgsums.8
#+DESCRIPTION: Linux manpage for updpkgsums.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
updpkgsums - update checksums of a PKGBUILD file

* SYNOPSIS
/updpkgsums/ [options] [build file]

* DESCRIPTION
/updpkgsums/ will perform an in place update of the checksums in the
path specified by [build file], defaulting to PKGBUILD in the current
working directory.

* OPTIONS
*-V, --version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-m, --nocolor*

#+begin_quote
  Disable colorized output messages.
#+end_quote

* SEE ALSO
*makepkg*(8), *pkgbuild*(5)

* BUGS
Bugs? You must be kidding; there are no bugs in this software. But if we
happen to be wrong, send us an email with as much detail as possible to
pacman-contrib@lists.archlinux.org.

* AUTHORS
Current maintainers:

#+begin_quote
  ·

  Johannes Löthberg <johannes@kyriasis.com>
#+end_quote

#+begin_quote
  ·

  Daniel M. Capella <polyzen@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the
pacman-contrib.git repository.
