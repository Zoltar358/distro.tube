#+TITLE: Manpages - ssh-keysign.8
#+DESCRIPTION: Linux manpage for ssh-keysign.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is used by

to access the local host keys and generate the digital signature
required during host-based authentication.

is disabled by default and can only be enabled in the global client
configuration file

by setting

to

is not intended to be invoked by the user, but from

See

and

for more information about host-based authentication.

Controls whether

is enabled.

These files contain the private parts of the host keys used to generate
the digital signature. They should be owned by root, readable only by
root, and not accessible to others. Since they are readable only by
root,

must be set-uid root if host-based authentication is used.

If these files exist they are assumed to contain public certificate
information corresponding with the private keys above.

first appeared in
