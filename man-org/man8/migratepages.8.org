#+TITLE: Manpages - migratepages.8
#+DESCRIPTION: Linux manpage for migratepages.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
migratepages - Migrate the physical location a processes pages

* SYNOPSIS
*migratepages* pid from-nodes to-nodes

* DESCRIPTION
*migratepages* moves the physical location of a processes pages without
any changes of the virtual address space of the process. Moving the
pages allows one to change the distances of a process to its memory.
Performance may be optimized by moving a processes pages to the node
where it is executing.

If multiple nodes are specified for from-nodes or to-nodes then an
attempt is made to preserve the relative location of each page in each
nodeset.

For example if we move from nodes 2-5 to 7,9,12-13 then the preferred
mode of operation is to move pages from 2->7, 3->9, 4->12 and 5->13.
However, this is only posssible if enough memory is available.

- Valid node specifiers :: 

| all               | All nodes                                        |
| number            | Node number                                      |
| number1{,number2} | Node number1 and Node number2                    |
| number1-number2   | Nodes from number1 to number2                    |
| ! nodes           | Invert selection of the following specification. |

* NOTES
Requires an NUMA policy aware kernel with support for page migration
(linux 2.6.16 and later).

migratepages will only move pages that are not shared with other
processes if called by a user without administrative priviledges (but
with the right to modify the process).

migratepages will move all pages if invoked from root (or a user with
administrative priviledges).

* FILES
//proc/<pid>/numa_maps/ for information about the NUMA memory use of a
process.

* COPYRIGHT
Copyright 2005-2006 Christoph Lameter, Silicon Graphics, Inc.
migratepages is under the GNU General Public License, v.2

* SEE ALSO
/numactl(8)/ , /set_mempolicy(2)/ , /get_mempolicy(2)/ , /mbind(2)/ ,
/sched_setaffinity(2)/ , /sched_getaffinity(2)/ , /proc(5)/ , /ftok(3)/
, /shmat(2)/ , /taskset(1)/
