#+TITLE: Manpages - tdbtool.8
#+DESCRIPTION: Linux manpage for tdbtool.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tdbtool - manipulate the contents TDB files

* SYNOPSIS
*tdbtool*

*tdbtool* [-l] /TDBFILE/ [/COMMANDS/...]

* DESCRIPTION
This tool is part of the *samba*(1) suite.

*tdbtool* a tool for displaying and altering the contents of Samba TDB
(Trivial DataBase) files. Each of the commands listed below can be
entered interactively or provided on the command line.

* OPTIONS
-l

#+begin_quote
  This options disables any locking, by passing TDB_NOLOCK to
  tdb_open_ex(). Only use this for database files which are not used by
  any other process! And also only if it is otherwise not possible to
  open the database, e.g. databases which were created with mutex
  locking.
#+end_quote

* COMMANDS
*create* /TDBFILE/

#+begin_quote
  Create a new database named /TDBFILE/.
#+end_quote

*open* /TDBFILE/

#+begin_quote
  Open an existing database named /TDBFILE/.
#+end_quote

*erase*

#+begin_quote
  Erase the current database.
#+end_quote

*dump*

#+begin_quote
  Dump the current database as strings.
#+end_quote

*cdump*

#+begin_quote
  Dump the current database as connection records.
#+end_quote

*keys*

#+begin_quote
  Dump the current database keys as strings.
#+end_quote

*hexkeys*

#+begin_quote
  Dump the current database keys as hex values.
#+end_quote

*info*

#+begin_quote
  Print summary information about the current database.
#+end_quote

*insert* /KEY/ /DATA/

#+begin_quote
  Insert a record into the current database.
#+end_quote

*move* /KEY/ /TDBFILE/

#+begin_quote
  Move a record from the current database into /TDBFILE/.
#+end_quote

*store* /KEY/ /DATA/

#+begin_quote
  Store (replace) a record in the current database.
#+end_quote

*storehex* /KEY/ /DATA/

#+begin_quote
  Store (replace) a record in the current database where key and data
  are in hex format.
#+end_quote

*show* /KEY/

#+begin_quote
  Show a record by key.
#+end_quote

*delete* /KEY/

#+begin_quote
  Delete a record by key.
#+end_quote

*list*

#+begin_quote
  Print the current database hash table and free list.
#+end_quote

*free*

#+begin_quote
  Print the current database and free list.
#+end_quote

*!* /COMMAND/

#+begin_quote
  Execute the given system command.
#+end_quote

*first*

#+begin_quote
  Print the first record in the current database.
#+end_quote

*next*

#+begin_quote
  Print the next record in the current database.
#+end_quote

*check*

#+begin_quote
  Check the integrity of the current database.
#+end_quote

*repack*

#+begin_quote
  Repack a database using a temporary file to remove fragmentation.
#+end_quote

*quit*

#+begin_quote
  Exit *tdbtool*.
#+end_quote

* CAVEATS
The contents of the Samba TDB files are private to the implementation
and should not be altered with *tdbtool*.

* VERSION
This man page is correct for version 3.6 of the Samba suite.

* AUTHOR
The original Samba software and related utilities were created by Andrew
Tridgell. Samba is now developed by the Samba Team as an Open Source
project similar to the way the Linux kernel is developed.
