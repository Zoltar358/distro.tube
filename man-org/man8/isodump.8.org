#+TITLE: Manpages - isodump.8
#+DESCRIPTION: Linux manpage for isodump.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about isodump.8 is found in manpage for: [[../man8/isoinfo.8][man8/isoinfo.8]]