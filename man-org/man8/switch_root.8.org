#+TITLE: Manpages - switch_root.8
#+DESCRIPTION: Linux manpage for switch_root.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
switch_root - switch to another filesystem as the root of the mount tree

* SYNOPSIS
*switch_root* [*-hV*]

*switch_root* /newroot init/ [/arg/...]

* DESCRIPTION
*switch_root* moves already mounted //proc/, //dev/, //sys/ and //run/
to /newroot/ and makes /newroot/ the new root filesystem and starts
/init/ process.

*WARNING: switch_root removes recursively all files and directories on
the current root filesystem.*

* OPTIONS
*-h, --help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-V, --version*

#+begin_quote
  Display version information and exit.
#+end_quote

* EXIT STATUS
*switch_root* returns 0 on success and 1 on failure.

* NOTES
*switch_root* will fail to function if /newroot/ is not the root of a
mount. If you want to switch root into a directory that does not meet
this requirement then you can first use a bind-mounting trick to turn
any directory into a mount point:

#+begin_quote
  #+begin_example
    mount --bind $DIR $DIR
  #+end_example
#+end_quote

* AUTHORS
* SEE ALSO
*chroot*(2), *init*(8), *mkinitrd*(8), *mount*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *switch_root* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
