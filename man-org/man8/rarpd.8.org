#+TITLE: Manpages - rarpd.8
#+DESCRIPTION: Linux manpage for rarpd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rarpd - answer RARP REQUESTs

* SYNOPSIS
*rarpd* [*-AadevV*] [*-b */bootdir/] {interface}

* DESCRIPTION
Listens for RARP requests broadcasted by clients. If the MAC address of
the client is found in /etc/ethers and the obtained hostname is
resolvable to a valid IP address from the attached network, *rarpd*
answers to the client with a RARPD reply and provides an IP address.

To allow multiple boot servers on the network *rarpd* optionally checks
if a Sun-like bootable image in the TFTP directory is present. It should
be formatted like *Hexadecimal_IP.ARCH*. For example: To load sparc
193.233.7.98, /C1E90762.SUN4M/ is linked to an image appropriate for
SUN4M in the directory /etc/tftpboot.

* WARNING
This facility is deeply obsoleted by BOOTP and later DHCP protocols.
However, some clients actually still need this to boot.

* OPTIONS
*-a*

#+begin_quote
  Listen on all available interfaces. Currently it is an internal
  option, its function is overwritten with the /interface/ argument. It
  should not be used.
#+end_quote

*-A*

#+begin_quote
  Listen not only to RARP but also ARP messages. Some rare clients use
  ARP for some unknown reason.
#+end_quote

*-v*

#+begin_quote
  Be verbose.
#+end_quote

*-d*

#+begin_quote
  Debug mode. Do not go to background.
#+end_quote

*-e*

#+begin_quote
  Do not check for the presence of a boot image. Reply if MAC address
  resolves to a valid IP address using /etc/ethers database and DNS.
#+end_quote

*-b* /bootdir/

#+begin_quote
  TFTP boot directory. Default is /etc/tftpboot
#+end_quote

*-V*

#+begin_quote
  Print version and exit.
#+end_quote

* SEE ALSO
*arping*(8), *tftpd*(8).

* AUTHOR
*rarpd* was written by Alexey Kuznetsov <kuznet@ms2.inr.ac.ru>.

* SECURITY
*rarpd* requires CAP_NET_RAW capability to listen and send RARP and ARP
packets. It also needs CAP_NET_ADMIN to assist the kernel with ARP
resolution; this is not strictly required, but some (to be more exact:
most) of the clients are so badly broken that they are not able to
answer to ARP before they are fully booted. This is no surprise, taking
into account that clients using RARPD in 2002 are all unsupported relic
creatures of the 90s and even earlier.

* AVAILABILITY
*rarpd* is part of the /iputils/ package.
