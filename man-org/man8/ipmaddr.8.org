#+TITLE: Manpages - ipmaddr.8
#+DESCRIPTION: Linux manpage for ipmaddr.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*ipmaddr* - adds, changes, deletes, and displays multicast addresses

* SYNOPSIS
*ipmaddr* [<*operation*>] [<*args*>]

* NOTE
This program is obsolete. Please use *ip maddr* instead, and see
*ip-maddress*(8) for more information.

* DESCRIPTION
The *ipmaddr* command can perform one of the following operations:

*add* - add a multicast address

*change* - change a multicast address

*del* - delete a multicast address

*show* - list multicast addresses

* SEE ALSO
*ip-maddress*(8), *ip*(8)
