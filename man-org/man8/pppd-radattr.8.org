#+TITLE: Manpages - pppd-radattr.8
#+DESCRIPTION: Linux manpage for pppd-radattr.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
radattr.so - RADIUS utility plugin for *pppd*(8)

* SYNOPSIS
*pppd* [ /options/ ] plugin radius.so plugin radattr.so

* DESCRIPTION
The radattr plugin for pppd causes all radius attributes returned by the
RADIUS server at authentication time to be stored in the file
//var/run/radattr.pppN/ where /pppN/ is the name of the PPP interface.
The RADIUS attributes are stored one per line in the format
"Attribute-Name Attribute-Value". This format is convenient for use in
/etc/ppp/ip-up and /etc/ppp/ip-down scripts.

Note that you /must/ load the radius.so plugin before loading the
radattr.so plugin; radattr.so depends on symbols defined in radius.so.

* USAGE
To use the plugin, simply supply the *plugin radius.so plugin
radattr.so* options to pppd.

* SEE ALSO
*pppd*(8)* pppd-radius*(8)

* AUTHOR
David F. Skoll <dfs@roaringpenguin.com>

Information about pppd-radattr.8 is found in manpage for: [[../\- RADIUS utility plugin for
radius.so plugin radattr.so
the radius.so plugin before loading the radattr.so plugin;
depends on symbols defined in radius.so.
plugin radius.so plugin radattr.so][\- RADIUS utility plugin for
radius.so plugin radattr.so
the radius.so plugin before loading the radattr.so plugin;
depends on symbols defined in radius.so.
plugin radius.so plugin radattr.so]]