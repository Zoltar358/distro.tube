#+TITLE: Manpages - swaplabel.8
#+DESCRIPTION: Linux manpage for swaplabel.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
swaplabel - print or change the label or UUID of a swap area

* SYNOPSIS
*swaplabel* [*-L* /label/] [*-U* /UUID/] /device/

* DESCRIPTION
*swaplabel* will display or change the label or UUID of a swap partition
located on /device/ (or regular file).

If the optional arguments *-L* and *-U* are not given, *swaplabel* will
simply display the current swap-area label and UUID of /device/.

If an optional argument is present, then *swaplabel* will change the
appropriate value on /device/. These values can also be set during swap
creation using *mkswap*(8). The *swaplabel* utility allows changing the
label or UUID on an actively used swap device.

* OPTIONS
*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

*-L*, *--label* /label/

#+begin_quote
  Specify a new /label/ for the device. Swap partition labels can be at
  most 16 characters long. If /label/ is longer than 16 characters,
  *swaplabel* will truncate it and print a warning message.
#+end_quote

*-U*, *--uuid* /UUID/

#+begin_quote
  Specify a new /UUID/ for the device. The /UUID/ must be in the
  standard 8-4-4-4-12 character format, such as is output by
  *uuidgen*(1).
#+end_quote

* ENVIRONMENT
LIBBLKID_DEBUG=all

#+begin_quote
  enables libblkid debug output.
#+end_quote

* AUTHORS
*swaplabel* was written by

and

* SEE ALSO
*uuidgen*(1), *mkswap*(8), *swapon*(8)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *swaplabel* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
