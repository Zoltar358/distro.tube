#+TITLE: Manpages - groupmod.8
#+DESCRIPTION: Linux manpage for groupmod.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
groupmod - modify a group definition on the system

* SYNOPSIS
*groupmod* [/options/] /GROUP/

* DESCRIPTION
The *groupmod* command modifies the definition of the specified /GROUP/
by modifying the appropriate entry in the group database.

* OPTIONS
The options which apply to the *groupmod* command are:

*-g*, *--gid* /GID/

#+begin_quote
  The group ID of the given /GROUP/ will be changed to /GID/.

  The value of /GID/ must be a non-negative decimal integer. This value
  must be unique, unless the *-o* option is used.

  Users who use the group as primary group will be updated to keep the
  group as their primary group.

  Any files that have the old group ID and must continue to belong to
  /GROUP/, must have their group ID changed manually.

  No checks will be performed with regard to the *GID_MIN*, *GID_MAX*,
  *SYS_GID_MIN*, or *SYS_GID_MAX* from /etc/login.defs.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help message and exit.
#+end_quote

*-n*, *--new-name* /NEW_GROUP/

#+begin_quote
  The name of the group will be changed from /GROUP/ to /NEW_GROUP/
  name.
#+end_quote

*-o*, *--non-unique*

#+begin_quote
  When used with the *-g* option, allow to change the group /GID/ to a
  non-unique value.
#+end_quote

*-p*, *--password* /PASSWORD/

#+begin_quote
  The encrypted password, as returned by *crypt*(3).

  *Note:* This option is not recommended because the password (or
  encrypted password) will be visible by users listing the processes.

  You should make sure the password respects the systems password
  policy.
#+end_quote

*-R*, *--root* /CHROOT_DIR/

#+begin_quote
  Apply changes in the /CHROOT_DIR/ directory and use the configuration
  files from the /CHROOT_DIR/ directory.
#+end_quote

*-P*, *--prefix* /PREFIX_DIR/

#+begin_quote
  Apply changes in the /PREFIX_DIR/ directory and use the configuration
  files from the /PREFIX_DIR/ directory. This option does not chroot and
  is intended for preparing a cross-compilation target. Some
  limitations: NIS and LDAP users/groups are not verified. PAM
  authentication is using the host files. No SELINUX support.
#+end_quote

* CONFIGURATION
The following configuration variables in /etc/login.defs change the
behavior of this tool:

*MAX_MEMBERS_PER_GROUP* (number)

#+begin_quote
  Maximum members per group entry. When the maximum is reached, a new
  group entry (line) is started in /etc/group (with the same name, same
  password, and same GID).

  The default value is 0, meaning that there are no limits in the number
  of members in a group.

  This feature (split group) permits to limit the length of lines in the
  group file. This is useful to make sure that lines for NIS groups are
  not larger than 1024 characters.

  If you need to enforce such limit, you can use 25.

  Note: split groups may not be supported by all tools (even in the
  Shadow toolsuite). You should not use this variable unless you really
  need it.
#+end_quote

* FILES
/etc/group

#+begin_quote
  Group account information.
#+end_quote

/etc/gshadow

#+begin_quote
  Secure group account information.
#+end_quote

/etc/login.defs

#+begin_quote
  Shadow password suite configuration.
#+end_quote

/etc/passwd

#+begin_quote
  User account information.
#+end_quote

* EXIT VALUES
The *groupmod* command exits with the following values:

/0/

#+begin_quote
  E_SUCCESS: success
#+end_quote

/2/

#+begin_quote
  E_USAGE: invalid command syntax
#+end_quote

/3/

#+begin_quote
  E_BAD_ARG: invalid argument to option
#+end_quote

/4/

#+begin_quote
  E_GID_IN_USE: specified group doesnt exist
#+end_quote

/6/

#+begin_quote
  E_NOTFOUND: specified group doesnt exist
#+end_quote

/9/

#+begin_quote
  E_NAME_IN_USE: group name already in use
#+end_quote

/10/

#+begin_quote
  E_GRP_UPDATE: cant update group file
#+end_quote

/11/

#+begin_quote
  E_CLEANUP_SERVICE: cant setup cleanup service
#+end_quote

/12/

#+begin_quote
  E_PAM_USERNAME: cant determine your username for use with pam
#+end_quote

/13/

#+begin_quote
  E_PAM_ERROR: pam returned an error, see syslog facility id groupmod
  for the PAM error message
#+end_quote

* SEE ALSO
*chfn*(1), *chsh*(1), *passwd*(1), *gpasswd*(8), *groupadd*(8),
*groupdel*(8), *login.defs*(5), *useradd*(8), *userdel*(8),
*usermod*(8).
