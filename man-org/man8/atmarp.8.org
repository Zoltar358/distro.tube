#+TITLE: Manpages - atmarp.8
#+DESCRIPTION: Linux manpage for atmarp.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
atmarp - administer classical IP over ATM connections

* SYNOPSIS
*atmarp* *-a*\\
*atmarp* *-c* [[atm]/number/]\\
*atmarp* *-q* /ip_addr/ [*qos /qos/*] [*sndbuf /bytes/*]\\
*atmarp* *-s* /ip_addr/ [/itf/.]/vpi/./vci/ [*qos /qos/*] [*sndbuf
/bytes/*] [*temp*] [*pub*] [*null*]\\
*atmarp* *-s* /ip_addr/ /atm_addr/ [*qos /qos/*] [*sndbuf /bytes/*]
[*temp*] [*pub*] [*arpsrv*]\\
*atmarp* *-d* /ip_addr/ [*arpsrv*]\\
*atmarp* *-V*

* DESCRIPTION
*atmarp* is used to maintain the ATMARP table of the ATMARP demon. The
table can be listed, new PVC and SVC entries can be added, and existing
entries can be deleted. In addition to that, *atmarp* is also used to
create new IP over ATM interfaces.

Note that the kernel has its own ATMARP table containing only entries
for destinations to which a connection exists. The table of *atmarpd*
can also contain currently unused entries.

* OPTIONS
- -a :: list the current ATMARP table.

- -c :: create the specified IP interface. If the interface number is
  omitted, the operating system assigns the next free number and
  *atmarp* prints the resulting interface name (e.g. `atm0') on standard
  output.

- -q :: sets the QOS and the send buffer size to use as the default for
  all VCs generated for that IP network (/ip_addr/ must be the address
  of the network).

- -s :: set up a PVC or create an SVC entry. The following options are
  recognized:

  - qos qos :: uses the specified quality of service (see qos(7) for the
    syntax). UBR at link speed is used by default.

  - sndbuf bytes :: tries to set the send buffer to the specified number
    of bytes. A system default value is used if *sndbuf* is not
    specified.

  - temp :: does not mark the entry as permanent, i.e. it will time out
    and then be removed.

  - pub :: publishes the entry (only relevant for ATMARP server). ATMARP
    requests for entries not marked for publishing yield an ATMARP_NAK
    response.

  - null :: uses NULL encapsulation instead of LLC/SNAP encapsulation on
    the PVC. This option is not available for SVCs, because the LLC/SNAP
    header is required to identify ATMARP packets. *null* also implies
    that the entry is permanent.

  - arpsrv :: identifies the entry pointing to the ATMARP server. Note
    that the node acting as the ATMARP server must have no ATMARP server
    entry in its ATMARP table.

- -d :: delete the specified ARP entry. In order to prevent accidental
  deletion of the ATMARP server entry, the *arpsrv* flag must be
  specified when deleting it.

- -V :: print the version number of *atmarp* on standard output and
  exit.

* FILES
- */var/run/atmarpd.table* :: ATMARP table

* AUTHOR
Werner Almesberger, EPFL ICA <Werner.Almesberger@epfl.ch>

* SEE ALSO
atmarpd(8), clip(8), qos(7)
