#+TITLE: Manpages - pam.8
#+DESCRIPTION: Linux manpage for pam.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pam.8 is found in manpage for: [[../man8/PAM.8][man8/PAM.8]]