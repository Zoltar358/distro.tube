#+TITLE: Manpages - cache_dump.8
#+DESCRIPTION: Linux manpage for cache_dump.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*cache_dump *- dump cache metadata from device or file to standard
output.

* SYNOPSIS
#+begin_example
  cache_dump [options] {device|file}
#+end_example

* DESCRIPTION
*cache_dump dumps binary cache metadata created by the device-mapper
cache* target on a device or file to standard output for analysis or
postprocessing in XML format. XML formatted metadata can be fed into
cache_restore in order to put it back onto a metadata device (to process
by the device-mapper target), or file.

This tool cannot be run on live metadata.

* OPTIONS
- **-h, --help** :: Print help and exit.

- **-V, --version** :: Print version information and exit.

- **-r, --repair** :: Repair the metadata whilst dumping it.

- **-o {xml file}** :: Specify an output file for the xml, rather than
  printing to stdout.

* EXAMPLES
Dumps the cache metadata on logical volume /dev/vg/metadata to standard
output in XML format:

#+begin_example
      $ cache_dump /dev/vg/metadata
#+end_example

Dumps the cache metadata on logical volume /dev/vg/metadata whilst
repairing it to standard output in XML format:

#+begin_example
      $ cache_dump --repair /dev/vg/metadata
#+end_example

* DIAGNOSTICS
*cache_dump returns an exit code of 0 for success or 1 for error.*

* SEE ALSO
*cache_check(8), cache_repair(8), cache_restore(8)*

* AUTHOR
Joe Thornber <ejt@redhat.com>, Heinz Mauelshagen <HeinzM@RedHat.com>
