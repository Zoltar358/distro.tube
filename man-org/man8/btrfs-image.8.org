#+TITLE: Manpages - btrfs-image.8
#+DESCRIPTION: Linux manpage for btrfs-image.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
btrfs-image - create/restore an image of the filesystem

* SYNOPSIS
*btrfs-image* [options] /<source>/ /<target>/

* DESCRIPTION
*btrfs-image* is used to create an image of a btrfs filesystem. All data
will be zeroed, but metadata and the like is preserved. Mainly used for
debugging purposes.

In the dump mode, source is the btrfs device/file and target is the
output file (use /-/ for stdout).

In the restore mode (option -r), source is the dumped image and target
is the btrfs device/file.

* OPTIONS
-r

#+begin_quote
  Restore metadump image. By default, this fixes super's chunk tree, by
  using 1 stripe pointing to primary device, so that file system can be
  restored by running tree log reply if possible. To restore without
  changing number of stripes in chunk tree check -o option.
#+end_quote

-c /<value>/

#+begin_quote
  Compression level (0 ~ 9).
#+end_quote

-t /<value>/

#+begin_quote
  Number of threads (1 ~ 32) to be used to process the image dump or
  restore.
#+end_quote

-o

#+begin_quote
  Use the old restore method, this does not fixup the chunk tree so the
  restored file system will not be able to be mounted.
#+end_quote

-s

#+begin_quote
  Sanitize the file names when generating the image. One -s means just
  generate random garbage, which means that the directory indexes won't
  match up since the hashes won't match with the garbage filenames.
  Using -ss will calculate a collision for the filename so that the
  hashes match, and if it can't calculate a collision then it will just
  generate garbage. The collision calculator is very time and CPU
  intensive so only use it if you are having problems with your file
  system tree and need to have it mostly working.
#+end_quote

-w

#+begin_quote
  Walk all the trees manually and copy any blocks that are referenced.
  Use this option if your extent tree is corrupted to make sure that all
  of the metadata is captured.
#+end_quote

-m

#+begin_quote
  Restore for multiple devices, more than 1 device should be provided.
#+end_quote

* EXIT STATUS
*btrfs-image* will return 0 if no error happened. If any problems
happened, 1 will be returned.

* SEE ALSO
*mkfs.btrfs*(8)
