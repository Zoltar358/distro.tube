#+TITLE: Manpages - grub-mkconfig.8
#+DESCRIPTION: Linux manpage for grub-mkconfig.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-mkconfig - generate a GRUB configuration file

* SYNOPSIS
*grub-mkconfig* [/OPTION/]

* DESCRIPTION
Generate a grub config file

- *-o*, *--output*=/FILE/ :: output generated config to FILE
  [default=stdout]

- *-h*, *--help* :: print this message and exit

- *-V*, *--version* :: print the version information and exit

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-install*(8)

The full documentation for *grub-mkconfig* is maintained as a Texinfo
manual. If the *info* and *grub-mkconfig* programs are properly
installed at your site, the command

#+begin_quote
  *info grub-mkconfig*
#+end_quote

should give you access to the complete manual.
