#+TITLE: Manpages - ldattach.8
#+DESCRIPTION: Linux manpage for ldattach.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldattach - attach a line discipline to a serial line

* SYNOPSIS
*ldattach* [*-1278denoVh*] [*-i* /iflag/] [*-s* /speed/] /ldisc device/

* DESCRIPTION
The *ldattach* daemon opens the specified /device/ file (which should
refer to a serial device) and attaches the line discipline /ldisc/ to it
for processing of the sent and/or received data. It then goes into the
background keeping the device open so that the line discipline stays
loaded.

The line discipline /ldisc/ may be specified either by name or by
number.

In order to detach the line discipline, *kill*(1) the *ldattach*
process.

With no arguments, *ldattach* prints usage information.

* LINE DISCIPLINES
Depending on the kernel release, the following line disciplines are
supported:

*TTY*(*0*)

#+begin_quote
  The default line discipline, providing transparent operation (raw
  mode) as well as the habitual terminal line editing capabilities
  (cooked mode).
#+end_quote

*SLIP*(*1*)

#+begin_quote
  Serial Line IP (SLIP) protocol processor for transmitting TCP/IP
  packets over serial lines.
#+end_quote

*MOUSE*(*2*)

#+begin_quote
  Device driver for RS232 connected pointing devices (serial mice).
#+end_quote

*PPP*(*3*)

#+begin_quote
  Point to Point Protocol (PPP) processor for transmitting network
  packets over serial lines.
#+end_quote

*STRIP*(*4*); *AX25*(*5*); *X25*(*6*)

#+begin_quote
  Line driver for transmitting X.25 packets over asynchronous serial
  lines.
#+end_quote

*6PACK*(*7*); *R3964*(*9*)

#+begin_quote
  Driver for Simatic R3964 module.
#+end_quote

*IRDA*(*11*)

#+begin_quote
  Linux IrDa (infrared data transmission) driver - see
  <http://irda.sourceforge.net/>
#+end_quote

*HDLC*(*13*)

#+begin_quote
  Synchronous HDLC driver.
#+end_quote

*SYNC_PPP*(*14*)

#+begin_quote
  Synchronous PPP driver.
#+end_quote

*HCI*(*15*)

#+begin_quote
  Bluetooth HCI UART driver.
#+end_quote

*GIGASET_M101*(*16*)

#+begin_quote
  Driver for Siemens Gigaset M101 serial DECT adapter.
#+end_quote

*PPS*(*18*)

#+begin_quote
  Driver for serial line Pulse Per Second (PPS) source.
#+end_quote

*GSM0710*(*21*)

#+begin_quote
  Driver for GSM 07.10 multiplexing protocol modem (CMUX).
#+end_quote

* OPTIONS
*-1*, *--onestopbit*

#+begin_quote
  Set the number of stop bits of the serial line to one.
#+end_quote

*-2*, *--twostopbits*

#+begin_quote
  Set the number of stop bits of the serial line to two.
#+end_quote

*-7*, *--sevenbits*

#+begin_quote
  Set the character size of the serial line to 7 bits.
#+end_quote

*-8*, *--eightbits*

#+begin_quote
  Set the character size of the serial line to 8 bits.
#+end_quote

*-d*, *--debug*

#+begin_quote
  Keep *ldattach* in the foreground so that it can be interrupted or
  debugged, and to print verbose messages about its progress to standard
  error output.
#+end_quote

*-e*, *--evenparity*

#+begin_quote
  Set the parity of the serial line to even.
#+end_quote

*-i*, *--iflag* /value/...

#+begin_quote
  Set the specified bits in the c_iflag word of the serial line. The
  given /value/ may be a number or a symbolic name. If /value/ is
  prefixed by a minus sign, the specified bits are cleared instead.
  Several comma-separated values may be given in order to set and clear
  multiple bits.
#+end_quote

*-n*, *--noparity*

#+begin_quote
  Set the parity of the serial line to none.
#+end_quote

*-o*, *--oddparity*

#+begin_quote
  Set the parity of the serial line to odd.
#+end_quote

*-s*, *--speed* /value/

#+begin_quote
  Set the speed (the baud rate) of the serial line to the specified
  /value/.
#+end_quote

*-c*, *--intro-command* /string/

#+begin_quote
  Define an intro command that is sent through the serial line before
  the invocation of ldattach. E.g. in conjunction with line discipline
  GSM0710, the command 'AT+CMUX=0\r' is commonly suitable to switch the
  modem into the CMUX mode.
#+end_quote

*-p*, *--pause* /value/

#+begin_quote
  Sleep for /value/ seconds before the invocation of ldattach. Default
  is one second.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* AUTHORS
* SEE ALSO
*inputattach*(1), *ttys*(4)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *ldattach* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
