#+TITLE: Manpages - uniconfd.8
#+DESCRIPTION: Linux manpage for uniconfd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
uniconfd - a daemon program for the UniConf configuration system

* SYNOPSIS
*uniconfd* [ /OPTIONS/ ] /MOUNT /...

* DESCRIPTION
UniConf is the One True Configuration system that includes all the
others because it has plugin backends /and/ frontends. Or, less
grandiosely, it's a lightweight, distributed, cacheable tree of strings.

It supports:

- retrieving, storing, and enumerating key/value pairs (where both keys
  and values are strings).

- multiple backends where the actual key/value pairs are stored.

- multiple frontends for tying it to other configuration architectures.

It operates locally, and across a network, allowing you to tie multiple
different applications together for distributed computing. Also, it
provides notifications in the form of callbacks, so your application can
be notified if a configuration key has changed.

*uniconfd* is necessary when you have more than one application, or
multiple instances of an application, sharing one configuration.
UniConf-enabled applications contact *uniconfd* which provides
notifications when any of their watched keys change.

You tell *uniconfd* which UniConf /MOUNT/ you want it to manage. See the
*MOUNTS* section for more information.

* OPTIONS
- *-f* :: Run in the foreground. Do not fork into a separate daemon
  process.

- *-d*, *-dd* :: Print debugging messages to the console. The second *d*
  increases the verbosity of the messages.

- *-V* :: Print the version number and exit.

- *-a* :: Require authentication on incoming connections.

- *-A* :: Check all accesses against a /perms/ moniker.

- *-p */port/ :: Listen on a given TCP /port/. The default is 4111. If
  /port/ is 0, then listening on TCP is disabled.

- *-s */port/ :: Listen on a given TCP /port/ wrapped in SSL. The
  default is 4112. If /port/ is 0, then listening on SSL-over-TCP is
  disabled.

- *-u */filename/ :: Listen on a given Unix socket /filename/. This is
  disabled by default.

* MOUNTS
Mounts are UniConf path monikers which are in the form:

#+begin_quote
  //SUBTREE/=/GENERATORS/:/PATH/
#+end_quote

- /SUBTREE/ :: This is the tree to manage. All trees are descended from
  the root tree, indicated by a bare slash (*/*).

- /GENERATORS/ :: These are the generators used to read and write
  key/value pairs. You can chain them with colons. For example, the
  generator chain: /cache:retry:ini/ will cache the configuration for
  speed, retry persistently if the data source disappears, and store the
  data in an INI-formatted file.

- /PATH/ :: This is the location where the data is stored. It is
  dependent on which /GENERATOR/S were specified. For instance, it could
  be: · a filename (ini://var/lib/app/config.ini/),\\
  · a network address, (tcp:/open.nit.ca:4111/),\\
  · or even an empty string (tmp:).

*Examples:*

#+begin_quote
  /=/tmp/:\\
  //ca/nit/=/ssl/:/open.nit.ca/\\
  //ca/nit/uniconfd/=/ini/://var/lib/uniconfd/uniconfd.ini/\\
  //apps/=/cache:retry:unix/://var/lib/apps/socket/
#+end_quote

* FILES
//etc/uniconfd.conf/\\
//var/lib/uniconf/uniconfd.ini/\\
//var/lib/uniconf/uniconf.ini/

* AUTHORS
This software was written by the hackers at Net Integration
Technologies. Contact us at <wvstreams-dev@lists.nit.ca>
