#+TITLE: Manpages - ip-mptcp.8
#+DESCRIPTION: Linux manpage for ip-mptcp.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ip-mptcp - MPTCP path manager configuration

* SYNOPSIS
*ip* [ /OPTIONS/ ] *mptcp* { *endpoint* | *limits* | *help* }

*ip mptcp endpoint add * /IFADDR/ [ *port* /PORT/ ] [ *dev* /IFNAME/ ] [
*id* /ID/ ] [ /FLAG-LIST/ ]

*ip mptcp endpoint del id * /ID/

*ip mptcp endpoint show * [ *id* /ID/ ]

*ip mptcp endpoint flush*

/FLAG-LIST/ := [ /FLAG-LIST/ ] /FLAG/

/FLAG/ := [ *signal* | *subflow* | *backup* ]

*ip mptcp limits set * [ *subflow* /SUBFLOW_NR/ ] [ *add_addr_accepted*
/ADD_ADDR_ACCEPTED_NR/ ]

*ip mptcp limits show*

*ip mptcp monitor*

* DESCRIPTION
MPTCP is a transport protocol built on top of TCP that allows TCP
connections to use multiple paths to maximize resource usage and
increase redundancy. The ip-mptcp sub-commands allow configuring several
aspects of the MPTCP path manager, which is in charge of subflows
creation:

The *endpoint* object specifies the IP addresses that will be used
and/or announced for additional subflows:

| ip mptcp endpoint add    | add new MPTCP endpoint             |
| ip mptcp endpoint delete | delete existing MPTCP endpoint     |
| ip mptcp endpoint show   | get existing MPTCP endpoint        |
| ip mptcp endpoint flush  | flush all existing MPTCP endpoints |

- /PORT/ :: When a port number is specified, incoming MPTCP subflows for
  already established MPTCP sockets will be accepted on the specified
  port, regardless the original listener port accepting the first MPTCP
  subflow and/or this peer being actually on the client side.

/ID/ is a unique numeric identifier for the given endpoint

- *signal* :: the endpoint will be announced/signalled to each peer via
  an ADD_ADDR MPTCP sub-option

- *subflow* :: if additional subflow creation is allowed by MPTCP
  limits, the endpoint will be used as the source address to create an
  additional subflow after that the MPTCP connection is established.

- *backup* :: the endpoint will be announced as a backup address, if
  this is a *signal* endpoint, or the subflow will be created as a
  backup one if this is a *subflow* endpoint

The *limits* object specifies the constraints for subflow creations:

| ip mptcp limits show | get current MPTCP subflow creation limits |
| ip mptcp limits set  | change the MPTCP subflow creation limits  |

- /SUBFLOW_NR/ :: specifies the maximum number of additional subflows
  allowed for each MPTCP connection. Additional subflows can be created
  due to: incoming accepted ADD_ADDR option, local *subflow* endpoints,
  additional subflows started by the peer.

- /ADD_ADDR_ACCEPTED_NR/ :: specifies the maximum number of ADD_ADDR
  suboptions accepted for each MPTCP connection. The MPTCP path manager
  will try to create a new subflow for each accepted ADD_ADDR option,
  respecting the /SUBFLOW_NR/ limit.

*monitor* displays creation and deletion of MPTCP connections as well as
addition or removal of remote addresses and subflows.

* AUTHOR
Original Manpage by Paolo Abeni <pabeni@redhat.com>
