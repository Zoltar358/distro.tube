#+TITLE: Manpages - partclone.imager.8
#+DESCRIPTION: Linux manpage for partclone.imager.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
partclone.imager - unsupported file system backup utility.(like `dd` )

* SYNOPSIS
*partclone.imager* {[*-c* | *--clone*] [*-r* | *--restore*] [*-b* |
*--dev-to-dev*]} {{*-s* | *--source*} /source/} {{*-o* | *--output*}
[*-O* | *--overwrite*] /target/} [*-dX* | *--debug=X*]
[*--restore_raw_file*] [*-z* | *--buffer_size*] [*-N* | *--ncurses*]
[*-q* | *--quiet*] [*-f* | *--UI-fresh*] [*-F* | *--force*] [*-I* |
*--ignore_fschk*] [*--ignore_crc*] [*-X* | *--dialog*] [*-C* |
*--nocheck*] [*-R* | *--rescue*] [{*-L* | *--logfile*} /logfile/]

* DESCRIPTION
*partclone.imager* is a part of *Partclone* project to clone unsupported
file system with dd method. It will backup all block from partition.
Partclone provide utilities to backup used blocks and design for higher
compatibility of the file system by using existing library, e.g.
e2fslibs is used to read the used block of ext2 partition.

*Partclone* supported file system include btrfs, ext2, ext3, ext4,
reiserfs, reiser4, xfs and jfs for LINUX. Also support some non-linux
operation system, ex: NTFS and FAT (for Windows), HFS plus(APPLE MAC
OS), UFS2(FreeBSD), VMFS(VMWare Vsphere). All partclone utils could be
run like partclone.xxx is very smiliar fsck or mkfs. For example, for
backup/restore hfsplus, just run partclone.hfsp.

* OPTIONS
The program follows the usual GNU command line syntax, with long options
starting with two dashes (`-). A summary of options is included below.

*-s */FILE/, *--source */FILE/

#+begin_quote
  Source FILE. The FILE could be a image file(made by partclone) or
  device depend on your action. Normanly, backup source is device,
  restore source is image file.

  Receving data from pipe line is supported ONLY for restoring, just
  ignore -s option or use - means receive data from stdin.
#+end_quote

*-o */FILE/, *--output */FILE/

#+begin_quote
  Output FILE. The FILE could be a image file(partclone will generate)
  or device depend on your action. Normanly, backup output to image file
  and restore output to device.

  Sending data to pipe line is also supported ONLY for back-up, just
  ignore -o option or use - means send data to stdout.
#+end_quote

*-O */FILE/, *--overwrite */FILE/

#+begin_quote
  Overwrite FILE, overwriting if exists.
#+end_quote

*-c*, *--clone*

#+begin_quote
  Save partition to the special image format.
#+end_quote

*-r*, *--restore*

#+begin_quote
  Restore partition from the special image format.
#+end_quote

*-b*, *--dev-to-dev*

#+begin_quote
  Local device to device copy on-the-fly, source and output both are
  device.
#+end_quote

*--restore_raw_file*

#+begin_quote
  Creating special raw file for loop device.
#+end_quote

*-L */FILE/, *--logfile */FILE/

#+begin_quote
  put special path to record partclone log information.(default
  /var/log/partclone.log)
#+end_quote

*-R*, *--rescue*

#+begin_quote
  Continue after disk read errors.
#+end_quote

*-C*, *--no_check*

#+begin_quote
  Dont check device size and free space.
#+end_quote

*-N*, *--ncurse*

#+begin_quote
  Using Ncurses Text User Interface.
#+end_quote

*-X*, *--dialog*

#+begin_quote
  Output message as Dialog Format.
#+end_quote

*-I*, *--ignore_fschk*

#+begin_quote
  Ignore filesystem check.
#+end_quote

*--ignore_crc*

#+begin_quote
  Ignore crc check error.
#+end_quote

*-F*, *--force*

#+begin_quote
  Force progress.
#+end_quote

*-f */sec/, *--UI-fresh */sec/

#+begin_quote
  put special second to different interval.
#+end_quote

*-z */size/, *--buffer_size */size/

#+begin_quote
  Read/write buffer size (default: 1048576)
#+end_quote

*-q*, *--quiet*

#+begin_quote
  Disable progress message.
#+end_quote

*-d*/level/, *--debug */level/

#+begin_quote
  Set the debug level [1|2|3]
#+end_quote

*-h*, *--help*

#+begin_quote
  Show summary of options.
#+end_quote

*-v*, *--version*

#+begin_quote
  Show version of program.
#+end_quote

* FILES
/var/log/partclone.log

#+begin_quote
  The log file of partclone.imager
#+end_quote

* EXAMPLES

#+begin_quote
  #+begin_example
     clone /dev/hda1 to hda1.dd.img and display debug information.
       partclone.dd -c -d -s /dev/hda1 -o hda1.dd.img

     restore /dev/hda1 from hda1.dd.img and display debug information.
       partclone.dd -r -d -s hda1.dd.img -o /dev/hda1
        
  #+end_example
#+end_quote

* DIAGNOSTICS
The following diagnostics may be issued on stderr:

*partclone.imager* provides some return codes, that can be used in
scripts:

| /Code/ | /Diagnostic/                  |
| *0*    | Program exited successfully.  |
| *1*    | Clone or Restore seem failed. |

* BUGS
Report bugs to thomas@nchc.org.tw or *http://partclone.org*.

You can get support at http://partclone.org

* SEE ALSO
*partclone*(8), *partclone.chkimg*(8), *partclone.restore*(8),
*partclone.dd*(8), *partclone.info*(8)

* AUTHOR
*Yu-Chin Tsai* <thomas@nchc.org.tw>

#+begin_quote
#+end_quote

* COPYRIGHT
\\
Copyright © 2007 Yu-Chin Tsai\\

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU General Public License, Version 2 or (at your
option) any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
