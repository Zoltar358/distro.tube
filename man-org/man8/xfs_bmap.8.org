#+TITLE: Manpages - xfs_bmap.8
#+DESCRIPTION: Linux manpage for xfs_bmap.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xfs_bmap - print block mapping for an XFS file

* SYNOPSIS
*xfs_bmap* [ *-adelpv* ] [ *-n* /num_extents/ ] /file/\\
*xfs_bmap -V*

* DESCRIPTION
*xfs_bmap* prints the map of disk blocks used by files in an XFS
filesystem. The map lists each /extent/ used by the file, as well as
regions in the file that do not have any corresponding blocks (holes).
Each line of the listings takes the following form:

#+begin_quote
  /extent/: [/startoffset/../endoffset/]: /startblock/../endblock/
#+end_quote

Holes are marked by replacing the /startblock/../endblock/ with /hole/.
All the file offsets and disk blocks are in units of 512-byte blocks, no
matter what the filesystem's block size is.

* OPTIONS
- *-a* :: If this option is specified, information about the file's
  attribute fork is printed instead of the default data fork.

- *-e* :: If this option is used, *xfs_bmap* obtains all delayed
  allocation extents, and does not flush dirty pages to disk before
  querying extent data. With the *-v* option, the /flags/ column will
  show which extents have not yet been allocated.

- *-l* :: If this option is used, then

#+begin_quote
  </nblocks/> blocks
#+end_quote

#+begin_quote
  will be appended to each line. /nblocks/ is the length of the extent
  described on the line in units of 512-byte blocks.

  This flag has no effect if the *-v* option is used.
#+end_quote

- *-n*/ num_extents/ :: If this option is given, *xfs_bmap* will display
  at most /num_extents/ extents. In the absence of *-n*, *xfs_bmap* will
  display all extents in the file.

- *-p* :: If this option is used, *xfs_bmap* obtains all unwritten
  (preallocated) extents that do not contain written data. With the *-v*
  option, the /flags/ column will show which extents are
  preallocated/unwritten.

- *-v* :: Shows verbose information. When this flag is specified,
  additional AG specific information is appended to each line in the
  following form:

#+begin_quote
  /agno/ (/startagoffset/../endagoffset/) /nblocks/ /flags/
#+end_quote

#+begin_quote
  A second *-v* option will print out the /flags/ legend.
#+end_quote

- *-V* :: Prints the version number and exits.

* SEE ALSO
*xfs_fsr*(8), *xfs*(5).
