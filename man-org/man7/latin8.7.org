#+TITLE: Manpages - latin8.7
#+DESCRIPTION: Linux manpage for latin8.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about latin8.7 is found in manpage for: [[../man7/iso_8859-14.7][man7/iso_8859-14.7]]