#+TITLE: Manpages - unixODBC.7
#+DESCRIPTION: Linux manpage for unixODBC.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
unixODBC - An ODBC implementation for Unix

* DESCRIPTION
ODBC is an open specification for providing application developers with
a predictable API with which to access Data Sources. Data Sources
include SQL Servers and any Data Source with an ODBC Driver.

The unixODBC Project goals are to develop and promote unixODBC to be the
definitive standard for ODBC on non MS Windows platforms.

The HTML-format "Administrator Manual" shipped with /unixODBC/ provides
additional details on configuration and usage to supplement these man
pages.

* ENVIRONMENT VARIABLES
- ODBCSYSINI :: Overloads path to unixODBC configuration files. By
  default equals to '/etc'.

- ODBCINSTINI :: Overloads the name of the drivers configuration file.
  It is relative to *ODBCSYSINI* and by default set to 'odbcinst.ini'.

- ODBCINSTUI :: Overloads the library name for UI. The final name that
  is searched for is evaluated as "lib$ODBCINSTUI". By default it is set
  to 'odbcinstQ4', so the resulting library name is 'libodbcinstQ4'.

- ODBCSEARCH :: Overloads the configuration mode and determines where to
  look for configuration files. Must be set to one of ODBC_SYSTEM_DSN,
  ODBC_USER_DSN or ODBC_BOTH_DSN (the last one is the default value).

- ODBCINI :: Overloads the path to user configuration file. By default
  it is set to "~/.odbc.ini".

* SEE ALSO
*unixODBC*(7), *isql*(1), *odbcinst*(1), *odbc.ini*(5),
*odbcinst.ini*(5)

®

* AUTHORS
The authors of unixODBC are Peter Harvey </pharvey@codebydesign.com/>
and Nick Gorham </nick@unixodbc.org/>. For the full list of contributors
see the AUTHORS file.

* COPYRIGHT
unixODBC is licensed under the GNU Lesser General Public License. For
details about the license, see the COPYING file.
