#+TITLE: Manpages - zmq_null.7
#+DESCRIPTION: Linux manpage for zmq_null.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_null - no security or confidentiality

* SYNOPSIS
The NULL mechanism is defined by the ZMTP 3.0 specification:
*http://rfc.zeromq.org/spec:23*. This is the default security mechanism
for ZeroMQ sockets.

* SEE ALSO
*zmq_plain*(7) *zmq_curve*(7) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
