#+TITLE: Manpages - armscii-8.7
#+DESCRIPTION: Linux manpage for armscii-8.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
armscii-8 - Armenian character set encoded in octal, decimal, and
hexadecimal

* DESCRIPTION
The Armenian Standard Code for Information Interchange, 8-bit coded
character set.

** ArmSCII-8 characters
The following table displays the characters in ArmSCII-8 that are
printable and unlisted in the *ascii*(7) manual page.

TABLE

* SEE ALSO
*ascii*(7), *charsets*(7), *utf-8*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
