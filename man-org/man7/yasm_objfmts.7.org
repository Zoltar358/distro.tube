#+TITLE: Manpages - yasm_objfmts.7
#+DESCRIPTION: Linux manpage for yasm_objfmts.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
yasm_objfmts - Yasm Supported Object Formats

* SYNOPSIS
*yasm* *-f */objfmt/ /.../

* DESCRIPTION
The standard Yasm distribution includes a number of modules for
different object formats (Yasms primary output).

The object format is selected on the *yasm*(1) command line by use of
the *-f */objfmt/ command line option.

* BIN
The “bin” object format produces a flat-format, non-relocatable binary
file. It is appropriate for producing DOS .COM executables or things
like boot blocks. It supports only 3 sections and those sections are
written in a predefined order to the output file.

* COFF
The COFF object format is an older relocatable object format used on
older Unix and compatible systems, and also (more recently) on the DJGPP
development system for DOS.

* DBG
The “dbg” object format is not a “real” object format; the output file
it creates simply describes the sequence of calls made to it by Yasm and
the final object and symbol table information in a human-readable text
format (that in a normal object format would get processed into that
object formats particular binary representation). This object format is
not intended for real use, but rather for debugging Yasms internals.

* ELF
The ELF object format really comes in three flavors: “elf32” (for 32-bit
targets), “elf64” (for 64-bit targets and “elfx32” (for x32 targets).
ELF is a standard object format in common use on modern Unix and
compatible systems (e.g. Linux, FreeBSD). ELF has complex support for
relocatable and shared objects.

* MACHO
The Mach-O object format really comes in two flavors: “macho32” (for
32-bit targets) and “macho64” (for 64-bit targets). Mach-O is used as
the object format on MacOS X. As Yasm currently only supports x86 and
AMD64 instruction sets, it can only generate Mach-O objects for
Intel-based Macs.

* RDF
The RDOFF2 object format is a simple multi-section format originally
designed for NASM. It supports segment references but not WRT
references. It was designed primarily for simplicity and has
minimalistic headers for ease of loading and linking. A complete
toolchain (linker, librarian, and loader) is distributed with NASM.

* WIN32
The Win32 object format produces object files compatible with Microsoft
compilers (such as Visual C++) that target the 32-bit x86 Windows
platform. The object format itself is an extended version of COFF.

* WIN64
The Win64 object format produces object files compatible with Microsoft
compilers that target the 64-bit “x64” Windows platform. This format is
very similar to the win32 object format, but produces 64-bit objects.

* XDF
The XDF object format is essentially a simplified version of COFF. Its a
multi-section relocatable format that supports 64-bit physical and
virtual addresses.

* SEE ALSO
*yasm*(1), *yasm_arch*(7)

* AUTHOR
*Peter Johnson* <peter@tortall.net>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2006 Peter Johnson\\
