#+TITLE: Manpages - ctags-lang-r.7
#+DESCRIPTION: Linux manpage for ctags-lang-r.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ctags-lang-r - Random notes about tagging R source code with Universal
Ctags

* SYNOPSIS
#+begin_example
  ctags ... --languages=+R ...
  ctags ... --language-force=R ...
  ctags ... --map-Python=+.r ...
#+end_example

* DESCRIPTION
This man page gathers random notes about tagging R source code with
Universal Ctags.

* KINDS
If a variable gets a value returned from a /well-known constructor/ and
the variable appears for the first time in the current input file, the R
parser makes a tag for the variable and attaches a kind associated with
the constructor to the tag regardless of whether the variable appears in
the top-level context or a function.

Well-known constructor and kind mapping

#+begin_quote

  #+begin_quote
    | _            |           |
    | Constructor  | kind      |
    | _            |           |
    | function()   | function  |
    | _            |           |
    | c()          | vector    |
    | _            |           |
    | list()       | list      |
    | _            |           |
    | data.frame() | dataframe |
    | _            |           |
  #+end_quote
#+end_quote

If a variable doesn't get a value returned from one of well-known
constructors, the R parser attaches *globalVar* or *functionVar* kind to
the tag for the variable depending on the context.

Here is an example demonstrating the usage of the kinds:

"input.r"

#+begin_quote

  #+begin_quote
    #+begin_example
      G <- 1
      v <- c(1, 2)
      l <- list(3, 4)
      d <- data.frame(n = v)
      f <- function(a) {
              g <- function (b) a + b
              w <- c(1, 2)
              m <- list (3, 4)
              e <- data.frame(n = w)
              L <- 2
      }
    #+end_example
  #+end_quote
#+end_quote

"output.tags" with "--options=NONE --sort=no --fields=+KZ -o - input.r"

#+begin_quote

  #+begin_quote
    #+begin_example
      G       input.r /^G <- 1$/;"    globalVar
      v       input.r /^v <- c(1, 2)$/;"      vector
      l       input.r /^l <- list(3, 4)$/;"   list
      d       input.r /^d <- data.frame(n = v)$/;"    dataframe
      n       input.r /^d <- data.frame(n = v)$/;"    nameattr        scope:dataframe:d
      f       input.r /^f <- function(a) {$/;"        function
      g       input.r /^      g <- function (b) a + b$/;"     function        scope:function:f
      w       input.r /^      w <- c(1, 2)$/;"        vector  scope:function:f
      m       input.r /^      m <- list (3, 4)$/;"    list    scope:function:f
      e       input.r /^      e <- data.frame(n = w)$/;"      dataframe       scope:function:f
      n       input.r /^      e <- data.frame(n = w)$/;"      nameattr        scope:dataframe:f.e
      L       input.r /^      L <- 2$/;"      functionVar     scope:function:f
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
ctags(1)
