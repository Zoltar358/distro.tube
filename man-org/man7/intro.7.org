#+TITLE: Manpages - intro.7
#+DESCRIPTION: Linux manpage for intro.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
intro - introduction to overview and miscellany section

* DESCRIPTION
Section 7 of the manual provides overviews on various topics, and
describes conventions and protocols, character set standards, the
standard filesystem layout, and miscellaneous other things.

* NOTES
** Authors and copyright conditions
Look at the header of the manual page source for the author(s) and
copyright conditions. Note that these can be different from page to
page!

* SEE ALSO
*standards*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
