#+TITLE: Manpages - zmq_plain.7
#+DESCRIPTION: Linux manpage for zmq_plain.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_plain - clear-text authentication

* SYNOPSIS
The PLAIN mechanism defines a simple username/password mechanism that
lets a server authenticate a client. PLAIN makes no attempt at security
or confidentiality. It is intended for use on internal networks where
security requirements are low. The PLAIN mechanism is defined by this
document: *http://rfc.zeromq.org/spec:24*.

* USAGE
To use PLAIN, the server shall set the ZMQ_PLAIN_SERVER option, and the
client shall set the ZMQ_PLAIN_USERNAME and ZMQ_PLAIN_PASSWORD socket
options. Which peer binds, and which connects, is not relevant.

* SEE ALSO
*zmq_setsockopt*(3) *zmq_null*(7) *zmq_curve*(7) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
