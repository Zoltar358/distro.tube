#+TITLE: Manpages - removal.7
#+DESCRIPTION: Linux manpage for removal.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*removal* - Cleaning the Slate

** Synopsis
So sad to see you go.

#+begin_quote
  #+begin_example
    sudo npm uninstall npm -g
  #+end_example
#+end_quote

Or, if that fails, get the npm source code, and do:

#+begin_quote
  #+begin_example
    sudo make uninstall
  #+end_example
#+end_quote

** More Severe Uninstalling
Usually, the above instructions are sufficient. That will remove npm,
but leave behind anything you've installed.

If that doesn't work, or if you require more drastic measures, continue
reading.

Note that this is only necessary for globally-installed packages. Local
installs are completely contained within a project's *node_modules*
folder. Delete that folder, and everything is gone less a package's
install script is particularly ill-behaved).

This assumes that you installed node and npm in the default place. If
you configured node with a different *--prefix*, or installed npm with a
different prefix setting, then adjust the paths accordingly, replacing
*/usr/local* with your install prefix.

To remove everything npm-related manually:

#+begin_quote
  #+begin_example
    rm -rf /usr/local/{lib/node{,/.npm,_modules},bin,share/man}/npm*
  #+end_example
#+end_quote

If you installed things /with/ npm, then your best bet is to uninstall
them with npm first, and then install them again once you have a proper
install. This can help find any symlinks that are lying around:

#+begin_quote
  #+begin_example
    ls -laF /usr/local/{lib/node{,/.npm},bin,share/man} | grep npm
  #+end_example
#+end_quote

Prior to version 0.3, npm used shim files for executables and node
modules. To track those down, you can do the following:

#+begin_quote
  #+begin_example
    find /usr/local/{lib/node,bin} -exec grep -l npm \{\} \; ;
  #+end_example
#+end_quote

** See also

#+begin_quote

  - npm help uninstall

  - npm help prune
#+end_quote
